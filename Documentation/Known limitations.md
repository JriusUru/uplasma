These are a few limitations UPlasma has to work around.

# Forward vs Deferred rendering

Plasma was vertex-based Forward rendering. Unity supports pixel lights in both Forward and Deferred modes.
For better looking lighting, UPlasma uses a few pixel lights.
Deferred is very useful for advanced post processing effects, so I intend to keep UPlasma compatible with it.

# Materials generation

Plasma materials can't automatically be translated into Unity shaders. Unity allows runtime compilation of shader scripts, but it's an undocumented legacy features, so I'd rather not code a complex shader generator if it's going to be removed at some point.
As a workaround, shaders for each material are written ahead of time, then specified in the Overrides of Plasma mods.

# Light groups

In Plasma, each object had a list of lights affecting it. Unity instead works with "light groups", using its 32 layers. However, only 4 layers are available in Deferred rendering.
I decided to try to limit myself to those 4 layers for maximum compatibility and whatnot.

# Lighting

Plasma allowed unlit materials to be affected by light specularity. This is evil and will require workarounds in a few Ages like Eder Tsogal.

# Transparency

Plasma used face sorting to solve alpha issues. Unity doesn't have it (mostly because it's CPU hungry and evil). As a workaround, most foliage will have to be rendered using alpha test (cutout) instead of alpha blend.

# Shadows

Plasma shadows aren't tied to specific lights - they are just projected shapes that darken the final color of objects. This is once again very evil.
Unity can only do shadows tied to specific lights, which means shadows work only for objects using full dynamic lighting. This means shadows work well in Gira, but not in Eder Tsogal and such.

# Convex hulls

Unity supports convex hull only up to 255 vertices. Triggers can't be concave meshes.
The physics importer works around most of these limitations, but sometime physics may not be 100% accurate.

# Physics interactions

Those don't work 100% well. Plasma objects can selectively block or allow cursor, avatars, kickables and camera LoS, which is harder to mimic in Unity. Will have to find a way.

# Audio and 3D sound

Plasma's audio engine is plain weird (just like the rest of the engine). 3D sounds transition from full 3D to full 2D based on distance, which in game sounds great but is incompatible with how Unity does its audio. Furthermore, sometime two mono sounds are baked into a single stereo clip (for instance for GUI on/off sounds), which is very very evil and obviously harder to correctly emulate.

