# Current state of things

UPlasma is in really early stage, so while I do my best to document things, functionalities themselves are rather lacking :P  
Right now it's possible to load most Plasma Ages, with a few limitations:
- there are almost no UIs, so the project must be configured using various JSON files or by tweaking Unity scenes.
- Shaders aren't generated at run-time, and must be written in advance. Otherwise Ages will look horrible. This is a known limitation. A few Ages already have been fixed, but not all. See "Age shader coverage" file for more infos.
- pretty much everything is WIP. Expect no interactions and lots of things crashing or behaving incorrectly.

See the "Known Limitations" file for more info.



# Architecture

UPlasma is built on top of the Unity engine. It consists of a few components:
- The **UPlasma** "core", which provides a few templates for avatar, GUIs, etc; all written in C#.
- The **PlEmu**, which handles most Plasma emulation features, written in C#.
- The **native library**, which handles interactions with C++ libraries like HSPlasma.
- [**HSPlasma**](https://github.com/H-uru/libhsplasma), a C++ library for reading Plasma (PRP) files.



# Very quick setup

First, you'll obviously need to download and install [Unity](https://unity3d.com/fr/get-unity/download/archive) (I myself usually work on the latest version available), as well as a Plasma based game (UPlasma works best with Uru - Complete Chronicles).

UPlasma requires a few native DLLs to run - a few precompiled ones are located in the `Libs/` folder. You'll need to copy those DLLs to the Unity project folder (inside `UPlasma/`, next to `Assets/` and `ProjectSettings/`). Make sure you pick the correct one for your configuration (when running things in the editor, you'll want the Debug libraries).

You can then open the `UPlasma/` folder as a Unity project. Before you can load Uru Ages and go nuts, you have to configure the `userData` folder, as well as the Plasma emulator. Fortunately for you, this is automated. Open the `UPlasma/tmp/FirstTimeSetup/FirstTimeSetup.unity` scene. Enter play mode, and follow the instructions. This will generate two files: one in the StreamingAssets folder, telling UPlasma where to store the user's own configuration files, mods and saved games (aka the `userData` folder), and a `plasma.json` file containing very basic informations about where your Uru folder is located.

Once that's done, you can load the `UPlasma/tmp/debugScene.unity`, which should allow you to quickly load an Uru Age. That's it ! You're done.

Note that the PlEmu also supports multiple game folders, can switch from one to the other at runtime, and can even merge multiple game folders into one (for instance, allowing you to play both Myst V and Uru at the same time). You'll have to tweak the `plasma.json` file to do so, however.



# Some infos about modding

UPlasma has two modding interfaces. One is for Plasma data, one is for Unity data.  
Furthermore, there are two types of mods for each interface: system mods, and user mods.

System mods are located in the `streamingAssets/` directory (in `systemMods_plasma/` and `systemMods_unity/`). They are mandatory and give hints to the engine on how to load game data, or provide patches for some un-importable data.  
User mods are located in the `userData` directory (in `mods_plasma` and `mods_unity`). Those mods are toggleable and allow players to customize their game. Those usually contain new fanmade Ages.

Back to the two modding interfaces themselves: each "mod" consists of a folder (whose name is the name of the mod), containing a descriptor (`descriptor.json`, which contains additional infos about the mod), and actual data.

Unity mods contain data serialised into Unity's own AssetBundle format (<bundle> and <bundle>.manifest files). They may also contain compiled C# scripts (DLLs) which are loaded at runtime.

Plasma mods themselves contain two types of data:
- Plasma native data (PRPs, audio files, Python PAKs, SDL, etc) which are virtually merged into the main game folder at runtime. These are essentially treated like additional data folders by the PlEmu.
- Override data, which contains loading hints and patches for Plasma data. This is required for instance to specify the correct shader for each material, disable some unnecessary objects, specify lightgroups, etc.

The list of active mods is located in the `userData` folder. This needs to be filled before mods are actually enabled.



# Building the native library

**Why ?**  
Because the project uses a few C++ libraries (mostly the Age loader which relies on HSPlasma) which must be recompiled for each platform.

**Is it required ?**  
If you only care about loading Ages, usually no. There is already a compiled version of this library in the project. As long as you're running in the 64 bit editor on Windows you're good.


I'll assume you know how to use CMake to generate and build C++ libraries, otherwise this tutorial will take entirely too much time :P

## Required

In order to build the native libraries, you'll need the following components:
- a C++ IDE, I recommend Visual Studio (2017 onwards should work), along with the following modules:
    - "Desktop C++ development"
    - "Desktop .NET development" (this is actually completely optional - only install this if you also want to use VS as your Unity C# editor)
    - "Visual C++ tools for CMake" (in individual components/compilers)
    - "Windows Universal CRT SDK (runtime C)" (in individual components/compilers)
    - "Windows XX SDK" (XX being your target Windows version, in individual components/SDKs and libraries)
- CMake, obviously
- The following libraries (download them all to a folder you can easily access to, like in My Documents).
    - [libHSPlasma](https://github.com/H-uru/libhsplasma), and its dependencies (see HSPlasma's readme):
    - string_theory
    - libpng
    - libjpeg-turbo
    - zlib


## Building

First, you will need to build HSPlasma along with its dependencies. I'll let you figure it out, cuz that would be wayy too long to explain :P

UPlasma's native library does have a CMake to generate project files, however since I suck at CMake it might not work perfectly. Hence the command-line to use it is a bit convoluted:

`cmake -G"Visual Studio 15 2017 Win64" -DEDITOR=TRUE -DCMAKE_PREFIX_PATH="<libhsplasma installation path>/share/cmake/HSPlasma" -DHSPLASMA_INCLUDE_DIR="<libhsplasma installation path>/include" -DHSPLASMA_LIBRARIES="<libhsplasma installation path>/lib/HSPlasma.lib" /<UPlasma path>/Assets/CppSource`

`-DEDITOR=TRUE` is required to generate a Unity editor build. Remove it when building for release.
