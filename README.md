# UPlasma - a Unity loader for Plasma data

## What is UPlasma ?

**UPlasma** (**UPl** for short) is a Unity-based alternative for the original [CyanWorlds.com game engine](http://wiki.openuru.org/index.php?title=CyanWorlds.com_Engine) (which the fan community mostly refers to by its former name, **Plasma**) by Cyan, Inc.

UPlasma's goal is to mimic most of Plasma's behavior, and add a few features on top of it. Right now though, it's rather barebone and mostly a fun pet project of mine to study both game engines. Achieving feature parity with Plasma is desirable though. Eventually. Maybe. Hopefully. ;)


## Main development and discussion thread

Over at the [Guild of Writers forums](https://forum.guildofwriters.org/viewtopic.php?f=10&t=6856).
Feel free to discuss anything related to the project.


## What else ?

Should you want to have a look, there is a documentation folder with infos on how to use, etc.