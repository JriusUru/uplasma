# -*- coding:utf-8 -*-

"""
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Use custom meta hook to import modules from one of the PAK files.
"""


import sys, imp
from UnityEngine import Debug
from UPlasma import PythonManager
from UPlasma.PlEmu import PlProPickedEventData

os_proxy = None
traceback = None

class ModuleLoaderImporter(object):
    def find_module(self, fullname, path):
        # if fullname in sys.modules:
            # return self
        if os_proxy:
            for p in sys.path:
                if (fullname + ".py") in os_proxy.listdir(p):
                    # module already exists in import path, don't override it
                    return None
        if PythonManager.Instance.HasScript(fullname):
            return self
        return None

    def load_module(self, fullname):
        global traceback
        # if fullname in sys.modules:
            # return sys.modules[fullname]
        
        if not PythonManager.Instance.HasScript(fullname):
            raise ImportError(fullname)
        
        print("Importing %s from pak library" % fullname)
        # create new module in which we'll import the code
        new_module = imp.new_module(fullname)
        # add it to system path so we don't import it twice
        sys.modules[fullname] = new_module
        # import the code
        exec PythonManager.Instance.LoadScript(fullname) in new_module.__dict__
        new_module.__name__ = fullname
        new_module.__file__ = fullname
        new_module.__package__ = fullname
        new_module.__module__ = fullname
        new_module.__loader__ = self
        # new_module.__path__ = []
        # new_module.__spec__ = imp.machinery.ModuleSpec(fullname, this)
        
        # if the pak is ready, also import traceback &co so we can improve logging...
        import traceback
        
        return new_module

class Tracer:
    def __init__(self):
        self.buffer = ""
    def write(self, msg):
        self.buffer += msg
    def flush(self):
        pass
    def getTrace(self):
        if traceback:
            traceback.print_stack(file=self, limit=30)
        return self.buffer
class UnityLogger:
    def __init__(self):
        self.buffer = ""
    def write(self, msg):
        self.buffer += msg
        res = self.buffer.split("\n")
        for i in range(len(res)-1):
            PythonManager.Instance.Log(res[i], Tracer())
        self.buffer = res[-1]
    def flush(self):
        if self.buffer:
            PythonManager.Instance.Log(res[i], Tracer())
            self.buffer = ""
class UnityErrorLogger:
    def __init__(self):
        self.buffer = ""
    def write(self, msg):
        self.buffer += msg
        res = self.buffer.split("\n")
        for i in range(len(res)-1):
            PythonManager.Instance.LogError(res[i], Tracer())
        self.buffer = res[-1]
    def flush(self):
        if self.buffer:
            PythonManager.Instance.LogError(res[i], Tracer())
            self.buffer = ""


def ConvertProEventDataToNotifyEvent(proEventData):
    if type(proEventData) == PlProPickedEventData:
        return [
            int(proEventData.eventType),
            1 if proEventData.enabled else 0,
            proEventData.picker,
            proEventData.picked,
            proEventData.hitPoint,
            proEventData.hitPoint # theoretically this should be converted to local space, but whatever.
        ]
    
    return []


# add the custom importer
# (this custom importer is checked first before importing any module)
sys.meta_path.append(ModuleLoaderImporter())
# import os. This must be done AFTER the module loader was registered ! The ML loads OS, then starts using it for future imports.
import os as os_proxy
# redirect printing to unity debug.log for convenience
sys.stdout = UnityLogger()
sys.stderr = UnityErrorLogger()
