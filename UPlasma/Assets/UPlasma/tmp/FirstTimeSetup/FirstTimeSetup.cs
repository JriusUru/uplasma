using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UPlasma.PlEmu;

namespace UPlasma
{
    public class FirstTimeSetup : MonoBehaviour
    {
        public Animator anim;
        public Button dataLocationValidateButton;
        public Button uruPathValidateButton;
        public Text dataLocationText;
        public Text uruPathText;

        GameManager.UserDataPathLocation curLoc;
        string curUruLoc;

        int CurState
        {
            get { return anim.GetInteger("state"); }
            set { anim.SetInteger("state", value); }
        }

        public void NextStep()
        {
            Debug.Log("NextStep");
            if (CurState == 1)
            {
                // validating userdata
                string dataPathFilePath = Path.Combine(Path.GetFullPath(Application.streamingAssetsPath), "dataPath.txt");
                File.WriteAllText(dataPathFilePath, curLoc.ToString());
            }
            else if (CurState == 2)
            {
                // validate uru location

                PlasmaGame uruFolderType = File.Exists(Path.Combine(curUruLoc, "sp.dll")) ?
                            PlasmaGame.CompleteChronicles : PlasmaGame.MystOnline;
                Debug.Log("Auto detected Uru folder as as " + uruFolderType.ToString());

                PlasmaConfig config = new PlasmaConfig
                {
                    activeGameSetup = 0,
                    gameSetups = new List<PlasmaConfig.GameSetup>()
                    {
                        new PlasmaConfig.GameSetup()
                        {
                            name = "Uru",
                            type = uruFolderType,
                            folders = new List<PlasmaConfig.GameFolder>()
                            {
                                new PlasmaConfig.GameFolder()
                                {
                                    name = "main folder",
                                    type = uruFolderType,
                                    path = curUruLoc,
                                }
                            }
                        }
                    },
                    savePath = Path.Combine(dataLocationText.text, "plasma.json")
                };
                Directory.CreateDirectory(dataLocationText.text); // just in case
                config.Save();
            }

            CurState++;

            if (CurState == 1)
            {
                // choosing userdata
                dataLocationValidateButton.interactable = false;
                dataLocationText.text = "";
            }
            else if (CurState == 2)
            {
                // choosing uru location
                uruPathValidateButton.interactable = false;
                uruPathText.text = "";
            }
        }

        public void OnUserDataLocationChanged(int value)
        {
            Debug.Log("OnUserDataLocationChanged" + value);

            dataLocationValidateButton.interactable = value != 0;

            switch (value)
            {
                case 0:
                    Debug.Log("choose");
                    dataLocationText.text = "";
                    break;
                case 1:
                case 4:
                    Debug.Log("appdata");
                    curLoc = GameManager.UserDataPathLocation.PersistentData;
                    dataLocationText.text = Path.Combine(Path.GetFullPath(Application.persistentDataPath), "userData");
                    break;
                case 2:
                    Debug.Log("my games");
                    curLoc = GameManager.UserDataPathLocation.DocumentsMyGames;
                    dataLocationText.text = Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                        "My Games",
                        Application.productName);
                    break;
                case 3:
                    Debug.Log("streamingassets");
                    curLoc = GameManager.UserDataPathLocation.StreamingAssets;
                    dataLocationText.text = Path.Combine(Path.GetFullPath(Application.streamingAssetsPath), "userData");
                    break;
            }
        }

        public void OnUruPathChanged(string value)
        {
            Debug.Log("OnUruPathChanged " + value);
            curUruLoc = Path.GetFullPath(value);
            if (Directory.Exists(curUruLoc) && File.Exists(Path.Combine(curUruLoc, "UruExplorer.exe")))
            {
                uruPathText.text = "Looks good.";
                uruPathValidateButton.interactable = true;
            }
            else
            {
                uruPathText.text = "No UruExplorer.exe in this folder.";
                uruPathValidateButton.interactable = false;
            }
        }
    }
}
