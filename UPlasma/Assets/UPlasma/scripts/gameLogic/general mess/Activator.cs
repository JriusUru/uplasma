/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;
using UnityEngine.Events;

namespace UPlasma
{
    /// <summary>
    /// Interactable object - this all-in-one script relays UnityEvents to whichever scripts wants to use mouse / VR controller
    /// input for clicking or dragging, or simply be notified when the player's avatar is in a region.
    /// Disabling the script will automatically fire all "exit" events if necessary, and prevent "enter" events from being received.
    /// Enabling it will automatically fire all "enter" events if necessary.
    /// This object requires one trigger collider (for avatar detection) and one normal collider (for mouse/controller detection).
    /// The normal collider can be omitted for non-clickables.
    /// Note that these objects can fortunately be children of the object this script is on. Unity will bubble the click/trigger
    /// events up to this script.
    /// 
    /// NOTE: this is only useful for fan-Ages. For imported Plasma Ages, use PlPickingDetector instead.
    /// 
    /// (oh, and make sure to disable queryHitTriggers in the project settings, otherwise the user will be able to click the
    /// region as well.)
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class Activator : MonoBehaviour
    {
        public enum HoverAppearance
        {
            /// <summary>
            /// No change to cursor appearance or controller model (regions).
            /// </summary>
            None,

            /// <summary>
            /// Clickable (or interactable with the index in VR).
            /// </summary>
            Click,

            /// <summary>
            /// Clickable as well, but VR controller will take appearance of open hand instead of finger.
            /// </summary>
            ClickPalm,

            /// <summary>
            /// Grabbable (index and thumb pinching the object in VR).
            /// TODO - magnet the hand to the interaction point.
            /// </summary>
            Grab,

            /// <summary>
            /// Grabbable as well, but VR controller will grab the object with the full hand instead of pinching it.
            /// </summary>
            GrabPalm,
        }
        public enum AutoDisable
        {
            None,
            OnStartHover,
            OnEndHover,
            OnStartActivate,
            OnEndActivate,
            OnAvatarEnter,
            OnAvatarExit,
        }
        
        public HoverAppearance appearance = HoverAppearance.None;
        
        [Tooltip("Event to fire when cursor is hovering the activator or the VR controller physically touches it.")]
        public UnityEvent startHover;
        [Tooltip("Event to fire when cursor stops hovering the activator or the VR controller physically stops touching it.")]
        public UnityEvent endHover;
        [Tooltip("Event to fire when clicking the activator with mouse or VR controller")]
        public UnityEvent startActivate;
        [Tooltip("Event to fire when releasing click on the activator with mouse or VR controller")]
        public UnityEvent endActivate;
        [Tooltip("Event to fire when the main avatar entered the region")]
        public UnityEvent avatarEnter;
        [Tooltip("Event to fire when the main avatar exited the region")]
        public UnityEvent avatarExit;
        
        [Tooltip("Automatically disable this activator when clicked")]
        public AutoDisable autoDisable = AutoDisable.None;
        
        // those fields KEEP updating even if the script is disabled. Hovered and activated are set to true even if outside range.
        public bool Hovered { get; protected set; }
        public bool Activated { get; protected set; }
        public bool InRange { get; protected set; }
        
        void OnMouseEnter()
        {
            Hovered = true;
            if (InRange)
            {
                if (enabled)
                {
                    GameCursor.Instance.RegisterHoveredActivator(this);
                    startHover.Invoke();
            
                    if (autoDisable == AutoDisable.OnStartHover)
                        enabled = false;
                }
            }
        }

        void OnMouseExit()
        {
            Hovered = false;
            if (InRange)
            {
                if (enabled)
                {
                    GameCursor.Instance.UnregisterHoveredActivator(this);
                    endHover.Invoke();
                    
                    if (autoDisable == AutoDisable.OnEndHover)
                        enabled = false;
                }
            }
        }

        void OnMouseDown()
        {
            Activated = true;
            if (InRange)
            {
                if (enabled)
                {
                    GameCursor.Instance.RegisterUsedActivator(this);
                    startActivate.Invoke();
                    
                    if (autoDisable == AutoDisable.OnStartActivate)
                        enabled = false;
                }
            }
        }

        void OnMouseUp()
        {
            Activated = false;
        }

        void OnMouseUpAsButton()
        {
            Activated = false;
            if (InRange)
            {
                if (enabled)
                {
                    GameCursor.Instance.UnregisterUsedActivator(this);
                    endActivate.Invoke();
                    
                    if (autoDisable == AutoDisable.OnEndActivate)
                        enabled = false;
                }
            }
        }
        
        void OnTriggerEnter(Collider collider)
        {
            if (collider.tag != "Player")
                return;
            
            InRange = true;
            if (enabled)
            {
                GameCursor.Instance.RegisterNearbyActivator(this);
                avatarEnter.Invoke();
                
                if (Hovered)
                {
                    GameCursor.Instance.RegisterHoveredActivator(this);
                    startHover.Invoke();
                }
                if (Activated)
                {
                    // this means we can start clicking an object before being in region, and the event will be fired when actually entering the region.
                    // This is a bit of an unexpected behavior, but not really bad - and it simplifies coding to make sure start/end events are always matching.
                    GameCursor.Instance.RegisterUsedActivator(this);
                    startActivate.Invoke();
                }
                
                if (autoDisable == AutoDisable.OnAvatarEnter)
                    enabled = false;
            }
        }

        void OnTriggerExit(Collider collider)
        {
            if (collider.tag != "Player")
                return;
            
            InRange = false;
            if (enabled)
            {
                GameCursor.Instance.UnregisterNearbyActivator(this);
                avatarExit.Invoke();
            
                if (Hovered)
                {
                    GameCursor.Instance.UnregisterHoveredActivator(this);
                    endHover.Invoke();
                }
                if (Activated)
                {
                    GameCursor.Instance.UnregisterUsedActivator(this);
                    endActivate.Invoke();
                }
                
                if (autoDisable == AutoDisable.OnAvatarExit)
                    enabled = false;
            }
        }
        
        void OnEnable()
        {
            if (InRange)
            {
                GameCursor.Instance.RegisterNearbyActivator(this);
                avatarEnter.Invoke();
            }
            if (Hovered && InRange)
            {
                GameCursor.Instance.RegisterHoveredActivator(this);
                startHover.Invoke();
            }
            if (Activated && InRange)
            {
                GameCursor.Instance.RegisterUsedActivator(this);
                startActivate.Invoke();
            }
        }

        void OnDisable()
        {
            if (InRange)
            {
                GameCursor.Instance.UnregisterNearbyActivator(this);
                avatarExit.Invoke();
            }
            if (Hovered && InRange)
            {
                GameCursor.Instance.UnregisterHoveredActivator(this);
                endHover.Invoke();
            }
            if (Activated && InRange)
            {
                GameCursor.Instance.UnregisterUsedActivator(this);
                endActivate.Invoke();
            }
        }
    }
}
