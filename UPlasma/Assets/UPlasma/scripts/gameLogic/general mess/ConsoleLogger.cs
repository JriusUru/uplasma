/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;
using UnityEngine.UI;

namespace UPlasma
{
    /// <summary>
    /// Handles the ingame debug console.
    /// Scripts can add themselves as command parsers using AddCommandHandler(func). This is to be used for custom console commands.
    /// 
    /// TODO:
    ///     - workaround Unity 16k chars limit ?
    ///     - set command must set config variables (only)
    ///     - use animator for better clarity
    /// </summary>
    public class ConsoleLogger : MonoBehaviour
    {
        public static ConsoleLogger Instance { get; protected set; }
        
        public CanvasGroup consoleScreen;
        public ScrollRect rect;
        public Text logText;
        public InputField inputField;
        public bool autosetFontSize = true;
        
        /*
        Queue<string> logs;
        bool consoleShown = false;
        bool logDirty = false;
        List<Func<string, bool>> commandHandlers; // functions called when a command was input (will be passed in params). These methods must return true if they handled the command, preventing other commands from parsing them or errors being printed.
        
        void Awake()
        {
            Instance = this;
            commandHandlers = new List<Func<string, bool>>();
            
            logs = new Queue<string>();
            Application.logMessageReceivedThreaded += HandleLog;
        }
        
        void Start()
        {
            if (autosetFontSize)
            {
                logText.fontSize = Mathf.Max(Screen.width / 45, 10);
                foreach (Text text in inputField.GetComponentsInChildren<Text>())
                    text.fontSize = Mathf.Max(Screen.width / 45, 10);
            }
            inputField.onEndEdit.AddListener(command => {
                if ((Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter)) && command != "")
                {
                    inputField.text = "";
                    inputField.Select();
                    inputField.ActivateInputField();
                    
                    print("<color=green>" + command + "</color>");
                    handleCommand(command);
                }
            });
            consoleScreen.alpha = 0;
        }
        
        void Update()
        {
            consoleScreen.GetComponent<Canvas>().worldCamera = GameManager.Instance.MainCamera;
            
            if (Input.GetKeyDown(KeyCode.F2)) // for now, uses the same key as the KI (hey, that's a fun sentence)
            {
                // toggles console
                consoleShown = !consoleShown;
            }
            
            // handles console transparency (TODO: us3 4n1m4tor, you dummy !)
            if (consoleShown)
            {
                consoleScreen.alpha += Time.deltaTime*3;
                bool wasInteractable = consoleScreen.interactable;
                consoleScreen.interactable = consoleScreen.blocksRaycasts = true;
                if (!wasInteractable)
                {
                    inputField.Select();
                    inputField.ActivateInputField();
                }
            }
            else
            {
                consoleScreen.alpha -= Time.deltaTime*3;
                consoleScreen.interactable = consoleScreen.blocksRaycasts = false;
            }
            
            if (logDirty && consoleShown)
            {
                // a new message was added. Refresh the view
                logDirty = false;
                float oldPos = rect.verticalNormalizedPosition;
                string log = "";
                foreach (string msg in logs)
                    log += msg + "\n";
                int maxLength = 65000 / 6; // around 4 verts per char, but using 6 as safeguard
                int currentLength = log.Length;
                if (currentLength > maxLength)
                    // max Unity text limit - we *might* go beyond...
                    log = log.Substring(log.Length-maxLength);
                logText.text = log;
                
                if (oldPos <= .00001)
                {
                    // view was already scrolled down to the last message, ensure it stays that way
                    LayoutRebuilder.ForceRebuildLayoutImmediate(rect.transform as RectTransform);
                    rect.verticalNormalizedPosition = 0;
                }
            }
        }
        
        void HandleLog(string logString, string stackTrace, LogType type)
        {
            switch (type)
            {
                case LogType.Error:
                    logString = "<color=red>" + logString + "</color>";
                    break;
                case LogType.Warning:
                    logString = "<color=yellow>" + logString + "</color>";
                    break;
                case LogType.Assert:
                    logString = "<color=cyan>" + logString + "</color>";
                    break;
                case LogType.Exception:
                    logString = "<color=red>" + logString + "</color>";
                    break;
            }
            logs.Enqueue(logString);
            // if (stackTrace != null && stackTrace != "")
                // logs.Enqueue(stackTrace);
            while ((logs.Count > GlobalGameManager.instance.globalConfig.console.logMaxLines) || (logs.Count > 60))
                logs.Dequeue();
            logDirty = true;
        }
        
        /// Handles inputted commands
        void handleCommand(string command)
        {
            string cmdLower = command.ToLower();
            bool handled = false;
            if (cmdLower == "help")
            {
                // help command. This command is rerouted to all command handlers, allowing them to display their own help
                print("  streamingassetspath -> displays streamingAssetsPath");
                print("  versionguid -> displays version GUID");
                print("  version -> displays application version number");
                print("  unityversion -> displays version of Unity Player the game was built with");
                print("  datapath -> displays application raw data path");
                print("  set <var> <value> -> set a config variable to value");
                print("  fullscreen -> toggles fullscreen");
                print("  exit -> do I need to go on ?");
                foreach (Func<string, bool> func in commandHandlers)
                    func(cmdLower);
                handled = true;
            }
            else
            {
                foreach (Func<string, bool> func in commandHandlers)
                {
                    handled = func(command);
                    if (handled)
                        break;
                }
            }
            if (!handled)
            {
                if (cmdLower == "streamingassetspath")
                {
                    handled = true;
                    print(Application.streamingAssetsPath);
                }
                else if (cmdLower == "versionguid")
                {
                    handled = true;
                    print(Application.buildGUID);
                }
                else if (cmdLower == "version")
                {
                    handled = true;
                    print(Application.version);
                }
                else if (cmdLower == "unityversion")
                {
                    handled = true;
                    print(Application.unityVersion);
                }
                else if (cmdLower == "datapath")
                {
                    handled = true;
                    print(Application.dataPath);
                }
                else if (cmdLower == "exit")
                {
                    handled = true;
                    Application.Quit();
                }
                else if (cmdLower == "fullscreen")
                {
                    handled = true;
                    Screen.fullScreen = !Screen.fullScreen;
                }
                else if (cmdLower.StartsWith("set "))
                {
                    handled = true;
                    string[] cmdSplit = cmdLower.Split(' ');
                    string variable = cmdSplit[1];
                    string valueStr = cmdSplit[2];
                    
                    // TODO handle setting config variables
                    switch (variable)
                    {
                        case "runinbackground":
                            bool value = getBoolFromStr(valueStr);
                            Application.runInBackground = value;
                            print("Application.runInBackground to " + value);
                            break;
                        default:
                            Debug.LogError("Unknown variable: " + variable);
                            break;
                    }
                }
            }
            if (!handled)
                Debug.LogError("Unknown command: " + command);
        }
        
        public void AddCommandHandler(Func<string, bool> func)
        {
            commandHandlers.Add(func);
        }
        public void RemoveCommandHandler(Func<string, bool> func)
        {
            commandHandlers.Remove(func);
        }
        
        public static void TryAddCommandHandler(Func<string, bool> func)
        {
            if (Instance != null)
                Instance.commandHandlers.Add(func);
        }
        public static void TryRemoveCommandHandler(Func<string, bool> func)
        {
            if (Instance != null)
                Instance.commandHandlers.Remove(func);
        }
        
        public static bool getBoolFromStr(string val)
        {
            val = val.ToLower();
            if (
                val == "1"
                || val == "true"
                || val == "on"
            ) return true;
            if (
                val == "0"
                || val == "false"
                || val == "off"
            ) return false;
            throw new ArgumentException(val + " cannot be parsed as boolean");
        }
        
        public void Show()
        {
            consoleShown = true;
        }
        public void Hide()
        {
            consoleShown = false;
        }
        public void Toggle()
        {
            consoleShown = !consoleShown;
        }
        public bool IsShown()
        {
            return consoleShown;
        }
        
        //*/
    }
}
