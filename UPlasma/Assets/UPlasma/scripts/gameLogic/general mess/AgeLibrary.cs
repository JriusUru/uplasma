/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;

namespace UPlasma
{
    public class AgeLibrary : MonoBehaviour
    {
        public static AgeLibrary Instance { get; protected set; }
        
        public List<AgeDescriptor> knownAges = new List<AgeDescriptor>();
        
        void Awake()
        {
            if (Instance == null)
                Instance = this;
        }
        
        public List<AgeDescriptor> GetNexusLinks()
        {
            List<AgeDescriptor> nxus = new List<AgeDescriptor>();
            nxus.AddRange(knownAges.FindAll(i => i.appearsInNexus));
            return nxus;
        }
        
        public List<AgeDescriptor> GetAgeAddons(string ageName)
        {
            List<AgeDescriptor> l = new List<AgeDescriptor>();
            l.AddRange(knownAges.FindAll(i => i.baseSceneName == ageName));
            return l;
        }
        
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }
    }
}
