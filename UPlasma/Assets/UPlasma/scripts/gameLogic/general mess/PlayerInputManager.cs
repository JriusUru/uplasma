/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma
{
    /// <summary>
    /// Simple relay for player input.
    /// Unity's input manager sucks. The new input system sound like it will be better, but it's still undocumented and rather hard to use.
    /// Also, VR input isn't standardized across all the fancy different controllers - as a matter of fact
    /// you can't have all the VR APIs installed all at once in the project, which add to the mess that is game input.
    /// 
    /// Hopefully this class will work around these limitations, by providing a base manager for keyboard/mouse.
    /// In the future we'll be able to bind it to the new input system and hopefully support all VR headsets without issues
    /// nor input lag.
    /// </summary>
    public class PlayerInputManager : MonoBehaviour
    {
        /// <summary>
        /// Types of inputs the player can supply to the main game
        /// Ex: moving a VR controller is not an input as it has no direct impact on gameplay,
        /// however moving the VR controller with natural locomotion on is an input as it moves the
        /// avatar towards a goal.
        /// </summary>
        public enum InputType
        {
            // avatar
            MoveX,      // left/right/aq/d/mouseprev/mousenext
            MoveY,      // up/down/zw/s/leftclick/middleclick
            ViewX,      // mouse
            ViewY,      // mouse
            Automove,   // a (as in Oblivion)
            Jump,       // space
            Crouch,     // c/ctrl
            Walk,       // shift
            Sprint,     // alt
            Flymode,    // f
            
            // gui
            Menu,       // escape (0 on num keypad in editor)
            PointOfView,
            KI,
            ReltoBook,
            Options,
            Screenshot,
            NewText,
            NewMarkerquest,
            NewMarker,
            
            // mouse thingies
            LockCursor, // right click
        }
        
        public static PlayerInputManager Instance { get; protected set; }
        
        public PlayerInputManager()
        {
            if (Instance == null)
                Instance = this;
        }
        ~PlayerInputManager()
        {
            if (Instance == this)
                Instance = null;
        }
        
        public virtual bool GetKeyDown(InputType key)
        {
            if (key == InputType.Automove)
                return Input.GetKeyDown(KeyCode.A);
            else if (key == InputType.Jump)
                return Input.GetButtonDown("Jump");
            else if (key == InputType.Crouch)
                return Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl);
            else if (key == InputType.Walk)
                return Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift);
            else if (key == InputType.Sprint)
                return Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown(KeyCode.RightAlt);
            else if (key == InputType.Flymode)
                return Input.GetKeyDown(KeyCode.F);
            
            else if (key == InputType.Menu)
                return Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Keypad0);
            else if (key == InputType.PointOfView)
                return Input.GetKeyDown(KeyCode.F1);
            else if (key == InputType.KI)
                return Input.GetKeyDown(KeyCode.F2);
            else if (key == InputType.ReltoBook)
                return Input.GetKeyDown(KeyCode.F3);
            else if (key == InputType.Options)
                return Input.GetKeyDown(KeyCode.F4);
            else if (key == InputType.Screenshot)
                return Input.GetKeyDown(KeyCode.F5);
            else if (key == InputType.NewText)
                return Input.GetKeyDown(KeyCode.F6);
            else if (key == InputType.NewMarkerquest)
                return Input.GetKeyDown(KeyCode.F7);
            else if (key == InputType.NewMarker)
                return Input.GetKeyDown(KeyCode.F8);
            
            else if (key == InputType.LockCursor)
                return Input.GetMouseButtonDown(1);
            
            Debug.LogError("Unsupported input key " + key);
            return false;
        }
        
        public virtual bool GetKeyUp(InputType key)
        {
            if (key == InputType.Automove)
                return Input.GetKeyUp(KeyCode.A);
            else if (key == InputType.Jump)
                return Input.GetButtonUp("Jump");
            else if (key == InputType.Crouch)
                return Input.GetKeyUp(KeyCode.C) || Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyUp(KeyCode.RightControl);
            else if (key == InputType.Walk)
                return Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift);
            else if (key == InputType.Sprint)
                return Input.GetKeyUp(KeyCode.LeftAlt) || Input.GetKeyUp(KeyCode.RightAlt);
            else if (key == InputType.Flymode)
                return Input.GetKeyUp(KeyCode.F);
            
            else if (key == InputType.Menu)
                return Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Keypad0);
            else if (key == InputType.PointOfView)
                return Input.GetKeyUp(KeyCode.F1);
            else if (key == InputType.KI)
                return Input.GetKeyUp(KeyCode.F2);
            else if (key == InputType.ReltoBook)
                return Input.GetKeyUp(KeyCode.F3);
            else if (key == InputType.Options)
                return Input.GetKeyUp(KeyCode.F4);
            else if (key == InputType.Screenshot)
                return Input.GetKeyUp(KeyCode.F5);
            else if (key == InputType.NewText)
                return Input.GetKeyUp(KeyCode.F6);
            else if (key == InputType.NewMarkerquest)
                return Input.GetKeyUp(KeyCode.F7);
            else if (key == InputType.NewMarker)
                return Input.GetKeyUp(KeyCode.F8);
            
            else if (key == InputType.LockCursor)
                return Input.GetMouseButtonUp(1);
            
            Debug.LogError("Unsupported input key " + key);
            return false;
        }
        
        public virtual bool GetKey(InputType key)
        {
            if (key == InputType.Automove)
                return Input.GetKey(KeyCode.A);
            else if (key == InputType.Jump)
                return Input.GetButton("Jump");
            else if (key == InputType.Crouch)
                return Input.GetKey(KeyCode.C) || Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
            else if (key == InputType.Walk)
                return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            else if (key == InputType.Sprint)
                return Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
            else if (key == InputType.Flymode)
                return Input.GetKey(KeyCode.F);
            
            else if (key == InputType.Menu)
                return Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.Keypad0);
            else if (key == InputType.PointOfView)
                return Input.GetKey(KeyCode.F1);
            else if (key == InputType.KI)
                return Input.GetKey(KeyCode.F2);
            else if (key == InputType.ReltoBook)
                return Input.GetKey(KeyCode.F3);
            else if (key == InputType.Options)
                return Input.GetKey(KeyCode.F4);
            else if (key == InputType.Screenshot)
                return Input.GetKey(KeyCode.F5);
            else if (key == InputType.NewText)
                return Input.GetKey(KeyCode.F6);
            else if (key == InputType.NewMarkerquest)
                return Input.GetKey(KeyCode.F7);
            else if (key == InputType.NewMarker)
                return Input.GetKey(KeyCode.F8);
            
            else if (key == InputType.LockCursor)
                return Input.GetMouseButton(1);
            
            Debug.LogError("Unsupported input key " + key);
            return false;
        }
        
        public virtual float GetAxis(InputType axis)
        {
            if (axis == InputType.MoveX)
            {
                float val = Input.GetAxisRaw("Horizontal");
                if (Input.GetMouseButton(3) && GameCursor.Instance.CursorLocked)
                {
                    // back button. Move left
                    val -= 1;
                    val = Mathf.Max(val, -1);
                }
                if (Input.GetMouseButton(4) && GameCursor.Instance.CursorLocked)
                {
                    // back button. Move right
                    val += 1;
                    val = Mathf.Min(val, 1);
                }
                return val;
            }
            else if (axis == InputType.MoveY)
            {
                float val = Input.GetAxisRaw("Vertical");
                if (Input.GetMouseButton(0) && !GameCursor.Instance.CursorOnObject && GameCursor.Instance.CursorLocked)
                {
                    // we're left-clicking, cursor is locked and not hovering any object. Move forward.
                    val += 1;
                    val = Mathf.Min(val, 1);
                    GameCursor.Instance.MovementLockingClick = true;
                }
                else
                    GameCursor.Instance.MovementLockingClick = false;
                if (Input.GetMouseButton(2) && GameCursor.Instance.CursorLocked)
                {
                    // we're middle-clicking, cursor is locked. Move backward.
                    val -= 1;
                    val = Mathf.Max(val, -1);
                }
                return val;
            }
            else if (axis == InputType.ViewX)
                return Input.GetAxisRaw("Mouse X");
            else if (axis == InputType.ViewY)
                return Input.GetAxisRaw("Mouse Y");
            
            Debug.LogError("Unsupported input axis " + axis);
            return 0;
        }
    }
}
