/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;
using Newtonsoft.Json;
using System;

namespace UPlasma
{
    [Serializable]
    public class AgeDescriptor
    {
        [Tooltip("Internal name used for the Age (.age file or scene name).")]
        public string sceneName;
        
        [Tooltip("If this scene is an addition to an existing age, this is the internal name of the base age (.age file or scene name). When this field is filled, displayName, collectionName, appearsInNexus etc are ignored.")]
        public string baseSceneName;
        
        [Tooltip("Display name for the Age. This is registered in the KI and such.")]
        public string displayName;
        
        [Tooltip("Collection in which to place the Age (for instance, 'city' for D'ni locations, etc - optional).")]
        public string collectionName;
        
        [Tooltip("Whether this Age appears in the Nexus.")]
        public bool appearsInNexus = true;
        
        [Tooltip("Whether the player can place a linking book to this Age in his Relto library.")]
        public bool registerInPersonalLibrary = true;
        
        // The following are used by the engine itself, fan-made ages shouldn't use it
        
        [JsonIgnore]
        [HideInInspector]
        public bool isFanAge = true;
        
        [JsonIgnore]
        [HideInInspector]
        public bool isUnityAge = true;
    }
}
