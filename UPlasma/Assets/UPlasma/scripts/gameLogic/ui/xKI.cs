/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UPlasma;
using UPlasma.PlEmu;

namespace UPlasma.PlEmu
{
    public class xKI : MonoBehaviour
    {
        public Animator mainInterfaceAnim;
        // public ??? playerList;
        // public ??? chatHistory;
        public Text gpsOrient;
        public Text gpsDist;
        public Text gpsElev;
        public Text ageName;
        public Text date;
        public Text time;
        // public ??? chatInputCaret;
        // public ??? chatInput;
        public Text playerName;
        public Text playerID;
        public Text playerHood;
        
        public Image markerProgress;

        readonly float markerFillMin = .1879012f; // used to compute fillAmount for marker counter
        readonly float markerFillMax = .8219646f; // used to compute fillAmount for marker counter
        
        Vector3 gpsCoordinates;
        
        void Start()
        {
            StartCoroutine(RefreshTime());
        }
        
        void Update()
        {
            if (PlMaintainersMarkerModifier.Instance != null && PlayerController.Instance != null)
                gpsCoordinates = PlMaintainersMarkerModifier.Instance.GetCoordinate(PlayerController.Instance.transform.position);
            else
                gpsCoordinates.x = gpsCoordinates.y = gpsCoordinates.z = 0;

            gpsOrient.text = ((int)gpsCoordinates.x).ToString("D5");
            gpsDist.text = ((int)gpsCoordinates.y).ToString("D");
            gpsElev.text = ((int)gpsCoordinates.z).ToString("D");
        }
        
        void SetMarkerProgress(int amount)
        {
            // 25 counter lights
            // fill range goes from markerFillMin to markerFillMax
            markerProgress.fillAmount = amount / 25f * (markerFillMax - markerFillMin) + markerFillMin;
        }
        
        IEnumerator RefreshTime()
        {
            while (true)
            {
                System.DateTime now = System.DateTime.Now;
                int h = now.Hour;
                int m = now.Minute;
                string timeStr = string.Format("{0}:{1}", h.ToString("D2"), m.ToString("D2"));
                time.text = timeStr;
                yield return new WaitForSeconds(.5f);
                timeStr = string.Format("{0} {1}", h.ToString("D2"), m.ToString("D2"));
                yield return new WaitForSeconds(.5f);
            }
        }
    }
}
