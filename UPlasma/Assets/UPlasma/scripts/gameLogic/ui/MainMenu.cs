/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;
using UPlasma.PlEmu;

namespace UPlasma
{
    public class MainMenu : MonoBehaviour
    {
        void Start ()
        {
            print("Accessed main menu. Trying to get games list...");
            // if (globalConfig.plasmaFolder == null)
            // {
                // select the game to load
            // }
            
            // load some city environment.
            // globalConfig.plasmaFolder
            
            // GlobalGameManager.instance.plasma.LoadAge("city");
            PlasmaManager.Instance.LoadPages("city", new string[]
            {
                "HarborReflect",
                "harbor",
                "ferry"
            });
        }
    }
}
