/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UPlasma
{
    public class GameCursor : MonoBehaviour
    {
        public enum PlasmaCursor
        {
            NoChange = 0,
            CursorUp,
            CursorLeft,
            CursorRight,
            CursorDown,
            CursorPoised,
            CursorClicked,
            CursorUnClicked,
            CursorHidden,
            CursorOpen,
            CursorGrab,
            CursorArrow,
            NullCursor,
        }
        
        public static GameCursor Instance { get; protected set; }
        public RectTransform cursorPos;
        public CanvasGroup cursorFader;
        public Image cursorCircle;
        public Image cursorCircleCenter;
        public Image cursorCircleCenterFill;
        public Image cursorArrowDown;
        public Image cursorArrowLeft;
        public Image cursorArrowUp;
        public Image cursorArrowRight;
        public bool CursorLocked { get; protected set; }
        
        public LinkedList<Component> nearbyActivators = new LinkedList<Component>();
        public LinkedList<Component> hoveredActivators = new LinkedList<Component>();
        public LinkedList<Component> usedActivators = new LinkedList<Component>();
        
        public bool CursorOnObject
        {
            get { return (showCursorCircleCenter || showCursorCircleCenterFill) && !movementLockingClick; }
        }
        bool movementLockingClick = false;
        public bool MovementLockingClick
        {
            // used to lock clickability while the avatar is moving (otherwise hovering a clickable while moving would stop us)
            get { return movementLockingClick; }
            set
            {
                movementLockingClick = value;
                RefreshCursorVisibility();
            }
        }
        
        bool showCursor;
        bool showCursorTransp;
        bool showCursorCircleCenter;
        bool showCursorCircleCenterFill;
        bool cursorOnUI = false;
        
        void Awake()
        {
            if (Instance == null)
                Instance = this;
        }
        
        void Start()
        {
            // DontDestroyOnLoad(this); // already done by gamemanager
            Cursor.visible = false;
            if (Application.isEditor)
                SetCursorLock(false);
            else
                SetCursorLock(true);
        }
        
        void Update()
        {
            if (CursorLocked)
                cursorPos.position = new Vector2(Screen.width / 2, Screen.height / 2);
            else
                cursorPos.position = Input.mousePosition;
            
            if (showCursor)
            {
                if (showCursorTransp)
                    cursorFader.alpha = Mathf.Min(cursorFader.alpha + Time.unscaledDeltaTime * 3, .1f);
                else
                    cursorFader.alpha = Mathf.Min(cursorFader.alpha + Time.unscaledDeltaTime * 3, 1);
            }
            else
                cursorFader.alpha = Mathf.Max(cursorFader.alpha - Time.unscaledDeltaTime * 3, 0);
            
            bool oldCursorOnUI = cursorOnUI;
            cursorOnUI = false;

            PointerEventData pe = new PointerEventData(EventSystem.current)
            {
                position = Input.mousePosition
            };
            List<RaycastResult> hits = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pe, hits);
            GameObject hitObj = null;
            foreach (RaycastResult h in hits)
            {
                hitObj = h.gameObject;
                Selectable select = hitObj.GetComponent<Selectable>();
                if (select == null)
                    select = hitObj.GetComponentInParent<Selectable>(); // some elements (toggles) don't have graphics themselves
                if (select != null && select.IsInteractable())
                {
                    cursorOnUI = true;
                    break;
                }
            }
            
            if (cursorOnUI != oldCursorOnUI)
            {
                if (cursorOnUI)
                {
                    RefreshCursorVisibility();
                    showCursor = true;
                    showCursorCircleCenterFill = false;
                    showCursorCircleCenter = true;
                    showCursorTransp = false;
                }
                else
                    RefreshCursorVisibility();
            }
            if (cursorOnUI)
                showCursorCircleCenterFill = Input.GetMouseButton(0);
            
            cursorCircleCenter.color = new Color(1,1,1, Mathf.Clamp(cursorCircleCenter.color.a + Time.unscaledDeltaTime * (showCursorCircleCenter ? 3 : -3), 0, 1));
            cursorCircleCenterFill.color = new Color(1,1,1, Mathf.Clamp(cursorCircleCenterFill.color.a + Time.unscaledDeltaTime * (showCursorCircleCenterFill ? 3 : -3), 0, 1));
            cursorArrowDown.color = new Color(1,1,1,0);
            cursorArrowUp.color = new Color(1,1,1,0);
            cursorArrowLeft.color = new Color(1,1,1,0);
            cursorArrowRight.color = new Color(1,1,1,0);
        }
        
        public void SetCursorLock(bool lockCursor)
        {
            if (lockCursor && !CursorLocked)
            {
                CursorLocked = true;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else if (!lockCursor && CursorLocked)
            {
                CursorLocked = false;
                Cursor.lockState = CursorLockMode.None;
            }
            Cursor.visible = false;
            
            RefreshCursorVisibility();
        }
        public void ToggleCursorLock()
        {
            SetCursorLock(!CursorLocked);
        }
        
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }
        
        void RefreshCursorVisibility()
        {
            if (CursorLocked)
            {
                showCursor = nearbyActivators.Count > 0;
                showCursorCircleCenterFill = usedActivators.Count > 0 && !movementLockingClick;
                showCursorCircleCenter = hoveredActivators.Count > 0 && !movementLockingClick;
                showCursorTransp = !showCursorCircleCenterFill && !showCursorCircleCenter;
            }
            else
            {
                showCursor = true;
                showCursorTransp = false;
                showCursorCircleCenter = hoveredActivators.Count > 0 && !movementLockingClick;
                showCursorCircleCenterFill = usedActivators.Count > 0 && !movementLockingClick;
            }
        }
        
        public void RegisterNearbyActivator(Component act)
        {
            nearbyActivators.AddLast(act);
            RefreshCursorVisibility();
        }
        public void RegisterHoveredActivator(Component act)
        {
            hoveredActivators.AddLast(act);
            RefreshCursorVisibility();
        }
        public void RegisterUsedActivator(Component act)
        {
            usedActivators.AddLast(act);
            RefreshCursorVisibility();
        }
        public void UnregisterNearbyActivator(Component act)
        {
            nearbyActivators.Remove(act);
            RefreshCursorVisibility();
        }
        public void UnregisterHoveredActivator(Component act)
        {
            hoveredActivators.Remove(act);
            RefreshCursorVisibility();
        }
        public void UnregisterUsedActivator(Component act)
        {
            usedActivators.Remove(act);
            RefreshCursorVisibility();
        }
    }
}
