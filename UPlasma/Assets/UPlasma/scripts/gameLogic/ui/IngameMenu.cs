/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;
using UnityEngine.UI;

namespace UPlasma
{
    public class IngameMenu : MonoBehaviour
    {
        public static IngameMenu Instance;
        
        public Animator animator;
        public RectTransform leftPanelArea;
        public RectTransform fullscreenViewArea;
        
        // public CanvasGroup settingsGroup;
        
        public bool Shown { get; protected set; }
        
        void Awake ()
        {
            if (Instance != null)
            {
                Debug.LogError("IngameMenu: only a single instance is allowed. Destroying.");
                Destroy(gameObject);
                return;
            }
            Instance = this;
        }
        
        public void OnButtonClicked(Button sender)
        {
            SettingsMenu.Instance.Hide();
            switch (sender.name)
            {
                case "ResumeBtn":
                    Show(false);
                    break;
                case "SettingsBtn":
                    SettingsMenu.Instance.Show();
                    break;
                case "ModsBtn":
                    break;
                case "MainMenuBtn":
                    break;
                case "ExitBtn":
                    Application.Quit();
                    #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                    #endif
                    break;
            }
        }
        
        void Show(bool doShow)
        {
            Shown = doShow;
            animator.SetBool("show", Shown);
            // make sure the fullscreen panel is of the right size so as to not overlap the left panel
            fullscreenViewArea.offsetMin = new Vector2(leftPanelArea.rect.width, fullscreenViewArea.offsetMin.y);
            GameManager.Instance.Pause(Shown);
        }
        
        void Update()
        {
            if (PlayerInputManager.Instance.GetKeyDown(PlayerInputManager.InputType.Menu))
                Toggle();
        }
        
        void Toggle()
        {
            Show(!Shown);
        }
        
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }
    }
}
