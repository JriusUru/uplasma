/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public static SettingsMenu Instance { get; protected set; }
    
    public Animator animator;
    public RectTransform buttonsArea;
    public RectTransform contentArea;
    
    void Start()
    {
        if (Instance == null)
            Instance = this;
    }
    
    public void Show()
    {
        if (contentArea.childCount == 0)
        {
            // try to find the first button and auto-click it to open a default view
            Button defBtn = buttonsArea.GetComponentInChildren<Button>();
            if (defBtn)
                defBtn.onClick.Invoke();
        }
        animator.SetBool("on", true);
    }
    
    public void Hide()
    {
        animator.SetBool("on", false);
    }
    
    public void SelectNewContent(GameObject contentPrefab)
    {
        // clear the area of any settings
        foreach (Transform child in contentArea)
            Destroy(child.gameObject);
        // instantiate the new prefab in this now free area
        Instantiate(contentArea, contentArea);
    }
    
    void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }
}
