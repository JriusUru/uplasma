/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;

namespace UPlasma
{
    /// <summary>
    /// Controls player movements.
    /// TODO - use a rigidbody instead. Charcontroller is cool, but Rigidbody replicates Plasma behavior best.
    /// Also handles kickables better than this script.
    /// </summary>
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        public static PlayerController Instance { get; protected set; }
        
        public Camera eyes;
        
        public float xSensitivity = 3.5f;
        public float ySensitivity = 3.5f;
        public float walkSpeed = 1.3f;
        public float flyWalkSpeed = 3f;
        public float runSpeed = 3f;
        public float flyRunSpeed = 10f;
        public float sprintSpeed = 5f;
        public float flySprintSpeed = 25f;
        public float jumpForce = 4f;
        
        public float dragCoefVertical = .2f;
        public float dragCoefHorizontal = .35f;
        
        public Vector3 velocity;
        
        CharacterController charCtrl;
        
        Vector3 moveDirection;
        float xRotation;
        float yRotation;
        enum MovementSpeed
        {
            Idle,
            Walking,
            Running,
            Sprinting
        };
        MovementSpeed currentSpeed;
        bool toggleWalk = false;
        bool doJump = false;
        bool grounded = false;
        bool flymode = false;
        bool autoMove = false;
        bool autoMoveLockForward = false;
        
        CollisionFlags collisionFlags;
        
        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
            {
                Debug.LogWarning("New player controller loaded, removing older one and replacing it with this one. This is only intended for debugging, make sure this is intentional.");
                Destroy(Instance.gameObject);
                Instance = this;
            }
        }

        void Start ()
        {
            charCtrl = GetComponent<CharacterController>();
            moveDirection = new Vector3(0, 0, 0);
            velocity = new Vector3(0, 0, 0);
            if (!Application.isEditor)
                GameCursor.Instance.SetCursorLock(true);
            currentSpeed = MovementSpeed.Idle;
        }
        
        void Update ()
        {
            if (GameManager.Instance.Paused)
                return;
            
            if (PlayerInputManager.Instance.GetKeyDown(PlayerInputManager.InputType.Flymode))
            {
                flymode = !flymode;
                // charCtrl.enabled = !flymode;
                velocity = new Vector3(0, 0, 0);
            }
            
            if (GameCursor.Instance.CursorLocked)
            {
                yRotation = PlayerInputManager.Instance.GetAxis(PlayerInputManager.InputType.ViewX) * xSensitivity;
                xRotation += PlayerInputManager.Instance.GetAxis(PlayerInputManager.InputType.ViewY) * ySensitivity;
                if (xRotation > 90)
                    xRotation = 90;
                else if (xRotation < -90)
                    xRotation = -90;
                eyes.transform.localRotation = Quaternion.Euler(-xRotation, 0, 0);
                transform.Rotate(new Vector3(0, yRotation, 0));
            }
            
            if (PlayerInputManager.Instance.GetKeyDown(PlayerInputManager.InputType.Jump) && (grounded || flymode))
                doJump = true;
            else if (PlayerInputManager.Instance.GetKeyUp(PlayerInputManager.InputType.Jump))
                doJump = false;
            if (PlayerInputManager.Instance.GetKey(PlayerInputManager.InputType.Sprint))
                currentSpeed = MovementSpeed.Sprinting;
            else if (PlayerInputManager.Instance.GetKey(PlayerInputManager.InputType.Walk) && !toggleWalk)
                currentSpeed = MovementSpeed.Walking;
            else
                currentSpeed = MovementSpeed.Running;
            if (PlayerInputManager.Instance.GetKeyDown(PlayerInputManager.InputType.Automove))
            {
                autoMove = !autoMove;
                autoMoveLockForward = autoMove;
            }
            
            float forwardInput = PlayerInputManager.Instance.GetAxis(PlayerInputManager.InputType.MoveY);
            float rightInput = PlayerInputManager.Instance.GetAxis(PlayerInputManager.InputType.MoveX);
            if (autoMove)
            {
                if (forwardInput < -.25f)
                    autoMove = false;
                if (forwardInput > .25f)
                {
                    if (!autoMoveLockForward)
                        autoMove = false;
                }
                else
                    autoMoveLockForward = false;
                if (autoMove)
                    forwardInput = 1;
            }
            forwardInput = Mathf.Clamp(forwardInput, -1, 1);
            rightInput = Mathf.Clamp(rightInput, -1, 1);
            
            Transform moveTransform = transform;
            if (flymode)
                moveTransform = eyes.transform;
            moveDirection = moveTransform.forward * forwardInput + moveTransform.right * rightInput;
            if (moveDirection.magnitude < .01 && moveDirection.magnitude > -.01 && !doJump)
                currentSpeed = MovementSpeed.Idle;
            moveDirection.Normalize();
            
            if (PlayerInputManager.Instance.GetKeyUp(PlayerInputManager.InputType.LockCursor))
                GameCursor.Instance.ToggleCursorLock();
        }
    
        void FixedUpdate()
        {
            float step = Time.fixedDeltaTime;
            
            if (!flymode)
                velocity += Physics.gravity*step;
            
            // air drag. Not physically correct, but whatever.
            velocity.x -= step * velocity.x * dragCoefHorizontal;
            velocity.y -= step * velocity.y * dragCoefVertical;
            
            Vector3 movementVelocity = new Vector3(moveDirection.x, moveDirection.y, moveDirection.z);
            float movementSpeed;
            switch (currentSpeed)
            {
                case MovementSpeed.Walking:
                    if (flymode)
                        movementSpeed = flyWalkSpeed;
                    else
                        movementSpeed = walkSpeed;
                    break;
                case MovementSpeed.Sprinting:
                    if (flymode)
                        movementSpeed = flySprintSpeed;
                    else
                        movementSpeed = sprintSpeed;
                    break;
                case MovementSpeed.Running:
                default:
                    if (flymode)
                        movementSpeed = flyRunSpeed;
                    else
                        movementSpeed = runSpeed;
                    break;
            }
            movementVelocity *= movementSpeed * transform.lossyScale.y;
            
            // if moving and grounded, add a downwards force to prevent falling into stairs
            // TODO - possible amelioration: slow down the charcontroller a bit when walking up slopes
            // (raycast down and get normal, dot it with forward, etc)
            if (grounded && !doJump && !flymode)
                movementVelocity.y -= charCtrl.stepOffset/step;
            
            if (doJump)
            {
                if (flymode)
                {
                    movementVelocity.y += movementSpeed;
                }
                else
                {
                    velocity.y += jumpForce;
                    doJump = false;
                }
            }
            
            if (flymode)
            {
                transform.position += movementVelocity*step;
                collisionFlags = CollisionFlags.None;
            }
            else
            {
                Vector3 absoluteMovement = movementVelocity;
                absoluteMovement = absoluteMovement + velocity;
                collisionFlags = charCtrl.Move(absoluteMovement * step);
            }
            if ((collisionFlags & CollisionFlags.Below) != 0)
            {
                velocity *= 0;
                grounded = true;
            }
            else
                grounded = false;
        }
        
        void OnControllerColliderHit(ControllerColliderHit hit)
        {
            // /*
            // push away kickables
            
            Rigidbody body = hit.collider.attachedRigidbody;
            // don't move the rigidbody if the character is on top of it
            if (collisionFlags == CollisionFlags.Below)
                return;
            if (body == null || body.isKinematic)
                return;
            Vector3 velocity = hit.moveDirection;
            float kickStrength = 1f;
            switch (currentSpeed)
            {
                case MovementSpeed.Walking:
                    kickStrength = 1.5f;
                    break;
                case MovementSpeed.Running:
                    kickStrength = 3;
                    break;
                case MovementSpeed.Sprinting:
                    kickStrength = 6;
                    break;
            }
            // kick the sob
            body.AddForceAtPosition(velocity * kickStrength, hit.point, ForceMode.Impulse);
            
            // TODO: improve this model. This is totally not realistic. Keep the ability to shoot straight forward, though.
            // We do love our soccer games in Minkata...
            
            //*/
            
            /*
            
            Rigidbody body = hit.collider.attachedRigidbody;
            if (body == null || body.isKinematic)
                return;
            
            // We apply the force on the point hit, using the move direction of the controller,
            // and multiply this force by the abs(dot(move, normal)) - this means sliding along the object won't push it much.
            Vector3 force = hit.moveDirection * Mathf.Abs(Vector3.Dot(hit.moveDirection.normalized, hit.normal));
            body.AddForceAtPosition(force, hit.point, ForceMode.Impulse);
            
            //*/
        }
        
        public void OnGamePause()
        {
            // release the cursor
            GameCursor.Instance.SetCursorLock(false);
        }
        
        public void OnGameUnpause()
        {
            // unpaused - recapture the cursor
            GameCursor.Instance.SetCursorLock(true);
        }
        
        void OnDrawGizmos()
        {
            /*
            Draw the charcontroller's outside shell (default gizmo only draws inner shell).
            Note: since gravity velocity is reset when touching ground, the avatar's outer shell
            collides with the ground. Therefore, charcontroller's radius/height must take skin width into account.
            //*/

            // nope, can't push the script to the Git without the author's consent. It's probably fine but I'm too lazy to ask -
            // I'll just reimplement it myself at some point...

            //CharacterController charCtrl = GetComponent<CharacterController>();
            //Gizmos.color = Color.white;
            //Vector3 a = new Vector3(0, charCtrl.center.y - charCtrl.height / 2 - charCtrl.skinWidth, 0);
            //Vector3 b = new Vector3(0, charCtrl.center.y + charCtrl.height / 2 + charCtrl.skinWidth, 0);
            //UPlasmaExtensions.DebugExtension.DrawCapsule(transform.position + a, transform.position + b, Color.white, charCtrl.radius + charCtrl.skinWidth);
        }
        
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }
    }
}
