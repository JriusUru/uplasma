/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using UnityEngine;
using UPlasma.PlEmu;

namespace UPlasma
{
    /// <summary>
    /// Descriptor for a Plasma mod. This descriptor is saved as descriptor.json in the mod's folder
    /// </summary>
    [Serializable]
    public class PlasmaModDescriptor
    {
        /// <summary>
        /// UAM descriptor, as found in uam.status.xml
        /// This is obviously for compatibility with downloading from UAM servers...
        /// </summary>
        [Serializable]
        public class UamDescriptor
        {
            [Serializable]
            public class UamDescriptorVersion
            {
                [Tooltip("Name (date) of the version")]
                public string name;
                [Tooltip("Archive type (zip/7z/...)")]
                public string archive;
                [Tooltip("Checksum")]
                public string sha1;
                [Tooltip("Download URL")]
                public string mirror;
            }
            
            [Tooltip("File name of the age without .age extension (often similar to mainfile")]
            public string filename;
            
            [Tooltip("Used to find whether the Age might already be installed some other way (interrogation mark in UAM)")]
            public string mainfile;
            
            [Tooltip("Name as displayed in the mod manager")]
            public string name;
            
            [Tooltip("Whether the Age/mod can be uninstalled once installed")]
            public bool deletable;
            
            [Tooltip("Verbose description")]
            public string info;
            
            [Tooltip("Minimum version of UAM/Drizzle required to be able to download the file")]
            public int minver;
            
            [Tooltip("Files to delete with the mod")]
            public string del;
            
            public UamDescriptorVersion version;
        }
        
        [Tooltip("Game version of the PRPs in this mod. NOTE - this is different from the \"game\" field ! This is used to ensure the PRP loader correctly opens your files")]
        public PlasmaGame prpVersion = PlasmaGame.CompleteChronicles;
        
        [Tooltip("Descriptor if the Age was downloaded with UAM (optional)")]
        public UamDescriptor uamDescriptor;
    }
}
