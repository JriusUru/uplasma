/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

namespace UPlasma
{
    /// <summary>
    /// Mod entry object.
    /// This is attached to a usually light prefab, instantiated when the mod is loaded, destroyed when unloaded, and
    /// kept instanced while the program is alive.
    /// </summary>
    public class ModMainEntry : MonoBehaviour
    {
        // NOTE: those three variables are set at runtime by the mod manager, when the mod is loaded.
        // [SerializeField]
        [HideInInspector]
        public ModDescriptor runtimeDescriptor;
        [HideInInspector]
        public Assembly runtimeAssembly;
        [HideInInspector]
        public List<AssetBundle> runtimeBundles;

        /// <summary>
        /// Called when the mod is loaded by the ModManager. This is called on every game start if the mod itself is enabled.
        /// Returns true on success. If there is an error activating the mod, you should return false so the mod manager
        /// cleans memory by unloading any bundle it might have loaded.
        /// The mod manager will handle adding available scenes to the Nexus.
        /// </summary>
        /// <param name="gameLoading"></param>
        /// <returns></returns>
        public virtual bool Activate(bool gameLoading)
        {
            // override to add more activation conditions, specific treatment, etc.
            return true;
        }

        /// <summary>
        /// Called when the mod is disabled by the user through the ModManager interface. Returns true on success.
        /// If the mod can't be deactivated and force is false, return false so the ModManager keeps the mod loaded.
        /// Note that uninstallation of the mod can be forced by the user(force= true), which can't be aborted by returning false.
        /// Also note that nothing prevents the user from uninstalling your mod manually, bypassing this function call entirely.
        /// (The mod manager handles unloading the bundles, assembly and unregistering Ages.It also avoids deactivating the mod
        /// if one of its scene is currently loaded.)
        /// </summary>
        /// <param name="force"></param>
        /// <returns></returns>
        public virtual bool Deactivate(bool force)
        {
            // override to add more conditions, deactivation steps, etc.
            return true;
        }
    }
}
