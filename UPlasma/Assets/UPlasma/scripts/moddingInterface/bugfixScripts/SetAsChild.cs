/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UPlasma.PlEmu;

namespace UPlasma.Bugfixes
{
    /// <summary>
    /// When the scene is loaded, this object is automatically set as another's child in the last loaded Age.
    /// </summary>
    public class SetAsChild : MonoBehaviour
    {
        [Tooltip("Name of the page in which is located the object")]
        public string pageName;
        [Tooltip("Name of the parent object")]
        public string objName;
        [Tooltip("Do the parenting on start if true, on awake otherwise")]
        public bool onStart = false;
        
        void Awake()
        {
            if (!onStart)
                DoSetAsChild();
        }
        void Start()
        {
            if (onStart)
                DoSetAsChild();
        }
        
        void DoSetAsChild()
        {
            GameObject par = PlAge.GetLatest().GetPage(pageName).GetObj(objName);
            Transform parTrans = par.transform;
            transform.parent = parTrans;
        }
    }
}
