/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma
{
    /// <summary>
    /// Descriptor for a mod. This descriptor is saved as descriptor.json in the mod's folder.
    /// </summary>
    [Serializable]
    public class ModDescriptor
    {
        /// <summary>
        /// Main purpose of the mod
        /// </summary>
        public enum ModPurpose
        {
            NewContent,
            Modification,
            Bugfix
        }
        
        [Tooltip("User-friendly name (for displaying in lists and such). Try not to make it too long.")]
        public string displayName;
        
        [Tooltip("Your name, or name of your team")]
        public string author;
        
        [Tooltip("Verbose description of what your mod does.")]
        public string description;
        
        [Tooltip("Ages (or Unity scenes) in the mod. Use this to specify scenes that should automatically " +
            "be added to the Nexus.")]
        public List<AgeDescriptor> ages = new List<AgeDescriptor>();
        
        [Tooltip("What other mods this one requires. When this mod is activated, all others are activated " +
            "and loaded before it.")]
        public List<string> modDependencies = new List<string>();
        
        //[Tooltip("Games for which your mod is intended. Leave empty, unless your mod is certain to not work with " +
        //    "a specific game. PRP version shouldn't be taken into account for Plasma mods")]
        //public List<PlasmaGame> game = new List<PlasmaGame>() { };
        
        [Tooltip("What the mod mainly does")]
        public ModPurpose purpose = ModPurpose.NewContent;
        
        [Tooltip("Keywords about what your mod does (for search function). Try to use only single-word lowercase.")]
        public List<string> tags = new List<string>();
        
        [Tooltip("Build number of your mod (automatically increases with each build). It can serve as version " +
            "number if you want, otherwise you're free to ignore it)")]
        public uint buildId = 1;
        
        [Tooltip("Can this mod be installed while the player is exploring an Age ? Unchecking means the mod " +
            "can only be installed in the main menu.")]
        public bool canInstallAtRuntime = true;
        
        [Tooltip("Can this mod be uninstalled while the player is exploring an Age ? Unchecking means the mod " +
            "can only be uninstalled in the main menu.")]
        public bool canUninstallAtRuntime = true;
        
        [Tooltip("Mod author authenticity proof. Not yet implemented.")]
        public string signature;
        
        [HideInInspector]
        [Tooltip("The path to your mod's main object (holding the ModMainEntry item). This must be formatted " +
            "NameOfTheBundle/SomePath/SomeObject.prefab")]
        public string mainAssetName = "";
    }
}
