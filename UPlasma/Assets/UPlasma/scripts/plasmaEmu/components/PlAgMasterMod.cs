/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
Worth noting - conversion from Plasma's space to Unity's:
- Quats are always converted directly from the native bindings
- Vectors are always converted by the C# animator script. This includes the following:
	- foot to meter conversion
	- Y/Z axis swap
	- conversion to degrees when necessary
Reason: quats will always be a rotation and use Unity's own class, so it's better to
setup them correctly from the start. Vectors can represent various things (scale, pos, eulers, colors...)
so it's better to wait until we know exactly what they are used for.
*/

namespace UPlasma.PlEmu
{
    /**
     * Copy of Plamza's shi...annoying animation system.
     * We can't create animationclips from Plasma animations, due to them being too different from Unity's,
     * and Plamza's system being a bit "insane" because of its lack of genericity (I see a pattern here).
     * 
     * This took me entirely too much time to figure out... I'm dumb :shrug:
     * 
     * The <see cref="PlAgMasterMod"/> contains and plays several <see cref="AgAnim"/>. Most of the time they play separately.
     * <see cref="AgAnim"/> themselves contain multiple <see cref="IAgApplicator"/> (one for each scene object). Those applicators
     * are tied to GameObjects by the <see cref="PlAgModifier"/>, and their role is obviously to apply the sampled value from the curve
     * onto an object's property.
     * <see cref="IAgApplicator"/> contain multiple <see cref="IAgChannel"/> (one for each "component" or modifier on the object).
     * Channels contain multiple Controllers (usually one for each animated property of the object).
     * 
     * Controllers:
     *      I simply hate the way Plasma handles controllers. MOUL tried to trim the number of classes
     *      by merging everything into the CompoundController (which is good) and doing some
     *      weird duck-typing lookalike to circumvent the lack of strong controller typing (which is bad).
     *      I tried refitting it here but no matter how you handle it, it just sucks IMHO. Also very
     *      difficult to setup from the native side.
     *      
     *      So instead we're going to redo it from scratch with some stronger typing. This will likely
     *      be less flexible, but screw this.
     * 
     * 
     * (...
     *      Why we are not using Unity's own animation system:
     *      - Plamza stores object transforms directly into matrices. Unity stores them into pos/rot/scale
     *        vectors/quaternion, and matrices are only used when computing the world space position.
     *        Unity's way is the "good way" (if there is such a thing), because it allows animating rotation
     *        and scale independently (contrarily to matrices, which do not have rotation nor scale).
     *      - It seems Unity's animation system only supports float interpolators, which are used for
     *        colors, vectors AND quaternions. Which means quats are interpolated linearly instead of
     *        spherically, which is really bad. How does Unity appear to support quat rotations ?
     *        By resampling them to euler rotations on build instead.
     *        I *guess* that's because there is no way to smooth quats using hermit curves ?
     * ...)
     * 
     * NOTE: the whole animation system is going to run several times each frame, and thus is going to be quite
     * time-critical. The current implementation should be optimized enough. Hopefully.
     * Measured: .8 ms in Relto. A bit higher than I expected, but we still have plenty of headroom...
     * 
     * Another note: might be worth converting it to Unity's job system.
     */

    /// <summary>
    /// Top-level message handler for animations.
    /// </summary>
    public class PlAgMasterMod : MonoBehaviour, IPlMessageable, ICannotBeBatched
    {
        public AgAnim[] anims;

        Dictionary<string, PlAgModifier> channels = new Dictionary<string, PlAgModifier>();

        void Start()
        {
            // About AGModifiers:
            // It seems those MUST be children of the master modifier (or on the same object). Good, this makes it easier for us.
            channels = GetComponentsInChildren<PlAgModifier>(true).ToDictionary(t => t.channelName, t => t);
            foreach (AgAnim anim in anims)
            {
                anim.Start();
                anim.UpdateTargets(channels);
            }
        }

        void Update()
        {
            if (anims.Length > 0)
                anims[0].Apply(Time.time); // just play the first anim for now...
        }

        public bool MsgReceive(PlMessage msg)
        {
            // yes, no, sorry, we're not interested right now. Goodbye.
            return false;
        }
    }
}
