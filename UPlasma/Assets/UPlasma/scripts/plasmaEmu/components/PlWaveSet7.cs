/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A Unity reimplementation of Plasma's plWaveSet7 component (and associated shader).
    /// The original code comes from the following article and associated sources by Mark Finch (all
    /// credit for the incredible code goes to him):
    /// https://developer.nvidia.com/gpugems/GPUGems/gpugems_ch01.html
    /// 
    /// TODO:
    /// - need to find out what/how to use the spec vector to control normal draw distance
    ///     currently it just sucks and is completely wrong
    ///     I think I swapped specvec.x with wispiness, but I'm not sure
    ///     spec start and end don't work the same with this code, why ?
    /// 
    /// - it seems both the Plamza and Unity implementation suffer from floating point precision error, leading to vertices
    ///     snapping into position, and weird "walls" occuring between waves. Might be worth fixing.
    /// - I'm not really fond of the "4 waves cycling" of this script, I know it's by design and works fine but I dunno if it could be improved
    /// - might be worth making a very simplified version for fan-ages ? wave arithmetics and texture generation might also be
    ///     a tad overkill - to be confirmed
    /// - tesselation for the water would be nice. This could be useful for geostates, but would also provide a nice upgrade
    ///     for texstates. Update: tesselation is complex and requires lots of custom code for vert/frag shaders.
    /// - there are some well-known (at least to me) performances issues, I think they're due to the wave length being too short.
    ///     Might be worth investigating/optimizing.
    /// - need to convert to Unity's axis and unit system before sending to the GPU (instead of when the game starts)
    /// 
    /// 
    /// Note:
    /// - it's worth noting texture choppiness cannot be changed at runtime as it's baked into the generated texture
    /// - if a property is [HideInInspector], it means it's either unused by this script, or memory managed by the script.
    ///     Hence it not being displayed in the inspector.
    /// - shores:
    ///     there is a whole buncha stuff related to the shore that we never see ingame.
    ///     Most notably, some sort of procedural foam for shores, consisting of
    ///     graph+bubbles+edge textures.
    ///     What this means is anyone's guess (bubbles probably means foam bubbles...).
    ///     You ask me, this is probably some programmer nonsense made by someone who wanted
    ///     completely procedural water. In the end, looks like Cyan went the usual way of simply
    ///     creating and blending a few shore textures manually... Much saner if you ask me, but hey.
    /// - scrunch:
    ///     once again something we never see in plamza. Somehow "scrunches" the waves. Some sort of
    ///     vertical scaling ?
    /// </summary>
    [Serializable]
    public class WaterState
    {
        [Tooltip("Maximum length of a generated wave (feet). Also affects wave height a bit.")]
        public float maxLen;
        [Tooltip("Minimum length of a generated wave (feet). Also affects wave height a bit.")]
        public float minLen;
        [Tooltip("Amplitude (divided by wavelength) for the waves. Increase to make stronger waves.")]
        public float ampOverlen;
        [Tooltip("How sharp wave crests are. 0 means round waves, higher values make sharper waves.\n" +
            "In the case of texture waves, this can only be changed before this script's Start().")]
        public float chop;
        [Tooltip("How much waves follow the wind's general direction (in radians). " +
            "0 = all parallel to wind dir, 1.6 = messy, 3.14 = utter mess.")]
        public float angleDev;
        public WaterState(float maxLen, float minLen, float ampOverlen, float chop, float angleDev)
        {
            this.maxLen = maxLen;
            this.minLen = minLen;
            this.ampOverlen = ampOverlen;
            this.chop = chop;
            this.angleDev = angleDev;
        }
        [HideInInspector]
        public int transIdx;
        [HideInInspector]
        public float transDel = -.2f;
    }
    [Serializable]
    public class WaterStateParams
    {
        [Tooltip("UV scale for texture waves.")]
        public float rippleScale=100;
        [Tooltip("Global height level of the water.")]
        public float waterHeight=0;
        [Tooltip("UNUSED.\n" +
            "Somehow multiplies the number of bubbles generated per second on the shore texture.")]
        public float wispiness=.5f;
        [Tooltip("UNUSED.\n" +
            "Somehow affects the lifetime of bubbles on the shore texture.")]
        public float period=1;
        [Tooltip("UNUSED.\n" +
            "Somehow affects the size of bubbles on the shore texture.")]
        public float fingerlength=1;
    }
    
    [Serializable]
    public class PlFixedWaterState7
    {
        // those default values come from my onlyisland Age, and represent a giganormous ocean with big-ass waves.
        // not really realistic by any stretch of the word, but useful for debugging.
        [Tooltip("Wave params for geometric deformation")]
        public WaterState geoState = new WaterState(670, 45, .24f, 1.4f, .81f);
        [Tooltip("Wave params for texture deformation")]
        public WaterState texState = new WaterState(62.5f, 8.8f, .2f, .7f, 1.3f);
        [HideInInspector]
        //[Tooltip("Increases randomness in texture wave speed")]
        public float texStateSpeedDeviation = 0; // NOT in Plasma - TODO might map to another value
        public WaterStateParams stateParams = new WaterStateParams();
        [Tooltip("Global direction vector for the wind. Z component is unused. Usually "
            + "you want to make sure that this vector is normalized, ie: (x*x + y*y) = 1")]
        public Vector3 windDir = new Vector3(0, -1, 0);
        [Tooltip("X - additional noise on the texture waves (useful for simulating rain).\n"
            + "Y - distance after which texture waves start to fade away."
            + "Z - distance after which texture waves are completely faded."
            + "Y and Z are useful to avoid aliasing of far off water. Usually, Z = distance to farthest " +
            "vertex, Y = 1/4 * Z")]
        public Vector3 specVec = new Vector3(.35f, 2000, 8000);
        [Tooltip("Water offset is a stupidly misleading name. What this does is change various " +
            "values of the depth filter thingie. More info:\n" +
            "X = multiplier for the depth-based opacity\n" +
            "Y = reflectivity multiplier" +
            "Z = unknown")]
        public Vector3 waterOffset = new Vector3(1, 1, 0);
        [Tooltip("? UNKNOWN - UNUSED ?")]
        public Vector3 maxAtten = new Vector3(1, 1, 1);
        [Tooltip("? UNKNOWN - UNUSED ?")]
        public Vector3 minAtten = new Vector3(0, 0, 0);
        [Tooltip("UNUSED.\n" +
            "Looks like something related to shore blending\n" +
            "From PyPRP docs, this should be something like this:\n" +
            "X = opacity\n" +
            "Y = reflectivity\n" +
            "Z = wave")]
        public Vector3 depthFalloff = new Vector3(.5f, 1, 1);
        [Tooltip("? UNKNOWN - UNUSED ?\n" +
            "Looks like the shore's runtime color")]
        public Color shoreTint = new Color(1, 1, 1, 1);
        [Tooltip("? UNKNOWN - UNUSED ?\n" +
            "Looks like it's used for the (missing) shore bubble texture color")]
        public Color maxColor = new Color(1, 1, 1, 1);
        [Tooltip("? UNKNOWN - UNUSED ?" +
            "Looks like it's used for the (missing) shore bubble texture color")]
        public Color minColor = new Color(.18f, .17f, .11f, 1);
        [Tooltip("? UNKNOWN - UNUSED ?\n" +
            "Used by the (missing) shore bubble tex")]
        public float edgeOpacity = 1;
        [Tooltip("? UNKNOWN - UNUSED ?" +
            "Used by the (missing) shore bubble tex")]
        public float edgeRadius = 1;
        [Tooltip("? UNKNOWN - UNUSED ?\n" +
            "Runtime color of the water ?...")]
        public Color waterTint = new Color(1, 1, 1, 1);
        [Tooltip("? UNKNOWN - UNUSED ?\n" +
            "Multiplier for the reflected color")]
        public Color specularTint = new Color(1, 1, 1, 1);
        [Tooltip("How often the reflections of the water are updated, in seconds. Usually you want " +
            "to leave this to 0 (disabled), unless you have a day/night cycle which requires " +
            "updating the reflection.\n" +
            "Try to leave this value to either 0 or at a high number (10 seconds for instance), " +
            "as updating the reflection can be pretty heavy.")]
        public float envRefresh = 0;
        [Tooltip("UNUSED.\n" +
            "Theoretically this should be the distance of the reflections when using box projection, but " +
            "this isn't implemented right now")]
        public float envRadius = 8000;
        [Tooltip("UNUSED.\n" +
            "Theoretically this should be the center of the reflections when using box projection, but " +
            "this isn't implemented right now")]
        public Vector3 envCenter = new Vector3(0, 0, 0);
    }

    // DO NOT RUNINEDITOR. We manage creation and destruction of textures - we want to ensure we don't leak those somehow. TODO.
    [RequireComponent(typeof(MeshRenderer))]
    public class PlWaveSet7 : MonoBehaviour
    {
        /// <summary>
        /// Holds pointers to material properties, to allow setting properties on each frame without passing dozens of strings around.
        /// </summary>
        public class MatWSParams
        {
            public int tEnvMap;
            public int tBumpMap;
            public int cWaterTint;
            public int cFrequency;
            public int cPhase;
            public int cAmplitude;
            public int cDirX;
            public int cDirY;
            public int cSpecAtten;
            public int cEnvAdjust;
            public int cEnvTint;
            public int cLengths;
            public int cDepthOffset;
            public int cDepthScale;
            public int cDirXK;
            public int cDirYK;
            public int cDirXW;
            public int cDirYW;
            public int cKW;
            public int cDirXSqKW;
            public int cDirYSqKW;
            public int cDirXDirYKW;
            public int _InvFade;
            public int _DistortionIntensity;
            
            public MatWSParams()
            {
                tEnvMap = Shader.PropertyToID("tEnvMap");
                tBumpMap = Shader.PropertyToID("tBumpMap");
                cWaterTint = Shader.PropertyToID("cWaterTint");
                cFrequency = Shader.PropertyToID("cFrequency");
                cPhase = Shader.PropertyToID("cPhase");
                cAmplitude = Shader.PropertyToID("cAmplitude");
                cDirX = Shader.PropertyToID("cDirX");
                cDirY = Shader.PropertyToID("cDirY");
                cSpecAtten = Shader.PropertyToID("cSpecAtten");
                cEnvAdjust = Shader.PropertyToID("cEnvAdjust");
                cEnvTint = Shader.PropertyToID("cEnvTint");
                cLengths = Shader.PropertyToID("cLengths");
                cDepthOffset = Shader.PropertyToID("cDepthOffset");
                cDepthScale = Shader.PropertyToID("cDepthScale");
                cDirXK = Shader.PropertyToID("cDirXK");
                cDirYK = Shader.PropertyToID("cDirYK");
                cDirXW = Shader.PropertyToID("cDirXW");
                cDirYW = Shader.PropertyToID("cDirYW");
                cKW = Shader.PropertyToID("cKW");
                cDirXSqKW = Shader.PropertyToID("cDirXSqKW");
                cDirYSqKW = Shader.PropertyToID("cDirYSqKW");
                cDirXDirYKW = Shader.PropertyToID("cDirXDirYKW");
                _InvFade = Shader.PropertyToID("_InvFade");
                _DistortionIntensity = Shader.PropertyToID("_DistortionIntensity");
            }
        }
        public class MatCCParams
        {
            public int tCosineLUT;
            public int tBiasNoise;
            public int[] cUTrans;
            public int[] cCoef;
            public int cReScale;
            public int cNoiseXForm0_00;
            public int cNoiseXForm0_10;
            public int cNoiseXForm1_00;
            public int cNoiseXForm1_10;
            public int cScaleBias;
            
            public MatCCParams()
            {
                tCosineLUT = Shader.PropertyToID("tCosineLUT");
                tBiasNoise = Shader.PropertyToID("tBiasNoise");
                cUTrans = new int[16];
                cCoef = new int[16];
                for (int i=0; i<16; i++)
                {
                    cUTrans[i] = Shader.PropertyToID("cUTrans" + i);
                    cCoef[i] = Shader.PropertyToID("cCoef" + i);
                }
                cReScale = Shader.PropertyToID("cReScale");
                cNoiseXForm0_00 = Shader.PropertyToID("cNoiseXForm0_00");
                cNoiseXForm0_10 = Shader.PropertyToID("cNoiseXForm0_10");
                cNoiseXForm1_00 = Shader.PropertyToID("cNoiseXForm1_00");
                cNoiseXForm1_10 = Shader.PropertyToID("cNoiseXForm1_10");
                cScaleBias = Shader.PropertyToID("cScaleBias");
            }
        }
        
        [HideInInspector]
        //[Tooltip("? UNKNOWN - UNUSED ?")]
        public float maxLen;
        public PlFixedWaterState7 state = new PlFixedWaterState7();
        [Tooltip("UNUSED\n" +
            "Shore objects. These have the same geometric deforms as the water, as well as " +
            "some bump mapping I don't know how to use. Will implement later.")]
        public GameObject[] shores;
        [Tooltip("UNUSED\n" +
            "Decals on the water (avatar splashing and such). Not implemented yet.")]
        public GameObject[] decals;
        [Tooltip("Buoy objects floating on this water (not in original Plasma)")]
        public List<PlBuoy> buoys;
        [Tooltip("The probe used to capture environmental reflections. You want to set this, " +
            "otherwise your water will be completely blank.")]
        public ReflectionProbe envMap;

        [Tooltip("Controls the speed of texture waves (not in original Plasma)")]
        public float textureSpeedMult = 1;
        [Tooltip("Controls the speed of geometric waves (not in original Plasma)")]
        public float geometrySpeedMult = 1;

        [Tooltip("Click this toggle to reset the water's waves, and immediately apply some properties " +
            "which are usually applied over time.")]
        public bool reset = false;
        
        // those textures/material are created at run-time ↓
        CustomRenderTexture bumpMap;
        Texture2D cosineLUT;
        Texture2D biasNoiseMap;
        
        Material waveComputeMaterial;
        
        MatWSParams matWSParams = new MatWSParams();
        MatCCParams matCCParams = new MatCCParams();
        
        class GeoWave
        {
            public float phase;
            public float amp;
            public float len;
            public float freq;
            public Vector2 dir;
            public float fade;
        }
        class TexWave
        {
            public float phase;
            public float amp;
            public float len;
            public float speed;
            public float freq;
            public Vector2 dir;
            public Vector2 rotScale;
            public float fade;
        }
        
        Material mat;
        GeoWave[] geoWaves;
        TexWave[] texWaves;

        // Usually those are best left alone, for the sole reason that they are usually stored
        // in vectors of fixed size in the shader.
        readonly int NumGeoWaves = 4;
        readonly int NumBumpPerPass = 4;
        readonly int NumTexWaves = 16;
        int NumBumpPasses;
        readonly int BumpTexSize = 256; // no real need to change this, but hey
        readonly float GravConst = 30; // approximative gravity constant in feet ?
        
        void Start()
        {
            NumBumpPasses = NumTexWaves / NumBumpPerPass;
            mat = GetComponent<MeshRenderer>().sharedMaterial;
            geoWaves = new GeoWave[NumGeoWaves];
            for (int i=0; i<NumGeoWaves; i++)
                geoWaves[i] = new GeoWave();
            texWaves = new TexWave[NumTexWaves];
            for (int i=0; i<NumTexWaves; i++)
                texWaves[i] = new TexWave();
            
            // set the correct shader for the material (arguments will be passed to it later)
            mat.shader = Shader.Find("Plasma/WaveSet/WaveSet7");
            // create normal compute material and texture
            waveComputeMaterial = new Material(Shader.Find("Plasma/WaveSet/CompCosine"));
            bumpMap = new CustomRenderTexture(BumpTexSize, BumpTexSize, RenderTextureFormat.ARGB32)
            {
                initializationMode = CustomRenderTextureUpdateMode.OnDemand,
                initializationColor = new Color(.5f, .5f, 1),
                material = waveComputeMaterial,
                wrapMode = TextureWrapMode.Repeat,
                useMipMap = true,
                autoGenerateMips = true,
                anisoLevel = 9,
                filterMode = FilterMode.Trilinear
            };
            int numPasses = 5;
            CustomRenderTextureUpdateZone[] updateZones = new CustomRenderTextureUpdateZone[numPasses];
            for (int i=0; i<5; i++)
            {
                CustomRenderTextureUpdateZone zone = new CustomRenderTextureUpdateZone
                {
                    passIndex = i,
                    updateZoneCenter = new Vector2(.5f, .5f),
                    updateZoneSize = new Vector2(1, 1)
                };
                updateZones[i] = zone;
            }
            bumpMap.SetUpdateZones(updateZones);
            bumpMap.updateMode = CustomRenderTextureUpdateMode.Realtime;
            bumpMap.Initialize();
            
            CreateCosineLUT();
            CreateBiasNoiseMap();
            InitTexWaves();
            InitGeoWaves();

            // setup the probe
            // we'll refresh the probe ourselves
            envMap.refreshMode = UnityEngine.Rendering.ReflectionProbeRefreshMode.ViaScripting;
            envMap.timeSlicingMode = UnityEngine.Rendering.ReflectionProbeTimeSlicingMode.IndividualFaces;
            // refresh at least once
            envMap.RenderProbe();
            StartCoroutine(ProbeRefreshCoroutine());
        }

        /// <summary>
        /// Refreshes the reflection probe every now and then.
        /// The refresh rate used is the one from the plWaveSet7 state, and NOT
        /// the cubic environmap's one (and those don't always match).
        /// This should be a good enough appromixation.
        /// If the probe is set to refresh every 0 seconds, then the probe
        /// won't update at all (like in Plasma).
        /// I might move this code to a custom script attached directly to the RP
        /// later, as this would be more "correct" behavior.
        /// </summary>
        /// <returns></returns>
        IEnumerator ProbeRefreshCoroutine()
        {
            while (enabled)
            {
                if (state.envRefresh != 0)
                {
                    yield return new WaitForSeconds(state.envRefresh);
                    envMap.RenderProbe();
                }
                else
                    yield return new WaitForSeconds(1);
            }
        }
        
        void OnDestroy()
        {
            if (cosineLUT != null)
                Destroy(cosineLUT);
            if (biasNoiseMap != null)
                Destroy(biasNoiseMap);
            if (bumpMap != null)
                Destroy(bumpMap);
            if (waveComputeMaterial != null)
                Destroy(waveComputeMaterial);
        }
        
        /// <summary>
        /// Updates the shader params before rendering.
        /// </summary>
        void Update()
        {
            if (reset)
            {
                // reset allows us to quickly change a specific water setting in the inspector
                // (otherwise we have to wait for waves to be regenerated, which can take a while)
                reset = false;
                InitTexWaves();
                InitGeoWaves();
            }
            // clamp max delta time (not that it's really required...)
            float deltaTime = Time.deltaTime;
            if (deltaTime > .3f)
                deltaTime = .3f;

            // update shader params
            UpdateTexWaves(deltaTime);
            UpdateGeoWaves(deltaTime);
            
            // render the bump texture from the newly computed tex wave params
            RenderTexture(deltaTime);
            
            // "render" the mesh (by which we really mean just transfer the current state to the material)
            SetWaterParams();

            // update buoy objects
            if (buoys != null)
            {
                foreach (PlBuoy buoy in buoys)
                {
                    if (buoy == null)
                        continue;
                    if (buoy.canCull && !buoy.GetComponent<MeshRenderer>().isVisible)
                        // outside the screen, don't update it
                        continue;
                    if (buoy.canRotate)
                    {
                        // modify position and rotation
                        Vector3[] pbtn = DisplaceAndRotatePoint(buoy.transform.parent.position);
                        buoy.transform.position = pbtn[0];
                        Quaternion quat = Quaternion.identity;
                        quat.SetLookRotation(pbtn[3].normalized, pbtn[1].normalized);
                        buoy.transform.rotation = quat;
                    }
                    else
                    {
                        // only modify position
                        buoy.transform.position = DisplacePoint(buoy.transform.parent.position);
                    }
                }
            }
        }

        /// <summary>
        /// Sends water parameters to the material's shader.
        /// </summary>
        void SetWaterParams()
        {
            mat.SetVector(matWSParams.cWaterTint, state.waterTint);
            
            Vector4 freq = new Vector4(geoWaves[0].freq, geoWaves[1].freq, geoWaves[2].freq, geoWaves[3].freq);
            mat.SetVector(matWSParams.cFrequency, freq);
            
            Vector4 phase = new Vector4(geoWaves[0].phase, geoWaves[1].phase, geoWaves[2].phase, geoWaves[3].phase);
            mat.SetVector(matWSParams.cPhase, phase);
            
            Vector4 amp = new Vector4(geoWaves[0].amp, geoWaves[1].amp, geoWaves[2].amp, geoWaves[3].amp);
            mat.SetVector(matWSParams.cAmplitude, amp);
            
            Vector4 dirX = new Vector4(geoWaves[0].dir.x, geoWaves[1].dir.x, geoWaves[2].dir.x, geoWaves[3].dir.x);
            mat.SetVector(matWSParams.cDirX, dirX);
            
            Vector4 dirY = new Vector4(geoWaves[0].dir.y, geoWaves[1].dir.y, geoWaves[2].dir.y, geoWaves[3].dir.y);
            mat.SetVector(matWSParams.cDirY, dirY);
            
            float normScale = state.specVec.x * state.texState.ampOverlen * 2 * Mathf.PI;
            normScale *= NumBumpPasses + state.specVec.x;
            normScale *= (state.texState.chop + 1);
            
            Vector4 specAtten = new Vector4(
                // negate those first two numbers because, huuuuh...
                -state.specVec.z,                           // spec end
                -1 / (state.specVec.z - state.specVec.y),   // spec transition range
                normScale * .1f,
                1 / state.stateParams.rippleScale);
            mat.SetVector(matWSParams.cSpecAtten, specAtten);
            
            Vector3 envCenter = envMap.transform.position;
            Vector3 camToCen = envCenter - GameManager.Instance.MainCamera.transform.position;
            float G = camToCen.magnitude * .3048f - state.envRadius * state.envRadius;
            mat.SetVector(matWSParams.cEnvAdjust, new Vector4(camToCen.x, camToCen.z, camToCen.y, G));
            
            Vector4 envTint = new Vector4(1, 1, 1, 1); // TODO: is it mapped in Plasma ?
            mat.SetVector(matWSParams.cEnvTint, envTint);
            
            Vector4 lengths = new Vector4(geoWaves[0].len, geoWaves[1].len, geoWaves[2].len, geoWaves[3].len);
            mat.SetVector(matWSParams.cLengths, lengths);
            
            // add water offset to depth offset → non-GPUgems behavior, dunno why Plamza does that.
            Vector4 depthOffset = new Vector4(state.stateParams.waterHeight + state.waterOffset.x,
                state.stateParams.waterHeight                               + state.waterOffset.y,
                state.stateParams.waterHeight                               + state.waterOffset.z,
                state.stateParams.waterHeight);
            mat.SetVector(matWSParams.cDepthOffset, depthOffset);
            
            Vector4 depthScale = new Vector4(.5f, .5f, .5f, 1); // TODO: is it mapped in Plasma ?
            mat.SetVector(matWSParams.cDepthScale, depthScale);
            
            float K = 5;
            if (state.geoState.ampOverlen > state.geoState.chop / (2 * Mathf.PI * NumGeoWaves * K))
                K = state.geoState.chop / (2*Mathf.PI * state.geoState.ampOverlen * NumGeoWaves);
            Vector4 dirXK = new Vector4(geoWaves[0].dir.x * K, 
                geoWaves[1].dir.x * K, 
                geoWaves[2].dir.x * K, 
                geoWaves[3].dir.x * K);
            Vector4 dirYK = new Vector4(geoWaves[0].dir.y * K, 
                geoWaves[1].dir.y * K, 
                geoWaves[2].dir.y * K, 
                geoWaves[3].dir.y * K);
            mat.SetVector(matWSParams.cDirXK, dirXK);
            mat.SetVector(matWSParams.cDirYK, dirYK);
            
            Vector4 dirXW = new Vector4(geoWaves[0].dir.x * geoWaves[0].freq, 
                geoWaves[1].dir.x * geoWaves[1].freq, 
                geoWaves[2].dir.x * geoWaves[2].freq, 
                geoWaves[3].dir.x * geoWaves[3].freq);
            Vector4 dirYW = new Vector4(geoWaves[0].dir.y * geoWaves[0].freq, 
                geoWaves[1].dir.y * geoWaves[1].freq, 
                geoWaves[2].dir.y * geoWaves[2].freq, 
                geoWaves[3].dir.y * geoWaves[3].freq);
            mat.SetVector(matWSParams.cDirXW, dirXW);
            mat.SetVector(matWSParams.cDirYW, dirYW);
            
            Vector4 KW = new Vector4(K * geoWaves[0].freq,
                K * geoWaves[1].freq,
                K * geoWaves[2].freq,
                K * geoWaves[3].freq);
            mat.SetVector(matWSParams.cKW, KW);
            
            Vector4 dirXSqKW = new Vector4(geoWaves[0].dir.x * geoWaves[0].dir.x * K * geoWaves[0].freq,
                geoWaves[1].dir.x * geoWaves[1].dir.x * K * geoWaves[1].freq,
                geoWaves[2].dir.x * geoWaves[2].dir.x * K * geoWaves[2].freq,
                geoWaves[3].dir.x * geoWaves[3].dir.x * K * geoWaves[3].freq);
            mat.SetVector(matWSParams.cDirXSqKW, dirXSqKW);
            
            Vector4 dirYSqKW = new Vector4(geoWaves[0].dir.y * geoWaves[0].dir.y * K * geoWaves[0].freq,
                geoWaves[1].dir.y * geoWaves[1].dir.y * K * geoWaves[1].freq,
                geoWaves[2].dir.y * geoWaves[2].dir.y * K * geoWaves[2].freq,
                geoWaves[3].dir.y * geoWaves[3].dir.y * K * geoWaves[3].freq);
            mat.SetVector(matWSParams.cDirYSqKW, dirYSqKW);
            
            Vector4 dirXdirYKW = new Vector4(geoWaves[0].dir.y * geoWaves[0].dir.x * K * geoWaves[0].freq,
                geoWaves[1].dir.x * geoWaves[1].dir.y * K * geoWaves[1].freq,
                geoWaves[2].dir.x * geoWaves[2].dir.y * K * geoWaves[2].freq,
                geoWaves[3].dir.x * geoWaves[3].dir.y * K * geoWaves[3].freq);
            mat.SetVector(matWSParams.cDirXDirYKW, dirXdirYKW);
            
            mat.SetTexture(matWSParams.tEnvMap, envMap.texture);
            mat.SetTexture(matWSParams.tBumpMap, bumpMap);
        }
        
        /// <summary>
        /// Creates the cosine lookup table used by the normalmap compute shader.
        /// </summary>
        void CreateCosineLUT()
        {
            // create a new argb tex, with a height of 1px, and no mipmap
            cosineLUT = new Texture2D(BumpTexSize, 1, TextureFormat.ARGB32, false, true);
            
            Color[] pDat = new Color[BumpTexSize];
            for (int i=0; i<BumpTexSize; i++)
            {
                float dist = (float)i / ((float)BumpTexSize-1) * 2 * Mathf.PI;
                float c = Mathf.Cos(dist);
                float s = Mathf.Sin(dist);
                s *= 0.5f;
                s += 0.5f;
                s = Mathf.Pow(s, state.texState.chop);
                c *= s;
                float cosDist = c * 0.5f + 0.5f;
                pDat[i] = new Color(cosDist, cosDist, 1, 1);
            }
            
            cosineLUT.SetPixels(pDat);
            
            // upload texture, don't update mipmaps (since there are none), and free CPU clone.
            cosineLUT.Apply(false, true);
        }
        
        /// <summary>
        /// Creates the noise normalmap used to give a bit more detail to the reflection (rain...).
        /// </summary>
        void CreateBiasNoiseMap()
        {
            // create a new argb tex, and no mipmap
            biasNoiseMap = new Texture2D(BumpTexSize, BumpTexSize, TextureFormat.ARGB32, false, true);
            
            // give a random value to red and green pixels
            Color[] pDat = new Color[BumpTexSize * BumpTexSize];
            for (int i=0; i<BumpTexSize*BumpTexSize; i++)
            {
                pDat[i] = new Color(
                    UnityEngine.Random.value, UnityEngine.Random.value, 1, 1
                );
            }
            
            biasNoiseMap.SetPixels(pDat);
            
            // upload texture, don't update mipmaps (since there are none), and free CPU clone.
            biasNoiseMap.Apply(false, true);
        }
        
        /// <summary>
        /// Reinits geometric wave params.
        /// </summary>
        void InitGeoWaves()
        {
            for (int i=0; i<NumGeoWaves; i++)
                InitGeoWave(i);
        }
        
        /// <summary>
        /// Reinits texture wave params.
        /// </summary>
        void InitTexWaves()
        {
            int i;
            for (i=0; i<NumTexWaves; i++)
                InitTexWave(i);
        }
        
        /// <summary>
        /// Reinits a specific geometric wave's param.
        /// </summary>
        /// <param name="i">index of the wave</param>
        void InitGeoWave(int i)
        {
            geoWaves[i].phase = UnityEngine.Random.value * Mathf.PI * 2;
            geoWaves[i].len = state.geoState.minLen + UnityEngine.Random.value * (state.geoState.maxLen - state.geoState.minLen);
            geoWaves[i].amp = geoWaves[i].len * state.geoState.ampOverlen / (float)NumGeoWaves;
            geoWaves[i].freq = 2 * Mathf.PI / geoWaves[i].len;
            geoWaves[i].fade = 1;

            float rotBase = state.geoState.angleDev;

            float rads = rotBase * (UnityEngine.Random.value * 2 - 1);
            float rx = Mathf.Cos(rads);
            float ry = Mathf.Sin(rads);

            // Plasma inverts winddir (the GPUgems version doesn't). Don't ask :shrug:
            // Just negate both components to account for this.
            float x = -state.windDir.x;
            float y = -state.windDir.y;
            geoWaves[i].dir.x = x * rx + y * ry;
            geoWaves[i].dir.y = x * -ry + y * rx;
        }
        
        /// <summary>
        /// Reinits a specific texture wave's params.
        /// </summary>
        /// <param name="i">index of the wave</param>
        void InitTexWave(int i)
        {
            float rads = (UnityEngine.Random.value * 2 - 1) * state.texState.angleDev;
            float dx = Mathf.Sin(rads);
            float dy = Mathf.Cos(rads);
            
            
            float tx = dx;
            dx = (-state.windDir.y) * dx - (-state.windDir.x) * dy;
            dy = (-state.windDir.x) * tx + (-state.windDir.y) * dy;
            
            float maxLen = state.texState.maxLen * BumpTexSize / state.stateParams.rippleScale;
            float minLen = state.texState.minLen * BumpTexSize / state.stateParams.rippleScale;
            float len = i / (float)(NumTexWaves-1) * (maxLen - minLen) + minLen;
            
            float reps = BumpTexSize / len;
            
            dx *= reps;
            dy *= reps;
            dx = (int)((dx >= 0) ? dx + 0.5f : dx - 0.5f);
            dy = (int)((dy >= 0) ? dy + 0.5f : dy - 0.5f);
            
            // bug: if winddir, maxLen or minLen are too small, this causes /0 error, resulting in the normalmap
            // being completely wrong. Force dx & dy to be != 0
            
            // if (dx >= 0 && dx < 1)
                // dx = 1;
            // if (dx < 0 && dx > -1)
                // dx = -1;
            // if (dy >= 0 && dy < 1)
                // dy = 1;
            // if (dy < 0 && dy > -1)
                // dy = -1;
            
            texWaves[i].rotScale.x = dx;
            texWaves[i].rotScale.y = dy;
            
            float effK = 1 / Mathf.Sqrt(dx * dx + dy * dy);
            texWaves[i].len = BumpTexSize * effK;
            texWaves[i].freq = Mathf.PI * 2 / texWaves[i].len;
            texWaves[i].amp = texWaves[i].len * state.texState.ampOverlen;
            texWaves[i].phase = UnityEngine.Random.value;
            
            texWaves[i].dir.x = dx * effK;
            texWaves[i].dir.y = dy * effK;
            
            texWaves[i].fade = 1;
            
            float speed = (1 / Mathf.Sqrt(texWaves[i].len / (2 * Mathf.PI * GravConst))) / 3;
            speed *= 1 + (UnityEngine.Random.value * 2 - 1) * state.texStateSpeedDeviation;
            texWaves[i].speed = speed;
        }
        
        /// <summary>
        /// Updates shader params for all geometric waves.
        /// </summary>
        /// <param name="dt">the elapsed time since last frame in seconds</param>
        void UpdateGeoWaves(float dt)
        {
            for (int i=0; i<NumGeoWaves; i++)
                UpdateGeoWave(i, dt);
        }

        /// <summary>
        /// Updates shader params for the specified geometric wave.
        /// </summary>
        /// <param name="i">wave id</param>
        /// <param name="dt">the elapsed time since last frame in seconds</param>
        void UpdateGeoWave(int i, float dt)
        {
            dt *= geometrySpeedMult;
            if (i == state.geoState.transIdx)
            {
                geoWaves[i].fade += state.geoState.transDel * dt;
                if (geoWaves[i].fade < 0)
                {
                    // This wave is faded out. Re-init and fade it back up.
                    InitGeoWave(i);
                    geoWaves[i].fade = 0;
                    state.geoState.transDel = -state.geoState.transDel;
                }
                else if (geoWaves[i].fade > 1)
                {
                    // This wave is faded back up. Start fading another down.
                    geoWaves[i].fade = 1;
                    state.geoState.transDel = -state.geoState.transDel;
                    state.geoState.transIdx++;
                    if (state.geoState.transIdx >= NumGeoWaves)
                        state.geoState.transIdx = 0;
                }
            }
            
            float speed = 1 / Mathf.Sqrt(geoWaves[i].len / (2 * Mathf.PI * GravConst));
            
            geoWaves[i].phase += speed * dt;
            geoWaves[i].phase = geoWaves[i].phase % (2*Mathf.PI);
            
            geoWaves[i].amp = geoWaves[i].len * state.geoState.ampOverlen / NumGeoWaves * geoWaves[i].fade;
        }

        /// <summary>
        /// Updates shader params for all texture waves.
        /// </summary>
        /// <param name="dt">the elapsed time since last frame in seconds</param>
        void UpdateTexWaves(float dt)
        {
            int i;
            for (i=0; i<NumTexWaves; i++)
                UpdateTexWave(i, dt);
        }

        /// <summary>
        /// Updates shader params for the specified texture wave.
        /// </summary>
        /// <param name="i">wave id</param>
        /// <param name="dt">the elapsed time since last frame in seconds</param>
        void UpdateTexWave(int i, float dt)
        {
            dt *= textureSpeedMult;
            if (i == state.texState.transIdx)
            {
                texWaves[i].fade += state.texState.transDel * dt;
                if (texWaves[i].fade < 0)
                {
                    // This wave is faded out. Re-init and fade it back up.
                    InitTexWave(i);
                    texWaves[i].fade = 0;
                    state.texState.transDel = -state.texState.transDel;
                }
                else if (texWaves[i].fade > 1)
                {
                    // This wave is faded back up. Start fading another down.
                    texWaves[i].fade = 1;
                    state.texState.transDel = -state.texState.transDel;
                    state.texState.transIdx++;
                    if (state.texState.transIdx >= NumTexWaves)
                        state.texState.transIdx = 0;
                }
            }
            texWaves[i].phase -= dt * texWaves[i].speed;
            texWaves[i].phase -= (int)(texWaves[i].phase);
        }

        /// <summary>
        /// Transmits the current shader params to the texture bumpmap generator.
        /// </summary>
        /// <param name="dt">the elapsed time since last frame in seconds</param>
        void RenderTexture(float dt)
        {
            int i;
            for (i=0; i<NumTexWaves; i++)
            {
                Vector4 UTrans = new Vector4(texWaves[i].rotScale.x, texWaves[i].rotScale.y, 0, texWaves[i].phase);
                waveComputeMaterial.SetVector(matCCParams.cUTrans[i], UTrans);
                
                float normScale = texWaves[i].fade / (float)NumBumpPasses;
                Vector4 Coef = new Vector4(texWaves[i].dir.x * normScale, texWaves[i].dir.y * normScale, 1, 1);
                waveComputeMaterial.SetVector(matCCParams.cCoef[i], Coef);
            }
            
            Vector4 xform;
            
            float kRate = 0.1f;
            xform = waveComputeMaterial.GetVector(matCCParams.cNoiseXForm0_00);
            xform.w += dt * kRate;
            waveComputeMaterial.SetVector(matCCParams.cNoiseXForm0_00, xform);
            
            xform = waveComputeMaterial.GetVector(matCCParams.cNoiseXForm1_10);
            xform.w += dt * kRate;
            waveComputeMaterial.SetVector(matCCParams.cNoiseXForm1_10, xform);
            
            float s = .5f / (NumBumpPerPass + state.specVec.x);
            Vector4 reScale = new Vector4(s, s, 1, 1);
            waveComputeMaterial.SetVector(matCCParams.cReScale, reScale);
            
            float scaleBias = 0.5f * state.specVec.x / (NumBumpPasses + state.specVec.x);
            Vector4 scaleBiasVec = new Vector4(scaleBias, scaleBias, 0, 1);
            waveComputeMaterial.SetVector(matCCParams.cScaleBias, scaleBiasVec);
            
            waveComputeMaterial.SetTexture(matCCParams.tCosineLUT, cosineLUT);
            waveComputeMaterial.SetTexture(matCCParams.tBiasNoise, biasNoiseMap);
        }
        
        // ↓ All kind of methods ported from the GLSL code
        // (I wish Visual Studio would stop trying to make those Pascal case...)

        float frac(float s)
        { return (s > 0) ? s - (int)s : 1 + s - (int)s; }
        Vector4 mult(Vector4 a, Vector4 b)
        { return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w); }
        Vector4 frac(Vector4 a)
        { return new Vector4(frac(a.x), frac(a.y), frac(a.z), frac(a.w)); }
        Vector4 add(Vector4 a, float s)
        { return new Vector4(a.x + s, a.y + s, a.z + s, a.w + s); }
        Vector4 xxxx(Vector4 a)
        { return new Vector4(a.x, a.x, a.x, a.x); }
        Vector4 yyyy(Vector4 a)
        { return new Vector4(a.y, a.y, a.y, a.y); }
        Vector4 zzzz(Vector4 a)
        { return new Vector4(a.z, a.z, a.z, a.z); }
        Vector4 wwww(Vector4 a)
        { return new Vector4(a.w, a.w, a.w, a.w); }
        Vector4 max(Vector4 a, float s)
        { return new Vector4(
            Mathf.Max(a.x, s),
            Mathf.Max(a.y, s),
            Mathf.Max(a.z, s),
            Mathf.Max(a.w, s)
        ); }
        Vector4 min(Vector4 a, float s)
        { return new Vector4(
            Mathf.Min(a.x, s),
            Mathf.Min(a.y, s),
            Mathf.Min(a.z, s),
            Mathf.Min(a.w, s)
        ); }
        float sum(Vector4 a)
        { return a.x + a.y + a.z + a.w; }

        /// <summary>
        /// Returns the new position of a specified point (in world space). Useful for floaters.
        /// This is obviously a clone of the SL code, so using it for many meshes might not be that much of a good idea.
        /// </summary>
        /// <param name="point">the point to displace in world space</param>
        /// <returns>the displaced point in world space</returns>
        public Vector3 DisplacePoint(Vector3 point)
        {
            float vertAlpha = 1; // vertex color alpha, but we don't have that.
            
            Vector4 wPosFeet = new Vector4(point.x, point.z, point.y) / .3048f;
            Vector4 dirX = new Vector4(geoWaves[0].dir.x, geoWaves[1].dir.x, geoWaves[2].dir.x, geoWaves[3].dir.x);
            Vector4 dirY = new Vector4(geoWaves[0].dir.y, geoWaves[1].dir.y, geoWaves[2].dir.y, geoWaves[3].dir.y);
            Vector4 freq = new Vector4(geoWaves[0].freq, geoWaves[1].freq, geoWaves[2].freq, geoWaves[3].freq);
            Vector4 phase = new Vector4(geoWaves[0].phase, geoWaves[1].phase, geoWaves[2].phase, geoWaves[3].phase);
            Vector4 lengths = new Vector4(geoWaves[0].len, geoWaves[1].len, geoWaves[2].len, geoWaves[3].len);
            Vector4 amp = new Vector4(geoWaves[0].amp, geoWaves[1].amp, geoWaves[2].amp, geoWaves[3].amp);
            Vector4 depthOffset = new Vector4(state.stateParams.waterHeight + 1,
                state.stateParams.waterHeight + 1,
                state.stateParams.waterHeight,
                state.stateParams.waterHeight);
            Vector4 depthScale = new Vector4(.5f, .5f, .5f, 1); // TODO: is it mapped in Plasma ? (warning: if so, invert y and z)
            float K = 5;
            if (state.geoState.ampOverlen > state.geoState.chop / (2 * Mathf.PI * NumGeoWaves * K))
                K = state.geoState.chop / (2*Mathf.PI * state.geoState.ampOverlen * NumGeoWaves);
            Vector4 dirXK = new Vector4(geoWaves[0].dir.x * K, 
                geoWaves[1].dir.x * K, 
                geoWaves[2].dir.x * K, 
                geoWaves[3].dir.x * K);
            Vector4 dirYK = new Vector4(geoWaves[0].dir.y * K, 
                geoWaves[1].dir.y * K, 
                geoWaves[2].dir.y * K, 
                geoWaves[3].dir.y * K);
            
            Vector4 dFilter = add(depthOffset, -wPosFeet.z);
            dFilter = mult(dFilter, depthScale);
            dFilter = max(dFilter, 0);
            dFilter = min(dFilter, 1);

            // CALCSINCOS -------------------------
            // Dot x and y with direction vectors
            Vector4 dists = mult(dirX, new Vector4(wPosFeet.x, wPosFeet.x, wPosFeet.x, wPosFeet.x));
            dists = mult(dirY, new Vector4(wPosFeet.y, wPosFeet.y, wPosFeet.y, wPosFeet.y)) + dists;
            
            // Scale in our frequency and add in our phase
            dists = mult(dists, freq);
            dists = dists + phase;

            const float kPi = Mathf.PI;
            const float kTwoPi = 2 * kPi;
            const float kOOTwoPi = 1 / kTwoPi;
            // Mod into range [-Pi..Pi]
            dists = add(dists, kPi);
            dists = dists * kOOTwoPi;
            dists = frac(dists);
            dists = dists * kTwoPi;
            dists = add(dists, -kPi);

            Vector4 dists2 = mult(dists, dists);
            Vector4 dists3 = mult(dists2, dists);
            Vector4 dists4 = mult(dists2, dists2);
            Vector4 dists5 = mult(dists3, dists2);
            Vector4 dists6 = mult(dists3, dists3);
            Vector4 dists7 = mult(dists4, dists3);

            Vector4 kSinConsts = new Vector4(1, -1/6.0f, 1/120.0f, -1/5040.0f);
            Vector4 kCosConsts = new Vector4(1, -1/2.0f, 1/24.0f, -1/720.0f);
            Vector4 sines = dists + dists3 * kSinConsts.y + dists5 * kSinConsts.z + dists7 * kSinConsts.w;
            Vector4 cosines = xxxx(kCosConsts) + mult(dists2, yyyy(kCosConsts)) + mult(dists4, zzzz(kCosConsts)) + mult(dists6, wwww(kCosConsts));

            Vector4 filteredAmp = lengths * vertAlpha;
            filteredAmp = max(filteredAmp, 0);
            filteredAmp = min(filteredAmp, 1);
            filteredAmp = filteredAmp * dFilter.z;
            filteredAmp = mult(filteredAmp, amp);

            sines = mult(sines, filteredAmp);
            cosines = mult(cosines, filteredAmp) * vertAlpha;
            
            // CalcFinalPosition------------------------
            // Sum to a scalar
            float h = sum(sines) + depthOffset.w;

            // Clamp to never go beneath input height
            wPosFeet.z = Mathf.Max(wPosFeet.z, h);

            wPosFeet.x = wPosFeet.x + Vector4.Dot(cosines, dirXK);
            wPosFeet.y = wPosFeet.y + Vector4.Dot(cosines, dirYK);
            
            return new Vector3(wPosFeet.x, wPosFeet.z, wPosFeet.y) * .3048f;
        }

        /// <summary>
        /// Returns the new position, along with binormal, tangent and normal of a specified point (in world space). Useful for floaters.
        /// This is obviously a clone of the SL code, so using it for many meshes might not be that much of a good idea.
        /// </summary>
        /// <param name="point">the point to displace in world space</param>
        /// <returns>the new world pos, along with binormal, tangent and normal</returns>
        public Vector3[] DisplaceAndRotatePoint(Vector3 point)
        {
            float vertAlpha = 1; // vertex color alpha, but we don't have that.

            Vector4 wPosFeet = new Vector4(point.x, point.z, point.y) / .3048f;
            Vector4 freq = mat.GetVector(matWSParams.cFrequency);
            Vector4 phase = mat.GetVector(matWSParams.cPhase);
            Vector4 amp = mat.GetVector(matWSParams.cAmplitude);
            Vector4 dirX = mat.GetVector(matWSParams.cDirX);
            Vector4 dirY = mat.GetVector(matWSParams.cDirY);
            // Vector4 specAtten = mat.GetVector(matWSParams.cSpecAtten);
            // Vector4 envAdjust = mat.GetVector(matWSParams.cEnvAdjust);
            // Vector4 envTint = mat.GetVector(matWSParams.cEnvTint);
            Vector4 lengths = mat.GetVector(matWSParams.cLengths);
            Vector4 depthOffset = mat.GetVector(matWSParams.cDepthOffset);
            Vector4 depthScale = mat.GetVector(matWSParams.cDepthScale);
            Vector4 dirXK = mat.GetVector(matWSParams.cDirXK);
            Vector4 dirYK = mat.GetVector(matWSParams.cDirYK);
            Vector4 dirXW = mat.GetVector(matWSParams.cDirXW);
            Vector4 dirYW = mat.GetVector(matWSParams.cDirYW);
            Vector4 KW = mat.GetVector(matWSParams.cKW);
            Vector4 dirXSqKW = mat.GetVector(matWSParams.cDirXSqKW);
            Vector4 dirYSqKW = mat.GetVector(matWSParams.cDirYSqKW);
            Vector4 dirXDirYKW = mat.GetVector(matWSParams.cDirXDirYKW);
            
            Vector4 dFilter = add(depthOffset, -wPosFeet.z);
            dFilter = mult(dFilter, depthScale);
            dFilter = max(dFilter, 0);
            dFilter = min(dFilter, 1);
            
            // CALCSINCOS -------------------------
            // Dot x and y with direction vectors
            Vector4 dists = mult(dirX, new Vector4(wPosFeet.x, wPosFeet.x, wPosFeet.x, wPosFeet.x));
            dists = mult(dirY, new Vector4(wPosFeet.y, wPosFeet.y, wPosFeet.y, wPosFeet.y)) + dists;
            
            // Scale in our frequency and add in our phase
            dists = mult(dists, freq);
            dists = dists + phase;

            const float kPi = Mathf.PI;
            const float kTwoPi = 2 * kPi;
            const float kOOTwoPi = 1 / kTwoPi;
            // Mod into range [-Pi..Pi]
            dists = add(dists, kPi);
            dists = dists * kOOTwoPi;
            dists = frac(dists);
            dists = dists * kTwoPi;
            dists = add(dists, -kPi);

            Vector4 dists2 = mult(dists, dists);
            Vector4 dists3 = mult(dists2, dists);
            Vector4 dists4 = mult(dists2, dists2);
            Vector4 dists5 = mult(dists3, dists2);
            Vector4 dists6 = mult(dists3, dists3);
            Vector4 dists7 = mult(dists4, dists3);

            Vector4 kSinConsts = new Vector4(1, -1/6.0f, 1/120.0f, -1/5040.0f);
            Vector4 kCosConsts = new Vector4(1, -1/2.0f, 1/24.0f, -1/720.0f);
            Vector4 sines = dists + dists3 * kSinConsts.y + dists5 * kSinConsts.z + dists7 * kSinConsts.w;
            Vector4 cosines = xxxx(kCosConsts) + mult(dists2, yyyy(kCosConsts)) + mult(dists4, zzzz(kCosConsts)) + mult(dists6, wwww(kCosConsts));

            Vector4 filteredAmp = lengths * vertAlpha;
            filteredAmp = max(filteredAmp, 0);
            filteredAmp = min(filteredAmp, 1);
            filteredAmp = filteredAmp * dFilter.z;
            filteredAmp = mult(filteredAmp, amp);

            sines = mult(sines, filteredAmp);
            cosines = mult(cosines, filteredAmp) * vertAlpha;
            
            // CalcFinalPosition------------------------
            // Sum to a scalar
            float h = sum(sines) + depthOffset.w;

            // Clamp to never go beneath input height
            wPosFeet.z = Mathf.Max(wPosFeet.z, h);

            wPosFeet.x = wPosFeet.x + Vector4.Dot(cosines, dirXK);
            wPosFeet.y = wPosFeet.y + Vector4.Dot(cosines, dirYK);
            
            Vector3 bin = new Vector3();
            Vector3 nor = new Vector3();
            Vector3 tan = new Vector3();
            
            // CalcTangentBasis-------------------------
            bin.x = 1 + Vector4.Dot(sines, -dirXSqKW);
            tan.x = Vector4.Dot(sines, -dirXDirYKW);
            nor.x = Vector4.Dot(cosines, -dirXW);

            bin.y = Vector4.Dot(sines, -dirXDirYKW);
            tan.y = 1 + Vector4.Dot(sines, -dirYSqKW);
            nor.y = Vector4.Dot(cosines, -dirYW);

            bin.z = Vector4.Dot(cosines, dirXW);
            tan.z = Vector4.Dot(cosines, dirYW);
            nor.z = (1 + Vector4.Dot(sines, -KW));
            
            return new Vector3[] {
                new Vector3(wPosFeet.x, wPosFeet.z, wPosFeet.y) * .3048f,
                new Vector3(bin.x, bin.z, bin.y),
                new Vector3(tan.x, tan.z, tan.y),
                new Vector3(nor.x, nor.z, nor.y)
            };
        }
    }
}
