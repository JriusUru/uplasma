/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// (A panic-link region isn't exactly a PlObjectInVolumeDetector, but whatever.)
    /// </summary>
    public class PlPanicLinkRegion : PlObjectInVolumeDetector
    {
        public bool playLinkOutAnim;
        
        protected override void OnTriggerEnter(Collider col)
        {
            base.OnTriggerEnter(col);
            
            if (col.gameObject.tag == "Player")
            {
                // if (collisionType == CollisionType.kTypeEnter || collisionType == CollisionType.kTypeAny)
                    GameManager.Instance.SpawnToPoint("LinkInPointDefault");
            }
        }
        
        // protected override void OnTriggerExit(Collider col)
        // {
            // base.OnTriggerExit(col);
            
            // if (col.gameObject.tag == "Player")
            // {
                // if (collisionType == CollisionType.kTypeExit || collisionType == CollisionType.kTypeAny)
                    // GlobalGameManager.instance.SpawnToPoint("LinkInPointDefault");
            // }
        // }
        
        protected override void OnCollisionEnter(Collision col)
        {
            base.OnCollisionEnter(col);
            
            if (col.gameObject.tag == "Player")
            {
                // if (collisionType == CollisionType.kTypeEnter || collisionType == CollisionType.kTypeAny || collisionType == collisionType.kTypeBump)
                    GameManager.Instance.SpawnToPoint("LinkInPointDefault");
            }
        }
        
        // protected override void OnCollisionExit(Collision col)
        // {
            // base.OnCollisionExit(col);
            
            // if (col.gameObject.tag == "Player")
            // {
                // if (collisionType == CollisionType.kTypeExit || collisionType == CollisionType.kTypeAny)
                    // GlobalGameManager.instance.SpawnToPoint("LinkInPointDefault");
            // }
        // }
    }
}
