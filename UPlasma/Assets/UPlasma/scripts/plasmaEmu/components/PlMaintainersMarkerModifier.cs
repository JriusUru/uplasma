/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Plasma's Maintainer marker GPS system.
    /// Note that this can also be used to create any coordinate system in fan-Ages (provided you don't mind a bit
    /// of experimenting with the object's rotation).
    /// 
    /// By default, this mimicks Plasma behavior: cylindrical coordinates with D'ni units (foot/16 length, 62500 units on circle).
    /// Plasma axis: GZ runs along positive Y (Z is up).
    /// Unity axis: GZ runs along positive Z (Y is up).
    /// </summary>
    public class PlMaintainersMarkerModifier : MonoBehaviour
    {
        public enum CalibrationLevel
        {
            Broken,
            Repaired,
            Calibrated
        };
        public CalibrationLevel calibrationLevel = CalibrationLevel.Calibrated;
        public int CalibrationLevelInt
        {
            get => (int)calibrationLevel;
            set => calibrationLevel = (CalibrationLevel)value;
        }

        public enum CoordinateType
        {
            CarthesianPos,
            CarthesianPosRot,
            // CarthesianPosRotScale,
            CylindricalPos,
            CylindricalPosRot,
            // cylindricalPosRotScale,
        }
        public CoordinateType coordinateType = CoordinateType.CylindricalPosRot; // Plasma's default
        public enum CoordinateUnitScale {
            meter,  // default unity scale
            foot,   // meter * .3048
            dni,    // foot / 16
            custom  // customUnitScale
        }
        public CoordinateUnitScale coordinateUnitScale = CoordinateUnitScale.dni;
        public float customUnitScale = 1;
        public enum CoordinateAngleScale {
            Radians,
            Degrees,
            Dni,    // degrees * 173.61
            Custom, // customAngleScale
        }
        public CoordinateAngleScale coordinateAngleScale = CoordinateAngleScale.Dni;
        public float customAngleScale = 1;
        
        public static PlMaintainersMarkerModifier Instance { get; protected set; }
        
        void Start()
        {
            if (Instance != null)
            {
                if (Instance.calibrationLevel <= calibrationLevel)
                {
                    Debug.LogWarning("Two PlMaintainersMarkerModifier currently loaded, replacing active one");
                    Instance = this;
                }
                else
                    Debug.LogWarning("Two PlMaintainersMarkerModifier currently loaded, not replacing active one as its calibration level is higher than the current one.");
            }
            else
                Instance = this;
        }
        
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }

        /// <summary>
        /// Standard coordinates, relative to position and rotation but not scale.
        /// </summary>
        /// <param name="point"></param>
        /// <returns>right-up-view</returns>
        public Vector3 GetCartesianCoordinates(Vector3 point)
        {
            var w2l = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one).inverse;
            Vector3 coord = w2l.MultiplyPoint3x4(point);
            coord.x = ScaleMeterLength(coord.x);
            coord.y = ScaleMeterLength(coord.y);
            coord.z = ScaleMeterLength(coord.z);
            return coord;
        }

        /// <summary>
        /// Standard coordinates, relative to position but not rotation nor scale.
        /// </summary>
        /// <param name="point"></param>
        /// <returns>right-up-view</returns>
        public Vector3 GetCartesianWorldCoordinates(Vector3 point)
        {
            Vector3 coord = point - transform.position;
            coord.x = ScaleMeterLength(coord.x);
            coord.y = ScaleMeterLength(coord.y);
            coord.z = ScaleMeterLength(coord.z);
            return coord;
        }

        /// <summary>
        /// Cylindrical (KI) coordinates, relative to position and rotation but not scale.
        /// </summary>
        /// <param name="point"></param>
        /// <returns>angle (deg) - distance - elevation</returns>
        public Vector3 GetCylindricalCoordinates(Vector3 point)
        {
            // first, get coordinates relative to object transform
            point = GetCartesianCoordinates(point);
            // now compute cylindrical coordinates
            point = CarthesianToCylindrical(point);
            point.x = ScaleRadianAngle(point.x);
            return point;
        }

        /// <summary>
        /// Cylindrical (KI) coordinates, relative to position but not rotation nor scale.
        /// </summary>
        /// <param name="point"></param>
        /// <returns>angle (deg) - distance - elevation</returns>
        public Vector3 GetCylindricalWorldCoordinates(Vector3 point)
        {
            // first, get coordinates relative to object transform
            point = GetCartesianWorldCoordinates(point);
            // now compute cylindrical coordinates
            point = CarthesianToCylindrical(point);
            point.x = ScaleRadianAngle(point.x);
            return point;
        }
        
        public static Vector3 CarthesianToCylindrical(Vector3 point)
        {
            // angle (rad) - distance - elevation = theta, radius, height
            float radius = Mathf.Sqrt(point.x * point.x + point.z * point.z);
            float theta = Mathf.Atan2(point.z, point.x);
            float height = point.y; // duh
            
            return new Vector3(theta, radius, height);
        }

        /// <summary>
        /// Converts the length/pos from meter to whatever we want.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        float ScaleMeterLength(float value)
        {
            switch (coordinateUnitScale)
            {
                case CoordinateUnitScale.meter:
                    return value;
                case CoordinateUnitScale.foot:
                    return value / .3048f;
                case CoordinateUnitScale.dni:
                default:
                    return value / .3048f / 16;
                case CoordinateUnitScale.custom:
                    return value * customUnitScale;
            }
        }

        /// <summary>
        /// Converts the angle from radians to whatever we want.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        float ScaleRadianAngle(float value)
        {
            // by default rotation is computed from right vector.
            // add 90 degrees to have it computed from view vector.
            value -= Mathf.PI / 2;
            
            // map it to range [0..2PI]
            float twoPi = 2*Mathf.PI;
            value = (((value % twoPi) + twoPi) % twoPi);
            
            switch (coordinateAngleScale)
            {
                case CoordinateAngleScale.Radians:
                    return value;
                case CoordinateAngleScale.Degrees:
                    return 360 - value / Mathf.PI * 180;
                case CoordinateAngleScale.Dni:
                default:
                    return (360 - value / Mathf.PI * 180) * 173.61f;
                case CoordinateAngleScale.Custom:
                    return value * customAngleScale;
            }
        }
        
        public Vector3 GetCoordinate(Vector3 point)
        {
            if (calibrationLevel == CalibrationLevel.Broken)
            {
                // marker broken, gives off random readings
                switch (coordinateType)
                {
                    case CoordinateType.CarthesianPos:
                    case CoordinateType.CarthesianPosRot:
                        return new Vector3(Random.Range(1, 999), Random.Range(1, 999), Random.Range(1, 999));
                    // case CoordinateType.carthesianPosRotScale:
                    case CoordinateType.CylindricalPos:
                    case CoordinateType.CylindricalPosRot:
                        return new Vector3(Random.Range(1, 62500), Random.Range(1, 999), Random.Range(1, 999));
                    // case CoordinateType.cylindricalPosRotScale:
                }
                return new Vector3(Random.Range(1, 62500), Random.Range(1, 999), Random.Range(1, 999));
            }
            else if (calibrationLevel == CalibrationLevel.Calibrated)
            {
                // marker ok and calibrated - returns actual coordinates
                switch (coordinateType)
                {
                    case CoordinateType.CarthesianPos:
                        return GetCartesianWorldCoordinates(point);
                    case CoordinateType.CarthesianPosRot:
                        return GetCartesianCoordinates(point);
                    // case CoordinateType.carthesianPosRotScale:
                    case CoordinateType.CylindricalPos:
                        return GetCylindricalWorldCoordinates(point);
                    case CoordinateType.CylindricalPosRot:
                        return GetCylindricalCoordinates(point);
                    // case CoordinateType.cylindricalPosRotScale:
                }
                return GetCylindricalCoordinates(point);
            }
            else // if (calibrationLevel == CalibrationLevel.repaired)
            {
                // marker ok but not calibrated - will always return 0
                return new Vector3(0, 0, 0);
            }
        }
    }
}
