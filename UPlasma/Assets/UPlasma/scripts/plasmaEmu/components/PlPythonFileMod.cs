/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;

#pragma warning disable 162 // unreachable code detected (but accessible in Python)
namespace UPlasma.PlEmu
{
    /// <summary>
    /// Loader for Plasma-made Python scripts.
    /// This loads scripts into a new PythonScope, the actual Python engine is initialized
    /// in the Python manager.
    /// </summary>
    public class PlPythonFileMod : MonoBehaviour, IPlMessageable
    {
        /*
        NOTE: Python methods called by the API:
        taken from HSP:
        FirstUpdate, Update, Notify, AtTimer,
        OnKeyEvent, Load, Save, GUINotify,
        PageLoad, ClothingUpdate, KIMsg, MemberUpdate,
        RemoteAvatarInfo, RTChat, VaultEvent, AvatarPage,
        SDLNotify, OwnershipNotify, AgeVaultEvent, Init,
        OnCCRMsg, OnServerInitComplete, OnVaultNotify,
        OnDefaultKeyCaught, OnMarkerMsg, OnBackdoorMsg,
        OnBehaviorNotify, OnLOSNotify, OnBeginAgeLoad,
        OnMovieEvent, OnScreenCaptureDone, OnClimbingWallEvent,
        OnClimbingWallInit, OnClimbBlockerEvent, OnAvatarSpawn,
        OnAccountUpdate, gotPublicAgeList, OnGameMgrMsg,
        OnGameCliMsg, lastone
        Note that we're *probably* not going to implement all of those... At least, not at first.
        */
        
        public string mainClassName; // note: only set AFTER awake()
        public List<object> parameters = new List<object>();
        public List<int> parameterIds = new List<int>();
        
        ScriptScope scope;
        object pythonClass;
        
        void Start()
        {
            // we have the name of the script, and it's loaded somewhere in the PlamzaManager memory...
            LoadScript();
            
            // compiling done, call required methods.
            TryCallMethod("OnInit");
            TryCallMethod("OnFirstUpdate");
            
            // FIXME: not really, but whatever
            TryCallMethod("OnServerInitComplete");
        }

#if DISABLE_PYTHON
        void LoadScript()
        {
            // retrieve the script so it's extracted (test)
            PythonManager.instance.loadScript(mainClassName);
            // /TMP
        }
        public dynamic CallMethod(string method, params dynamic[] arguments)
        {
            return null;
        }
        public bool TryCallMethod(string method, params dynamic[] arguments)
        {
            return false;
        }
        
        public bool TryCallMethod(string method, out dynamic result, params dynamic[] arguments)
        {
            return false;
        }
        
        void CompileSourceAndExecute(string code)
        {
        }
#else

        void LoadScript()
        {
            if (string.IsNullOrEmpty(mainClassName))
                throw new ArgumentTypeException("PFM: no main class name ?");
            // create scope
            scope = PythonManager.Instance.CreateScope();
            // set the current PFM to this one (used by ptAttrib to retrieve the objects)
            PythonManager.Instance.PushActivePythonScript(this);
            // import class and create instance
            // print("Python: loading " + mainClassName);
            CompileSourceAndExecute("import sys"); // useful for traceback
            // loading the script using "from x import x" allows the script importer to cache
            // the code into sys.modules, thus reducing parse time.
            CompileSourceAndExecute("from " + mainClassName + " import " + mainClassName);

            PythonManager.Instance.CallWrapped(() =>
            {
                // retrieve the instance and store it in a c# dyn variable
                dynamic mainClassType = scope.GetVariable(mainClassName);
                pythonClass = PythonManager.Instance.Engine.Operations.CreateInstance(mainClassType);
                PythonManager.Instance.Engine.Operations.SetMember(pythonClass, "key", this);
            });
            PythonManager.Instance.PopActivePythonScript();

            // NOTE: it's worth noting we don't care about Python glue - it doesn't do anything as long as we don't
            // use it, and we have no need of it.
        }
        
        /// <summary>
        /// Call the specified method of the associated script, passing the specified arguments.
        /// </summary>
        /// <param name="method">name of the method to call</param>
        /// <param name="arguments">arguments to give the method</param>
        /// <returns>whatever the method returns</returns>
        public dynamic CallMethod(string method, params dynamic[] arguments)
        {
            if (arguments == null)
                arguments = new dynamic[0];
            PythonManager.Instance.PushActivePythonScript(this);
            dynamic result = PythonManager.Instance.CallWrapped<dynamic>(() =>
            {
                return PythonManager.Instance.Engine.Operations.InvokeMember(pythonClass, method, arguments);
            });
            PythonManager.Instance.PopActivePythonScript();
            return result;
        }

        /// <summary>
        /// Attempts to call the specified method, if it exists.
        /// </summary>
        /// <param name="method">name of the method to call</param>
        /// <param name="arguments">arguments to give the method</param>
        /// <returns>true if the method exists and was called, false otherwise</returns>
        public bool TryCallMethod(string method, params dynamic[] arguments)
        {
            dynamic dontCare;
            return TryCallMethod(method, out dontCare, arguments);
        }

        /// <summary>
        /// Attempts to call the specified method, if it exists.
        /// </summary>
        /// <param name="method">name of the method to call</param>
        /// <param name="result">whatever the method returns</param>
        /// <param name="arguments">arguments to give the method</param>
        /// <returns>true if the method exists and was called, false otherwise</returns>
        public bool TryCallMethod(string method, out dynamic result, params dynamic[] arguments)
        {
            if (arguments == null)
                arguments = new dynamic[0];
            if (PythonManager.Instance.Engine.Operations.ContainsMember(pythonClass, method))
            {
                PythonManager.Instance.PushActivePythonScript(this);
                dynamic tmpResult = default(dynamic);
                bool execResult = PythonManager.Instance.CallWrapped(() => {
                    tmpResult = PythonManager.Instance.Engine.Operations.InvokeMember(pythonClass, method, arguments);
                }, true);
                result = tmpResult;
                PythonManager.Instance.PopActivePythonScript();
                return execResult;
            }
            result = null;
            return false;
        }
        
        /// <summary>
        /// Compiles some Python source code and execute it.
        /// </summary>
        /// <param name="code">the Python code</param>
        void CompileSourceAndExecute(string code)
        {
            PythonManager.Instance.PushActivePythonScript(this);
            ScriptSource source = PythonManager.Instance.Engine.CreateScriptSourceFromString(code, SourceCodeKind.Statements);
            CompiledCode compiled = source.Compile();
            PythonManager.Instance.CallWrapped(() => {
                compiled.Execute(scope);
            });
            PythonManager.Instance.PopActivePythonScript();
        }

#endif

        public bool MsgReceive(PlMessage msg)
        {
            if (msg is PlNotifyMsg)
            {
                PlNotifyMsg nmsg = msg as PlNotifyMsg;
                float state = nmsg.notifyState;
                int id = -1; /*nmsg.notifyId*/ // TODO
                List<PlProEventData> events = nmsg.notifyEvents;
                List<dynamic> pyEvents = new List<dynamic>();
                foreach (PlProEventData _event in events)
                    pyEvents.Add(PythonManager.Instance.ModuleLoader.ConvertProEventDataToNotifyEvent(nmsg));
                TryCallMethod("OnNotify", state, id, pyEvents);
                return true;
            }
            return false;
        }

        public void OnSDLNotify(string VARname, string SDLname, uint playerID, string tag)
        {
            CallMethod("OnSDLNotify", new dynamic[] { VARname, SDLname, playerID, tag });
        }
    }
}
