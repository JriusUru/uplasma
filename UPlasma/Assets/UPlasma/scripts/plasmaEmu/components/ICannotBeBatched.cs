using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Simply defines a component which prevents static batching of an object.
    /// This should be used by any component which affects object transform or for which
    /// static batching might have undesirable side-effects.
    /// Also note: this will prevent static batching for all children.
    /// </summary>
    public interface ICannotBeBatched
    { }
}
