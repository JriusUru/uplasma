/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlViewFaceModifier : MonoBehaviour, ICannotBeBatched
    {
        // note: can't add the [Flags] attrib, since they aren't actually power-of-twos
        public enum Flags
        {
            PivotFace,
            PivotFavorY,
            PivotY,
            PivotTumple,
            Scale,

            FaceCam,
            FaceList,
            FacePlay,
            FaceObj,
            Offset,
            OffsetLocal,
            MaxBounds
        };
        public List<int> flags = new List<int>();

        // ↓ no longer required, Unity takes care of rebuilding the matrix itself when needed.
        // public Vector3 scale = new Vector3(1,1,1);
        // public Vector3 offset;
        // public Matrix4x4 l2p;
        
        Vector3 zToYRotation = new Vector3(-90, 0, 0);
        bool onlyY;
        Quaternion baseRot;
        Quaternion baseLocalRot;

        void Start()
        {
            onlyY = flags.Contains((int)Flags.PivotY);
            baseRot = transform.rotation;
            baseLocalRot = transform.localRotation;
        }
        
        void OnWillRenderObject()
        {
            // standard view face: pivot face + face cam (+ max bounds, but don't care about that one)
            // special case: uses pivot y instead.
            if (onlyY)
            {
                Vector3 objUp = baseRot * Vector3.forward;
                Vector3 objRight = baseRot * -Vector3.up;
                Vector3 camDir = Camera.current.transform.position - transform.position;
                camDir = Vector3.ProjectOnPlane(camDir, objUp);
                float newAngle = Vector3.SignedAngle(camDir, objRight, objUp);
                transform.localRotation = baseLocalRot * Quaternion.Euler(0, 0, -newAngle);
            }
            else
            {
                transform.LookAt(Camera.current.transform.position);
                transform.Rotate(zToYRotation); // Plamza lookat is on axis y, unity is on z
            }
        }
    }
}
