/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Unity animation can't animate meshrenderer shared material. Animating this class's properties will
    /// redirect it to the specified material.
    /// This object should be put on a child of the first object to use the animated material (so that Animation
    /// will not conflict with any PlAGMasterMod).
    /// </summary>
    [RequireComponent(typeof(Animation))]
    public class PlLayerAnimation : MonoBehaviour, IPlMessageable
    {
        public AnimationClip clip;
        public Material material;
        
        public bool preshadeColorAnimated = false;
        public bool runtimeColorAnimated = false;
        public bool ambientColorAnimated = false;
        public bool specularColorAnimated = false;
        public bool opacityAnimated = false;
        public bool uvwTransformAnimated = false;
        public int animationPropIndex = 0; // which layer to animate. Useful for animating custom shaders using multiple layers.
        
        public Animation animPlayer;
        
        // animated properties
        public Color preshadeColor;
        public Color runtimeColor;
        public Color ambientColor;
        public Color specularColor;
        public float opacity;
        public Matrix4x4 uvwTransform;
        
        string preshadeStr;
        string runtimeStr;
        string ambientStr;
        string specularStr;
        string opacStr;
        string uvwStr;
        
        void Start()
        {
            string suffix = "";
            if (animationPropIndex > 0)
                suffix += (animationPropIndex+1);
            
            preshadeStr = "_Color" + suffix;
            runtimeStr = "_Runtime" + suffix;
            ambientStr = "_Amb" + suffix;
            specularStr = "_Spec" + suffix;
            opacStr = "_Opac" + suffix;
            uvwStr = "_Matrix" + suffix;
        }
        
        void Update()
        {
            if (preshadeColorAnimated)
                material.SetColor(preshadeStr, preshadeColor);
            if (runtimeColorAnimated)
                material.SetColor(runtimeStr, preshadeColor);
            if (ambientColorAnimated)
                material.SetColor(ambientStr, ambientColor);
            if (specularColorAnimated)
                material.SetColor(specularStr, specularColor);
            if (opacityAnimated)
                material.SetFloat(opacStr, opacity);
            if (uvwTransformAnimated)
                material.SetMatrix(uvwStr, uvwTransform);
        }
        
        #pragma warning disable 162 // unreachable code detected
        public bool MsgReceive(PlMessage msg)
        {
            // return until we have written the corresponding native code
            return false;
            
            if (msg is PlAnimCmdMsg)
            {
                PlAnimCmdMsg amsg = (PlAnimCmdMsg)msg;
                foreach (int command in amsg.commands)
                {
                    PlAnimCmdMsg.ModCmds ecommand = (PlAnimCmdMsg.ModCmds)command;
                    switch (ecommand)
                    {
                        case PlAnimCmdMsg.ModCmds.Continue:
                            animPlayer.clip = clip;
                            animPlayer.Play();
                            break;
                        case PlAnimCmdMsg.ModCmds.AddCallbacks:
                            StartCoroutine(CallbackCo(amsg.callbacks));
                            break;
                        default:
                            print("Unknown command " + ecommand);
                            break;
                    }
                }
                return true;
            }
            return false;
        }
        #pragma warning restore 162
        
        IEnumerator CallbackCo(List<PlMessage> callbacks)
        {
            foreach (PlEventCallbackMsg cMsg in callbacks)
            {
                if (cMsg.eventId == CallbackEvent.Time)
                    yield return new WaitForSeconds(cMsg.eventTime);
                else
                    Debug.LogError("Unimplemented callback event " + cMsg.eventId + " !");
                cMsg.Send();
            }
        }
    }
}
