/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;
using UnityEngine.Networking;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlFadeParams
    {
        public enum Type
        {
            Linear,
            Logarithmic,
            Exponential
        };

        public float lengthInSecs;
        public float volStart;
        public float volEnd;
        public Type type;
        public int TypeInt
        {
            get => (int)type;
            set => type = (Type)value;
        }
        public bool stopWhenDone;
        public bool fadeSoftVol;
        public float currTime;
    }

    [Serializable]
    public class PlEAXSourceSoftSettings
    {
        public short occlusion;
        public float occlusionLFRatio;
        public float occlusionRoomRatio;
        public float occlusionDirectRatio;
    }

    [Serializable]
    public class PlEAXSourceSettings
    {
        public short room;
        public short roomHF;
        public bool enabled;
        public bool roomAuto;
        public bool roomHFAuto;
        public short outsideVolHF;
        public float airAbsorptionFactor;
        public float roomRolloffFactor;
        public float dopplerFactor;
        public float rolloffFactor;
        public PlEAXSourceSoftSettings softStarts;
        public PlEAXSourceSoftSettings softEnds;
        public float occlusionSoftValue;
    }

    [Serializable]
    public class PlWAVHeader
    {
        public int formatTag;
        public int numChannels;
        public int numSamplesPerSec;
        public int avgBytesPerSec;
        public int blockAlign;
        public int bitsPerSample;
    }

    [Serializable]
    public class PlSoundBuffer
    {
        public PlWAVHeader header;
        public string fileName;
        public int dataLength;
        // public unsigned char* fData;
        public Flags flags;
        public int FlagsInt
        {
            get => (int)flags;
            set => flags = (Flags)value;
        }

        [Flags]
        public enum Flags
        {
            IsExternal = 0x1,
            AlwaysExternal = 0x2,
            OnlyLeftChannel = 0x4,
            OnlyRightChannel = 0x8,
            StreamCompressed = 0x10
        };
    }


    /// <summary>
    /// Plasma Sound on Unity.
    /// NOTES:
    /// Uru has a few differences when it comes to 3D sounds.
    /// Mainly, 3D sounds are often stereo. Now, OpenAL itself (used by both engines) doesn't support stereo 3D sound.
    /// Somehow Plasma manages to get two 3D mono sounds to a single stereo sound when close enough to the source.
    /// For some reason this does NOT always happen though.
    /// Also, sometimes two mono sounds in Plasma are merged into a single stereo sound, for on/off effects (which is very very evil).
    /// 
    /// The closest approximation I could find in Unity is set 3D sounds to pan either to left or right, then on update
    /// set the spatial blend to go from 3D to 2D between min and max range. Works perfectly for the Relto ambiance.
    /// Might not work for other things, will have to keep an ear out as we do more testing.
    /// 
    /// (Oh, and distance attenuation is the same in both engine: clamped inverse (logarithmic).)
    /// 
    /// BUG: Unity can't play same clip in two different audiosources if the clip was loaded at runtime.
    /// We'll use different audio clips. More memory will be used, but whatever for now.
    /// See unityCantPlayTwo2DSourcesIfTheyShareTheSameRuntimeLoadedClip
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class PlSound : MonoBehaviour
    {
        [Flags]
        public enum Property
        {
            PropIs3DSound = 0x1,
            PropDisableLOD = 0x2,
            PropLooping = 0x4,
            PropAutoStart = 0x8,
            PropLocalOnly = 0x10,
            PropLoadOnlyOnCall = 0x20,
            PropFullyDisabled = 0x40,
            PropDontFade = 0x80,
            PropIncidental = 0x100     // incidental are "really-unimportant-sounds". Usually they are limited to 4 simultaneously playing.
        };
        public enum Type
        {
            StartType = 0,
            SoundFX = StartType,
            Ambience,
            BackgroundMusic,
            GUISound,
            NPCVoices,
            NumTypes
        };
        public enum StreamType
        {
            NoStream,
            StreamFromRAM,
            StreamFromDisk,
            StreamCompressed
        };

        /// <summary>
        /// NOTE: actually never used by the engine !
        /// </summary>
        public bool channel;
        public Type type;
        public int TypeInt
        {
            get => (int)type;
            set => type = (Type)value;
        }
        public int priority;
        public bool playing;
        public double time;
        public int maxFalloff;
        public int minFalloff;
        public int outerVol;
        public int innerCone;
        public int outerCone;
        public float currVolume;
        public float desiredVol;
        public float fadedVolume;
        public Property properties;
        public int PropertiesInt
        {
            get => (int)properties;
            set => properties = (Property)value;
        }
        public PlEAXSourceSettings EAXSettings;
        public PlFadeParams fadeInParams;
        public PlFadeParams fadeOutParams;
        // public GameObject softRegion;
        // public GameObject softOcclusionRegion;
        public PlSoundBuffer dataBuffer;
        public string subtitleId;
        
        public AudioSource audioSource;
        
        bool is3D = false;
        string dictAudioKey = "";
        
        class AudioClipInfo
        {
            public AudioClip clip;
            public int numUsers;
            public bool loaded;
        }
        
        static AudioMixerGroup amgUnset;
        static AudioMixerGroup amgAmbiance;
        static AudioMixerGroup amgGUI;
        static AudioMixerGroup amgMusic;
        static AudioMixerGroup amgNPC;
        static AudioMixerGroup amgSFX;
        static AudioMixerGroup amgPlayerVoice;
        static Dictionary<string, AudioClipInfo> loadedAudioClips;
        static AudioListener listener;
        
        static void EnsureLoadedMixerGroups()
        {
            if (amgUnset == null)
                amgUnset = (AudioMixerGroup)Resources.Load("Unset", typeof(AudioMixerGroup));
            if (amgAmbiance == null)
                amgAmbiance = (AudioMixerGroup)Resources.Load("Ambiance", typeof(AudioMixerGroup));
            if (amgGUI == null)
                amgGUI = (AudioMixerGroup)Resources.Load("GUI", typeof(AudioMixerGroup));
            if (amgMusic == null)
                amgMusic = (AudioMixerGroup)Resources.Load("Music", typeof(AudioMixerGroup));
            if (amgNPC == null)
                amgNPC = (AudioMixerGroup)Resources.Load("NPCVoices", typeof(AudioMixerGroup));
            if (amgSFX == null)
                amgSFX = (AudioMixerGroup)Resources.Load("Sfx", typeof(AudioMixerGroup));
            if (amgPlayerVoice == null)
                amgPlayerVoice = (AudioMixerGroup)Resources.Load("PlayerVoices", typeof(AudioMixerGroup));
            
            if (loadedAudioClips == null)
                loadedAudioClips = new Dictionary<string, AudioClipInfo>();
        }
        
        void Awake()
        {
            EnsureLoadedMixerGroups();
        }
        
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.enabled = false; // disable until we've loaded the audio clip - this will force playOnAwake to do its job.
            SetupAudioSource();
        }
        
        void Update()
        {
            if (is3D && EnsureAudioListener())
            {
                // compute 2D/3D blend according to distance
                Vector3 listenerToSource = (transform.position - listener.transform.position);
                float distance = listenerToSource.magnitude;
                float factor;

                if (distance < audioSource.minDistance)
                {
                    // full stereo 2D
                    factor = 0;
                }
                else if (distance > audioSource.maxDistance)
                {
                    // full mono 3D
                    factor = 1;
                }
                else
                {
                    // compute factor. This is copied from OpenAL's clamped inverse attenuation, as that's what Uru
                    // seems to be using.
                    // rolloffFactor seems to be some kind of unit. In Plasma it was .3048 so I'm pretty sure in Unity it's 1.
                    float rolloffFactor = 1;
                    distance = Mathf.Max(distance, audioSource.minDistance);
                    distance = Mathf.Min(distance, audioSource.maxDistance);
                    factor = 1 - audioSource.minDistance / (audioSource.minDistance + rolloffFactor * (distance - audioSource.minDistance));
                }
                
                // audioSource.volume = currVolume; // TODO - use currVolume for fadein and fadeout
                audioSource.spatialBlend = factor;
            }
        }

        /// <summary>
        /// Updates the reference to the AudioListener. Returns whether it exists at all.
        /// </summary>
        /// <returns></returns>
        bool EnsureAudioListener()
        {
            if (listener == null)
            {
                // listener changed or wasn't found yet. Try to fetch it.
                listener = FindObjectOfType<AudioListener>();
                if (listener == null)
                    Debug.LogError("Couldn't find an AudioListener !");
            }
            
            return listener != null;
        }
        
        void SetupAudioSource()
        {
			audioSource.playOnAwake = properties.HasFlag(Property.PropAutoStart);
            audioSource.loop = properties.HasFlag(Property.PropLooping);
			audioSource.priority = 128 - priority; // TODO - while this is generally correct, indidental sounds use different priority range. FIXME.
			audioSource.volume = desiredVol;
			audioSource.spatialBlend = (properties.HasFlag(Property.PropIs3DSound)) ? 1 : 0;
            
            if (properties.HasFlag(Property.PropIs3DSound))
            {
                // 3D sound... check whether we need to set stereo panning
                if (dataBuffer.flags.HasFlag(PlSoundBuffer.Flags.OnlyLeftChannel))
                    audioSource.panStereo = -1;
                else if (dataBuffer.flags.HasFlag(PlSoundBuffer.Flags.OnlyRightChannel))
                    audioSource.panStereo = 1;
            }
            is3D = properties.HasFlag(Property.PropIs3DSound);
            
            // doppler: I don't think Plasma uses doppler outside of EAX regions... ?
            // FIXME: make sure of it. For now we'll disable doppler entirely since it's often over-the-top in Unity (AFAIK)
			// audioSource.dopplerLevel =
                    // ((properties.HasFlag(Property.kPropIs3DSound) &&
                    // type != Type.kBackgroundMusic) ? 1 : 0;
            audioSource.dopplerLevel = 0;
            
			audioSource.minDistance = Mathf.Max(minFalloff, 1f); // have to have at least that, otherwise sound fading might glitch out.
			audioSource.maxDistance = maxFalloff;
            SetType();
            
            // finally, load the sound
            StartCoroutine(LoadCo());
        }
        
        void SetType()
        {
            switch (type)
            {
                case Type.Ambience:
                    audioSource.outputAudioMixerGroup = amgAmbiance;
                    break;
                case Type.GUISound:
                    audioSource.outputAudioMixerGroup = amgGUI;
                    break;
                case Type.BackgroundMusic:
                    audioSource.outputAudioMixerGroup = amgMusic;
                    break;
                case Type.NPCVoices:
                    audioSource.outputAudioMixerGroup = amgNPC;
                    break;
                case Type.SoundFX:
                    audioSource.outputAudioMixerGroup = amgSFX;
                    break;
                default:
                    audioSource.outputAudioMixerGroup = amgUnset;
                    break;
            }
        }
        
        IEnumerator LoadCo()
        {
            dictAudioKey = dataBuffer.fileName;
            bool unityCantPlayTwo2DSourcesIfTheyShareTheSameRuntimeLoadedClip = true;
            if (unityCantPlayTwo2DSourcesIfTheyShareTheSameRuntimeLoadedClip)
                dictAudioKey += UnityEngine.Random.value;
            
            if (loadedAudioClips.ContainsKey(dictAudioKey))
            {
                loadedAudioClips[dictAudioKey].numUsers++;
                // we've got two PlSound loading the same sound at the same time... Wooops !
                // do NOT GetAudioClip on the object we just loaded (which means no new asset is created),
                // and instead use the clip that was already loaded.
                // (do timeout if the audio isn't done loading though...)
                while (!loadedAudioClips[dictAudioKey].loaded)
                    yield return null;
                audioSource.clip = loadedAudioClips[dictAudioKey].clip;
                audioSource.enabled = true;
            }
            else
            {
                // register the yet-to-be-loaded audioclip as being, uh, loading.
                AudioClipInfo info = new AudioClipInfo
                {
                    numUsers = 1,
                    loaded = false
                };

                loadedAudioClips[dictAudioKey] = info;

                // TODO - right now we load the WAV file if it exists - might be worth checking if it's required ? Else remove this comment.
                string url = GetSfxLocation(dataBuffer.fileName);
                if (string.IsNullOrEmpty(url))
                {
                    Debug.LogWarning("Couldn't file audio file " + dataBuffer.fileName);
                    yield break;
                }
                // NOTE: we need the file:// prefix for loading from files. However, WWW is a bit quirky, and can have issues loading files across
                // drives... Prefixing with three forward slashes solves the issue, but require an absolute filename (IIRC)
                string prefix = "file:///";

                using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(prefix + url,
                    url.ToLower().EndsWith(".ogg") ? AudioType.OGGVORBIS : AudioType.WAV))
                {
                    yield return www.SendWebRequest();

                    if (www.isNetworkError)
                    {
                        Debug.LogError(www.error);
                        yield break;
                    }
                    AudioClip clip = DownloadHandlerAudioClip.GetContent(www);
                    clip.name = Path.GetFileName(url);
                    audioSource.clip = clip;
                    audioSource.enabled = true;
                    // now tell other audio sources that the file is done loading, and can be played
                    info.clip = clip;
                    info.loaded = true;
                }
            }
        }
        
        string GetSfxLocation(string fileName)
        {
            string fileOggOrWav = Path.Combine("sfx", fileName);
            string fileWav = Path.Combine("sfx", "streamingCache", fileName.Substring(0, fileName.Length-4) + ".wav");

            // if it has a wav version, it's likely better to use that instead.
            if (PlGameFileAccessor.Instance.FileExists(fileWav))
                return PlGameFileAccessor.Instance.GetFile(fileWav);

            // Trivia time: although the main SFX folder usually contains exclusively ogg files,
            // it's OCCASIONALY possible for it to contain WAV files.
            // Throughout all the games, only CC has one - ercaScopeGUI-Sliders.wav.
            // Either it was a mistake, or it was because the file is so short that it wouldn't load
            // correctly as an OGG file. In any case, this won't do any difference to us
            // (I hope you enjoy how I waste your time on useless trivia like this :D Oh, and you're most welcome.)
            return PlGameFileAccessor.Instance.GetFile(fileOggOrWav);
        }
        
        public void Play()
        {
            audioSource.Play();
        }
        
        void OnDestroy()
        {
            loadedAudioClips[dictAudioKey].numUsers--;
            if (loadedAudioClips[dictAudioKey].numUsers == 0)
            {
                // audio no longer used, destroy it
                Destroy(loadedAudioClips[dictAudioKey].clip);
                loadedAudioClips.Remove(dictAudioKey);
            }
        }
    }
}
