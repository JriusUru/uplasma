using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlCameraModifier : MonoBehaviour, IPlMessageable
    {
        // FYI: PlCameraModifiers usually won't actually require creating a Camera component, because they are mostly
        // presets and rarely used for actual rendering. But since the need will probably arise at some point,
        // here is some code to generate one. You're welcome.
        //Camera Camera
        //{
        //    get
        //    {
        //        if (cam == null)
        //        {
        //            // same deal as light sources - they need to be rotated 90 degrees, hence the sub object
        //            GameObject camObj = new GameObject("Cam");
        //            camObj.transform.SetParent(transform, false);
        //            camObj.transform.localPosition = Vector3.zero;
        //            camObj.transform.localRotation = Quaternion.Euler(90, 0, 0);
        //            cam = camObj.AddComponent<Camera>();
        //        }
        //        return cam;
        //    }
        //}
        //Camera cam;

        public bool MsgReceive(PlMessage msg)
        {
            if (msg is PlCameraMsg camMsg)
            {
                throw new NotImplementedException();
            }

            return false;
        }
    }
}
