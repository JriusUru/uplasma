﻿/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A buoy object floating on a geometrically deformed PlWaveSet7.
    /// Currently rather barebone:
    /// - doesn't react to physics
    /// - can't drift (acts like it's anchored to the sea's floor)
    /// - not affected by buoyancy (yes, despite the name...)
    /// 
    /// Will probably improve it later...
    /// </summary>
    public class PlBuoy : MonoBehaviour
    {
        //[Tooltip("Whether the buoy can drift over time.")]
        //public bool canDrift = false;

        [Tooltip("Whether the buoy is updated when it is outside the screen. You may want to " +
            "disable this if you notice pop-in or if the buoy is set to drift")]
        public bool canCull = true;

        [Tooltip("Whether the buoy rotates with the wave's roll. Recommended for boats.")]
        public bool canRotate = true;
    }
}
