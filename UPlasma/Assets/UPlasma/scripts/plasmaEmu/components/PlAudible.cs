/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlAudible : MonoBehaviour, IPlMessageable
    {
        public PlSound[] sounds;
        
        public bool MsgReceive(PlMessage msg)
        {
            if (msg is PlSoundMsg smsg)
            {
                foreach (PlSoundMsg.ModCmds command in smsg.commands)
                {
                    switch (command)
                    {
                        case PlSoundMsg.ModCmds.Play:
                            sounds[smsg.index].Play();
                            break;
                        default:
                            print("Unknown command " + command);
                            break;
                    }
                }
            }
            return false;
        }
    }
}
