/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlFadeOpacityMod : MonoBehaviour
    {
        public enum FadeType
        {
            BoundsCenter = 0x1
        }
        public int flags;
        public float fadeUp;
        public float fadeDown;

        bool visible = false;
        Material material;
        int colId;
        int colIdAmb;

        void Start()
        {
            material = GetComponent<Renderer>().sharedMaterial;
            colId = Shader.PropertyToID("_Color");
            colIdAmb = Shader.PropertyToID("_Amb");
        }

        void Update()
        {
            Camera cam = GameManager.Instance.MainCamera;
            float dist = Vector3.Distance(transform.position, cam.transform.position);
            Ray ray = new Ray(cam.transform.position, transform.position - cam.transform.position);
            visible = !Physics.Raycast(ray, dist);

            Color col1 = material.GetColor(colId);
            Color col2 = material.GetColor(colIdAmb);
            if (visible)
            {
                col1.a += Time.deltaTime / Mathf.Max(fadeUp, .001f);
                col2.a += Time.deltaTime / Mathf.Max(fadeUp, .001f);
            }
            else
            {
                col1.a -= Time.deltaTime / Mathf.Max(fadeDown, .001f);
                col2.a -= Time.deltaTime / Mathf.Max(fadeDown, .001f);
            }
            col1.a = Mathf.Clamp01(col1.a);
            col2.a = Mathf.Clamp01(col1.a);
            material.SetColor(colId, col1);
            material.SetColor(colIdAmb, col2);
        }
    }
}
