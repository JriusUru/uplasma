/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlSittingModifier : MonoBehaviour, IPlMessageable
    {
        [Flags]
        public enum SittingFlags
        {
            ApproachFront = 0x1,
            ApproachLeft = 0x2,
            ApproachRight = 0x4,
            ApproachRear = 0x8,
            ApproachMask = ApproachFront | ApproachLeft |
                           ApproachRight | ApproachRear,
            kDisableForward = 0x10
        };
        
        public SittingFlags flags;
        public int FlagsInt
        {
            get => (int)flags;
            set => flags = (SittingFlags)value;
        }
        public List<IPlMessageable> notifies = new List<IPlMessageable>();
        
        public bool MsgReceive(PlMessage msg)
        {
            return false;
        }
    }
}
