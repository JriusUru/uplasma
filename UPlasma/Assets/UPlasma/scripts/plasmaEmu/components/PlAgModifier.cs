/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A modifier which allows identifying an animated object using a string key.
    /// A <see cref="PlAgMasterMod"/> then binds its applicators to such an object.
    /// (More or less - should work.)
    /// </summary>
    public class PlAgModifier : MonoBehaviour, ICannotBeBatched
    {
        // TODO

        //static Dictionary<string, GameObject> knownAgModifiers = new Dictionary<string, GameObject>();

        public string channelName;

        //void Awake()
        //{
        //    knownAgModifiers[channelName] = gameObject;
        //}

        //void OnDestroy()
        //{
        //    knownAgModifiers.Remove(channelName);
        //}
    }
}
