/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public struct PlLodDist
    {
        public float minDist;
        public float maxDist;
    }

    public class PlClusterGroup : MonoBehaviour, IPlMessageable
    {
        public Material material;
        public List<PlCluster> clusters = new List<PlCluster>();
        //public List<PlVisRegion> regions = new List<PlVisRegion>();
        //public List<Light> lights = new List<Light>(); // we can't limit lights, so we don't really care
        public PlLodDist lod;
        public int renderLevel; // probably useless to us, but who knows...

        public bool MsgReceive(PlMessage msg)
        {
            return false;
        }
    }
}
