/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlRandomSoundModGroup
    {
        public List<int> indices = new List<int>();
        public int groupedIdx;
    }
    
    public class PlRandomSoundMod : MonoBehaviour, IPlMessageable
    {
        [Flags]
        public enum RandomMode
        {
            Normal = 0,
            NoRepeats = 0x1,
            Coverall = 0x2,
            OneCycle = 0x4,
            OneCmd = 0x8,
            DelayFromEnd = 0x10,
            Sequential = 0x20
        };
        
        public int state;
        public RandomMode mode;
        public int ModeInt
        {
            get => (int)mode;
            set => mode = (RandomMode)value;
        }
        public float minDelay;
        public float maxDelay;
        public List<PlRandomSoundModGroup> groups = new List<PlRandomSoundModGroup>();
        
        public bool MsgReceive(PlMessage msg)
        {
            return false;
        }
    }
}
