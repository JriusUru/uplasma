/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlExcludeRegionModifier : MonoBehaviour, IPlMessageable
    {
        public bool seek;
        public float seekTime;
        public List<Transform> safePoints = new List<Transform>();
        
        List<GameObject> objsInRegion = new List<GameObject>();
        
        void OnTriggerEnter(Collider col)
        {
            if (col.GetComponent<Rigidbody>() != null || col.GetComponent<CharacterController>() != null)
                objsInRegion.Add(col.gameObject);
        }

        void OnTriggerExit(Collider col)
        {
            if (col.GetComponent<Rigidbody>() != null || col.GetComponent<CharacterController>() != null)
                objsInRegion.Remove(col.gameObject);
        }
        
        void Clear()
        {
            foreach (GameObject obj in objsInRegion)
            {
                // TODO - might be worth teleporting to closest point. But in truth, eh, whatever.
                // TODO - xrgns support avatar seek instead of teleport, but I've never seen it used
                // (it's worth noting that in Plasma teleporting also sets the avatar's rotation to that of the
                // safe point, but that sounds like a byproduct of setting the transform, so we won't replicate the behavior)
                obj.transform.position = safePoints[0].position;
            }
            objsInRegion.Clear();
            GetComponent<Collider>().isTrigger = false;
        }
        
        void Release()
        {
            GetComponent<Collider>().isTrigger = true;
        }
        
        public bool MsgReceive(PlMessage msg)
        {
            if (msg is PlExcludeRegionMsg)
            {
                PlExcludeRegionMsg emsg = (PlExcludeRegionMsg)msg;
                if (emsg.command == PlExcludeRegionMsg.CmdType.Clear)
                    Clear();
                else if (emsg.command == PlExcludeRegionMsg.CmdType.Release)
                    Release();
                return true;
            }
            return false;
        }
    }
}
