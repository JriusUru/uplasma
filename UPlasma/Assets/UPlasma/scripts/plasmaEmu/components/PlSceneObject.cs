/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Base component added to all GameObjects matching actual Plasma scene objects.
    /// While not an actual modifier or interface, we need it to handle messages to
    /// this object.
    /// Due to it being used extensively in the scene, it's best to keep it as light as
    /// possible. You'd better not add an Update() or similar method if you don't want
    /// me to come kick your arse.
    /// </summary>
    public class PlSceneObject : MonoBehaviour, IPlMessageable
    {
        public bool MsgReceive(PlMessage msg)
        {
            if (msg is PlEnableMsg emsg)
            {
                bool doEnable;
                if (emsg.commands.Contains(PlEnableMsg.Command.Disable))
                    doEnable = false;
                else if (emsg.commands.Contains(PlEnableMsg.Command.Enable))
                    doEnable = true;
                else
                    throw new InvalidOperationException("Enable message without an enable command ?");

                if (emsg.commands.Contains(PlEnableMsg.Command.ByType))
                    throw new NotImplementedException("Dunno what ByType flag is");

                bool all = emsg.commands.Contains(PlEnableMsg.Command.All);
                bool audible = emsg.commands.Contains(PlEnableMsg.Command.Audible) || all;
                bool drawable = emsg.commands.Contains(PlEnableMsg.Command.Drawable) || all;
                bool physical = emsg.commands.Contains(PlEnableMsg.Command.Physical) || all;

                if (audible)
                {
                    // not working and not really true anyway. We should probably send it a PlSoundMsg ?
                    //GetComponents<PlSound>().ForEach(x => x.enabled = doEnable);
                    throw new NotImplementedException();
                }
                if (drawable)
                {
                    MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
                    SkinnedMeshRenderer skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
                    if (meshRenderer != null) meshRenderer.enabled = doEnable;
                    if (skinnedMeshRenderer != null) skinnedMeshRenderer.enabled = doEnable;
                }
                if (physical)
                    Array.ForEach(GetComponents<Collider>(), x => x.enabled = doEnable);

                return true;
            }
            return false;
        }
    }
}
