/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /*
    public class PlAnimStage
    {
        [Flags]
        public enum NotifyType
        {
            Enter = 0x1,
            Loop = 0x2,
            Advance = 0x4,
            Regress = 0x8
        };
        
        public enum PlayType
        {
            None,
            Key,
            Auto,
            Max
        };
        
        public enum AdvanceType
        {
            None,
            OnMove,
            Auto,
            OnAnyKey,
            Max
        };
        
        public PlayType forwardType;
        public PlayType backType;
        public AdvanceType advanceType;
        public AdvanceType regressType;
        public string fAnimName;
        public char notify;
        public int loops;
        public bool doAdvanceTo;
        public bool doRegressTo;
        public uint advanceTo;
        public uint regressTo;
        
        // Aux Params
        public float localTime;
        public float length;
        public int curLoop;
        public bool attached;
    }
    
    public class PlMultistageBehMod : MonoBehaviour, PlMessageable
    {
        public List<PlAnimStage> stages;
        public bool fFreezePhys;
        public bool smartSeek;
        public bool reverseFBControlsOnRelease;
        public List<PlMessageable> receivers;
        
        public bool MsgReceive(PlMessage msg)
        {
            return false;
        }
    }
    //*/
}
