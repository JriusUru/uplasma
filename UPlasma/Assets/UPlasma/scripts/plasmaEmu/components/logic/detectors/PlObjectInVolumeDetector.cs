/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlObjectInVolumeDetector : PlDetector
    {
        [Flags]
        public enum CollisionType
        {
            TypeEnter   = 0x01,
            TypeExit    = 0x02,
            TypeAny     = 0x04,
            TypeUnEnter = 0x08,
            TypeUnExit  = 0x10,
            TypeBump    = 0x20,
        };
        
        public CollisionType collisionType;
        public int CollisionTypeInt
        {
            get => (int)collisionType;
            set => collisionType = (CollisionType)value;
        }

        protected virtual void OnTriggerEnter(Collider col)
        {
            // HAXX - detect only the player
            if (col.gameObject.tag != "Player")
                return;
            
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.TriggerEnter, col.gameObject);
        }

        protected virtual void OnTriggerExit(Collider col)
        {
            // HAXX - detect only the player
            if (col.gameObject.tag != "Player")
                return;
            
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.TriggerExit, col.gameObject);
        }
        
        protected virtual void OnCollisionEnter(Collision col)
        {
            // HAXX - detect only the player
            if (col.gameObject.tag != "Player")
                return;
            
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.CollisionEnter, col.gameObject);
        }
        
        protected virtual void OnCollisionExit(Collision col)
        {
            // HAXX - detect only the player
            if (col.gameObject.tag != "Player")
                return;
            
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.CollisionExit, col.gameObject);
        }
        
        protected void SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType type, GameObject triggerer)
        {
            List<IPlMessageable> filteredReceivers = new List<IPlMessageable>();
            foreach (IPlMessageable receiver in receivers)
                if (!(receiver is PlVolumeSensorConditionalObject)) // since the logicmod reroutes messages to the vsco, we don't want to duplicate it.
                    filteredReceivers.Add(receiver);

            PlInternalDetectorMessage msg = new PlInternalDetectorMessage
            {
                eventType = type,
                sender = this,
                receivers = filteredReceivers,
                hitPoint = Vector3.zero,
                hitterObj = triggerer
            };
            msg.Send();
        }
    }
}
