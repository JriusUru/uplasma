/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlPickingDetector : PlDetector
    {
        void OnMouseDown()
        {
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.MouseDown);
        }

        void OnMouseUp()
        {
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.MouseUp);
        }

        void OnMouseUpAsButton()
        {
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.MouseUpAsButton);
        }

        void OnMouseDrag()
        {
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.MouseDrag);
        }

        void OnMouseEnter()
        {
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.MouseEnter);
        }

        void OnMouseExit()
        {
            SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.MouseExit);
        }

        // void OnMouseOver()
        // {
            // SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType.mouseOver);
        // }
        
        void SendDetectorMessage(PlInternalDetectorMessage.DetectorEventType type)
        {
            PlInternalDetectorMessage msg = new PlInternalDetectorMessage
            {
                eventType = type,
                sender = this,
                receivers = receivers,
                hitPoint = GetHitPoint(),
                hitterObj = GetLocalAvatar()
            };
            msg.Send();
        }
        
        Vector3 GetHitPoint()
        {
            Camera cam = GameManager.Instance.MainCamera;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit hit, cam.farClipPlane, Physics.DefaultRaycastLayers, QueryTriggerInteraction.Collide))
                // retrieved the click point in 3D space
                return hit.point;
            return Vector3.zero;
        }
        
        GameObject GetLocalAvatar()
        {
            return null;
        }
    }
}
