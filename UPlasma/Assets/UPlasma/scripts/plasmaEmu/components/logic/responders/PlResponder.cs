/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlResponder : MonoBehaviour, IPlMessageable
    {
        [Flags]
        public enum Flags
        {
            DetectTrigger = 0x1,
            DetectUnTrigger = 0x2,
            SkipFFSound = 0x4
        };

        public int curState = 0;
        public Flags flags = Flags.DetectTrigger;
        public int FlagsInt
        {
            get => (int)flags;
            set => flags = (Flags)value;
        }

        public PlResponderState[] states;
        
        Coroutine runCo;
        int waitingOn = -1;
        
        public bool MsgReceive(PlMessage msg)
        {
            if (msg is PlNotifyMsg)
                Trigger();
            else if (msg is PlEventCallbackMsg emsg)
            {
                waitingOn = emsg.user;
            }
            return false;
        }
        
        void Trigger()
        {
            if (!enabled)
            {
                Debug.LogError("Not running responder " + name + " as it's disabled.");
                return;
            }
            if (runCo != null)
            {
                Debug.LogError("Not running responder " + name + " as it's already running.");
                return;
            }
            
            runCo = StartCoroutine(Run());
        }
        
        IEnumerator Run()
        {
            PlResponderState curRespState = states[curState];
            
            foreach (PlResponderCommand command in curRespState.commands)
            {
                if (command == null || command.message == null)
                    // can happen for messages that aren't imported yet
                    continue;
                command.message.Send();
                if (command.waitOn != -1)
                {
                    // TODO
                    yield return new WaitUntil(() => command.waitOn == waitingOn);
                }
                waitingOn = -1;
            }
            
            // end running, switch to the new state
            curState = curRespState.switchToState;
            runCo = null;
        }
    }
}
