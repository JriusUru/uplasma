/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


namespace UPlasma.PlEmu
{
    public class PlVolumeSensorConditionalObject : PlConditionalObject
    {
        public enum DetectType
        {
            TypeEnter = 1,
            TypeExit,
        }
        
        public int trigNum = -1;
        public DetectType type = DetectType.TypeEnter;
        public int TypeInt
        {
            get => (int)type;
            set => type = (DetectType)value;
        }
        public bool first = false;
        
        public override bool MsgReceive(PlMessage msg)
        {
            if (msg is PlInternalDetectorMessage)
            {
                PlInternalDetectorMessage dmsg = (PlInternalDetectorMessage)msg;
                if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.TriggerEnter && type == DetectType.TypeEnter)
                    logicMod.OnDetectRegionTrigger(dmsg.hitterObj);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.TriggerExit && type == DetectType.TypeExit)
                    logicMod.OnDetectRegionTrigger(dmsg.hitterObj);
            }
            return false;
        }
    }
}
