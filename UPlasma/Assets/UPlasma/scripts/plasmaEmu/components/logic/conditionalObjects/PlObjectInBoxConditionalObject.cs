/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


namespace UPlasma.PlEmu
{
    public class PlObjectInBoxConditionalObject : PlConditionalObject
    {
        public override bool MsgReceive(PlMessage msg)
        {
            if (msg is PlInternalDetectorMessage dmsg)
            {
                if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.CollisionEnter)
                    logicMod.OnClickRegion(true, dmsg.hitterObj);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.CollisionExit)
                    logicMod.OnClickRegion(false, dmsg.hitterObj);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.TriggerEnter)
                    logicMod.OnClickRegion(true, dmsg.hitterObj);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.TriggerExit)
                    logicMod.OnClickRegion(false, dmsg.hitterObj);
            }
            return false;
        }
    }
}
