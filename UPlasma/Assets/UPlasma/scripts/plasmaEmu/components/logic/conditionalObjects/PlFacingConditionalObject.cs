/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlFacingConditionalObject : PlConditionalObject
    {
        public float tolerance = -1;
        public bool directional = false;
        
        public override bool MsgReceive(PlMessage msg)
        {
            if (msg is PlActivatorMsg amsg)
            {
                // print("FacingConditionalObject: EVALUATING.");
                if (amsg.hitterObj != null)
                {
                    // print(amsg.hitterObj.transform);
                    // print(transform);
                    // print(amsg.hitterObj.transform.forward);
                    // print(transform.forward);
                    // print(Vector3.Dot(amsg.hitterObj.transform.forward, transform.forward));
                    // TODO - handle tolerance
                    satisfied = WouldSatisfy(amsg.hitterObj);
                }
            }
            return false;
        }
        
        public bool WouldSatisfy(GameObject obj)
        {
            return Vector3.Dot(obj.transform.forward, transform.forward) > 0;
        }
    }
}
