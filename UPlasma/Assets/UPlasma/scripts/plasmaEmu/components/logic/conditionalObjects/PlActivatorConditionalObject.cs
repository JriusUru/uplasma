/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;

namespace UPlasma.PlEmu
{
    public class PlActivatorConditionalObject : PlConditionalObject
    {
        public List<PlDetector> activators;
        
        public override bool MsgReceive(PlMessage msg)
        {
            if (!activators.Contains((PlDetector)msg.sender))
                return false;
            if (msg is PlInternalDetectorMessage dmsg)
            {
                if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.MouseEnter)
                    logicMod.OnMouseHover(true);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.MouseExit)
                    logicMod.OnMouseHover(false);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.MouseDown)
                    logicMod.OnMouseClick(true);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.MouseUp)
                    logicMod.OnMouseClick(false);
                else if (dmsg.eventType == PlInternalDetectorMessage.DetectorEventType.MouseUpAsButton)
                    logicMod.OnMouseActivate();
            }
            return false;
        }
    }
}
