/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UPlasma;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Plamza logic gate.
    /// To be honest, Plamza logic suxx big time, so we'll rewrite it in a still sucky but easier to understand way.
    /// LogicModifiers, throughout all Plamza, only do two things:
    ///  - detect avatar or object in region (using plVolumeSensorConditionalObject)
    ///  - detect click on object (plObjectInBoxConditionalObject, optional plActivatorConditionalObject and optional plFacingConditionalObject)
    /// We'll simplify the messaging hell with those two use cases.
    /// (Originally each modifier sent its own messages to the logicmod, which would then relay to the OTHER modifiers it relied on, which would
    /// then send more messages, and so on. Theoretically this gives a lot of flexibility when it comes to interacting with other modifiers,
    /// however since logicmod are always setup the same it just results in needless complexity which I'd rather avoid mimicking.)
    /// </summary>
    public class PlLogicModifier : MonoBehaviour, IPlMessageable
    {
        public List<PlConditionalObject> conditions;
        public PlMessage message;
        public CursorType cursor;
        public int CursorInt
        {
            get => (int)cursor;
            set => cursor = (CursorType)value;
        }

#pragma warning disable 414
        PlVolumeSensorConditionalObject facingCondition;
        bool hasClickRegion = false;
        
        public bool inClickRegion = false;
        bool facingClickable = false;
        bool cursorHovered = false;
#pragma warning restore 414

        /// <summary>
        /// Called by the native bindings after Awake and before Start, so the logic modifier can guess what
        /// it has to do before being spammed by OnTriggerEnter
        /// </summary>
        public void UpdateConditions()
        {
            if (conditions != null)
            {
                foreach (PlConditionalObject condition in conditions)
                {
                    // Plamza's stupid reference system...
                    condition.logicMod = this;
                }
            
                // Okay. Now determine this logicmods' type from its conditions...
                foreach (PlConditionalObject condition in conditions)
                {
                    if (condition is PlVolumeSensorConditionalObject)
                        facingCondition = (PlVolumeSensorConditionalObject)condition;
                    if (condition is PlObjectInBoxConditionalObject)
                        hasClickRegion = true;
                }
            }
        }
        
        public bool MsgReceive(PlMessage msg)
        {
            // ugh... we still have ONE last message to process: PlObjectInVolumeDetector is linked to PlVolumeSensorConditionalObject,
            // but not PlObjectInBoxConditionalObject.
            foreach (PlConditionalObject condition in conditions)
            {
                // reroute this message to all conditionals
                List<IPlMessageable> l = new List<IPlMessageable>
                {
                    condition
                };
                msg.receivers = l;
                msg.Send();
            }
            return false;
        }
        
        
        // void Update()
        // {
            // check the facing condition
            // if (inClickRegion && facingCondition != null)
                // facingClickable = facingCondition.wouldSatisfy(GetLocalAvatar());
            // facingClickable = false;
        // }
        
        public void OnDetectRegionTrigger(GameObject triggerer)
        {
            // this is a trigger. Whether the object is entered or exited is handled by the volume sensor

            //if (triggerer is not local avatar)
            //    return;

            // print("LogicModifier: OnDetectRegionTrigger");
            if (message.sender == null) // PFMs can get the sender of the message, so I guess it's set anyway...
                message.sender = this;
            message.Send();
        }
        
        public void OnClickRegion(bool enter, GameObject triggerer)
        {
            // if (triggerer is not local avatar)
                // return;
            
            if (enter)
            {
                GameCursor.Instance.RegisterNearbyActivator(this);
                inClickRegion = true;
            }
            else
            {
                GameCursor.Instance.UnregisterNearbyActivator(this);
                inClickRegion = false;
            }
        }
        
        public void OnMouseHover(bool enter)
        {
            if (enter && (inClickRegion || !hasClickRegion))
                GameCursor.Instance.RegisterHoveredActivator(this);
            else
                GameCursor.Instance.UnregisterHoveredActivator(this);
        }
        
        public void OnMouseClick(bool down)
        {
            // note: OnMouseClick with down=false DOESN'T mean this object was activated ! User might have dragged the cursor away...
            if (down && (inClickRegion || !hasClickRegion))
                GameCursor.Instance.RegisterUsedActivator(this);
            else
                GameCursor.Instance.UnregisterUsedActivator(this);
        }
        
        public void OnMouseActivate()
        {
            // called when user clicked the object without draggin the cursor away from it
            if (!inClickRegion && hasClickRegion)
                return;
            // print("LogicModifier: OnMouseActivate");
            if (message.sender == null) // PFMs can get the sender of the message, so I guess it's set anyway...
                message.sender = this;
            if (message is PlNotifyMsg)
            {
                PlNotifyMsg nmsg = message as PlNotifyMsg;
                nmsg.notifyState = 1; // click down

                PlProPickedEventData evt = new PlProPickedEventData
                {
                    eventType = PlProEventData.EventType.Picked,
                    enabled = true,
                    picker = PlayerController.Instance.gameObject,
                    picked = gameObject
                };

                Ray ray = GameManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hit))
                    evt.hitPoint = hit.point;

                nmsg.notifyEvents.Clear();
                nmsg.notifyEvents.Add(evt);
            }
            message.Send();

            // theoretically, we SHOULD send those two messages on click up and click down...
            // I prefer the current behavior, but it will mess up things like Teledahn's aquarium,
            // so this will probably have to be fixed in the future...
            if (message is PlNotifyMsg)
            {
                PlNotifyMsg nmsg = message as PlNotifyMsg;
                nmsg.notifyState = 0; // click up

                PlProPickedEventData evt = new PlProPickedEventData
                {
                    eventType = PlProEventData.EventType.Picked,
                    enabled = true,
                    picker = PlayerController.Instance.gameObject,
                    picked = gameObject
                };

                Ray ray = GameManager.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out RaycastHit hit))
                    evt.hitPoint = hit.point;

                nmsg.notifyEvents.Clear();
                nmsg.notifyEvents.Add(evt);
            }
            message.Send();
        }
    }
}
