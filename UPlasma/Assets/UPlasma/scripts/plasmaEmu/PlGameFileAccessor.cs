/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Uru runs from a single game folder. UPlasma can handle multiple game folders at once (for merging Uru and EoA, for instance),
    /// along with system mods, user mods, overrides & such - thus providing flexibility.
    /// The purpose of this class is to allows the code to treat those virtual game folders as being merged into one for simplicity,
    /// while they are "physically" into different folders.
    /// 
    /// The various folders used by this class are (in order):
    /// - the main game folders
    ///     (vanilla Uru, EoA, MOUL, CT, etc)
    ///     in PlasmaConfig
    /// - the "system mods" folder
    ///     (bugfixes, shader lists, prp load hints)
    ///     in streaming assets
    ///     always active for all game setups, can't be disabled
    /// - the user mods folder
    ///     (fages mostly, some bugfixes, some overhauls/texture packs, etc)
    ///     in the user's persistent folder
    ///     
    /// note: we still haven't figured out how to handle age name clash for descent, ahny and kveer.
    /// Somehow I know this is going to bite me back in the ass sooner or later...
    /// </summary>
    public class PlGameFileAccessor : MonoBehaviour
    {
        public static PlGameFileAccessor Instance { get; protected set; }
        
        public List<PlasmaConfig.GameFolder> userMods = new List<PlasmaConfig.GameFolder>();
        public List<PlasmaConfig.GameFolder> systemMods = new List<PlasmaConfig.GameFolder>();
        
        void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        /// <summary>
        /// Fetches the absolue path from the relative file (forward slashes only), from the highest-priority game folder.
        /// Example: dat/Garrison_District_Exterior.prp returns c:\mod_path\dat\Garrison_District_Exterior.prp
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        public string GetFile(string fname)
        {
            // check user mods folder
            for (int i=userMods.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(userMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return filePath;
            }
            
            // check system mods folder
            for (int i=systemMods.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(systemMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return filePath;
            }
            
            // check game folders
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            for (int i=gs.folders.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(gs.folders[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return filePath;
            }
            return null;
        }

        /// <summary>
        /// Fetches the absolue path from the relative file (forward slashes only), from the highest-priority game folder
        /// with the same type as specified.
        /// </summary>
        /// <param name="fname"></param>
        /// <param name="game"></param>
        /// <returns></returns>
        public string GetFileFromGame(string fname, PlasmaGame game)
        {
            // check user mods folder
            for (int i=userMods.Count-1; i>=0; i--)
            {
                if (userMods[i].type != game)
                    continue;
                string filePath = Path.Combine(userMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return filePath;
            }
            
            // check system mods folder
            for (int i=systemMods.Count-1; i>=0; i--)
            {
                if (systemMods[i].type != game)
                    continue;
                string filePath = Path.Combine(systemMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return filePath;
            }
            
            // check game folders
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            for (int i=gs.folders.Count-1; i>=0; i--)
            {
                if (gs.folders[i].type != game)
                    continue;
                string filePath = Path.Combine(gs.folders[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return filePath;
            }
            return null;
        }

        /// <summary>
        /// Fetches the list of files in a folder, sorted by modification date (descending).
        /// Returned names don't include the file path.
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public string[] GetFilesByModificationDateDescending(string folder)
        {
            List<FileInfo> files = new List<FileInfo>();
            List<string> knownFiles = new List<string>(); // to check for duplicates
            
            // get files from the user mod folders
            for (int i=userMods.Count-1; i>=0; i--)
            {
                PlasmaConfig.GameFolder gfolder = userMods[i];
                DirectoryInfo info = new DirectoryInfo(Path.Combine(
                    gfolder.path,
                    folder.Replace('/', Path.DirectorySeparatorChar)));
                if (!info.Exists)
                    continue;
                FileInfo[] localFiles = info.GetFiles();
                foreach (FileInfo file in localFiles)
                {
                    if (!knownFiles.Contains(file.Name))
                    {
                        knownFiles.Add(file.Name);
                        files.Add(file);
                    }
                    // else file was already overridden
                }
            }
            // get files from the system mod folders
            for (int i=systemMods.Count-1; i>=0; i--)
            {
                PlasmaConfig.GameFolder gfolder = systemMods[i];
                DirectoryInfo info = new DirectoryInfo(Path.Combine(
                    gfolder.path,
                    folder.Replace('/', Path.DirectorySeparatorChar)));
                if (!info.Exists)
                    continue;
                FileInfo[] localFiles = info.GetFiles();
                foreach (FileInfo file in localFiles)
                {
                    if (!knownFiles.Contains(file.Name))
                    {
                        knownFiles.Add(file.Name);
                        files.Add(file);
                    }
                    // else file was already overridden
                }
            }
            
            // get files from the game root folders
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            for (int i=gs.folders.Count-1; i>=0; i--)
            {
                PlasmaConfig.GameFolder gfolder = gs.folders[i];
                DirectoryInfo info = new DirectoryInfo(Path.Combine(
                    gfolder.path,
                    folder.Replace('/', Path.DirectorySeparatorChar)));
                if (!info.Exists)
                    continue;
                FileInfo[] localFiles = info.GetFiles();
                foreach (FileInfo file in localFiles)
                {
                    if (!knownFiles.Contains(file.Name))
                    {
                        knownFiles.Add(file.Name);
                        files.Add(file);
                    }
                    // else file was already overridden
                }
            }
            
            // Sort by modification time descending
            files.Sort(delegate(FileInfo f1, FileInfo f2) {
                return f2.LastWriteTime.CompareTo(f1.LastWriteTime);
            });
            
            List<string> fileNames = new List<string>();
            foreach (FileInfo file in files)
                fileNames.Add(file.Name);
            
            return fileNames.ToArray();
        }

        /// <summary>
        /// Fetches the list of files in a folder.
        /// Returned names don't include the file path.
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public string[] GetFiles(string folder)
        {
            List<FileInfo> files = new List<FileInfo>();
            List<string> knownFiles = new List<string>(); // to check for duplicates
            
            // get files from the user mod folders
            for (int i=userMods.Count-1; i>=0; i--)
            {
                PlasmaConfig.GameFolder gfolder = userMods[i];
                DirectoryInfo info = new DirectoryInfo(Path.Combine(
                    gfolder.path,
                    folder.Replace('/', Path.DirectorySeparatorChar)));
                if (!info.Exists)
                    continue;
                FileInfo[] localFiles = info.GetFiles();
                foreach (FileInfo file in localFiles)
                {
                    if (!knownFiles.Contains(file.Name))
                    {
                        knownFiles.Add(file.Name);
                        files.Add(file);
                    }
                    // else file was already overridden
                }
            }
            // get files from the system mod folders
            for (int i=systemMods.Count-1; i>=0; i--)
            {
                PlasmaConfig.GameFolder gfolder = systemMods[i];
                DirectoryInfo info = new DirectoryInfo(Path.Combine(
                    gfolder.path,
                    folder.Replace('/', Path.DirectorySeparatorChar)));
                if (!info.Exists)
                    continue;
                FileInfo[] localFiles = info.GetFiles();
                foreach (FileInfo file in localFiles)
                {
                    if (!knownFiles.Contains(file.Name))
                    {
                        knownFiles.Add(file.Name);
                        files.Add(file);
                    }
                    // else file was already overridden
                }
            }
            
            // get files from the game root folders
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            for (int i=gs.folders.Count-1; i>=0; i--)
            {
                PlasmaConfig.GameFolder gfolder = gs.folders[i];
                DirectoryInfo info = new DirectoryInfo(Path.Combine(
                    gfolder.path,
                    folder.Replace('/', Path.DirectorySeparatorChar)));
                if (!info.Exists)
                    continue;
                FileInfo[] localFiles = info.GetFiles();
                foreach (FileInfo file in localFiles)
                {
                    if (!knownFiles.Contains(file.Name))
                    {
                        knownFiles.Add(file.Name);
                        files.Add(file);
                    }
                    // else file was already overridden
                }
            }
            
            List<string> fileNames = new List<string>();
            foreach (FileInfo file in files)
                fileNames.Add(file.Name);
            
            return fileNames.ToArray();
        }
        
        /// <summary>
        /// Checks whether the given file exists in any of the game installs.
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        public bool FileExists(string fname)
        {
            // check user mods folder
            for (int i=userMods.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(userMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return true;
            }
            
            // check system mods folder
            for (int i=systemMods.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(systemMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return true;
            }
            
            // check game folders
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            for (int i=gs.folders.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(gs.folders[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Checks whether the given directory exists in any of the game installs.
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        public bool DirectoryExists(string fname)
        {
            // check user mods folder
            for (int i=userMods.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(userMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (Directory.Exists(filePath))
                    return true;
            }
            
            // check system mods folder
            for (int i=systemMods.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(systemMods[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (Directory.Exists(filePath))
                    return true;
            }
            
            // check game folders
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            for (int i=gs.folders.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(gs.folders[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (Directory.Exists(filePath))
                    return true;
            }
            return false;
        }

        // ↓ not sure those two are required...

        ///// fetches the absolue path from the relative file (forward slashes only), from the specified mod
        //public string GetModFile(string modname, string fname)
        //{
        //    PlasmaConfig.GameSetup gs = PlasmaConfig.instance.getActiveGameSetup();
        //    string modPath = gs.folders.Find(x => x.name == modname).path;
        //    if (string.IsNullOrEmpty(modPath))
        //        return null;

        //    string filePath = Path.Combine(modPath, fname.Replace('/', Path.DirectorySeparatorChar));
        //    if (File.Exists(filePath))
        //        return filePath;
        //    return null;
        //}

        /// <summary>
        /// Returns the game type for the specified file (for loading PRPs across game versions).
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        public PlasmaGame GetFileGameType(string fname)
        {
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            for (int i=gs.folders.Count-1; i>=0; i--)
            {
                string filePath = Path.Combine(gs.folders[i].path, fname.Replace('/', Path.DirectorySeparatorChar));
                if (File.Exists(filePath))
                    return gs.folders[i].type;
            }
            throw new FileNotFoundException(fname);
        }
        
        /// <summary>
        /// Returns the path to a texture override if it exists.
        /// </summary>
        /// <param name="ageName"></param>
        /// <param name="pageName"></param>
        /// <param name="fileName">name of the texture to find</param>
        /// <returns></returns>
        public string GetTexOverride(string ageName, string pageName, string fileName)
        {
            // TODO - this should also look into the texture bounty list...
            string fullFilePath = Path.Combine(new string[] {"overrides", ageName, pageName, "textures", fileName});
            string[] supportedFormats = new string[]{".png", ".jpg", ".jpeg", ".dds"};
            foreach (string format in supportedFormats)
            {
                string existingFile = GetFile(fullFilePath + format);
                if (!string.IsNullOrEmpty(existingFile))
                    return existingFile;
            }
            return null;
        }

        /// <summary>
        /// Returns the path to an anim override if it exists.
        /// </summary>
        /// <param name="ageName"></param>
        /// <param name="pageName"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetAnimOverride(string ageName, string pageName, string fileName)
        {
            string fullFilePath = Path.Combine(new string[] {"overrides", ageName, pageName, "animations", fileName});
            return GetFile(fullFilePath);
        }

        /// <summary>
        /// Returns the path to a drawable mesh override if it exists.
        /// </summary>
        /// <param name="ageName"></param>
        /// <param name="pageName"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetDrawMeshOverride(string ageName, string pageName, string fileName)
        {
            string fullFilePath = Path.Combine(new string[] {"overrides", ageName, pageName, "meshes", fileName});
            return GetFile(fullFilePath);
        }

        /// <summary>
        /// Returns the path to a physic mesh override if it exists.
        /// </summary>
        /// <param name="ageName"></param>
        /// <param name="pageName"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string GetPhysMeshOverride(string ageName, string pageName, string fileName)
        {
            string fullFilePath = Path.Combine(new string[] {"overrides", ageName, pageName, "physics", fileName});
            return GetFile(fullFilePath);
        }

        /// <summary>
        /// Returns the path to a loading clues file.
        /// </summary>
        /// <param name="ageName"></param>
        /// <param name="pageName"></param>
        /// <param name="game"></param>
        /// <returns></returns>
        public PrpImportClues GetPrpLoadClues(string ageName, string pageName, PlasmaGame game)
        {
            string fullRelPath = Path.Combine("overrides", ageName, pageName + ".json");
            string fullFilePath = GetFileFromGame(fullRelPath, game);
            if (!string.IsNullOrEmpty(fullFilePath))
                return PrpImportClues.Load(fullFilePath);
            
            // none exists... Create one, which will be modified by the importer (useful to create Age shader list)
            // Note: since we almost always want this file created to make modding easier, we'll save it in a "cache" folder
            // in the user's data path. And since we save it there, might as well try to load from this location first, right ?
            // Keep in mind that the cache folder is NOT a real mod, it's only meant to be temporary. Ideally we'd save the file
            // in a real mod folder, but hey, which one ?
            // anyway... find the path to the cache folder.
            string fileCachePath = Path.Combine(new string[] {
                GameManager.Instance.userDataPath,
                "cache",
                game.ToString(),
                "ages",
                ageName,
                pageName + ".json",
            });
            if (File.Exists(fileCachePath))
                // there we go. Just load it from this location.
                return PrpImportClues.Load(fileCachePath);

            // really nothing. Ah well, just create it already.
            PrpImportClues clue = new PrpImportClues
            {
                savePath = fileCachePath
            };
            // oh, and make sure the folder exists - that might be useful
            Directory.CreateDirectory(Path.GetDirectoryName(clue.savePath));
            return clue;
        }
        
        void OnDestroy()
        {
            if (Instance == null)
                Instance = this;
        }
    }
}
