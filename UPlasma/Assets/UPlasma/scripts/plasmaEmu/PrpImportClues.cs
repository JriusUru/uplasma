/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;
using Newtonsoft.Json;
using System;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Allows overriding some shader properties per-material.
    /// This allows overriding any property save texture ones (for the obvious reason that
    /// overriding textures is too cumbersome).
    /// </summary>
    [Serializable]
    public class MatPropOverride
    {
        /// <summary>
        /// Essentially a copy of UnityEditor.ShaderPropertyType for runtime use.
        /// (Why this isn't included into the runtime assembly is beyond me...)
        /// </summary>
        public enum ShaderPropertyType
        {
            Color,
            Vector,
            Float,
            Range,
            TexEnv
        }

        /// <summary>
        /// Name of the shader property.
        /// </summary>
        public string name = null;

        /// <summary>
        /// Values of the property (whether it's float, range, color, vector, etc).
        /// </summary>
        public float[] floatVals;

        public ShaderPropertyType type;
    }

    [Serializable]
    public class MatOverride
    {
        /// <summary>
        /// Name of the shader for this material.
        /// </summary>
        public string shaderName = null;

        /// <summary>
        /// Render queue of the material. Higher means the material is drawn later, which helps with transparency issues.
        /// </summary>
        public int renderQueue = -1;

        /// <summary>
        /// Per-material overrides.
        /// </summary>
        public MatPropOverride[] props;

        /// <summary>
        /// Apply all serialized properties to the given Unity material.
        /// </summary>
        /// <param name="mat"></param>
        public void ApplyPropsToMat(Material mat)
        {
            if (props == null)
                return;
            foreach (MatPropOverride prop in props)
            {
                switch (prop.type)
                {
                    case MatPropOverride.ShaderPropertyType.Color:
                        if (prop.floatVals.Length >= 4)
                            mat.SetColor(prop.name, new Color(prop.floatVals[0], prop.floatVals[1], prop.floatVals[2], prop.floatVals[3]));
                        else if (prop.floatVals.Length == 3)
                            mat.SetColor(prop.name, new Color(prop.floatVals[0], prop.floatVals[1], prop.floatVals[2], 1));
                        break;
                    case MatPropOverride.ShaderPropertyType.Float:
                    case MatPropOverride.ShaderPropertyType.Range:
                        if (prop.floatVals.Length >= 1)
                            mat.SetFloat(prop.name, prop.floatVals[0]);
                        break;
                    case MatPropOverride.ShaderPropertyType.Vector:
                        if (prop.floatVals.Length >= 4)
                            mat.SetVector(prop.name, new Vector4(prop.floatVals[0], prop.floatVals[1], prop.floatVals[2], prop.floatVals[3]));
                        break;
                }
            }
        }
    }

    [Serializable]
    public class MeshRendererOverride
    {
        public ShadowCastingMode castShadows = ShadowCastingMode.Off;
    }

    [Serializable]
    public class LightOverride
    {
        public float range = -1;
        public float intensity = -1;
        public bool shadows = false;
        public float shadowStrength = -1;
        public int layers = -1;
    }

    [Serializable]
    public class ObjOverride
    {
        /// <summary>
        /// Whether the Unity object is active.
        /// </summary>
        public bool enableSelf = true;
        // ↓ whether the various components are active
        public bool enableDraw = true;
        public bool enablePhys = true;
        public bool enableLight = true;
        public bool enableAnim = true;
        public bool enableAudio = true;
        
        // ↓ if !=null, will override the corresponding properties of the object
        public LightOverride lightOverride = null;
        public MeshRendererOverride meshRendererOverride = null;
    }

    /// <summary>
    /// Simple class for storing overrides to the PRP, to simplify patching the imported scene.
    /// For instance, allows changing material shader, object layer, disable bogus meshes, etc.
    /// </summary>
    [Serializable]
    public class PrpImportClues
    {
        /// <summary>
        /// Whether to load this page at all.
        /// </summary>
        public bool load = true;
        
        public Dictionary<string, ObjOverride> objOverrides = new Dictionary<string, ObjOverride>();
        public Dictionary<string, MatOverride> matOverrides = new Dictionary<string, MatOverride>();
        public Dictionary<int, List<string>> layers = new Dictionary<int, List<string>>();
        
        // convenience functions because I don't feel like generating native bindings for dictionary<>
        public ObjOverride GetObjOverride(string objName)
        {
            if (objOverrides.ContainsKey(objName))
                return objOverrides[objName];
            return null;
        }
        public MatOverride GetMatOverride(string matName)
        {
            if (matOverrides.ContainsKey(matName))
                return matOverrides[matName];
            return null;
        }
        public void AddMatOverride(string matName, MatOverride overrde)
        { matOverrides[matName] = overrde; }

        /// <summary>
        /// Simply used to make file saving simpler.
        /// </summary>
        [JsonIgnore]
        public string savePath;
        
        public void Save()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            File.WriteAllText(savePath, JsonConvert.SerializeObject(this, settings));
        }
        
        public static PrpImportClues Load(string path)
        {
            PrpImportClues clue = JsonConvert.DeserializeObject<PrpImportClues>(File.ReadAllText(path));
            clue.savePath = path;
            return clue;
        }
    }
}
