/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Since we're creating assets dynamically, Unity doesn't manage them. This class will store assets such as meshes, materials and
    /// textures, and when destroyed, it will also destroy the children objects.
    /// </summary>
    public class AssetsLifeBinder : MonoBehaviour
    {
        [HideInInspector] // For eh, PERFORMANCE reasons.
        public List<Object> assets = new List<Object>();
        
        void OnDestroy()
        {
            foreach (Object asset in assets)
            {
                // time to die !
                Destroy(asset);
            }
        }
    }
}
