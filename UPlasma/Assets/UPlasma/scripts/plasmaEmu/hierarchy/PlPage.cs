/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlPage : MonoBehaviour
    {
        public int id;
        [HideInInspector] // hide the object in the inspector, otherwise eats up performances
        public List<GameObject> pageObjects = new List<GameObject>();
        
        public GameObject GetObj(string objName)
        {
            foreach (GameObject obj in pageObjects)
            {
                if (obj.name == objName)
                    return obj;
            }
            return null;
        }

        void OnDestroy()
        {
            // also destroy children objects
            foreach (GameObject obj in pageObjects)
            {
                if (obj)
                    Destroy(obj);
            }
        }
    }
}
