/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    public class PlAge : MonoBehaviour
    {
        /// <summary>
        /// List of all currently loaded Ages.
        /// </summary>
        static List<PlAge> ages = new List<PlAge>();

        /// <summary>
        /// Latest Age loaded.
        /// </summary>
        static PlAge latestAge = null;

        /// <summary>
        /// This Age's Plasma sequence prefix
        /// </summary>
        public int sequencePrefix;
        
        /// <summary>
        /// Pages in this Age.
        /// </summary>
        public List<PlPage> pages = new List<PlPage>();
        
        void Awake()
        {
            ages.Add(this);
            latestAge = this;
        }

        void OnDestroy()
        {
            // destroying the Age also unloads the pages
            // (this is also required because the AssetsLifeBinder is on this object too)
            foreach (PlPage page in pages)
            {
                if (page)
                    Destroy(page);
            }

            ages.Remove(this);
            if (latestAge == this)
                latestAge = null;
        }
        
        public static PlAge GetLatest()
        {
            return latestAge;
        }
        
        /// <summary>
        /// Finds an Age from its name.
        /// </summary>
        /// <param name="ageName">system name of the Age (from the .age file)</param>
        /// <returns>the corresponding PlAge or null</returns>
        public static PlAge Get(string ageName)
        {
            PlAge age = ages.Find((x) => x.name == ageName);
            return age;
        }

        /// <summary>
        /// Finds an Age from its sequence prefix.
        /// </summary>
        /// <param name="sequencePrefix">Plasma sequence prefix for the Age</param>
        /// <returns>the corresponding PlAge or null</returns>
        public static PlAge Get(int sequencePrefix)
        {
            PlAge age = ages.Find((x) => x.sequencePrefix == sequencePrefix);
            return age;
        }

        /// <summary>
        /// Finds a page from its name.
        /// </summary>
        /// <param name="pageName">name of the page (as listed in the .age file)</param>
        /// <returns>the PlPage or null</returns>
        public PlPage GetPage(string pageName)
        {
            PlPage page = pages.Find((x) => x.name == pageName);
            return page;
        }

        /// <summary>
        /// Finds a page from its id.
        /// </summary>
        /// <param name="pageId">id of the page (as listed in the .age file)</param>
        /// <returns>the PlPage or null</returns>
        public PlPage GetPage(int pageId)
        {
            PlPage page = pages.Find((x) => x.id == pageId);
            return page;
        }
    }
}
