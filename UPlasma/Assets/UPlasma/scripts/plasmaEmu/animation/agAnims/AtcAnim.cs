/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// An animation which has a "Animation Time Controller" (concept, doesn't have written class equivalent) to handle remapping ingame time to animation time.
    /// </summary>
    public class AtcAnim : AgAnim
    {
        public float initial;
        public float loopStart;
        public float loopEnd;
        public bool autoStart;
        public bool loop;
        public byte easeInType;
        public byte easeOutType;
        public float easeInLength;
        public float easeInMin;
        public float easeInMax;
        public float easeOutLength;
        public float easeOutMin;
        public float easeOutMax;
        public Dictionary<string, float> markers = new Dictionary<string, float>();
        public Dictionary<string, Tuple<float, float>> loops = new Dictionary<string, Tuple<float, float>>();
        public float[] stopPoints;

        float curTime = 0;
        bool enabled = false;

        public override void Start()
        {
            if (autoStart)
                enabled = true;
            if (initial >= 0)
                curTime = initial;
        }

        public override void Apply(float time)
        {
            if (!enabled)
                return;

            // TODO - actually rely on the given time.
            // For now we'll manage with delta time.
            curTime += Time.deltaTime;
            if (loop)
            {
                float loopLength = loopEnd - loopStart;
                while (curTime > loopEnd)
                    curTime -= loopLength;
                while (curTime < loopStart)
                    curTime += loopLength;
            }
            curTime = Mathf.Clamp(curTime, start, end);
            foreach (IAgApplicator applicator in applicators)
                if (applicator.Enabled)
                    applicator.Apply(curTime);
        }
    }
}
