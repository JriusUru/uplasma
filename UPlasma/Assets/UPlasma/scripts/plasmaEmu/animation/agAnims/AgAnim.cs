/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Plasma's version of AnimationClip.
    /// </summary>
    public abstract class AgAnim
    {
        public float blend;
        public float start;
        public float end;
        public string name;
        public IAgApplicator[] applicators;

        public virtual void Start() { }
        public abstract void Apply(float time);

        public void UpdateTargets(Dictionary<string, PlAgModifier> channels)
        {
            Array.ForEach(applicators, x => x.Target = channels[x.ChannelName]);
        }
    }
}
