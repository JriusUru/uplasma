/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// An anim which is tied directly to Age time (=SDL var).
    /// </summary>
    public class AgeGlobalAnim : AgAnim
    {
        public string globalVarName;

        public override void Apply(float time)
        {
            // TODO - actually rely on the given time.
            // For now we'll manage with delta time.
            float curTime = Time.timeSinceLevelLoad;
            while (curTime > end)
                curTime -= end;
            while (curTime < start)
                curTime += start;
            foreach (IAgApplicator applicator in applicators)
                applicator.Apply(curTime);
        }
    }
}
