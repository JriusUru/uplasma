/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Applies a vector3/point/rgbcolor (without alpha) value to a light's diffuse color property.
    /// </summary>
    public class LightDiffuseApplicator : IAgApplicator
    {
        public PointControllerChannel PointChannel { get; set; }
        public IAgChannel Channel { get => PointChannel; set => PointChannel = (PointControllerChannel)value; } // reroute to the fully-typed object, which we use more often.
        public bool Enabled { get; set; }
        public string ChannelName { get; set; }
        public PlAgModifier Target
        {
            get => targetLight.transform.parent.GetComponent<PlAgModifier>();
            set => targetLight = value.transform.GetComponentInChildren<Light>();
        }

        Light targetLight;

        public void Apply(float animTime)
        {
            PointChannel.Interp(animTime);
            targetLight.color = PointChannel.value.AsColor();
            // FIXME: IIRC Plasma lights can go above color value of 1, thus modifying the light's intensity.
            // Unity colors are clamped, so light won't go above this value.
            // But then shader light is clamped in Plasma, while it isn't in Unity. xgmrlbgr.
        }
    }
}
