/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Applies a matrix value (sampled) to an object's transform/coordinate component.
    /// (Should be called CoordinateApplicator IMHO...)
    /// </summary>
    public class MatrixChannelApplicator : IAgApplicator
    {
        // ↓ originally a MatrixControllerChannel, but we're no longer animating a matrix >.>
        public TrsControllerChannel TrsChannel { get; set; }
        public IAgChannel Channel { get => TrsChannel; set => TrsChannel = (TrsControllerChannel)value; } // reroute to the fully-typed object, which we use more often.
        public bool Enabled { get; set; }
        public string ChannelName { get; set; }
        public PlAgModifier Target
        {
            get => targetTransform.GetComponent<PlAgModifier>();
            set => targetTransform = value.transform;
        }

        Transform targetTransform;

        public void Apply(float animTime)
        {
            TrsChannel.Interp(animTime);
            // Remember: Vector3s are NOT already in Unity's space, but Quaternions are.
            targetTransform.localPosition = TrsChannel.GetPosition().Swizzle_xzy() * .3048f;
            targetTransform.localRotation = TrsChannel.GetRotation();
            targetTransform.localScale = TrsChannel.GetScale().Swizzle_xzy();
        }
    }
}
