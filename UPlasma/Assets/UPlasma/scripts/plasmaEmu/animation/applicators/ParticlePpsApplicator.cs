/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Applies a scalar to a particle system's particle-per-second property.
    /// </summary>
    public class ParticlePpsApplicator : IAgApplicator
    {
        public ScalarControllerChannel ScalarChannel { get; set; }
        public IAgChannel Channel { get => ScalarChannel; set => ScalarChannel = (ScalarControllerChannel)value; } // reroute to the fully-typed object, which we use more often.
        public bool Enabled { get; set; }
        public string ChannelName { get; set; }
        public PlAgModifier Target
        {
            get => targetPartsys.GetComponent<PlAgModifier>();
            set => targetPartsys = value.GetComponent<ParticleSystem>();
        }

        ParticleSystem targetPartsys;

        public void Apply(float animTime)
        {
            ScalarChannel.Interp(animTime);

            return;

            // Particle system are not imported yet, will be for later !

            //ParticleSystem.EmissionModule emissionModule = targetPartsys.emission;
            //emissionModule.rateOverTime = ScalarChannel.value;
        }
    }
}
