/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A <see cref="LeafController{T}"/> for animating a <see cref="Vector3"/> value.
    /// </summary>
    public class Point3Controller : LeafController<Vector3, Point3Key>
    {
        protected override Vector3 LerpKeys(Point3Key left, Point3Key right, float progress)
        {
            if (left.outTan == Vector3.zero && right.inTan == Vector3.zero)
                return Vector3.Lerp(left.Value, right.Value, progress);

            float scale = GetScale(left.Time, right.Time);

            return new Vector3(
                BezScalarEval(left.Value.x, left.outTan.x, right.Value.x, right.inTan.x, progress, scale),
                BezScalarEval(left.Value.y, left.outTan.y, right.Value.y, right.inTan.y, progress, scale),
                BezScalarEval(left.Value.z, left.outTan.z, right.Value.z, right.inTan.z, progress, scale));
        }
    }
}
