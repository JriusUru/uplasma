/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A controller for a particular type of object. Doesn't use sub-controllers.
    /// </summary>
    /// <typeparam name="T">type of the value interpolated</typeparam>
    /// <typeparam name="T">type of the keyframe used to interpolate the value</typeparam>
    public abstract class LeafController<T, TKey> : IController<T> where TKey: IKeyFrame<T>
    {
        public TKey[] Keyframes { get; set; }

        protected int lastKeyId;

        /// <summary>
        /// Returns some scaling number to be used with <see cref="BezScalarEval(float, float, float, float, float, float)"/>.
        /// 30 is the number of frames in a Plasma anim. 160 is the "MAX_TICKS_PER_FRAME" constant.
        /// Don't ask me more about how it works. All I know is it does.
        /// </summary>
        /// <param name="leftTime"></param>
        /// <param name="rightTime"></param>
        /// <returns></returns>
        protected float GetScale(float leftTime, float rightTime) => (rightTime - leftTime) * 160 * 30 / 3f;

        /// <summary>
        /// Interpolates the two values using Bézier curve.
        /// (Yes, you write that with an é, that's the only thing azerty keyboards are useful for.)
        /// The tangent values are actually not standard tangents. See Plamza's source for more info:
        /// https://github.com/H-uru/Plasma/blob/d9f59fe1cec2d47337e048104d793efd7aeae047/Sources/Plasma/PubUtilLib/plInterp/hsInterp.cpp#L332
        /// This code is copy/pasted almost verbatim from Plamza. I leave the intricate math majiicks
        /// to people who actually understand those.
        /// </summary>
        /// <param name="leftVal"></param>
        /// <param name="leftTan"></param>
        /// <param name="rightVal"></param>
        /// <param name="rightTan"></param>
        /// <param name="t"></param>
        /// <param name="tanScale"></param>
        /// <returns></returns>
        protected float BezScalarEval(float leftVal, float leftTan, float rightVal, float rightTan, float t, float tanScale)
        {
            float oneMinusT = (1.0f - t);
            float tSquared = t * t;
            float oneMinusTSquared = oneMinusT * oneMinusT;

            return (oneMinusT * oneMinusTSquared * leftVal) +
                (3 * t * oneMinusTSquared * (leftVal + leftTan * tanScale)) +
                (3 * tSquared * oneMinusT * (rightVal + rightTan * tanScale)) +
                (tSquared * t * rightVal);
        }

        /// <summary>
        /// Lerp a value between the two given keys.
        /// The <see cref="Interp(float)"/> method is already defined, but it relies on this method which 
        /// children class must implement depending on the type of keyframes they use.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <param name="progress">0 for left key, 1 for right key</param>
        /// <returns></returns>
        protected abstract T LerpKeys(TKey left, TKey right, float progress);

        /// <summary>
        /// Samples the animation curve at the specified time. You don't need to redefine this method in your sub class.
        /// Will automatically extend the values outside of the animation clip's bounds.
        /// </summary>
        /// <param name="animTime"></param>
        /// <returns></returns>
        public void Interp(float animTime, ref T result)
        {
            // NOTE: this function is likely going to be the most time-critical of the anim system. Current implementation should perform pretty fast.

            if (Keyframes.Length == 0)
                return;

            TKey zero = Keyframes[0];
            TKey last = Keyframes[Keyframes.Length - 1];
            if (animTime <= zero.Time)
            {
                // before our first frame - just return its value
                lastKeyId = 0;
                result = zero.Value;
            }
            else if (animTime >= last.Time)
            {
                // after our last frame - just return its value
                lastKeyId = Keyframes.Length - 2; // never assign the last item in the list - when sampling, we work with keyframe duos, which makes things easier.
                result = last.Value;
            }
            else
            {
                // We're going to need two keyframes that are left and right of the current time.
                // We'll start from the last used keyframe, then move forward or backward (never both at once though) until our two keys are correct.
                // To do that, we'll loop twice:
                // - Once in order to go backward so our left key is before the curtime
                // - Once in order to go forward so our right key is after the curtime
                // Obviously, both loops should be mutually exclusive.

                // Get the left key.
                TKey curKey = Keyframes[lastKeyId];
                while (curKey.Time > animTime) // compare the left key's time with the current time
                {
                    // animTime is located before our two sampling keys. Move backward.
                    lastKeyId--;
                    curKey = Keyframes[lastKeyId];
                }
                // Now take the right key, and compare it with the current time again.
                TKey nextKey = Keyframes[lastKeyId + 1];
                while (nextKey.Time < animTime) // compare the right key's time with the current time. This only happens if the above loop didn't execute one.
                {
                    // animTime is located after our two sampling keys. Move forward.
                    lastKeyId++;
                    nextKey = Keyframes[lastKeyId + 1];
                }

                // now that we are sure the right key is correctly placed, fetch the left key again
                curKey = Keyframes[lastKeyId];

                // and blend between the two !
                float progress = Mathf.InverseLerp(curKey.Time, nextKey.Time, animTime); // blessed be reverse lerping...
                result = LerpKeys(curKey, nextKey, progress); // blessed be lerping...
            }
        }
    }
}
