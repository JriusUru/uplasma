/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A <see cref="LeafController{T}"/> for animating a float value.
    /// </summary>
    public class ScalarController : LeafController<float, ScalarKey>
    {
        protected override float LerpKeys(ScalarKey left, ScalarKey right, float progress)
        {
            if (left.outTan == 0 && right.inTan == 0)
                return Mathf.Lerp(left.Value, right.Value, progress);

            float scale = GetScale(left.Time, right.Time);
            return BezScalarEval(left.Value, left.outTan, right.Value, right.inTan, progress, scale);
        }
    }
}
