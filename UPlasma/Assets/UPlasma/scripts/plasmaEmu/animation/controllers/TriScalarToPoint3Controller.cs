/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// An <see cref="IController{T}"/> for animating a <see cref="Vector3"/> value using
    /// multiple <see cref="IController{float}"/>s.
    /// </summary>
    public class TriScalarToPoint3Controller : IController<Vector3>
    {
        public IController<float> x;
        public IController<float> y;
        public IController<float> z;

        public void Interp(float animTime, ref Vector3 result)
        {
            x?.Interp(animTime, ref result.x);
            y?.Interp(animTime, ref result.y);
            z?.Interp(animTime, ref result.z);
        }
    }
}
