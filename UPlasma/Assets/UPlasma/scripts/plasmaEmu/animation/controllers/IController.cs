/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A controller for a specific type of data.
    /// </summary>
    /// <typeparam name="T">The data type this controller can animate.</typeparam>
    public interface IController<T>
    {
        /// <summary>
        /// Interpolates a value at the given time.
        /// </summary>
        /// <param name="animTime">Animation time to use for interpolation.</param>
        /// <param name="result">The resulting interpolated value. Will be left untouched if
        /// the controller is unable to sample a value (can happen for some compound controllers).</param>
        void Interp(float animTime, ref T result);
    }
}
