/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A <see cref="LeafController{T}"/> for animating a quaternion value.
    /// </summary>
    public class QuatController : LeafController<Quaternion, QuatKey>
    {
        protected override Quaternion LerpKeys(QuatKey left, QuatKey right, float progress)
        {
            // Correctly interpolate the quaternion. Instead of stupidly interpolating XYZW components like Unity's animation system does... Duh.
            // Hmmm... Note that we have two options here:
            // - spherical interpolation, or
            // - linear interpolation with normalizing afterwards.
            // Spherical interpolation should look better, but linear is faster.
            // Plasma's default is spherical. I'm going to assume we can use spherical too without any performance issue.
            // (Also, quaternion aren't allowed to have in/out tangents. Sucks 4 em.)
            return Quaternion.Slerp(left.Value, right.Value, progress);
        }
    }
}
