/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// A <see cref="IController{T}"/> for animating a quaternion value from three euler angles using
    /// <see cref="IController{float}"/>s.
    /// </summary>
    public class TriScalarToQuatController : IController<Quaternion>
    {
        public IController<float> x;
        public IController<float> y;
        public IController<float> z;

        public void Interp(float animTime, ref Quaternion result)
        {
            float vx = 0, vy = 0, vz = 0;
            x?.Interp(animTime, ref vx);
            y?.Interp(animTime, ref vy);
            z?.Interp(animTime, ref vz);

            // TODO - could this be optimized ?
            result =
                Quaternion.Euler(0, -vz * Mathf.Rad2Deg, 0) *
                Quaternion.Euler(0, 0, -vy * Mathf.Rad2Deg) *
                Quaternion.Euler(-vx * Mathf.Rad2Deg, 0, 0);
        }
    }
}
