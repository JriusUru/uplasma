/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// An <see cref="IController{T}"/> for animating <see cref="AffineParts"/> using
    /// several sub-controllers.
    /// </summary>
    public class TrsController : IController<AffineParts>
    {
        public IController<Vector3> posCtrl;
        public IController<Quaternion> rotCtrl;
        public IController<Vector3> scaCtrl;

        public void Interp(float animTime, ref AffineParts result)
        {
            posCtrl?.Interp(animTime, ref result.translation);
            rotCtrl?.Interp(animTime, ref result.rotation);
            scaCtrl?.Interp(animTime, ref result.scale);
        }
    }
}
