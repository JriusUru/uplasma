/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Plasma-specific intermediary component. Just a simple passthrough for now.
    /// </summary>
    public class ScalarControllerChannel : IAgChannel
    {
        public string Name { get; set; }

        /// <summary>
        /// The controller that actually samples animation.
        /// </summary>
        public IController<float> controller;

        public float value;

        public void Interp(float animTime)
        {
            controller.Interp(animTime, ref value);
        }
    }
}
