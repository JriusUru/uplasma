/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Stores how Plasma is configured.
    /// Most notably, it stores the various game setups, which consist of multiple folders that work together
    /// as a single Plasma game (more info in PlGameFileAccessor).
    /// </summary>
    [Serializable]
    public class PlasmaConfig
    {
        public static PlasmaConfig Instance { get; protected set; }
        
        [JsonIgnore]
        public string savePath;
        
        public class GameFolder
        {
            public string name;
            public string path;
            public PlasmaGame type;
        }
        public class GameSetup
        {
            public string name;
            public PlasmaGame type;
            public List<GameFolder> folders = new List<GameFolder>();
        }
        
        public List<GameSetup> gameSetups = new List<GameSetup>();
        public int activeGameSetup = -1;
        
        public PlasmaConfig()
        {
            if (Instance == null)
                Instance = this;
        }
        
        ~PlasmaConfig()
        {
            if (Instance == this)
                Instance = null;
        }
        
        public GameSetup GetActiveGameSetup()
        {
            if (activeGameSetup >= 0 && gameSetups.Count > activeGameSetup)
                return gameSetups[activeGameSetup];
            return null;
        }
        
        public void Save()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            File.WriteAllText(savePath, JsonConvert.SerializeObject(this, settings));
        }
        
        public static PlasmaConfig Load(string path)
        {
            PlasmaConfig cfg = JsonConvert.DeserializeObject<PlasmaConfig>(File.ReadAllText(path));
            cfg.savePath = path;
            return cfg;
        }
    }
}
