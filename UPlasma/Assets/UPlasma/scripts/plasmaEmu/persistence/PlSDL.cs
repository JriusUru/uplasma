/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Text.RegularExpressions;
using Google.Protobuf;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Plasma State Description file. Parsed from a statedesc language file (obviously).
    /// </summary>
    [Serializable]
    public class PlSDL
    {
        public enum SDLVarType
        {
            // from plSDLDescriptor in Plasma source
            None   = -1,

            // atomic types
            Int,
            Float,
            Bool,
            String32,
            /// <summary>
            /// plKey - basically a uoid
            /// </summary>
            Key,
            /// <summary>
            /// this var refers to another state descriptor
            /// </summary>
            StateDescriptor,
            /// <summary>
            /// plCreatable - basically a classIdx and a read/write buffer
            /// </summary>
            Creatable,
            Double,
            /// <summary>
            /// double which is automatically converted to server clock and back, use for animation times
            /// </summary>
            Time,
            Byte,
            Short,
            /// <summary>
            /// float which is automatically set to the current age time of day (0-1)
            /// </summary>
            AgeTimeOfDay,
            
            // the following are a vector of floats
            Vector3=50,// atomicCount=3
            Point3,    // atomicCount=3
            RGB,       // atomicCount=3
            RGBA,      // atomicCount=4
            Quaternion,    // atomicCount=4
            RGB8,      // atomicCount=3
            RGBA8,     // atomicCount=4
        }
        
        /// <summary>
        /// Standard state description variable. This class is accessed using PtGetAgeSDL()["var name"] in the Python API.
        /// In the Python API this essentially behaves like a list, but assignments to its values
        /// will send those values on the server, which in turn will send back a notification to all clients.
        /// </summary>
        /// 
        /// TODO:
        /// - prevent writing to agetimeofday
        /// - check type before assignment
        /// - net sync
        public class PlSDVar :
            ICollection<object>,
            IEnumerable<object>,
            IList<object>,
            IReadOnlyCollection<object>,
            IReadOnlyList<object>,
            ICollection
        {
            public string name;
            public SDLVarType type;
            public int count;
            public List<object> values;
            public object defaultValue;
            public PlSDL sdl;
            /// <summary>
            /// Listeners for any change to this variable. The float value represents modification tolerance.
            /// </summary>
            public Dictionary<PlPythonFileMod, float> listeners = new Dictionary<PlPythonFileMod, float>();

            public object this[int index]
            {
                get { return values[index]; }
                set {
                    values[index] = value;
                    ProgressManager.Instance.RegisterDirtySDLVar(this);
                }
            }

            public int Count { get { return values.Count; } }
            public bool IsReadOnly { get { return false; } }
            public bool IsSynchronized { get { return false; } }
            public object SyncRoot { get { return null; } }
            // SDL value list are fixed size if the size was specified in the description file.
            public bool IsFixedSize { get { return count != -1; } }

            public void Add(object item)
            {
                if (IsFixedSize)
                    return;
                values.Add(item);
                ProgressManager.Instance.RegisterDirtySDLVar(this);
            }

            public void Clear()
            {
                if (IsFixedSize)
                    return;
                values.Clear();
                ProgressManager.Instance.RegisterDirtySDLVar(this);
            }

            public bool Contains(object item)
            {
                return values.Contains(item);
            }

            public void CopyTo(object[] array, int arrayIndex)
            {
                values.CopyTo(array, arrayIndex);
            }

            public void CopyTo(Array array, int index)
            {
                for (int i=0; i<values.Count; i++)
                    array.SetValue(values[i], i + index);
            }

            public IEnumerator<object> GetEnumerator()
            {
                return values.GetEnumerator();
            }

            public int IndexOf(object item)
            {
                return values.IndexOf(item);
            }

            public void Insert(int index, object item)
            {
                if (IsFixedSize)
                    return;
                values.Insert(index, item);
                ProgressManager.Instance.RegisterDirtySDLVar(this);
            }

            public bool Remove(object item)
            {
                if (IsFixedSize)
                    return false;
                ProgressManager.Instance.RegisterDirtySDLVar(this);
                return values.Remove(item);
            }

            public void RemoveAt(int index)
            {
                if (IsFixedSize)
                    return;
                values.RemoveAt(index);
                ProgressManager.Instance.RegisterDirtySDLVar(this);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return values.GetEnumerator();
            }
        }

        public string name;
        public int version;
        public List<PlSDVar> variables = new List<PlSDVar>();
        
        public static List<PlSDL> GetSDLsFromString(string fileContent, bool ignoreDuplicates)
        {
            List<PlSDL> sdls = new List<PlSDL>();
            
            // would be better to use an ast parser, maybe. Bah, what the heck.
            
            // sdl syntax comes in two variants: the uru (uppercase) one, and the eoa (lowercase) one.
            // they also use slightly different keywords, the uru one is separated by newlines while the eoa one uses
            // semicolon (we'll just treat it as newline too...)
            
            string[] lines = fileContent.Split('\n');
            bool eoaSyntax = false;
            bool waitingOpeningBracket = false;
            bool waitingForVersion = false;
            int lineNum = 0;
            PlSDL curSDL = null;
            string curSDLName = null;
            foreach (string rawLine in lines)
            {
                lineNum++;
                // sanitise line: remove leading/trailing spaces and comments
                string line = rawLine.Replace("\r", ""); // just in case...
                int dashPos = line.IndexOf('#');
                if (dashPos != -1)
                    line = line.Substring(0, dashPos);
                line = line.Trim();
                if (line == string.Empty)
                    // empty line, ignore
                    continue;
                
                // okay, so this line contains some actual data
                // in which case, we're either in a statedesc, or out of it.
                if (curSDL != null)
                {
                    if (line == "}")
                    {
                        // end of statedesc
                        sdls.Add(curSDL);
                        curSDL = null;
                        continue;
                    }
                    
                    // first line of a statedesc is the version
                    if (waitingForVersion)
                    {
                        string eoaVarKw = "version ";
                        string uruVarKw = "VERSION ";
                        if (eoaSyntax && line.StartsWith(eoaVarKw))
                        {
                            // found version
                            // parse the integer. Remove the trailing semicolon.
                            curSDL.version = Int32.Parse(line.Substring(eoaVarKw.Length, line.Length - eoaVarKw.Length - 1).Trim());
                            waitingForVersion = false;
                        }
                        else if (!eoaSyntax && line.StartsWith(uruVarKw))
                        {
                            // found version
                            curSDL.version = Int32.Parse(line.Substring(uruVarKw.Length).Trim());
                            waitingForVersion = false;
                        }
                        else
                        {
                            throw new SdlParseException("Could not parse line " + lineNum + ": \"" + line + "\", expecting version number");
                        }
                    }
                    else
                    {
                        // version already read, this means this line is a variable
                        if (eoaSyntax)
                        {
                            Debug.LogError("MV SDL parsing not yet implemented");
                            break;
                        }
                        else
                        {
                            // Uru variable
                            // remove duplicate spaces so we can split things
                            line = Regex.Replace(line, @"\s+", " ");
                            line = line.Replace(";", ""); // alright Cyan, this is getting a bit silly, isn't it ?
                            string[] ls = line.Split(' ');
                            if (ls.Length < 3)
                            {
                                Debug.LogError("SDL: line " + lineNum + ": \"" + line + "\": variable incomplete");
                                continue;
                            }
                            if (ls[0] != "VAR")
                            {
                                Debug.LogError("SDL: line " + lineNum + ": no var keyword");
                                continue;
                            }
                            string varName = ls[2];
                            int sqBracketL = varName.IndexOf('[');
                            int sqBracketR = varName.IndexOf(']');
                            int arrLength = -1; // default to variable size (-1)
                            if (sqBracketL != -1 && sqBracketR != -1 && (sqBracketL+1 != sqBracketR))
                            {
                                arrLength = Int32.Parse(varName.Substring(sqBracketL+1, sqBracketR - sqBracketL - 1));
                                varName = varName.Substring(0, sqBracketL);
                            }

                            PlSDVar var = new PlSDVar
                            {
                                sdl = curSDL,
                                name = varName,
                                type = SDLVarType.None,
                                count = arrLength,
                                values = new List<object>()
                            };

                            if (ls[1].StartsWith("$"))
                            {
                                // statedesc, whose name follows the $
                                var.type = SDLVarType.StateDescriptor;
                            }
                            else if (ls[1] == "INT")
                            {
                                var.type = SDLVarType.Int;
                                var.defaultValue = 0;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = Int32.Parse(cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", ""));
                                }
                            }
                            else if (ls[1] == "FLOAT")
                            {
                                var.type = SDLVarType.Float;
                                var.defaultValue = 0.0f;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = Single.Parse(cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", ""),
                                            System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture);
                                }
                            }
                            else if (ls[1] == "BOOL")
                            {
                                var.type = SDLVarType.Bool;
                                var.defaultValue = false;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                    {
                                        string valStr = cmd.Substring("DEFAULT=".Length).Replace("(", "").Replace(")", "");
                                        var.defaultValue = (valStr == "0" || valStr.ToLower() == "false");
                                    }
                                }
                            }
                            else if (ls[1] == "STRING32")
                            {
                                var.type = SDLVarType.String32;
                                var.defaultValue = "";
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", "")
                                            .Replace("\"", "").Replace("'", "");
                                }
                            }
                            else if (ls[1] == "PLKEY")
                            { var.type = SDLVarType.Key; }
                            else if (ls[1] == "CREATABLE")
                            { var.type = SDLVarType.Creatable; }
                            else if (ls[1] == "DOUBLE")
                            {
                                var.type = SDLVarType.Double;
                                var.defaultValue = 0.0d;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = Double.Parse(cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", ""));
                                }
                            }
                            else if (ls[1] == "TIME")
                            {
                                var.type = SDLVarType.Time;
                                var.defaultValue = 0.0d;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = Double.Parse(cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", ""));
                                }
                            }
                            else if (ls[1] == "BYTE")
                            {
                                var.type = SDLVarType.Byte;
                                var.defaultValue = (byte)0;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = Byte.Parse(cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", ""));
                                }
                            }
                            else if (ls[1] == "SHORT")
                            {
                                var.type = SDLVarType.Short;
                                var.defaultValue = (short)0;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = Int16.Parse(cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", ""));
                                }
                            }
                            else if (ls[1] == "AGETIMEOFDAY")
                            {
                                var.type = SDLVarType.AgeTimeOfDay;
                                var.defaultValue = 0.0f;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = Single.Parse(cmd.Substring("DEFAULT=".Length)
                                            .Replace("(", "").Replace(")", ""));
                                }
                            }
                            else if (ls[1] == "VECTOR3")
                            {
                                var.type = SDLVarType.Vector3;
                                var.defaultValue = Vector3.zero;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = ParseVector3(cmd.Substring("DEFAULT=".Length));
                                }
                            }
                            else if (ls[1] == "POINT3")
                            {
                                var.type = SDLVarType.Point3;
                                var.defaultValue = Vector3.zero;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = ParseVector3(cmd.Substring("DEFAULT=".Length));
                                }
                            }
                            // else if (ls[1] == "RGB") // both unused AFAIK
                            // else if (ls[1] == "RGBA")
                            else if (ls[1] == "QUATERNION")
                            {
                                var.type = SDLVarType.Quaternion;
                                var.defaultValue = Quaternion.identity;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = ParseQuaternion(cmd.Substring("DEFAULT=".Length));
                                }
                            }
                            else if (ls[1] == "RGB8")
                            {
                                var.type = SDLVarType.RGB8;
                                var.defaultValue = Color.black;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = ParseColor(cmd.Substring("DEFAULT=".Length));
                                }
                            }
                            else if (ls[1] == "RGBA8")
                            {
                                var.type = SDLVarType.RGBA8;
                                var.defaultValue = Color.black;
                                for (int i=3; i<ls.Length; i++)
                                {
                                    string cmd = ls[i];
                                    if (cmd.StartsWith("DEFAULT="))
                                        var.defaultValue = ParseColor(cmd.Substring("DEFAULT=".Length));
                                }
                            }
                            else
                                Debug.LogError("Unknown variable type " + ls[1]);

                            for (int i = 0; i < arrLength; i++)
                                var.values.Add(var.defaultValue);

                            curSDL.variables.Add(var);
                        }
                    }
                }
                else
                {
                    // not in statedesc yet. Which means we're either reading the statedesc keyword, or the opening bracket
                    if (waitingOpeningBracket)
                    {
                        // statedesc keyword and name already read. Hopefully this is an opening bracket...
                        if (line == "{")
                        {
                            waitingOpeningBracket = false;
                            curSDL = new PlSDL
                            {
                                name = curSDLName
                            };
                            waitingForVersion = true;
                        }
                        else
                        {
                            throw new SdlParseException("Could not parse line " + lineNum + ": \"" + line + "\", expecting opening bracket");
                        }
                    }
                    else
                    {
                        // still no statedesc found. Try to read one
                        if (line.StartsWith("STATEDESC"))
                        {
                            // trim excess spaces
                            line = Regex.Replace(line, @"\s+", " ");
                            // statedesc, Uru version
                            int kwLength = "STATEDESC ".Length;
                            int bracketPos = line.IndexOf('{'); // see if the bracket is on the same line
                            if (bracketPos != -1) // does that ever happen ?...
                            {
                                string descName = line.Substring(kwLength, bracketPos - kwLength).Trim();
                                eoaSyntax = false;
                                waitingOpeningBracket = false;
                                curSDL = new PlSDL
                                {
                                    name = descName
                                };
                                waitingForVersion = true;
                            }
                            else
                            {
                                curSDLName = line.Substring(kwLength);
                                eoaSyntax = false;
                                waitingOpeningBracket = true;
                            }
                        }
                        else if (line.StartsWith("struct "))
                        {
                            // statedesc, EOA version
                            int kwLength = "struct ".Length;
                            int bracketPos = line.IndexOf('{'); // see if the bracket is on the same line
                            if (bracketPos != -1) // does that ever happen ?...
                            {
                                string descName = line.Substring(kwLength, bracketPos - kwLength).Trim();
                                eoaSyntax = true;
                                waitingOpeningBracket = false;
                                curSDL = new PlSDL
                                {
                                    name = descName
                                };
                                waitingForVersion = true;
                            }
                            else
                            {
                                curSDLName = line.Substring(kwLength);
                                eoaSyntax = true;
                                waitingOpeningBracket = true;
                            }
                        }
                        else
                        {
                            throw new SdlParseException("Could not parse line " + lineNum + ": \"" + line + "\", expecting statedesc keyword");
                        }
                    }
                }
            }
            
            if (ignoreDuplicates)
            {
                for (int i=0; i<sdls.Count; i++)
                {
                    for (int j=i+1; j<sdls.Count; j++)
                    {
                        PlSDL x = sdls[i];
                        PlSDL y = sdls[j];
                        if (x.name == y.name)
                        {
                            if (x.version < y.version)
                                sdls.Remove(x);
                            else
                                sdls.Remove(y);
                        }
                    }
                }
            }
            
            return sdls;
        }

        /// <summary>
        /// Returns a Vector3 from a string (formatted SDL way, ie: (x,y,z))
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static Vector3 ParseVector3(string str)
        {
            // remove the parenthesis and optional space
            str = str.Replace("(", "");
            str = str.Replace(")", "");
            str = str.Replace(" ", "");
            string[] valuesStr = str.Split(','); // get the values individually
            return new Vector3(
                Single.Parse(valuesStr[0]),
                Single.Parse(valuesStr[1]),
                Single.Parse(valuesStr[2])
            );
        }

        /// <summary>
        /// Returns a Quaternion from a string (formatted SDL way, ie: (x,y,z,w))
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static Quaternion ParseQuaternion(string str)
        {
            // remove the parenthesis and optional space
            str = str.Replace("(", "");
            str = str.Replace(")", "");
            str = str.Replace(" ", "");
            string[] valuesStr = str.Split(','); // get the values individually
            return new Quaternion(
                Single.Parse(valuesStr[0]),
                Single.Parse(valuesStr[1]),
                Single.Parse(valuesStr[2]),
                Single.Parse(valuesStr[3])
            );
        }

        /// <summary>
        /// Returns a Color from a string (formatted SDL way, ie: (x,y,z))
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static Color ParseColor(string str)
        {
            // remove the parenthesis and optional space
            str = str.Replace("(", "");
            str = str.Replace(")", "");
            str = str.Replace(" ", "");
            string[] valuesStr = str.Split(','); // get the values individually
            if (valuesStr.Length == 4) // RGBA
                return new Color(
                    float.Parse(valuesStr[0]),
                    float.Parse(valuesStr[1]),
                    float.Parse(valuesStr[2]),
                    float.Parse(valuesStr[3])
                );
            
            return new Color( // RGB
                float.Parse(valuesStr[0]),
                float.Parse(valuesStr[1]),
                float.Parse(valuesStr[2]),
                1
            );
        }

        public PlSDVar GetVariable(string varName)
        {
            return variables.Find((x) => x.name == varName);
        }

        public void RestoreValues(Stream stream)
        {
            PlSerializableSDL serializedSDL = PlSerializableSDL.Parser.ParseFrom(stream);

            // now we need to copy the values to this class.
            // we could keep the values inside the serialized class, but having the definition
            // and actual values would be messy.
            // plus this makes it easier ensure the SDL values are correct.
            
            bool isSaveOlder = serializedSDL.Version < version;
            bool isSaveNewer = serializedSDL.Version > version;
            //bool isSaveSameVersion = values.Version == version;

            if (isSaveNewer)
            {
                Debug.LogError("SDL " + name + ": got version that is newer than the one in the definition.\n"
                    + "This is REALLY not something that should happen ! I'll load it anyway, but seriously, "
                    + "you should try to find the reason for this...");
            }
            else if (isSaveOlder)
            {
                Debug.Log("SDL " + name + ": upgrading from v" + serializedSDL.Version + " to v" + version);
            }

            foreach (PlSerializableSDLVar variable in serializedSDL.Variables)
            {
                // check if it's known to us
                PlSDVar varDef = variables.Find((var) => var.name == variable.Name);
                if (varDef == null)
                    // dafuk ? we can't find a definition matching the saved variable. Simply ignore it.
                    continue;
                if (!IsSerialVarOfType(variable, varDef.type))
                {
                    Debug.LogWarning("SDL " + name + ": variable " + variable.Name + " changed type ! "
                        + "Old type: " + variable.Val[0].ValCase + ", new type: " + varDef.type);

                    // reset values to default
                    for (int i = 0; i < varDef.count; i++)
                        varDef.values[i] = varDef.defaultValue;
                    // and continue to the next variable
                    continue;
                }
                if (varDef.count != -1 && (variable.Val.Count != varDef.count))
                {
                    Debug.LogWarning("SDL " + name + ": variable " + variable.Name + " changed length ! "
                        + "Old length: " + variable.Val.Count + ", new length: " + varDef.count);
                    // in practice this doesn't change much for us...
                }

                // now copy the values from the serialized class to the current one.
                // ignore extraneous values (above varDef.count), unless varDef.count == -1
                // (when that's the case, the array is dynamic)
                int maxCount = (varDef.count != -1) ?
                    Mathf.Min(varDef.count, variable.Val.Count) :
                    variable.Val.Count;
                for (int i = 0; i < maxCount; i++)
                {
                    switch (varDef.type)
                    {
                        case SDLVarType.Int:
                        case SDLVarType.Byte:
                        case SDLVarType.Short:
                            varDef.values[i] = variable.Val[i].ValInt;
                            break;
                        case SDLVarType.Float:
                        case SDLVarType.AgeTimeOfDay:
                            varDef.values[i] = variable.Val[i].ValFloat;
                            break;
                        case SDLVarType.Bool:
                            varDef.values[i] = variable.Val[i].ValBool;
                            break;
                        case SDLVarType.String32:
                            varDef.values[i] = variable.Val[i].ValString;
                            break;
                        case SDLVarType.Double:
                        case SDLVarType.Time:
                            varDef.values[i] = variable.Val[i].ValDouble;
                            break;
                        case SDLVarType.Vector3:
                        case SDLVarType.Point3:
                            varDef.values[i] = new Vector3()
                            {
                                x = variable.Val[i].ValVector3.X,
                                y = variable.Val[i].ValVector3.Y,
                                z = variable.Val[i].ValVector3.Z
                            };
                            break;
                        case SDLVarType.RGB:
                        case SDLVarType.RGB8:
                            varDef.values[i] = new Color()
                            {
                                r = variable.Val[i].ValVector3.X,
                                g = variable.Val[i].ValVector3.Y,
                                b = variable.Val[i].ValVector3.Z
                            };
                            break;
                        case SDLVarType.Quaternion:
                            varDef.values[i] = new Quaternion()
                            {
                                x = variable.Val[i].ValVector4.X,
                                y = variable.Val[i].ValVector4.Y,
                                z = variable.Val[i].ValVector4.Z,
                                w = variable.Val[i].ValVector4.W
                            };
                            break;
                        case SDLVarType.RGBA:
                        case SDLVarType.RGBA8:
                            varDef.values[i] = new Color()
                            {
                                r = variable.Val[i].ValVector4.X,
                                g = variable.Val[i].ValVector4.Y,
                                b = variable.Val[i].ValVector4.Z,
                                a = variable.Val[i].ValVector4.W
                            };
                            break;
                    }
                }
            }
        }
        public void SaveValues(Stream stream)
        {
            PlSerializableSDL serialSDL = new PlSerializableSDL
            {
                // update to the latest version
                Version = version
            };
            foreach (PlSDVar sdlvar in variables)
            {
                if (!IsTypeSupported(sdlvar.type))
                    continue;
                if (sdlvar.values.Count == 0)
                    // no need to save what doesn't exist...
                    continue;

                PlSerializableSDLVar serialVar = new PlSerializableSDLVar
                {
                    Name = sdlvar.name
                };
                foreach (object val in sdlvar.values)
                    serialVar.Val.Add(CreateSerializedVarValue(sdlvar.type, val));
                serialSDL.Variables.Add(serialVar);
            }
            // and save
            serialSDL.WriteTo(stream);
        }

        bool IsTypeSupported(SDLVarType type)
        {
            switch (type)
            {
                case SDLVarType.Int:
                case SDLVarType.Byte:
                case SDLVarType.Short:
                case SDLVarType.Float:
                case SDLVarType.AgeTimeOfDay:
                case SDLVarType.Bool:
                case SDLVarType.String32:
                case SDLVarType.Double:
                case SDLVarType.Time:
                case SDLVarType.Vector3:
                case SDLVarType.Point3:
                case SDLVarType.RGB:
                case SDLVarType.RGB8:
                case SDLVarType.Quaternion:
                case SDLVarType.RGBA:
                case SDLVarType.RGBA8:
                    return true;
                default:
                    return false;
            }
        }

        PlSerializableSDLValue CreateSerializedVarValue(SDLVarType type, object value)
        {
            var val = new PlSerializableSDLValue();
            switch (type)
            {
                case SDLVarType.Int:
                    val.ValInt = (int)value;
                    break;
                case SDLVarType.Byte:
                    val.ValInt = (byte)value; // serious-fucking-ly, csharp...
                    break;
                case SDLVarType.Short:
                    val.ValInt = (short)value;
                    break;
                case SDLVarType.Float:
                case SDLVarType.AgeTimeOfDay:
                    val.ValFloat = (float)value;
                    break;
                case SDLVarType.Bool:
                    val.ValBool = (bool)value;
                    break;
                case SDLVarType.String32:
                    val.ValString = (string)value;
                    break;
                case SDLVarType.Double:
                case SDLVarType.Time:
                    val.ValDouble = (double)value;
                    break;
                case SDLVarType.Vector3:
                case SDLVarType.Point3:
                    Vector3 defValVect = (Vector3)value;
                    val.ValVector3 = new PlSerializableSDLVector3() {
                        X = defValVect.x,
                        Y = defValVect.y,
                        Z = defValVect.z
                    };
                    break;
                case SDLVarType.RGB:
                case SDLVarType.RGB8:
                    Color defValCol = (Color)value;
                    val.ValVector3 = new PlSerializableSDLVector3() {
                        X = defValCol.r,
                        Y = defValCol.g,
                        Z = defValCol.b
                    };
                    break;
                case SDLVarType.Quaternion:
                    Quaternion defValQuat = (Quaternion)value;
                    val.ValVector4 = new PlSerializableSDLVector4()
                    {
                        X = defValQuat.x,
                        Y = defValQuat.y,
                        Z = defValQuat.z,
                        W = defValQuat.w
                    };
                    break;
                case SDLVarType.RGBA:
                case SDLVarType.RGBA8:
                    defValCol = (Color)value;
                    val.ValVector4 = new PlSerializableSDLVector4()
                    {
                        X = defValCol.r,
                        Y = defValCol.g,
                        Z = defValCol.b,
                        W = defValCol.a
                    };
                    break;
                default:
                    throw new NotImplementedException();
            }
            return val;
        }

        bool IsSerialVarOfType(PlSerializableSDLVar var, SDLVarType type)
        {
            if (var.Val.Count == 0)
                // empty values means there is no data, so the var isn't typed yet.
                // this MIGHT happen for dynamically sized array, unfortunately...
                // since this var has no type, it's compatible with whatever type we want,
                // so return true.
                return true;

            PlSerializableSDLValue val = var.Val[0];
            switch (type)
            {
                // we're going to take a few liberties. if the variable type changed but the stored
                // data is the same, then return true.
                case SDLVarType.Int:
                case SDLVarType.Byte:
                case SDLVarType.Short:
                    return val.ValCase == PlSerializableSDLValue.ValOneofCase.ValInt;
                case SDLVarType.Float:
                case SDLVarType.AgeTimeOfDay:
                    return val.ValCase == PlSerializableSDLValue.ValOneofCase.ValFloat;
                case SDLVarType.Bool:
                    return val.ValCase == PlSerializableSDLValue.ValOneofCase.ValBool;
                case SDLVarType.String32:
                    return val.ValCase == PlSerializableSDLValue.ValOneofCase.ValString;
                case SDLVarType.Double:
                case SDLVarType.Time:
                    return val.ValCase == PlSerializableSDLValue.ValOneofCase.ValDouble;
                case SDLVarType.Vector3:
                case SDLVarType.Point3:
                case SDLVarType.RGB:
                case SDLVarType.RGB8:
                    return val.ValCase == PlSerializableSDLValue.ValOneofCase.ValVector3;
                case SDLVarType.RGBA:
                case SDLVarType.Quaternion:
                case SDLVarType.RGBA8:
                    return val.ValCase == PlSerializableSDLValue.ValOneofCase.ValVector4;
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the specified object to receive notifications from modifications to the specified SDL variable,
        /// with the specified modification tolerance.
        /// </summary>
        /// <param name="selfkey">object receiving notifies</param>
        /// <param name="key">name of the variable to listen to</param>
        /// <param name="tolerance">delta under which no notifications are sent</param>
        public void SetNotify(PlPythonFileMod selfkey, string key, float tolerance)
        {
            GetVariable(key).listeners[selfkey] = tolerance;
        }

        /// <summary>
        /// Called when a player changed a variable value on the server.
        /// This also happens when we're the one changing the value - the full
        /// </summary>
        /// <param name="name"></param>
        /// <param name="values"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        public void ServerValueChanged(string name, List<object> values, int playerID, string tag)
        {
            PlSDVar variable = GetVariable(name);
            if (variable == null)
                return;

            List<object> oldvalues = variable.values;
            variable.values = values;

            float delta = 0;
            bool ignoreDelta = false;
            for (int i = 0; i < oldvalues.Count && i < values.Count; i++)
            {
                switch (variable.type)
                {
                    case SDLVarType.AgeTimeOfDay:
                    case SDLVarType.Float:
                        delta += Mathf.Abs((float)values[i] - (float)oldvalues[i]);
                        break;
                    case SDLVarType.Double:
                    case SDLVarType.Time:
                        delta += Mathf.Abs((float)((double)values[i] - (double)oldvalues[i]));
                        break;
                    // also implement other int types here if needed
                    default:
                        ignoreDelta = true;
                        break;
                }
            }

            foreach (KeyValuePair<PlPythonFileMod, float> kv in variable.listeners)
            {
                PlPythonFileMod listener = kv.Key;
                float tolerance = kv.Value;
                if (ignoreDelta || delta > tolerance)
                    listener.TryCallMethod("OnSDLNotify",
                        new dynamic[] { variable.name, variable.sdl.name, playerID, tag });
            }
        }
    }
}
