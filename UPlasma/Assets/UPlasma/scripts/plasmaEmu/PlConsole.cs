/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Replicates Plamsa's console, allowing to do things like execute FNIs etc.
    /// </summary>
    public class PlConsole : MonoBehaviour
    {
        public static PlConsole Instance { get; protected set; }
        
        /// <summary>
        /// Changes the far rendering distance of the camera.
        /// </summary>
        /// <param name="yon">distance in meters</param>
        public void Graphics_Renderer_SetYon(float yon)
        {
            // TODO - make it modify all potential cameras
            GameManager.Instance.MainCamera.farClipPlane = yon;
        }

        /// <summary>
        /// Setups fog with linear density.
        /// </summary>
        /// <param name="start">start distance in meters</param>
        /// <param name="end">end distance in meters</param>
        /// <param name="density">multiplier (?) for fog thickness</param>
        public void Graphics_Renderer_Fog_SetDefLinear(float start, float end, float density)
        {
            RenderSettings.fog = density > 0 && end > 0;
            if (density > 0)
            {
                RenderSettings.fogStartDistance = start / density;
                RenderSettings.fogEndDistance = end / density;
                RenderSettings.fogMode = FogMode.Linear;
            }
        }

        /// <summary>
        /// Setups fog with exponential density.
        /// </summary>
        /// <param name="end">end distance in meters</param>
        /// <param name="density">multiplier (?) for fog thickness</param>
        public void Graphics_Renderer_Fog_SetDefExp2(float end, float density)
        {
            RenderSettings.fog = density > 0 && end > 0;
            RenderSettings.fogMode = FogMode.ExponentialSquared;
            // exp2 is slightly different in unity, as it uses only density
            // The following formula seems correct, although I don't know why since I found it through trial and error...
            // Dunno why you must divide end by 3, but that gives the most accurate result. Tested in
            // my Canyon and Er'cana, result is ok.
            float uDensity = 1 / (end * .3333f) * density;
            RenderSettings.fogDensity = uDensity;
        }

        /// <summary>
        /// Sets the fog color.
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        public void Graphics_Renderer_Fog_SetDefColor(float r, float g, float b)
        {
            // these values actually need to be clamped. Thanks, Oolbahnneea !
            r = Mathf.Clamp(r, 0, 1);
            g = Mathf.Clamp(g, 0, 1);
            b = Mathf.Clamp(b, 0, 1);
            RenderSettings.fogColor = new Color(r, g, b);
        }

        /// <summary>
        /// Sets the clear color for the camera (ie: color when no object is displayed).
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        public void Graphics_Renderer_Fog_SetClearColor(float r, float g, float b)
        {
            r = Mathf.Clamp(r, 0, 1);
            g = Mathf.Clamp(g, 0, 1);
            b = Mathf.Clamp(b, 0, 1);
            // TODO - make it modify all potential cameras
            GameManager.Instance.MainCamera.backgroundColor = new Color(r, g, b);
        }
        
        /// <summary>
        /// Parses and executes the given commands.
        /// </summary>
        /// <param name="content">a text file with commands to run (separated by newline character)</param>
        /// <returns>whether all commands were run successfully</returns>
        public bool Parse(string content)
        {
            // TODO - might be better to actually throw exceptions when a line failed to run
            bool fullyParsed = true;
            foreach (string line in content.Split('\n'))
            {
                string lineTrim = line.Trim();
                if (lineTrim.IndexOf('#') != -1)
                    lineTrim = lineTrim.Substring(0, line.IndexOf('#')).Trim();
                if (lineTrim == string.Empty)
                    continue;
                
                try {
                    string[] lineSplit = line.Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
                    string cmd = lineSplit[0].ToLower();
                    if (cmd == "graphics.renderer.setyon")
                    {
                        float yon = ParseFloat(lineSplit[1]) * .3048f;
                        Graphics_Renderer_SetYon(yon);
                    }
                    else if (cmd == "graphics.renderer.fog.setdeflinear")
                    {
                        float start     = ParseFloat(lineSplit[1]) * .3048f;
                        float end       = ParseFloat(lineSplit[2]) * .3048f;
                        float density   = ParseFloat(lineSplit[3]);
                        
                        Graphics_Renderer_Fog_SetDefLinear(start, end, density);
                    }
                    else if (cmd == "graphics.renderer.fog.setdefexp2")
                    {
                        float end       = ParseFloat(lineSplit[1]) * .3048f;
                        float density   = ParseFloat(lineSplit[2]);
                        
                        Graphics_Renderer_Fog_SetDefExp2(end, density);
                    }
                    else if (cmd == "graphics.renderer.fog.setdefcolor")
                    {
                        float r = ParseFloat(lineSplit[1]);
                        float g = ParseFloat(lineSplit[2]);
                        float b = ParseFloat(lineSplit[3]);
                        
                        Graphics_Renderer_Fog_SetDefColor(r, g, b);
                    }
                    else if (cmd == "graphics.renderer.setclearcolor")
                    {
                        float r = ParseFloat(lineSplit[1]);
                        float g = ParseFloat(lineSplit[2]);
                        float b = ParseFloat(lineSplit[3]);
                        
                        Graphics_Renderer_Fog_SetClearColor(r, g, b);
                    }
                    else if (cmd == "graphics.renderer.gamma2")
                    {
                        // float g = parseFloat(lineSplit[1]);
                        
                        // this line is mostly bogus in MV's FNIs.
                        // Graphics_Renderer_Gamma2(g);
                    }
                    else
                        Debug.LogError("Unparsed Plasma command: " + lineTrim);
                }
                catch (System.Exception ex)
                {
                    Debug.LogError(ex);
                    fullyParsed = false;
                    Debug.LogError("Error while processing Plasma command: " + lineTrim);
                }
                
                fullyParsed = false;
            }
            return fullyParsed;
        }

        /// <summary>
        /// Simplified version of <see cref="float.Parse(string)"/>.
        /// Otherwise float.parse will fail because of culture thingies (French pcs will expect comma instead of dot, etc).
        /// </summary>
        /// <param name="s">string to parse</param>
        /// <returns>the float value</returns>
        float ParseFloat(string s)
        {
            return float.Parse(s, System.Globalization.NumberStyles.Float, System.Globalization.CultureInfo.InvariantCulture);
        }
        
        void Awake()
        {
            if (Instance == null)
                Instance = this;
        }
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }
    }
}
