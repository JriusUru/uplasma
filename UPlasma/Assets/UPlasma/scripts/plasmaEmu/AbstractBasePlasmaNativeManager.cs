/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// WORKAROUND to objects not returned correctly from the native plugin T_T
    /// </summary>
    public class HackReturnObject
    {
        public object val;
    }
    
    /// <summary>
    /// Base class for a Unity component written in full C++ O.o
    /// This will be the main entry point for all common interactions with HSPlasma's
    /// resmanager, among other things.
    /// </summary>
    public abstract class AbstractBasePlasmaNativeManager : MonoBehaviour
    {
        public abstract void Start();

        /// <summary>
        /// Sets the location where to store Age cached data (shader list, uncompiled Python files and so on).
        /// Cache data will usually be written to <paramref name="path"/>/cache/[game]/ageName.
        /// </summary>
        /// <param name="path"></param>
        public abstract void SetCachePath(string path);

        /// <summary>
        /// Sets the location of import tools and such (Drizzle, pycdc...)
        /// This is a temporary workaround, hopefully it won't stay here forever.
        /// </summary>
        /// <param name="path"></param>
        public abstract void SetToolsPath(string path);

        /// <summary>
        /// Loads a full Age in the active scene.
        /// </summary>
        /// <param name="ageName">System name (ie: <paramref name="ageName"/>.age) of the Age to load</param>
        public abstract void LoadAge(string ageName);

        /// <summary>
        /// Loads pages from an Age into the active scene.
        /// Note:
        ///     do not try to load two pages of the same Age by calling this function twice. Since Plasma memory is
        ///     freed once the function ends, cross references (ex: textures) will be broken on the second call.
        /// </summary>
        /// <param name="ageName"></param>
        /// <param name="pageNames"></param>
        public abstract void LoadPages(string ageName, string[] pageNames);

        /// <summary>
        /// Reads a Python PAK with given filename from the game's python folder, and store it in memory for future use.
        /// pakName must be a file in the Python folder.
        /// </summary>
        /// <param name="pakName"></param>
        public abstract void ReadPythonPak(string pakName);

        /// <summary>
        /// Reads all Python PAKs from the game's python folder, storing them in memory for future use.
        /// </summary>
        public abstract void ReadPythonPaks();

        /// <summary>
        /// Unloads a previously read Python pak.
        /// </summary>
        /// <param name="pakName"></param>
        public abstract void UnloadPythonPak(string pakName);

        /// <summary>
        /// Returns the code for the given Python script, decompiled from an archive read with ReadPythonPak(s).
        /// (note: due to binding issues, we instead put the code into a <see cref="HackReturnObject"/> to return it)
        /// </summary>
        /// <param name="pythonScriptName"></param>
        /// <param name="pythonScript"></param>
        public abstract void GetPythonScript(string pythonScriptName, HackReturnObject pythonScript);

        /// <summary>
        /// Checks whether the specified script exists in one of the PAKs.
        /// </summary>
        /// <param name="pythonScriptName"></param>
        /// <returns></returns>
        public abstract bool HasPythonScript(string pythonScriptName);

        /// <summary>
        /// Reads an encrypted text file.
        /// Useful for reading fni, sdl, age files, etc.
        /// (note: due to binding issues, we instead put the code into a <see cref="HackReturnObject"/> to return it)
        /// </summary>
        /// <param name="filePath">file path relative to game root folder</param>
        /// <param name="decryptedFile"></param>
        public abstract void ReadEncryptedFile(string filePath, HackReturnObject decryptedFile);
    }
}
