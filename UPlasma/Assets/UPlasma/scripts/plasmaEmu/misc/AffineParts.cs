/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Stores the original translation/rotation/scale of an object, before it was baked into the
    /// CoordinateInterface's matrix.
    /// When animating a single channel, this allows us to rebuild the l2w matrix at runtime without
    /// relying on inexact matrix component extraction.
    /// </summary>
    public class AffineParts
    {
        public int i; // unused
        public Vector3 translation;
        public Quaternion rotation;
        public Quaternion scaleRotation;
        public Vector3 scale;
        public float signOfDeterminant;
    }
}
