/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Simplified version of a Plasma plKey.
    /// I expect it to be mostly useful in networking, since otherwise we can reference
    /// the UnityEngine.Object directly...
    /// </summary>
    public class PlKey
    {
        public int agePrefix;
        public int pageId;
        public short classType;
        public string objName;
        public Object obj;

        /// <summary>
        /// Overloaded equality operator to compare object location (age prefix and page id) and name
        /// instead of PlKey reference.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(PlKey a, PlKey b)
        {
            return a.agePrefix == b.agePrefix
                && a.pageId == b.pageId
                && a.classType == b.classType
                && a.objName == b.objName;
        }
        public static bool operator !=(PlKey a, PlKey b) => !(a == b);
        
        public override bool Equals(object obj)
        {
            var key = obj as PlKey;
            return key != null &&
                   agePrefix == key.agePrefix &&
                   pageId == key.pageId &&
                   classType == key.classType &&
                   objName == key.objName;
        }

        public override int GetHashCode()
        {
            var hashCode = 1700977971;
            hashCode = hashCode * -1521134295 + agePrefix.GetHashCode();
            hashCode = hashCode * -1521134295 + pageId.GetHashCode();
            hashCode = hashCode * -1521134295 + classType.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(objName);
            return hashCode;
        }
    }
}
