/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlAgeInfoStruct
    {
        [Flags]
        public enum AgeInfoFlags
        {
            HasAgeFilename = 0x1,
            HasAgeInstanceName = 0x2,
            HasAgeInstanceGuid = 0x4,
            HasAgeUserDefinedName = 0x8,
            HasAgeSequenceNumber = 0x10,
            HasAgeDescription = 0x20,
            HasAgeLanguage = 0x40
        };
        
        public AgeInfoFlags flags;
        public int FlagsInt
        {
            get => (int)flags;
            set => flags = (AgeInfoFlags)value;
        }
        public string ageFilename, ageInstanceName;
        public PlUuid ageInstanceGuid;
        public string ageUserDefinedName, ageDescription;
        public int ageSequenceNumber, ageLanguage;
    }
    
    [Serializable]
    public class PlSpawnPointInfo
    {
        public enum SpawnPointFlags
        {
            HasTitle,
            HasSpawnPt,
            HasCameraStack
        };

        public string title;
        public string spawnPt;
        public string cameraStack;
    }
    
    [Serializable]
    public class PlAgeLinkStruct
    {
        public enum LinkFlags
        {
            HasAgeInfo = 0x1,
            HasLinkingRules = 0x2,
            HasSpawnPt_DEAD = 0x4,
            HasSpawnPt_DEAD2 = 0x8,
            HasAmCCR = 0x10,
            HasSpawnPt = 0x20,
            HasParentAgeFilename = 0x40
        };
        
        public enum LinkingRules
        {
            /// <summary>
            /// Link to public age; Don't remember this link in KI/Vault
            /// </summary>
            BasicLink,
            /// <summary>
            /// Link and create a book in the AgesIOwn folder
            /// </summary>
            OriginalBook,
            /// <summary>
            /// Link to a sub age of current age
            /// </summary>
            SubAgeBook,
            /// <summary>
            /// Link using info from my AgesIOwn folder
            /// </summary>
            OwnedBook,
            /// <summary>
            /// Link using info from my AgesICanVisit folder
            /// </summary>
            VisitBook,
            /// <summary>
            /// Link to a child age of current age
            /// </summary>
            ChildAgeBook
        };
        
        //#pragma warning disable 414
        ///** Textual name lookup for linking rule types */
        //static string[] LinkingRuleNames = new string[] {
        //    "kBasicLink",
        //    "kOriginalBook",
        //    "kSubAgeBook",
        //    "kOwnedBook",
        //    "kVisitBook",
        //    "kChildAgeBook"
        //};
        //#pragma warning restore 414
        
        public LinkFlags flags;
        public int FlagsInt
        {
            get => (int)flags;
            set => flags = (LinkFlags)value;
        }
        public LinkingRules linkingRules;
        public int LinkingRulesInt
        {
            get => (int)linkingRules;
            set => linkingRules = (LinkingRules)value;
        }
        public int amCCR;
        public PlAgeInfoStruct ageInfo;
        public PlSpawnPointInfo spawnPoint;
        public string parentAgeFilename;
    }
}
