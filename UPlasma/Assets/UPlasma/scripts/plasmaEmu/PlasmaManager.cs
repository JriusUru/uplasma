/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using System.IO;
using UnityEngine;
using NativeScript;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Handles most of the Plasma-specific features (Age loading, reading Plasma data, and so on).
    /// Note: it's recommended to shutdown the manager before managing mods, so load order and
    /// Python/SDL updates are registered in correct order.
    /// </summary>
    public class PlasmaManager : MonoBehaviour
    {
        public static PlasmaManager Instance { get; protected set; }

        /// <summary>
        /// State Descriptions for the game. Indexed by statedesc name (not file name).
        /// </summary>
        [HideInInspector]
        public Dictionary<string, PlSDL> sdls = new Dictionary<string, PlSDL>();
        
        // native bindings properties...
		public int MemorySize = 1024 * 1024 * 16;
		// Reloading requires dynamic loading of the C++ plugin, which is only
		// available in the editor
#if UNITY_EDITOR
            public bool AutoReload;
            
            public float AutoReloadPollTime = 1.0f;
            float lastAutoReloadPollTime;
            Coroutine autoReloadCoroutine;
#endif

        /// <summary>
        /// The c++-written component, along with all its helper methods to access the plResMgr.
        /// Other scripts should almost never need to interact with it directly (hopefully).
        /// </summary>
        AbstractBasePlasmaNativeManager native;

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            
            // initialize the Plasma mod manager...
            gameObject.AddComponent<PlasmaModManager>();
            PlasmaModManager.Instance.RefreshAvailableMods();
        }
        
        /// <summary>
        /// Loads all SDLs from the game folders. This must be done before loading actual Ages.
        /// </summary>
        void LoadAllSDLs()
        {
            foreach (string fileName in PlGameFileAccessor.Instance.GetFiles("sdl"))
                if (fileName.ToLower().EndsWith(".sdl"))
                    ReadSDLDesc(fileName);
            print(sdls.Count.ToString() + " sdls loaded. Restoring values...");
            foreach (PlSDL sdl in sdls.Values)
                ProgressManager.Instance.LoadSDLValues(sdl);
            print("Done restoring sdl values.");
        }

        /// <summary>
        /// Reads the specified sdl from the SDL folder (using the <see cref="PlGameFileAccessor"/>)
        /// </summary>
        /// <param name="fileName"></param>
        void ReadSDLDesc(string fileName)
        {
            HackReturnObject retObj = new HackReturnObject();
            native.ReadEncryptedFile("sdl/" + fileName, retObj);
            string fileContent = (string)retObj.val;
            try {
                foreach (PlSDL sdl in PlSDL.GetSDLsFromString(fileContent, true)) // get the sdl records in this file, ignore previous versions.
                    sdls[sdl.name] = sdl;
            }
            catch (System.Exception) {
                Debug.LogError("Error reading file " + fileName);
                throw;
            }
        }

#if UNITY_EDITOR
        void Update()
        {
            if (native != null)
            {
                Bindings.Update();

                if (AutoReload)
                {
                    if (AutoReloadPollTime > 0)
                    {
                        // Not started yet. Start.
                        if (autoReloadCoroutine == null)
                        {
                            lastAutoReloadPollTime = AutoReloadPollTime;
                            autoReloadCoroutine = StartCoroutine(
                                Bindings.AutoReload(
                                    AutoReloadPollTime));
                        }
                        // Poll time changed. Restart.
                        else if (AutoReloadPollTime != lastAutoReloadPollTime)
                        {
                            StopCoroutine(autoReloadCoroutine);
                            lastAutoReloadPollTime = AutoReloadPollTime;
                            autoReloadCoroutine = StartCoroutine(
                                Bindings.AutoReload(
                                    AutoReloadPollTime));
                        }
                    }
                }
                else
                {
                    // Not stopped yet. Stop.
                    if (autoReloadCoroutine != null)
                    {
                        StopCoroutine(autoReloadCoroutine);
                        autoReloadCoroutine = null;
                    }
                }
            }
        }
#endif

        void OnDestroy()
		{
            if (Instance == this)
                Instance = null;
            Shutdown();
		}

        /// <summary>
        /// Opens the native bindings, read Python/SDLs and gets ready for loading some sweet Plamza schtuff.
        /// </summary>
        /// <exception cref="AlreadyInitializedException"></exception>
        /// <exception cref="MissingGameSetupException"></exception>
        public void Initialize()
        {
            // check whether we're already initialized
            if (native != null)
                throw new AlreadyInitializedException("Engine can't be initialized twice.");
            
            // find the game setup. If it's not valid don't initialize
            PlasmaConfig.GameSetup gs = PlasmaConfig.Instance.GetActiveGameSetup();
            if (gs == null)
                throw new MissingGameSetupException("No Plasma game setup defined, or is invalid ?");
            
            gameObject.AddComponent<PlGameFileAccessor>();
            try
            {
                PlasmaModManager.Instance.LoadMods();
            }
            catch
            {
                // some exception when loading mods... cleanup the mod manager
                PlasmaModManager.Instance.UnloadMods();
                Destroy(gameObject.GetComponent<PlGameFileAccessor>());
                // and rethrow the exception
                throw;
            }
            
            // open the bindings
            #if UNITY_EDITOR
                lastAutoReloadPollTime = AutoReloadPollTime;
            #endif
			Bindings.Open(MemorySize);
            
            // the plasma native component is added when opening the bindings, so fetch it
            native = GetComponent<AbstractBasePlasmaNativeManager>();
            
            // now that the native interface is ready, add all Plasma sub components
            gameObject.AddComponent<PlConsole>();
            gameObject.AddComponent<PlGameFileAccessor>();
            gameObject.AddComponent<PythonManager>();
            
            // set the various paths
            string persistencePath = GameManager.Instance.userDataPath;
            string systemDataPath = GameManager.Instance.streamingAssetsPath;
            foreach (PlasmaGame game in System.Enum.GetValues(typeof(PlasmaGame)))
            {
                // create a cache folder for each supported game
                string cachePath = Path.Combine(persistencePath, "cache", game.ToString());
                Directory.CreateDirectory(cachePath);
            }
            // then set the global cache folder for the native library
            native.SetCachePath(Path.Combine(persistencePath, "cache"));
            string toolsPath = Path.Combine(systemDataPath, "tools");
            Directory.CreateDirectory(toolsPath);
            native.SetToolsPath(toolsPath);
            native.ReadPythonPaks();
            LoadAllSDLs();
        }

        /// <summary>
        /// Closes the native bindings and clear all loaded python/sdl.
        /// </summary>
        public void Shutdown()
        {
            if (native == null)
            {
                print("Engine wasn't initialized");
                return;
            }
            
            sdls.Clear();
            PlasmaModManager.Instance.UnloadMods();
            Destroy(gameObject.GetComponent<PythonManager>());
            Destroy(gameObject.GetComponent<PlGameFileAccessor>());
            Destroy(gameObject.GetComponent<PlConsole>());
            Destroy(native);
            Bindings.Close();
        }
        
        /// <summary>
        /// Attempts to detect the game type of the given Plasma folder, from the file names it contains.
        /// </summary>
        /// <param name="gameFolder"></param>
        /// <returns></returns>
        /// <exception cref="UnknownPlasmaGameException"></exception>
        public static PlasmaGame DetectGameType(string gameFolder)
        {
            if (File.Exists(Path.Combine(gameFolder, "UruExplorer.exe")))
            {
                // one of the many versions of uru
                if (File.Exists(Path.Combine(gameFolder, "PhysXLoader.dll")))
                    // only moul uses physx
                    return PlasmaGame.MystOnline;
                else if (File.Exists(Path.Combine(gameFolder, "cypython22.dll")) && File.Exists(Path.Combine(gameFolder, "sp.dll")))
                    // Probably a Complete Chronicles/PotS install... ABM might contain those too, though.
                    return PlasmaGame.CompleteChronicles;
                else
                    throw new UnknownPlasmaGameException($"Detected an Uru game but unable to tell its version at path {gameFolder}");
            }
            else if (File.Exists(Path.Combine(gameFolder, "eoa.exe")))
                return PlasmaGame.EndOfAges;
            else if (File.Exists(Path.Combine(gameFolder, "ct.exe")))
                return PlasmaGame.CrowThistle;

            throw new UnknownPlasmaGameException($"Simply couldn't detect which game is at path {gameFolder}");
        }
        
        ///// <summary>
        ///// Reads the given Python pak file.
        ///// </summary>
        ///// <param name="fileName"></param>
        //public void ReadPythonPak(string fileName)
        //{
        //    native.ReadPythonPak(fileName);
        //}
        
        /// <summary>
        /// Returns the decompiled code of the Python script with the given name.
        /// (uses Plasma's own PAK override mechanism to find the correct version
        /// of the script).
        /// </summary>
        /// <param name="pythonScriptName"></param>
        /// <returns></returns>
        public string GetPythonScript(string pythonScriptName)
        {
            HackReturnObject retObj = new HackReturnObject();
            native.GetPythonScript(pythonScriptName, retObj);
            return (string)retObj.val;
        }

        /// <summary>
        /// Returns whether the Python script with the given name exists in one of the PAK files.
        /// </summary>
        /// <param name="pythonScriptName"></param>
        /// <returns></returns>
        public bool HasPythonScript(string pythonScriptName)
        {
            return native.HasPythonScript(pythonScriptName);
        }
        
        /// <summary>
        /// Returns whether the Age with the given name exists in the game installs.
        /// </summary>
        /// <param name="ageName"></param>
        /// <returns></returns>
        public bool HasAge(string ageName)
        {
            return PlGameFileAccessor.Instance.FileExists(Path.Combine("dat", ageName + ".age"));
        }
        
        /// <summary>
        /// Loads an Age from its filename.
        /// </summary>
        /// <param name="ageName"></param>
        public void LoadAge(string ageName)
        {
            native.LoadAge(ageName);
        }

        /// <summary>
        /// Loads only a few pages from the given Age.
        /// </summary>
        /// <param name="ageName"></param>
        /// <param name="pageNames"></param>
        public void LoadPages(string ageName, string[] pageNames)
        {
            native.LoadPages(ageName, pageNames);
        }

        // public void ReadFni(string fniName);

        /// <summary>
        /// Returns the SDL record for the specified or current Age.
        /// </summary>
        /// <param name="ageName">Name of the Age SDL to get, or null to get the current</param>
        /// <returns></returns>
        public PlSDL GetAgeSDL(string ageName=null)
        {
            if (string.IsNullOrEmpty(ageName))
                return sdls[GameManager.Instance.CurrentAge];
            return sdls[ageName];
        }
    }
}
