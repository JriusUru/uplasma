/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


namespace UPlasma.PlEmu
{
    /// <summary>
    /// Represents a Plasma PRP object which can receive <see cref="PlMessage"/>.
    /// </summary>
    public interface IPlMessageable
    {
        /// <summary>
        /// Called when another object sends a <see cref="PlMessage"/> to this object.
        /// Note that the message might *not* be intended to be processed by this object,
        /// and may have lost its way to here due to Plamza being a bit too flexible when
        /// it comes to messaging.
        /// If it looks like this message is meant to be processed by this object, the implementation
        /// should return true. If it's clearly not meant for this object, simply return false.
        /// If it looks like this message is meant for this object, but it's impossible to carry
        /// out the execution, it might be a good idea to throw an exception or display a warning
        /// message to ease debugging - whichever you prefer.
        /// </summary>
        /// <param name="msg">the message to process</param>
        /// <returns>whether the message was handled by the receiver</returns>
        bool MsgReceive(PlMessage msg);
    }
}
