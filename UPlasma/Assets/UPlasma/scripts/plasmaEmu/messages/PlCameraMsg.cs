/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation,
either version 3 of the License,
or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not,
see <https://www.gnu.org/licenses/>
*/


using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlCameraMsg : PlMessage
    {
        public enum ModCmds
        {
            SetSubject,
            CameraMod,
            SetAsPrimary,
            TransitionTo,
            Push,
            Pop,
            Entering,
            Cut,
            ResetOnEnter,
            ResetOnExit,
            ChangeParams,
            Worldspace,
            CreateNewDefaultCam,
            RegionPushCamera,
            RegionPopCamera,
            RegionPushPOA,
            RegionPopPOA,
            FollowLocalPlayer,
            ResponderTrigger,
            SetFOV,
            AddFOVKeyFrame,
            StartZoomIn,
            StartZoomOut,
            StopZoom,
            SetAnimated,
            PythonOverridePush,
            PythonOverridePop,
            PythonOverridePushCut,
            PythonSetFirstPersonOverrideEnable,
            PythonUndoFirstPerson,
            UpdateCameras,
            ResponderSetThirdPerson,
            ResponderUndoThirdPerson,
            NonPhysOn,
            NonPhysOff,
            ResetPanning
        };

        public PlCameraModifier newCam;
        public Component triggerer; // usually null
        public float transTime;
        public PlCameraConfig config;
        public bool activated;
        public List<ModCmds> commands = new List<ModCmds>();
        public List<int> CommandsInt
        {
            get => commands.Select(x => (int)x).ToList();
            set => commands = value.Select(x => (ModCmds)x).ToList();
        }
    }

    public class PlCameraConfig
    {
        public Vector3 offset;
        public float accel;
        public float decel;
        public float vel;
        public float FPAccel;
        public float FPDecel;
        public float FPVel;
        public float FOVw;
        public float FOVh;
        public bool worldspace;

    }
}
