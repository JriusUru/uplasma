/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlEventCallbackMsg : PlMessage
    {
        public float eventTime;
        public CallbackEvent eventId;
        public int EventIdInt
        {
            get => (int)eventId;
            set => eventId = (CallbackEvent)value;
        }
        public int index;
        public int repeats;
        public int user;
    }
}
