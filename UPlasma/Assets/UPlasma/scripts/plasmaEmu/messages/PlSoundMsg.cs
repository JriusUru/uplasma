/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlSoundMsg : PlMessageWithCallbacks
    {
        public enum ModCmds
        {
            Play,
            Stop,
            SetLooping,
            UnSetLooping,
            SetBegin,
            ToggleState,
            AddCallbacks,
            RemoveCallbacks,
            GetStatus,
            NumSounds,
            StatusReply,
            GoToTime,
            SetVolume,
            SetTalkIcon,
            ClearTalkIcon,
            SetFadeIn,
            SetFadeOut,
            IsLocalOnly,
            SelectFromGroup,
            NumCmds,
            FastForwardPlay,
            FastForwardToggle
        };

        public enum FadeType
        {
            Linear,
            Logarithmic,
            Exponential
        };
        
        public List<ModCmds> commands;
        public List<int> CommandsInt
        {
            get => commands.Select(x => (int)x).ToList();
            set => commands = value.Select(x => (ModCmds)x).ToList();
        }
        public float begin;
        public float end;
        public bool loop;
        public bool playing;
        public float speed;
        public float time;
        public int index;
        public int repeats;
        public int nameStr;
        public float volume;
        public FadeType fadeType;
        public int FadeTypeInt
        {
            get => (int)fadeType;
            set => fadeType = (FadeType)value;
        }
    }
}
