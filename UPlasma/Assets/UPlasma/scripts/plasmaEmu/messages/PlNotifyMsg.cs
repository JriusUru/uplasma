/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlNotifyMsg : PlMessage
    {
        public enum NotificationType
        {
            Activator,
            VarNotification,
            NotifySelf,
            ResponderFF,
            ResponderChangeState
        };

        public NotificationType notifyType;
        public int NotifyTypeInt
        {
            get => (int)notifyType;
            set => notifyType = (NotificationType)value;
        }
        public float notifyState;
        public int notifyId;
        public List<PlProEventData> notifyEvents = new List<PlProEventData>();
    }
}
