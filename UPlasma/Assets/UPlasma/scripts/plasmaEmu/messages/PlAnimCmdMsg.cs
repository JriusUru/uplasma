/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlAnimCmdMsg : PlMessageWithCallbacks
    {
        public enum ModCmds
        {
            Continue,
            Stop,
            SetLooping,
            UnSetLooping,
            SetBegin,
            SetEnd,
            SetLoopEnd,
            SetLoopBegin,
            SetSpeed,
            GoToTime,
            SetBackwards,
            SetForwards,
            ToggleState,
            AddCallbacks,
            RemoveCallbacks,
            GoToBegin,
            GoToEnd,
            GoToLoopBegin,
            GoToLoopEnd,
            IncrementForward,
            IncrementBackward,
            RunForward,
            RunBackward,
            PlayToTime,
            PlayToPercentage,
            FastForward,
            GoToPercent,
            NumCmds
        };
        
        public string animName;
        public string loopName;
        public float begin;
        public float end;
        public float loopBegin;
        public float loopEnd;
        public float speed;
        public float speedChangeRate;
        public float time;
        public List<ModCmds> commands;
        public List<int> CommandsInt
        {
            get => commands.Select(x => (int)x).ToList();
            set => commands = value.Select(x => (ModCmds)x).ToList();
        }
    }
}
