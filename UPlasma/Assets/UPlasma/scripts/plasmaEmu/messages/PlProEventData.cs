/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using UnityEngine;

namespace UPlasma.PlEmu
{
    [Serializable]
    public class PlProEventData
    {
        public enum EventType
        {
            Collision = 1,
            Picked,
            ControlKey,
            Variable,
            Facing,
            Contained,
            Activate,
            Callback,
            ResponderState,
            MultiStage,
            Spawned,
            ClickDrag,
            Coop,
            OfferLinkBook,
            Book,
            ClimbingBlockerHit,
            None
        };

        public enum DataType
        {
            Number,
            Key,
            Notta
        };

        public enum MultiStageEventType
        {
            EnterStage = 1,
            BeginningOfLoop,
            AdvanceNextStage,
            RegressPrevStage,
            Nothing
        };

        public EventType eventType = EventType.None;
    }

    [Serializable]
    public class PlProPickedEventData : PlProEventData
    {
        public GameObject picker;
        public GameObject picked;
        public bool enabled;
        public Vector3 hitPoint;
    }

    // Aaaaaand plenty more where that came from. Will have to reimplement those...
}
