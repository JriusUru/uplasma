/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using UnityEngine;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Standard Plamza message between two components.
    /// </summary>
    [Serializable]
    public abstract class PlMessage
    {
        /// <summary>
        /// Copied almost verbatim from Plasma's source.
        /// (tip: most of the time, you'll want LocalPropagate and NetPropagate for PRPs,
        /// other flags are used either internally or not at all.
        /// In the future, we might use it more internally...)
        /// </summary>
        [Flags]
        public enum BCastFlags
        {
            BCastNone = 0x0,
            /// <summary>
            /// To everyone registered for this msg type or msgs this is derived from
            /// </summary>
            BCastByType = 0x1,
            /// <summary>
            /// Obsolete option (never used). Was BCastBySender
            /// </summary>
            BCastUNUSED_0 = 0x2,
            /// <summary>
            /// Propagate down through SceneObject heirarchy
            /// </summary>
            PropagateToChildren = 0x4,
            /// <summary>
            /// To everyone registered for this exact msg type.
            /// </summary>
            BCastByExactType = 0x8,
            /// <summary>
            /// Send the msg to an object and all its modifier
            /// </summary>
            PropagateToModifiers = 0x10,
            /// <summary>
            /// Clear registration for this type after sending this msg
            /// </summary>
            ClearAfterBCast = 0x20,
            /// <summary>
            /// Propagate this message over the network (remotely)
            /// </summary>
            NetPropagate = 0x40,
            /// <summary>
            /// Internal use-This msg has been sent over the network
            /// </summary>
            NetSent = 0x80,
            /// <summary>
            /// Used along with NetPropagate to filter the msg bcast using relevance regions
            /// </summary>
            NetUseRelevanceRegions = 0x100,
            /// <summary>
            /// Used along with NetPropagate to force the msg to go out (ie. ignore cascading rules)
            /// </summary>
            NetForce = 0x200,
            /// <summary>
            /// Internal use-This msg came in over the network (remote msg)
            /// </summary>
            NetNonLocal = 0x400,
            /// <summary>
            /// Propagate this message locally (ON BY DEFAULT)
            /// </summary>
            LocalPropagate = 0x800,
            /// <summary>
            /// This msg is a non-deterministic response to another msg
            /// </summary>
            NetNonDeterministic = NetForce,
            /// <summary>
            /// Debug only - will break in dispatch before sending this msg
            /// </summary>
            MsgWatch = 0x1000,
            /// <summary>
            /// Internal use-msg is non-local and initiates a cascade of net msgs. This bit is not inherited or computed, it's a property.
            /// </summary>
            NetStartCascade = 0x2000,
            /// <summary>
            /// If rcvr is online but not in current age, they will receive msg courtesy of pls routing.
            /// </summary>
            NetAllowInterAge = 0x4000,
            /// <summary>
            /// Don't use reliable send when net propagating
            /// </summary>
            NetSendUnreliable = 0x8000,
            /// <summary>
            /// CCRs can send a plMessage to all online players.
            /// </summary>
            CCRSendToAllPlayers = 0x10000,
            /// <summary>
            /// kNetSent and kNetNonLocal are inherited by child messages sent off while processing a net-propped
            /// parent. This flag ONLY gets sent on the actual message that went across the wire.
            /// </summary>
            NetCreatedRemotely = 0x20000,
        };
        
        public Component sender;
        public List<IPlMessageable> receivers = new List<IPlMessageable>();
        public BCastFlags castFlags = BCastFlags.LocalPropagate;
        public int CastFlagsInt
        {
            get => (int)castFlags;
            set => castFlags = (BCastFlags)value;
        }

        /// <summary>
        /// Sends the message, either online or locally.
        /// </summary>
        public void Send()
        {
            if (castFlags == BCastFlags.NetPropagate)
            {
                // TODO - reroute this message online
                // (so, does this mean the server will have to ignore the sender client when rerouting the message ?
                // this would mean less local lag, but at the same time why would we bother...)
                // (we'll see later, when multiplayer is a thing)
            }
            Execute();
        }

        /// <summary>
        /// Dispatches the message to local receivers.
        /// </summary>
        void Execute()
        {
            //string debugMsg = $"MESSAGE: {GetType()}, {castFlags}\n";
            foreach (IPlMessageable receiver in receivers)
            {
                if (receiver != null)
                {
                    if (castFlags.HasFlag(BCastFlags.MsgWatch))
                        // not a "true" break, but will pause the editor (at the end of the current frame)
                        // you can obviously place a breakpoint on this line too if you prefer :)
                        Debug.Break();

                    BCastFlags allUnsupportedFlags = ~(
                        BCastFlags.LocalPropagate |
                        BCastFlags.NetPropagate |
                        BCastFlags.MsgWatch
                    );
                    if ((castFlags & allUnsupportedFlags) != 0)
                        throw new NotImplementedException("Unsupported cast flag: " + castFlags);

                    //debugMsg += receiver.ToString();
                    //bool processed =
                        receiver.MsgReceive(this);
                    //if (!processed)
                    //    Debug.LogWarning($"Unprocessed message: {this} from {sender} to {receiver}");
                }
            }
            // if (this.GetType() != typeof(PlInternalDetectorMessage))
                // Debug.Log(debugMsg);
        }
    }
}
