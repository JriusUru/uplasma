/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using MIConvexHull;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Simple class used as workaround for some methods crashing when called from the native library
    /// (or simply being overall extremely inconvenient to call from a C++ environment)
    /// </summary>
    public class NativeHelpers
    {
        /// unity returns non null fake object in editor
        public static bool HasComponent<T>(GameObject obj) => (obj.GetComponent<T>() as Component) != null; // "as component" REQUIRED for Unity's crappy null check.
        public static bool HasComponentInParent<T>(GameObject obj) => (obj.GetComponentInParent<T>() as Component) != null; // "as component" REQUIRED for Unity's crappy null check.

        /// casting doesn't work, use helper function
        public static T CastObjTo<T>(object obj)
        {
            try
            {
                return (T)obj;
            }
            catch (Exception)
            {
                if (obj == null)
                    Debug.LogError($"Cannot cast null to {typeof(T)}");
                else
                    Debug.LogError($"Cannot cast {obj} from {obj.GetType()} to {typeof(T)}");
                Debug.Break();
                throw;
            }
        }

        /// useful when debugging handle leaks
        public static void DebugType(object obj)
        {
            if (obj != null)
                Debug.Log("DT: " + obj.GetType() + " --- " + obj);
            else
                Debug.Log("null");
        }

        /// Simplification so we don't have to deal with all those managed types in the native lib
        public static void CreateFacesForHull(Mesh mesh)
        {
            // load the vertices
            List<DefaultVertex> data = new List<DefaultVertex>();
            foreach (Vector3 v in mesh.vertices)
            {
                DefaultVertex miv = new DefaultVertex();
                miv.Position = new double[] { v.x, v.y, v.z };
                data.Add(miv);
            }
            // create the faces
            try {
                ConvexHull<DefaultVertex, DefaultConvexFace<DefaultVertex>> hull = ConvexHull.Create<
                    DefaultVertex,
                    DefaultConvexFace<DefaultVertex>>
                    (data);
                
                // compute the number of points & faces
                int numPoints = 0;
                int numFaces = 0;
                foreach (DefaultVertex hpoint in hull.Points)
                    numPoints++;
                foreach (DefaultConvexFace<DefaultVertex> hface in hull.Faces)
                    numFaces++;
                
                // copy the vertices
                // MICH faces use points directly instead of indices, so keep track of point indices too
                Vector3[] uVerts = new Vector3[numPoints];
                DefaultVertex[] hVerts = new DefaultVertex[numPoints]; // used to find the id of a face vertex
                int i=0;
                foreach (DefaultVertex hpoint in hull.Points)
                {
                    uVerts[i] = new Vector3((float)hpoint.Position[0], (float)hpoint.Position[1], (float)hpoint.Position[2]);
                    hVerts[i] = hpoint;
                    i++;
                }
                
                // copy the faces
                int[] uIndices = new int[3*numFaces];
                i=0;
                foreach (DefaultConvexFace<DefaultVertex> hface in hull.Faces)
                {
                    uIndices[i*3] = Array.IndexOf(hVerts, hface.Vertices[0]);
                    uIndices[i*3+1] = Array.IndexOf(hVerts, hface.Vertices[1]);
                    uIndices[i*3+2] = Array.IndexOf(hVerts, hface.Vertices[2]);
                    i++;
                }
                
                mesh.vertices = uVerts;
                mesh.triangles = uIndices;
            }
            catch (System.Exception)
            {
                Debug.LogError("Failed to create convex hull from mesh " + mesh.name + " !");
            }
        }

        /// can't add this to system.object...
        public static string ToString(object obj) => obj.ToString();

        /// returns the layer id and true if the object's layer is overridden, false otherwise
        public static bool GetObjOverrideLayer(PrpImportClues clues, string objName, out int layer)
        {
            foreach (int lyrId in clues.layers.Keys)
            {
                if (clues.layers[lyrId].Contains(objName))
                {
                    layer = lyrId;
                    return true;
                }
            }
            layer = -1;
            return false;
        }

        public static bool IsInt(ref object thing) => thing.GetType() == typeof(int);
        public static void IsInt(ref object thing, out bool result) => result = thing.GetType() == typeof(int);
        public static bool IsSameType(ref object thingA, ref object thingB) => thingA.GetType() == thingB.GetType();

        /// int32s cast to system.object aren't ref counted (handle loss)
        public static void AddIntToObjList(List<object> l, int i) => l.Add((object)i);

        /// bools cast to system.object aren't ref counted (handle loss)
        public static void AddBoolToObjList(List<object> l, bool b) => l.Add((object)b);

        // ↓ nested generics are just a pain to deal with anyway...
        public static void SetLeafControllerKeyframe(Point3Controller controller, Point3Key keyframe, int index)
            => controller.Keyframes[index] = keyframe;
        public static void SetLeafControllerKeyframe(ScalarController controller, ScalarKey keyframe, int index)
            => controller.Keyframes[index] = keyframe;
        public static void SetLeafControllerKeyframe(QuatController controller, QuatKey keyframe, int index)
            => controller.Keyframes[index] = keyframe;

        // same as above
        public static void SetLeafControllerNumKeyframe(Point3Controller controller, int numKeyframes)
            => controller.Keyframes = new Point3Key[numKeyframes];
        public static void SetLeafControllerNumKeyframe(ScalarController controller, int numKeyframes)
            => controller.Keyframes = new ScalarKey[numKeyframes];
        public static void SetLeafControllerNumKeyframe(QuatController controller, int numKeyframes)
            => controller.Keyframes = new QuatKey[numKeyframes];


        #region old chtuff that should no longer be needed since rvo workaround
        ///// seriously ? even returning enums fails ?
        //public static void GetFileGameType(string file, out PlasmaGame game)
        //{
        //    game = PlGameFileAccessor.Instance.GetFileGameType(file);
        //}
        ///// int
        //public static void GetMatOverrideRenderQueue(MatOverride overrde, out int renderQueue)
        //{ renderQueue = overrde.renderQueue; }
        ///// Normal return of matrix4x4 doesn't work, use out ref
        //public static void GetTransformLocalToWorldMatrix(Transform t, out Matrix4x4 mat)
        //{ mat = t.localToWorldMatrix; }
        //public static void GetTransformWorldToLocalMatrix(Transform t, out Matrix4x4 mat)
        //{ mat = t.worldToLocalMatrix; }
        //public static void MatrixMultiply(Matrix4x4 a, Matrix4x4 b, out Matrix4x4 mat)
        //{ mat = a * b; }
        ///// returning int doesn't work, use out ref
        //public static void GetLayerMaskNameToLayer(string x, out int y)
        //{ y = UnityEngine.LayerMask.NameToLayer(x); }
        //public static void GetCluesListCount(List<PrpImportClues> l, out int c)
        //{ c = l.Count; }
        //public static void GetUnityObjListCount(List<UnityEngine.Object> l, out int c)
        //{ c = l.Count; }
        //public static void GetLightOverrideLayers(LightOverride l, out int i)
        //{ i = l.layers; }
        //public static void GetLightOverrideRange(LightOverride l, out float f)
        //{ f = l.range; }
        //public static void GetLightOverrideIntensity(LightOverride l, out float f)
        //{ f = l.intensity; }
        //public static void GetLightOverrideShadows(LightOverride l, out bool f)
        //{ f = l.shadows; }
        //public static void GetLightOverrideShadowStrength(LightOverride l, out float f)
        //{ f = l.shadowStrength; }
        //public static void GetMeshRendererOverrideCastShadows(MeshRendererOverride mr, out ShadowCastingMode scm)
        //{ scm = mr.castShadows; }
        //public static void Vector3Dot(Vector3 v1, Vector3 v2, out float result)
        //{ result = Vector3.Dot(v1, v2); }
        #endregion
    }
}
