/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// List of all games and game installs supported by the PlEmu.
    /// </summary>
    [Flags]
    public enum PlasmaGame
    {
        /// <summary>
        /// The standard one.
        /// This includes all versions: Steam, GoG, DI, 25th anniversary, etc. They are all
        /// similar enough that we can use them as basis.
        /// </summary>
        CompleteChronicles = 0x1,

        /// <summary>
        /// The online one (duh).
        /// This also includes all Shards based on MOULa.
        /// </summary>
        MystOnline = 0x2,

        /// <summary>
        /// The solo one.
        /// </summary>
        EndOfAges = 0x4,

        /// <summary>
        /// The fantasy one.
        /// </summary>
        CrowThistle = 0x8,

        /* unsupported ones:
        untilUru,
        agesBeyondMyst,
        toDni,
        There usually arent any reasons to play those, aside from some very minute details
        which we will assume are merged into the main CompleteChronicles one...

        About realMyst: PlasmaV1 differs way too much from PlasmaV2. We'll make a new custom
        emulator for that one (assuming it's reverse engineered one day...)
        // */
    }
}
