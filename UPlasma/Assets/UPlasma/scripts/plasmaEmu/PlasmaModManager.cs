/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

namespace UPlasma.PlEmu
{
    /// <summary>
    /// Manager for mod loading, unloading etc (duh). Plasma version.
    /// 
    /// Plasma mods contain Plasma native data such as prp, sfx, sdl, pak, etc.
    /// Also contains Plasma overrides: textures, anims, etc.
    /// 
    /// Mods are contained in their own folder on the disk. The folder name gives the modId, which is used to keep track of loaded mods, etc
    /// 
    /// Standard hierarchy of a mod:
    ///     modId/                                  # id of the mod
    ///         descriptor.json                     # descriptor (optional)
    ///         dat/                                # Plasma's own folders
    ///         sfx/                                # Plasma's own folders
    ///         overrides/                          # Contains data that modify PRP loader behavior (technically not data Plasma can read though)
    ///             age/                            # Age for which the data will be used
    ///                 page/                       # Page for which the data will be used
    ///                     overrides.json          # PrpImportClues that will give a few basic instructions to the PRP loader to better import the page (notably which shader for which material)
    ///                     textures/               # overrides PRP textures (for texture packs. Neato.)
    ///                     animations/             # overrides PRP animations (because our importer SUKX)
    ///                     physics/                # overrides PRP physics meshes (physics that can't be hulled, etc)
    ///                     meshes/                 # overrides PRP drawable meshes (because why not)
    /// 
    /// The descriptor gives plenty of information that are displayed in the mod manager GUI, allowing for easier mod management.
    /// It's not required though.
    /// 
    /// Mods can be managed through the mod manager GUI (in the main menu). This GUI also allows changing the mod order,
    /// and (not coded at time of writing) even download mods without leaving the game (kinda like an integrated UAM).
    /// It's a pretty neat feature.
    /// However, this makes mod management trickier from the code. Because of the annoying way SDLs and Python paks work,
    /// we have to reinitialize the whole Plasma manager when making changes - hence this feature being only available in the main
    /// menu while the Plasma manager is off.
    /// </summary>
    public class PlasmaModManager : MonoBehaviour
    {
        [System.Serializable]
        public class ModLoadingInfo
        {
            public PlasmaModDescriptor descriptor;
            public string path; // full path (absolute)
            public string modId; // this is actually the mod folder name, which serves as ID
            public bool isSystemMod = false;
        }
        
        public static PlasmaModManager Instance { get; protected set; }

        /// <summary>
        /// List of mods that we know exist
        /// </summary>
        public Dictionary<string, ModLoadingInfo> availableMods = new Dictionary<string, ModLoadingInfo>();

        /// <summary>
        /// Mods that are actually loaded in the PlGameFileAccessor.
        /// Note that this list is in order of activation.
        /// </summary>
        public List<string> currentlyActiveMods = new List<string>();
        
        void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        /// <summary>
        /// Refreshes the list of available mods
        /// </summary>
        public void RefreshAvailableMods()
        {
            List<string> addedMods = new List<string>();
            List<string> removedMods = new List<string>();
            
            Dictionary<string, ModLoadingInfo> previouslyAvailableMods = new Dictionary<string, ModLoadingInfo>(availableMods);
            availableMods.Clear();
            
            // search the system mods folder.
            string systemModsFolderPath = Path.Combine(GameManager.Instance.streamingAssetsPath, "systemMods_plasma");
            if (Directory.Exists(systemModsFolderPath))
            {
                foreach (string dir in Directory.GetDirectories(systemModsFolderPath))
                {
                    string modId = new DirectoryInfo(dir).Name;
                    if (!previouslyAvailableMods.ContainsKey(modId))
                    {
                        // mod is not known yet - add it to the list of mods we know.
                        
                        // load the descriptors
                        string descriptorFileName = Path.Combine(dir, "descriptor.json");
                        ModLoadingInfo info = new ModLoadingInfo();
                        try
                        {
                            if (File.Exists(descriptorFileName))
                                info.descriptor = JsonConvert.DeserializeObject<PlasmaModDescriptor>(File.ReadAllText(descriptorFileName));
                        }
                        catch (JsonException ex) {
                            // not a problem but issue a warning
                            Debug.LogWarning("Could not load mod descriptor" + dir);
                            Debug.LogWarning(ex.Message);
                        }
                        info.path = dir;
                        info.modId = modId;
                        info.isSystemMod = true;
                        
                        availableMods[modId] = info;
                    }
                    else
                    {
                        // mod is already known, no need to do anything.
                        availableMods[modId] = previouslyAvailableMods[modId];
                        // in the future we might update its descriptor ? although that might do more harm than good...
                    }
                }
            }
            // do the same for the user mods folder.
            string modsFolderPath = Path.Combine(GameManager.Instance.userDataPath, "mods_plasma");
            Directory.CreateDirectory(modsFolderPath); // just in case
            foreach (string dir in Directory.GetDirectories(modsFolderPath))
            {
                string modId = new DirectoryInfo(dir).Name;
                if (!previouslyAvailableMods.ContainsKey(modId))
                {
                    // mod is not known yet - add it to the list of mods we know.
                    
                    // load the descriptors
                    string descriptorFileName = Path.Combine(dir, "descriptor.json");
                    ModLoadingInfo info = new ModLoadingInfo();
                    try
                    {
                        if (File.Exists(descriptorFileName))
                            info.descriptor = JsonConvert.DeserializeObject<PlasmaModDescriptor>(File.ReadAllText(descriptorFileName));
                    }
                    catch (JsonException ex) {
                        // not a problem but issue a warning
                        Debug.LogWarning("Could not load mod descriptor" + dir);
                        Debug.LogWarning(ex.Message);
                    }
                    info.path = dir;
                    info.modId = modId;
                    info.isSystemMod = false;
                    
                    if (!GlobalConfig.Instance.mods.knownPlMods.Contains(info.modId))
                    {
                        Debug.LogWarning("New mod found: " + modId);
                        addedMods.Add(modId);
                    }
                    
                    availableMods[modId] = info;
                }
                else
                {
                    // mod is already known, no need to do anything.
                    availableMods[modId] = previouslyAvailableMods[modId];
                }
            }
            
            // register removed mods (don't register those that were previously inactive, we don't care about those...)
            foreach (string modId in GlobalConfig.Instance.mods.activePlMods)
            {
                if (!availableMods.ContainsKey(modId))
                {
                    // aw shit
                    Debug.LogWarning("Mod removed: " + modId);
                    removedMods.Add(modId);
                }
            }
            // force-uninstall removed mods
            foreach (string modId in removedMods)
            {
                // "forget" it by removing it from the list of available mods
                // FIXME - should force return to main menu...
                availableMods.Remove(modId);
                // if (currentlyActiveMods.Contains(modId))
                    // UnloadMod(modId, true);
            }
            
            // warn the user about removed mods
            // warn the user about added mods
            // and when the user said ok to both, UpdateKnownAndActiveModsInConfig()
        }

        /// <summary>
        /// Unloads all mods (duh).
        /// </summary>
        public void UnloadMods()
        {
            foreach (string modId in currentlyActiveMods)
            {
                bool userMod = availableMods[modId].isSystemMod;
                PlasmaConfig.GameFolder gfolder = null;
                if (userMod)
                    gfolder = PlGameFileAccessor.Instance.userMods.Find(o => o.name == modId);
                else
                    gfolder = PlGameFileAccessor.Instance.systemMods.Find(o => o.name == modId);
                
                if (userMod)
                    PlGameFileAccessor.Instance.userMods.Remove(gfolder);
                else
                    PlGameFileAccessor.Instance.systemMods.Remove(gfolder);
            }
        }

        /// <summary>
        /// Loads all mods.
        /// System mods are loaded first.
        /// User mods are loaded second, if they are enabled.
        /// </summary>
        public void LoadMods()
        {
            // load system mods first
            foreach (string modId in availableMods.Keys)
            {
                if (!availableMods[modId].isSystemMod)
                    continue;
                LoadMod(modId);
            }
            // then load user mods
            foreach (string modId in availableMods.Keys)
            {
                if (availableMods[modId].isSystemMod)
                    continue;
                if (GlobalConfig.Instance.mods.activePlMods.Contains(modId))
                    LoadMod(modId);
            }
        }

        /// <summary>
        /// Loads a specific mod. This should only be used by LoadMods, during Plasma manager initialization !
        /// This only adds the mod to the game path, loading the paks and sdls are done automatically during the next step of initialization.
        /// </summary>
        /// <param name="modId"></param>
        void LoadMod(string modId)
        {
            ModLoadingInfo info = availableMods[modId];
            string folderPath = info.path;
            bool userMod = info.isSystemMod;
            PlasmaGame modType = PlasmaGame.CompleteChronicles;
            
            // see if the mod already exists
            if (currentlyActiveMods.Contains(modId))
            {
                string msg = $"The mod with id {modId} is already active.";
                GameManager.Instance.MessageBox(msg);
                throw new ModLoadException(msg);
            }
            
            if (info.descriptor != null)
                modType = info.descriptor.prpVersion;
            else
            {
                // try to auto detect the mod type
                string datFolder = Path.Combine(folderPath, "dat");
                if (Directory.Exists(datFolder))
                {
                    foreach (string file in Directory.GetFiles(datFolder))
                    {
                        string fnameWoPath = Path.GetFileName(file).ToLower();
                        if (fnameWoPath.EndsWith(".age"))
                        {
                            if (fnameWoPath.IndexOf("_District_") != -1)
                                modType = PlasmaGame.CompleteChronicles;
                            else
                                modType = PlasmaGame.MystOnline;
                            break;
                        }
                    }
                }
            }
            
            // add the mod to the user modlist
            PlasmaConfig.GameFolder gfolder = new PlasmaConfig.GameFolder();
            gfolder.name = modId;
            gfolder.path = folderPath;
            gfolder.type = modType;
            if (userMod)
                PlGameFileAccessor.Instance.userMods.Add(gfolder);
            else
                PlGameFileAccessor.Instance.systemMods.Add(gfolder);
        }

        /// <summary>
        /// Syncs the modlist in the config file with the current one.
        /// config.knownPlMods = <see cref="availableMods"/>
        /// config.activePlMods = <see cref="currentlyActiveMods"/> without the system mods
        /// Only call this when all the mods are loaded ! Calling it in the main menu will disable all user mods since they
        /// won't be active yet.
        /// </summary>
        public void UpdateKnownAndActiveModsInConfig()
        {
            GlobalConfig.Instance.mods.knownPlMods.Clear();
            foreach (KeyValuePair<string, ModLoadingInfo> pair in availableMods)
                GlobalConfig.Instance.mods.knownPlMods.Add(pair.Key);
            GlobalConfig.Instance.mods.activePlMods = new List<string>(currentlyActiveMods)
                .FindAll(modId => !availableMods[modId].isSystemMod);
        }
        
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }
    }
}
