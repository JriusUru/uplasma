/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UPlasma.PlEmu;

namespace UPlasma
{
    /// <summary>
    /// Serves as global configuration for the engine. Stores settings, active mods, etc.
    /// </summary>
    [Serializable]
    public class GlobalConfig
    {
        public static GlobalConfig Instance;
        
        [JsonIgnore]
        public string savePath;

        [Serializable]
        public class ModsConfig
        {
            public List<string> knownMods = new List<string>();
            public List<string> knownPlMods = new List<string>();
            public List<string> activeMods = new List<string>();
            public List<string> activePlMods = new List<string>();
        }

        [Serializable]
        public class ConsoleConfig
        {
            public int logMaxLines = 60;
            /// <summary>
            /// Python logging will be printed to both the Python log and the Unity log.
            /// Disabled by default as it clutters the Unity log for nothing.
            /// </summary>
            public bool logPythonToDebugLog = false;
            public PythonManager.PlasmaLoggingLevel pythonLoggingLevel
                = PythonManager.PlasmaLoggingLevel.ErrorLevel; // default for external clients.
            /// <summary>
            /// If true, Python exception are rethrown in the calling C# code. If
            /// false, they are silently ignored (which CAN lead to even more
            /// exceptions down the line...).
            /// </summary>
            public bool rethrowPythonExceptions = true;
        }
        
        public ModsConfig mods = new ModsConfig();
        public ConsoleConfig console = new ConsoleConfig();
        
        public GlobalConfig()
        {
            if (Instance == null)
                Instance = this;
        }
        
        ~GlobalConfig()
        {
            if (Instance == this)
                Instance = null;
        }
        
        public void Save()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            File.WriteAllText(savePath, JsonConvert.SerializeObject(this, settings));
        }
        
        public static GlobalConfig Load(string path)
        {
            GlobalConfig cfg = JsonConvert.DeserializeObject<GlobalConfig>(File.ReadAllText(path));
            cfg.savePath = path;
            return cfg;
        }
    }
}
