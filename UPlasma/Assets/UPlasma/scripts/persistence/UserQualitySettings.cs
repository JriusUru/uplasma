/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System;
using System.IO;
using UnityEngine;
using Newtonsoft.Json;

namespace UPlasma
{
    /// <summary>
    /// Allows overriding some quality settings. Most of the time though quality settings will be handled
    /// by Unity's quality levels.
    /// </summary>
    [Serializable]
    public class UserQualitySettings
    {
        // the defaults roughly match a high quality settings
        [Serializable]
        public class StandardOverrides
        {
            // things that will be configurable in the graphic settings panel
            public AnisotropicFiltering anisotropicFiltering {
                get { return QualitySettings.anisotropicFiltering; }
                set { QualitySettings.anisotropicFiltering = value; }
            }
            public int antiAliasing {
                get { return QualitySettings.antiAliasing; }
                set { QualitySettings.antiAliasing = value; }
            }
            //public BlendWeights blendWeights {
            //    get { return QualitySettings.blendWeights; }
            //    set { QualitySettings.blendWeights = value; }
            //}
            public float lodBias {
                get { return QualitySettings.lodBias; }
                set { QualitySettings.lodBias = value; }
            }
            public int pixelLightCount {
                get { return QualitySettings.pixelLightCount; }
                set { QualitySettings.pixelLightCount = value; }
            }
            public int vSyncCount {
                get { return QualitySettings.vSyncCount; }
                set { QualitySettings.vSyncCount = value; }
            }
            public int masterTextureLimit {
                get { return QualitySettings.masterTextureLimit; }
                set { QualitySettings.masterTextureLimit = value; }
            }
            public ShadowQuality shadows {
                get { return QualitySettings.shadows; }
                set { QualitySettings.shadows = value; }
            }
            public ShadowResolution shadowResolution {
                get { return QualitySettings.shadowResolution; }
                set { QualitySettings.shadowResolution = value; }
            }
            public float shadowDistance {
                get { return QualitySettings.shadowDistance; }
                set { QualitySettings.shadowDistance = value; }
            }
            public int shadowCascades {
                get { return QualitySettings.shadowCascades; }
                set { QualitySettings.shadowCascades = value; }
            }
        }
        
        [Serializable]
        public class AdvancedOverrides {
            // things that rarely need to be tweaked and will be left as is
            public bool billboardsFaceCameraPosition {
                get { return QualitySettings.billboardsFaceCameraPosition; }
                set { QualitySettings.billboardsFaceCameraPosition = value; }
            }
            public int maxQueuedFrames {
                get { return QualitySettings.maxQueuedFrames; }
                set { QualitySettings.maxQueuedFrames = value; }
            }
            public int particleRaycastBudget {
                get { return QualitySettings.particleRaycastBudget; }
                set { QualitySettings.particleRaycastBudget = value; }
            }
            public bool realtimeReflectionProbes {
                get { return QualitySettings.realtimeReflectionProbes; }
                set { QualitySettings.realtimeReflectionProbes = value; }
            }
            public float shadowCascade2Split {
                get { return QualitySettings.shadowCascade2Split; }
                set { QualitySettings.shadowCascade2Split = value; }
            }
            public Vector3 shadowCascade4Split {
                get { return QualitySettings.shadowCascade4Split; }
                set { QualitySettings.shadowCascade4Split = value; }
            }
            public ShadowmaskMode shadowmaskMode {
                get { return QualitySettings.shadowmaskMode; }
                set { QualitySettings.shadowmaskMode = value; }
            }
            public float shadowNearPlaneOffset {
                get { return QualitySettings.shadowNearPlaneOffset; }
                set { QualitySettings.shadowNearPlaneOffset = value; }
            }
            public ShadowProjection shadowProjection {
                get { return QualitySettings.shadowProjection; }
                set { QualitySettings.shadowProjection = value; }
            }
            public float resolutionScalingFixedDPIFactor {
                get { return QualitySettings.resolutionScalingFixedDPIFactor; }
                set { QualitySettings.resolutionScalingFixedDPIFactor = value; }
            }
            public int maximumLODLevel {
                get { return QualitySettings.maximumLODLevel; }
                set { QualitySettings.maximumLODLevel = value; }
            }
            public bool softParticles {
                get { return QualitySettings.softParticles; }
                set { QualitySettings.softParticles = value; }
            }
            public bool softVegetation {
                get { return QualitySettings.softVegetation; }
                set { QualitySettings.softVegetation = value; }
            }
            public bool streamingMipmapsActive {
                get { return QualitySettings.streamingMipmapsActive; }
                set { QualitySettings.streamingMipmapsActive = value; }
            }
            public bool streamingMipmapsAddAllCameras {
                get { return QualitySettings.streamingMipmapsAddAllCameras; }
                set { QualitySettings.streamingMipmapsAddAllCameras = value; }
            }
            public int streamingMipmapsMaxFileIORequests {
                get { return QualitySettings.streamingMipmapsMaxFileIORequests; }
                set { QualitySettings.streamingMipmapsMaxFileIORequests = value; }
            }
            public int streamingMipmapsMaxLevelReduction {
                get { return QualitySettings.streamingMipmapsMaxLevelReduction; }
                set { QualitySettings.streamingMipmapsMaxLevelReduction = value; }
            }
            public float streamingMipmapsMemoryBudget {
                get { return QualitySettings.streamingMipmapsMemoryBudget; }
                set { QualitySettings.streamingMipmapsMemoryBudget = value; }
            }
            //public int streamingMipmapsRenderersPerFrame {
            //    get { return QualitySettings.streamingMipmapsRenderersPerFrame; }
            //    set { QualitySettings.streamingMipmapsRenderersPerFrame = value; }
            //}
            public int asyncUploadBufferSize {
                get { return QualitySettings.asyncUploadBufferSize; }
                set { QualitySettings.asyncUploadBufferSize = value; }
            }
            public int asyncUploadTimeSlice {
                get { return QualitySettings.asyncUploadTimeSlice; }
                set { QualitySettings.asyncUploadTimeSlice = value; }
            }
        }
        
        public static UserQualitySettings instance;
        
        [JsonIgnore]
        public string savePath;
        
        public UserQualitySettings()
        {
            if (instance == null)
                instance = this;
        }
        
        ~UserQualitySettings()
        {
            if (instance == this)
                instance = null;
        }
        
        public void Save()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            File.WriteAllText(savePath, JsonConvert.SerializeObject(this, settings));
        }
        
        public static UserQualitySettings Load(string path)
        {
            UserQualitySettings cfg = JsonConvert.DeserializeObject<UserQualitySettings>(File.ReadAllText(path));
            cfg.savePath = path;
            return cfg;
        }
    }
}
