/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;
using Newtonsoft.Json;
using UPlasma.PlEmu;
using System;

namespace UPlasma
{
    /// <summary>
    /// Class handling game bootup, loading etc.
    /// Should always be present, and never duplicated.
    /// 
    /// On awake automatically creates various components like the mod manager, etc. which also initializes most of these components.
    /// The two exception are the Plasma manager and the Mod Manager, which are only partly initialized when in the main menu.
    /// When loading a game (or when debugOptions.ageToLoad is not empty), those two are fully initialized. When returning to the main menu,
    /// those two return to their basic state.
    /// This is required so changing the game setup and active mods/load order isn't too complex.
    /// Concerning the mod manager: "partly initialized" means the mod list is loaded and system mods are activated (since they are
    /// always loaded even when the game isn't started).
    /// </summary>
    public class GameManager : MonoBehaviour
    {
        [Serializable]
        public struct PostProcesses
        {
            public PostProcessVolume main;
            public PostProcessVolume dof;
        }
        
        [Serializable]
        public class DebugOptions
        {
            public string ageToLoad = "Neighborhood";
            public string spawnToPoint = "LinkInPointDefault";
        }
        
        public enum UserDataPathLocation
        {
            /// <summary>
            /// Unity's persistentDataPath. Harder for the user to find, but at least it's always writeable, so good for all platforms.
            /// </summary>
            PersistentData,

            /// <summary>
            /// My Documents/My Games. Not a "recommended" location, but easier for the user to find, and already used by quite
            /// a few Windoz games already, like Bethesda's. Non-windoz platforms should stick to appdata.
            /// </summary>
            DocumentsMyGames,

            /// <summary>
            /// Portable desktop version. Usable only if the streamingAssetsPath is writeable (which isn't always the case).
            /// </summary>
            StreamingAssets,
        }

        public static GameManager Instance { get; protected set; }

        /// <summary>
        /// List of objects to enable on play mode (GUI, etc)
        /// </summary>
        public GameObject[] enableOnPlay;
        public PostProcesses postProcesses;
        public DebugOptions debugOptions;
        
        [HideInInspector]
        public UserDataPathLocation dataLocation;
        [HideInInspector]
        public string streamingAssetsPath;
        [HideInInspector]
        public string userDataPath;
        
        [Range(0.001f, 3)]
        public float pauseSpeed = .15f;
        public bool Paused { get; protected set; }

        /// <summary>
        /// Retrieves the current rendering camera.
        /// Unlike <see cref="Camera.current"/>, this always returns the GAME camera, not the editor's.
        /// Unlike <see cref="Camera.main"/>, this is safe to call every frame.
        /// </summary>
        public Camera MainCamera
        {
            get
            {
                // Implementation detail: right now we only call Camera.main once and cache the result
                // for later. Camera.main is notoriously slow because it uses/used FindObjectWithTag
                // (dunno if this changed since then, but just in case).
                // We might change this once we have a proper camera system (like plCamera).
                // Still, the goal remains for this method to be able to be called as many times per frame
                // as required.
                if (mainCamera == null)
                    mainCamera = Camera.main;
                return mainCamera;
            }
        }

        Camera mainCamera;
        readonly string configCfgFname = "config.json";
        readonly string qualityCfgFname = "quality.json";
        readonly string plasmaCfgFname = "plasma.json";
        // readonly string playerCfgFname = "player.json";
        // readonly string avatarCfgFname = "avatar.json";
        List<string> loadedScenes = new List<string>();

        public string CurrentAge { get; protected set; }

        void Awake ()
        {
            if (Instance != null)
                // a gamemanager already exists ? that means we're just a duplicate of opening a new scene. Silently destroy...
                Destroy(gameObject);
            
            Instance = this;
            transform.parent = null;
            DontDestroyOnLoad(this);
            
            // okay, now we're setup... Sortof.
            
            // find where persistent data is stored...
            FindPersistentDataPath();
            // now load the configs
            LoadConfig();
            
            // initialize the various managers
            // (this also ensures they are initialized in the correct order,
            // more-or-less from least important to most important)
            gameObject.AddComponent<PlayerInputManager>();
            gameObject.AddComponent<AgeLibrary>();
            gameObject.AddComponent<ProgressManager>();
            gameObject.AddComponent<PlasmaManager>();
            gameObject.AddComponent<ModManager>();
            
            // enable play-mode only objects. Most notably, the interfaces, as they get in the way
            // when in edit mode
            foreach (GameObject eop in enableOnPlay)
                eop.SetActive(true);
            
            StartCoroutine(RunInitCo());
        }
        
        IEnumerator RunInitCo()
        {
            // first, start the mod manager itself
            yield return StartCoroutine(InitModManager());
            
            // useful for development: automatically load an Age once the mods are loaded
            if (!string.IsNullOrEmpty(debugOptions.ageToLoad))
            {
                ModManager.Instance.LoadUserMods(null); // TODO callback
                PlasmaManager.Instance.Initialize();
                if (PlasmaManager.Instance.HasAge(debugOptions.ageToLoad))
                {
                    CurrentAge = debugOptions.ageToLoad;
                    PlasmaManager.Instance.LoadAge(debugOptions.ageToLoad);
                    if (!string.IsNullOrEmpty(debugOptions.spawnToPoint))
                        SpawnToPoint(debugOptions.spawnToPoint);
                    // if there is an addon scene, load it now
                    foreach (AgeDescriptor ageDesc in AgeLibrary.Instance.GetAgeAddons(debugOptions.ageToLoad))
                        SceneManager.LoadScene(ageDesc.sceneName, LoadSceneMode.Additive);
                }
                else
                    Debug.LogError("Age " + debugOptions.ageToLoad + " doesn't exist in this install");
            }
        }
        
        IEnumerator InitModManager()
        {
            // refresh available mods
            ModManager.Instance.RefreshAvailableMods();
            // load system mods
            yield return StartCoroutine(ModManager.Instance.LoadSystemMods(
                (b, s) => Debug.Log("Mod activation: " + b + ": " + s) // TODO proper callback
            ));
        }

        /// <summary>
        /// Returns whether the scene is loaded (even if it might not be active).
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        public bool IsSceneLoaded(string sceneName)
        {
            return loadedScenes.Contains(sceneName);
        }
        
        /// <summary>
        /// Helper method.
        /// </summary>
        /// <param name="time"></param>
        /// <param name="action"></param>
        public void AtTimeCallback(float time, Action action)
        {
            StartCoroutine(AtTimeCallbackCo(time, action));
        }
        IEnumerator AtTimeCallbackCo(float time, Action action)
        {
            yield return new WaitForSeconds(time);
            action();
        }
        
        void Update()
        {
            // handle pausing of the game
            if (Paused && postProcesses.dof.weight < 1)
            {
                Time.timeScale = Mathf.Max(Time.timeScale - Time.unscaledDeltaTime / pauseSpeed, 0);
                postProcesses.dof.weight = Mathf.Min(postProcesses.dof.weight + Time.unscaledDeltaTime / pauseSpeed, 1);
            }
            else if (!Paused && postProcesses.dof.weight > 0)
            {
                Time.timeScale = Mathf.Min(Time.timeScale + Time.unscaledDeltaTime / pauseSpeed, 1);
                postProcesses.dof.weight = Mathf.Max(postProcesses.dof.weight - Time.unscaledDeltaTime / pauseSpeed, 0);
            }
        }

        /// <summary>
        /// Displays a messagebox that captures all player input, with the specified text for the buttons (or "ok" if no button specified).
        /// This method will yield until one of the buttons was pressed.
        /// When a button is pressed, the callback method is called with the chosen button (if callback != null).
        /// </summary>
        /// <param name="message"></param>
        /// <param name="buttons"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IEnumerator MessageBox(string message, string[] buttons, System.Action<string> callback)
        {
            print("Displaying MessageBox:\n" + message);
            // TODO
            yield return new WaitForSeconds(5);
            if (callback != null)
            {
                if (buttons != null && buttons.Length != 0)
                    callback(buttons[0]);
            }
        }
        public IEnumerator MessageBox(string message, string[] buttons)
        { yield return StartCoroutine(MessageBox(message, buttons, null)); }
        public IEnumerator MessageBox(string message, System.Action<string> callback)
        { yield return StartCoroutine(MessageBox(message, null, callback)); }
        public IEnumerator MessageBox(string message)
        { yield return StartCoroutine(MessageBox(message, null, null)); }
        
        public void Link(string ageName)
        {
            Link(ageName, "LinkInPointDefault");
        }
        public void Link(string ageName, string spawnPointName)
        {
            // player might be parented to an object (subworlds, etc). Make sure to re-parent it to the DontDestroyOnLoad scene during loading.
            if (PlayerController.Instance != null)
            {
                DontDestroyOnLoad(PlayerController.Instance.gameObject);
            }
            Scene sce = SceneManager.GetSceneByName(ageName);
            if (PlasmaManager.Instance.HasAge(ageName))
            {
                // we need to load a new non-existing scene then import Plasma data into it. SceneManager.CreateScene doesn't unload the existing one,
                // so we have to manually set the active scene, purge assets, etc
                Scene oldScene = SceneManager.GetActiveScene();
                Scene newScene = SceneManager.CreateScene(ageName);
                SceneManager.SetActiveScene(newScene);
                SceneManager.UnloadSceneAsync(oldScene);
                Resources.UnloadUnusedAssets();
                PlasmaManager.Instance.LoadAge(ageName);

                // if there is an addon scene, load it now
                foreach (AgeDescriptor ageDesc in AgeLibrary.Instance.GetAgeAddons(ageName))
                    SceneManager.LoadScene(ageDesc.sceneName, LoadSceneMode.Additive);
            }
            else if (sce.IsValid())
                // regular Unity scene
                SceneManager.LoadScene(ageName);
            else
                throw new UnknownAgeException("No Age named " + ageName);

            CurrentAge = ageName;
            SpawnToPoint(spawnPointName);
        }
        
        public bool SpawnToPoint(string spawnPointName)
        {
            Transform spPoint = null;
            Transform firstFoundSpPoint = null;
            // TODO: one we get around to asynchronously loading Ages, we'll need to wait for loading to end before moving the avatar
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("SpawnPoint"))
            {
                if (firstFoundSpPoint == null)
                    firstFoundSpPoint = obj.transform;
                if (obj.name == spawnPointName)
                {
                    spPoint = obj.transform;
                    break;
                }
            }
            
            bool spWasFound = true;
            if (spPoint == null)
            {
                // no LinkInPointDefault ? Sigh... Just find any link in point.
                spPoint = firstFoundSpPoint;
                spWasFound = false;
            }
            if (spPoint == null)
            {
                // aww come on !
                PlayerController.Instance.transform.SetParent(null);
                PlayerController.Instance.transform.position = Vector3.zero;
                PlayerController.Instance.transform.rotation = Quaternion.identity;
                PlayerController.Instance.transform.localScale = new Vector3(1, 1, 1);
                PlayerController.Instance.velocity = Vector3.zero;
                return false;
            }
            
            PlayerController.Instance.transform.SetParent(null);
            PlayerController.Instance.transform.position = spPoint.transform.position;
            PlayerController.Instance.transform.rotation = spPoint.transform.rotation * Quaternion.Euler(0, 180, 0);
            PlayerController.Instance.transform.localScale = new Vector3(1, 1, 1);
            PlayerController.Instance.velocity = Vector3.zero;
            return spWasFound;
        }
        
        public void Pause(bool doPause)
        {
            bool oldPaused = Paused;
            Paused = doPause;
            
            if (Paused && !oldPaused)
            {
                float maxDofDist = 3;
                // pausing brings some DoF, focused on the object looked at if it's closer than maxDofDist. Compute the DoF distance
                RaycastHit hit;
                Transform camTr = MainCamera.transform;
                // grab the setting
                DepthOfField dofSetting = null;
                foreach (PostProcessEffectSettings setting in postProcesses.dof.sharedProfile.settings)
                    if (setting is DepthOfField)
                        dofSetting = (DepthOfField)setting;
                if (Physics.Raycast(camTr.position, camTr.forward, out hit, maxDofDist))
                    dofSetting.focusDistance.value = hit.distance;
                else
                    dofSetting.focusDistance.value = maxDofDist;
                
                if (PlayerController.Instance != null)
                    PlayerController.Instance.OnGamePause();
            }
            else if (!Paused && oldPaused)
            {
                // exit pause
                if (PlayerController.Instance != null)
                    PlayerController.Instance.OnGameUnpause();
            }
        }

        /// <summary>
        /// Returns to the main menu. Fast skips the fadeout effect.
        /// </summary>
        /// <param name="fast"></param>
        public void GoToMainMenu(bool fast)
        {
            // TODO - actually use fast
            
            // player might be parented to an object (subworlds, etc). Make sure to re-parent it to the DontDestroyOnLoad scene during loading.
            if (PlayerController.Instance != null)
            {
                DontDestroyOnLoad(PlayerController.Instance.gameObject);
                PlayerController.Instance.enabled = false;
            }
            SceneManager.LoadScene("MainMenu");
        }
        
        void OnLoadScene(Scene scene, LoadSceneMode mode)
        {
            loadedScenes.Add(scene.name);
        }
        
        void OnUnloadScene(Scene scene)
        {
            loadedScenes.Remove(scene.name);
        }
        
        void FindPersistentDataPath()
        {
            // get the folder where we'll load additional engine data
            streamingAssetsPath = Application.streamingAssetsPath;

            // In this folder is (should be) a file called dataPath.txt, which tells us where the user would prefer to store his game data.
            // In case this file doesn't exist, let Unity find the default location for this file (usually in some kind of appdata dedicated folder).
            // This ensures we won't screw up trying to write stuff to a non writeable folder.
            dataLocation = UserDataPathLocation.PersistentData;
            
            // now, find whether the user has a preference for a specific save path
            string dataPathFilePath = Path.Combine(Application.streamingAssetsPath, "dataPath.txt"); // stupid variable name
            
            bool userSpecifiedSaveLocation = false;
            if (File.Exists(dataPathFilePath))
            {
                foreach (string line in File.ReadAllLines(dataPathFilePath))
                {
                    if (Enum.TryParse(line, out dataLocation))
                    {
                        // got it
                        userSpecifiedSaveLocation = true;
                        break;
                    }
                }
            }
            if (!userSpecifiedSaveLocation)
            {
                Debug.LogWarning($"You have not specified a location for save files (or incorrectly specified it). Defaulting to {dataLocation}.\n" +
                    $"If you want to override this, open {dataLocation} and write one of the following values: " +
                    $"{string.Join(", ", Enum.GetNames(typeof(UserDataPathLocation)))}");
                
                try
                {
                    File.WriteAllText(dataPathFilePath, dataLocation.ToString());
                }
                catch
                {
                    // folder is not writeable. Fail silently.
                    Debug.Log($"(Not writing the default location to {dataLocation}, as the folder isn't writeable.)");
                }
            }
            
            switch (dataLocation)
            {
                case UserDataPathLocation.PersistentData:
                    userDataPath = Path.Combine(Application.persistentDataPath, "userData");
                    break;
                case UserDataPathLocation.DocumentsMyGames:
                    userDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "My Games");
                    userDataPath = Path.Combine(userDataPath, Application.productName);
                    break;
                case UserDataPathLocation.StreamingAssets:
                    userDataPath = Path.Combine(Application.streamingAssetsPath, "userData");
                    break;
                default:
                    Debug.LogWarning("Unknown config location type " + dataLocation + ", defaulting to Unity's writeable path...");
                    goto case UserDataPathLocation.PersistentData;
            }
            Directory.CreateDirectory(userDataPath);
            Directory.CreateDirectory(Path.Combine(userDataPath, "cache"));
        }
        
        void LoadConfig()
        {
            string configCfgFpath = Path.Combine(userDataPath, configCfgFname);
            string qualityCfgFpath = Path.Combine(userDataPath, qualityCfgFname);
            string plasmaCfgFpath = Path.Combine(userDataPath, plasmaCfgFname);
            
            // load or create the config files
            if (File.Exists(configCfgFpath))
                GlobalConfig.Load(configCfgFpath);
            else
                new GlobalConfig().savePath = configCfgFpath;
            if (File.Exists(qualityCfgFpath))
                UserQualitySettings.Load(qualityCfgFpath);
            else
                new UserQualitySettings().savePath = qualityCfgFpath;
            if (File.Exists(plasmaCfgFpath))
                PlasmaConfig.Load(plasmaCfgFpath);
            else
                new PlasmaConfig().savePath = plasmaCfgFpath;
            
            // save config. This ensures missing fields are re-added.
            GlobalConfig.Instance.Save();
            UserQualitySettings.instance.Save();
            PlasmaConfig.Instance.Save();
        }
        
        void OnDestroy()
        {
            // save the config for the next boot
            GlobalConfig.Instance.Save();
            UserQualitySettings.instance.Save();
            PlasmaConfig.Instance.Save();
            
            if (Instance == this)
                Instance = null;
        }
    }
}
