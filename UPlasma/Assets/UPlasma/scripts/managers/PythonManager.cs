/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IronPython;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using IronPython.Hosting;
using IronPython.Runtime;
using UPlasma.PlEmu;
using System.Text;
using System.IO;

namespace UPlasma
{
    /// <summary>
    /// Class handling creation and setup of the Python engine.
    /// It is later used by PlPythonFileMods to create scripting contexts and run
    /// Python scripts.
    /// The complementary PythonConsole window can be used to keep track of Python logging.
    /// </summary>
    public class PythonManager : MonoBehaviour
    {
        public static PythonManager Instance { get; protected set; }
        public static Dictionary<string, CompiledCode> compiledScripts = new Dictionary<string, CompiledCode>();

        public delegate void LogCallback(string msg, string stackTrace, Object caller, LogType type);
        public static event LogCallback LogMessageReceived;

        /// <summary>
        /// Plasma logging levels. Using a low logging level prints moar data,
        /// using a higher prints less.
        /// Which also means messages printed with a lower level are printed less often.
        /// This is only effective when using PtDebugPrint, regular prints are always printed.
        /// (The actual value used by the engine is stored in the global config class/file.)
        /// </summary>
        public enum PlasmaLoggingLevel
        {
            NoLevel = 0,
            DebugDumpLevel = 1,
            /// <summary>
            /// Internal client level
            /// </summary>
            WarningLevel = 2,
            /// <summary>
            /// External client level (default when using PtDebugPrint without specifying the level)
            /// </summary>
            ErrorLevel = 3,
            AssertLevel = 4
        }

        public ScriptEngine Engine { get; protected set; }
        public dynamic ModuleLoader { get; set; } // set via Python

        StringBuilder logBuilder = new StringBuilder();
        readonly int logWriteEveryXSeconds = 5;
        string logsDir;
        Stack<PlPythonFileMod> activePFMs = new Stack<PlPythonFileMod>();

        void Awake()
        {
            if (Instance != null)
            {
                Debug.LogError("Only a single PythonManager is allowed");
                Destroy(gameObject);
                return;
            }
            Instance = this;
        }

        void Start ()
        {
            Engine = Python.CreateEngine();
            StartCoroutine(WriteLogCoroutine());

            // load the Unity module and the Python bindings
            Engine.Runtime.LoadAssembly(System.Reflection.Assembly.GetAssembly(typeof(GameObject)));
            Engine.Runtime.LoadAssembly(System.Reflection.Assembly.GetAssembly(typeof(PythonManager)));
            
            // setup import path & module loader
            ScriptScope scope = Engine.CreateScope();
            string pyPath = Path.Combine(Application.streamingAssetsPath, "python");
            string pyLibPath = Path.Combine(pyPath, "Lib");
            /*
            NOTE !
            pyLibPath (the default library of IronPython) must NOT be used ! Do not add it to the import path !
            Not only has it some light incompatibility with Cyan's Python scripts (to be expected, no big deal),
            but for some reason it doesn't even execute correctly !
            (for instance: the last line of types.py creates an infinite generator expression, which causes a recursion/stack overflow bug).
            Don't ask me why, I'm not sure (might be because somehow a Python 3 lib was put into a Python 2 engine ? Dunno).
            
            Still, just let UPlasma load Python scripts straight from Cyan's system.pak, will do great.
            */
            string code = $@"p = '{pyPath}'
import sys, UnityEngine
if p not in sys.path:
    sys.path.append(p)
p = '{pyLibPath}'
#if p not in sys.path:
#    sys.path.append(p)
import UPlasmaModuleLoader
from UPlasma import PythonManager
PythonManager.Instance.ModuleLoader = UPlasmaModuleLoader";
            ScriptSource source = Engine.CreateScriptSourceFromString(code, SourceCodeKind.Statements);
            CallWrapped(() => source.Execute(scope));
        }

        IEnumerator WriteLogCoroutine()
        {
            logsDir = Path.Combine(GameManager.Instance.userDataPath, "logs");
            Directory.CreateDirectory(logsDir);
            string logPath = Path.Combine(logsDir, "pythonLog.txt");
            if (File.Exists(logPath))
                File.Delete(logPath);
            while (true)
            {
                yield return new WaitForSecondsRealtime(logWriteEveryXSeconds);
                FlushLog();
            }
        }

        void FlushLog()
        {
            string logPath = Path.Combine(logsDir, "pythonLog.txt");
            File.AppendAllText(logPath, logBuilder.ToString());
            logBuilder.Clear();
        }

        public ScriptScope CreateScope()
        {
            // create the actual scope
            ScriptScope scope = Engine.CreateScope();
            return scope;
        }

        /// <summary>
        /// Loads a script source from its name and returns its compiled version.
        /// The script will be fetched from a PAK file or one of the override directories.
        /// The compiled version of the script is cached and thus loading it is faster on subsequent attempts.
        /// </summary>
        /// <param name="scriptName"></param>
        /// <returns></returns>
        public CompiledCode LoadCompiledScript(string scriptName)
        {
            if (compiledScripts.ContainsKey(scriptName))
                return compiledScripts[scriptName];
            
            string code = PlasmaManager.Instance.GetPythonScript(scriptName);
            ScriptSource source = PythonManager.Instance.Engine.CreateScriptSourceFromString(code, SourceCodeKind.Statements);
            CompiledCode compiled = source.Compile();
            compiledScripts[scriptName] = compiled;
            return compiled;
        }

        /// <summary>
        /// Loads a script source from its name.
        /// The script will be fetched from a PAK file or one of the override directories.
        /// Note that this version requires recompiling the source code afterwards, so it's best to let
        /// the Python module loader script register it in the modules when possible.
        /// </summary>
        /// <param name="scriptName"></param>
        /// <returns></returns>
        public string LoadScript(string scriptName)
        {
            string code = PlasmaManager.Instance.GetPythonScript(scriptName);
            return code;
        }

        public bool HasScript(string scriptName)
        {
            return PlasmaManager.Instance.HasPythonScript(scriptName);
        }

        /// <summary>
        /// Sets the PFM as the "active" one. This should always be called before executing Python code,
        /// since it allows retrieving attribs from ptAttribXXX methods, as well as get a stack trace when
        /// printing messages.
        /// Internally, this only sets the global variable curPFM to the PFM itself, which can be
        /// accessed by other scripts like PlasmaTypes.py.
        /// Note that restoring the previous PFM is usually a good idea after your operation is complete,
        /// so that tracebacking is more efficient.
        /// </summary>
        /// <param name="pfm">the pfm to set as active</param>
        public void PushActivePythonScript(PlPythonFileMod pfm)
        {
            if (pfm != null)
            {
                activePFMs.Push(pfm);
                Engine.GetBuiltinModule().SetVariable("curPFM", pfm);
            }
            else
                PopActivePythonScript();
        }

        /// <summary>
        /// Restores the last PFM used before pushing, or null if there was none.
        /// </summary>
        /// <returns>the newly restored PFM or null</returns>
        public PlPythonFileMod PopActivePythonScript()
        {
            PlPythonFileMod pfm = null;
            if (activePFMs.Count != 0)
                pfm = activePFMs.Pop();
            Engine.GetBuiltinModule().SetVariable("curPFM", pfm);
            return pfm;
        }

        /// <summary>
        /// Prints a message in the Python log.
        /// </summary>
        /// <param name="msg"></param>
        public void Log(string msg, dynamic tracer = null)
        {
            // since Plamza usually doesn't know the difference between info, error and warning,
            // try to guess from the message's beginning...
            bool isError = msg.ToLower().StartsWith("error:");
            bool isWarning = msg.ToLower().StartsWith("warning:");

            if (isError)
                LogError(msg, tracer);
            else if (isWarning)
                LogWarning(msg, tracer);
            else
            {
                PlPythonFileMod curPFM = (activePFMs.Count != 0) ? activePFMs.Peek() : null;
                string stackTrace = curPFM ? ("PFM: " + curPFM.name + "\n") : "";
                if (tracer != null)
                    stackTrace += tracer.getTrace();

                Object callerObject = null;
                if (curPFM is Object)
                    callerObject = curPFM as Object;

                logBuilder.Append(System.DateTime.Now.ToString("(dd/MM HH:mm:ss) ")
                    + msg + "\n");
                if (GlobalConfig.Instance.console.logPythonToDebugLog)
                    Debug.Log(msg);
                if (LogMessageReceived != null)
                    LogMessageReceived.Invoke(msg, stackTrace, callerObject, LogType.Log);
            }
        }

        /// <summary>
        /// Prints an error message in the Python log.
        /// </summary>
        /// <param name="msg"></param>
        public void LogError(string msg, dynamic tracer = null)
        {
            PlPythonFileMod curPFM = (activePFMs.Count != 0) ? activePFMs.Peek() : null;
            string stackTrace = curPFM ? ("PFM: " + curPFM.name + "\n") : "";
            if (tracer != null)
                stackTrace += tracer.getTrace();

            Object callerObject = null;
            if (curPFM is Object)
                callerObject = curPFM as Object;

            logBuilder.Append(System.DateTime.Now.ToString("(dd/MM HH:mm:ss) ") + "[E] "
                + msg + "\n");
            if (GlobalConfig.Instance.console.logPythonToDebugLog)
                Debug.LogError(msg);
            if (LogMessageReceived != null)
                LogMessageReceived.Invoke(msg, stackTrace, callerObject, LogType.Error);
        }

        /// <summary>
        /// Prints a warning message in the Python log.
        /// </summary>
        /// <param name="msg"></param>
        public void LogWarning(string msg, dynamic tracer = null)
        {
            PlPythonFileMod curPFM = (activePFMs.Count != 0) ? activePFMs.Peek() : null;
            string stackTrace = curPFM ? ("PFM: " + curPFM.name + "\n") : "";
            if (tracer != null)
                stackTrace += tracer.getTrace();

            Object callerObject = null;
            if (curPFM is Object)
                callerObject = curPFM as Object;

            logBuilder.Append(System.DateTime.Now.ToString("(dd/MM HH:mm:ss) ") + "[W] "
                + msg + "\n");
            if (GlobalConfig.Instance.console.logPythonToDebugLog)
                Debug.LogWarning(msg);
            if (LogMessageReceived != null)
                LogMessageReceived.Invoke(msg, stackTrace, callerObject, LogType.Warning);
        }

        /// <summary>
        /// Calls the specified function. If the function throws an exception, this method
        /// prints a traceback to the Python log before rethrowing (depending on whether
        /// exception rethrow is enabled in the global config file.)
        /// </summary>
        /// <typeparam name="T">return value of the function</typeparam>
        /// <param name="func">function to invoke</param>
        /// <returns>whatever the function returns, or null/zero if the method fails and
        /// the exception isn't rethrown.</returns>
        public T CallWrapped<T>(System.Func<T> func)
        {
            try
            {
                return func.Invoke();
            }
            catch (System.Exception e)
            {
                LogError(Instance.Engine.GetService<ExceptionOperations>().FormatException(e), null);
                if (GlobalConfig.Instance.console.rethrowPythonExceptions)
                    throw;
            }
            return default(T);
        }

        /// <summary>
        /// Calls the specified function. If the function throws an exception, this method
        /// prints a traceback to the Python log before rethrowing (depending on whether
        /// exception rethrow is enabled in the global config file.)
        /// </summary>
        /// <param name="act">function to invoke</param>
        /// <param name="neverRethrow">disallow rethrowing, instead simply returning false on error</param>
        /// <returns>true if the function executed, false if there was an unthrown
        /// exception.</returns>
        public bool CallWrapped(System.Action act, bool neverRethrow = false)
        {
            try
            {
                act.Invoke();
                return true;
            }
            catch (System.Exception e)
            {
                LogError(Instance.Engine.GetService<ExceptionOperations>().FormatException(e), null);
                if (GlobalConfig.Instance.console.rethrowPythonExceptions && !neverRethrow)
                    throw;
            }
            return false;
        }

        void OnDestroy()
        {
            FlushLog();
            if (Engine != null)
            {
                // clean up path... just in case. I have no idea if that's actually required
                ScriptScope scope = Engine.CreateScope();
                string pyPath = Path.Combine(Application.streamingAssetsPath, "python");
                string pyLibPath = Path.Combine(pyPath, "Lib");
                string code = $@"p = '{pyPath}'
import sys, UnityEngine
if p in sys.path:
    sys.path.remove(p)
p = '{pyLibPath}'
if p in sys.path:
    sys.path.remove(p)";
                ScriptSource source = Engine.CreateScriptSourceFromString(code, SourceCodeKind.Statements);
                CompiledCode compiled = source.Compile();
                CallWrapped(() => compiled.Execute(scope));
            }
            
            if (Instance == this)
                Instance = null;
        }
    }
}
