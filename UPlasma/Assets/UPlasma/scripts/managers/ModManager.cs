/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;
using Newtonsoft.Json;
using System;

namespace UPlasma
{
    /// <summary>
    /// Manager for mod loading, unloading etc (duh). Unity mods only (bundle+assembly)
    /// Mods are contained in their own folder on the disk. The folder name gives the modId, which is used to keep track of loaded mods, etc
    /// Note: Plasma mods are handled by their own PlasmaModManager.
    /// 
    /// Standard hierarchy of a mod:
    ///     modId/                                  # id of the mod
    ///         descriptor.json                     # descriptor
    ///         some_asset_bundle                   # Unity's native asset bundle
    ///         some_asset_bundle.manifest          # some Unity-specific manifest for the bundle of the same name
    ///         modId.dll                           # the mod's main script assembly (optional, for Unity mods that use scripts)
    /// 
    /// The descriptor gives plenty of information that are displayed in the mod manager GUI, allowing for easier mod management.
    /// 
    /// Mods can be managed in-game through the mod manager GUI. This GUI also allows changing the mod order,
    /// and (not coded at time of writing) even download mods without leaving the game (kinda like an integrated UAM).
    /// It's a pretty neat feature.
    /// However, this makes mod management trickier from the code. Adding Unity assets at runtime is often simple enough,
    /// but unloading those can be a bit more problematic.
    /// For instance: what about unloading an AssetBundle which contains the scene we're in ? Also, can we unload an assembly in the current domain at runtime ?
    /// (Or can we use a separate domain for FAge assemblies ?) Can we load a previously-unloaded mod without Unity going bonkers ?
    /// I guess this will have to be tested...
    /// </summary>
    public class ModManager : MonoBehaviour
    {
        [Serializable]
        public class ModLoadingInfo
        {
            public ModDescriptor descriptor; // from the unity json manifest
            public string path; // full path (absolute)
            public string modId; // this is actually the mod folder name, which serves as ID
            public ModMainEntry mainEntry = null;
            public bool isSystemMod = false;
        }
        
        public static ModManager Instance { get; protected set; }

        /// <summary>
        /// List of mods that we know exist.
        /// </summary>
        public Dictionary<string, ModLoadingInfo> availableMods = new Dictionary<string, ModLoadingInfo>();

        /// <summary>
        /// mods that are actually instanced in the DontDestroyOnLoad scene (the actual entry for those is stored in the ModLoadingInfo)
        /// Note that this list is in order of activation
        /// </summary>
        public List<string> currentlyActiveMods = new List<string>();

        /// <summary>
        /// assemblies that are actually loaded.
        /// Note: currently an assembly can be neither unloaded nor reloaded at runtime
        /// this will require loading them into a new appdomain... TODO.
        /// </summary>
        public Dictionary<string, Assembly> currentlyLoadedAssembly = new Dictionary<string, Assembly>();
        
        [HideInInspector]
        public Transform modParent;
        
        void Awake()
        {
            if (Instance == null)
                Instance = this;
            
            modParent = new GameObject("mods").transform;
            modParent.SetParent(transform);
        }

        /// <summary>
        /// Refreshes the list of available mods
        /// </summary>
        public void RefreshAvailableMods()
        {
            List<string> addedMods = new List<string>();
            List<string> removedMods = new List<string>();
            
            Dictionary<string, ModLoadingInfo> previouslyAvailableMods = new Dictionary<string, ModLoadingInfo>(availableMods);
            availableMods.Clear();
            
            // search the system mods folder.
            string systemModsFolderPath = Path.Combine(GameManager.Instance.streamingAssetsPath, "systemMods_unity");
            if (Directory.Exists(systemModsFolderPath))
            {
                foreach (string dir in Directory.GetDirectories(systemModsFolderPath))
                {
                    string modId = new DirectoryInfo(dir).Name;
                    if (!previouslyAvailableMods.ContainsKey(modId))
                    {
                        // mod is not known yet - add it to the list of mods we know.
                        
                        // load the descriptors
                        string descriptorFileName = Path.Combine(dir, "descriptor.json");
                        if (!File.Exists(descriptorFileName))
                        {
                            Debug.LogWarning("Empty mod folder: " + dir + ", ignoring...");
                            continue;
                        }
                        
                        ModLoadingInfo info = new ModLoadingInfo();
                        
                        try {
                            info.descriptor = JsonConvert.DeserializeObject<ModDescriptor>(File.ReadAllText(descriptorFileName));
                        }
                        catch (JsonException ex) {
                            Debug.LogError($"Could not load mod descriptor {dir}");
                            Debug.LogError(ex.Message);
                            continue;
                        }
                        info.path = dir;
                        info.modId = modId;
                        info.isSystemMod = true;
                        
                        availableMods[modId] = info;
                    }
                    else
                    {
                        // mod is already known, no need to do anything.
                        availableMods[modId] = previouslyAvailableMods[modId];
                        // in the future we might update its descriptor ? although that might do more harm than good...
                    }
                }
            }
            // do the same for the user mods folder.
            string modsFolderPath = Path.Combine(GameManager.Instance.userDataPath, "mods_unity");
            Directory.CreateDirectory(modsFolderPath); // just in case
            foreach (string dir in Directory.GetDirectories(modsFolderPath))
            {
                string modId = new DirectoryInfo(dir).Name;
                if (!previouslyAvailableMods.ContainsKey(modId))
                {
                    // mod is not known yet - add it to the list of mods we know.
                    
                    // load the descriptors
                    string descriptorFileName = Path.Combine(dir, "descriptor.json");
                    if (!File.Exists(descriptorFileName))
                    {
                        Debug.LogWarning("Empty mod folder: " + dir + ", ignoring...");
                        continue;
                    }
                    
                    ModLoadingInfo info = new ModLoadingInfo();
                    
                    try {
                        info.descriptor = JsonConvert.DeserializeObject<ModDescriptor>(File.ReadAllText(descriptorFileName));
                    }
                    catch (JsonException ex) {
                        Debug.LogError("Could not load mod descriptor" + dir);
                        Debug.LogError(ex.Message);
                        continue;
                    }
                    info.path = dir;
                    info.modId = modId;
                    info.isSystemMod = false;
                    
                    if (!GlobalConfig.Instance.mods.knownMods.Contains(info.modId))
                    {
                        Debug.LogWarning("New mod found: " + modId);
                        addedMods.Add(modId);
                    }
                    
                    availableMods[modId] = info;
                }
                else
                {
                    // mod is already known, no need to do anything.
                    availableMods[modId] = previouslyAvailableMods[modId];
                }
            }
            
            // register removed mods (don't register those that were previously inactive, we don't care about those...)
            foreach (string modId in GlobalConfig.Instance.mods.activeMods)
            {
                if (!availableMods.ContainsKey(modId))
                {
                    // aw shit
                    Debug.LogWarning("Mod removed: " + modId);
                    removedMods.Add(modId);
                }
            }
            // force-uninstall removed mods
            foreach (string modId in removedMods)
            {
                // "forget" it by removing it from the list of available mods
                availableMods.Remove(modId);
                if (currentlyActiveMods.Contains(modId))
                    UnloadMod(modId, true);
            }
            
            // warn the user about removed mods
            // warn the user about added mods
            // and when the user said ok to both, UpdateKnownAndActiveModsInConfig()
        }

        /// <summary>
        /// Load all known system mods that aren't yet active
        /// (called when entering the main menu)
        /// on error or success callback is called with success status and message
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IEnumerator LoadSystemMods(System.Action<bool, string> callback)
        {
            foreach (string modId in availableMods.Keys)
            {
                if (availableMods[modId].isSystemMod && !currentlyActiveMods.Contains(modId))
                {
                    bool ok = true;
                    // TODO - use callback func to warn about mod not being loaded
                    yield return StartCoroutine(LoadMod(modId, true,
                        (b, s) => {
                            if (!b)
                            {
                                // error while loading - this is critical since system mods can't be deactivated.
                                // let the gamemanager handle this...
                                callback?.Invoke(b, s);
                                ok = false;
                            }
                        }
                    ));
                    if (!ok)
                        yield break;
                }
            }

            callback?.Invoke(true, "ok");
        }

        /// <summary>
        /// Load all non-system mods which are in the active mods list but not yet active
        /// (called when starting the game from the main menu)
        /// on error or success callback is called with success status and message
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IEnumerator LoadUserMods(System.Action<bool, string> callback)
        {
            foreach (string modId in GlobalConfig.Instance.mods.activeMods)
            {
                if (!availableMods[modId].isSystemMod && !currentlyActiveMods.Contains(modId))
                {
                    bool ok = true;
                    // TODO - use callback func to warn about mod not being loaded
                    yield return StartCoroutine(LoadMod(modId, true,
                        (b, s) => {
                            if (!b)
                            {
                                // error while loading - this isn't critical, but we must notify the user.
                                // let the gamemanager handle this...
                                if (callback != null)
                                    callback(b, s);
                                ok = false;
                            }
                        }
                    ));
                    if (!ok)
                        yield break;
                }
            }
            
            if (callback != null)
                callback(true, "ok");
        }

        /// <summary>
        /// Unload all non-system mods which are in the active mods list
        /// (called when returning to the main menu)
        /// </summary>
        public void UnloadUserMods()
        {
            foreach (string modId in currentlyActiveMods)
            {
                if (!availableMods[modId].isSystemMod)
                    UnloadMod(modId, true);
            }
        }

        /// <summary>
        /// Unload the specified mod. Returns false if deactivation isn't forced and the mod requests to not be unloaded.
        /// If you want this to be permanent, you must call UpdateKnownAndActiveModsInConfig() afterwards
        /// </summary>
        /// <param name="modId"></param>
        /// <param name="force"></param>
        /// <returns></returns>
        public bool UnloadMod(string modId, bool force)
        {
            print("Deactivating " + modId);
            
            // first see if one of the scenes from the mod is loaded
            foreach (AgeDescriptor ageDesc in availableMods[modId].descriptor.ages)
            {
                if (GameManager.Instance.IsSceneLoaded(ageDesc.sceneName))
                {
                    // we're in the mod's Age... what do we do ?
                    if (force)
                    {
                        // okay, we're not kidding around. Go to the main menu so it unloads the scene,
                        // which should avoid errors while unloading the asset bundle
                        GameManager.Instance.GoToMainMenu(true);
                    }
                    else
                        // not forced ? then simply refuse deactivation.
                        return false;
                }
            }
            
            // let the main entry remove things if needed
            bool result = availableMods[modId].mainEntry.Deactivate(force);
            if (result || force)
            {
                // now unregister the Ages from the library
                foreach (AgeDescriptor ageDesc in availableMods[modId].descriptor.ages)
                    AgeLibrary.Instance.knownAges.Remove(ageDesc);
                
                currentlyActiveMods.Remove(modId);
                // TODO - find a way to unload the assembly without risking crashing everything
                // availableMods[modId].mainEntry.runtimeAssembly
                
                // unload the asset bundle and all its assets
                foreach (AssetBundle bundle in availableMods[modId].mainEntry.runtimeBundles)
                    bundle.Unload(true);
                
                availableMods.Remove(modId);
                
                return true;
            }
            return false;
        }

        /// <summary>
        /// Loads a mod. <paramref name="callback"/>(result, message) is called when the mod is loaded (or if it failed to validate).
        /// Note that this process is asynchronous, so the callback function will often be called after returning.
        /// If you want this to be permanent, you must call <see cref="UpdateKnownAndActiveModsInConfig"/> afterwards
        /// </summary>
        /// <param name="modId"></param>
        /// <param name="isGameInit"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IEnumerator LoadMod(string modId, bool isGameInit, System.Action<bool, string> callback)
        {
            // TODO - check dependencies, including circular dep
            
            print("Activating " + modId);
            
            if (currentlyActiveMods.Contains(modId))
            {
                string msg = "Mod is already active";
                Debug.LogError(msg);
                if (callback != null)
                    callback(false, msg);
                yield break;
            }
            
            ModLoadingInfo info = availableMods[modId];
            string modFolderPath = info.path;
            
            // get a list of the bundles to load
            List<string> bundleNames = new List<string>();
            foreach (string file in Directory.GetFiles(modFolderPath))
                if (File.Exists(file + ".manifest"))
                    // okay, that's a bundle since it has a manifest
                    bundleNames.Add(file);
            
            // load assembly
            bool hasAssembly = false;
            if (currentlyLoadedAssembly.ContainsKey(modId))
            {
                print("Mod assembly already loaded");
                hasAssembly = true;
            }
            else
            {
                string assemblyName = Path.Combine(modFolderPath, info.modId + ".dll");
                if (File.Exists(assemblyName))
                {
                    print("Loading mod assembly " + assemblyName);
                    currentlyLoadedAssembly[modId] = Assembly.Load(File.ReadAllBytes(assemblyName));
                    hasAssembly = true;
                }
                else
                    print("Mod doesn't have an assembly. Skipping.");
            }
            
            List<AssetBundle> bundles = new List<AssetBundle>();
            foreach (string bundle in bundleNames)
            {
                AssetBundleCreateRequest request = AssetBundle.LoadFromFileAsync(bundle);
                yield return request;
                if (request.assetBundle == null)
                {
                    string msg = "Failed to load bundle " + bundle + " !";
                    Debug.LogError(msg);
                    if (callback != null)
                        callback(false, msg);
                    // TODO force unload mods
                    yield break;
                }
                bundles.Add(request.assetBundle);
            }
            
            // find scenes that weren't mentioned in the descriptor
            foreach (AssetBundle bundle in bundles)
            {
                foreach (string asset in bundle.GetAllScenePaths())
                {
                    string assetWoPath = asset;
                    if (assetWoPath.LastIndexOf('/') != -1)
                        assetWoPath = assetWoPath.Substring(assetWoPath.LastIndexOf('/') + 1);
                    if (info.descriptor.ages.Find(x => x.sceneName == assetWoPath) == null)
                    {
                        // not in the list of available ages. Add it.
                        AgeDescriptor ageDesc = new AgeDescriptor();
                        ageDesc.displayName = ageDesc.sceneName = assetWoPath;
                        ageDesc.appearsInNexus = ageDesc.registerInPersonalLibrary = false;
                        info.descriptor.ages.Add(ageDesc);
                    }
                }
            }
            
            // register the Ages in the library
            // (we also register non-public Ages and non-Ages, because why not)
            foreach (AgeDescriptor ageDesc in info.descriptor.ages)
            {
                // make sure those two are correct
                ageDesc.isFanAge = true;
                ageDesc.isUnityAge = true;
                AgeLibrary.Instance.knownAges.Add(ageDesc);
            }
            
            ModMainEntry modEntry = null;
            if (!string.IsNullOrEmpty(info.descriptor.mainAssetName))
            {
                // get main asset path
                string bundleName = info.descriptor.mainAssetName.Split('/')[0];
                string mainAssetPath = info.descriptor.mainAssetName.Substring(bundleName.Length + 1);
                AssetBundle bundle = bundles.Find(x => x.name == bundleName);
                GameObject modObj = (GameObject)Instantiate(bundle.LoadAsset(mainAssetPath));
                modObj.transform.SetParent(modParent);
                modEntry = modObj.GetComponent<ModMainEntry>();
            }
            else
            {
                // create dummy main entry. It won't do any processing but will hold the bundles etc for us.
                GameObject modObj = new GameObject(modId);
                modObj.transform.SetParent(modParent);
                modEntry = modObj.AddComponent<ModMainEntry>();
            }
            modEntry.runtimeDescriptor = info.descriptor;
            if (hasAssembly)
                modEntry.runtimeAssembly = currentlyLoadedAssembly[modId];
            modEntry.runtimeBundles = bundles;
            info.mainEntry = modEntry;
            modEntry.Activate(isGameInit);
            currentlyActiveMods.Add(modId);

            callback?.Invoke(true, "");
        }

        /// <summary>
        /// Syncs the modlist in the config file with the current one.
        /// config.knownMods = <see cref="availableMods"/>
        /// config.activeMods = <see cref="currentlyActiveMods"/> without the system mods
        /// Only call this when all the mods are loaded ! Calling it in the main menu will disable all user mods since they
        /// won't be active yet.
        /// </summary>
        public void UpdateKnownAndActiveModsInConfig()
        {
            GlobalConfig.Instance.mods.knownMods.Clear();
            foreach (KeyValuePair<string, ModLoadingInfo> pair in availableMods)
                GlobalConfig.Instance.mods.knownMods.Add(pair.Key);
            GlobalConfig.Instance.mods.activeMods = new List<string>(currentlyActiveMods)
                .FindAll(modId => !availableMods[modId].isSystemMod);
        }
        
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }
    }
}
