/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UPlasma.PlEmu;
using System;

namespace UPlasma
{
    /// <summary>
    /// Handles persistance of any progress-related data (=saves).
    /// Its goal is both facilitating access to said data, and act as a proxy
    /// so that saving locally or on a server (when online) is made simpler and transparent.
    /// 
    /// Because of the obvious delay in networking, most of these methods are async.
    /// Using them usually requires chaining coroutines ("yield StartCoroutine(x)").
    /// IMHO this is fugly compared to C# Tasks, but is the default wei to do things in Unity,
    /// and ensures parts of the code using Unity API run on the correct thread.
    /// </summary>
    public class ProgressManager : MonoBehaviour
    {
        public static ProgressManager Instance { get; protected set; }

        public Dictionary<PlSDL, PlSDL.PlSDVar> dirtySDLvars = new Dictionary<PlSDL, PlSDL.PlSDVar>();
        /// <summary>
        /// SDLs that are listening for changes from the server.
        /// </summary>
        public List<PlSDL> watchedSDLs = new List<PlSDL>();

        void Awake()
        {
            if (Instance == null)
                Instance = this;
        }
        void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }

        /// <summary>
        /// Fills an SDL with persisted data.
        /// </summary>
        /// <param name="sdl"></param>
        public void LoadSDLValues(PlSDL sdl)
        {
            string savesFolder = Path.Combine(
                GameManager.Instance.userDataPath,
                "saves",
                "default",
                "sdl"
            );
            // .uplpxxx extension: uplasma persistent xxx
            string sdlFile = Path.Combine(savesFolder, sdl.name) + ".uplpsdl";
            if (File.Exists(sdlFile))
            {
                using (FileStream f = File.OpenRead(sdlFile))
                {
                    sdl.RestoreValues(f);
                }
            }
            else
            {
                SaveSDL(sdl);
            }
        }

        /// <summary>
        /// Registers an SDL var as dirty. This var will be sent to the server, which will then sync
        /// it with ourselves and other clients.
        /// </summary>
        /// <param name="sdl">state record which was modified</param>
        /// <param name="sdvar">state variable which was modified</param>
        public void RegisterDirtySDLVar(PlSDL.PlSDVar sdvar)
        {
            if (!watchedSDLs.Contains(sdvar.sdl))
                throw new Exception("Didn't expect to change a variable from an unwatched SDL...");

            Debug.Log("Dirty var: " + sdvar.name + " from " + sdvar.sdl.name);
            dirtySDLvars[sdvar.sdl] = sdvar;
        }

        /// <summary>
        /// Handles progress-related events.
        /// Most notably, it relays sdl modification events.
        /// </summary>
        public void Update()
        {
            HandleDirtySDLs();
        }

        /// <summary>
        /// Tells all SDLs which registered themselves for notifications about changes to SDLs,
        /// usually coming from the server.
        /// TODO since right now this is obviously an offline process...
        /// (I'm beginning to suspect this code is really bad and will be horrible to network properly...)
        /// </summary>
        void HandleDirtySDLs()
        {
            List<PlSDL> dirtySDLs = new List<PlSDL>();
            foreach (KeyValuePair<PlSDL, PlSDL.PlSDVar> kv in dirtySDLvars)
            {
                if (!watchedSDLs.Contains(kv.Key))
                {
                    // we got an SDL modification from an unwatched SDL. Ignore it, I guess ?
                    continue;
                }

                kv.Key.ServerValueChanged(kv.Value.name, kv.Value.values, -1, null);
                dirtySDLs.Add(kv.Key);
            }
            dirtySDLvars.Clear();

            foreach (PlSDL sdl in dirtySDLs)
                SaveSDL(sdl);
        }

        void SaveSDL(PlSDL sdl)
        {
            string savesFolder = Path.Combine(
                GameManager.Instance.userDataPath,
                "saves",
                "default",
                "sdl"
            );
            // .uplpxxx extension: uplasma persistent xxx
            string sdlFile = Path.Combine(savesFolder, sdl.name) + ".uplpsdl";
            Directory.CreateDirectory(savesFolder);
            using (FileStream f = File.OpenWrite(sdlFile))
            {
                sdl.SaveValues(f);
            }
        }
    }
}
