﻿Shader "Plasma/WaveSet/CompCosine"
{
    Properties
    {
        [NoScaleOffset]
        tCosineLUT ("Cosine LUT", 2D) = "white" {}
        [NoScaleOffset]
        tBiasNoise ("Bias noise", 2D) = "white" {}

        cUTrans0 ("UTrans0", Vector) = (1, 1, 1, 1)
        cUTrans1 ("UTrans1", Vector) = (1, 1, 1, 1)
        cUTrans2 ("UTrans2", Vector) = (1, 1, 1, 1)
        cUTrans3 ("UTrans3", Vector) = (1, 1, 1, 1)
        cUTrans4 ("UTrans4", Vector) = (1, 1, 1, 1)
        cUTrans5 ("UTrans5", Vector) = (1, 1, 1, 1)
        cUTrans6 ("UTrans6", Vector) = (1, 1, 1, 1)
        cUTrans7 ("UTrans7", Vector) = (1, 1, 1, 1)
        cUTrans8 ("UTrans8", Vector) = (1, 1, 1, 1)
        cUTrans9 ("UTrans9", Vector) = (1, 1, 1, 1)
        cUTrans10 ("UTrans10", Vector) = (1, 1, 1, 1)
        cUTrans11 ("UTrans11", Vector) = (1, 1, 1, 1)
        cUTrans12 ("UTrans12", Vector) = (1, 1, 1, 1)
        cUTrans13 ("UTrans13", Vector) = (1, 1, 1, 1)
        cUTrans14 ("UTrans14", Vector) = (1, 1, 1, 1)
        cUTrans15 ("UTrans15", Vector) = (1, 1, 1, 1)

        cCoef0 ("Coef0", Vector) = (1, 1, 1, 1)
        cCoef1 ("Coef1", Vector) = (1, 1, 1, 1)
        cCoef2 ("Coef2", Vector) = (1, 1, 1, 1)
        cCoef3 ("Coef3", Vector) = (1, 1, 1, 1)
        cCoef4 ("Coef4", Vector) = (1, 1, 1, 1)
        cCoef5 ("Coef5", Vector) = (1, 1, 1, 1)
        cCoef6 ("Coef6", Vector) = (1, 1, 1, 1)
        cCoef7 ("Coef7", Vector) = (1, 1, 1, 1)
        cCoef8 ("Coef8", Vector) = (1, 1, 1, 1)
        cCoef9 ("Coef9", Vector) = (1, 1, 1, 1)
        cCoef10 ("Coef10", Vector) = (1, 1, 1, 1)
        cCoef11 ("Coef11", Vector) = (1, 1, 1, 1)
        cCoef12 ("Coef12", Vector) = (1, 1, 1, 1)
        cCoef13 ("Coef13", Vector) = (1, 1, 1, 1)
        cCoef14 ("Coef14", Vector) = (1, 1, 1, 1)
        cCoef15 ("Coef15", Vector) = (1, 1, 1, 1)

        cReScale ("ReScale", Vector) = (1, 1, 1, 1)

        cNoiseXForm0_00 ("NoiseXForm0_00", Vector) = (20, 0, 0, 0)
        cNoiseXForm0_10 ("NoiseXForm0_10", Vector) = (0, 20, 0, 0)
        cNoiseXForm1_00 ("NoiseXForm1_00", Vector) = (20, 0, 0, 0)
        cNoiseXForm1_10 ("NoiseXForm1_10", Vector) = (0, 20, 0, 0)
        
        cScaleBias ("ScaleBias", Vector) = (1, 1, 1, 1)

    }
    
    CGINCLUDE
        //This code is available to all passes below
        #include "UnityCustomRenderTexture.cginc"
        
        struct v2f
        {
            float4 Position : SV_POSITION;
            float4 Uv0 : TEXCOORD0; // Ripple texture coords
            float4 Uv1 : TEXCOORD1; // Ripple texture coords
            float4 Uv2 : TEXCOORD2; // Ripple texture coords
            float4 Uv3 : TEXCOORD3; // Ripple texture coords
        };
        
        sampler2D tCosineLUT;
        sampler2D tBiasNoise;
        
        float4 cUTrans0;
        float4 cUTrans1;
        float4 cUTrans2;
        float4 cUTrans3;
        float4 cUTrans4;
        float4 cUTrans5;
        float4 cUTrans6;
        float4 cUTrans7;
        float4 cUTrans8;
        float4 cUTrans9;
        float4 cUTrans10;
        float4 cUTrans11;
        float4 cUTrans12;
        float4 cUTrans13;
        float4 cUTrans14;
        float4 cUTrans15;
        
        float4 cCoef0;
        float4 cCoef1;
        float4 cCoef2;
        float4 cCoef3;
        float4 cCoef4;
        float4 cCoef5;
        float4 cCoef6;
        float4 cCoef7;
        float4 cCoef8;
        float4 cCoef9;
        float4 cCoef10;
        float4 cCoef11;
        float4 cCoef12;
        float4 cCoef13;
        float4 cCoef14;
        float4 cCoef15;
        
        float4 cReScale;
        
        v2f vertCRT(appdata_customrendertexture IN)
        {
            v2f OUT;

            #if UNITY_UV_STARTS_AT_TOP
                const float2 vertexPositions[6] =
                {
                    { -1.0f,  1.0f },
                    { -1.0f, -1.0f },
                    {  1.0f, -1.0f },
                    {  1.0f,  1.0f },
                    { -1.0f,  1.0f },
                    {  1.0f, -1.0f }
                };

                const float2 texCoords[6] =
                {
                    { 0.0f, 0.0f },
                    { 0.0f, 1.0f },
                    { 1.0f, 1.0f },
                    { 1.0f, 0.0f },
                    { 0.0f, 0.0f },
                    { 1.0f, 1.0f }
                };
            #else
                const float2 vertexPositions[6] =
                {
                    {  1.0f,  1.0f },
                    { -1.0f, -1.0f },
                    { -1.0f,  1.0f },
                    { -1.0f, -1.0f },
                    {  1.0f,  1.0f },
                    {  1.0f, -1.0f }
                };

                const float2 texCoords[6] =
                {
                    { 1.0f, 1.0f },
                    { 0.0f, 0.0f },
                    { 0.0f, 1.0f },
                    { 0.0f, 0.0f },
                    { 1.0f, 1.0f },
                    { 1.0f, 0.0f }
                };
            #endif

            uint primitiveID = IN.vertexID / 6;
            uint vertexID = IN.vertexID % 6;
            float3 updateZoneCenter = CustomRenderTextureCenters[primitiveID].xyz;
            float3 updateZoneSize = CustomRenderTextureSizesAndRotations[primitiveID].xyz;
            float rotation = CustomRenderTextureSizesAndRotations[primitiveID].w * UNITY_PI / 180.0f;

            #if !UNITY_UV_STARTS_AT_TOP
                rotation = -rotation;
            #endif

            // Normalize rect if needed
            if (CustomRenderTextureUpdateSpace > 0.0) // Pixel space
            {
                // Normalize xy because we need it in clip space.
                updateZoneCenter.xy /= _CustomRenderTextureInfo.xy;
                updateZoneSize.xy /= _CustomRenderTextureInfo.xy;
            }
            else // normalized space
            {
                // Un-normalize depth because we need actual slice index for culling
                updateZoneCenter.z *= _CustomRenderTextureInfo.z;
                updateZoneSize.z *= _CustomRenderTextureInfo.z;
            }

            // Compute rotation

            // Compute quad vertex position
            float2 clipSpaceCenter = updateZoneCenter.xy * 2.0 - 1.0;
            float2 pos = vertexPositions[vertexID] * updateZoneSize.xy;
            pos = CustomRenderTextureRotate2D(pos, rotation);
            pos.x += clipSpaceCenter.x;
            #if UNITY_UV_STARTS_AT_TOP
                pos.y += clipSpaceCenter.y;
            #else
                pos.y -= clipSpaceCenter.y;
            #endif
            
            OUT.Position = float4(pos, 0.0, 1.0);
            float4 uvs = float4(pos.xy * 0.5 + 0.5, CustomRenderTexture3DTexcoordW, 1);
            #if UNITY_UV_STARTS_AT_TOP
                uvs.y = 1.0 - uvs.y;
            #endif
            OUT.Uv0 = uvs;

            return OUT;
        }
        v2f vertUVDot(appdata_customrendertexture IN, float4 rot0, float4 rot1, float4 rot2, float4 rot3)
        {
            v2f OUT = vertCRT(IN);
            float4 uvs = OUT.Uv0;
            int tex_size = 256;
            // offset the X coordinate
            // don't ask me why :shrug:
            uvs.x += .5 / tex_size;
            OUT.Uv0 = float4(dot(uvs, rot0), 0, 0, 1);
            OUT.Uv1 = float4(dot(uvs, rot1), 0, 0, 1);
            OUT.Uv2 = float4(dot(uvs, rot2), 0, 0, 1);
            OUT.Uv3 = float4(dot(uvs, rot3), 0, 0, 1);

            return OUT;
        }
        
        fixed4 fragCompCosines(v2f i, float4 c0, float4 c1, float4 c2, float4 c3)
        {
            // eh, assembly looks horrible but in this case translating it to shader language is quite simple...
            // still dunno why mark bothered writing it in assembly tho. Wouldn't the compiler automatically optimize
            // the hlsl equivalent so you'd end up with similar performances ? Bah, whatever.
            fixed4 col = (tex2D(tCosineLUT, i.Uv0) - 0.5f) * 2.0f * c0;
            col += (tex2D(tCosineLUT, i.Uv1) - 0.5f) * 2.0f * c1;
            col += (tex2D(tCosineLUT, i.Uv2) - 0.5f) * 2.0f * c2;
            col += (tex2D(tCosineLUT, i.Uv3) - 0.5f) * 2.0f * c3;
            col = col * cReScale + cReScale;
            return col;
        }
    ENDCG
    
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        
		Cull Off
        ZWrite Off
        Lighting Off
        
        // we need four passes to sum all the waves
        // (multipass is not ideal, but it's the only alternative to doing dot into the fragment shader, so eh, why not.)
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            
            v2f vert (appdata_customrendertexture v)
            { return vertUVDot(v, cUTrans0, cUTrans1, cUTrans2, cUTrans3); }
            
            fixed4 frag (v2f i) : SV_Target
            { return fragCompCosines(i, cCoef0, cCoef1, cCoef2, cCoef3); }
            
            ENDCG
        }
        Pass
        {
            Blend One One
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            
            v2f vert (appdata_customrendertexture v)
            { return vertUVDot(v, cUTrans4, cUTrans5, cUTrans6, cUTrans7); }
            
            fixed4 frag (v2f i) : SV_Target
            { return fragCompCosines(i, cCoef4, cCoef5, cCoef6, cCoef7); }
            
            ENDCG
        }
        Pass
        {
            Blend One One
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            
            v2f vert (appdata_customrendertexture v)
            { return vertUVDot(v, cUTrans8, cUTrans9, cUTrans10, cUTrans11); }
            
            fixed4 frag (v2f i) : SV_Target
            { return fragCompCosines(i, cCoef8, cCoef9, cCoef10, cCoef11); }
            
            ENDCG
        }
        Pass
        {
            Blend One One
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            
            v2f vert (appdata_customrendertexture v)
            { return vertUVDot(v, cUTrans12, cUTrans13, cUTrans14, cUTrans15); }
            
            fixed4 frag (v2f i) : SV_Target
            { return fragCompCosines(i, cCoef12, cCoef13, cCoef14, cCoef15); }
            
            ENDCG
        }
        Pass
        {
            Blend One One
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0
            
            float4 cNoiseXForm0_00;
            float4 cNoiseXForm0_10;
            float4 cNoiseXForm1_00;
            float4 cNoiseXForm1_10;
            float4 cScaleBias;
            
            v2f vert (appdata_customrendertexture v)
            {
                v2f OUT = vertCRT(v);
                
                // ugh... That one was tricky.
                float4 uvs = OUT.Uv0;
                OUT.Uv0 = float4(
                    dot(uvs, cNoiseXForm0_00),
                    dot(uvs, cNoiseXForm0_10),
                    0,
                    1
                );
                OUT.Uv1 = float4(
                    dot(uvs, cNoiseXForm1_00),
                    dot(uvs, cNoiseXForm1_10),
                    0,
                    1
                );
                OUT.Uv2 = float4(
                    cScaleBias.x,
                    cScaleBias.x,
                    cScaleBias.z,
                    cScaleBias.z
                );
                OUT.Uv3 = float4(
                    cScaleBias.y,
                    cScaleBias.y,
                    cScaleBias.z,
                    cScaleBias.z
                );
                
                return OUT;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 t0 = tex2D(tBiasNoise, i.Uv0);
                fixed4 t1 = tex2D(tBiasNoise, i.Uv1);
                
                fixed4 col = fixed4((t0.rgb + t1.rgb) * 0.5f, t0.a + t1.a);
                col = col * i.Uv2 + i.Uv3;
                return col;
            }
            
            ENDCG
        }
    }
}
