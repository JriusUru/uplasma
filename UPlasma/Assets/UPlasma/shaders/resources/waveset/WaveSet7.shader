﻿Shader "Plasma/WaveSet/WaveSet7"
{
	Properties
	{
        [NoScaleOffset]
        tEnvMap ("Reflection", CUBE) = "" {}
        [NoScaleOffset]
        tBumpMap ("Bump texture", 2D) = "bump" {}
        
        cWaterTint ("Water Tint", Vector) = (1, 1, 1, 1)
        cFrequency ("Frequency", Vector) = (1, 1, 1, 1)
        cPhase ("Phase", Vector) = (1, 1, 1, 1)
        cAmplitude ("Amplitude", Vector) = (1, 1, 1, 1)
        cDirX ("Dir X", Vector) = (1, 1, 1, 1)
        cDirY ("Dir Y", Vector) = (1, 1, 1, 1)
        cSpecAtten ("Spec Atten", Vector) = (1, 1, 1, 1) // uvScale is w component
        cEnvAdjust ("Env Adjust", Vector) = (1, 1, 1, 1)
        cEnvTint ("Env Tint", Vector) = (1, 1, 1, 1)
        cLengths ("Lengths", Vector) = (1, 1, 1, 1)
        cDepthOffset ("Depth Offset", Vector) = (1, 1, 1, 1) // water level is w component
        cDepthScale ("Depth Scale", Vector) = (1, 1, 1, 1)
        cDirXK ("Dir XK", Vector) = (1, 1, 1, 1)
        cDirYK ("Dir YK", Vector) = (1, 1, 1, 1)
        cDirXW ("Dir XW", Vector) = (1, 1, 1, 1)
        cDirYW ("Dir YW", Vector) = (1, 1, 1, 1)
        cKW ("KW", Vector) = (1, 1, 1, 1)
        cDirXSqKW ("Dir XSqKW", Vector) = (1, 1, 1, 1)
        cDirXDirYKW ("Dir XDirYKW", Vector) = (1, 1, 1, 1)
        cDirYSqKW ("Dir YSq fuck this", Vector) = (1, 1, 1, 1)
        
		_Runtime ("Runtime color", Color) = (1,1,1,1)
        
        // Note: edge blending relies a lot on texel size, and thus might look completely wrong on some meshes...
        _InvFade ("Edge blending (only if Soft Particles is enabled)", Range(0.01,10.0)) = 1.0
        _DistortionIntensity ("Distortion Intensity", range (-128,128)) = 40
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 100
        
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha
        Lighting Off
        
		// This pass grabs the screen behind the object into a texture.
		// We can access the result in the next pass as _GrabTexture
		GrabPass {
			Name "BASE"
		}
        
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
            #pragma multi_compile_particles
			
			#include "UnityCG.cginc"
            
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
                UNITY_VERTEX_INPUT_INSTANCE_ID
			};
            
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
                float4 BTN_X : TEXCOORD2;
                float4 BTN_Y : TEXCOORD3;
                float4 BTN_Z : TEXCOORD4;
                float4 color : COLOR;
				float4 color2 : NORMAL; // Unity doesn't have a second color channel so we use this one. Good enough.
				float4 worldPos : TEXCOORD5;
                #ifdef SOFTPARTICLES_ON
                float4 projPos : TEXCOORD6;
                #endif
                float4 uvgrab : TEXCOORD7;
                UNITY_VERTEX_OUTPUT_STEREO
			};
            
            
            float4 cWaterTint;
            float4 cFrequency;
            float4 cPhase;
            float4 cAmplitude;
            float4 cDirX;
            float4 cDirY;
            float4 cSpecAtten;
            float4 cEnvAdjust;
            float4 cEnvTint;
            float4 cLengths;
            float4 cDepthOffset;
            float4 cDepthScale;
            float4 cFogParams;
            float4 cDirXK;
            float4 cDirYK;
            float4 cDirXW;
            float4 cDirYW;
            float4 cKW;
            float4 cDirXSqKW;
            float4 cDirXDirYKW;
            float4 cDirYSqKW;
            
            // Depth filter channels control:
            // dFilter.x => overall opacity
            // dFilter.y => reflection strength
            // dFilter.z => wave height
            float3 CalcDepthFilter(const in float4 depthOffset, const in float4 depthScale, const in float4 wPos, const in float waterLevel)
            {
                float3 dFilter = float3(depthOffset.xyz) - wPos.zzz;

                dFilter = dFilter * float3(depthScale.xyz);
                dFilter = max(dFilter, 0);
                dFilter = min(dFilter, 1);

                return dFilter;
            }
            
            // See Pos in CalcTangentBasis comments.
            float4 CalcFinalPosition(in float4 wPosMeters,
                                     const in float4 sines,
                                     const in float4 cosines,
                                     const in float depthOffset,
                                     const in float4 dirXK,
                                     const in float4 dirYK)
            {
                // Height

                // Sum to a scalar
                float h = dot(sines, 1.f) + depthOffset;

                // Clamp to never go beneath input height
                wPosMeters.z = max(wPosMeters.z, h);


                wPosMeters.x = wPosMeters.x + dot(cosines, dirXK);
                wPosMeters.y = wPosMeters.y + dot(cosines, dirYK);

                return wPosMeters;
            }
            
            void CalcEyeRayAndBumpAttenuation(const in float4 wPos,
								  const in float4 cameraPos,
								  const in float4 specAtten,
								  out float3 cam2Vtx,
								  out float pertAtten)
            {
                // Get normalized vec from camera to vertex, saving original distance.
                cam2Vtx = float3(wPos.xyz) - float3(cameraPos.xyz);
                pertAtten = length(cam2Vtx);
                cam2Vtx = cam2Vtx / pertAtten;

                // Calculate our normal perturbation attenuation. This attenuation will be
                // applied to the horizontal components of the normal read from the computed
                // ripple bump map, mostly to fight aliasing. This doesn't attenuate the 
                // color computed from the normal map, it attenuates the "bumps".
                pertAtten = pertAtten + specAtten.x;
                pertAtten = pertAtten * specAtten.y;
                pertAtten = min(pertAtten, 1.f);
                pertAtten = max(pertAtten, 0.f);
                pertAtten = pertAtten * pertAtten; // Square it to account for perspective.
                pertAtten = pertAtten * specAtten.z;
            }
            
            void CalcSinCos(const in float4 wPos,
                            const in float4 dirX,
                            const in float4 dirY,
                            const in float4 amplitude,
                            const in float4 frequency,
                            const in float4 phase,
                            const in float4 lengths,
                            const in float ooEdgeLength,
                            const in float scale,
                            out float4 sines, out float4 cosines)
            {
                // Dot x and y with direction vectors
                float4 dists = dirX * wPos.xxxx;
                dists = dirY * wPos.yyyy + dists;
                
                // Scale in our frequency and add in our phase
                dists = dists * frequency;
                dists = dists + phase;

                const float kPi = 3.14159265f;
                const float kTwoPi = 2.f * kPi;
                const float kOOTwoPi = 1.f / kTwoPi;
                // Mod into range [-Pi..Pi]
                dists = dists + kPi;
                dists = dists * kOOTwoPi;
                dists = frac(dists);
                dists = dists * kTwoPi;
                dists = dists - kPi;

                float4 dists2 = dists * dists; 
                float4 dists3 = dists2 * dists;
                float4 dists4 = dists2 * dists2;
                float4 dists5 = dists3 * dists2;
                float4 dists6 = dists3 * dists3;
                float4 dists7 = dists4 * dists3;

                const float4 kSinConsts = float4(1.f, -1.f/6.f, 1.f/120.f, -1.f/5040.f);
                const float4 kCosConsts = float4(1.f, -1.f/2.f, 1.f/24.f, -1.f/720.f);
                sines = dists + dists3 * kSinConsts.yyyy + dists5 * kSinConsts.zzzz + dists7 * kSinConsts.wwww;
                cosines = kCosConsts.xxxx + dists2 * kCosConsts.yyyy + dists4 * kCosConsts.zzzz + dists6 * kCosConsts.wwww;

                float4 filteredAmp = lengths * ooEdgeLength;
                filteredAmp = max(filteredAmp, 0.f);
                filteredAmp = min(filteredAmp, 1.f);
                filteredAmp = filteredAmp * scale; 
                filteredAmp = filteredAmp * amplitude;

                sines = sines * filteredAmp;
                cosines = cosines * filteredAmp * scale;
            }
            
            float3 FinitizeEyeRay(const in float3 cam2Vtx, const in float4 envAdjust)
            {
                // Compute our finitized eyeray.

                // Our "finitized" eyeray is:
                //	camPos + D * t - envCenter = D * t - (envCenter - camPos)
                // with
                //	D = (pos - camPos) / |pos - camPos| // normalized usual eyeray
                // and
                //	t = D dot F + sqrt( (D dot F)^2 - G )
                // with
                //	F = (envCenter - camPos)	=> envAdjust.xyz
                //	G = F^2 - R^2				=> nevAdjust.w
                // where R is the sphere radius.
                //
                // This all derives from the positive root of equation
                //	(camPos + (pos - camPos) * t - envCenter)^2 = R^2,
                // In other words, where on a sphere of radius R centered about envCenter
                // does the ray from the real camera position through this point hit.
                //
                // Note that F and G are both constants (one 3-point, one scalar).
                float dDotF = dot(cam2Vtx, float3(envAdjust.xyz));
                float t = dDotF + sqrt(dDotF * dDotF - envAdjust.w);
                return cam2Vtx * t - float3(envAdjust.xyz);
            }
            
            void CalcFinalColors(const in float3 norm, 
                                 const in float3 cam2Vtx, 
                                 const in float opacMin, 
                                 const in float opacScale,
                                 const in float colorFilter,
                                 const in float opacFilter,
                                 const in float4 tint,
                                 out float4 modColor,
                                 out float4 addColor)
            {
                // Calculate colors
                // Final color will be
                // rgb = Color1.rgb + Color0.rgb * envMap.rgb
                // alpha = Color0.a

                // Color 0

                // Vertex based Fresnel-esque effect.
                // Input vertex color.b limits how much we attenuate based on angle.
                // So we map 
                // (dot(norm,cam2Vtx)==0) => 1 for grazing angle
                // and (dot(norm,cam2Vtx)==1 => 1-In.Color.b for perpendicular view.
                float atten = 1.0 + dot(norm, cam2Vtx) * opacMin;

                // Filter the color based on depth
                modColor.rgb = colorFilter * atten;

                // Boost the alpha so the reflections fade out faster than the tint
                // and apply the input attenuation factors.
                modColor.a = (atten + 1.0) * 0.5 * opacFilter * opacScale * tint.a;

                // Color 1 is just a constant.
                addColor = tint;
            }
            
            // Normal, binormal, tangent

            // Okay, here we go:
            // W == sum(k w Dir.x^2 A sin())
            // V == sum(k w Dir.x Dir.y A sin())
            // U == sum(k w Dir.y^2 A sin())
            //
            // T == sum(A sin())
            //
            // S == sum(k Dir.x A cos())
            // R == sum(k Dir.y A cos())
            //
            // Q == sum(k w A cos())
            //
            // M == sum(A cos())
            //
            // P == sum(w Dir.x A cos())
            // N == sum(w Dir.y A cos())
            //
            // Then:
            // Pos = (in.x + S, in.y + R, waterheight + T)
            // 
            // Bin = (1 - W, -V, P)
            // Tan = (-V, 1 - U, N)
            // Nor = (-P, -N, 1 - Q)
            //
            // Remember we want the transpose of Binormal, Tangent, and Normal
            void CalcTangentBasis(const in float4 sines, 
                                  const in float4 cosines, 
                                  const in float4 dirXSqKW,
                                  const in float4 dirXDirYKW,
                                  const in float4 dirYSqKW,
                                  const in float4 dirXW,
                                  const in float4 dirYW,
                                  const in float4 KW,
                                  const in float pertAtten,
                                  const in float3 eyeRay,
                                  out float4 BTN_X,
                                  out float4 BTN_Y,
                                  out float4 BTN_Z,
                                  out float3 norm)
            {
                BTN_X.x = 1.f + dot(sines, -dirXSqKW);
                BTN_X.y = dot(sines, -dirXDirYKW);
                BTN_X.z = dot(cosines, -dirXW);
                BTN_X.xy = BTN_X.xy * pertAtten;
                norm.x = BTN_X.z;

                BTN_Y.x = dot(sines, -dirXDirYKW);
                BTN_Y.y = 1.f + dot(sines, -dirYSqKW);
                BTN_Y.z = dot(cosines, -dirYW);
                BTN_Y.xy = BTN_Y.xy * pertAtten;
                norm.y = BTN_Y.z;

                BTN_Z.x = dot(cosines, dirXW);
                BTN_Z.y = dot(cosines, dirYW);
                BTN_Z.z = (1.f + dot(sines, -KW));
                BTN_Z.xy = BTN_Z.xy * pertAtten;
                norm.z = BTN_Z.z;

                BTN_X.w = eyeRay.x;
                BTN_Y.w = eyeRay.z;
                BTN_Z.w = eyeRay.y;
            }

			v2f vert (appdata v)
			{
				v2f o;
                
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				
                float4 wPosMeters = mul(unity_ObjectToWorld, v.vertex);
                float4 wPos = float4(wPosMeters.xzy / .3048, wPosMeters.w);
                
                // Calculate ripple UV from position
                o.uv.xy = wPos.xy * cSpecAtten.ww;
                
                // Get our depth based filters.
                float3 dFilter = CalcDepthFilter(cDepthOffset, cDepthScale, wPos, cDepthOffset.w);
                
                float4 sines;
                float4 cosines;
                CalcSinCos(wPos,
                    cDirX, cDirY,
                    cAmplitude, cFrequency, cPhase,
                    cLengths, v.color.a, dFilter.z,
                    sines, cosines);
                
                wPos = CalcFinalPosition(wPos, sines, cosines, cDepthOffset.w, cDirXK, cDirYK);
                wPosMeters = float4(wPos.xzy * .3048, wPos.w);
                
                // We have our final position. We'll be needing normalized vector from camera 
                // to vertex several times, so we go ahead and grab it.
                float3 cam2Vtx;
                float pertAtten;
                CalcEyeRayAndBumpAttenuation(wPos, float4(_WorldSpaceCameraPos.xzy / .3048, 1), cSpecAtten, cam2Vtx, pertAtten);
                
                // Compute our finitized eyeray.
                float3 eyeRay = FinitizeEyeRay(cam2Vtx, cEnvAdjust);
                
                float3 norm;
                CalcTangentBasis(sines, cosines, 
                    cDirXSqKW,
                    cDirXDirYKW,
                    cDirYSqKW,
                    cDirXW,
                    cDirYW,
                    cKW,
                    pertAtten,
                    eyeRay,
                    o.BTN_X,
                    o.BTN_Y,
                    o.BTN_Z,
                    norm);
                
                // Calc screen position and fog
                o.vertex = mul(UNITY_MATRIX_VP, wPosMeters);
                o.worldPos = wPosMeters;
				UNITY_TRANSFER_FOG(o, o.vertex);
                
                #ifdef SOFTPARTICLES_ON
                o.projPos = ComputeScreenPos(o.vertex);
                COMPUTE_EYEDEPTH(o.projPos.z);
                #endif
                o.uvgrab = ComputeGrabScreenPos(o.vertex);
                
                CalcFinalColors(norm,
                                cam2Vtx,
                                v.color.b,
                                v.color.r,
                                dFilter.y,
                                dFilter.x,
                                cWaterTint,
                                o.color,
                                o.color2);
                
                // o.color = float4(pertAtten, pertAtten, pertAtten, 1);
                
				return o;
			}
			
			sampler2D tBumpMap;
			samplerCUBE tEnvMap;
            UNITY_DECLARE_DEPTH_TEXTURE(_CameraDepthTexture);
            float _InvFade;
            sampler2D _GrabTexture;
            float4 _GrabTexture_TexelSize;
            float _DistortionIntensity;
            fixed4 _Runtime;
            
			fixed4 frag (v2f i) : SV_Target
			{
                // We deviate a bit from the original shader here...
                // Basically, the water now distords the objects seen through it.
                // There is a second effect which allows fading the water when close
                // to another object (simulating a thinner water volume where the
                // water meets the shore - on top of the existing height-based
                // transparency). However this fx isn't perfect (based on viewport
                // depth and not distance, requires small faces...), so it's disabled
                // by default.
                
                // READ: the water transparency value (computed from the viewing angle etc)
                // is no longer used as pixel alpha blending value. Instead, we capture
                // the existing image using a grab pass, distord it using the water's normal,
                // then use the transparency value to lerp between the grabbed image and
                // the water itself.
                // Pixel alpha blending is used only by the aforementioned SOFTPARTICLES
                // effect (if it's enabled).
                
                // BUG: grab texture doesn't account for VR viewport, have to figure out why...
                
                // fade = [0..1] multiplicator for the transparency value.
                // computed from the viewport depth between the water pixel and the object below.
                float fade = 1;
                
                 // Comment out the following block if the fade effect doesn't work for your mesh
                // #ifdef SOFTPARTICLES_ON
                // float sceneZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
                // float partZ = i.projPos.z;
                // fade = saturate(_InvFade * (sceneZ-partZ));
                // #endif
                
                #if UNITY_SINGLE_PASS_STEREO
                i.uvgrab.xy = TransformStereoScreenSpaceTex(i.uvgrab.xy, i.uvgrab.w);
                #endif
                
				fixed4 norm = tex2D(tBumpMap, i.uv);
                norm.xyz = normalize((norm.xyz - 0.5) * 2);
                // norm.xyz = float3(0,0,1);
                
                // transform normal from tangent to world space
                half3 worldNormal;
                worldNormal.x = dot(i.BTN_X.yxz, norm.xyz);
                worldNormal.y = dot(i.BTN_Z.yxz, norm.xyz);
                worldNormal.z = dot(i.BTN_Y.yxz, norm.xyz);
                // worldNormal = normalize(worldNormal);
                // worldNormal.xyz = float3(0,1,0);
                // worldNormal.y *= .1;
                
                float4 origGrab = i.uvgrab;
                float2 offset = worldNormal.xz * _DistortionIntensity * _GrabTexture_TexelSize.xy;
                #ifdef UNITY_Z_0_FAR_FROM_CLIPSPACE //to handle recent standard asset package on older version of unity (before 5.5)
                    i.uvgrab.xy = offset * UNITY_Z_0_FAR_FROM_CLIPSPACE(i.uvgrab.z) + i.uvgrab.xy;
                #else
                    i.uvgrab.xy = offset * i.uvgrab.z + i.uvgrab.xy;
                #endif
                
                // get the image without the water on it
                half4 grabCol = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(i.uvgrab));
                
                // if we have a depth texture, try to avoid depth-errors when stretching the
                // grabbed image... This avoids close objects being reflected by far off water.
                #ifdef SOFTPARTICLES_ON
                float grabDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.uvgrab)));
                half4 grabColClear = tex2Dproj(_GrabTexture, UNITY_PROJ_COORD(origGrab));
                grabCol = (grabDepth > origGrab.w) ? grabCol : grabColClear;
                grabCol = grabColClear;
                #endif
                
                // Plamza per-vertex ray version
                // float3 eyeRay = float3(i.BTN_X.w, i.BTN_Y.w, i.BTN_Z.w);
                // half3 worldRefl = reflect(eyeRay.xzy, worldNormal);
                
                // Unity per-pixel ray version (much bettah)
                half3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
                half3 worldRefl = reflect(-worldViewDir, worldNormal);
                
                fixed4 cube = texCUBE(tEnvMap, worldRefl);
                
                fixed4 col = fixed4(
                    (cube * i.color * i.color2).rgb,
                    i.color.a
                );
                
                // add the base material's runtime color
                // this makes the water glow a bit which is dumb. But that's
                // Plamza for you.
                col.rgb += _Runtime.rgb;
                
                col.rgb = lerp(grabCol.rgb, col.rgb, col.a);
                col.a = 1;
                // col.rgb = grabCol.rgb;
                col.a *= fade;
                
				UNITY_APPLY_FOG(i.fogCoord, col);
                
                return col;
                // return i.color;
                // return i.color2;
                // return cube;
                // return float4(worldNormal * .5 + .5, 1);
                // return float4(float3(i.BTN_X.z, i.BTN_Y.z, i.BTN_Z.z) * .5 + .5, 1);
			}
			ENDCG
		}
	}
}
