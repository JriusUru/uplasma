﻿Shader "Plasma/Waving/WavingAlphaBlendYNoFog" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
        _WaveAndDistance ("Wave and distance", Vector) = (12, 1.14, 1.8, 1)
	}
	SubShader {
		Tags {"Queue"="Transparent" "RenderType"="Transparent"}
		LOD 200
        
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        
        // Note: due to how alpha blending works, we have to disable writing to the depth buffer to avoid errors
        // with potential DoF/AmbientOcclusion.

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alpha:blend nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa nofog vertex:PlWavingGrass
		#pragma target 3.0
        #include "TerrainEngine.cginc"

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
            float4 color : COLOR;
		};
        
        void PlWavingGrass(inout appdata_full v)
        {
            // this vertex shader uses Unity's own TerrainWaveGrass function to displace vertex.
            // (works well, why bother with porting the original code...)
            
            // set the wave offset - I guess this was originally set by Unity's terrain component...
            _WaveAndDistance.x = _Time.x;
            // compute wave amount (using Y uv, instead of Unity which uses color...)
            float waveAmount = (1-v.texcoord.y) * _WaveAndDistance.z;
            
            // TerrainWaveGrass also computes vertex color for distance attenuation and glass cluster color,
            // which we don't care about.
            fixed4 colorWhichWeDontCareAbout = 1;
            colorWhichWeDontCareAbout = TerrainWaveGrass (v.vertex, waveAmount, colorWhichWeDontCareAbout);
        }

		fixed4 _Color;
		fixed4 _Amb;
        uniform half _Opac;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
            o.Emission = c.rgb * (IN.color * _Color + _Amb);
			o.Alpha = c.a * _Opac;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
