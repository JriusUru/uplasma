﻿Shader "Plasma/Waving/WavingCutoutDoubleSidedX" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Cutoff ("cutoff", Range(0,1)) = .5
		_Cutoff2 ("cutoff (use same value as above)", Range(0,1)) = .5 // thank you unity /s
        _MipScale ("Mip Level Alpha Scale", Range(0,1)) = 0.25
        // x: wind speed, y: wave size, z: wind amount, w: max sqr distance
        // somehow x is not used.
        // y is useful for turbulences - closer to 0 means all grass blades wave the same way,
        //      higher means more havoc.
        // z is the max displacement distance
        // w is for grass fading, which we don't use
        _WaveAndDistance ("Wave and distance", Vector) = (12, 1.14, .7, 1)
		_TimeScale("Timescale multiplier", Range(0,10)) = 1
	}
	SubShader {
		Tags {"Queue"="AlphaTest" "RenderType"="Grass"}
		LOD 200
        
        Cull Off
        
        AlphaToMask On

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alphatest:_Cutoff vertex:PlWavingGrass nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0
        #include "TerrainEngine.cginc"

		sampler2D _MainTex;
        float4 _MainTex_TexelSize;

		struct Input {
			float2 uv_MainTex;
            float4 color : COLOR;
		};

		fixed4 _Color;
        uniform float _Cutoff2;
        uniform float _MipScale;
		fixed4 _Amb;
        uniform half _Opac;
		uniform half _TimeScale;
        
        void PlWavingGrass(inout appdata_full v)
        {
            // this vertex shader uses Unity's own TerrainWaveGrass function to displace vertex.
            // (works well, why bother with porting the original code...)
            
            // set the wave offset - I guess this was originally set by Unity's terrain component...
            _WaveAndDistance.x = _Time.x * _TimeScale;
            // compute wave amount (using Y uv, instead of Unity which uses color...)
            float waveAmount = (v.texcoord.x) * _WaveAndDistance.z;
            
            // TerrainWaveGrass also computes vertex color for distance attenuation and glass cluster color,
            // which we don't care about.
            fixed4 colorWhichWeDontCareAbout = 1;
            colorWhichWeDontCareAbout = TerrainWaveGrass (v.vertex, waveAmount, colorWhichWeDontCareAbout);
        }
        
        float CalcMipLevel(float2 texture_coord)
        {
            float2 dx = ddx(texture_coord);
            float2 dy = ddy(texture_coord);
            float delta_max_sqr = max(dot(dx, dx), dot(dy, dy));
            
            return max(0.0, 0.5 * log2(delta_max_sqr));
        }

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            
            // improve alpha for msaa
            c.a = (c.a - _Cutoff2) / max(fwidth(c.a), 0.0001) + 0.5;
            c.a *= 1 + CalcMipLevel(IN.uv_MainTex * _MainTex_TexelSize.zw) * _MipScale;
            c.a = saturate(c.a);
            
            c.rgb *= _Opac;
            
			o.Albedo = c.rgb;
            o.Emission = c.rgb * (IN.color * _Color + _Amb);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Legacy Shaders/Transparent/Cutout/VertexLit"
}
