﻿Shader "Plasma/Cube only" {
	Properties {
		_Cube ("Cubemap", CUBE) = "" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		samplerCUBE _Cube;

		struct Input {
            float4 color : COLOR;
            float3 worldRefl;
		};

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
            fixed3 cube = texCUBE(_Cube, IN.worldRefl).rgb;
			o.Albedo = 0;
            o.Emission = cube;
			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
