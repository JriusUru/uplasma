﻿Shader "Plasma/Default"
{
	Properties
    {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Runtime ("Runtime color", Color) = (1,1,1,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	}
	SubShader
    {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
        // #include "Assets/PlasmaLighting.cginc"
		// #pragma surface surf PlasmaLambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma surface surf Lambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input
        {
			float2 uv_MainTex;
            float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _Amb;
		fixed4 _Runtime;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		// void surf(Input IN, inout PlasmaSurfaceOutput o)
		void surf(Input IN, inout SurfaceOutput o)
        {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * _Runtime;
            o.Emission = c.rgb * (IN.color * _Color + _Amb);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
