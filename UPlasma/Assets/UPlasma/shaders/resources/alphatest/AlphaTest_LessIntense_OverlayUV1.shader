﻿Shader "Plasma/Default, alpha test (double sided), less intense, overlay uv1" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Color2 ("Color2", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("Tex2", 2D) = "white" {}
		_Cutoff ("cutoff", Range(0,1)) = .5
		_Cutoff2 ("cutoff (use same value as above)", Range(0,1)) = .5 // thank you unity /s
        _MipScale ("Mip Level Alpha Scale", Range(0,1)) = 0.25
	}
	SubShader {
		Tags {"Queue"="AlphaTest" "RenderType"="TransparentCutout"}
		LOD 200
        
        Cull Off
        
        AlphaToMask On

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alphatest:_Cutoff nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;
        float4 _MainTex_TexelSize;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_Tex2;
            float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _Color2;
        uniform float _Cutoff2;
        uniform float _MipScale;
		fixed4 _Amb;
        uniform half _Opac;
        
        float CalcMipLevel(float2 texture_coord)
        {
            float2 dx = ddx(texture_coord);
            float2 dy = ddy(texture_coord);
            float delta_max_sqr = max(dot(dx, dx), dot(dy, dy));
            
            return max(0.0, 0.5 * log2(delta_max_sqr));
        }

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 c2 = tex2D (_Tex2, IN.uv2_Tex2);
            
            c = lerp(c, c2, c.a * c2.a);
            
            // improve alpha for msaa
            c.a = (c.a - _Cutoff2) / max(fwidth(c.a), 0.0001) + 0.5;
            c.a *= 1 + CalcMipLevel(IN.uv_MainTex * _MainTex_TexelSize.zw) * _MipScale;
            c.a = saturate(c.a);
            
            c.rgb *= _Opac;
            
			o.Albedo = c.rgb;
            o.Emission = c.rgb * (IN.color * _Color + _Amb);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Legacy Shaders/Transparent/Cutout/VertexLit"
}
