﻿Shader "Plasma/Ages/Ahnonay Cathedral/glassGlobe" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Runtime ("Runtime color", Color) = (1,1,1,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Cube ("Cubemap", CUBE) = "" {}
	}
	SubShader {
		Tags {"Queue"="Transparent" "RenderType"="Transparent"}
		LOD 200
        
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alpha:blend nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		samplerCUBE _Cube;

		struct Input {
			float2 uv_MainTex;
            float4 color : COLOR;
            float3 worldRefl;
		};

		fixed4 _Color;
		fixed4 _Amb;
		fixed4 _Runtime;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            fixed4 cube = texCUBE(_Cube, IN.worldRefl);
            fixed4 cf = lerp(c, cube, .5);
            cf.a = max(c.a, .5);
			o.Albedo = c.rgb * _Runtime;
            o.Emission = cf.rgb * (IN.color * _Color + _Amb);
            o.Alpha = cf.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
