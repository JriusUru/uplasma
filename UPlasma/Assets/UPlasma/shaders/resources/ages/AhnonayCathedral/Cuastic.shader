﻿Shader "Plasma/Ages/Ahnonay Cathedral/Cuastic" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_Opac2 ("Opacity 2", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("Albedo 2 (RGB)", 2D) = "white" {}
	}

    Category {
        Tags { "Queue"="Transparent+50" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
        Blend One One
        ColorMask RGB
        Cull Off Lighting Off ZWrite Off
        Offset -1, 0
        
        SubShader {
            Pass {
            
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma target 2.0
                #pragma multi_compile_fog

                #include "UnityCG.cginc"

                sampler2D _MainTex;
                sampler2D _Tex2;
                fixed4 _Color;
                fixed4 _Amb;
                
                struct appdata_t {
                    float4 vertex : POSITION;
                    float2 texcoord : TEXCOORD0;
                    float4 color : COLOR;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct v2f {
                    float4 vertex : SV_POSITION;
                    float2 texcoord : TEXCOORD0;
                    float2 texcoord2 : TEXCOORD1;
                    float4 color : COLOR;
                    UNITY_FOG_COORDS(2)
                    UNITY_VERTEX_OUTPUT_STEREO
                };
                
                float4 _MainTex_ST;
                float4 _Tex2_ST;
                uniform half _Opac;

                v2f vert (appdata_t v)
                {
                    v2f o;
                    UNITY_SETUP_INSTANCE_ID(v);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
                    o.texcoord2 = TRANSFORM_TEX(v.texcoord,_Tex2);
                    o.color = v.color;
                    UNITY_TRANSFER_FOG(o,o.vertex);
                    return o;
                }
                
                fixed4 frag (v2f i) : SV_Target
                {
                    fixed4 col = tex2D(_MainTex, i.texcoord + float2(-1, 1) * _Time.y * .03) * i.color * _Opac;
                    fixed4 col2 = tex2D(_Tex2, i.texcoord + float2(-1, 0) * _Time.y * .03 + float2(.3, 0)) * i.color;
                    col.rgb += col2.rgb;
                    UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(0,0,0,0)); // fog towards black due to our blend mode
                    return col;
                }
                ENDCG 
            }
        }	
    }
}
