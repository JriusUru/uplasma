﻿Shader "Plasma/Ages/Ahnonay Cathedral/m_135" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Runtime ("Runtime color", Color) = (1,1,1,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("Albedo 2 (RGB)", 2D) = "white" {}
		_Cube ("Cubemap", CUBE) = "" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;
		samplerCUBE _Cube;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_Tex2;
            float4 color : COLOR;
            float3 worldRefl;
		};

		fixed4 _Color;
		fixed4 _Amb;
		fixed4 _Runtime;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex * float2(3.10858, 2.51728) + IN.uv_MainTex.yx * float2(3.10858, -2.51728) + float2(-0.312932, 0.204349));
			fixed4 c2 = tex2D (_Tex2, IN.uv2_Tex2);
            fixed3 cube = texCUBE(_Cube, IN.worldRefl).rgb;
            c = lerp(c, c2, c2.a * .3);
            c.rgb = lerp(cube, c.rgb, c.a);
			o.Albedo = c.rgb * _Runtime;
            o.Emission = c * (IN.color * _Color + _Amb);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
