﻿Shader "Plasma/Ages/EderTsogal/SunBack" {
    Properties {
        _Color ("Tint Color", Color) = (1,1,1,1)
        _Amb ("Ambient color", Color) = (0,0,0,1)
        _Opac ("Opacity", Range(0,1)) = 1
        _AlphaFactor ("Alpha factor", Range(0,5)) = 1
        _MainTex ("Glow Texture", 2D) = "white" {}
        _Tex2 ("Add Texture", 2D) = "black" {}
    }

    Category {
        Tags { "Queue"="Transparent+40" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
        
        // essentially the same as SunAlphaLOS, but with ztest on
        
        SubShader {
            Pass {
                Blend SrcAlpha OneMinusSrcAlpha
                ColorMask RGB
                Cull Off Lighting Off ZWrite Off
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma target 2.0

                #include "UnityCG.cginc"

                sampler2D _MainTex;
                fixed4 _Color;
                fixed4 _Amb;
                
                struct appdata_t {
                    float4 vertex : POSITION;
                    float2 texcoord : TEXCOORD0;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct v2f {
                    float4 vertex : SV_POSITION;
                    float2 texcoord : TEXCOORD0;
                    UNITY_VERTEX_OUTPUT_STEREO
                };
                
                float4 _MainTex_ST;
                float _AlphaFactor;
                uniform half _Opac;

                v2f vert (appdata_t v)
                {
                    v2f o;
                    UNITY_SETUP_INSTANCE_ID(v);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
                    return o;
                }
                
                fixed4 frag (v2f i) : SV_Target
                {
                    fixed4 col = _AlphaFactor * _Amb * tex2D(_MainTex, i.texcoord) * _Opac;
                    return col;
                }
                ENDCG 
            }
            Pass {
                Blend One One
                ColorMask RGB
                Cull Off Lighting Off ZWrite Off
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma target 2.0

                #include "UnityCG.cginc"

                sampler2D _Tex2;
                
                struct appdata_t {
                    float4 vertex : POSITION;
                    float2 texcoord : TEXCOORD0;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct v2f {
                    float4 vertex : SV_POSITION;
                    float2 texcoord : TEXCOORD0;
                    UNITY_VERTEX_OUTPUT_STEREO
                };
                
                float4 _Tex2_ST;

                v2f vert (appdata_t v)
                {
                    v2f o;
                    UNITY_SETUP_INSTANCE_ID(v);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.texcoord = TRANSFORM_TEX(v.texcoord,_Tex2);
                    return o;
                }
                
                fixed4 frag (v2f i) : SV_Target
                {
                    fixed4 col = tex2D(_Tex2, i.texcoord);
                    col = pow(col, 2.2);
                    return col;
                }
                ENDCG 
            }
        }	
    }
}
