﻿Shader "Plasma/Ages/Nexus/Light_2_Lamps" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_FadeDist ("Fade dist", Range(0,500)) = 40
	}
	SubShader {
		Tags {"Queue"="Transparent" "RenderType"="Transparent"}
		LOD 200
        
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alpha:blend nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa vertex:vert
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
            float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _Amb;
        uniform half _Opac;
        uniform half _FadeDist;
            
        void vert (inout appdata_full v) {
            v.color.a *= saturate(length(WorldSpaceViewDir(v.vertex)) / _FadeDist);
        }

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex + float2(.06, .03) * _Time.y);
			o.Albedo = c.rgb * IN.color;
            o.Emission = c.rgb * (IN.color * _Color + _Amb);
            
			o.Alpha = c.a * _Opac * IN.color.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
