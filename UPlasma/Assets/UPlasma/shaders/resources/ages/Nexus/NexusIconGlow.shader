Shader "Plasma/Ages/Nexus/NexusIconGlow" {
Properties {
	_Color ("Tint Color", Color) = (1,1,1,1)
    _Amb ("Ambient color", Color) = (0,0,0,1)
    _Opac ("Opacity", Range(0,1)) = 1
	_AlphaFactor ("Alpha factor", Range(0,5)) = 1
	_MainTex ("Glow Texture", 2D) = "white" {}
	_Tex2 ("Noise tex", 2D) = "white" {}
}

Category {
	Tags { "Queue"="Transparent+50" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend SrcAlpha One
	ColorMask RGB
	Cull Off Lighting Off ZWrite Off
	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _Tex2;
			fixed4 _Color;
			fixed4 _Amb;
			
			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float2 texcoord2 : TEXCOORD1;
				UNITY_FOG_COORDS(2)
				UNITY_VERTEX_OUTPUT_STEREO
			};
			
			float4 _MainTex_ST;
			float4 _Tex2_ST;
            float _AlphaFactor;
            uniform half _Opac;

			v2f vert (appdata_t v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				o.texcoord2 = TRANSFORM_TEX(v.texcoord,_Tex2);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = _AlphaFactor * _Amb * tex2D(_MainTex, i.texcoord) * _Opac;
				col.a *= tex2D(_Tex2, i.texcoord2 + float2(15, 0) * _Time.y).a;
				UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(0,0,0,0)); // fog towards black due to our blend mode
				return col;
			}
			ENDCG 
		}
	}	
}
}
