﻿Shader "Plasma/Ages/Nexus/7_-_Default4_nexusBookMachineFloor_LM" {
	Properties {
		// _Color ("Color", Color) = (1,1,1,1)
		// _Color2 ("Color2", Color) = (1,1,1,1)
		// _Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Cube ("Cubemap", CUBE) = "" {}
		_Lightmap ("Lightmap", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Lightmap;
		samplerCUBE _Cube;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_Lightmap;
            float4 color : COLOR;
            float3 worldRefl;
		};

		// fixed4 _Color;
		// fixed4 _Color2;
		fixed4 _Amb;
        // uniform half _Opac;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 lm = tex2D (_Lightmap, IN.uv2_Lightmap);
            fixed3 cube = texCUBE(_Cube, IN.worldRefl).rgb;
			o.Albedo = c * IN.color;
            o.Emission = lerp(cube, c.rgb, c.a) * IN.color * lm;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
