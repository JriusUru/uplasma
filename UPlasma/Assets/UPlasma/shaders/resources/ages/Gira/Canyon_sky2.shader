Shader "Plasma/Ages/Gira/Canyon_sky2" {
Properties {
	_Color ("Color", Color) = (1,1,1,1)
    _Amb ("Ambient color", Color) = (0,0,0,1)
    _Opac ("Opacity", Range(0,1)) = 1
	_MainTex ("Glow Texture", 2D) = "white" {}
    _Tex2 ("Tex2", 2D) = "white" {}
    _Tex3 ("Tex3", 2D) = "white" {}
}

Category {
    Tags { "RenderType"="Opaque" }
	ColorMask RGB
	Cull Off Lighting Off ZWrite On
	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _Tex2;
			sampler2D _Tex3;
			fixed4 _Color;
			fixed4 _Amb;
			
			struct appdata_t {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float2 texcoord2 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float2 texcoord2 : TEXCOORD2;
				float2 texcoord3 : TEXCOORD3;
				UNITY_FOG_COORDS(1)
				UNITY_VERTEX_OUTPUT_STEREO
			};
			
			float4 _MainTex_ST;
			float4 _Tex2_ST;
			float4 _Tex3_ST;
            uniform half _Opac;

			v2f vert (appdata_t v)
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				o.texcoord2 = TRANSFORM_TEX(v.texcoord2,_Tex2);
				o.texcoord3 = TRANSFORM_TEX(v.texcoord2,_Tex3);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// fixed4 col = _Amb * tex2D(_MainTex, i.texcoord) * _Opac;
				fixed4 col = _Amb * tex2D(_Tex3, i.texcoord3) * _Opac;
				col += _Amb * tex2D(_Tex2, i.texcoord2) * _Opac;
				UNITY_APPLY_FOG_COLOR(i.fogCoord, col, fixed4(0,0,0,0)); // fog towards black due to our blend mode
				return col;
			}
			ENDCG 
		}
	}	
}
}
