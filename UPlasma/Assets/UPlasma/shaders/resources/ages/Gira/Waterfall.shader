﻿Shader "Plasma/Ages/Gira/Waterfall" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("Tex2", 2D) = "white" {}
	}
	SubShader {
		Tags {"Queue"="Transparent+200" "RenderType"="Transparent"}
        
        // simply render to the depth buffer first so face sorting is *slightly* better
        Pass {
            ColorMask 0
            Cull Off
        }
        
		LOD 200
        
        ZWrite Off
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha
        
        // Note: due to how alpha blending works, we have to disable writing to the depth buffer to avoid errors
        // with potential DoF/AmbientOcclusion.

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alpha:blend nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Tex2;
            float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _Amb;
        uniform half _Opac;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex + float2(0, _Time.y * .75));
			fixed4 c2 = tex2D (_Tex2, IN.uv_Tex2 + float2(0, _Time.y * 1.5));
            c.rgb *= lerp(1, c2.rgb, c2.a);
			o.Albedo = c.rgb;
            o.Emission = c.rgb * (IN.color * _Color + _Amb);
			o.Alpha = c.a * _Opac * IN.color.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
