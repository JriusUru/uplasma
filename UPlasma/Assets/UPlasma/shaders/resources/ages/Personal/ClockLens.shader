﻿Shader "Plasma/Ages/Personal/ClockLens" {
	Properties {
		// _Color ("Color", Color) = (1,1,1,1)
		// _Color2 ("Color2", Color) = (1,1,1,1)
		// _Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("Tex2", 2D) = "white" {}
		_Cube ("Cubemap", CUBE) = "" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;
		samplerCUBE _Cube;

		struct Input {
			float2 uv_MainTex;
			float2 uv2_Tex2;
            float4 color : COLOR;
            float3 worldRefl;
		};

		// fixed4 _Color;
		// fixed4 _Color2;
		fixed4 _Amb;
        // uniform half _Opac;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 c2 = tex2D (_Tex2, IN.uv2_Tex2);
            fixed3 cube = texCUBE(_Cube, IN.worldRefl).rgb;
			o.Albedo = lerp(c, c2, c2.a) * IN.color;
            o.Emission = lerp(c.rgb + cube, c2, c2.a) * IN.color;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
