﻿Shader "Plasma/Ages/Personal/IslandButte_7" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Color2 ("Color", Color) = (1,1,1,1)
		_Color3 ("Color", Color) = (1,1,1,1)
		_Color4 ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("", 2D) = "white" {}
		_Tex3 ("", 2D) = "white" {}
		_Tex4 ("", 2D) = "white" {}
        [HideInInspector]
		_Hack ("(hack, leave empty)", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;
		sampler2D _Tex3;
		sampler2D _Tex4;

		struct Input {
			float2 uv2_MainTex;
			float2 uv2_Tex2;
			float2 uv_Tex3;
			float2 uv2_Tex4;
			float2 uv3_Hack;
            float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _Color2;
		fixed4 _Color3;
		fixed4 _Color4;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv2_MainTex) * _Color;
			fixed4 c2 = tex2D (_Tex2, IN.uv2_Tex2) * _Color2;
			fixed4 c3 = tex2D (_Tex3, IN.uv_Tex3) * _Color3;
			fixed4 c4 = tex2D (_Tex4, IN.uv2_Tex4) * _Color4;
            c = lerp(c, c2, c2.a);
            c = lerp(c, c3, IN.uv3_Hack.x);
            c = lerp(c, c4, IN.uv3_Hack.y);
			o.Albedo = c.rgb * IN.color;
            o.Emission = c.rgb * IN.color;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
