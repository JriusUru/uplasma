﻿Shader "Plasma/Ages/Personal/Fogcloud-1" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("", 2D) = "white" {}
        [HideInInspector]
		_Hack ("(hack, leave empty)", 2D) = "white" {}
	}
	SubShader {
		Tags {"Queue"="Transparent+20" "RenderType"="Transparent"}
		LOD 200
        
        ZWrite Off
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha
        
        // Note: due to how alpha blending works, we have to disable writing to the depth buffer to avoid errors
        // with potential DoF/AmbientOcclusion.

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alpha:blend nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Tex2;
			float2 uv2_Hack;
            float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _Amb;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex + fixed2(1, 1) * _Time.y * .01515);
			fixed4 c2 = tex2D (_Tex2, IN.uv_Tex2 + fixed2(1, 1) * _Time.y * .0025);
            c.rgb = lerp(c.rgb, c2.rgb, c2.a);
            c.a = max(c.a, c2.a);
			o.Albedo = c.rgb * IN.color;
            o.Emission = saturate(c.rgb * _Color * IN.color + _Amb);
			o.Alpha = c.a * IN.uv2_Hack.x;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
