﻿Shader "Plasma/Ages/Personal/Alpha test, higher cutout (no DS)" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Cutoff ("cutoff", Range(0,1)) = .5
		_Cutoff2 ("cutoff (use same value as above)", Range(0,1)) = .5 // thank you unity /s
        _MipScale ("Mip Level Alpha Scale", Range(0,1)) = 0.25
	}
	SubShader {
		Tags {"Queue"="AlphaTest" "RenderType"="TransparentCutout"}
		LOD 200
        
        /*
        Now this is going to be a bit complex.
        
        Alpha blend is great in that it's visually pleasant and coherent. However, it makes automatic face sorting near-impossible
        (I don't know if/how Plasma solves it, it's possible to simply reorder the faces based on distance on the CPU then make 1
        draw call per face - which in terms of performance is a huge bottleneck - however I'm not aware if it's possible on the GPU,
        and Unity doesn't even allow this. Either way, intersecting faces will still have the problem, so screw this).
        And no face sorting is something I will not tolerate in VR.
        
        Interestingly enough, it's incompatible with shadows on deferred rendering (not that we care), which also means it will look
        odd using some deferred-only FX (AO, SSR... we do care about those). No matter what, I'd like to retain max compatibility with
        both render paths as deferred will look better on 2D, and forward will run faster on VR (since Myst people like running the
        games on toasters, this can be helpful).
        
        Back on topic, alpha test is much better since there is no partial transparency and thus no overdraw problem, as long as you
        write to the Z buffer. It's fully compatible with deferred too. However it looks different (duh) and it's prone to LOTS of
        aliasing (think an entire grass field with lots of aliasing, blech).
        
        Let's try to cut short to my ramblings: since we have no "true" efficient face sorting on Unity, we'll simply resort to
        using cutout shaders on all vegetations and non-overlay meshes (for max compatibility).
        Deferred rendering will rely on its super-crappy postFX for AA (or downsampling).
        There is fortunately a pretty cool workaround to make cutout work with MSAA on forward rendering, using advanced majyyks.
        Details can be found here: 
        https://medium.com/@bgolus/anti-aliased-alpha-test-the-esoteric-alpha-to-coverage-8b177335ae4f
        
        Result ranges from great to meh (pine needles, but those are asking for trouble with mipmapping).
        */
        
        // NOTE: still highly experimental. Have to run more tests before I can assess it's working as it should
        
        AlphaToMask On

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows alphatest:_Cutoff nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
        float4 _MainTex_TexelSize;

		struct Input {
			float2 uv_MainTex;
            float4 color : COLOR;
		};

		fixed4 _Color;
        uniform float _Cutoff2;
        uniform float _MipScale;
        
        float CalcMipLevel(float2 texture_coord)
        {
            float2 dx = ddx(texture_coord);
            float2 dy = ddy(texture_coord);
            float delta_max_sqr = max(dot(dx, dx), dot(dy, dy));
            
            return max(0.0, 0.5 * log2(delta_max_sqr));
        }

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            
            // improve alpha for msaa
            c.a = (c.a - _Cutoff2) / max(fwidth(c.a), 0.0001) + 0.5;
            c.a *= 1 + CalcMipLevel(IN.uv_MainTex * _MainTex_TexelSize.zw) * _MipScale;
            c.a = saturate(c.a);
            
			o.Albedo = c.rgb * IN.color;
            o.Emission = c.rgb * IN.color;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Legacy Shaders/Transparent/Cutout/VertexLit"
}
