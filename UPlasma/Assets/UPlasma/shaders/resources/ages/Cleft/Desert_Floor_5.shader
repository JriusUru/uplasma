﻿Shader "Plasma/Ages/Cleft/Desert_Floor_5" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Amb ("Ambient color", Color) = (0,0,0,1)
		_Opac ("Opacity", Range(0,1)) = 1
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tex2 ("Tex2", 2D) = "white" {}
		_Tex3 ("Tex3", 2D) = "white" {}
		_Tex4 ("Tex4", 2D) = "white" {}
        [HideInInspector]
		_Hack ("", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
        
        // Blend Zero One

		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows nolightmap nodynlightmap nodirlightmap nolppv noshadowmask nometa
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;
		sampler2D _Tex3;
		sampler2D _Tex4;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Tex2;
			float2 uv_Tex3;
			float2 uv_Tex4;
			float2 uv2_Hack;
            float4 color : COLOR;
		};

		fixed4 _Color;
		fixed4 _Amb;
        // uniform half _Opac;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 c2 = tex2D (_Tex2, IN.uv_Tex2);
			fixed4 c3 = tex2D (_Tex3, IN.uv_Tex3.yx * float2(-75,100));
			fixed4 c4 = tex2D (_Tex4, IN.uv_Tex4 * float2(-857.167,-857.167) + IN.uv_Tex4.yx * float2(-515.038,515.038));
            c.rgb = lerp(lerp(c.rgb, c2.rgb, c2.a), lerp(c3.rgb, c4.rgb, c4.a), IN.uv2_Hack.y);
			o.Albedo = c.rgb;
            o.Emission = c * (_Color * IN.color + _Amb);
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
