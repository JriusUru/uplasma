Shader "Plasma/Null" { // seriously, Cyan, for fuck's sake.
Properties {
    _Color ("Color", Color) = (1,1,1,1)
    _Amb ("Ambient color", Color) = (0,0,0,1)
    _Opac ("Opacity", Range(0,1)) = 1
    _MainTex ("Albedo (RGB)", 2D) = "white" {}
}

Category {
	Tags { "Queue"="Transparent+50" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" }
	Blend One Zero
	Cull Off Lighting Off ZWrite Off
	
	SubShader {
		Pass {
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0

			#include "UnityCG.cginc"
			
			struct appdata_t {
			};

			struct v2f {
			};

			v2f vert (appdata_t v)
			{
				v2f o;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return 0;
			}
			ENDCG 
		}
	}	
}
}
