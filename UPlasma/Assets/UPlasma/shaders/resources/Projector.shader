Shader "Plasma/ProjectorLight" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
        [NoScaleOffset]
        _MainTex ("Texture", 2D) = "white" {}
    }
    
    Subshader {
        Tags {"Queue"="Transparent"}
        Pass {
            ZWrite Off
            Ztest Equal
            ColorMask RGB
            // Blend DstColor One
            // Blend SrcAlpha OneMinusSrcAlpha
            // Blend One One
            Blend SrcAlpha One
            // Blend DstColor Zero
            // Offset -1, -1
    
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            #include "UnityCG.cginc"
            
            struct v2f {
                float4 uvShadow : TEXCOORD0;
                float4 uvFalloff : TEXCOORD1;
                UNITY_FOG_COORDS(2)
                float4 pos : SV_POSITION;
            };
            
            float4x4 unity_Projector;
            float4x4 unity_ProjectorClip;
            
            v2f vert (float4 vertex : POSITION)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(vertex);
                o.uvShadow = mul (unity_Projector, vertex);
                o.uvFalloff = mul (unity_ProjectorClip, vertex);
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            
            fixed4 _Color;
            sampler2D _MainTex;
            
            fixed4 frag (v2f i) : SV_Target
            {
                float4 coords = UNITY_PROJ_COORD(i.uvShadow);
                // coords.y *= -1;
                fixed4 texS = tex2Dproj (_MainTex, coords);
                texS.rgb *= _Color.rgb;
                // texS.a = 1.0-texS.a;
    
                // fixed4 texF = tex2Dproj (_FalloffTex, UNITY_PROJ_COORD(i.uvFalloff));
                // fixed4 res = texS * texF.a;
                // res.a = 1;
                
                float x = UNITY_PROJ_COORD(i.uvFalloff).x;
                half factor = (x < .01) ?
                    0 :
                    ((x > .99) ? 0 : 1);
                fixed4 res = texS * factor;

                UNITY_APPLY_FOG_COLOR(i.fogCoord, res, fixed4(0,0,0,0));
                return res;
            }
            ENDCG
        }
    }
}
