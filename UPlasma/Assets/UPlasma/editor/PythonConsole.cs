﻿/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace UPlasma
{
    /// <summary>
    /// Editor console window for our Python engine logging.
    /// Shamelessly stolen from those tutorials:
    /// http://gram.gs/gramlog/creating-clone-unitys-console-window/
    /// https://yatanasov.com/2017/02/02/recreating-the-unity-console-tips-and-trick-for-editor-scripting-ii/
    /// </summary>
    public class PythonConsole : EditorWindow, IHasCustomMenu
    {
        [Serializable]
        public class PythonLog
        {
            public bool isSelected;
            public string message;
            public string stackTrace;
            public LogType type;
            public DateTime dateTime;
            public bool collapsedIntoOther = false;
            public int collapseCount = 0;
            public string[] fullMessageLines;
            public UnityEngine.Object caller;

            public PythonLog(string msg, string trace, UnityEngine.Object callerObj, LogType t)
            {
                isSelected = false;
                message = msg;
                stackTrace = trace;
                type = t;
                dateTime = DateTime.Now;
                caller = callerObj;

                string fullMessage = message + "\n" + stackTrace;
                fullMessageLines = fullMessage.Split('\n');
            }
        }
        
        private Rect upperPanel;
        private Rect lowerPanel;
        private Rect resizer;
        private Rect menuBar;

        private float lineHeight = 19;
        private int numLines = 1;

        private float sizeRatio = 0.7f;
        private bool isResizing;

        private float resizerHeight = 5f;
        private float menuBarHeight = 20f;

        private int numAllowedLines = 4;

        private bool collapse = false;
        private bool clearOnPlay = true;
        private bool errorPause = false;
        private bool showLog = true;
        private bool showWarnings = true;
        private bool showErrors = true;
        private bool showTimestamp = false;

        private Vector2 upperPanelScroll;
        private Vector2 lowerPanelScroll;

        private GUIStyle resizerStyle;
        private GUIStyle boxStyle;
        private GUIStyle textAreaStyle;
        private GUIStyle toolbarButtonStyle;

        private GUIContent clearContent;
        private GUIContent collapseContent;
#pragma warning disable 414
        private GUIContent clearOnPlayContent;
#pragma warning restore 414
        private GUIContent errorPauseContent;
        
        private GUIContent menuOpenPythonLogContent;
        private GUIContent menuShowTimestampContent;
        private GUIContent[] menuLogEntryContents;

        private Texture2D boxBgOdd;
        private Texture2D boxBgEven;
        private Texture2D boxBgSelected;
        private Texture2D icon;

        private Texture2D errorIcon;
        private Texture2D errorIconSmall;
        private Texture2D warningIcon;
        private Texture2D warningIconSmall;
        private Texture2D infoIcon;
        private Texture2D infoIconSmall;
        private Texture2D errorIconInactiveSmall;
        private Texture2D warningIconInactiveSmall;
        private Texture2D infoIconInactiveSmall;

        private Texture2D windowIcon;

        private PythonLog selectedLog;
        private int numLog;
        private int numLogWarn;
        private int numLogError;

        private string logsDir;

        [SerializeField]
        public static List<PythonLog> logs;

        [MenuItem("Window/Python console")]
        public static void CreateWindow()
        {
            GetWindow<PythonConsole>();
        }

        private void OnEnable()
        {
            showLog = EditorPrefs.GetBool("PythonConsole_showLog", showLog);
            showWarnings = EditorPrefs.GetBool("PythonConsole_showWarnings", showWarnings);
            showErrors = EditorPrefs.GetBool("PythonConsole_showErrors", showErrors);
            sizeRatio = EditorPrefs.GetFloat("PythonConsole_sizeRatio", sizeRatio);
            
            collapse = EditorPrefs.GetBool("PythonConsole_collapse", collapse);
            clearOnPlay = EditorPrefs.GetBool("PythonConsole_clearOnPlay", clearOnPlay);
            errorPause = EditorPrefs.GetBool("PythonConsole_errorPause", errorPause);

            showTimestamp = EditorPrefs.GetBool("PythonConsole_showTimestamp", showTimestamp);
            numLines = EditorPrefs.GetInt("PythonConsole_numLines", numLines);
            SetNumLines(numLines, false);

            if (logs == null)
                logs = new List<PythonLog>();

            //if (clearOnPlay && EditorApplication.isPlaying)
            //{
            //    logs.Clear();
            //    selectedLog = null;
            //}

            errorIcon = EditorGUIUtility.Load("icons/console.erroricon.png") as Texture2D;
            warningIcon = EditorGUIUtility.Load("icons/console.warnicon.png") as Texture2D;
            infoIcon = EditorGUIUtility.Load("icons/console.infoicon.png") as Texture2D;

            errorIconSmall = EditorGUIUtility.Load("icons/console.erroricon.sml.png") as Texture2D;
            warningIconSmall = EditorGUIUtility.Load("icons/console.warnicon.sml.png") as Texture2D;
            infoIconSmall = EditorGUIUtility.Load("icons/console.infoicon.sml.png") as Texture2D;

            errorIconInactiveSmall = EditorGUIUtility.Load("icons/console.erroricon.inactive.sml.png") as Texture2D;
            warningIconInactiveSmall = EditorGUIUtility.Load("icons/console.warnicon.inactive.sml.png") as Texture2D;
            infoIconInactiveSmall = infoIconSmall; // there is no inactive icon for normal log

            windowIcon = EditorGUIUtility.Load("icons/UnityEditor.ConsoleWindow.png") as Texture2D;
            titleContent = new GUIContent("Python console", windowIcon);

            resizerStyle = new GUIStyle();
            resizerStyle.normal.background = EditorGUIUtility.Load("icons/d_AvatarBlendBackground.png") as Texture2D;

            boxStyle = new GUIStyle();
            boxStyle.normal.textColor = new Color(0.7f, 0.7f, 0.7f);
            //boxStyle.wordWrap = true;

            boxBgOdd = EditorGUIUtility.Load("builtin skins/darkskin/images/cn entrybackodd.png") as Texture2D;
            boxBgEven = EditorGUIUtility.Load("builtin skins/darkskin/images/cnentrybackeven.png") as Texture2D;
            boxBgSelected = EditorGUIUtility.Load("builtin skins/darkskin/images/menuitemhover.png") as Texture2D;

            textAreaStyle = new GUIStyle();
            textAreaStyle.normal.textColor = new Color(0.9f, 0.9f, 0.9f);
            textAreaStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/projectbrowsericonareabg.png") as Texture2D;
            textAreaStyle.wordWrap = true;

            try
            {
                toolbarButtonStyle = EditorStyles.toolbarButton;
            }
            catch (NullReferenceException) // Unity bug. Might happen.
            {
                toolbarButtonStyle = null;
            }

            clearContent = new GUIContent("Clear");
            collapseContent = new GUIContent("Collapse");
            clearOnPlayContent = new GUIContent("Clear on Play");
            errorPauseContent = new GUIContent("Error Pause");

            menuOpenPythonLogContent = new GUIContent("Open Python Log");
            menuShowTimestampContent = new GUIContent("Show Timestamp");
            
            menuLogEntryContents = new GUIContent[numAllowedLines];
            for (int i = 0; i < numAllowedLines; i++)
                menuLogEntryContents[i] = new GUIContent(
                    "Log Entry/" +
                    (i+1).ToString() +
                    ((i > 1) ? " lines" : " line"));

            selectedLog = null;

            PythonManager.LogMessageReceived += LogMessageReceived;
        }

        private void OnDisable()
        {
            EditorPrefs.SetBool("PythonConsole_showLog", showLog);
            EditorPrefs.SetBool("PythonConsole_showWarnings", showWarnings);
            EditorPrefs.SetBool("PythonConsole_showErrors", showErrors);
            EditorPrefs.SetFloat("PythonConsole_sizeRatio", sizeRatio);

            EditorPrefs.SetBool("PythonConsole_collapse", collapse);
            EditorPrefs.SetBool("PythonConsole_clearOnPlay", clearOnPlay);
            EditorPrefs.SetBool("PythonConsole_errorPause", errorPause);

            EditorPrefs.SetBool("PythonConsole_showTimestamp", showTimestamp);
            EditorPrefs.SetInt("PythonConsole_numLines", numLines);

            PythonManager.LogMessageReceived -= LogMessageReceived;
        }

        private void OnDestroy()
        {
            PythonManager.LogMessageReceived -= LogMessageReceived;
        }

        private void LogMessageReceived(string msg, string stackTrace, UnityEngine.Object caller, LogType type)
        {
            PythonLog l = new PythonLog(msg, stackTrace, caller, type);
            foreach (PythonLog prevLog in logs)
            {
                if (prevLog.message == l.message)
                {
                    l.collapsedIntoOther = true;
                    prevLog.collapseCount++;
                    break;
                }
            }
            logs.Add(l);
            if (errorPause)
            {
                foreach (PythonLog log in logs)
                    log.isSelected = false;
                l.isSelected = true;
                selectedLog = l;
                Debug.Break();
            }
            Repaint();
        }

        private void OnGUI()
        {
            DrawMenuBar();
            DrawUpperPanel();
            DrawLowerPanel();
            DrawResizer();

            ProcessEvents(Event.current);

            if (GUI.changed) Repaint();

            if (EditorApplication.isPlaying)
                logsDir = Path.Combine(GameManager.Instance.userDataPath, "logs");
        }

        private void DrawMenuBar()
        {
            menuBar = new Rect(0, 0, position.width, menuBarHeight);

            GUILayout.BeginArea(menuBar, EditorStyles.toolbar);
            GUILayout.BeginHorizontal();

            if (toolbarButtonStyle == null)
                toolbarButtonStyle = EditorStyles.toolbarButton;

            bool clear = GUILayout.Button(clearContent, toolbarButtonStyle,
                GUILayout.Width(toolbarButtonStyle.CalcSize(clearContent).x));

            if (clear)
            {
                logs.Clear();
                selectedLog = null;
            }

            GUILayout.Space(7);

            collapse = GUILayout.Toggle(collapse, collapseContent,
                toolbarButtonStyle, GUILayout.Width(toolbarButtonStyle.CalcSize(collapseContent).x));
            // doesn't work due to Unity refusing to serialize it :rollseyes:
            //clearOnPlay = GUILayout.Toggle(clearOnPlay, clearOnPlayContent,
            //    toolbarButtonStyle, GUILayout.Width(toolbarButtonStyle.CalcSize(clearOnPlayContent).x));
            errorPause = GUILayout.Toggle(errorPause, errorPauseContent,
                toolbarButtonStyle, GUILayout.Width(toolbarButtonStyle.CalcSize(errorPauseContent).x));

            GUILayout.FlexibleSpace();

            numLog = 0;
            numLogWarn = 0;
            numLogError = 0;
            foreach (PythonLog log in logs)
            {
                switch (log.type)
                {
                    case LogType.Error:
                    case LogType.Exception:
                        numLogError++;
                        break;
                    case LogType.Warning:
                        numLogWarn++;
                        break;
                    default:
                        numLog++;
                        break;
                }
            }
            
            GUIContent logContent = new GUIContent(numLog.ToString(),
                (numLog > 0) ? infoIconSmall : infoIconInactiveSmall);
            GUIContent warnContent = new GUIContent(numLogWarn.ToString(),
                (numLogWarn > 0) ? warningIconSmall : warningIconInactiveSmall);
            GUIContent errContent = new GUIContent(numLogError.ToString(),
                (numLogError > 0) ? errorIconSmall : errorIconInactiveSmall);
            showLog = GUILayout.Toggle(showLog, logContent,
                toolbarButtonStyle, GUILayout.Width(toolbarButtonStyle.CalcSize(logContent).x));
            showWarnings = GUILayout.Toggle(showWarnings, warnContent,
                toolbarButtonStyle, GUILayout.Width(toolbarButtonStyle.CalcSize(warnContent).x));
            showErrors = GUILayout.Toggle(showErrors, errContent,
                toolbarButtonStyle, GUILayout.Width(toolbarButtonStyle.CalcSize(errContent).x));

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        private void DrawUpperPanel()
        {
            upperPanel = new Rect(0, menuBarHeight, position.width, (position.height * sizeRatio) - menuBarHeight);

            GUILayout.BeginArea(upperPanel);
            upperPanelScroll = GUILayout.BeginScrollView(upperPanelScroll);

            int i = 0;
            if (logs != null)
            {
                foreach (PythonLog log in logs)
                {
                    // ignore message types if they are unchecked
                    if (log.type == LogType.Error || log.type == LogType.Exception)
                    {
                        if (!showErrors)
                            continue;
                    }
                    else if (log.type == LogType.Warning)
                    {
                        if (!showWarnings)
                            continue;
                    }
                    else
                    {
                        if (!showLog)
                            continue;
                    }
                    if (log.collapsedIntoOther && collapse)
                        continue;

                    string[] logContentSelect = new string[numLines];
                    for (int j = 0; j < numLines && j < log.fullMessageLines.Length; j++)
                        logContentSelect[j] = log.fullMessageLines[j] + '\n';
                    string logContentFull = string.Concat(logContentSelect);
                    if (DrawBox(logContentFull, log.type, i % 2 == 0, log.isSelected))
                    {
                        if (selectedLog != null)
                            selectedLog.isSelected = false;

                        log.isSelected = true;
                        selectedLog = log;
                        GUI.changed = true;
                        if (log.caller)
                            EditorGUIUtility.PingObject(log.caller);
                    }
                    i++;
                }
            }

            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        private void DrawLowerPanel()
        {
            lowerPanel = new Rect(0, (position.height * sizeRatio) + resizerHeight, position.width, (position.height * (1 - sizeRatio)) - resizerHeight);

            GUILayout.BeginArea(lowerPanel);
            lowerPanelScroll = GUILayout.BeginScrollView(lowerPanelScroll);

            if (selectedLog != null)
                GUILayout.TextArea(string.Concat(selectedLog.message, '\n', selectedLog.stackTrace), textAreaStyle);

            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        private void DrawResizer()
        {
            resizer = new Rect(0, (position.height * sizeRatio) - resizerHeight, position.width, resizerHeight * 2);

            GUILayout.BeginArea(new Rect(resizer.position + (Vector2.up * resizerHeight), new Vector2(position.width, 2)), resizerStyle);
            GUILayout.EndArea();

            EditorGUIUtility.AddCursorRect(resizer, MouseCursor.ResizeVertical);
        }
        private bool DrawBox(string content, LogType boxType, bool isOdd, bool isSelected)
        {
            if (isSelected)
                boxStyle.normal.background = boxBgSelected;
            else
            {
                if (isOdd)
                    boxStyle.normal.background = boxBgOdd;
                else
                    boxStyle.normal.background = boxBgEven;
            }

            switch (boxType)
            {
                case LogType.Error:     icon = errorIcon;   break;
                case LogType.Exception: icon = errorIcon;   break;
                case LogType.Assert:    icon = errorIcon;   break;
                case LogType.Warning:   icon = warningIcon; break;
                case LogType.Log:       icon = infoIcon;    break;
            }

            bool clicked = GUILayout.Button(new GUIContent(content, icon), boxStyle, GUILayout.ExpandWidth(true), GUILayout.Height(lineHeight));
            if (GUILayoutUtility.GetLastRect().Contains(Event.current.mousePosition)
                && Event.current.type == EventType.MouseDown && Event.current.clickCount > 1)
            {
                // double click
            }
            return clicked;
        }

        private void ProcessEvents(Event e)
        {
            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0 && resizer.Contains(e.mousePosition))
                    {
                        isResizing = true;
                    }
                    break;

                case EventType.MouseUp:
                    isResizing = false;
                    break;
            }

            Resize(e);
        }

        private void Resize(Event e)
        {
            if (isResizing)
            {
                sizeRatio = e.mousePosition.y / position.height;
                Repaint();
            }
        }

        public void AddItemsToMenu(GenericMenu menu)
        {
            menu.AddItem(menuOpenPythonLogContent, false, OpenPythonLog);
            menu.AddItem(menuShowTimestampContent, showTimestamp, () => ShowTimestamp(!showTimestamp));
            
            for (int i = 0; i < menuLogEntryContents.Length; i++)
            {
                int finalI = i;
                GUIContent entryContent = menuLogEntryContents[i];
                menu.AddItem(entryContent, (i + 1) == numLines, () => SetNumLines(finalI + 1, true));
            }
        }

        public void OpenPythonLog()
        {
            if (string.IsNullOrEmpty(logsDir))
                logsDir = Path.Combine(Application.persistentDataPath, "logs");
            string logPath = Path.Combine(logsDir, "pythonLog.txt");
            if (File.Exists(logPath))
                Application.OpenURL(logPath);
            else
                Debug.LogWarning("Couldn't find log file. You may need to start play mode once so that the "
                    + "user data path can be registered.");
        }

        public void SetNumLines(int lines, bool repaint = false)
        {
            numLines = lines;
            lineHeight = 19 + 13 * (lines - 1);
            if (repaint)
                Repaint();
        }

        public void ShowTimestamp(bool timestamp)
        {
            showTimestamp = timestamp;
            Repaint();
        }
    }
}
#endif
