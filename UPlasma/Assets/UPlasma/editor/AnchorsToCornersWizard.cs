﻿/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AnchorsToCornersWizard : ScriptableWizard {
    [Tooltip("Objects for which anchors will be repositioned")]
    public RectTransform[] transforms;
    
    [MenuItem ("Edit/UPlasma/Move anchors to corners")]
    static void CreateWizard () {
        ScriptableWizard.DisplayWizard<AnchorsToCornersWizard>("Move anchors to corners", "Move");
    }
    
    void OnWizardCreate () {
        foreach (RectTransform transform in transforms) {
            AnchorsToCorners(transform);
        }
    }
    
    public void AnchorsToCorners(RectTransform transform)
    {
        RectTransform t = transform as RectTransform;
        RectTransform pt = t.parent as RectTransform;
        
        Vector2 newAnchorsMin = new Vector2(t.anchorMin.x + t.offsetMin.x / pt.rect.width,
                                            t.anchorMin.y + t.offsetMin.y / pt.rect.height);
        Vector2 newAnchorsMax = new Vector2(t.anchorMax.x + t.offsetMax.x / pt.rect.width,
                                            t.anchorMax.y + t.offsetMax.y / pt.rect.height);
        t.anchorMin = newAnchorsMin;
        t.anchorMax = newAnchorsMax;
        t.offsetMin = t.offsetMax = new Vector2(0, 0);
        
        foreach (RectTransform childTransform in transform)
            AnchorsToCorners(childTransform);
    }
}
#endif