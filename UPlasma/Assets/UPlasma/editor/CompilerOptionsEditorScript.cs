/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

/// Forces Unity to not recompile scripts in playmode.
[InitializeOnLoad]
public class CompilerOptionsEditorScript
{
    static bool waitingForStop = false;
 
    static CompilerOptionsEditorScript()
    {
        EditorApplication.update += OnEditorUpdate;
    }

    static void OnEditorUpdate()
    {
        if (!waitingForStop && EditorApplication.isCompiling && EditorApplication.isPlaying)
        {
             EditorApplication.LockReloadAssemblies();
             EditorApplication.playModeStateChanged += PlaymodeChanged;
             waitingForStop = true;
        }
    }

    static void PlaymodeChanged(PlayModeStateChange state)
    {
        if (EditorApplication.isPlaying)
             return;
        
        EditorApplication.UnlockReloadAssemblies();
        EditorApplication.playModeStateChanged -= PlaymodeChanged;
        waitingForStop = false;
    }
}
#endif