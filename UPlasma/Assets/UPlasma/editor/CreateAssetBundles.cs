﻿/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using Newtonsoft.Json;
using System.IO;
using UPlasma;

public class CreateAssetBundles : ScriptableWizard
{
    [Tooltip("Informations about the mod")]
    public ModDescriptor descriptor;
    
    [Tooltip("Main entry for the mod (optional, not required for regular mods)")]
    public ModMainEntry mainEntry;
    
    bool loaded = false;
    
    [MenuItem ("Assets/Build AssetBundles")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<CreateAssetBundles>("Build asset bundles and manifest", "Build", "Save descriptor");
    }

    void OnWizardUpdate()
    {
        if (!loaded)
        {
            loaded = true;
            
            string[] bundles = AssetDatabase.GetAllAssetBundleNames();
            if (bundles.Length != 0)
                helpString = "Ready to generate bundles: " + string.Join(", ", bundles);
            else
            {
                errorString = "No bundle specified. You'll need to put your assets into bundle before being able to build them.";
                isValid = false;
                return;
            }
            
            // load the previously generated descriptor
            string buildDir = Path.Combine(Application.dataPath, "..", "AssetBundle_BUILD");
            Directory.CreateDirectory(buildDir);
            string descPath = Path.Combine(buildDir, "descriptor.json");
            if (File.Exists(descPath))
            {
                try {
                    descriptor = JsonConvert.DeserializeObject<ModDescriptor>(File.ReadAllText(descPath));
                    // increase the version number. Most users won't bother with it - this means the version will most likely serve
                    // as build number. Which is generally a good thing.
                    descriptor.buildId += 1;
                    if (!string.IsNullOrEmpty(descriptor.mainAssetName))
                    {
                        // remove the bundle name
                        string assetPath = descriptor.mainAssetName.Substring(descriptor.mainAssetName.Split('/')[0].Length + 1);
                        mainEntry = AssetDatabase.LoadAssetAtPath(assetPath, typeof(ModMainEntry)) as ModMainEntry;
                    }
                } catch (System.Exception) {
                    errorString = "Existing descriptor could not be loaded.";
                }
            }
        }
    }

    void OnWizardCreate() // "build" button
    {
        errorString = "";
        saveDescriptor();
        BuildPipeline.BuildAssetBundles("AssetBundle_BUILD", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
    }

    void OnWizardOtherButton() // "save descriptor" button
    {
        errorString = "";
        saveDescriptor();
    }
    
    void saveDescriptor()
    {
        string buildDir = Path.Combine(Application.dataPath, "..", "AssetBundle_BUILD");
        Directory.CreateDirectory(buildDir);
        JsonSerializerSettings settings = new JsonSerializerSettings { Formatting = Formatting.Indented };
        string descPath = Path.Combine(buildDir, "descriptor.json");
        if (mainEntry != null)
        {
            // mod has a main entry, write its path in the descriptor
            // -> asset path
            descriptor.mainAssetName = AssetDatabase.GetAssetPath(mainEntry);
            // -> bundle name + asset path
            descriptor.mainAssetName = AssetDatabase.GetImplicitAssetBundleName(descriptor.mainAssetName) + "/" + descriptor.mainAssetName;
        }
        File.WriteAllText(descPath, JsonConvert.SerializeObject(descriptor, settings));
        Debug.Log("Wrote descriptor to " + descPath);
    }
}
#endif
