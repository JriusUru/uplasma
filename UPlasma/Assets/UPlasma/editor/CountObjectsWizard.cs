﻿/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class CountObjectsWizard : ScriptableWizard {

	[MenuItem ("GameObject/Count objects")]
	static void CreateWizard () {
		// ScriptableWizard.DisplayWizard<CountObjectsWizard>("Import sound emitters from JSON", "Import");
        Debug.Log(GameObject.FindObjectsOfType(typeof(Transform)).Length);
	}

	void OnWizardCreate () {
    }

	void OnWizardUpdate () {
	}

	void OnWizardOtherButton () {
	}
}
#endif
