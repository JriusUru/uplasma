/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "PRP/Light/plLightInfo.h"
#include "PRP/Light/plOmniLightInfo.h"
#include "PRP/Light/plDirectionalLightInfo.h"
#include "PRP/Light/plShadowCaster.h"
#pragma warning(pop)
#include "ImporterUtils.h"
#include "MatImporter.h"
#include <algorithm> // std::max


namespace PlImporter
{
	struct LightImporter
	{
		void import(UnityEngine::GameObject& gameObject, const plKey& lightKey, PlasmaVer version, plKey* shadowCasterKey, bool doDisable, UPlasma::PlEmu::LightOverride lightOverride);
		void importLimitedDirLight(UnityEngine::GameObject& gameObject, const plKey& lightKey, PlasmaVer version, UPlasma::PlEmu::AssetsLifeBinder assetsBinder, bool doDisable, UPlasma::PlEmu::LightOverride lightOverride);
	private:
		MatImporter matImporter;
	};
}
