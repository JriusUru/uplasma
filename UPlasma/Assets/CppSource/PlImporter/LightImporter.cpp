/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "Bindings.h"
#include "PRP/Light/plLightInfo.h"
#include "PRP/Light/plShadowMaster.h"
#include "LightImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

namespace PlImporter
{
	void LightImporter::import(UnityEngine::GameObject & gameObject, const plKey & lightKey, PlasmaVer version, plKey* shadowCasterKey, bool doDisable, LightOverride lightOverride)
	{
		if (!lightKey.Exists() || !lightKey.isLoaded())
			return;

		/*
		Finally, Unity lights match Plazma's own ! (something I've always failed to achieve in a Blender importer...)
		Well, not perfectly. Should be enough in most cases, though...
		*/

		// note: lights in Unity emit from the side, not the bottom. This means we need to add a light
		// transform object to rotate the light 90 degrees... Light animations will have to be updated accordingly.
		ST::string lightNameST = "LIGHT_" + ImporterUtils::sharpStringToTheoryString(gameObject.GetName());
		String lightName(lightNameST.c_str());
		GameObject lightCorrecter(lightName);
		Transform lightCorrecterTranform = lightCorrecter.GetTransform();
		lightCorrecterTranform.SetParent(gameObject.GetTransform());
		Vector3 correction(90, 0, 0);
		Vector3 poszero(0, 0, 0);
		lightCorrecterTranform.SetLocalEulerAngles(correction);
		lightCorrecterTranform.SetLocalPosition(poszero);
		Light light = lightCorrecter.AddComponent<Light>();
		int finalLyrMask = ImporterUtils::unlitLyrMask;
		short prpLightType = lightKey->getType();
		plLightInfo* info;
		plOmniLightInfo* omniInfo = 0;
		plDirectionalLightInfo* directInfo = 0;
		plSpotLightInfo* spotInfo = 0;

		LightType lightType(LightType::Point);
		if (prpLightType == pdClassType::kOmniLightInfo)
		{
			omniInfo = plOmniLightInfo::Convert(lightKey->getObj(), true);
			info = omniInfo;
			lightType = LightType::Point;
			light.SetType(lightType);
		}
		else if (prpLightType == pdClassType::kDirectionalLightInfo)
		{
			directInfo = plDirectionalLightInfo::Convert(lightKey->getObj(), true);
			info = directInfo;
			lightType = LightType::Directional;
			light.SetType(lightType);
		}
		else if (prpLightType == pdClassType::kSpotLightInfo)
		{
			spotInfo = plSpotLightInfo::Convert(lightKey->getObj(), true);
			info = spotInfo;
			lightType = LightType::Spot;
			light.SetType(lightType);
		}
		else
			return;

		if (info->getProperty(plLightInfo::kLPMovable))
			// Movable lights light up all the objects
			finalLyrMask = ~0;

		if (info->getProperty(plLightInfo::kLPHasIncludes))
		{
			// HasIncludes is a bit weird. If it's set but IncludesChars isn't, then the light should be disabled.
			// If IncludeChars is enabled, then it should be a normal light and still light up dynamic objects.
			// Hmmm, yeah, not exactly sure about this. Just hope it stays consistent in all Ages...
			if (info->getProperty(plLightInfo::kLPIncludesChars))
				;// finalLyrMask &= charOnlyLyrMask;
			else
				// if it doesn't include chars then it includes nothing. Don't ask.
				;// finalLyrMask &= 0;
		}

		// light color alpha component: PROBABLY not used. Seems the Max plugin creates random values from it.
		// I guess the default value is 1, but on export the color values are all multiplied by the intensity...
		float ambient[4] = { info->getAmbient().r, info->getAmbient().g, info->getAmbient().b, info->getAmbient().a };
		float diffuse[4] = { info->getDiffuse().r, info->getDiffuse().g, info->getDiffuse().b, info->getDiffuse().a };
		float ambientIntensity = (ambient[0] + ambient[1] + ambient[2]) / 3;
		float diffuseIntensity = (diffuse[0] + diffuse[1] + diffuse[2]) / 3;
		Vector4 finalColor(diffuse[0], diffuse[1], diffuse[2], diffuse[3]);
		if (ambientIntensity > diffuseIntensity)
			finalColor = Vector4(ambient[0], ambient[1], ambient[2], ambient[3]);
		// this color may be over the normal color range (HDR col in uru, hah), so remap it to the normal range,
		// and tweak the lamp's energy accordingly.
		// Note that Uru didn't use any kind of HDR and light intensity was per vertex + clamped to [0..1] (I think ?),
		// which isn't what Unity uses per default.
		float maxValue = std::max(finalColor.x, std::max(finalColor.y, finalColor.z));
		if (maxValue > 1)
		{
			finalColor.x = finalColor.x / maxValue;
			finalColor.y = finalColor.y / maxValue;
			finalColor.z = finalColor.z / maxValue;
			light.SetIntensity(maxValue);
		}
		light.SetColor(Color(finalColor.x, finalColor.y, finalColor.z, 1));
		if (shadowCasterKey)
		{
			// Plasma can do the distinction between hard and soft shadows. However it seems soft shadows are almost never used by Cyan's artists,
			// but I'm still not sure whether that was because of lack of information about it or because of additional overhead...
			// I'm not even sure there is a performance difference for using hard or soft shadows in Plasma.
			// In our case we can just use soft shadows for everything :shrug:
			light.SetShadows(LightShadows::Soft);
		}
		if (lightType != LightType::Directional)
		{
			if (prpLightType == pdClassType::kSpotLightInfo)
			{
				if (spotInfo->getAttenCutoff())
					light.SetRange(spotInfo->getAttenCutoff() * ImporterUtils::importScale);
				else
					// 0 means this light never fades. Just crank up the range.
                    // Note that those lights usually light only a few objects, so will require patching in lots of Ages.
					light.SetRange(99999);

				// three new properties: falloff, spotInner, spotOuter. Falloff is unused as it's mostly a useless hack.
				// SpotInner has no equivalent in Unity, but won't make such a difference.
				// SpotOuter is a half-angle in radians, whereas Unity is full-angle in degrees
				light.SetSpotAngle(spotInfo->getSpotOuter() * 180 / ImporterUtils::PI * 2);
			}
			else if (prpLightType == pdClassType::kOmniLightInfo)
			{
				if (omniInfo->getAttenCutoff())
					light.SetRange(omniInfo->getAttenCutoff() * ImporterUtils::importScale);
				else
					// 0 means this light never fades. Just crank up the range.
                    // Note that those lights usually light only a few objects, so will require patching in lots of Ages.
					light.SetRange(99999);
			}
		}
		light.SetBounceIntensity(0);

		plKey projKey = info->getProjection();
		if (projKey.Exists() && projKey.isLoaded())
		{
			// TODO - support light cookies
		}

		if (info->getProperty(plLightInfo::kDisable))
		{
			// disabled light
			light.SetEnabled(false);
		}
		light.SetCullingMask(finalLyrMask);

		if (doDisable)
			light.SetEnabled(false);

		if (lightOverride.Handle)
		{
			float intensity = lightOverride.GetIntensity();
			float range = lightOverride.GetRange();
			bool shadows = lightOverride.GetShadows();
			float shadStrength = lightOverride.GetShadowStrength();
			int lyrs = lightOverride.GetLayers();

			if (intensity >= 0)
				light.SetIntensity(intensity);
			if (range >= 0)
				light.SetRange(range);
			if (lyrs != -1)
				light.SetCullingMask(lyrs);
			if (shadStrength >= 0)
				light.SetShadowStrength(shadStrength);
			light.SetShadows(shadows ? LightShadows::Soft : LightShadows::None);
		}
	}
	void LightImporter::importLimitedDirLight(UnityEngine::GameObject & gameObject, const plKey & lightKey, PlasmaVer version, AssetsLifeBinder assetsBinder, bool doDisable, LightOverride lightOverride)
	{
		if (!lightKey.Exists() || !lightKey.isLoaded())
			return;

		return;

		/* as the name implies, the limited dir light is a directional light that only projects on a
		limited volume.
		Problem is, Unity doesn't have a perfect equivalent... We have two alternatives:
		- using a directional light with a cookie
		problem: only supports a single color channel, and the projection ratio can't be changed.
		- using a projector
		problem: applied after all lights, so does NOT work on black objects.
        
        Solution: just do it in a Unity patch.
		//*/


		/*

		// note: lights in Unity emit from the side, not the bottom. This means we need to add a light
		// transform object to rotate the light 90 degrees... Light animations will have to be updated accordingly.
		ST::string lightNameST = "LIGHT_" + ImporterUtils::sharpStringToTheoryString(gameObject.GetName());
		String lightName(lightNameST.c_str());
		GameObject lightCorrecter(lightName);
		Transform lightCorrecterTranform = lightCorrecter.GetTransform();
		lightCorrecterTranform.SetParent(gameObject.GetTransform());
		Vector3 correction(90, 0, 0);
		Vector3 poszero(0, 0, 0);
		lightCorrecterTranform.SetLocalEulerAngles(correction);
		lightCorrecterTranform.SetLocalPosition(poszero);
		Projector projector = lightCorrecter.AddComponent<Projector>();
		int finalLyrMask = ImporterUtils::unlitLyrMask;

		plLimitedDirLightInfo* info = plLimitedDirLightInfo::Convert(lightKey->getObj(), true);

		if (info->getProperty(plLightInfo::kLPMovable))
		// Movable lights light up all the objects
		finalLyrMask = ~0;

		// ambient is generally unused for limited dir lights
		//float ambient[4] = { info->getAmbient().r, info->getAmbient().g, info->getAmbient().b, info->getAmbient().a };
		float diffuse[4] = { info->getDiffuse().r, info->getDiffuse().g, info->getDiffuse().b, info->getDiffuse().a };
		//float ambientIntensity = (ambient[0] + ambient[1] + ambient[2]) / 3;
		float diffuseIntensity = (diffuse[0] + diffuse[1] + diffuse[2]) / 3;
		Vector4 finalColor(diffuse[0], diffuse[1], diffuse[2], diffuse[3]);
		//if (ambientIntensity > diffuseIntensity)
		//	finalColor = Vector4(ambient[0], ambient[1], ambient[2], ambient[3]);

		plKey projKey = info->getProjection();
		Material projMat(0);
		// import the texture (if it doesn't exist just returns a textureless material)
		matImporter.importLightLayer(projMat, projKey, version, assetsBinder);

		projMat.SetColor(Color(finalColor.x, finalColor.y, finalColor.z, 1));


		float ratio = info->getWidth() / info->getHeight();
		int maxSize;
		if (projKey.Exists() && projKey.isLoaded())
		{
		if (? ? ? )
		{
		Texture2D tex(NativeHelpers::CastObjTo<Texture2D>(projMat.GetMainTexture()));
		tex.Resize(, , , true);
		}
		}

		projector.SetOrthographic(true);
		// orthographic size: based on height, but Unity uses half-height
		projector.SetOrthographicSize(info->getHeight() * ImporterUtils::importScale * .5);
		projector.SetAspectRatio(info->getWidth() / info->getHeight());
		projector.SetFarClipPlane(info->getDepth() * ImporterUtils::importScale);
		projector.SetMaterial(projMat);

		if (info->getProperty(plLightInfo::kDisable))
		{
		// disabled light
		projector.SetEnabled(false);
		}
		projector.SetIgnoreLayers(~finalLyrMask);

		if (doDisable)
		projector.SetEnabled(false);

		if (lightOverride.Handle)
		{
		Single intensityCs;
		Single rangeCs;
		Single shadowsStrengthCs;
		Int32 lyrsCs;
		NativeHelpers::GetLightOverrideIntensity(lightOverride, &intensityCs);
		NativeHelpers::GetLightOverrideRange(lightOverride, &rangeCs);
		NativeHelpers::GetLightOverrideShadowStrength(lightOverride, &shadowsStrengthCs);
		NativeHelpers::GetLightOverrideLayers(lightOverride, &lyrsCs);
		float intensity = (float)intensityCs;
		float range = (float)rangeCs;
		bool shadows = lightOverride.GetShadows();
		float shadStrength = (float)shadowsStrengthCs;
		int lyrs = (int)lyrsCs;

		/ *if (intensity >= 0)
		projector.SetIntensity(intensity);
		if (range >= 0)
		projector.SetRange(range);* /
		if (lyrs != -1)
		projector.SetIgnoreLayers(~lyrs);
		/ *if (shadStrength >= 0)
		projector.SetShadowStrength(shadStrength);
		projector.SetShadows(shadows ? LightShadows::Soft : LightShadows::None);* /
		}
		//*/
	}
}
