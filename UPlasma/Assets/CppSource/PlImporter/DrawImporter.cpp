/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "DrawImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

namespace PlImporter
{
	void DrawImporter::import(UnityEngine::GameObject & gameObject, const plKey & drawKey,
		bool castShadows, Transform& parentObjTransform, bool& meshIsStatic, bool& objHasStaticRenderer, PlasmaVer version, AssetsLifeBinder assetsBinder, bool doDisable)
	{
		objHasStaticRenderer = false;
		if (!drawKey.Exists() || !drawKey.isLoaded())
		{
			meshIsStatic = false;
			return;
		}

		plDrawInterface* drawInt = plDrawInterface::Convert(drawKey->getObj(), true);
		int numUvs = 0;
		int numWeights = 0;
		bool hasSkinIndices = false;
		unsigned int numVertices = 0;
		unsigned int numIndices = 0;
		std::vector<int> indicesPerSubmesh;
		int baseMatrix = 0;
		int matrixCount = 0;
		int dspanMatrixCount = 0;
		bool boneStartIdWasSet = false;
		Transform goTransform(gameObject.GetTransform());

		meshIsStatic = true;

		for (int drawableId = 0; drawableId < drawInt->getNumDrawables(); drawableId++)
		{
			// first pass: gather total number of verts and faces
			plKey drawable = drawInt->getDrawable(drawableId);
			int drawableKey = drawInt->getDrawableKey(drawableId);
			if (!drawable.Exists() || !drawable.isLoaded())
				continue;
			if (drawableKey == -1)
			{
				// particle without visible data, ignore it
				meshIsStatic = false;
				continue;
			}
			plDrawableSpans *drawSpan = plDrawableSpans::Convert(drawable->getObj());
			meshIsStatic &= (bool)(drawSpan->getProps() && plDrawableSpans::kCritStatic);
			if (drawSpan->getDIIndex(drawableKey).fFlags & plDISpanIndex::kMatrixOnly)
			{
				// this is a bone without visuals, and doesn't have an icicle
				// DO register it since we'll need it for skinned meshes in a later pass, though.

				// check if we already registered this dspan
				bool found = false;
				int index = 0;
				for (plKey key : dspanBoneObjectsKeys)
				{
					if (key == drawable)
					{
						found = true;
						break;
					}
					index++;
				}
				if (!found)
				{
					// not found ? add it to the map
					Array1<Transform> transforms((int)drawSpan->getNumTransforms());
					dspanBoneObjectsKeys.push_back(drawable);
					dspanBoneObjectsValues.push_back(transforms);
					index = (int)dspanBoneObjectsKeys.size() - 1;
				}
				dspanBoneObjectsValues[index][drawSpan->getDIIndex(drawableKey).fIndices[0]] = gameObject.GetTransform();
				meshIsStatic = false;
				continue;
			}
			dspanMatrixCount = (int)drawSpan->getNumTransforms();

			for (unsigned int icicleIndex : drawSpan->getDIIndex(drawableKey).fIndices)
			{
				plSpan *span = drawSpan->getSpan(icicleIndex);
				// blind cast to an icicle - I don't recall any PRP version that used anything else...
				plIcicle *icicle = (plIcicle*)span;
				// lots of icicle properties are public, but have accessor nevertheless. Wtf ? Will use accessor, JustInCase(c)
				plGBufferGroup *bufferGroup = drawSpan->getBuffer(icicle->getGroupIdx());
				if (bufferGroup->getNumUVs() > numUvs)
					numUvs = (int)bufferGroup->getNumUVs();
				unsigned int format = bufferGroup->getFormat();
				if (bufferGroup->getSkinWeights() > numWeights)
					numWeights = (int)bufferGroup->getSkinWeights();
				hasSkinIndices |= bufferGroup->getHasSkinIndices();
				numVertices += icicle->getVLength();
				numIndices += icicle->getILength();
				indicesPerSubmesh.push_back(icicle->getILength());

				// some objects use dspan matrix for positioning but don't have a coordinterface (ex: canyon)
				// this would be wrong if every submesh had a different matrix, but this doesn't seem to happen.
				CoordImporter::importMatrixToTransform(goTransform, icicle->getLocalToWorld());

				// baseMatrix is the starting bone used by this mesh
				// bones are duplicated if the deformed object is in multiple dspans, which is very
				// fortunate for us... When setting up the object's bone and bindposes, we'll use
				// the armature from the first dspan. Hence only register baseMatrix and matrixCount
				// for the first span.
				if (!boneStartIdWasSet)
				{
					baseMatrix = icicle->getBaseMatrix();
					matrixCount = icicle->getNumMatrices();
					boneStartIdWasSet = true;
				}
			}
		}

		int totalFaces = 0;
		for (int i : indicesPerSubmesh)
			totalFaces += i;
		if (!totalFaces)
		{
			meshIsStatic = false;
			return;
		}

		Array1<Vector3> uVertices(numVertices);
		Array1<Vector3> uNormals(numVertices);
		Array1<Color32> uColors(numVertices);
		Array1<Vector2> uUV((numUvs > 0) ? numVertices : 0);
		Array1<Vector2> uUV2((numUvs > 1) ? numVertices : 0);
		Array1<Vector2> uUV3((numUvs > 2) ? numVertices : 0);
		Array1<Vector2> uUV4((numUvs > 3) ? numVertices : 0);
		Array1<BoneWeight> uWeights(numWeights ? numVertices : 0);
		std::vector<std::vector<int>> uFaces;
		for (int i = 0; i < indicesPerSubmesh.size(); i++)
		{
			std::vector<int> tmp(indicesPerSubmesh[i]);
			uFaces.push_back(tmp);
		}
		std::vector<plKey> materials;
		std::vector<plDrawableSpans*> matDSpans;
		bool receiveShadows = true;
		bool lit = false;

		int maxVertexWeightBoneId = -1;

		int submeshId = 0;
		int curVertexId = 0;
		for (int drawableId = 0; drawableId < drawInt->getNumDrawables(); drawableId++)
		{
			// second pass: fill arrays with vertex data
			plKey drawable = drawInt->getDrawable(drawableId);
			int drawableKey = drawInt->getDrawableKey(drawableId);
			if (!drawable.Exists() || !drawable.isLoaded())
				continue;
			if (drawableKey == -1)
				// particle without visible data, ignore it
				continue;
			plDrawableSpans *drawSpan = plDrawableSpans::Convert(drawable->getObj());
			if (drawSpan->getDIIndex(drawableKey).fFlags & plDISpanIndex::kMatrixOnly)
			{
				// this is a bone without visuals, and doesn't have an icicle
				continue;
			}
			for (unsigned int icicleIndex : drawSpan->getDIIndex(drawableKey).fIndices)
			{
				// now get the actual vertex and faces data
				plSpan *span = drawSpan->getSpan(icicleIndex);
				lit |= (span->getProps() & plSpan::kPropRunTimeLight) != 0;
				// blind cast to an icicle. If that's not possible, this will result in a pretty hard to nail exception.
				plIcicle *icicle = (plIcicle*)span;
				// get the material and store it for later importing
				plKey plMat = drawSpan->getMaterials()[icicle->getMaterialIdx()];
				materials.push_back(plMat);
				matDSpans.push_back(drawSpan);

				plGBufferGroup *bufferGroup = drawSpan->getBuffer(icicle->getGroupIdx());
				std::vector<plGBufferVertex> plVertices = bufferGroup->getVertices(icicle->getVBufferIdx(), icicle->getVStartIdx(), icicle->getVLength());
				std::vector<unsigned short> indices = bufferGroup->getIndices(icicle->getIBufferIdx(), icicle->getIStartIdx(), icicle->getILength());

				for (plGBufferVertex vertex : plVertices)
				{
					uVertices[curVertexId] = Vector3(vertex.fPos.X*ImporterUtils::importScale, vertex.fPos.Z*ImporterUtils::importScale, vertex.fPos.Y*ImporterUtils::importScale);
					uNormals[curVertexId] = Vector3(vertex.fNormal.X, vertex.fNormal.Z, vertex.fNormal.Y);
					int r, g, b, a;
					ImporterUtils::getComponentsFromIntColor(vertex.fColor, r, g, b, a);
					uColors[curVertexId] = Color32(r, g, b, a);
					if (numUvs > 0)
					{
						// Vector2 constructor (float, float) causes crash, for some reason.
						// Workaround: use parameterless constructor, then use Set(float, float).
						Vector2 v;
						if (bufferGroup->getNumUVs() > 0)
							v.Set(vertex.fUVWs[0].X, vertex.fUVWs[0].Y);
						uUV[curVertexId] = v;
					}
					if (numUvs > 1)
					{
						Vector2 v;
						if (bufferGroup->getNumUVs() > 1)
							v.Set(vertex.fUVWs[1].X, vertex.fUVWs[1].Y);
						uUV2[curVertexId] = v;
					}
					if (numUvs > 2)
					{
						Vector2 v;
						if (bufferGroup->getNumUVs() > 2)
							v.Set(vertex.fUVWs[2].X, vertex.fUVWs[2].Y);
						uUV3[curVertexId] = v;
					}
					if (numUvs > 3)
					{
						Vector2 v;
						if (bufferGroup->getNumUVs() > 3)
							v.Set(vertex.fUVWs[3].X, vertex.fUVWs[3].Y);
						uUV4[curVertexId] = v;
					}

					if (numWeights)
					{
						int skinIdx = vertex.fSkinIdx;
						BoneWeight uWeight;
						if (hasSkinIndices)
						{
							int ids[] = { -1, -1, -1 };
							float weights[] = { 0, 0, 0 };
							// Plasma: max 3 bones per vertex (max Unity: 4)
							// importing Plasma rigging will only work if ALL submeshes share the EXACT
							// same bone list (which should always be the case)
							if (numWeights > 0)
							{
								int id = skinIdx & 0xff;
								float weight = vertex.fSkinWeights[0];
								maxVertexWeightBoneId = (id > maxVertexWeightBoneId) ? id : maxVertexWeightBoneId;
								ids[0] = id;
								weights[0] = weight;
							}
							if (numWeights > 1)
							{
								int id = (skinIdx & 0xff00) >> 8;
								float weight = vertex.fSkinWeights[1];
								maxVertexWeightBoneId = (id > maxVertexWeightBoneId) ? id : maxVertexWeightBoneId;
								ids[1] = id;
								weights[1] = weight;
							}
							if (numWeights > 2)
							{
								int id = (skinIdx & 0xff0000) >> 16;
								float weight = vertex.fSkinWeights[2];
								maxVertexWeightBoneId = (id > maxVertexWeightBoneId) ? id : maxVertexWeightBoneId;
								ids[2] = id;
								weights[2] = weight;
							}

							// Make sure the sum of weights is 1.
							// Required otherwise deform will be wrong (especially around NPC cheekbones and armpit)
							float sum = weights[0] + weights[1] + weights[2];
							if (sum)
							{
								weights[0] /= sum;
								weights[1] /= sum;
								weights[2] /= sum;
							}

							// Unity also prefers weights to be sorted from most important to less important.
							// This allows for instance to limit the number of interpolated bones for mobile platforms.
							// (and might fix other annoying issues)
							if (weights[0] < weights[1]) // sort the first and second numbers
							{
								float buff = weights[0];
								int bufi = ids[0];
								ids[0] = ids[1];
								weights[0] = weights[1];
								ids[1] = bufi;
								weights[1] = buff;
							}
							if (weights[1] < weights[2]) // sort the second and third numbers
							{
								float buff = weights[1];
								int bufi = ids[1];
								ids[1] = ids[2];
								weights[1] = weights[2];
								ids[2] = bufi;
								weights[2] = buff;
							}
							if (weights[0] < weights[1]) // sort the first and second numbers once again
							{
								float buff = weights[0];
								int bufi = ids[0];
								ids[0] = ids[1];
								weights[0] = weights[1];
								ids[1] = bufi;
								weights[1] = buff;
							}

							if (numWeights > 0 && ids[0] >= 0)
							{
								uWeight.SetBoneIndex0(ids[0]);
								uWeight.SetWeight0(weights[0]);
							}
							if (numWeights > 1 && ids[1] >= 0)
							{
								uWeight.SetBoneIndex1(ids[1]);
								uWeight.SetWeight1(weights[1]);
							}
							if (numWeights > 2 && ids[2] >= 0)
							{
								uWeight.SetBoneIndex2(ids[2]);
								uWeight.SetWeight2(weights[2]);
							}
						}
						else
						{
							// a mesh without indices is always deformed by a single bone and the world.
							// -> use only the vertex' first weight.
							float weight = vertex.fSkinWeights[0];

							if (weight > 1) weight = 1;
							if (weight < 0) weight = 0;

							if (weight > .5)
							{
								// assign weight to the null bone
								uWeight.SetBoneIndex0(0);
								uWeight.SetWeight0(weight);
								// add remaining weight to the deform bone
								uWeight.SetBoneIndex1(1);
								uWeight.SetWeight1(1 - weight);
							}
							else
							{
								// assign weight to the null bone
								uWeight.SetBoneIndex1(0);
								uWeight.SetWeight1(weight);
								// add remaining weight to the deform bone
								uWeight.SetBoneIndex0(1);
								uWeight.SetWeight0(1 - weight);
							}
						}

						uWeights[curVertexId] = uWeight;
					}

					curVertexId++;
				}

				int indexOffset = curVertexId - (icicle->getVStartIdx() + icicle->getVLength());
				for (int i = 0; i < indices.size() / 3; i++)
				{
					uFaces[submeshId][i * 3] = indices[i * 3] + indexOffset;
					uFaces[submeshId][i * 3 + 1] = indices[i * 3 + 2] + indexOffset;
					uFaces[submeshId][i * 3 + 2] = indices[i * 3 + 1] + indexOffset;
				}
				receiveShadows = receiveShadows && (icicle->getProps() & plGeometrySpan::kPropNoShadow);
				submeshId++;
			}
		}

		Mesh mesh;
		String meshName(drawKey->getName().c_str());
		mesh.SetName(meshName);

		if (hasSkinIndices)
		{
			// for some stupid reason, vertices can use a boneid beyond the icicle's matrixCount, and Plamza
			// doesn't even care.
			// Sigh... Just replace matrixCount by 1 + the maximum bone id found when processing vertices.
			matrixCount = maxVertexWeightBoneId + 1;
		}

		std::pair<int, int> startAndCount(baseMatrix, matrixCount);
		objsBoneStartAndLength[drawKey] = startAndCount;
		objsUsingSkinIndices[drawKey] = hasSkinIndices;

		// load bone data - only set bone transforms to identity matrices.
		// we'll overwrite it later when post-processing the skinned object
		Array1<Matrix4x4> uBindPoses(matrixCount);
		for (int i = 0; i<matrixCount; i++)
		{
			// damnable identity accessor still crashes... build the mtx manually.
			Matrix4x4 mtx;
			Vector4 row0(1, 0, 0, 0);
			Vector4 row1(0, 1, 0, 0);
			Vector4 row2(0, 0, 1, 0);
			Vector4 row3(0, 0, 0, 1);
			mtx.SetRow(0, row0);
			mtx.SetRow(1, row1);
			mtx.SetRow(2, row2);
			mtx.SetRow(3, row3);
			uBindPoses[i] = mtx;
			i++;
		}
		if (matrixCount)
			mesh.SetBindposes(uBindPoses);

		// some meshes (notably Elodea's terrain) reach a cumulative vertex number over 2**16.
		// Plasma renders meshes separately, which avoids issues, but in Unity we combine those vertex lists.
		// Fortunately, Unity supports meshes with int32 vertex indexing (since not long ago).
		if (uVertices.GetLength() >= 65535)
			mesh.SetIndexFormat(Rendering::IndexFormat::UInt32);

		// load vertex data
		mesh.SetVertices(uVertices);
		mesh.SetNormals(uNormals);
		mesh.SetColors32(uColors);
		if (numUvs > 0)
			mesh.SetUv(uUV);
		if (numUvs > 1)
			mesh.SetUv2(uUV2);
		if (numUvs > 2)
			mesh.SetUv3(uUV3);
		if (numUvs > 3)
			mesh.SetUv4(uUV4);
		mesh.SetBoneWeights(uWeights);

		mesh.SetSubMeshCount((int)uFaces.size());
		for (int subMeshId = 0; subMeshId < uFaces.size(); subMeshId++)
		{
			std::vector<int>& uFacesV = uFaces[subMeshId];
			Array1<Int32> uSubFaces((Int32)(int)uFaces[subMeshId].size());
			for (int i = 0; i < uFacesV.size(); i++)
				uSubFaces[i] = uFacesV[i];
			mesh.SetTriangles(uSubFaces, subMeshId, false, 0);
		}

		// finalize and send to GC
		mesh.RecalculateBounds();
		//mesh.RecalculateTangents();
		//mesh.UploadMeshData(true); // not required as Unity will automatically do it on next frame

		// now, build the material list. If a material is not yet imported, do so now
		Array1<Material> uMaterials((int)materials.size());
		Cubemap cube(0);
		for (int i = 0; i < materials.size(); i++)
		{
			plDrawableSpans* span = matDSpans[i];
			Material mat(0);
			matImporter.import(mat, materials[i], cube, gameObject, span, version, assetsBinder);
			if (mat.Handle)
				uMaterials[i] = mat;
		}

		if (numWeights)
		{
			objHasStaticRenderer = false;
			SkinnedMeshRenderer &mRender = gameObject.AddComponent<SkinnedMeshRenderer>();
			if (drawInt->getProperty(plDrawInterface::kDisable))
				mRender.SetEnabled(false);
			mRender.SetSharedMaterials(uMaterials);
			mRender.SetSharedMesh(mesh);
			// register this drawable as needing its bones set in a second pass
			skinnedMeshesThatNeedBones[drawKey] = matDSpans[0];
			// following statements are rendering optimization(not that it really matters much, but whatevs)
			mRender.SetLightProbeUsage(Rendering::LightProbeUsage::Off);
			mRender.SetReflectionProbeUsage(Rendering::ReflectionProbeUsage::Off);
			mRender.SetShadowCastingMode(castShadows ? Rendering::ShadowCastingMode::On : Rendering::ShadowCastingMode::Off);
			mRender.SetReceiveShadows(receiveShadows);

			Array1<Transform> bones(matrixCount);
			// fill the SMR's bone list with nothing but nulls. We'll reset this list correctly
			// in post-processing of the object.
			mRender.SetBones(bones);

			if (doDisable)
				mRender.SetEnabled(false);

			// DISABLED - see MatImporter for more info.
			if (cube.Handle != 0 && false)
			{
				// we have a cubemap - enable the simplest probe sampling possible...
				mRender.SetReflectionProbeUsage(Rendering::ReflectionProbeUsage::Simple);
				// we can't directly set an override probe, so instead place the probe exactly at the sampling point.
				String probeName(cube.GetName());
				GameObject probeGO(probeName);
				Transform probeTransform = probeGO.GetTransform();
				ReflectionProbe probe = probeGO.AddComponent<ReflectionProbe>();
				Transform goTransform(gameObject.GetTransform());
				probeTransform.SetParent(goTransform);
				// usually the sampling point is the renderer's bounding box center. However bounds for skinned renderers move with time.
				// So instead set the sampling point to the object's location.
				Vector3 zero(0, 0, 0);
				probeTransform.SetLocalPosition(zero);
				Vector3 smallSize(.1f, .1f, .1f); // a 10-cm sized box should do it
				probe.SetSize(smallSize);
				mRender.SetProbeAnchor(goTransform);
				probe.SetMode(Rendering::ReflectionProbeMode::Custom);
				probe.SetCustomBakedTexture(cube); // oh, that might be useful too
			}
		}
		else
		{
			objHasStaticRenderer = true;
			MeshFilter &mFilter = gameObject.AddComponent<MeshFilter>();
			MeshRenderer &mRender = gameObject.AddComponent<MeshRenderer>();
			if (drawInt->getProperty(plDrawInterface::kDisable))
			{
				mRender.SetEnabled(false);
				objHasStaticRenderer = false; // otherwise this breaks static batching. Don't ask.
			}
			mFilter.SetMesh(mesh);
			mRender.SetSharedMaterials(uMaterials);
			// following statements are rendering optimization(not that it really matters much, but whatevs)
			mRender.SetLightProbeUsage(Rendering::LightProbeUsage::Off);
			mRender.SetReflectionProbeUsage(Rendering::ReflectionProbeUsage::Off);
			mRender.SetShadowCastingMode(castShadows ? Rendering::ShadowCastingMode::On : Rendering::ShadowCastingMode::Off);
			mRender.SetReceiveShadows(receiveShadows);

			if (doDisable)
				mRender.SetEnabled(false);

			// DISABLED - see MatImporter for more info.
			if (cube.Handle != 0 && false)
			{
				// we have a cubemap - enable the simplest probe sampling possible...
				mRender.SetReflectionProbeUsage(Rendering::ReflectionProbeUsage::Simple);
				// we can't directly set an override probe, so instead place the probe exactly at the sampling point.
				String probeName(cube.GetName());
				GameObject probeGO(probeName);
				Transform probeTransform = probeGO.GetTransform();
				ReflectionProbe probe = probeGO.AddComponent<ReflectionProbe>();
				probeTransform.SetParent(gameObject.GetTransform());
				// the sampling point is the center of the mesh render's bounds. Place it exactly there, and make the probe's size
				// small enough that it won't accidentally affect other objects
				probeTransform.SetPosition(mRender.GetBounds().GetCenter());
				Vector3 smallSize(.1f, .1f, .1f); // a 10-cm sized box should do it
				probe.SetSize(smallSize);
				probe.SetMode(Rendering::ReflectionProbeMode::Custom);
				probe.SetCustomBakedTexture(cube); // oh, that might be useful too
			}
		}

		// put the mesh on the nolight layer
		if (lit)
			gameObject.SetLayer(ImporterUtils::litLyr);
		else
			gameObject.SetLayer(ImporterUtils::unlitLyr);

		Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
		assets.Add(mesh);
	}
	void DrawImporter::importCluster(UnityEngine::GameObject & parentObject, const plKey & poolObjKey, PlasmaVer version, AssetsLifeBinder assetsBinder)
	{
		plClusterGroup* clusterGroup = plClusterGroup::Convert(poolObjKey->getObj());
		/* mmmokay, this is going to be a bit tricky. Clusters use a template model, which is duplicated
		all around, which is good. However it also uses two additional GPU float array per instance for
		vertex color and vertex offset, which can't be duplicated in Unity.
		We'll therefore have to rely on dynamic lighting for those objects - as if current light import isn't
		hell already... Fortunately it seems these properties are rarely used, which is good for us.
		Also, make sure these objects are compatible with instanced rendering.
		Last note: cluster templates use TWO vertex colors. I assume the second one is run-time override of
		the instance...
		*/

		Transform parentObjectTransform(parentObject.GetTransform());

		plSpanTemplate& spanTemplate = clusterGroup->getTemplate();

		/* NOTE:
		sometime spanTemplate is invalid (notably in Minkata where Drizzle edits it to
		move the clusters to another page). As such, .getNumVerts() will crash the loading process.
		Need to find how to return when that's the case...
		//*/

		String meshName(poolObjKey->getName().c_str());
		Mesh mesh;
		mesh.SetName(meshName);

		int numVertices = spanTemplate.getNumVerts();
		int numTris = spanTemplate.getNumTris();
		int numUvs = (spanTemplate.getFormat() & plSpanTemplate::kUVWMask) / 0x10;
		Array1<Vector3> uVertices(numVertices);
		Array1<Vector3> uNormals(numVertices);
		Array1<Color32> uColors(numVertices);
		Array1<Vector2> uUV(numUvs ? numVertices : 0);
		Array1<Vector2> uUV2((numUvs > 1) ? numVertices : 0);
		Array1<Vector2> uUV3((numUvs > 2) ? numVertices : 0);
		Array1<Vector2> uUV4((numUvs > 3) ? numVertices : 0);
		Array1<Int32> uFaces((Int32)numTris * 3);

		int curVertexId = 0;
		for (plSpanTemplate::Vertex vertex : spanTemplate.getVertices())
		{
			uVertices[curVertexId] = Vector3(vertex.fPosition.X*ImporterUtils::importScale, vertex.fPosition.Z*ImporterUtils::importScale, vertex.fPosition.Y*ImporterUtils::importScale);
			uNormals[curVertexId] = Vector3(vertex.fNormal.X, vertex.fNormal.Z, vertex.fNormal.Y);
			int r, g, b, a;
			ImporterUtils::getComponentsFromIntColor(vertex.fColor1, r, g, b, a);
			uColors[curVertexId] = Color32(r, g, b, a);
			if (numUvs > 0)
			{
				// Vector2 constructor (float, float) causes crash, for some reason.
				// Workaround: use parameterless constructor, then use Set(float, float).
				Vector2 v;
				if (numUvs > 0)
					v.Set(vertex.fUVWs[0].X, vertex.fUVWs[0].Y);
				uUV[curVertexId] = v;
			}
			if (numUvs > 1)
			{
				Vector2 v;
				if (numUvs > 1)
					v.Set(vertex.fUVWs[1].X, vertex.fUVWs[1].Y);
				uUV2[curVertexId] = v;
			}
			if (numUvs > 2)
			{
				Vector2 v;
				if (numUvs > 2)
					v.Set(vertex.fUVWs[2].X, vertex.fUVWs[2].Y);
				uUV3[curVertexId] = v;
			}
			if (numUvs > 3)
			{
				Vector2 v;
				if (numUvs > 3)
					v.Set(vertex.fUVWs[3].X, vertex.fUVWs[3].Y);
				uUV4[curVertexId] = v;
			}
			curVertexId++;
		}

		const unsigned short* indices = spanTemplate.getIndices();
		for (int i = 0; i < numTris; i++)
		{
			uFaces[i * 3] = indices[i * 3];
			uFaces[i * 3 + 1] = indices[i * 3 + 2];
			uFaces[i * 3 + 2] = indices[i * 3 + 1];
		}

		mesh.SetVertices(uVertices);
		mesh.SetNormals(uNormals);
		mesh.SetColors32(uColors);
		if (numUvs)
			mesh.SetUv(uUV);
		if (numUvs > 1)
			mesh.SetUv(uUV2);
		if (numUvs > 2)
			mesh.SetUv(uUV3);
		if (numUvs > 3)
			mesh.SetUv(uUV4);
		mesh.SetTriangles(uFaces);

		Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
		assets.Add(mesh);

		// good... now create instances !

        // start with the parent object though
		int numClusters = 0;
		for (plCluster* cluster : clusterGroup->getClusters())
			for (plSpanInstance* instance : cluster->getInstances())
				numClusters++;
		GameObject clusterGOParent(meshName);
		Transform clusterGOParentTransform(clusterGOParent.GetTransform());
		clusterGOParentTransform.SetParent(parentObjectTransform);

        // import the material
        Cubemap cube(0);
        Material mat(0);
        matImporter.import(mat, clusterGroup->getMaterial(), cube, clusterGOParent, version, assetsBinder);
        // now enable GPU instancing for better perfs. Fuck yeah.
        mat.SetEnableInstancing(true);
        
        PlClusterGroup uClusterGroup(clusterGOParent.AddComponent<PlClusterGroup>());
        uClusterGroup.SetMaterial(mat);
        PlLodDist lod;
        lod.minDist = clusterGroup->getLOD().getMin();
        lod.maxDist = clusterGroup->getLOD().getMax();
        uClusterGroup.SetLod(lod);
        uClusterGroup.SetRenderLevel(clusterGroup->getRenderLevel());
		ImporterUtils::keyToObj_Set(poolObjKey, uClusterGroup);

		Array1<GameObject> staticGOs(numClusters);
		int i = 0;
		for (plCluster* cluster : clusterGroup->getClusters())
		{
			GameObject clusterGO(meshName);
            PlCluster uCluster(clusterGO.AddComponent<PlCluster>());
            uCluster.SetGroup(uClusterGroup);
            uClusterGroup.GetClusters().Add(uCluster);
			Transform clusterTR = clusterGO.GetTransform();
			clusterTR.SetParent(clusterGOParentTransform);
			for (plSpanInstance* instance : cluster->getInstances())
			{
				hsMatrix44 l2w = instance->getLocalToWorld();
				GameObject clusterInstance(meshName);
				Transform clusterInstanceTR = clusterInstance.GetTransform();
				clusterInstanceTR.SetParent(clusterTR);
				CoordImporter::importMatrixToTransform(clusterInstanceTR, l2w);

				MeshFilter filter = clusterInstance.AddComponent<MeshFilter>();
				MeshRenderer rndr = clusterInstance.AddComponent<MeshRenderer>();
				filter.SetSharedMesh(mesh);
				//rndr.SetReceiveShadows(false);
				rndr.SetReceiveShadows(true);
				rndr.SetLightProbeUsage(Rendering::LightProbeUsage::Off);
				rndr.SetReflectionProbeUsage(Rendering::ReflectionProbeUsage::Off);
				rndr.SetShadowCastingMode(Rendering::ShadowCastingMode::Off);
				rndr.SetSharedMaterial(mat);
				clusterInstance.SetLayer(ImporterUtils::litLyr);
				staticGOs[i++] = clusterInstance;
			}
		}

		StaticBatchingUtility::Combine(staticGOs, parentObject);

		/*char msg[256];
		sprintf_s(msg, sizeof msg, "Marked %d clusters as batchable static.", numClusters);
		String msgc(msg);
		Debug::Log(msgc);*/
	}
	void DrawImporter::applyOverride(UnityEngine::GameObject & gameObject, MeshRendererOverride mrOverride)
	{
		MeshRenderer mr(gameObject.GetComponent<MeshRenderer>());
		Rendering::ShadowCastingMode castMode = mrOverride.GetCastShadows();
		mr.SetShadowCastingMode(castMode);
	}
	void DrawImporter::postProcessSkinnedObject(UnityEngine::GameObject & gameObject, const plKey & drawKey, PlasmaVer version)
	{
		if (!drawKey.Exists() || !drawKey.isLoaded())
			return;

		plDrawInterface* drawInt = plDrawInterface::Convert(drawKey->getObj());
		Transform goTransform = gameObject.GetTransform();

		/*
		ISSUE: in the editor, GetComponent returns some kind of "fake null component", NOT "null" !
		Which means object.Handle == 0 won't work for null checking.
		Why ? Just because Unity wants to display a funky error message for "easier debugging",
		and overloads == operator null checking :facepalm:
		I don't mind stupid code behavior, however this is documented absolutely NOWHERE. F you, unity.
		Workaround: use extension class, adding HasComponent method.
		*/
		if (NativeHelpers::HasComponent<SkinnedMeshRenderer>(gameObject))
		{
			SkinnedMeshRenderer mRender = gameObject.GetComponent<SkinnedMeshRenderer>();
			mRender.SetUpdateWhenOffscreen(true);
			int index = 0;
			std::pair<int, int> startAndCount = objsBoneStartAndLength[drawKey];
			for (plKey key : dspanBoneObjectsKeys)
			{
				if (key == drawInt->getDrawable(0))
				{
					ST::string objName = ImporterUtils::sharpStringToTheoryString(mRender.GetName());
					bool usesSkinIndices = objsUsingSkinIndices[drawKey];
					/*char msgc[256];
					sprintf_s(msgc, sizeof msgc, "%s: start %d, %d bones, ids=%d", objName.c_str(), startAndCount.first, startAndCount.second, usesSkinIndices);
					String msg(msgc);
					Debug::Log(msg);*/

					System::Array1<UnityEngine::Transform> thisMeshBones(startAndCount.second);
					Array1<Matrix4x4> uBindposes = mRender.GetSharedMesh().GetBindposes();

					for (int i = 0; i < startAndCount.second; i++)
					{
						Transform bone(dspanBoneObjectsValues[index][i + startAndCount.first]);
						if (!bone.Handle)
						{
							// if it has no handle, then it's a null bone... simple as that.

							/*
							// don't assign a bone (Unity will understand), but setup a correct bindpose.
							Matrix4x4 matGo;
							NativeHelpers::GetTransformLocalToWorldMatrix(goTransform, &matGo);
							uBindposes[i] = matGo;
							continue;
							//*/

							// Unity understands null bones as using world origin, while in Plasma they
							// use object transform instead (useful for Gira fishes)
							bone = goTransform;
						}
						thisMeshBones[i] = bone;
						Matrix4x4 matBone(bone.GetWorldToLocalMatrix());
						Matrix4x4 matGo(goTransform.GetLocalToWorldMatrix());
						Matrix4x4 bindpose(matBone * matGo);
						uBindposes[i] = bindpose;
					}
					mRender.GetSharedMesh().SetBindposes(uBindposes);
					mRender.SetBones(thisMeshBones);
					break;
				}
				index++;
			}
		}
	}
}
