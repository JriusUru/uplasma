/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "Bindings.h"
#include "PageImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

namespace PlImporter
{
	void PageImporter::import(plResManager & rmgr, const plLocation & pageLoc, UnityEngine::GameObject & parentObj,
		AssetsLifeBinder assetsBinder)
	{
		PlasmaVer version = pageLoc.getVer();
		plSceneNode* sceneNode = rmgr.getSceneNode(pageLoc);
		if (!sceneNode)
			// no scene node ? probably just a global file of some kind...
			return;
		PrpImportClues clues(PlImporter::ImporterUtils::prpClues_Get(pageLoc));

		Transform parentObjTransform(parentObj.GetTransform());
		PlPage uPage(parentObj.GetComponent<PlPage>());

		// keep track of static objects too... We can batch them !
		Array1<GameObject> staticGOs((int)sceneNode->getSceneObjects().size());
		int totalBatchedObjects = 0;

		/* PROBLEM: using anim checking is more efficient for static batching.
		However, right now it doesn't check the object's hierarchy for /other/ animated objects, which makes it overzealous...
		*/
		bool useAnimsForStaticChecking = false;

		// import all sceneobjects
		for (int i = 0; i < sceneNode->getSceneObjects().size(); i++)
		{
			// get the scene object
			plKey& objKey = sceneNode->getSceneObjects()[i];

			if (!objKey.Exists() || !objKey.isLoaded())
				continue;

			plSceneObject* plObj = plSceneObject::Convert(objKey->getObj(), true);
			// get all interesting keys/objects
			plKey audioKey = plObj->getAudioInterface();
			plKey coordKey = plObj->getCoordInterface();
			plKey drawKey = plObj->getDrawInterface();
			plKey simKey = plObj->getSimInterface();
			std::vector<plKey>& interfaces = plObj->getInterfaces();
			std::vector<plKey>& modifiers = plObj->getModifiers();

			String objName(objKey->getName().c_str());
			GameObject uObject(objName);
            PlSceneObject uSO(uObject.AddComponent<PlSceneObject>());

			ObjOverride objOverride(clues.GetObjOverride(objName));
			if (objOverride.Handle && !objOverride.GetEnableSelf())
				uObject.SetActive(false);

			ImporterUtils::keyToObj_Set(objKey, uSO);

			if (UnityEngine::Application::GetIsEditor())
			{
				// set the object as child of the Age/Page master, to make it easier to browse the editor hierarchy.
				// only do this in the editor though... Complex hierarchy might eat up a bit of performance.
                // (although I think hierarchy perf was improved in Unity 2017+ ?)
				// We'll rebuild PRP parent/child relations in a second pass.
				uObject.GetTransform().SetParent(parentObjTransform);
			}
			uPage.GetPageObjects().Add(uObject);

			bool castShadow = false;
			for (plKey modifierKey : modifiers)
			{
				if (modifierKey->getType() == pdClassType::kShadowCaster)
					castShadow = true;
			}

			bool meshIsStatic;
			bool objHasRenderer;
			bool physIsDynamic = false;
			bool drawIsStatic = true;
			bool coordIsDynamic = false;
			audioImporter.import(uObject, audioKey, version, objOverride.Handle && !objOverride.GetEnableAudio());
			drawImporter.import(uObject, drawKey, castShadow, parentObjTransform, drawIsStatic, objHasRenderer, version, assetsBinder, objOverride.Handle && !objOverride.GetEnableDraw());
			coordImporter.import(uObject, coordKey, coordIsDynamic, version);
			physImporter.import(uObject, simKey, physIsDynamic, version, assetsBinder, objOverride.Handle && !objOverride.GetEnablePhys());
			if (objOverride.Handle && objOverride.GetMeshRendererOverride().Handle)
				drawImporter.applyOverride(uObject, objOverride.GetMeshRendererOverride());
			/*meshIsStatic = drawIsStatic;
			meshIsStatic &= !physIsDynamic;
			meshIsStatic &= !coordIsDynamic;*/
			meshIsStatic = !coordIsDynamic && !physIsDynamic;

			bool objIsStatic = false;
			if (useAnimsForStaticChecking && objHasRenderer)
				objIsStatic = true;
			else
			{
				if ((!coordKey || !coordKey.Exists() || !coordKey.isLoaded() || meshIsStatic) && objHasRenderer)
					objIsStatic = true;
			}

			plKey* shadowCasterKey = 0;
			for (plKey modifierKey : modifiers)
			{
				short modifierType = modifierKey->getType();

				if (modifierType == pdClassType::kAGModifier)
				{
					animImporter.importAGModifier(uObject, modifierKey, version, assetsBinder);
				}
				else if (modifierType == pdClassType::kAGMasterMod)
				{
					// animated object
					// these usually also contain an AGModifier, which doesn't contain much data. Just ignore the thing...
					animImporter.importAGMasterMod(uObject, modifierKey, version, assetsBinder, objOverride.Handle && !objOverride.GetEnableAnim());

					// an AG master mod usually denotes an animated object
					if (coordKey && coordKey.Exists() && coordKey.isLoaded() && drawKey && drawKey.Exists() && drawKey.isLoaded())
						// is a visible mesh, has coord and is animated. It's almost certain it's not static.
						objIsStatic = false;
				}
				else if (modifierType == pdClassType::kViewFaceModifier)
				{
					PlViewFaceModifier plobj(uObject.AddComponent<PlViewFaceModifier>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
					objIsStatic = false;
				}
				else if (modifierType == pdClassType::kFadeOpacityMod)
				{
					PlFadeOpacityMod plobj(uObject.AddComponent<PlFadeOpacityMod>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}

				// LOGIC/NOTIFY/EVENTS COMPONENTS
				else if (modifierType == pdClassType::kLogicModifier)
				{
					PlLogicModifier plobj(uObject.AddComponent<PlLogicModifier>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
					// now import conditional objects, as they aren't directly linked to the SO itself
					plLogicModifier* logicMod = plLogicModifier::Convert(modifierKey->getObj());
					for (plKey conditionKey : logicMod->getConditions())
					{
						short conditionType = conditionKey->getType();
						if (conditionType == pdClassType::kActivatorConditionalObject)
						{
							PlActivatorConditionalObject plobj(uObject.AddComponent<PlActivatorConditionalObject>());
							ImporterUtils::keyToObj_Set(conditionKey, plobj);
						}
						else if (conditionType == pdClassType::kFacingConditionalObject)
						{
							PlFacingConditionalObject plobj(uObject.AddComponent<PlFacingConditionalObject>());
							ImporterUtils::keyToObj_Set(conditionKey, plobj);
						}
						else if (conditionType == pdClassType::kObjectInBoxConditionalObject)
						{
							PlObjectInBoxConditionalObject plobj(uObject.AddComponent<PlObjectInBoxConditionalObject>());
							ImporterUtils::keyToObj_Set(conditionKey, plobj);
						}
						else if (conditionType == pdClassType::kVolumeSensorConditionalObject)
						{
							PlVolumeSensorConditionalObject plobj(uObject.AddComponent<PlVolumeSensorConditionalObject>());
							ImporterUtils::keyToObj_Set(conditionKey, plobj);
						}
					}
				}
				// detectors
				else if (modifierType == pdClassType::kPickingDetector)
				{
					PlPickingDetector plobj(uObject.AddComponent<PlPickingDetector>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kObjectInVolumeDetector)
				{
					PlObjectInVolumeDetector plobj(uObject.AddComponent<PlObjectInVolumeDetector>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				// other logic
				else if (modifierType == pdClassType::kResponderModifier)
				{
					PlResponder plobj(uObject.AddComponent<PlResponder>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kPythonFileMod)
				{
                    /* so, it seems whenever multiple scene objects reference the plKey
                    of the SAME plPythonFileMod, they all execute a different instance
                    of the python file. All these DIFFERENT instances use the SAME PLKEY.
                    So, since of course I did not expect modifiers to be shared in such a
                    stupid way, we have to improvise a nice little hack...
                    */
					PlPythonFileMod plobj(uObject.AddComponent<PlPythonFileMod>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj); // we're not interested in the actual value, only in the key
                    ImporterUtils::keyToObj_SetWithSharingPlKey(modifierKey, plobj);
				}
				// xrgn
				else if (modifierType == pdClassType::kExcludeRegionModifier)
				{
					PlExcludeRegionModifier plobj(uObject.AddComponent<PlExcludeRegionModifier>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				// avatar
				else if (modifierType == pdClassType::kArmatureLODMod || modifierType == pdClassType::kArmatureMod)
				{
					PlArmatureMod plobj(uObject.AddComponent<PlArmatureMod>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				// others
				else if (modifierType == pdClassType::kCameraModifier)
				{
					PlCameraModifier plobj(uObject.AddComponent<PlCameraModifier>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kMaintainersMarkerModifier)
				{
					PlMaintainersMarkerModifier plobj(uObject.AddComponent<PlMaintainersMarkerModifier>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kOneShotMod)
				{
					PlOneShotMod plobj(uObject.AddComponent<PlOneShotMod>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kSittingModifier)
				{
					PlSittingModifier plobj(uObject.AddComponent<PlSittingModifier>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kRandomSoundMod)
				{
					PlRandomSoundMod plobj(uObject.AddComponent<PlRandomSoundMod>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kSpawnModifier)
				{
					// spawn modifiers are really empty objects... Just set a tag.
					String tag("SpawnPoint");
					uObject.SetTag(tag);
				}
				else if (modifierType == pdClassType::kPanicLinkRegion)
				{
					PlPanicLinkRegion plobj(uObject.AddComponent<PlPanicLinkRegion>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kWaveSet7)
				{
					PlWaveSet7 plobj(uObject.AddComponent<PlWaveSet7>());
					ImporterUtils::keyToObj_Set(modifierKey, plobj);
				}
				else if (modifierType == pdClassType::kInterfaceInfoModifier)
				{
				} // looks like a legacy object. Doesn't do anything in Plasma.
				// END LOGIC/NOTIFY/EVENTS COMPONENTS
			}
			for (plKey interfaceKey : interfaces) // shadow casters
			{
				short type = interfaceKey->getType();
				if (type == pdClassType::kPointShadowMaster || type == pdClassType::kDirectShadowMaster)
					shadowCasterKey = &interfaceKey;
			}
			for (plKey interfaceKey : interfaces) // light
			{
				short type = interfaceKey->getType();

				// the three light types supported in Plasma
				// TODO figure out if we should import plLimitedDirLightInfo, which is really a very specific light type...
				if (type == pdClassType::kOmniLightInfo ||
					type == pdClassType::kDirectionalLightInfo ||
					type == pdClassType::kSpotLightInfo)
				{
					// import light, and pass a plKey* to the shadow caster, if it exists
					if (objOverride.Handle)
						lightImporter.import(uObject, interfaceKey, version, shadowCasterKey, objOverride.Handle && !objOverride.GetEnableLight(), objOverride.GetLightOverride());
					else
						lightImporter.import(uObject, interfaceKey, version, shadowCasterKey, objOverride.Handle && !objOverride.GetEnableLight(), 0);
				}
				else if (type == pdClassType::kLimitedDirLightInfo)
				{
					if (objOverride.Handle)
						lightImporter.importLimitedDirLight(uObject, interfaceKey, version, assetsBinder, objOverride.Handle && !objOverride.GetEnableLight(), objOverride.GetLightOverride());
					else
						lightImporter.importLimitedDirLight(uObject, interfaceKey, version, assetsBinder, objOverride.Handle && !objOverride.GetEnableLight(), 0);
				}
			}

			Int32 layerCS;
			if (NativeHelpers::GetObjOverrideLayer(clues, objName, &layerCS))
				uObject.SetLayer((int)layerCS);

			if (objIsStatic && drawKey && drawKey.Exists() && drawKey.isLoaded())
			{
				staticGOs[i] = uObject;
				totalBatchedObjects++;
			}
		}

		// rebuild hierarchy
		SuckyMap<plKey, int> prpToUnityObjects;
		ImporterUtils::keyToObj_GetAllIndicesFromPage(prpToUnityObjects, pageLoc);
		for (plKey objKey : prpToUnityObjects.keys)
		{
			if (objKey->getType() != pdClassType::kSceneObject)
				// that's a modifier or texture or something - ignore it.
				continue;
			Transform parentObj = (NativeHelpers::CastObjTo<PlSceneObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObjects.get(objKey)))).GetTransform();
			plSceneObject* obj = plSceneObject::Convert(objKey->getObj(), true);
			plKey coordKey = obj->getCoordInterface();
			if (!coordKey.Exists() || !coordKey.isLoaded())
				continue;
			plCoordinateInterface* coord = plCoordinateInterface::Convert(coordKey->getObj(), true);
			std::vector<plKey>& children = coord->getChildren();
			for (plKey child : children)
			{
				if (!child.Exists() || !child.isLoaded())
					continue;
				for (plKey key2 : prpToUnityObjects.keys)
				{
					if (key2 == child)
					{
						// this is the child sceneObject - set parent...
						GameObject childObj = NativeHelpers::CastObjTo<PlSceneObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObjects.get(key2))).GetGameObject();
						childObj.GetTransform().SetParent(parentObj);
						plSceneObject* childObjSO = plSceneObject::Convert(key2->getObj(), true);
						coordImporter.importLocal2Parent(childObj, childObjSO->getCoordInterface(), version);
						break;
					}
				}
			}
		}

		// check our currently static objects. If a non-static one is in the parent hierarchy, remove it from statics.
		// (FIXME: parsing the hierarchy in such a way could be really slow with the current handle system...)
		//for (int i = 0; i < totalBatchedObjects; i++)
		//{
		//	GameObject obj = staticGOs[i];
		//	Transform parentTransform(obj.GetTransform().GetParent());
		//	do {
		//		bool parentIsStatic = true;
		//		for (int j = 0; j < totalBatchedObjects && parentIsStatic; j++)
		//			if (parentObjTransform.GetGameObject() == staticGOs[j])
		//				parentIsStatic = false;
		//		if (!parentIsStatic)
		//		{
		//			// parent isn't static.
		//			// WAIT... non-drawable meshes aren't static, so we're screwed. Well fuck this
		//			break;
		//		}
		//		parentTransform = parentTransform.GetParent();
		//	} while (parentTransform.Handle != 0);
		//}

		// set bones for skinned mesh renderers
		for (plKey objKey : prpToUnityObjects.keys)
		{
			if (objKey->getType() != pdClassType::kSceneObject)
				// that's a modifier or texture or something - ignore it.
				continue;
			GameObject uObject = NativeHelpers::CastObjTo<PlSceneObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObjects.get(objKey))).GetGameObject();

			plSceneObject* plObj = plSceneObject::Convert(objKey->getObj(), true);
			// get all interesting keys/objects
			plKey drawKey = plObj->getDrawInterface();

			drawImporter.postProcessSkinnedObject(uObject, drawKey, version);
		}


		// import pool objects (eg: clusters)
		for (plKey poolObjKey : sceneNode->getPoolObjects())
		{
			if (poolObjKey->getType() == pdClassType::kClusterGroup)
				drawImporter.importCluster(parentObj, poolObjKey, version, assetsBinder);
			else if (poolObjKey->getType() == pdClassType::kMsgForwarder)
			{
				String objName(poolObjKey->getName().c_str());
				GameObject uObject(objName);
				uObject.GetTransform().SetParent(parentObjTransform);
				PlMessageForwarder uMsgFwd(uObject.AddComponent<PlMessageForwarder>());
				ImporterUtils::keyToObj_Set(poolObjKey, uMsgFwd);
				plMsgForwarder* msgFwd = plMsgForwarder::Convert(poolObjKey->getObj());
				for (plKey recv : msgFwd->getForwardKeys())
				{
					IPlMessageable msgReceiver(NativeHelpers::CastObjTo<IPlMessageable>(ImporterUtils::keyToObj_Get(recv)));
					uMsgFwd.GetForwardKeys().Add(msgReceiver);
				}
			}
		}

		// apply runtime static batching, which lowers draw calls and thus increases the framerate.
		// Theoretically should have a negligible impact on loading time. Unfortunately, Uru Ages
		// rarely reuse materials, which makes static batching much less efficient (still worth it tho)...
		// (before we do that, make sure to remove null objects from the array. Also, make sure
		// there isn't an animator in the parent hierarchy...)
		for (int objIndex = 0; objIndex < staticGOs.GetLength(); objIndex++)
		{
			GameObject obj(staticGOs[objIndex]);
			if (!obj.Handle)
				continue;
			if (NativeHelpers::HasComponentInParent<ICannotBeBatched>(obj))
			{
				staticGOs[objIndex] = nullptr;
				totalBatchedObjects--;
			}
		}
		Array1<GameObject> staticGOsNonNull(totalBatchedObjects);
		int i = 0;
		for (int objIndex = 0; objIndex < staticGOs.GetLength(); objIndex++)
		{
			GameObject obj(staticGOs[objIndex]);
			if (obj.Handle)
				staticGOsNonNull[i++] = obj;
		}
		if (i)
		{
			char msg[256];
			sprintf_s(msg, sizeof msg, "Marking %d objects as batchable static.", totalBatchedObjects);
			String msgc(msg);
			Debug::Log(msgc);

			StaticBatchingUtility::Combine(staticGOsNonNull, parentObj);
		}
	}
	void PageImporter::postProcessImport(plResManager & rmgr, const plLocation & pageLoc, AssetsLifeBinder assetsBinder)
	{
		// setup modifiers now that we have all the objects.
		componentImporter.setupComponents(pageLoc, pageLoc.getVer(), assetsBinder);
	}
}
