/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "ImporterUtils.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

const float PlImporter::ImporterUtils::importScale = .3048f;
const float PlImporter::ImporterUtils::PI = 3.14159265358979323846f;
PlImporter::SuckyMap<plLocation, PlImporter::SuckyMap<plKey, int>> PlImporter::ImporterUtils::knownObjectIndicesPerPage;
PlImporter::ImporterUtils::UObjectList PlImporter::ImporterUtils::allKnownObjects(0);
PlImporter::SuckyMap<plKey, std::vector<int>> PlImporter::ImporterUtils::keyToUObjects;
int PlImporter::ImporterUtils::unlitLyrMask = -1;
//int PlImporter::ImporterUtils::charOnlyLyrMask = -1;
int PlImporter::ImporterUtils::unlitLyr = -1;
int PlImporter::ImporterUtils::litLyr = -1;
//int PlImporter::ImporterUtils::charLyr = -1;
int PlImporter::ImporterUtils::camLyr = -1;
PlImporter::ExtendedResManager* PlImporter::ImporterUtils::curResManager;
std::vector<plLocation> PlImporter::ImporterUtils::prpCluesKeys;
System::Collections::Generic::List_1<PrpImportClues> PlImporter::ImporterUtils::prpCluesValues(0);

UnityEngine::Color32 PlImporter::ImporterUtils::getColor32FromIntColor(int col)
{
	System::Byte a = (col & 0xff000000) >> 24;
	System::Byte r = (col & 0xff0000) >> 16;
	System::Byte g = (col & 0xff00) >> 8;
	System::Byte b = col & 0xff;
	UnityEngine::Color32 c(r, g, b, a);
	return c;
}

void PlImporter::ImporterUtils::getComponentsFromIntColor(int col, int & r, int & g, int & b, int & a)
{
	a = (col & 0xff000000) >> 24;
	r = (col & 0xff0000) >> 16;
	g = (col & 0xff00) >> 8;
	b = col & 0xff;
}

ST::string PlImporter::ImporterUtils::stripIllegalChars(const char* name)
{
	ST::string s(name);
	return stripIllegalChars(s);
}

ST::string PlImporter::ImporterUtils::stripIllegalChars(ST::string name)
{
	return name
		.replace("Material #", "m_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("_LIGHTMAPGEN", "_LM", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace(" ", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace(".hsm", "", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace(".", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("~", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("#", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("*", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("?", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("\\", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("/", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace(">", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("<", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace(":", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("&", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		.replace("|", "_", ST::case_sensitivity_t::case_sensitive, ST::utf_validation_t::assume_valid)
		;
}

ST::string PlImporter::ImporterUtils::sharpStringToTheoryString(System::String str)
{
	if (str.Handle)
	{
		// copy string content from managed memory into unmanaged one. Then use the voidpointer returned as char array.
		// This works because of pointer majiicks and both ST and C#String being unicode.
		// Booyeah !
		const char16_t* chrs = (const char16_t*)Runtime::InteropServices::Marshal::StringToHGlobalUni(str);
		ST::string theory(chrs);
		// err, don't forget to release unmanaged memory.
		Runtime::InteropServices::Marshal::FreeHGlobal((void*)chrs);
		return theory;
	}
	else
	{
		// this obviously won't work...
		ST::string empty("");
		return empty;
	}
}
ST::string PlImporter::ImporterUtils::plasmaVerToPathsafeString(PlasmaVer ver)
{
	//ST::string versionName(PlasmaVer::GetVersionName(ver));
	// What the fuck ? PlasmaVer::GetVersionName returns local const char* variable.
	// Let's not use that, and simply copy the code here.

	ST::string versionName("unknown");

	switch (ver) {
	case PlasmaVer::pvPrime:
		versionName = "Prime/UU";
	case PlasmaVer::pvPots:
		versionName = "PotS/CC";
	case PlasmaVer::pvMoul:
		versionName = "MOUL/MQO";
	case PlasmaVer::pvEoa:
		versionName = "Myst V/Crowthistle";
	case PlasmaVer::pvHex:
		versionName = "Hex Isle";
	case PlasmaVer::pvUniversal:
		versionName = "Universal";
	default:
		if (ver < PlasmaVer::pvPrime)
			versionName = "Choru";
		if (ver > PlasmaVer::pvPots && ver < PlasmaVer::pvMoul)
			versionName = "MOUL Beta";
		else
			versionName = "Unknown";
	}

	return stripIllegalChars(versionName);
}
ST::string PlImporter::ImporterUtils::getGameFile(ST::string relativePath)
{
	String csstr(relativePath.c_str());
	PlGameFileAccessor accessor(PlGameFileAccessor::GetInstance());
	String pathcsstr(accessor.GetFile(csstr));
	ST::string pathTheory(sharpStringToTheoryString(pathcsstr));
	return pathTheory;
}
bool PlImporter::ImporterUtils::parseInt(ST::string str, int& out)
{
	// fuck cpp
	// https://stackoverflow.com/questions/4442658/c-parse-int-from-string

	const char* s = str.c_str();

	bool negate = (s[0] == '-');
	if (*s == '+' || *s == '-')
		++s;

	int result = 0;
	while (*s)
	{
		if (*s >= '0' && *s <= '9')
			result = result * 10 - (*s - '0');  //assume negative number
		else
			return false;
		++s;
	}
	out = negate ? result : -result; //-result is positive!
	return true;
}
bool PlImporter::ImporterUtils::parseUInt(ST::string str, unsigned int& out)
{
	int i;
	if (!parseInt(str, i))
		// could not parse
		return false;
	if (i < 0)
		// negative
		return false;
	out = (unsigned int)i;
	return true;
}
void PlImporter::ImporterUtils::keyToObj_Set(const plKey& key, UnityEngine::Object obj)
{
	if (!allKnownObjects.Handle)
	{
		// initialize the list now we have access to the cs engine
		UObjectList newList;
		allKnownObjects = newList;
	}
	plLocation location = key->getLocation();
	if (knownObjectIndicesPerPage.containsKey(location))
	{
		// we already have registered the page, now add/update the key and object
		SuckyMap<plKey, int>& keyMap(knownObjectIndicesPerPage.get(location));
		if (keyMap.containsKey(key))
		{
			// already exists, update
			allKnownObjects.SetItem(keyMap.get(key), obj);
		}
		else
		{
			// is new, create
			keyMap.set(key, allKnownObjects.GetCount());
			allKnownObjects.Add(obj);
		}
	}
	else
	{
		// we don't have this page yet... create it
		SuckyMap<plKey, int> newMap;
		newMap.set(key, allKnownObjects.GetCount());
		allKnownObjects.Add(obj);

		knownObjectIndicesPerPage.set(location, newMap);
	}
}
void PlImporter::ImporterUtils::keyToObj_SetWithSharingPlKey(const plKey& key, UnityEngine::Object obj)
{
    if (!keyToUObjects.containsKey(key))
    {
        std::vector<int> vec;
        keyToUObjects.set(key, vec);
    }
    keyToUObjects.get(key).push_back(allKnownObjects.GetCount());
    allKnownObjects.Add(obj);
}
UnityEngine::Object PlImporter::ImporterUtils::keyToObj_Get(const plKey& key)
{
	if (!allKnownObjects.Handle)
	{
		// initialize the list now we have access to the cs engine
		UObjectList newList;
		allKnownObjects = newList;
	}
	plLocation location = key->getLocation();
	if (knownObjectIndicesPerPage.containsKey(location))
	{
		// page found. Try to find the object
		SuckyMap<plKey, int>& keyMap = knownObjectIndicesPerPage.get(location);

		if (keyMap.containsKey(key))
		{
			// object found. Fetch it.
			return allKnownObjects.GetItem(keyMap.get(key));
		}
	}

	// not found - return c# null
	return 0;
}
UnityEngine::Object PlImporter::ImporterUtils::keyToObj_GetWithSharedKey(const plKey& key, int index)
{
    if (!keyToUObjects.containsKey(key))
        return 0;

    return allKnownObjects.GetItem(keyToUObjects.get(key)[index]);
}
int PlImporter::ImporterUtils::keyToObj_GetSharedKeyCount(const plKey& key)
{
    if (!keyToUObjects.containsKey(key))
        return 0;
    return (int)keyToUObjects.get(key).size();
}

void PlImporter::ImporterUtils::keyToObj_GetAllIndicesFromPage(SuckyMap<plKey, int>& mapOUT, const plLocation& location)
{
	if (!allKnownObjects.Handle)
	{
		// initialize the list now we have access to the cs engine
		UObjectList newList;
		allKnownObjects = newList;
	}

	mapOUT = knownObjectIndicesPerPage.get(location);
}

UnityEngine::Object PlImporter::ImporterUtils::keyToObj_GetObjFromId(int id)
{
	return allKnownObjects.GetItem(id);
}

void PlImporter::ImporterUtils::keyToObj_Clear(const plLocation& location)
{
	knownObjectIndicesPerPage.unset(location);
}

void PlImporter::ImporterUtils::prpClues_Set(const plLocation& key, PrpImportClues obj)
{
	if (prpCluesValues == 0)
	{
		System::Collections::Generic::List_1<PrpImportClues> newList;
		prpCluesValues = newList;
	}

	for (int i = 0; i < prpCluesKeys.size(); i++) {
		if (prpCluesKeys[i] == key) {
			// update the element
			prpCluesValues.SetItem(i, obj);
			return;
		}
	}
	// insert a new element
	prpCluesKeys.push_back(key);
	prpCluesValues.Add(obj);
}

PrpImportClues PlImporter::ImporterUtils::prpClues_Get(const plLocation& key)
{
	if (prpCluesValues == 0)
	{
		System::Collections::Generic::List_1<PrpImportClues> newList;
		prpCluesValues = newList;
	}
	for (int i = 0; i < prpCluesKeys.size(); i++)
		if (prpCluesKeys[i] == key)
			return prpCluesValues.GetItem(i);
	return 0;
}

void PlImporter::ImporterUtils::prpClues_Clear()
{
	if (prpCluesValues == 0)
	{
		System::Collections::Generic::List_1<PrpImportClues> newList;
		prpCluesValues = newList;
	}
	prpCluesKeys.clear();
	prpCluesValues.Clear();
}

void PlImporter::ImporterUtils::prpClues_SaveAll()
{
	if (prpCluesValues == 0)
	{
		System::Collections::Generic::List_1<PrpImportClues> newList;
		prpCluesValues = newList;
	}
	int count = prpCluesValues.GetCount();
	for (int i = 0; i < count; i++)
		prpCluesValues.GetItem(i).Save();
}

PlasmaVer PlImporter::ImporterUtils::getPlasmaVerFromGame(PlasmaGame game)
{
	/*if (game == PlasmaGame::untilUru)
	return PlasmaVer::pvPrime;
	if (game == PlasmaGame::agesBeyondMyst)
	return PlasmaVer::pvPrime;
	if (game == PlasmaGame::toDni)
	return PlasmaVer::pvPrime;*/
	if (game == PlasmaGame::CompleteChronicles)
		return PlasmaVer::pvPots;
	if (game == PlasmaGame::MystOnline)
		return PlasmaVer::pvMoul;
	if (game == PlasmaGame::EndOfAges)
		return PlasmaVer::pvEoa;
	if (game == PlasmaGame::CrowThistle)
		return PlasmaVer::pvEoa;
	/*if (game == PlasmaGame::magiQuest)
	return PlasmaVer::pvMoul;
	if (game == PlasmaGame::hexIsle)
	return PlasmaVer::pvHex;*/

	return PlasmaVer::pvPots;
}

void PlImporter::ImporterUtils::init()
{
	// fetch all layers

	String noLightLyrName("Plasma Unlit");
	String lightLyrName("Default");
	//String charLyrName("Plasma Characters");
	String camLyrName("Ignore Raycast"); // we can still raycast manually on this layer, but this ensures OnMouseEnter etc work through these.
	
	unlitLyr = LayerMask::NameToLayer(noLightLyrName);
	litLyr = LayerMask::NameToLayer(lightLyrName);
	//charLyr = LayerMask::NameToLayer(charLyrName);
	camLyr = LayerMask::NameToLayer(camLyrName);

	if (unlitLyr != -1)
		unlitLyrMask = ~(1 << unlitLyr);
	//if (charLyr != -1)
	//charOnlyLyrMask = (1 << charLyr);
}


hsKeyedObject * PlImporter::ExtendedResManager::getObjectFromName(ST::string objName, short type, const plLocation & loc)
{
	for (plKey key : keys.getKeys(loc, type))
	{
		if (key.Exists() && key.isLoaded())
		{
			if (key->getName() == objName)
				return getObject(key);
		}
	}
	return NULL;
}
plKey PlImporter::ExtendedResManager::getObjectKeyFromName(ST::string objName, short type, const plLocation & loc)
{
	for (plKey key : keys.getKeys(loc, type))
	{
		if (key.Exists() && key.isLoaded())
		{
			if (key->getName() == objName)
				return key;
		}
	}
	plKey nullkey;
	return nullkey;
}

void PlImporter::ImporterUtils::keyToObj_InitPage(const plLocation & location)
{
	SuckyMap<plKey, int> emptyMap;
	knownObjectIndicesPerPage.set(location, emptyMap);
}

void PlImporter::ImporterUtils::print(ST::string str)
{
    String cstr(str.c_str());
    Debug::Log(cstr);
}

bool PlImporter::ExtendedSimSuppressMsg::getSuppress()
{
    return fSuppress;
}
