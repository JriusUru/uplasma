/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "MatImporter.h"


using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

namespace PlImporter
{
	void MatImporter::import(Material& matOUT, const plKey & matKey, Cubemap& cubeOut, GameObject lyrAnimParentObject,
		PlasmaVer version, AssetsLifeBinder assetsBinder)
	{
		if (!matKey.Exists() || !matKey.isLoaded())
			return;

		// check if we already imported the material
		UnityEngine::Object texObj = ImporterUtils::keyToObj_Get(matKey);
		if (texObj.Handle != 0)
		{
			// already imported. Return it.
			matOUT = NativeHelpers::CastObjTo<Material>(texObj);
			return;
		}

		// else import it
		hsGMaterial* plMat = hsGMaterial::Convert(matKey->getObj(), true);

		ST::string defaultShaderName("Plasma/Default");

		if (matKey->getLocation().getSeqPrefix() >= 100)
		{
			// for fages, try to automatically find the most appropriate shader
			// ... this is really basic guessing, hopefully this should allow most fages to look decent
			// without having to make a shader pack for them...
			// Note that we don't care about this for Cyan Ages. I prefer them to default
			// to the most simple possible shader, this makes it easier to identify materials
			// that should be modified. They all will have a dedicated shader list anyway.
			if (plMat->getPiggyBacks().size())
				defaultShaderName = "Plasma/Default, lightmapped";
			if (matKey->getName().to_lower().find("invis") != -1 ||
				matKey->getName().to_lower().find("transp") != -1)
				defaultShaderName = "Plasma/Null";
			int texIndex = 0;
			for (plKey layer : plMat->getLayers())
			{
				plKey lyrKey = getBaseLayerKey(layer, version);
				if (lyrKey != layer)
				{
					// we went through a layer animation, use an animated layer
					defaultShaderName = "Plasma/Animated, alpha blend";
				}
				plLayer* plLyr = plLayer::Convert(lyrKey->getObj());
				if (texIndex == 0)
				{
					if (plLyr->getState().fMiscFlags & hsGMatState::kMiscTwoSided)
						defaultShaderName = "Plasma/Default, double sided";
					if (plLyr->getState().fBlendFlags & hsGMatState::kBlendAlpha)
						// unfortunately used for all object in all fages... :roll:
						// defaultShaderName = "Plasma/Default, alpha blend";
						// use a simple alpha test instead and f it
						defaultShaderName = "Plasma/Default, alpha test";
					if (plLyr->getState().fMiscFlags & hsGMatState::kMiscTwoSided &&
						plLyr->getState().fBlendFlags & hsGMatState::kBlendAlpha)
						defaultShaderName = "Plasma/Default, alpha test (double sided)";
					if (plLyr->getState().fShadeFlags & hsGMatState::kShadeNoFog)
						defaultShaderName = "Plasma/Default, alpha blend, no fog";
					if (plLyr->getState().fBlendFlags & hsGMatState::kBlendAdd)
						defaultShaderName = "Plasma/Glow";
					plKey texKey = plLyr->getTexture();
					if (texKey && texKey.Exists() && texKey.isLoaded())
					{
						if (texKey->getType() == pdClassType::kCubicEnvironmap)
						{
							defaultShaderName = "Plasma/Cube only";
						}
					}
				}
				if (texIndex == 1)
				{
					// probably an overlay ?
					//defaultShaderName = "Plasma/Default, overlay uv1";
					if (plLyr->getState().fBlendFlags & hsGMatState::kBlendMult)
						// some people set lightmaps as mult textures instead of piggyback... no problem.
						defaultShaderName = "Plasma/Default, double sided";
				}
				texIndex++;
			}
		}

		String shaderName(defaultShaderName.c_str());
		ST::string correctMatName = ImporterUtils::stripIllegalChars(matKey->getName().c_str());
		String correctMatNameCs(correctMatName.c_str());
		plLocation loc = matKey->getLocation();
		PrpImportClues clue(ImporterUtils::prpClues_Get(loc));
		MatOverride matOverride(clue.GetMatOverride(correctMatNameCs));
		if (matOverride.Handle != 0)
		{
			// override name found B-)
			shaderName = matOverride.GetShaderName();
		}
		else
		{
			// we didn't create the shader code for this material yet
			// simply add it to the list...
			MatOverride overrde;
			overrde.SetShaderName(shaderName);
			clue.AddMatOverride(correctMatNameCs, overrde);
		}
		Shader s(Shader::Find(shaderName));
		Material uMat(s);
		String correctMatNameCS(correctMatName.c_str());
		uMat.SetName(correctMatNameCS);

		if (matOverride.Handle != 0)
		{
			// set the render queue if specified in the clues...
			int renderQueue = matOverride.GetRenderQueue();
			if (renderQueue != -1)
				uMat.SetRenderQueue(renderQueue);
		}

		// register the material so we don't import it twice
		ImporterUtils::keyToObj_Set(matKey, uMat);

		std::vector<plKey>& layers = plMat->getLayers();
		int texPropId = 0;
		for (plKey layer : layers)
		{
			importAnimLayers(layer, lyrAnimParentObject, true, version);

			plKey lyrKey = getBaseLayerKey(layer, version);
			if (isPlLayerVisibleAndSurface(lyrKey, version))
			{
				plLayer* plLyr = plLayer::Convert(lyrKey->getObj());
				bool hasTex = plLyr->getTexture().Exists() && plLyr->getTexture().isLoaded();
				if (hasTex && (plLyr->getTexture()->getName() == "ALPHA_BLEND_FILTER_U2ALPHA_TRANS_64x4" || plLyr->getTexture()->getName() == "ALPHA_BLEND_FILTER_V2ALPHA_TRANS_4x64"))
					// we really don't care about those.
					continue;

				char texPropC[256];
				char texPropColorC[256];
				char texPropAmbC[256];
				char texPropRuntimeC[256];
				char texPropOpacC[256];
				if (!texPropId)
				{
					sprintf_s(texPropC, sizeof texPropC, "_MainTex");
					sprintf_s(texPropColorC, sizeof texPropColorC, "_Color");
					sprintf_s(texPropAmbC, sizeof texPropAmbC, "_Amb");
					sprintf_s(texPropRuntimeC, sizeof texPropRuntimeC, "_Runtime");
					sprintf_s(texPropOpacC, sizeof texPropOpacC, "_Opac");
				}
				else
				{
					sprintf_s(texPropC, sizeof texPropC, "_Tex%d", texPropId + 1);
					sprintf_s(texPropColorC, sizeof texPropColorC, "_Color%d", texPropId + 1);
					sprintf_s(texPropAmbC, sizeof texPropAmbC, "_Amb%d", texPropId + 1);
					sprintf_s(texPropRuntimeC, sizeof texPropRuntimeC, "_Runtime%d", texPropId + 1);
					sprintf_s(texPropOpacC, sizeof texPropOpacC, "_Opac%d", texPropId + 1);
				}
				String texProp(texPropC);
				String texPropColor(texPropColorC);
				String texPropAmb(texPropAmbC);
				String texPropRuntime(texPropRuntimeC);
				String texPropOpac(texPropOpacC);

				if (hasTex)
				{
					Texture2D tex(0);
					bool clampU = (plLyr->getState().fClampFlags & hsGMatState::kClampTextureU) != 0;
					bool clampV = (plLyr->getState().fClampFlags & hsGMatState::kClampTextureV) != 0;
					// bumpmaps must be in linear space
					bool bump = (plLyr->getState().fMiscFlags & hsGMatState::kMiscBumpLayer) != 0;
					imgImporter.import(tex, plLyr->getTexture(),
						version, assetsBinder,
						clampU, clampV, bump);

					if (tex.Handle)
						uMat.SetTexture(texProp, tex);
				}
				uMat.SetFloat(texPropOpac, plLyr->getOpacity());

				hsColorRGBA preshade = plLyr->getPreshade();
				hsColorRGBA runtime = plLyr->getRuntime();
				hsColorRGBA ambient = plLyr->getAmbient();
				// We bound regular color to preshade parameter
				Color col(preshade.r, preshade.g, preshade.b, preshade.a);
				uMat.SetColor(texPropColor, col);
				// Now also specify ambient and runtime as it's required for some rarer shaders and fx
				Color amb(ambient.r, ambient.g, ambient.b, ambient.a);
				uMat.SetColor(texPropAmb, amb);
				Color run(runtime.r, runtime.g, runtime.b, runtime.a);
				uMat.SetColor(texPropRuntime, run);

				hsMatrix44 lyrTrs = plLyr->getTransform();
				const float* glMatrix = lyrTrs.glMatrix();

				// check if the matrix is a simple pos/scale mtx. If it is, apply it to the material. If not, let the shader work on it.
				if (hasTex &&
					glMatrix[1] == 0. &&
					glMatrix[2] == 0. &&
					glMatrix[3] == 0. &&
					glMatrix[4] == 0. &&
					glMatrix[6] == 0. &&
					glMatrix[7] == 0. &&
					glMatrix[8] == 0. &&
					glMatrix[9] == 0. &&
					glMatrix[11] == 0. &&
					glMatrix[14] == 0. &&
					glMatrix[15] == 1.
					)
				{
					// regular mtx, yay
					Vector2 offset; offset.Set(glMatrix[12], glMatrix[13]);
					Vector2 scale; scale.Set(glMatrix[0], glMatrix[5]);
					uMat.SetTextureOffset(texProp, offset);
					uMat.SetTextureScale(texProp, scale);
				}

				texPropId++;
			}
			else if (isPlLayerVisibleAndCubemap(lyrKey, version))
			{
				plLayer* plLyr = plLayer::Convert(lyrKey->getObj());
				//cubeOut = imgImporter.importCube(plLyr->getTexture(), version, assetsBinder);
				Cubemap cube(0);
				imgImporter.importCube(cube, plLyr->getTexture(), version, assetsBinder);

				/* Now we have the choice:
				- set this cubemap as property for the material (simple, Plasma way)
				- set this cubemap as texture for a newly created reflection probe, and let Unity's SSR hook into our shaders
				SSR is tempting, but has a lot of drawbacks:
				- not compatible with forward, so no benefit on this path
				- PBR shading (using the Surface shader struct) would make the objects look wrong
				- SSR might not be able to hook into shaders using custom surfaces
				- reflection is tied to probe. Setting a single probe for the MeshRenderer is possible with forward, but not with deferred.
				Meaning there might be conflicts with overlapping reflection probes all over the scene, or reflective objects
				might move outside the influence zone
				- overall more complex to code

				Going with the simple Plamza wei for now. Will look into other ways to hook SSR later.
				*/

				String cubeStr("_Cube");
				uMat.SetTexture(cubeStr, cube);

				char texPropCUBEC[256];
				sprintf_s(texPropCUBEC, sizeof texPropCUBEC, "_OpacCUBE");
				String texPropCUBE(texPropCUBEC);
				uMat.SetFloat(texPropCUBE, plLyr->getOpacity());
			}
		}
		std::vector<plKey> lightmaps = plMat->getPiggyBacks();
		if (lightmaps.size())
		{
			plKey lmKey = lightmaps[0];
			lmKey = getBaseLayerKey(lmKey, version); // yes, when there is a layer animation, PyPRP exports the lightmap as an empty layer anim...
			if (lmKey.Exists() && lmKey.isLoaded())
			{
				plLayer* lyr = plLayer::Convert(lmKey->getObj());

				Texture2D tex(0);
				imgImporter.import(tex, lyr->getTexture(), version, assetsBinder,
					lyr->getState().fClampFlags & hsGMatState::kClampTextureU,
					lyr->getState().fClampFlags & hsGMatState::kClampTextureV,
					true); // lightmaps are linear
				String texProp("_Lightmap");
				if (tex.Handle)
					uMat.SetTexture(texProp, tex);
			}
		}

		if (matOverride.Handle != 0)
			matOverride.ApplyPropsToMat(uMat);

		Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
		assets.Add(uMat);

		matOUT = uMat;
	}

	void MatImporter::import(Material& matOUT, const plKey & matKey, Cubemap& cubeOut, GameObject lyrAnimParentObject, plDrawableSpans * span, PlasmaVer version, AssetsLifeBinder assetsBinder)
	{
		import(matOUT, matKey, cubeOut, lyrAnimParentObject, version, assetsBinder);

		// DOESN'T WORK. Has some signed issues, and sorting isn't great anyway.
		// Will keep setting it by hand, much safer this way...
		if (matOUT.Handle && false)
		{
			// render level are per-span in Plasma (which is equivalent to per-object).
			// In unity, they are per-material. To circumvent that, we always assign the highest renderlevel available to the object.
			// Plasma draw order:
			//   renderLevel:
			//       left-most digit of the hex representation: major number (opaque, transp, etc). 0 to 3.
			//       rest of the hex repr: minor number. Up to a lot (I've seen 0x40 at some point)
			//   criteria: these are face sorting flags. Hopefully Unity does sorting on its own well enough.
			//   span number:
			//       up to 40 or so. The higher levels are used mostly for particles and such
			// Unity draw order:
			//   from 0 to 5000
			//       geometry: 2000
			//       alphatest: 2450
			//       transparent: 3000
			unsigned int renderLevel = span->getRenderLevel();
			unsigned int spanType = (renderLevel & plDrawableSpans::kSpanTypeMask) >> 30;
			unsigned int spanId = renderLevel & plDrawableSpans::kSpanIDMask;

			unsigned int criteria = span->getCriteria();
			// spanNumber = ??? // NOT a parameter of the span itself ! Need to fetch it from the key name ? That'd be ugly...

			// for now this should make sure major and minor level are correctly mapped to Plasma levels.
			// TODO: figure out if we also need to sort faces according to span number.
			int approxRenderQueue = 2000 + spanType * 500 + spanId;
			//if (mat.GetRenderQueue() < approxRenderQueue) // CRASH
			matOUT.SetRenderQueue(approxRenderQueue);
		}
	}

	void MatImporter::importLightLayer(UnityEngine::Material & matOUT, const plKey & lyrKey, PlasmaVer version, AssetsLifeBinder assetsBinder)
	{
		if (!lyrKey.Exists() || !lyrKey.isLoaded())
			return;

		plKey finalLyrKey = getBaseLayerKey(lyrKey, version);
		plLayer* plLyr = plLayer::Convert(finalLyrKey->getObj(), true);

		String projShdrName("Plasma/ProjectorLight");
		Shader projShdr(Shader::Find(projShdrName));
		Material projMat(projShdr);
		String matName(lyrKey->getName().c_str());
		projMat.SetName(matName);

		if (plLyr->getTexture().Exists() && plLyr->getTexture().isLoaded())
		{
			Texture2D tex(0);
			imgImporter.import(tex, plLyr->getTexture(),
				version, assetsBinder,
				true, true,
				true); // are those linear ? probably ?

			if (tex.Handle)
				projMat.SetMainTexture(tex);
		}

		assetsBinder.GetAssets().Add(projMat);
		matOUT = projMat;
	}

	plKey MatImporter::getBaseLayerKey(const plKey & plLayerKey, PlasmaVer version)
	{
		if (!plLayerKey.Exists() || !plLayerKey.isLoaded())
			return plLayerKey;

		if (plLayerKey->getType() == pdClassType::kLayer)
		{
			plLayer* plLyr = plLayer::Convert(plLayerKey->getObj(), true);
			if (plLyr->getUnderLay().Exists() && plLyr->getUnderLay().isLoaded())
				return getBaseLayerKey(plLyr->getUnderLay(), version);
		}
		else if (plLayerKey->getType() == pdClassType::kLayerLinkAnimation)
		{
			plLayerLinkAnimation* plLyr = plLayerLinkAnimation::Convert(plLayerKey->getObj(), true);
			if (plLyr->getUnderLay().Exists() && plLyr->getUnderLay().isLoaded())
				return getBaseLayerKey(plLyr->getUnderLay(), version);
		}
		else if (plLayerKey->getType() == pdClassType::kLayerAnimation)
		{
			plLayerAnimation* plLyr = plLayerAnimation::Convert(plLayerKey->getObj(), true);
			if (plLyr->getUnderLay().Exists() && plLyr->getUnderLay().isLoaded())
				return getBaseLayerKey(plLyr->getUnderLay(), version);
		}
		else if (plLayerKey->getType() == pdClassType::kLayerSDLAnimation)
		{
			plLayerSDLAnimation* plLyr = plLayerSDLAnimation::Convert(plLayerKey->getObj(), true);
			if (plLyr->getUnderLay().Exists() && plLyr->getUnderLay().isLoaded())
				return getBaseLayerKey(plLyr->getUnderLay(), version);
		}
		else if (plLayerKey->getType() == pdClassType::kLayerBink)
		{
			ST::string keyName(plLayerKey->getName());
			int index = plLayerKey->getObj()->ClassIndex();
			int keyindex = plLayerKey->getType();
			ST::string classname(plLayerKey->getObj()->ClassName());
			plLayerBink* plLyr = plLayerBink::Convert(plLayerKey->getObj(), true);
			if (plLyr->getUnderLay().Exists() && plLyr->getUnderLay().isLoaded())
				return getBaseLayerKey(plLyr->getUnderLay(), version);
		}
		return plLayerKey;
	}
	bool MatImporter::isPlLayerVisibleAndSurface(const plKey & plLayerKey, PlasmaVer version)
	{
		if (!plLayerKey.Exists() || !plLayerKey.isLoaded())
			return false;
		if (plLayerKey->getType() == pdClassType::kLayer)
		{
			plLayer* plLyr = plLayer::Convert(plLayerKey->getObj(), true);
			/*if (plLyr->getState().fBlendFlags & hsGMatState::kBlendNoTexColor)
			return false;*/
			if (plLyr->getTexture().Exists() && plLyr->getTexture().isLoaded())
			{
				if (plLyr->getTexture()->getType() == pdClassType::kCubicEnvironmap ||
					plLyr->getTexture()->getType() == pdClassType::kDynamicEnvMap ||
					plLyr->getTexture()->getType() == pdClassType::kDynamicCamMap)
					return false;
			}
			return true;
		}
		return false;
	}
	bool MatImporter::isPlLayerVisibleAndCubemap(const plKey & plLayerKey, PlasmaVer version)
	{
		if (!plLayerKey.Exists() || !plLayerKey.isLoaded())
			return false;
		if (plLayerKey->getType() == pdClassType::kLayer)
		{
			plLayer* plLyr = plLayer::Convert(plLayerKey->getObj(), true);
			/*if (plLyr->getState().fBlendFlags & hsGMatState::kBlendNoTexColor)
			return false;*/
			if (!plLyr->getTexture().Exists() || !plLyr->getTexture().isLoaded())
				return false;
			if (plLyr->getTexture()->getType() == pdClassType::kCubicEnvironmap)
				return true;
		}
		return false;
	}
	bool MatImporter::isPlLayerMask(const plKey & plLayerKey, PlasmaVer version)
	{
		if (!plLayerKey.Exists() || !plLayerKey.isLoaded())
			return false;
		if (plLayerKey->getType() == pdClassType::kLayer)
		{
			plLayer* plLyr = plLayer::Convert(plLayerKey->getObj(), true);
			return  (plLyr->getState().fBlendFlags & hsGMatState::kBlendAlpha) &&
				(plLyr->getState().fBlendFlags & hsGMatState::kBlendAlphaMult) &&
				(plLyr->getState().fBlendFlags & hsGMatState::kBlendNoTexColor);
		}
		return false;
	}
	void MatImporter::importAnimLayers(const plKey & plLayerKey, GameObject animObject, bool createAnimObject, PlasmaVer version)
	{
		if (plLayerKey->getType() != pdClassType::kLayerAnimation)
			// already in a normal layer
			return;

		plLayerAnimation* plLyr = plLayerAnimation::Convert(plLayerKey->getObj(), true);
		if (createAnimObject)
		{
			// material animation object wasn't created yet - do so now
			String animObjName("LayerAnim");
			GameObject animSubObject(animObjName);
			animSubObject.GetTransform().SetParent(animObject.GetTransform());
			animSubObject.AddComponent<Animation>();
			// simply modify the reference so we use this new object from now on.
			animObject = animSubObject;
		}
		//else
		// material animation object was already created, simply add a PlLayerAnimation to it

		PlLayerAnimation uLyrAnim(animObject.AddComponent<PlLayerAnimation>());

		//uLyrAnim.SetAnimPlayer();

		plController* preshadeCtrl = plLyr->getPreshadeCtl();
		plController* runtimeCtrl = plLyr->getRuntimeCtl();
		plController* ambientCtrl = plLyr->getAmbientCtl();
		plController* specularCtrl = plLyr->getSpecularCtl();
		plController* opacCtrl = plLyr->getOpacityCtl();
		plController* uvwCtrl = plLyr->getPreshadeCtl();

		/*if (preshadeCtrl)
		animImporter.importAnimClip(0, preshadeCtrl, "");*/

		// register the animation so other objects can message it
		ImporterUtils::keyToObj_Set(plLayerKey, uLyrAnim);
		// and process the next anim !
		importAnimLayers(plLyr->getUnderLay(), animObject, false, version);
	}
}
