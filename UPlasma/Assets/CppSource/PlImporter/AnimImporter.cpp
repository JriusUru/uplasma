/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "AnimImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

/*
Worth noting - conversion from Plasma's space to Unity's:
- Quats are always converted directly from the native bindings
- Vectors are always converted by the C# animator script. This includes the following:
	- foot to meter conversion
	- Y/Z axis swap
	- conversion to degrees when necessary
Reason: quats will always be a rotation and use Unity's own class, so it's better to
setup them correctly from the start. Vectors can represent various things (scale, pos, eulers, colors...)
so it's better to wait until we know exactly what they are used for.
*/

namespace PlImporter
{
	void AnimImporter::importAGModifier(GameObject& gameObject, const plKey& agmKey, PlasmaVer version, AssetsLifeBinder& assetsBinder)
	{
		if (!agmKey.Exists() || !agmKey.isLoaded())
			return;

		plAGModifier* agm = plAGModifier::Convert(agmKey->getObj());
		String channelName(agm->getChannelName().c_str());
		PlAgModifier uAgModifier = gameObject.AddComponent<PlAgModifier>();
		uAgModifier.SetChannelName(channelName);
		// other properties available in hsplasma: fAutoApply, fEnabled. I doubt we're going to need those...
	}
	void AnimImporter::importAGMasterMod(UnityEngine::GameObject & gameObject, const plKey & agmmKey, PlasmaVer version, AssetsLifeBinder & assetsBinder, bool doDisable)
	{
		if (!agmmKey.Exists() || !agmmKey.isLoaded())
			return;

		ST::string agmmName(agmmKey->getName());
		plAGMasterMod* agmm = plAGMasterMod::Convert(agmmKey->getObj());
		std::vector<plKey>& anims = agmm->getPrivateAnims();
        currentlyProcessedObjectName = ImporterUtils::sharpStringToTheoryString(gameObject.GetName());

		PlAgMasterMod uAgMasterMod = gameObject.AddComponent<PlAgMasterMod>();
        Array1<AgAnim> mmAnims((Int32)(int32_t)anims.size());
		uAgMasterMod.SetAnims(mmAnims);

        // TODO:
        //uAgMasterMod.SetGroupName(String(agmm->getGroupName())); // not actually required, was removed in pots
        //uAgMasterMod.SetIsGrouped(agmm->getIsGrouped());
        //uAgMasterMod.SetIsGroupMaster(agmm->getIsGroupMaster());
        //uAgMasterMod.SetMsgForwarder(agmm->getMsgForwarder());

		int agAnimId = 0;
		for (plKey anim : anims)
		{
			if (!anim.Exists() || !anim.isLoaded())
				continue;

			plAGAnim* agAnim = plAGAnim::Convert(anim->getObj());

            currentlyProcessedAnimName = agAnim->getName();
            AgAnim uAgAnim(0);
            if (anim->getType() == kAgeGlobalAnim)
            {
                plAgeGlobalAnim* ageGlobalAnim = (plAgeGlobalAnim*)agAnim;
                AgeGlobalAnim uAgeAnim;
                uAgAnim = uAgeAnim;
                String globalVarName(ageGlobalAnim->getGlobalVarName().c_str());
                uAgeAnim.SetGlobalVarName(globalVarName);
            }
            else if (anim->getType() == kATCAnim)
            {
                plATCAnim* atcAnim = (plATCAnim*)agAnim;
                AtcAnim uAtcAnim;
                uAgAnim = uAtcAnim;

				uAtcAnim.SetInitial(atcAnim->getInitial());
				uAtcAnim.SetLoopStart(atcAnim->getLoopStart());
				uAtcAnim.SetLoopEnd(atcAnim->getLoopEnd());
				uAtcAnim.SetAutoStart(atcAnim->getAutoStart());
				uAtcAnim.SetLoop(atcAnim->getDoLoop());
				uAtcAnim.SetEaseInType(atcAnim->getEaseInType());
				uAtcAnim.SetEaseOutType(atcAnim->getEaseOutType());
				uAtcAnim.SetEaseInLength(atcAnim->getEaseInLength());
				uAtcAnim.SetEaseInMin(atcAnim->getEaseInMin());
				uAtcAnim.SetEaseInMax(atcAnim->getEaseInMax());
				uAtcAnim.SetEaseOutLength(atcAnim->getEaseOutLength());
				uAtcAnim.SetEaseOutMin(atcAnim->getEaseOutMin());
				uAtcAnim.SetEaseOutMax(atcAnim->getEaseOutMax());
                
                // TODO - set atc marker/loops/stop points
            }

            ST::string completeAnimNameT(anim->getName());
            String completeAnimName(completeAnimNameT.c_str());

            uAgAnim.SetBlend(agAnim->getBlend());
            uAgAnim.SetEnd(agAnim->getEnd());
            uAgAnim.SetStart(agAnim->getStart());
            uAgAnim.SetName(completeAnimName);

			// now import the curve...
			std::vector<plAGApplicator*>& applicators = agAnim->getApplicators();
            Array1<IAgApplicator> uApplicators((Int32)(int32_t)applicators.size());
			uAgAnim.SetApplicators(uApplicators);
			int applicatorId = 0;
            for (plAGApplicator* applicator : applicators)
            {
				IAgApplicator uApplicator(importAnimApplicator(gameObject, applicator));
				if (uApplicator.Handle != 0)
				{
					String channelName(applicator->getChannelName().c_str());
					uApplicator.SetChannelName(channelName);
					uApplicator.SetEnabled(applicator->isEnabled());
				}
				uApplicators[applicatorId++] = uApplicator;
            }

            mmAnims[agAnimId++] = uAgAnim;
		}

		ImporterUtils::keyToObj_Set(agmmKey, uAgMasterMod);
	}
	UPlasma::PlEmu::IAgApplicator AnimImporter::importAnimApplicator(UnityEngine::GameObject& gameObject, plAGApplicator * applicator)
    {
        plAGChannel* chan = applicator->getChannel();
        short classIndex = chan->ClassIndex();

        if (classIndex == kMatrixControllerChannel && applicator->ClassIndex() == kMatrixChannelApplicator)
        {
            // transform animation
            plMatrixControllerChannel* mcc = (plMatrixControllerChannel*)chan;
            TrsControllerChannel uTrscc;
			MatrixChannelApplicator uApplicator;
            uApplicator.SetChannel(uTrscc);

            importMatrixControllerChannel(uTrscc, mcc);

			return uApplicator;
        }
        else if (classIndex == kPointControllerChannel && applicator->ClassIndex() == kLightDiffuseApplicator)
        {
            // light animation
            plPointControllerChannel* pcc = (plPointControllerChannel*)chan;
			PointControllerChannel uPcc;
			LightDiffuseApplicator uApplicator;
			uApplicator.SetChannel(uPcc);

			importPointControllerChannel(uPcc, pcc);

			return uApplicator;
        }
        else if (classIndex == kScalarControllerChannel && applicator->ClassIndex() == kParticlePPSApplicator)
        {
            // particle per second
			plScalarControllerChannel* scc = (plScalarControllerChannel*)chan;
			ScalarControllerChannel uScc;
			ParticlePpsApplicator uApplicator;
			uApplicator.SetChannel(uScc);

			importScalarControllerChannel(uScc, scc);

			return uApplicator;
        }
        else
        {
            char buf[256];
            sprintf_s(buf, 256, "Unsupported channel controller type %d ! (applicator: %d)", classIndex, applicator->ClassIndex());
            String msg(buf);
            Debug::LogError(msg);
        }
		return nullptr;
    }
	void AnimImporter::importMatrixControllerChannel(TrsControllerChannel& uTrscc, plMatrixControllerChannel* mcc)
    {
        String mccName(mcc->getName().c_str());
        uTrscc.SetName(mccName);

        AffineParts uAffineParts;
        hsAffineParts affineParts = mcc->getAffine();
        uAffineParts.SetI(affineParts.fI);
		// REMEMBER: only quats are converted to Unity's axis system from within the native bindings.
        Vector3 translation(affineParts.fT.X, affineParts.fT.Y, affineParts.fT.Z);
        Quaternion rotation(affineParts.fQ.X, affineParts.fQ.Z, affineParts.fQ.Y, -affineParts.fQ.W);
        Quaternion scaleRotation(affineParts.fU.X, affineParts.fU.Z, affineParts.fU.Y, -affineParts.fU.W);
        Vector3 scale(affineParts.fK.X, affineParts.fK.Y, affineParts.fK.Z);
        uAffineParts.SetTranslation(translation);
        uAffineParts.SetRotation(rotation);
        uAffineParts.SetScaleRotation(scaleRotation);
        uAffineParts.SetScale(scale);
        uAffineParts.SetSignOfDeterminant(affineParts.fF);
        uTrscc.SetAffineParts(uAffineParts);

		TrsController trsCtrl;
		uTrscc.SetController(trsCtrl);

        plController* ctrl = mcc->getController();
        short ctrlClassIndex = ctrl->ClassIndex();
        if (ctrlClassIndex == kTMController)
        {
			// PotS
            plTMController* tmController = (plTMController*)ctrl;
			importController(trsCtrl, tmController);
        }
        else if (ctrlClassIndex == kCompoundController)
        {
			// MOUL
			plCompoundController* cpController = (plCompoundController*)ctrl;
			importController(trsCtrl, cpController);
        }
        else
        {
            char buf[256];
            sprintf_s(buf, 256, "Unsupported transform animation controller type %d !", ctrlClassIndex);
            String msg(buf);
            Debug::LogError(msg);
        }
    }
	void AnimImporter::importPointControllerChannel(UPlasma::PlEmu::PointControllerChannel& uPcc, plPointControllerChannel* pcc)
	{
		String pccName(pcc->getName().c_str());
		uPcc.SetName(pccName);

		plController* ctrl = pcc->getController();
		short ctrlClassIndex = ctrl->ClassIndex();
		if (ctrlClassIndex == kSimplePosController)
		{
			// PotS
			Point3Controller uCtrl;
			uPcc.SetController(uCtrl);
			importController(uCtrl, ((plSimplePosController*)ctrl)->getPosition());
		}
		else if (ctrlClassIndex == kCompoundPosController)
		{
			// PotS
			TriScalarToPoint3Controller uCtrl;
			uPcc.SetController(uCtrl);
			importController(uCtrl, (plCompoundController*)ctrl);
		}
		else if (ctrlClassIndex == kLeafController)
		{
			// MOUL
			Point3Controller uCtrl;
			uPcc.SetController(uCtrl);
			importController(uCtrl, (plLeafController*)ctrl);
		}
		else
		{
			char buf[256];
			sprintf_s(buf, 256, "Unsupported point animation controller type %d !", ctrlClassIndex);
			String msg(buf);
			Debug::LogError(msg);
		}
	}
	void AnimImporter::importScalarControllerChannel(ScalarControllerChannel& uScc, plScalarControllerChannel* scc)
	{
		String sccName(scc->getName().c_str());
		uScc.SetName(sccName);

		ScalarController uCtrl;
		uScc.SetController(uCtrl);

		plController* ctrl = scc->getController();
		short ctrlClassIndex = ctrl->ClassIndex();
		if (ctrlClassIndex == kScalarController || ctrlClassIndex == kLeafController)
		{
			// PotS & MOUL (ScalarController is a LeafController too)
			importController(uCtrl, (plLeafController*)ctrl);
		}
		else
		{
			char buf[256];
			sprintf_s(buf, 256, "Unsupported scalar animation controller type %d !", ctrlClassIndex);
			String msg(buf);
			Debug::LogError(msg);
		}
	}
    
	void AnimImporter::importController(TrsController& uCtrl, plTMController* tmController)
	{
		plPosController* posCtrl = tmController->getPosController();
		if (posCtrl)
		{
			if (posCtrl->getType() == plPosController::kSimple)
			{
	            Point3Controller uSubCtrl;
				uCtrl.SetPosCtrl(uSubCtrl);
				importController(uSubCtrl, ((plSimplePosController*)posCtrl)->getPosition());
			}
			else if (posCtrl->getType() == plPosController::kCompound)
			{
				TriScalarToPoint3Controller uSubCtrl;
				uCtrl.SetPosCtrl(uSubCtrl);
				importController(uSubCtrl, (plCompoundPosController*)posCtrl);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported pos controller %d ! (anim %s, obj %s)", posCtrl->getType(),
	                currentlyProcessedObjectName.c_str(),
	                currentlyProcessedAnimName.c_str());
				String msgcs(msg);
				Debug::LogError(msgcs);
			}
		}

		plRotController* rotCtrl = tmController->getRotController();
		if (rotCtrl)
		{
			if (rotCtrl->getType() == plRotController::kSimple)
			{
	            QuatController uSubCtrl;
				uCtrl.SetRotCtrl(uSubCtrl);
				importController(uSubCtrl, ((plSimpleRotController*)rotCtrl)->getRot());
			}
			else if (rotCtrl->getType() == plRotController::kCompound)
			{
	            TriScalarToQuatController uSubCtrl;
				uCtrl.SetRotCtrl(uSubCtrl);
				importController(uSubCtrl, (plCompoundRotController*)rotCtrl);
			}
			else
			{
				char msg[256];
	            sprintf_s(msg, sizeof msg, "Unsupported rot controller %d ! (anim %s, obj %s)", rotCtrl->getType(),
	                currentlyProcessedObjectName.c_str(),
	                currentlyProcessedAnimName.c_str());
				String msgcs(msg);
				Debug::LogError(msgcs);
			}
		}

		plScaleController* scaCtrl = tmController->getScaleController();
		if (scaCtrl)
		{
			if (scaCtrl->getType() == plScaleController::kSimple)
			{
	            Point3Controller uSubCtrl;
				uCtrl.SetScaCtrl(uSubCtrl);
				importController(uSubCtrl, ((plSimpleScaleController*)scaCtrl)->getValue());
			}
			else
			{
				char msg[256];
	            sprintf_s(msg, sizeof msg, "Unsupported scale controller %d ! (anim %s, obj %s)", scaCtrl->getType(),
	                currentlyProcessedObjectName.c_str(),
	                currentlyProcessedAnimName.c_str());
				String msgcs(msg);
				Debug::LogError(msgcs);
			}
		}
	}

	void AnimImporter::importController(TrsController& uCtrl, plCompoundController* cpController)
	{
		plController* posCtrl = cpController->getXController();
		if (posCtrl)
		{
			short posClassIndex = posCtrl->ClassIndex();
			if (posClassIndex == kLeafController)
			{
	            Point3Controller uSubCtrl;
				uCtrl.SetPosCtrl(uSubCtrl);
				importController(uSubCtrl, (plLeafController*)posCtrl);
			}
			else if (posClassIndex == kCompoundController)
			{
				TriScalarToPoint3Controller uSubCtrl;
				uCtrl.SetPosCtrl(uSubCtrl);
				importController(uSubCtrl, (plCompoundController*)posCtrl);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported pos controller %d ! (anim %s, obj %s)", posClassIndex,
	                currentlyProcessedObjectName.c_str(),
	                currentlyProcessedAnimName.c_str());
				String msgcs(msg);
				Debug::LogError(msgcs);
			}
		}

		plController* rotCtrl = cpController->getYController();
		if (rotCtrl)
		{
			short rotClassIndex = rotCtrl->ClassIndex();
			if (rotClassIndex == kLeafController)
			{
	            QuatController uSubCtrl;
				uCtrl.SetRotCtrl(uSubCtrl);
				importController(uSubCtrl, (plLeafController*)rotCtrl);
			}
			else if (rotClassIndex == kCompoundController)
			{
	            TriScalarToQuatController uSubCtrl;
				uCtrl.SetRotCtrl(uSubCtrl);
				importController(uSubCtrl, (plCompoundController*)rotCtrl);
			}
			else
			{
				char msg[256];
	            sprintf_s(msg, sizeof msg, "Unsupported rot controller %d ! (anim %s, obj %s)", rotClassIndex,
	                currentlyProcessedObjectName.c_str(),
	                currentlyProcessedAnimName.c_str());
				String msgcs(msg);
				Debug::LogError(msgcs);
			}
		}

		plController* scaCtrl = cpController->getZController();
		if (scaCtrl)
		{
			short scaClassIndex = scaCtrl->ClassIndex();
			if (scaClassIndex == kLeafController)
			{
	            Point3Controller uSubCtrl;
				uCtrl.SetScaCtrl(uSubCtrl);
				importController(uSubCtrl, (plLeafController*)scaCtrl);
			}
			else if (scaClassIndex == kCompoundController)
			{
				TriScalarToPoint3Controller uSubCtrl;
				uCtrl.SetScaCtrl(uSubCtrl);
				importController(uSubCtrl, (plCompoundController*)scaCtrl);
			}
			else
			{
				char msg[256];
	            sprintf_s(msg, sizeof msg, "Unsupported scale controller %d ! (anim %s, obj %s)", scaClassIndex,
	                currentlyProcessedObjectName.c_str(),
	                currentlyProcessedAnimName.c_str());
				String msgcs(msg);
				Debug::LogError(msgcs);
			}
		}
	}

	void AnimImporter::importController(UPlasma::PlEmu::Point3Controller& uCtrl, plLeafController* leafctrl)
	{
		if (leafctrl == 0)
		    return;
		const std::vector<hsKeyFrame*>& keyframes = leafctrl->getKeys();
		NativeHelpers::SetLeafControllerNumKeyframe(uCtrl, (int32_t)keyframes.size());
		if (leafctrl->getType() == hsKeyFrame::kPoint3KeyFrame || leafctrl->getType() == hsKeyFrame::kBezPoint3KeyFrame) // pretty much the same
		{
			int kfIndex = 0;
			for (hsKeyFrame* kf : keyframes)
			{
			    Point3Key ukf;
			    hsPoint3Key* kff = (hsPoint3Key*)kf;
				NativeHelpers::SetLeafControllerKeyframe(uCtrl, ukf, kfIndex++);
			    ukf.SetTime(kf->getFrameTime());
			    ukf.SetValue(Vector3(kff->fValue.X, kff->fValue.Y, kff->fValue.Z));
			    ukf.SetInTan(Vector3(kff->fInTan.X, kff->fInTan.Y, kff->fInTan.Z));
			    ukf.SetOutTan(Vector3(kff->fOutTan.X, kff->fOutTan.Y, kff->fOutTan.Z));
			}
		}
		else if (leafctrl->getType() == hsKeyFrame::kScaleKeyFrame || leafctrl->getType() == hsKeyFrame::kBezScaleKeyFrame)
		{
			int kfIndex = 0;
			for (hsKeyFrame* kf : keyframes)
			{
			    Point3Key ukf;
			    hsScaleKey* kff = (hsScaleKey*)kf;
				NativeHelpers::SetLeafControllerKeyframe(uCtrl, ukf, kfIndex++);
			    ukf.SetTime(kf->getFrameTime());
				// TODO - support the scale's quaternion (possibly not required at all ?)
			    ukf.SetValue(Vector3(kff->fS.X, kff->fS.Y, kff->fS.Z));
			    ukf.SetInTan(Vector3(kff->fInTan.X, kff->fInTan.Y, kff->fInTan.Z));
			    ukf.SetOutTan(Vector3(kff->fOutTan.X, kff->fOutTan.Y, kff->fOutTan.Z));
			}
		}
		else
		{
			char msg[256];
			sprintf_s(msg, sizeof msg, "Unsupported keyframe type %d for point3 controller !", leafctrl->getType());
			String msgs(msg);
			Debug::LogError(msgs);
		}
	}

	void AnimImporter::importController(UPlasma::PlEmu::ScalarController& uCtrl, plLeafController* leafctrl)
	{
		if (leafctrl == 0)
		    return;
		const std::vector<hsKeyFrame*>& keyframes = leafctrl->getKeys();
		NativeHelpers::SetLeafControllerNumKeyframe(uCtrl, (int32_t)keyframes.size());
		if (leafctrl->getType() == hsKeyFrame::kScalarKeyFrame || leafctrl->getType() == hsKeyFrame::kBezScalarKeyFrame)
		{
			int kfIndex = 0;
			for (hsKeyFrame* kf : keyframes)
			{
				ScalarKey ukf;
				hsScalarKey* kff = (hsScalarKey*)kf;
				NativeHelpers::SetLeafControllerKeyframe(uCtrl, ukf, kfIndex++);
				ukf.SetTime(kf->getFrameTime());
				ukf.SetValue(kff->fValue);
				ukf.SetInTan(kff->fInTan);
				ukf.SetOutTan(kff->fOutTan);
			}
		}
		else
		{
			char msg[256];
			sprintf_s(msg, sizeof msg, "Unsupported keyframe type %d for scalar controller !", leafctrl->getType());
			String msgs(msg);
			Debug::LogError(msgs);
		}
	}

	void AnimImporter::importController(UPlasma::PlEmu::QuatController& uCtrl, plLeafController* leafctrl)
	{
		if (leafctrl == 0)
		    return;
		const std::vector<hsKeyFrame*>& keyframes = leafctrl->getKeys();
		NativeHelpers::SetLeafControllerNumKeyframe(uCtrl, (int32_t)keyframes.size());
		if (leafctrl->getType() == hsKeyFrame::kQuatKeyFrame)
		{
			int kfIndex = 0;
			for (hsKeyFrame* kf : keyframes)
			{
			    QuatKey ukf;
				hsQuatKey* kff = (hsQuatKey*)kf;
				NativeHelpers::SetLeafControllerKeyframe(uCtrl, ukf, kfIndex++);
			    ukf.SetTime(kf->getFrameTime());
			    ukf.SetValue(Quaternion(kff->fValue.X, kff->fValue.Z, kff->fValue.Y, -kff->fValue.W));
			}
		}
		else if (leafctrl->getType() == hsKeyFrame::kCompressedQuatKeyFrame32)
		{
			int kfIndex = 0;
			for (hsKeyFrame* kf : keyframes)
			{
			    QuatKey ukf;
				hsCompressedQuatKey32* kff = (hsCompressedQuatKey32*)kf;
				NativeHelpers::SetLeafControllerKeyframe(uCtrl, ukf, kfIndex++);
			    ukf.SetTime(kf->getFrameTime());
				hsQuat q(kff->getQuat());
				float x = q.X, y = q.Y, z = q.Z, w = q.W;
				// compressed quat shenanigans...
				if (isnan(x)) x = 0;
				if (isnan(y)) y = 0;
				if (isnan(z)) z = 0;
				if (isnan(w)) w = 0;
			    ukf.SetValue(Quaternion(x, z, y, -w));
			}
		}
		else if (leafctrl->getType() == hsKeyFrame::kCompressedQuatKeyFrame64)
		{
			int kfIndex = 0;
			for (hsKeyFrame* kf : keyframes)
			{
			    QuatKey ukf;
				hsCompressedQuatKey64* kff = (hsCompressedQuatKey64*)kf;
				NativeHelpers::SetLeafControllerKeyframe(uCtrl, ukf, kfIndex++);
			    ukf.SetTime(kf->getFrameTime());
				hsQuat q(kff->getQuat());
				float x = q.X, y = q.Y, z = q.Z, w = q.W;
				// compressed quat shenanigans...
				if (isnan(x)) x = 0;
				if (isnan(y)) y = 0;
				if (isnan(z)) z = 0;
				if (isnan(w)) w = 0;
				ukf.SetValue(Quaternion(x, z, y, -w));
			}
		}
		else
		{
			char msg[256];
			sprintf_s(msg, sizeof msg, "Unsupported keyframe type %d for quat controller !", leafctrl->getType());
			String msgs(msg);
			Debug::LogError(msgs);
		}
	}

	void AnimImporter::importController(UPlasma::PlEmu::TriScalarToPoint3Controller& uCtrl, plCompoundPosController* cpctrl)
	{
		if (cpctrl == 0)
		    return;

		plScalarController* x = cpctrl->getX();
		plScalarController* y = cpctrl->getY();
		plScalarController* z = cpctrl->getZ();
		if (x != 0)
		{
			ScalarController leafControllerX;
			uCtrl.SetX(leafControllerX);
			importController(leafControllerX, x);
		}
		if (y != 0)
		{
			ScalarController leafControllerY;
			uCtrl.SetY(leafControllerY);
			importController(leafControllerY, y);
		}
		if (z != 0)
		{
			ScalarController leafControllerZ;
			uCtrl.SetZ(leafControllerZ);
			importController(leafControllerZ, z);
		}
	}

	void AnimImporter::importController(UPlasma::PlEmu::TriScalarToPoint3Controller& uCtrl, plCompoundController* cpctrl)
	{
		if (cpctrl == 0)
		    return;

		plController* x = cpctrl->getXController();
		plController* y = cpctrl->getYController();
		plController* z = cpctrl->getZController();
		if (x != 0)
		{
			if (x->ClassIndex() == kScalarController || x->ClassIndex() == kLeafController) // both the same, more or less.
			{
				ScalarController leafControllerX;
				uCtrl.SetX(leafControllerX);
				importController(leafControllerX, (plLeafController*)x);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported compound controller sub controller x type %d !", x->ClassIndex());
				String msgs(msg);
				Debug::LogError(msgs);
			}
		}
		if (y != 0)
		{
			if (y->ClassIndex() == kScalarController || y->ClassIndex() == kLeafController) // both the same, more or less.
			{
				ScalarController leafControllerY;
				uCtrl.SetY(leafControllerY);
				importController(leafControllerY, (plLeafController*)y);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported compound controller sub controller y type %d !", y->ClassIndex());
				String msgs(msg);
				Debug::LogError(msgs);
			}
		}
		if (z != 0)
		{
			if (z->ClassIndex() == kScalarController || z->ClassIndex() == kLeafController) // both the same, more or less.
			{
				ScalarController leafControllerZ;
				uCtrl.SetZ(leafControllerZ);
				importController(leafControllerZ, (plLeafController*)z);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported compound controller sub controller z type %d !", z->ClassIndex());
				String msgs(msg);
				Debug::LogError(msgs);
			}
		}
	}

	void AnimImporter::importController(UPlasma::PlEmu::TriScalarToQuatController& uCtrl, plCompoundRotController* cpctrl)
	{
		if (cpctrl == 0)
			return;

		plScalarController* x = cpctrl->getX();
		plScalarController* y = cpctrl->getY();
		plScalarController* z = cpctrl->getZ();
		if (x != 0)
		{
			ScalarController leafControllerX;
			uCtrl.SetX(leafControllerX);
			importController(leafControllerX, x);
		}
		if (y != 0)
		{
			ScalarController leafControllerY;
			uCtrl.SetY(leafControllerY);
			importController(leafControllerY, y);
		}
		if (z != 0)
		{
			ScalarController leafControllerZ;
			uCtrl.SetZ(leafControllerZ);
			importController(leafControllerZ, z);
		}
	}

	void AnimImporter::importController(UPlasma::PlEmu::TriScalarToQuatController& uCtrl, plCompoundController* cpctrl)
	{
		if (cpctrl == 0)
			return;

		plController* x = cpctrl->getXController();
		plController* y = cpctrl->getYController();
		plController* z = cpctrl->getZController();
		if (x != 0)
		{
			if (x->ClassIndex() == kLeafController)
			{
				ScalarController leafControllerX;
				uCtrl.SetX(leafControllerX);
				importController(leafControllerX, (plLeafController*)x);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported compound controller sub quat controller type %d !", x->ClassIndex());
				String msgs(msg);
				Debug::LogError(msgs);
			}
		}
		if (y != 0)
		{
			if (y->ClassIndex() == kLeafController)
			{
				ScalarController leafControllerY;
				uCtrl.SetY(leafControllerY);
				importController(leafControllerY, (plLeafController*)y);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported compound controller sub quat controller type %d !", y->ClassIndex());
				String msgs(msg);
				Debug::LogError(msgs);
			}
		}
		if (z != 0)
		{
			if (z->ClassIndex() == kLeafController)
			{
				ScalarController leafControllerZ;
				uCtrl.SetZ(leafControllerZ);
				importController(leafControllerZ, (plLeafController*)z);
			}
			else
			{
				char msg[256];
				sprintf_s(msg, sizeof msg, "Unsupported compound controller sub quat controller type %d !", z->ClassIndex());
				String msgs(msg);
				Debug::LogError(msgs);
			}
		}
	}
}
