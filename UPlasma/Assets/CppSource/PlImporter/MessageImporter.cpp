/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "MessageImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

PlMessage PlImporter::MessageImporter::import(plMessage* msg, PlasmaVer version)
{
	short msgType = msg->ClassIndex();
	if (msgType == pdClassType::kAnimCmdMsg)
	{
		PlAnimCmdMsg uMsg;
		importGeneric(msg, uMsg, version);
		importMsgWithCallbacks(msg, uMsg, version);
		plAnimCmdMsg* pMsg = (plAnimCmdMsg*)msg;
		String animName(pMsg->getAnimName().c_str());
		String loopName(pMsg->getLoopName().c_str());
		uMsg.SetAnimName(animName);
		uMsg.SetLoopName(loopName);
		uMsg.SetBegin(pMsg->getBegin());
		uMsg.SetEnd(pMsg->getEnd());
		uMsg.SetLoopBegin(pMsg->getLoopBegin());
		uMsg.SetLoopEnd(pMsg->getLoopEnd());
		uMsg.SetSpeed(pMsg->getSpeed());
		uMsg.SetSpeedChangeRate(pMsg->getSpeedChangeRate());
		uMsg.SetTime(pMsg->getTime());

		Collections::Generic::List_1<Int32> command;
		for (int i = 0; i < plAnimCmdMsg::kNumCmds; i++)
			if (pMsg->getCmd(i))
				command.Add(i);
		uMsg.SetCommandsInt(command);
		return uMsg;
	}
	else if (msgType == pdClassType::kArmatureEffectStateMsg)
	{
		PlArmatureEffectStateMsg uMsg;
		importGeneric(msg, uMsg, version);
		plArmatureEffectStateMsg* pMsg = (plArmatureEffectStateMsg*)msg;
		uMsg.SetAdd(pMsg->getAddSurface());
		uMsg.SetSurfaceInt(pMsg->getSurface());
		return uMsg;
	}
	else if (msgType == pdClassType::kEventCallbackMsg)
	{
		PlEventCallbackMsg uMsg;
		importGeneric(msg, uMsg, version);
		plEventCallbackMsg* pMsg = (plEventCallbackMsg*)msg;
		uMsg.SetEventTime(pMsg->getEventTime());
		uMsg.SetEventIdInt(pMsg->getEvent());
		uMsg.SetIndex(pMsg->getIndex());
		uMsg.SetRepeats(pMsg->getRepeats());
		uMsg.SetUser(pMsg->getUser());
		return uMsg;
	}
	else if (msgType == pdClassType::kExcludeRegionMsg)
	{
		PlExcludeRegionMsg uMsg;
		importGeneric(msg, uMsg, version);
		plExcludeRegionMsg* pMsg = (plExcludeRegionMsg*)msg;
		uMsg.SetCommandInt(pMsg->getCmd());
		uMsg.SetSynchFlags(pMsg->getSynchFlags());
		return uMsg;
	}
	else if (msgType == pdClassType::kLinkToAgeMsg)
	{
		PlLinkToAgeMsg uMsg;
		importGeneric(msg, uMsg, version);
		plLinkToAgeMsg* pMsg = (plLinkToAgeMsg*)msg;
		plAgeLinkStruct& als(pMsg->getAgeLink());
		plAgeLinkEffects& alfx(pMsg->getLinkEffects());

		PlAgeLinkStruct uAgeLink;
		PlAgeLinkEffects uLinkEffects;

		uAgeLink.SetFlagsInt(
			(als.hasAgeInfo() ? plAgeLinkStruct::kHasAgeInfo : 0) |
			(als.hasLinkingRules() ? plAgeLinkStruct::kHasLinkingRules : 0) |
			(als.hasAmCCR() ? plAgeLinkStruct::kHasAmCCR : 0) |
			(als.hasSpawnPoint() ? plAgeLinkStruct::kHasSpawnPt : 0) |
			(als.hasParentAgeFilename() ? plAgeLinkStruct::kHasParentAgeFilename : 0)
		);
		uAgeLink.SetLinkingRulesInt(als.getLinkingRules());
		uAgeLink.SetAmCCR(als.getAmCCR());
		String parentAge(als.getParentAgeFilename().c_str());
		uAgeLink.SetParentAgeFilename(parentAge);

		PlAgeInfoStruct uInfo;
		plAgeInfoStruct& info(als.getAgeInfo());
		String ageDesc(info.getAgeDescription().c_str());
		String ageFilename(info.getAgeFilename().c_str());
		String ageInstanceName(info.getAgeInstanceName().c_str());
		String ageUserDefinedName(info.getAgeUserDefinedName().c_str());
		/*PlUuid uUuid;
		const plUuid uuid(info.getAgeInstanceGuid());*/
		//uUuid.SetData1(uuid.data);
		uInfo.SetAgeDescription(ageDesc);
		uInfo.SetAgeFilename(ageFilename);
		//uInfo.SetAgeInstanceGuid();
		uInfo.SetAgeInstanceName(ageInstanceName);
		uInfo.SetAgeLanguage(info.getAgeLanguage());
		uInfo.SetAgeSequenceNumber(info.getAgeSequenceNumber());
		uInfo.SetAgeUserDefinedName(ageUserDefinedName);
		uInfo.SetFlagsInt(
			(info.hasAgeFilename() ? plAgeInfoStruct::kHasAgeFilename : 0) |
			(info.hasAgeInstanceName() ? plAgeInfoStruct::kHasAgeInstanceName : 0) |
			(info.hasAgeInstanceGuid() ? plAgeInfoStruct::kHasAgeInstanceGuid : 0) |
			(info.hasAgeUserDefiniedName() ? plAgeInfoStruct::kHasAgeUserDefinedName : 0) |
			(info.hasAgeDescription() ? plAgeInfoStruct::kHasAgeDescription : 0) |
			(info.hasAgeSequenceNumber() ? plAgeInfoStruct::kHasAgeSequenceNumber : 0) |
			(info.hasAgeLanguage() ? plAgeInfoStruct::kHasAgeLanguage : 0)
		);

		uAgeLink.SetAgeInfo(uInfo);

		plSpawnPointInfo& spawnPoint(als.getSpawnPoint());
		PlSpawnPointInfo uSpawnPoint;
		String camStack(spawnPoint.getCameraStack().c_str());
		String spawnPointCs(spawnPoint.getSpawnPt().c_str());
		String title(spawnPoint.getTitle().c_str());
		uSpawnPoint.SetCameraStack(camStack);
		uSpawnPoint.SetSpawnPt(spawnPointCs);
		uSpawnPoint.SetTitle(title);
		uAgeLink.SetSpawnPoint(uSpawnPoint);

		String lian(alfx.getLinkInAnimName().c_str());
		uLinkEffects.SetLinkInAnimName(lian);
		uLinkEffects.SetBool1(alfx.getBool1());
		uLinkEffects.SetBool2(alfx.getBool2());
		uLinkEffects.SetBool3(alfx.getBool3());
		uLinkEffects.SetBool4(alfx.getBool4());

		uMsg.SetAgeLink(uAgeLink);
		uMsg.SetLinkEffects(uLinkEffects);
		return uMsg;
	}
	else if (msgType == pdClassType::kNotifyMsg)
	{
		PlNotifyMsg uNotify;
		importGeneric(msg, uNotify, version);
		plNotifyMsg* nMsg = (plNotifyMsg*)msg;
		uNotify.SetNotifyId(nMsg->getID());
		uNotify.SetNotifyState(nMsg->getState());
		uNotify.SetNotifyTypeInt(nMsg->getType());
		return uNotify;
	}
	else if (msgType == pdClassType::kOneShotMsg)
	{
		PlOneShotMsg uMsg;
		importGeneric(msg, uMsg, version);
		plOneShotMsg* pMsg = (plOneShotMsg*)msg;
		System::Collections::Generic::List_1<PlOneShotCallback> callbacks;

		for (plOneShotCallbacks::plOneShotCallback callback : pMsg->getCallbacks().getCallbacks())
		{
			PlOneShotCallback uCallback;
			String marker(callback.fMarker.c_str());
			uCallback.SetMarker(marker);
			uCallback.SetUser(callback.fUser);
			IPlMessageable receiver = NativeHelpers::CastObjTo<IPlMessageable>(ImporterUtils::keyToObj_Get(callback.fReceiver));
			uCallback.SetReceiver(receiver);
		}

		uMsg.SetCallbacks(callbacks);
		return uMsg;
	}
	else if (msgType == pdClassType::kSoundMsg)
	{
		PlSoundMsg uMsg;
		importGeneric(msg, uMsg, version);
		plSoundMsg* pMsg = (plSoundMsg*)msg;
		uMsg.SetBegin((float)pMsg->getBegin());
		uMsg.SetEnd((float)pMsg->getEnd());
		uMsg.SetLoop(pMsg->getLoop());
		uMsg.SetPlaying(pMsg->getPlaying());
		uMsg.SetSpeed(pMsg->getSpeed());
		uMsg.SetTime((float)pMsg->getTime());
		uMsg.SetIndex(pMsg->getIndex());
		uMsg.SetRepeats(pMsg->getRepeats());
		uMsg.SetNameStr(pMsg->getNameStr());
		uMsg.SetVolume(pMsg->getVolume());
		uMsg.SetFadeTypeInt(pMsg->getFadeType());
		Collections::Generic::List_1<Int32> command;
		for (int i = 0; i < 32; i++)
			if (pMsg->getCmd(i))
				command.Add(i);
		uMsg.SetCommandsInt(command);
		return uMsg;
	}
	//else if (msgType == pdClassType::kCameraMsg)
	//{
	//	// TODO
	//	plCameraMsg* pMsg = (plCameraMsg*)msg;
	//}
	else if (msgType == pdClassType::kTimerCallbackMsg)
	{
		plTimerCallbackMsg* pMsg = (plTimerCallbackMsg*)msg;
        PlTimerCallbackMsg uMsg;
        importGeneric(msg, uMsg, version);
        uMsg.SetId(pMsg->getID());
        uMsg.SetTime(pMsg->getTime());
	}
	else if (msgType == pdClassType::kEnableMsg)
	{
		plEnableMsg* pMsg = (plEnableMsg*)msg;
        PlEnableMsg uMsg;
        importGeneric(msg, uMsg, version);
        Collections::Generic::List_1<Int32> command;
        hsBitVector& plCommands = pMsg->getCmd();
        for (int i = 0; i < 32; i++)
            if (plCommands.get(i))
                command.Add(i);
        uMsg.SetCommandsInt(command);
        Collections::Generic::List_1<Int32> types;
        hsBitVector& plTypes = pMsg->getTypes();
        for (int i = 0; i < 32; i++)
            if (plTypes.get(i))
                types.Add(i);
        uMsg.SetTypesInt(types);
	}
	else if (msgType == pdClassType::kSimSuppressMsg
	//	|| msgType == pdClassType::kAvBrainCoop // yeah, that DOES happen... wrong id in correspondance table ?
    )
	{
		ExtendedSimSuppressMsg* pMsg = (ExtendedSimSuppressMsg*)msg;
        PlSimSuppressMsg uMsg;
		importGeneric(msg, uMsg, version);
        uMsg.SetSuppress(pMsg->getSuppress());
	}
	else if (msgType == pdClassType::kCameraMsg)
	{
		PlCameraMsg uMsg;
		importGeneric(msg, uMsg, version);
		plCameraMsg* pMsg = (plCameraMsg*)msg;
		plCameraConfig& pConfig = pMsg->getConfig();

		plKey newCam = pMsg->getNewCam();
		if (newCam.Exists() && newCam.isLoaded())
		{
			// for some very weird reason (c), Plasma references the cam's object instead of the cam itself :shrug:
			PlCameraModifier uNewCam = NativeHelpers::CastObjTo<PlSceneObject>(ImporterUtils::keyToObj_Get(newCam))
				.GetGameObject().GetComponent<PlCameraModifier>();
			uMsg.SetNewCam(uNewCam);
		}
		plKey triggerer = pMsg->getTriggerer();
		if (triggerer.Exists() && triggerer.isLoaded())
		{
			UnityEngine::Component uTriggerer = NativeHelpers::CastObjTo<UnityEngine::Component>(ImporterUtils::keyToObj_Get(triggerer));
			uMsg.SetTriggerer(uTriggerer);
		}

		uMsg.SetTransTime((float)pMsg->getTransTime());
		uMsg.SetActivated(pMsg->getActivated());
		PlCameraConfig camConfig;
		uMsg.SetConfig(camConfig);
		hsVector3 offset = pConfig.getOffset() * ImporterUtils::importScale;
		camConfig.SetOffset(Vector3(offset.X, offset.Z, offset.Y));
		camConfig.SetAccel(pConfig.getAccel());
		camConfig.SetDecel(pConfig.getDecel());
		camConfig.SetVel(pConfig.getVel());
		camConfig.SetFPAccel(pConfig.getFPAccel());
		camConfig.SetFPDecel(pConfig.getFPDecel());
		camConfig.SetFPVel(pConfig.getFPVel());
		camConfig.SetFOVh(pConfig.getFOVh());
		camConfig.SetFOVw(pConfig.getFOVw());
		camConfig.SetWorldspace(pConfig.getWorldspace());
	}
	else
	{
		char msgc[256];
		sprintf_s(msgc, sizeof msgc, "Unknown message type %d", msgType);
		String msg(msgc);
		Debug::LogError(msg);
	}

	return 0;
}

void PlImporter::MessageImporter::importGeneric(plMessage * msg, PlMessage uMsg, PlasmaVer version)
{
	uMsg.SetCastFlagsInt(msg->getBCastFlags());

	plKey sender = msg->getSender();
	if (sender.Exists() && sender.isLoaded())
	{
		UnityEngine::Component uSender = NativeHelpers::CastObjTo<UnityEngine::Component>(ImporterUtils::keyToObj_Get(sender));
		uMsg.SetSender(uSender);
	}

	System::Collections::Generic::List_1<IPlMessageable> uReceivers;

	for (plKey receiver : msg->getReceivers())
	{
		UnityEngine::Object uobj(ImporterUtils::keyToObj_Get(receiver));
		/*char dMsgc[256];
		sprintf_s(dMsgc, sizeof dMsgc, "receiver %s %d", receiver->getName().c_str(), receiver->getType());
		String dMsg(dMsgc);
		Debug::Log(dMsg);
		Debug::Log(uobj);*/
		if (uobj.Handle)
			uReceivers.Add(NativeHelpers::CastObjTo<IPlMessageable>(uobj));
		else
		{
			char errorMsgc[256];
			sprintf_s(errorMsgc, sizeof errorMsgc, "Couldn't find receiver %s of type %d", receiver->getName().c_str(), receiver->getType());
			String errorMsg(errorMsgc);
			Debug::LogError(errorMsg);
		}
	}
	uMsg.SetReceivers(uReceivers);
}

void PlImporter::MessageImporter::importMsgWithCallbacks(plMessage * msg, PlMessageWithCallbacks uMsg, PlasmaVer version)
{
	plMessageWithCallbacks* msgc = (plMessageWithCallbacks*)msg;
	System::Collections::Generic::List_1<PlMessage> uCallbacks;
	for (plMessage* callback : msgc->getCallbacks())
	{
		PlMessage uCallback(import(callback, version));
		uCallbacks.Add(uCallback);
	}
	uMsg.SetCallbacks(uCallbacks);
}
