/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "ComponentImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

void PlImporter::ComponentImporter::setupComponents(plLocation location, PlasmaVer version, AssetsLifeBinder & assetsBinder)
{
	SuckyMap<plKey, int> prpToUnityObj;
	ImporterUtils::keyToObj_GetAllIndicesFromPage(prpToUnityObj, location);

	for (plKey objKey : prpToUnityObj.keys)
	{
		short compType = objKey->getType();
		if (compType == pdClassType::kViewFaceModifier)
		{
			PlViewFaceModifier uViewFace(NativeHelpers::CastObjTo<PlViewFaceModifier>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plViewFaceModifier* viewFace = plViewFaceModifier::Convert(objKey->getObj());
			Collections::Generic::List_1<Int32> flags;
			for (int i = 0; i < 32; i++)
				if (viewFace->getFlag(i))
					flags.Add(i);
			uViewFace.SetFlags(flags);
		}
		else if (compType == pdClassType::kFadeOpacityMod)
		{
			PlFadeOpacityMod uFadeOpac(NativeHelpers::CastObjTo<PlFadeOpacityMod>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plFadeOpacityMod* fadeOpac = plFadeOpacityMod::Convert(objKey->getObj());
			if (fadeOpac->getFlag(plFadeOpacityMod::kBoundsCenter)) // fuck this...
				uFadeOpac.SetFlags(1);
			else
				uFadeOpac.SetFlags(0);
			uFadeOpac.SetFadeDown(fadeOpac->getFadeDown());
			uFadeOpac.SetFadeUp(fadeOpac->getFadeUp());
		}
		else if (compType == pdClassType::kLogicModifier)
		{
			PlLogicModifier uLogicMod(NativeHelpers::CastObjTo<PlLogicModifier>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plLogicModifier* logicMod = plLogicModifier::Convert(objKey->getObj());
			uLogicMod.SetCursorInt(logicMod->getCursor());
			PlMessage msg(messageImporter.import(logicMod->getNotify(), version));
			uLogicMod.SetMessage(msg);

			System::Collections::Generic::List_1<PlConditionalObject> conditions;
			for (plKey key : logicMod->getConditions())
			{
				UnityEngine::Object uObj = ImporterUtils::keyToObj_Get(key);
				conditions.Add(NativeHelpers::CastObjTo<PlConditionalObject>(uObj));
			}
			uLogicMod.SetConditions(conditions);
			uLogicMod.UpdateConditions();
		}

		// ------ DETECTORS ------
		else if (compType == pdClassType::kPickingDetector)
		{
			PlPickingDetector uPickingDetect(NativeHelpers::CastObjTo<PlPickingDetector>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plPickingDetector* pickingDetect = plPickingDetector::Convert(objKey->getObj());

			System::Collections::Generic::List_1<IPlMessageable> receivers;
			for (plKey receiver : pickingDetect->getReceivers())
			{
				UnityEngine::Object uObj = ImporterUtils::keyToObj_Get(receiver);
				receivers.Add(NativeHelpers::CastObjTo<IPlMessageable>(uObj));
			}
			uPickingDetect.SetReceivers(receivers);
		}
		else if (compType == pdClassType::kObjectInVolumeDetector)
		{
			PlObjectInVolumeDetector uObjIVD(NativeHelpers::CastObjTo<PlObjectInVolumeDetector>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plObjectInVolumeDetector* objIVD = plObjectInVolumeDetector::Convert(objKey->getObj());

			System::Collections::Generic::List_1<IPlMessageable> receivers;
			for (plKey receiver : objIVD->getReceivers())
			{
				UnityEngine::Object uObj = ImporterUtils::keyToObj_Get(receiver);
				receivers.Add(NativeHelpers::CastObjTo<IPlMessageable>(uObj));
			}
			uObjIVD.SetReceivers(receivers);
			uObjIVD.SetCollisionTypeInt(objIVD->getType());
		}

		// ------ CONDITIONAL OBJECTS ------
		else if (compType == pdClassType::kActivatorConditionalObject)
		{
			PlActivatorConditionalObject uActivatorCond(NativeHelpers::CastObjTo<PlActivatorConditionalObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plActivatorConditionalObject* activatorCond = plActivatorConditionalObject::Convert(objKey->getObj());
			uActivatorCond.SetSatisfied(activatorCond->getSatisfied());
			uActivatorCond.SetToggle(activatorCond->getToggle());

			System::Collections::Generic::List_1<PlDetector> activators;
			for (plKey activator : activatorCond->getActivators())
			{
				UnityEngine::Object uObj = ImporterUtils::keyToObj_Get(activator);
				activators.Add(NativeHelpers::CastObjTo<PlDetector>(uObj));
			}
			uActivatorCond.SetActivators(activators);
		}
		else if (compType == pdClassType::kFacingConditionalObject)
		{
			PlFacingConditionalObject uFacingCond(NativeHelpers::CastObjTo<PlFacingConditionalObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plFacingConditionalObject* facingCond = plFacingConditionalObject::Convert(objKey->getObj());
			uFacingCond.SetSatisfied(facingCond->getSatisfied());
			uFacingCond.SetToggle(facingCond->getToggle());

			uFacingCond.SetTolerance(facingCond->getTolerance());
			uFacingCond.SetDirectional(facingCond->getDirectional());
		}
		else if (compType == pdClassType::kObjectInBoxConditionalObject)
		{
			PlObjectInBoxConditionalObject uOIBCond(NativeHelpers::CastObjTo<PlObjectInBoxConditionalObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plObjectInBoxConditionalObject* oibCond = plObjectInBoxConditionalObject::Convert(objKey->getObj());
			uOIBCond.SetSatisfied(oibCond->getSatisfied());
			uOIBCond.SetToggle(oibCond->getToggle());
		}
		else if (compType == pdClassType::kVolumeSensorConditionalObject)
		{
			PlVolumeSensorConditionalObject uVolSensorCond(NativeHelpers::CastObjTo<PlVolumeSensorConditionalObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plVolumeSensorConditionalObject* volSensorCond = plVolumeSensorConditionalObject::Convert(objKey->getObj());
			uVolSensorCond.SetSatisfied(volSensorCond->getSatisfied());
			uVolSensorCond.SetToggle(volSensorCond->getToggle());

			uVolSensorCond.SetTrigNum(volSensorCond->getTrigNum());
			uVolSensorCond.SetTypeInt(volSensorCond->getType());
			uVolSensorCond.SetFirst(volSensorCond->getFirst());
		}

		// ------ OTHER LOGIC ------
		else if (compType == pdClassType::kResponderModifier)
		{
			PlResponder uResponder(NativeHelpers::CastObjTo<PlResponder>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plResponderModifier* responder = plResponderModifier::Convert(objKey->getObj());

			uResponder.SetCurState(responder->getCurState());
			uResponder.SetEnabled(responder->isEnabled());
			uResponder.SetFlagsInt(responder->getFlags());

			Array1<PlResponderState> uStates((int32_t)responder->getStates().size());

			int stateId = 0;
			for (plResponderModifier::plResponderState* respState : responder->getStates())
			{
				PlResponderState uState;
				uState.SetNumCallbacks(respState->fNumCallbacks);
				uState.SetSwitchToState(respState->fSwitchToState);

				Array1<PlResponderCommand> uCommands((int32_t)respState->fCmds.size());

				int cmdId = 0;
				for (plResponderModifier::plResponderCmd* respCmd : respState->fCmds)
				{
					PlResponderCommand uCommand;
					uCommand.SetWaitOn(respCmd->fWaitOn);
					PlMessage cmdMsg(messageImporter.import(respCmd->fMsg, version));
					uCommand.SetMessage(cmdMsg);

					uCommands[cmdId] = uCommand;
					cmdId++;
				}

				uState.SetCommands(uCommands);

				uStates[stateId] = uState;
				stateId++;
			}
			uResponder.SetStates(uStates);
		}
		else if (compType == pdClassType::kPythonFileMod)
		{
			// importing Python scripts *might* be important to our Plamza emulator...
			plPythonFileMod* pythonfm = plPythonFileMod::Convert(objKey->getObj());

            // we have several of those sharing the same plKey because of the stupid way Plasma works...
            for (int pythIndex = 0; pythIndex < ImporterUtils::keyToObj_GetSharedKeyCount(objKey); pythIndex++)
            {
                PlPythonFileMod uPythonfm(NativeHelpers::CastObjTo<PlPythonFileMod>(ImporterUtils::keyToObj_GetWithSharedKey(objKey, pythIndex)));

                String fname(pythonfm->getFilename().c_str());
                uPythonfm.SetMainClassName(fname);
                Collections::Generic::List_1<System::Int32> paramIds(uPythonfm.GetParameterIds());
                Collections::Generic::List_1<System::Object> params(uPythonfm.GetParameters());

                for (plPythonParameter pyParam : pythonfm->getParameters())
                {
                    paramIds.Add(pyParam.fID);
                    switch (pyParam.fValueType)
                    {
                    case plPythonParameter::kInt:
                    {
                        System::Int32 v(pyParam.fIntValue);
						// ugh... That's ugly, but the cpp lib still has issues with counting refs to ints...
						// At Least, It Works (c)
						NativeHelpers::AddIntToObjList(params, v);
                        break;
                    }
                    case plPythonParameter::kFloat:
                    {
                        Single csValue = pyParam.fFloatValue;
                        params.Add((System::Object)csValue);
                        break;
                    }
                    case plPythonParameter::kBoolean:
                    {
						// bogus bool ref count when used in lists, use native helper
						NativeHelpers::AddBoolToObjList(params, pyParam.fBoolValue);
                        break;
                    }
                    case plPythonParameter::kString:
                    {
                        String csValue(pyParam.fStrValue.c_str());
                        params.Add((System::Object)csValue);
                        break;
                    }
                    case plPythonParameter::kSceneObject:
                    {
                        PlSceneObject csValue(
                            NativeHelpers::CastObjTo<PlSceneObject>(
                                ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        params.Add(csValue);
                        break;
                    }
                    case plPythonParameter::kSceneObjectList:
                    {
                        PlSceneObject csValue(
                            NativeHelpers::CastObjTo<PlSceneObject>(
                                ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        params.Add(csValue);
                        break;
                    }
                    case plPythonParameter::kActivator:
                    {
                        short recvType = pyParam.fObjKey->getType();
                        if (recvType == pdClassType::kLogicModifier)
                        {
                            // most common case
                            PlLogicModifier csValue(
                                NativeHelpers::CastObjTo<PlLogicModifier>(
                                    ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                            params.Add(csValue);
                        }
                        else if (recvType == pdClassType::kPythonFileMod)
                        {
                            // does happen sometime
                            PlPythonFileMod csValue(
                                NativeHelpers::CastObjTo<PlPythonFileMod>(
                                    ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                            params.Add(csValue);
                        }
                        //else if (recvType == pdClassType::kAnimEventModifier)
                        //{
                        //	PlAnimEventModifier csValue(
                        //		NativeHelpers::CastObjTo<PlAnimEventModifier>(
                        //			ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        //	params.Add(csValue);
                        //}
                        //else if (recvType == pdClassType::kMultistageBehMod)
                        //{
                        //	PlMultistageBehMod csValue(
                        //		NativeHelpers::CastObjTo<PlMultistageBehMod>(
                        //			ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        //	params.Add(csValue);
                        //}
                        else
                        {
                            char buf[256];
                            sprintf_s(buf, sizeof(buf), "PFM: Activator type %d not implemented", recvType);
                            String errorMsg(buf);
                            Debug::LogError(errorMsg);
                            System::Object obj(0);
                            params.Add(obj);
                        }
                        break;
                    }
                    case plPythonParameter::kResponder:
                    {
                        PlResponder csValue(
                            NativeHelpers::CastObjTo<PlResponder>(
                                ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        params.Add(csValue);
                        break;
                    }
                    /*case plPythonParameter::kDynamicText:
                    break;
                    case plPythonParameter::kGUIDialog:
                    break;
                    //*/
                    case plPythonParameter::kExcludeRegion:
                    {
                        PlExcludeRegionModifier csValue(
                            NativeHelpers::CastObjTo<PlExcludeRegionModifier>(
                                ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        params.Add(csValue);
                        break;
                    }
                    case plPythonParameter::kAnimation:
                    {
                        if (!pyParam.fObjKey.Exists() || !pyParam.fObjKey.isLoaded())
                        {
                            System::Object null(0);
                            params.Add(null);
                        }
                        else if (pyParam.fObjKey->getType() == pdClassType::kMsgForwarder)
                        {
                            PlMessageForwarder csValue(
                                NativeHelpers::CastObjTo<PlMessageForwarder>(
                                    ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                            params.Add(csValue);
                        }
                        else // if (pyParam.fObjKey->getType() == pdClassType::kAGMasterMod)
                        {
                            PlAgMasterMod csValue(
                                NativeHelpers::CastObjTo<PlAgMasterMod>(
                                    ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                            params.Add(csValue);
                        }
                        break;
                    }
                    case plPythonParameter::kAnimationName:
                    {
                        String csValue(pyParam.fStrValue.c_str());
                        params.Add((System::Object)csValue);
                        break;
                    }
                    //case plPythonParameter::kBehavior:
                    //	break;
                    case plPythonParameter::kMaterial:
                    {
                        Material csValue(
                            NativeHelpers::CastObjTo<Material>(
                                ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        params.Add(csValue);
                        break;
                    }
                    /*
                    case plPythonParameter::kGUIPopUpMenu:
                    break;
                    case plPythonParameter::kGUISkin:
                    break;
                    case plPythonParameter::kWaterComponent:
                    break;
                    case plPythonParameter::kSwimCurrentInterface:
                    break;
                    //*/
                    case plPythonParameter::kClusterComponent:
                    {
                        PlClusterGroup csValue(
                            NativeHelpers::CastObjTo<PlClusterGroup>(
                                ImporterUtils::keyToObj_Get(pyParam.fObjKey)));
                        params.Add((System::Object)csValue);
                        break;
                    }
                    /*
                    case plPythonParameter::kMaterialAnimation:
                    break;
                    case plPythonParameter::kGrassShaderComponent:
                    break;*/
                    case plPythonParameter::kGlobalSDLVar:
                    {
                        String csValue(pyParam.fStrValue.c_str());
                        params.Add((System::Object)csValue);
                        break;
                    }
                    /*case plPythonParameter::kSubtitle:
                    break;
                    case plPythonParameter::kBlowerComponent:
                    break;*/
                    default:
                    {
                        // not implemented. Use null.
                        System::Object obj(0);
                        params.Add(obj);
                        break;
                    }
                    }
                }
            }
		}

		// ------ AVATAR ------
		else if (compType == pdClassType::kArmatureLODMod || compType == pdClassType::kArmatureMod)
		{
			PlArmatureMod uArmature(NativeHelpers::CastObjTo<PlArmatureMod>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plArmatureMod* armMod = plArmatureMod::Convert(objKey->getObj());

			ArmaturePropertiesGetter* armGetter = (ArmaturePropertiesGetter*)armMod;
			GameObject uObject(uArmature.GetGameObject());
			Rigidbody rb(uObject.GetComponent<Rigidbody>());
			if (rb.Handle)
				rb.SetFreezeRotation(true);

			// put this object and all children on the plasma characters layer.
			// this ensures correct lighting and allows us to remove collision between players.
			//uObject.SetLayer(ImporterUtils::charLyr);
			Array1<Transform> children(uArmature.GetComponentsInChildren<Transform>());
			for (int i = 0; i < children.GetLength(); i++)
			{
				Transform child(children[i]);
				//child.GetGameObject().SetLayer(ImporterUtils::charLyr);
			}

			String avName(armGetter->getRootName().c_str());
			uArmature.SetRootName(avName);
			uArmature.SetBodyTypeInt(armGetter->getBodyType());

			for (plKey lodKey : armGetter->getMeshKeys())
			{
				if (lodKey != armGetter->getDefaultMesh())
				{
					SuckyMap<plKey, int> prpToIndex;
					ImporterUtils::keyToObj_GetAllIndicesFromPage(prpToIndex, location);
					// this is a sub-lod. Since we're not using it now, just disable it.
					for (plKey objKey2 : prpToUnityObj.keys)
					{
						if (objKey2 == lodKey)
							(NativeHelpers::CastObjTo<PlSceneObject>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey2)))).GetGameObject().SetActive(false);
					}
				}
			}
		}

		// ------ OTHERS ------
		else if (compType == pdClassType::kCameraModifier)
		{
			PlCameraModifier uCam(NativeHelpers::CastObjTo<PlCameraModifier>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plCameraModifier* pCam = plCameraModifier::Convert(objKey->getObj());
			// TODO - setup camera
		}
		else if (compType == pdClassType::kMaintainersMarkerModifier)
		{
			PlMaintainersMarkerModifier uMarker(NativeHelpers::CastObjTo<PlMaintainersMarkerModifier>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plMaintainersMarkerModifier* maintMarker = plMaintainersMarkerModifier::Convert(objKey->getObj());
			uMarker.SetCalibrationLevelInt(maintMarker->getCalibration());
		}
		else if (compType == pdClassType::kOneShotMod)
		{
			PlOneShotMod uObj(NativeHelpers::CastObjTo<PlOneShotMod>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plOneShotMod* plObj = plOneShotMod::Convert(objKey->getObj());

			String animName(plObj->getAnimName().c_str());
			uObj.SetAnimName(animName);
			uObj.SetDrivable(plObj->isDrivable());
			uObj.SetReversable(plObj->isReversable());
			uObj.SetSmartSeek(plObj->getSmartSeek());
			uObj.SetNoSeek(plObj->getNoSeek());
			uObj.SetSeekDuration(plObj->getSeekDuration());
		}
		else if (compType == pdClassType::kSittingModifier)
		{
			PlSittingModifier uObj(NativeHelpers::CastObjTo<PlSittingModifier>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plSittingModifier* plObj = plSittingModifier::Convert(objKey->getObj());

			uObj.SetFlagsInt(plObj->getMiscFlags());
			for (plKey recv : plObj->getNotifyKeys())
			{
				IPlMessageable uRecv(NativeHelpers::CastObjTo<IPlMessageable>(ImporterUtils::keyToObj_Get(recv)));
				uObj.GetNotifies().Add(uRecv);
			}
		}
		else if (compType == pdClassType::kRandomSoundMod)
		{
			PlRandomSoundMod uObj(NativeHelpers::CastObjTo<PlRandomSoundMod>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			RandomSoundPropGetter* plObj = (RandomSoundPropGetter*)plRandomSoundMod::Convert(objKey->getObj());

			uObj.SetState(plObj->getState());
			uObj.SetModeInt(plObj->getMode());
			uObj.SetMinDelay(plObj->getMinDelay());
			uObj.SetMaxDelay(plObj->getMaxDelay());
			for (plRandomSoundModGroup& rsg : plObj->getGroups())
			{
				RandomSoundGroupPropGetter* rsgg = (RandomSoundGroupPropGetter*)&rsg;
				PlRandomSoundModGroup group;
				group.SetGroupedIdx(rsgg->getGroupIdx());
				for (int x : rsgg->getIndices())
					group.GetIndices().Add(x);
				uObj.GetGroups().Add(group);
			}
		}
		else if (compType == pdClassType::kPanicLinkRegion)
		{
			PlPanicLinkRegion uObj(NativeHelpers::CastObjTo<PlPanicLinkRegion>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plPanicLinkRegion* plObj = plPanicLinkRegion::Convert(objKey->getObj());
			uObj.SetPlayLinkOutAnim(plObj->getPlayLinkOutAnim());
			uObj.SetCollisionTypeInt(plObj->getType());
		}
		else if (compType == pdClassType::kExcludeRegionModifier)
		{
			PlExcludeRegionModifier uXRgn(NativeHelpers::CastObjTo<PlExcludeRegionModifier>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plExcludeRegionModifier* xRgn = plExcludeRegionModifier::Convert(objKey->getObj());
			uXRgn.SetSeek(xRgn->getSeek());
			uXRgn.SetSeekTime(xRgn->getSeekTime());

			for (plKey safeKey : xRgn->getSafePoints())
			{
                PlSceneObject uSafePt(NativeHelpers::CastObjTo<PlSceneObject>(ImporterUtils::keyToObj_Get(safeKey)));
				uXRgn.GetSafePoints().Add(uSafePt.GetTransform());
			}
		}
		else if (compType == pdClassType::kWaveSet7)
		{
			PlWaveSet7 uObj(NativeHelpers::CastObjTo<PlWaveSet7>(ImporterUtils::keyToObj_GetObjFromId(prpToUnityObj.get(objKey))));
			plWaveSet7* plObj = plWaveSet7::Convert(objKey->getObj());
			uObj.SetMaxLen(plObj->getMaxLen());
			// TODO - import shores and decals
			//for (plKey shore : plObj->getShores());
			//for (plKey decal : plObj->getDecals());

			plKey envMapKey = plObj->getEnvMap();
			if (envMapKey.Exists() && envMapKey.isLoaded())
				uObj.SetEnvMap(importDynamicEnvMap(envMapKey));

			plFixedWaterState7& state = plObj->getState();
			PlFixedWaterState7 uState = uObj.GetState();

			Vector3 windDir(state.getWindDir().X, state.getWindDir().Y, state.getWindDir().Z);
			Vector3 specVec(state.getSpecVector().X, state.getSpecVector().Y, state.getSpecVector().Z);
			Vector3 waterOffset(state.getWaterOffset().X, state.getWaterOffset().Y, state.getWaterOffset().Z);
			Vector3 maxAtten(state.getMaxAtten().X, state.getMaxAtten().Y, state.getMaxAtten().Z);
			Vector3 minAtten(state.getMinAtten().X, state.getMinAtten().Y, state.getMinAtten().Z);
			Vector3 depthFalloff(state.getDepthFalloff().X, state.getDepthFalloff().Y, state.getDepthFalloff().Z);
			Color shoreTint(state.getShoreTint().r, state.getShoreTint().g, state.getShoreTint().b, state.getShoreTint().a);
			Color maxColor(state.getMaxColor().r, state.getMaxColor().g, state.getMaxColor().b, state.getMaxColor().a);
			Color minColor(state.getMinColor().r, state.getMinColor().g, state.getMinColor().b, state.getMinColor().a);
			Color waterTint(state.getWaterTint().r, state.getWaterTint().g, state.getWaterTint().b, state.getWaterTint().a);
			Color specularTint(state.getSpecularTint().r, state.getSpecularTint().g, state.getSpecularTint().b, state.getSpecularTint().a);
			Vector3 envCenter(state.getEnvCenter().X, state.getEnvCenter().Y, state.getEnvCenter().Z);

			uState.SetWindDir(windDir);
			uState.SetSpecVec(specVec);
			uState.SetWaterOffset(waterOffset);
			uState.SetMaxAtten(maxAtten);
			uState.SetMinAtten(minAtten);
			uState.SetDepthFalloff(depthFalloff);
			uState.SetShoreTint(shoreTint);
			uState.SetMaxColor(maxColor);
			uState.SetMinColor(minColor);
			uState.SetEdgeOpacity(state.getEdgeOpacity());
			uState.SetEdgeRadius(state.getEdgeRadius());
			uState.SetWaterTint(waterTint);
			uState.SetSpecularTint(specularTint);
			uState.SetEnvRefresh(state.getEnvRefresh());
			uState.SetEnvRadius(state.getEnvRadius());
			uState.SetEnvCenter(envCenter);

			WaterState geoState = uState.GetGeoState();
			WaterState texState = uState.GetTexState();
			WaterStateParams stateParams = uState.GetStateParams();
			geoState.SetAmpOverlen(state.getGeoState().fAmpOverLen);
			geoState.SetAngleDev(state.getGeoState().fAngleDev);
			geoState.SetChop(state.getGeoState().fChop);
			geoState.SetMaxLen(state.getGeoState().fMaxLength);
			geoState.SetMinLen(state.getGeoState().fMinLength);
			texState.SetAmpOverlen(state.getTexState().fAmpOverLen);
			texState.SetAngleDev(state.getTexState().fAngleDev);
			texState.SetChop(state.getTexState().fChop);
			texState.SetMaxLen(state.getTexState().fMaxLength);
			texState.SetMinLen(state.getTexState().fMinLength);
			stateParams.SetFingerlength(state.getFingerLength());
			stateParams.SetPeriod(state.getPeriod());
			stateParams.SetRippleScale(state.getRippleScale());
			stateParams.SetWaterHeight(state.getWaterHeight());
			stateParams.SetWispiness(state.getWispiness());
		}
	}
}

UnityEngine::ReflectionProbe PlImporter::ComponentImporter::importDynamicEnvMap(plKey envMapKey)
{
	plDynamicEnvMap* envMap = plDynamicEnvMap::Convert(envMapKey->getObj());
	String probeName(envMapKey->getName().c_str());
	GameObject probeObj(probeName);
	Vector3 position(
		envMap->getPosition().X * ImporterUtils::importScale,
		envMap->getPosition().Z * ImporterUtils::importScale,
		envMap->getPosition().Y * ImporterUtils::importScale);
	probeObj.GetTransform().SetPosition(position);
	ReflectionProbe probe = probeObj.AddComponent<ReflectionProbe>();
	probe.SetMode(Rendering::ReflectionProbeMode::Realtime);
	if (envMap->getRefreshRate())
		probe.SetRefreshMode(Rendering::ReflectionProbeRefreshMode::EveryFrame);
	else
		probe.SetRefreshMode(Rendering::ReflectionProbeRefreshMode::OnAwake);
	probe.SetTimeSlicingMode(Rendering::ReflectionProbeTimeSlicingMode::IndividualFaces);
	probe.SetResolution(envMap->getWidth());
	Color black(0, 0, 0, 0);
	probe.SetBackgroundColor(black);
	probe.SetNearClipPlane(envMap->getHither() * ImporterUtils::importScale);
	probe.SetFarClipPlane(envMap->getYon() * ImporterUtils::importScale);
	// DONT - it's returning an int, which crashes the whole bindings. Duh T_T
    // (new CS ws7 code should fix it)
	//probe.RenderProbe();
	return probe;
}
