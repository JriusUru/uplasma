/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "ResManager/plFactory.h"
#include "PRP/Animation/plController.h"
#include "PRP/Avatar/plAGModifier.h"
#include "PRP/Avatar/plAGMasterMod.h"
#include "PRP/Avatar/plATCAnim.h"
#include "PRP/Avatar/plMatrixChannel.h"
#include "PRP/Avatar/plPointChannel.h"
#include "PRP/Avatar/plScalarChannel.h"
#include "PRP/Animation/plTMController.h"
#pragma warning(pop)
#include "ImporterUtils.h"


namespace PlImporter
{
	/**
	Handles importing animations.
	*/
	struct AnimImporter
	{
		void importAGModifier(UnityEngine::GameObject& gameObject, const plKey& agmKey, PlasmaVer version,
			UPlasma::PlEmu::AssetsLifeBinder& assetsBinder);
		void importAGMasterMod(UnityEngine::GameObject& gameObject, const plKey& agmmKey, PlasmaVer version,
			UPlasma::PlEmu::AssetsLifeBinder& assetsBinder, bool doDisable);

	private:
		UPlasma::PlEmu::IAgApplicator importAnimApplicator(UnityEngine::GameObject& gameObject, plAGApplicator * applicator);
		void importMatrixControllerChannel(UPlasma::PlEmu::TrsControllerChannel& uTrscc, plMatrixControllerChannel* mcc);
		void importPointControllerChannel(UPlasma::PlEmu::PointControllerChannel& uPcc, plPointControllerChannel* pcc);
		void importScalarControllerChannel(UPlasma::PlEmu::ScalarControllerChannel& uScc, plScalarControllerChannel* scc);

		void importController(UPlasma::PlEmu::TrsController& uCtrl, plTMController* tmController);
		void importController(UPlasma::PlEmu::TrsController& uCtrl, plCompoundController* cpController);
		void importController(UPlasma::PlEmu::ScalarController& uCtrl, plLeafController* leafctrl);
		void importController(UPlasma::PlEmu::Point3Controller& uCtrl, plLeafController* leafctrl);
		void importController(UPlasma::PlEmu::QuatController& uCtrl, plLeafController* leafctrl);
		void importController(UPlasma::PlEmu::TriScalarToPoint3Controller& uCtrl, plCompoundPosController* cpctrl);
		void importController(UPlasma::PlEmu::TriScalarToPoint3Controller& uCtrl, plCompoundController* cpctrl);
		void importController(UPlasma::PlEmu::TriScalarToQuatController& uCtrl, plCompoundRotController* cpctrl);
		void importController(UPlasma::PlEmu::TriScalarToQuatController& uCtrl, plCompoundController* cpctrl);

        // ↓ not the cleanest, but useful for debugging
        ST::string currentlyProcessedObjectName;
        ST::string currentlyProcessedAnimName;
	};
}
