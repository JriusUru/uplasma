/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "PRP/Physics/plGenericPhysical.h"
#include "PRP/Object/plSimulationInterface.h"
#pragma warning(pop)
#include "ImporterUtils.h"
#include "LightImporter.h"


namespace PlImporter
{
	struct PhysImporter
	{
		void import(UnityEngine::GameObject& gameObject, const plKey& simKey, bool& dynamic, PlasmaVer version,
			UPlasma::PlEmu::AssetsLifeBinder assetsBinder, bool doDisable);
	};
}
