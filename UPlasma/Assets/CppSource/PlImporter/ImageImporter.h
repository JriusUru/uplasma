/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "ResManager/plFactory.h"
#include "PRP/Surface/plMipmap.h"
#include "PRP/Surface/plCubicEnvironmap.h"
#pragma warning(pop)
#include "ImporterUtils.h"
#include <fstream>


namespace PlImporter
{
	/**
	Handles importing texture images.
	*/
	struct ImageImporter
	{
		void import(UnityEngine::Texture2D& texOUT, const plKey& origTexKey, PlasmaVer version,
			UPlasma::PlEmu::AssetsLifeBinder assetsBinder, bool clampU, bool clampV, bool linear);
		void importCube(UnityEngine::Cubemap& cubeOUT, const plKey& texKey, PlasmaVer version,
			UPlasma::PlEmu::AssetsLifeBinder assetsBinder);

		static void setResManagerForImageSearch(plResManager* resmgr) { rmgr = resmgr; }

	private:
		static plResManager * rmgr;
	};
}
