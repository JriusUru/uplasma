/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "EncryptedTextFileReader.h"

using namespace System;
using namespace UnityEngine;

bool PlImporter::EncryptedTextFile::read(ST::string filename, ST::string& out)
{
	// I *think* FNI files are always encrypted. Some of them are, at least.
	// (code copied from the excellent PlasmaShop)
	if (plEncryptedStream::IsFileEncrypted(filename))
	{
		plEncryptedStream S(PlasmaVer::pvUnknown);
		if (S.open(filename, fmRead, plEncryptedStream::kEncAuto))
		{
			if (S.getEncType() == plEncryptedStream::kEncDroid)
			{
				String msg("Cannot decode encrypted file without the encryption key for now");
				Debug::LogError(msg);
				return false;
			}
			encoding e = detectEncoding(&S);

			size_t dataSize = S.size() - S.pos();
			unsigned char* buf = new unsigned char[dataSize];
			S.read(dataSize, buf);
			size_t trimmedDataSize = dataSize;
			// seems there are some padding bytes appended to the data. Remove them.
			for (; trimmedDataSize >= 0; trimmedDataSize--)
			{
				if (buf[trimmedDataSize - 1] != 0 && buf[trimmedDataSize - 1] != 0xFD)
					break;
			}
			ST::string fniContentT;
			switch (e)
			{
			case utf8:
				fniContentT = ST::string::from_utf8((char*)buf, trimmedDataSize);
				break;
			case utf16:
				fniContentT = ST::string::from_utf16((char16_t*)buf, trimmedDataSize);
				break;
			case utf32:
				fniContentT = ST::string::from_utf32((char32_t*)buf, trimmedDataSize);
				break;
			default:
			case ansi:
				fniContentT = ST::string::from_latin_1((char*)buf, trimmedDataSize);
				break;
			}
			delete[] buf;

			out = fniContentT;
			return true;
		}
		else
		{
			ST::string msgT("Cannot open Plasma document");
			msgT += filename;
			msgT += " !";
			String msg(msgT.c_str());
			Debug::LogError(msg);
			return false;
		}
	}
	else
	{
		hsFileStream S(PlasmaVer::pvUnknown);
		if (S.open(filename, fmRead))
		{
			encoding e = detectEncoding(&S);

			size_t dataSize = S.size() - S.pos();
			unsigned char* buf = new unsigned char[dataSize];
			S.read(dataSize, buf);
			size_t trimmedDataSize = dataSize;
			// seems there are some padding bytes appended to the data. Remove them.
			for (; trimmedDataSize >= 0; trimmedDataSize--)
			{
				if (buf[trimmedDataSize - 1] != 0 && buf[trimmedDataSize - 1] != 0xFD)
					break;
			}
			ST::string fniContentT;
			switch (e)
			{
			case utf8:
				fniContentT = ST::string::from_utf8((char*)buf, trimmedDataSize);
				break;
			case utf16:
				fniContentT = ST::string::from_utf16((char16_t*)buf, trimmedDataSize);
				break;
			case utf32:
				fniContentT = ST::string::from_utf32((char32_t*)buf, trimmedDataSize);
				break;
			default:
			case ansi:
				fniContentT = ST::string::from_latin_1((char*)buf, trimmedDataSize);
				break;
			}
			delete[] buf;

			out = fniContentT;
			return true;
		}
		else
		{
			ST::string msgT("Cannot open FNI file ");
			msgT += filename;
			msgT += " !";
			String msg(msgT.c_str());
			Debug::LogError(msg);
			return false;
		}
	}
	return false;
}

PlImporter::EncryptedTextFile::encoding PlImporter::EncryptedTextFile::detectEncoding(hsStream * S)
{
	unsigned char markerbuf[4];
	encoding mode = ansi;
	if (S->size() >= 2) {
		S->read(2, markerbuf);
		if (markerbuf[0] == 0xFF && markerbuf[1] == 0xFE) {
			if (S->size() >= 4) {
				S->read(2, markerbuf + 2);
				if (markerbuf[2] == 0 && markerbuf[3] == 0) {
					// UTF32
					mode = utf32;
				}
				else {
					// some other UTF16 char, go back to it
					S->seek(2);
					mode = utf16;
				}
			}
			else {
				// Not big enough for UTF32, must be UTF16
				mode = utf16;
			}
		}
		else if (markerbuf[0] == 0xEF && markerbuf[1] == 0xBB) {
			if (S->size() >= 3) {
				S->read(1, markerbuf + 2);
				if (markerbuf[2] == 0xBF) {
					// UTF8
					mode = utf8;
				}
				else {
					// Random ANSI junk
					S->seek(0);
				}
			}
			else {
				// Random ANSI junk
				S->seek(0);
			}
		}
		else {
			// Normal ANSI file
			S->seek(0);
		}
	}
	return mode;
}
