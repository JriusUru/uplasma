/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "Stream/hsStream.h"
#pragma warning(pop)
#include "ImporterUtils.h"

#include <fstream>
#include <sstream>

namespace PlImporter
{
	struct PythonPAKReader
	{
		struct FileBlob {
			struct _data {
				const uint8_t* fData;
				size_t fSize;
				unsigned int fRefs;
			} *fData;

			FileBlob() : fData(NULL) { }

			FileBlob(const uint8_t* data, size_t size)
			{
				fData = new _data;
				fData->fData = data;
				fData->fSize = size;
				fData->fRefs = 1;
			}

			~FileBlob()
			{
				if (fData != NULL && (--fData->fRefs == 0))
					delete fData;
			}

			FileBlob(const FileBlob& copy) : fData(copy.fData)
			{
				if (fData != NULL)
					++fData->fRefs;
			}

			FileBlob& operator=(const FileBlob& copy)
			{
				if (fData != NULL && (--fData->fRefs == 0))
					delete fData;
				fData = copy.fData;
				if (fData != NULL)
					++fData->fRefs;
				return *this;
			}

			const uint8_t* getData() const { return fData ? fData->fData : NULL; }
			size_t getSize() const { return fData ? fData->fSize : 0; }
		};

		struct FileEntry {
			ST::string fName;
			uint32_t fOffset;
			FileBlob fData;
			//plFont fFontData;

			FileEntry() { }
		};

		enum PackageType {
			kPythonPak, kCursorsDat, kFontsPfp,
			kMyst5Arc = 0xCBBCF00D,
		};

		PackageType fType;
		uint32_t fUnknown;
		std::vector<FileEntry> fEntries;

		PythonPAKReader(ST::string n) : fType(kPythonPak), fUnknown(1), name(n) {}

		bool read(hsStream* S);
		bool read(ST::string pakFilename);
		std::vector<ST::string> getFileNames();
		ST::string decompileScript(size_t scriptIndex) { return uncompressData(fEntries[scriptIndex]); }
		ST::string getName() { return name; }
		void setPythonCachePath(ST::string path) { pythonCachePath = path; }

		static void setPythonDecompilerPath(ST::string path) { pythonDecompilerPath = path; }

	private:
		ST::string uncompressData(const FileEntry& entry);
		ST::string name;
		ST::string pythonCachePath;
		static ST::string pythonDecompilerPath;
	};
}
