/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "ResManager/plFactory.h"
#include "PRP/Message/plMessage.h"
#include "PRP/Message/plNotifyMsg.h"
#include "PRP/Message/plAnimCmdMsg.h"
#include "PRP/Message/plLinkToAgeMsg.h"
#include "PRP/Message/plArmatureEffectMsg.h"
#include "PRP/Message/plEventCallbackMsg.h"
#include "PRP/Message/plExcludeRegionMsg.h"
#include "PRP/Message/plResponderMsg.h"
#include "PRP/Message/plOneShotCallbacks.h"
#include "PRP/Message/plSoundMsg.h"
#include "PRP/Message/plCameraMsg.h"
#include "PRP/Message/plTimerCallbackMsg.h"
#include "PRP/Message/plEnableMsg.h"
#include "PRP/Message/plSimulationMsg.h"
#pragma warning(pop)
#include "ImporterUtils.h"


namespace PlImporter
{
	/**
	Handles importing plMessages (such as plNotifyMsg).
	*/
	struct MessageImporter
	{
		UPlasma::PlEmu::PlMessage import(plMessage* msg, PlasmaVer version);

	private:
		void importGeneric(plMessage* msg, UPlasma::PlEmu::PlMessage uMsg, PlasmaVer version);
		void importMsgWithCallbacks(plMessage* msg, UPlasma::PlEmu::PlMessageWithCallbacks uMsg, PlasmaVer version);
	};
}
