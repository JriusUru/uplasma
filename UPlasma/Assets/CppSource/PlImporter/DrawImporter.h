/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "PRP/Object/plDrawInterface.h"
#include "PRP/Geometry/plDrawableSpans.h"
#include "PRP/Geometry/plClusterGroup.h"
#pragma warning(pop)
#include "ImporterUtils.h"
#include "MatImporter.h"
#include "LightImporter.h"
#include "CoordImporter.h"


namespace PlImporter
{
	struct DrawImporter
	{
		void import(UnityEngine::GameObject& gameObject, const plKey& drawKey, bool castShadows, UnityEngine::Transform& parentObjTransform, bool& meshIsStatic,
			bool& objHasStaticRenderer, PlasmaVer version, UPlasma::PlEmu::AssetsLifeBinder assetsBinder, bool doDisable);
		void importCluster(UnityEngine::GameObject& parentObject, const plKey& poolObjKey, PlasmaVer version,
			UPlasma::PlEmu::AssetsLifeBinder assetsBinder);
		void applyOverride(UnityEngine::GameObject& gameObject, UPlasma::PlEmu::MeshRendererOverride mrOverride);
		void postProcessSkinnedObject(UnityEngine::GameObject& gameObject, const plKey& drawKey, PlasmaVer version);
		MatImporter* getMatImporter() { return &matImporter; }
	private:
		MatImporter matImporter;
		std::map<plKey, plDrawableSpans*> skinnedMeshesThatNeedBones;
		std::map<plKey, std::pair<int, int>> objsBoneStartAndLength;
		std::map<plKey, bool> objsUsingSkinIndices;

		// std::map can only work for objects with constructors, which is dumb.
		std::vector<plKey> dspanBoneObjectsKeys;
		std::vector<System::Array1<UnityEngine::Transform>> dspanBoneObjectsValues;
	};
}
