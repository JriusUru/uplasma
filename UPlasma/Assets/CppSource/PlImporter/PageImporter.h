/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "PRP/plSceneNode.h"
#include "PRP/Message/plMsgForwarder.h"
#pragma warning(pop)
#include "DrawImporter.h"
#include "CoordImporter.h"
#include "PhysImporter.h"
#include "AudioImporter.h"
#include "LightImporter.h"
#include "AnimImporter.h"
#include "ComponentImporter.h"


namespace PlImporter
{
	struct PageImporter
	{
	private:
		DrawImporter drawImporter;
		CoordImporter coordImporter;
		PhysImporter physImporter;
		AudioImporter audioImporter;
		LightImporter lightImporter;
		AnimImporter animImporter;
		ComponentImporter componentImporter;
	public:
		void import(plResManager& rmgr, const plLocation& pageLoc, UnityEngine::GameObject& parentObj,
			UPlasma::PlEmu::AssetsLifeBinder assetsBinder);
		void postProcessImport(plResManager& rmgr, const plLocation& pageLoc, UPlasma::PlEmu::AssetsLifeBinder assetsBinder);
		DrawImporter* getDrawImporter() { return &drawImporter; }
	};
}
