/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "PRP/Object/plCoordinateInterface.h"
#include "PRP/Geometry/plDrawableSpans.h"
#pragma warning(pop)
#include "ImporterUtils.h"


namespace PlImporter
{
	/// Imports the object's coordinateinterface into Unity's transform component, by decomposing the local2world matrix and performing necessary axis conversion.
	struct CoordImporter
	{
		void import(UnityEngine::GameObject& gameObject, const plKey& coordKey, bool& dynamic, PlasmaVer version);
		/// same as import(), but using local to parent coordinates instead
		void importLocal2Parent(UnityEngine::GameObject& gameObject, const plKey& coordKey, PlasmaVer version);
		static void importMatrixToTransform(UnityEngine::Transform& transform, const hsMatrix44& mat);
	};
}
