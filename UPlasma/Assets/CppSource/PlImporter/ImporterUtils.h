/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "string_theory/st_string.h"
#include "ResManager/plResManager.h"
#include "PRP/Message/plSimulationMsg.h"
#pragma warning(pop)
#include <map>
#include <algorithm>
#include <iostream>
#include <vector>


namespace PlImporter
{
	class ExtendedResManager;


	// Because of some CPP bullshit ("objects don't have equality operators!" even though they do),
    // I have to create my own map class. Yay.
	template<typename T1, typename T2> struct SuckyMap {
		std::vector<T1> keys;
		std::vector<T2> values;

		T2& get(const T1& key) {
			std::vector<T1>::iterator it = std::find(keys.begin(), keys.end(), key);
			int index = (int)(it - keys.begin());
			return values[index];
		}
		/// returns true if added, false if updated
		bool set(T1 key, T2 value) {
			std::vector<T1>::iterator it = std::find(keys.begin(), keys.end(), key);
			if (it != keys.end())
			{
				// update
				int index = (int)(it - keys.begin());
				values[index] = value;
				return false;
			}
			else
			{
				// create
				keys.push_back(key);
				values.push_back(value);
				return true;
			}
		}
		bool containsKey(const T1& key) {
			std::vector<T1>::iterator it = std::find(keys.begin(), keys.end(), key);
			return it != keys.end();
		}
		void unset(const T1& key) {
			std::vector<T1>::iterator it = std::find(keys.begin(), keys.end(), key);
			int index = (int)(it - keys.begin());
			if (it < keys.end())
			{
				keys.erase(it);
				values.erase(values.begin() + index);
			}
		}
	};

	struct ImporterUtils
	{
		//typedef std::map<plKey, int> KeyToIndexMap;
		typedef System::Collections::Generic::List_1<UnityEngine::Object> UObjectList;

		static const float importScale; // Plasma unit = foot, we prefer meter

		static UnityEngine::Color32 getColor32FromIntColor(int col);
		static void getComponentsFromIntColor(int col, int& r, int& g, int& b, int& a);
		/// Returns a system path-safe string. Also removes some long qualifiers from materials.
		static ST::string stripIllegalChars(const char* name);
		/// Returns a system path-safe string. Also removes some long qualifiers from materials.
		static ST::string stripIllegalChars(ST::string name);
		/// Returns a ST string from a C# string
		static ST::string sharpStringToTheoryString(System::String str);
		static ST::string plasmaVerToPathsafeString(PlasmaVer ver);
		/// Returns the absolute path to a file (maybe from a mod) using a path relative to the game folder
		static ST::string getGameFile(ST::string relativePath);
		static bool parseInt(ST::string str, int& out);
		static bool parseUInt(ST::string str, unsigned int& out);
		static const char pathSeparator =
#ifdef _WIN32
			'\\';
#else
			'/';
#endif
		static const float PI;

		static void keyToObj_Set(const plKey& key, UnityEngine::Object obj);
        static void keyToObj_SetWithSharingPlKey(const plKey& key, UnityEngine::Object obj);
		static UnityEngine::Object keyToObj_Get(const plKey& key);
        static UnityEngine::Object keyToObj_GetWithSharedKey(const plKey& key, int index);
        static int keyToObj_GetSharedKeyCount(const plKey& key);
		static void keyToObj_GetAllIndicesFromPage(SuckyMap<plKey, int>& mapOUT, const plLocation& location);
		static UnityEngine::Object keyToObj_GetObjFromId(int id);
		static void keyToObj_Clear(const plLocation& location);
		static void keyToObj_InitPage(const plLocation& location);
		static void prpClues_Set(const plLocation& key, UPlasma::PlEmu::PrpImportClues obj);
		static UPlasma::PlEmu::PrpImportClues prpClues_Get(const plLocation& key);
		static void prpClues_Clear();
		static void prpClues_SaveAll();
		static PlasmaVer getPlasmaVerFromGame(UPlasma::PlEmu::PlasmaGame game);
        static void print(ST::string str);

		static void init();

		static int unlitLyrMask;
		//static int charOnlyLyrMask;
		static int unlitLyr;
		static int litLyr;
		//static int charLyr;
		static int camLyr;

		static ExtendedResManager* curResManager;

	private:
		/*
		For each prp, map a plKey to a UnityEngine::Object.
		Since limiting the number of handles is critical for performances,
		we need to store ALL known objects in a single C# container >.>
		//*/
		static SuckyMap<plLocation, SuckyMap<plKey, int>> knownObjectIndicesPerPage;
		static UObjectList allKnownObjects;
        // because some PRP objects share the same modifier (ex: PFM), we might need to generate more
        // than one Unity object per key. This is a unique case so store those separately.
        static SuckyMap<plKey, std::vector<int>> keyToUObjects;

		/*
		Similarly, we need to store prp clues objects, and since each Age can have a LOT of pages
		we let C# hold those for us once again.
		*/
		static std::vector<plLocation> prpCluesKeys;
		static System::Collections::Generic::List_1<UPlasma::PlEmu::PrpImportClues> prpCluesValues;
	};



	/**
	Workaround: adds missing getter properties to HSPlasma bindings.
	*/
	class ExtendedResManager : plResManager
	{
    public:
		/* Returns an object from its name, type and location. Useful for finding objects that
		we aren't sure actually exist.
		*/
		hsKeyedObject* getObjectFromName(ST::string objName, short type, const plLocation& loc);
		/* Returns an object's key from its name, type and location (assuming it exists).
		Useful for finding objects that we aren't sure actually exist.
		*/
		plKey getObjectKeyFromName(ST::string objName, short type, const plLocation& loc);
    };
    class ExtendedSimSuppressMsg : plSimSuppressMsg
    {
    public:
        /* suppress is protected */
        bool getSuppress();
    };
}
