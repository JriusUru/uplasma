/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once


// DO NOT USE
// (research material for using pycdc as a library - hacky and annoying to use)

#ifdef I_want_to_use_pycdc_and_redirect_its_output_with_hacks

// haxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


#include <cstdarg>
#include <stdio.h>
#include "ASTree.h"
#include "string_theory\st_string.h"

bool REDIRECT_STD_IO = false;
ST::string redirectedString;

int fprintf_OVERRIDE(FILE * stream, const char * format, ...)
{
	if (REDIRECT_STD_IO && stream == pyc_output)
	{
		// redirection active. Use sprintf_s instead of fprintf so we print the string directly into an ST::string,
		// without printing it in standard output
		va_list args;
		va_start(args, format);
		// we don't know the final length of the string chararray, so do snprintf-malloc-sprintf_s...
		// a bit slower, but this will never fail
		int size = snprintf(NULL, 0, format, args);
		char * buffer = new char[size + 1];
		int ret = sprintf_s(buffer, sizeof(buffer), format, args);
		ST::string bufferST(buffer);
		redirectedString += bufferST;
		delete[] buffer;
		return ret;
	}
	else
	{
		// redirection disabled. Just use fprintf...
		va_list args;
		va_start(args, format);
		return fprintf(stream, format, args);
	}
}
int fputs_OVERRIDE(const char * str, FILE * stream)
{
	if (REDIRECT_STD_IO && stream == pyc_output)
	{
		ST::string bufferST(str);
		redirectedString += bufferST;
		return 1;
	}
	else
	{
		// redirection disabled. Just use fputs...
		return fputs(str, stream);
	}
}

void enableSTDIORedirection()
{
	redirectedString = "";
	REDIRECT_STD_IO = true;
}
ST::string disableSTDIORedirection()
{
	REDIRECT_STD_IO = false;
	return redirectedString;
}

// now redirect fprintf
#define fprintf fprintf_OVERRIDE
#define fputs fputs_OVERRIDE

#endif