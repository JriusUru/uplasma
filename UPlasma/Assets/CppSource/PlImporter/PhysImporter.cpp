/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "Bindings.h"
#include "PhysImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

namespace PlImporter
{
	void PhysImporter::import(UnityEngine::GameObject & gameObject, const plKey & simKey, bool& dynamic, PlasmaVer version,
		AssetsLifeBinder assetsBinder, bool doDisable)
	{
		if (!simKey.Exists() || !simKey.isLoaded())
			return;
		plSimulationInterface* simInt = plSimulationInterface::Convert(simKey->getObj(), true);
		plKey physKey = simInt->getPhysical();
		if (!physKey.Exists() || !physKey.isLoaded())
			return;
		plGenericPhysical* phys = plGenericPhysical::Convert(physKey->getObj(), true);

		// flags will change according to physics engine version.
		// Fortunately, HSPlasma generalizes those for us through plSimDefs.
		// detectors are non-collideable regions if they detect the avatar or dynamic objects
		// if that's a click detector, it's still collideable (I think, works in most cases)
		bool requiresTrigger = phys->getMemberGroup() == plSimDefs::kGroupDetector && (
			phys->getReportGroup() == plSimDefs::kGroupDynamic ||
			phys->getReportGroup() == plSimDefs::kGroupAvatar
			);
		// LOS only are collision objects for the camera. Hence they are ignored by pretty much every other objects.
		// Usually they don't have visible data either, so we can safely set their layer.
		if (phys->getMemberGroup() == plSimDefs::kGroupLOSOnly)
			gameObject.SetLayer(ImporterUtils::camLyr);
		// pos/rot properties:
		// available to offset/rotate the collision vertices.
		// It seems Plasma uses this as a hack to place physical objects which have mass != 0 (meaning a coordinate interface), but leaves those unused
		// for objects with mass == 0 (objects without coordint, which means vertices are expressed in world-space).
		Vector3 vectPos;
		Quaternion quatRot(0, 0, 0, 1);
		Matrix4x4 objW2L(gameObject.GetTransform().GetWorldToLocalMatrix());
		// note: physics engine used by Plasma don't support scale. However Age authors like to do
		// scaling in object mode, sigh... Anyway, use the object's local scale to un-scale the
		// object, since Unity DOES support physics scaling (albeit a slow one).
		Vector3 objLocalScale(gameObject.GetTransform().GetLocalScale());
		bool useWorldCoordinates = false;
		if (phys->getMass() || phys->getProperty(plSimulationInterface::kPhysAnim))
		{
			// object has coordinates interface, so pos / rot is useless to us since vertices are already positioned correctly
			// keep vectPos at zero and quatRot at identity
		}
		else
		{
			// object has no coordint, so collision is expressed from world origin.
			// pos/rot should still be null/identity and unused, but we'll take possible values into account either way
			Quaternion tmpQuat(phys->getRot().W, phys->getRot().X, phys->getRot().Y, phys->getRot().Z);
			quatRot = tmpQuat;
			// ^^^ TODO - convert this to Unity's XZY space
			Vector3 tmpVec(
				phys->getPos().X * ImporterUtils::importScale,
				phys->getPos().Z * ImporterUtils::importScale,
				phys->getPos().Y * ImporterUtils::importScale);
			vectPos = tmpVec;
			useWorldCoordinates = true;
		}
		Vector3 offset(phys->getOffset().X * ImporterUtils::importScale, phys->getOffset().Z * ImporterUtils::importScale, phys->getOffset().Y * ImporterUtils::importScale);

		// Plamza identifies kickable objects with mass != 0.
		// Mass != 0 also means collision is expressed in object space - which is sometime useful for dynamic non-kickable
		// objects (parenting, etc).
		bool pinned = phys->getProperty(plSimulationInterface::kPinned) || phys->getProperty(plSimulationInterface::kPassive);
		bool upright = phys->getProperty(plSimulationInterface::kUpright);

		if (phys->getBoundsType() == plSimDefs::kHullBounds ||
			phys->getBoundsType() == plSimDefs::kProxyBounds ||
			phys->getBoundsType() == plSimDefs::kExplicitBounds ||
			phys->getVerts().size()) // box bounds are actually convex hulls on PotS
		{
			Mesh bm;
			String meshName(physKey->getName().c_str());
			bm.SetName(meshName);

			// this is thankfully much easier than importing actual drawable meshes...
			float smallestX = 1e9;
			float smallestY = 1e9;
			float smallestZ = 1e9;
			float biggestX = -1e9;
			float biggestY = -1e9;
			float biggestZ = -1e9;
			bool isCube = phys->getVerts().size() == 8;
			Array1<Vector3> uVerts((int)phys->getVerts().size());
			Array1<Int32> uFaces((Int32)(int)phys->getIndices().size());
			bool convex = phys->getBoundsType() == plSimDefs::kHullBounds;

			std::vector<uint32_t>& indices = phys->getIndices();
			std::vector<hsVector3>& verts = phys->getVerts();

			for (int i = 0; i < verts.size(); i++)
			{
				Vector3 vect(verts[i].X * ImporterUtils::importScale, verts[i].Z * ImporterUtils::importScale, verts[i].Y * ImporterUtils::importScale);
				// TODO - multiply by quat
				if (useWorldCoordinates)
				{
					vect.x = (vect.x + vectPos.x) / objLocalScale.x;
					vect.y = (vect.y + vectPos.y) / objLocalScale.y;
					vect.z = (vect.z + vectPos.z) / objLocalScale.z;
					vect = objW2L.MultiplyPoint(vect);
				}
				else
				{
					vect.x = (vect.x + vectPos.x) / objLocalScale.x;
					vect.y = (vect.y + vectPos.y) / objLocalScale.y;
					vect.z = (vect.z + vectPos.z) / objLocalScale.z;
				}
				uVerts[i] = vect;
			}

			for (int i = 0; i < indices.size() / 3; i++)
			{
				// note: plamsa faces not in same order as unity's
				uFaces[i * 3] = indices[i * 3];
				uFaces[i * 3 + 1] = indices[i * 3 + 2];
				uFaces[i * 3 + 2] = indices[i * 3 + 1];
			}

			bm.SetVertices(uVerts);
			if (indices.size())
				bm.SetTriangles(uFaces);
			else
			{
				// this is a Havok convex hull, which is created from a point cloud.
				// Unity needs faces to generate CH, so we use a hull algorithm to generate those.
				// (PERF: might be useful to port this to C++ for performances ?)
				NativeHelpers::CreateFacesForHull(bm);
			}

			MeshCollider meshCol = gameObject.AddComponent<MeshCollider>();
			meshCol.SetSharedMesh(bm);

			// convex:
			//	required for triggers
			//	required for kickables
			//	mesh must have less than 255 faces. probably a stupid limit that could be removed but we'll live with it.
			// forcing a mesh to convex in those cases is usually pretty safe as it's often hardly noticeable.
			// however, if your convex mesh has more than 255 faces, you're screwed.

			if (bm.GetTriangles().GetLength() <= 255 * 3)
			{
				// normal case where you can enforce convex if needed.
				// we can still enable convex for meshes that were already stored as convex hulls.

				meshCol.SetConvex(convex || requiresTrigger); // can't use non-convex triggers in Unity, force convex
				meshCol.SetIsTrigger(requiresTrigger);

				if (phys->getMass() && !pinned)
				{
					meshCol.SetConvex(true); // mandatory in Unity for kickables
					Rigidbody& rb = gameObject.AddComponent<Rigidbody>();
					rb.SetMass(phys->getMass());
					rb.SetFreezeRotation(upright);
					rb.SetCollisionDetectionMode(CollisionDetectionMode::Continuous);
					dynamic = true;
				}
			}
			else
			{
				// really can't use Unity convex hull in this case...
				if (requiresTrigger)
				{
					// oh shite
					char errorMsgC[256];
					sprintf_s(errorMsgC, sizeof errorMsgC, "Physical %s uses more than 255 faces but must be convex due to being a trigger. This is not supported at all by Unity, disabling.",
						physKey->getName().c_str());
					String errorMsg(errorMsgC);
					Debug::LogError(errorMsg);
					meshCol.SetEnabled(false);
				}
				if (phys->getMass() && !pinned)
				{
					// re-shite
					char errorMsgC[256];
					sprintf_s(errorMsgC, sizeof errorMsgC, "Physical %s uses more than 255 faces but must be convex due to being dynamic. This is not supported at all by Unity, disabling.",
						physKey->getName().c_str());
					String errorMsg(errorMsgC);
					Debug::LogError(errorMsg);
					meshCol.SetEnabled(false);
				}

				// originally convex but has more than 255 faces, and doesn't /actually/ require convex ?
				// Just use mesh collision. Carry on, buddy !
			}

			if (doDisable)
				meshCol.SetEnabled(false);

			Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
			assets.Add(bm);
		}
		else if (phys->getBoundsType() == plSimDefs::kBoxBounds)
		{
			BoxCollider boxCol = gameObject.AddComponent<BoxCollider>();
			boxCol.SetSize(Vector3(
				phys->getDimensions().X * ImporterUtils::importScale * 2,
				phys->getDimensions().Z * ImporterUtils::importScale * 2,
				phys->getDimensions().Y * ImporterUtils::importScale * 2));
			boxCol.SetCenter(vectPos + offset);
			boxCol.SetIsTrigger(requiresTrigger);

			if (phys->getMass() && !pinned)
			{
				Rigidbody& rb = gameObject.AddComponent<Rigidbody>();
				rb.SetMass(phys->getMass());
				rb.SetFreezeRotation(upright);
				rb.SetCollisionDetectionMode(CollisionDetectionMode::Continuous);
				dynamic = true;
			}

			if (doDisable)
				boxCol.SetEnabled(false);
		}
		else if (phys->getBoundsType() == plSimDefs::kSphereBounds)
		{
			SphereCollider sphereCol = gameObject.AddComponent<SphereCollider>();
			sphereCol.SetRadius(phys->getRadius() * ImporterUtils::importScale);

			sphereCol.SetCenter(vectPos + offset);
			sphereCol.SetIsTrigger(requiresTrigger);

			if (phys->getMass() && !pinned)
			{
				Rigidbody& rb = gameObject.AddComponent<Rigidbody>();
				rb.SetMass(phys->getMass());
				rb.SetFreezeRotation(upright);
				rb.SetCollisionDetectionMode(CollisionDetectionMode::Continuous);
				dynamic = true;
			}
			if (doDisable)
				sphereCol.SetEnabled(false);
		}
		else if (phys->getBoundsType() == plSimDefs::kCylinderBounds)
		{
			// theoretically, this is only ever used by Laki's pirahna birds
			// import it as a capsule collider. Close enough.
			CapsuleCollider capsuleCol = gameObject.AddComponent<CapsuleCollider>();
			capsuleCol.SetRadius(phys->getRadius() * ImporterUtils::importScale);
			capsuleCol.SetHeight(phys->getLength() * ImporterUtils::importScale);
			capsuleCol.SetCenter(vectPos + offset);
			capsuleCol.SetIsTrigger(requiresTrigger);

			if (phys->getMass() && !pinned)
			{
				Rigidbody& rb = gameObject.AddComponent<Rigidbody>();
				rb.SetMass(phys->getMass());
				rb.SetFreezeRotation(upright);
				rb.SetCollisionDetectionMode(CollisionDetectionMode::Continuous);
				dynamic = true;
			}
			if (doDisable)
				capsuleCol.SetEnabled(false);
		}
	}
}
