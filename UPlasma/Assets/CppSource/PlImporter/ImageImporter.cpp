﻿/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "ImageImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

namespace PlImporter
{
	plResManager * ImageImporter::rmgr;

	void ImageImporter::import(Texture2D& texOUT, const plKey & origTexKey, PlasmaVer version,
		AssetsLifeBinder assetsBinder, bool clampU, bool clampV, bool linear)
	{
		plKey texKey = origTexKey;
		if (texKey->getType() != pdClassType::kMipmap)
			return;
		if (!texKey.Exists() || !texKey.isLoaded())
			return;

		// check if we already imported the texture
		UnityEngine::Object texObj = ImporterUtils::keyToObj_Get(texKey);
		if (texObj.Handle != 0)
		{
			// already imported. Return it.
			texOUT = NativeHelpers::CastObjTo<Texture2D>(texObj);
			return;
		}

		ST::string texNameT = texKey->getName();

		// Circumvent the not-so-famous bug with textures not being at their highest resolution.
		// This requires searching the textures prp for a texture with a similar name.
		// (myTexture_0_2 will look for myTexture_0_0)
		// TODO: this obviously won't work if the high-res texture isn't even in the PRP (ex: Nexus).
		// We need to put up some kind of "bounty" system, which will register bounties for high
		// res textures. Encountering the high-res equivalent in another Age will dump the texture
		// to the cache so it can be found when re-linking to the Age with the missing tex...
		ST::string texNameModif = texNameT;
		ST::string extension;
		if (texNameModif.find_last('.') == texNameModif.size() - 4)
		{
			extension = texNameModif.substr(texNameModif.size() - 4);
			texNameModif = texNameModif.substr(0, texNameModif.size() - 4);
		}
		size_t hashIndex = texNameModif.find_last('#');
		size_t starIndex = texNameModif.find_last('*');
		if (hashIndex != -1 && starIndex != -1)
		{
			ST::string levelS = texNameModif.substr(hashIndex + 1);
			ST::string formatS = texNameModif.substr(starIndex + 1, hashIndex - starIndex - 1);
			unsigned int level, format;
			if (ImporterUtils::parseUInt(levelS, level))
			{
				if (level > 0 && ImporterUtils::parseUInt(formatS, format))
				{
					// we are now sure the name is that of a Cyan texture, and that it uses
					// a low-res version of the texture... Try to find a better version !
					for (unsigned int i = 0; i < level; i++)
					{
						ST::string newLevelS(std::to_string(i));
						ST::string lookTexName(texNameModif.substr(0, hashIndex + 1) + newLevelS + extension);
						plKey objKey = ImporterUtils::curResManager->getObjectKeyFromName(
							lookTexName,
							texKey->getType(),
							texKey->getLocation());
						if (objKey.Exists())
						{
							char msgc[512];
							sprintf_s(msgc, sizeof(msgc), "Replacing texture %s with %s", texNameT.c_str(), lookTexName.c_str());
							String msg(msgc);
							Debug::Log(msg);
							texKey = objKey;
							break;
						}
					}
				}
			}
		}

		texNameT = ImporterUtils::stripIllegalChars(texKey->getName().c_str());
		String texName(texNameT.c_str());

		// check (again) if we already imported the texture
		texObj = ImporterUtils::keyToObj_Get(texKey);
		if (texObj.Handle != 0)
		{
			// already imported. Return it.
			texOUT = NativeHelpers::CastObjTo<Texture2D>(texObj);
			return;
		}

		// see if we have an override somewhere
		PlGameFileAccessor accessor(PlGameFileAccessor::GetInstance());
		String pathcsstr(accessor.GetFile(texName));
		if (pathcsstr.Handle)
		{
			// oh, looks like we have an override tex... Load it then.
			ST::string pathTheory(ImporterUtils::sharpStringToTheoryString(pathcsstr));
			ST::string pathTheoryLower = pathTheory.to_lower();

			if (pathTheoryLower.ends_with(".dds"))
			{
				// dds image

				// load all bytes from the file
				std::ifstream f(pathTheory.c_str(), std::ios::binary | std::ios::ate);
				std::ifstream::pos_type pos = f.tellg();
				int len = (int)pos;
				Array1<Byte> csArr(len);
				char* ddsBytes = new char[len];
				f.seekg(0, std::ios::beg);
				f.read(ddsBytes, len);
				for (int i = 0; i < len; i++)
					csArr[i] = ddsBytes[i];

				// okay so let's try to parse the DDS header
				// we only support dxt1 and dxt5 for now, because they are the most common.
				// we also support mipmaps, although they are optional.
				// full doc of the header format is here, for reference:
				// https://docs.microsoft.com/en-us/windows/desktop/direct3ddds/dds-header

				char headerSig[4] = { ddsBytes[84], ddsBytes[85], ddsBytes[86], ddsBytes[87] };
				ST::string headerSigT(headerSig);

				if (ddsBytes[4] == 124 && (headerSigT == "DXT1" || headerSigT == "DXT5"))
				{
					int ddsFlags = 0; int mipCount = 0;
					ddsFlags = *(ddsBytes + 4); // flags is second int of header
					mipCount = *(ddsBytes + 6 * 4); // mip count is sixth integer
					TextureFormat format = TextureFormat::DXT1;
					if (headerSigT == "DXT5")
						format = TextureFormat::DXT5;
					bool doMipmaps = (ddsFlags & 0x20000) && mipCount > 1;

					int height = ddsBytes[13] * 256 + ddsBytes[12];
					int width = ddsBytes[17] * 256 + ddsBytes[16];

					int DDS_HEADER_SIZE = 128;
					int dxtSize = len - DDS_HEADER_SIZE;
					char* dxtBytes = new char[dxtSize];
					memcpy_s(dxtBytes, sizeof(dxtBytes), ddsBytes, dxtSize);

					Texture2D uTex(width, height, format, doMipmaps, linear);
					uTex.LoadRawTextureData(dxtBytes, dxtSize);
					uTex.Apply(false, true);

					delete[] dxtBytes;
					delete[] ddsBytes;
					uTex.SetWrapModeU(clampU ? TextureWrapMode::Clamp : TextureWrapMode::Repeat);
					uTex.SetWrapModeV(clampV ? TextureWrapMode::Clamp : TextureWrapMode::Repeat);

					// register the texture so we don't import it twice
					ImporterUtils::keyToObj_Set(texKey, uTex);
					if (texKey != origTexKey)
						ImporterUtils::keyToObj_Set(origTexKey, uTex);

					Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
					assets.Add(uTex);

					texOUT = uTex;
					return;
				}
				else
				{
					char msgc[1024];
					sprintf_s(msgc, sizeof(msgc), "Could not load %s: invalid header or format", pathTheory.c_str());
					String msg(msgc);
					Debug::LogError(msg);
				}
			}
			else if (pathTheoryLower.ends_with(".png")
				|| pathTheoryLower.ends_with(".jpg")
				|| pathTheoryLower.ends_with(".jpeg"))
			{
				// jpeg or png

				// create the texture we will store the image in.
				// note that unity rebuilds the texture from near scratch on loading. However,
				// specifying textureformat and mipmaps give Unity a hint how it should load the image.
				// (specifically, dxt5 means we want the texture compressed dxt1 or dxt5)
				Texture2D uTex(2, 2, TextureFormat::DXT5, true, linear);

				// load all bytes from the file
				std::ifstream f(pathTheory.c_str(), std::ios::binary | std::ios::ate);
				std::ifstream::pos_type pos = f.tellg();
				Array1<Byte> csArr((int)pos);
				char* result = new char[pos];
				f.seekg(0, std::ios::beg);
				f.read(&result[0], pos);
				for (int i = 0; i < pos; i++)
					csArr[i] = result[i];
				delete[] result;

				// load the image
				if (ImageConversion::LoadImage(uTex, csArr, true))
				{
					uTex.SetWrapModeU(clampU ? TextureWrapMode::Clamp : TextureWrapMode::Repeat);
					uTex.SetWrapModeV(clampV ? TextureWrapMode::Clamp : TextureWrapMode::Repeat);

					// register the texture so we don't import it twice
					ImporterUtils::keyToObj_Set(texKey, uTex);
					if (texKey != origTexKey)
						ImporterUtils::keyToObj_Set(origTexKey, uTex);

					Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
					assets.Add(uTex);

					texOUT = uTex;
					return;
				}
				else
				{
					char msgc[1024];
					sprintf_s(msgc, sizeof(msgc), "Could not load %s, check that the image is valid", pathTheory.c_str());
					String msg(msgc);
					Debug::LogError(msg);
				}
			}
		}

		// if we're all the way here, it means we have no other version of the texture.
		// Just import it.
		plMipmap* plTex = plMipmap::Convert(texKey->getObj(), true);

		TextureFormat uTexFormat = TextureFormat::DXT1;
		if (plTex->getCompressionType() == plBitmap::kDirectXCompression)
		{
			if (plTex->getDXCompression() == plBitmap::kDXT1)
				uTexFormat = TextureFormat::DXT1;
			else if (plTex->getDXCompression() == plBitmap::kDXT5)
				uTexFormat = TextureFormat::DXT5;
			else
			{
				char buf[256];
				sprintf_s(buf, 256, "DXT format %d not supported !", plTex->getDXCompression());
				String msg(buf);
				Debug::LogError(msg);
				return;
			}
		}
		else if (plTex->getCompressionType() == plBitmap::kUncompressed)
		{
			if (plTex->getARGBType() == plBitmap::kRGB8888)
				uTexFormat = TextureFormat::BGRA32;
			else
			{
				char buf[256];
				sprintf_s(buf, 256, "uncompressed format %d not supported !", plTex->getARGBType());
				String msg(buf);
				Debug::LogError(msg);
				return;
			}
		}
		else
		{
			char buf[256];
			sprintf_s(buf, 256, "texture format %d not supported !", plTex->getCompressionType());
			String msg(buf);
			Debug::LogError(msg);
			return;
		}

		if (plTex->getSpace() != plBitmap::kDirectSpace &&
			plTex->getSpace() != plBitmap::kNoSpace)
		{
			// we only support direct space. No space means we don't give a fuck about the texture itself,
			// since most of the time it's a white or black 1x1 texture (so it will look ok no matter the colorspace)
			char buf[256];
			sprintf_s(buf, 256, "texture space %d not supported !", plTex->getSpace());
			String msg(buf);
			Debug::LogError(msg);
			return;
		}

		Texture2D uTex(plTex->getWidth(), plTex->getHeight(), uTexFormat, plTex->getNumLevels() > 1, linear);
		uTex.SetName(texName);
		uTex.SetFilterMode(FilterMode::Trilinear);

		if (plTex->getNumLevels() > 1)
		{
			/*
			Plasma has a bug which results in the lowest sized mipmap being skipped (which means it also happens in HSPlasma).
			This causes issues with Unity, since LoadRawTextureData expects to find all mipmaps down to the lowest level.
			To circumvent that, we append a few bytes of null data, which act as the lowest level mipmap (I don't know how DXT5
			works, but this is enough to satisfy Unity).
			NOTE: it seems it happens systematically for DXT5 textures, and sometime for DXT1 textures from fan-Ages.
			UPDATE: it might also be caused by textures not having a 1:1 width:height ratio (think the HNY banner in Bevin). Unity
			might be expecting the last mips to scale down to 1x1 pixel from 1x8, 1x4 and 1x2.
			Either way, make sure we add enough zero data.
			Memcpy-ing data into a tmp buffer has an almost negligible impact on loading time.
			UPDATE 2: so fake last level mipmap is actually noticeable ingame. Crap. FIXME.
			*/

			size_t textureSize = plTex->getTotalSize();
			size_t padding = 64;
			uint8_t* buffer = new uint8_t[textureSize + padding];
			memcpy(buffer, plTex->getImageData(), textureSize);
			// attempt to fix lowest mip...
			// Overwriting the last 32 bits of data often (but not always) fixes incorrect colors,
			// but replaces the last mipmap with black.
			// On square DXT5 textures, the last two mips are either missing or corrupted.
			// Since DXT works by "blocks" and one block represents 4 pixels (thus the 4:1 compression ratio),
			// we can copy those blocks around to the next mip - this is not correct mipmap generation since
			// it means only the upper left corner is copied, but this is a good way to create a mipmap using
			// roughly the same color palette as the previous level.
			// Works for: DXT5 square textures only. That's an improvement, but must still find something
			// for rectangle textures and DXT1...
			memset(buffer + textureSize, 0, padding);
			memcpy(buffer + textureSize - 16, buffer + textureSize - 32, 16); // last-but-one corrupted mip
			memcpy(buffer + textureSize, buffer + textureSize - 32, 16); // last missing mip
			memcpy(buffer + textureSize + 16, buffer + textureSize - 32, 48); // moar random data (failed attempt...)
			uTex.LoadRawTextureData((void*)buffer, (int)(textureSize + padding));
			delete[] buffer;
		}
		else
		{
			uTex.LoadRawTextureData((void*)plTex->getImageData(), (int)plTex->getTotalSize());
		}
		uTex.SetWrapModeU(clampU ? TextureWrapMode::Clamp : TextureWrapMode::Repeat);
		uTex.SetWrapModeV(clampV ? TextureWrapMode::Clamp : TextureWrapMode::Repeat);
		uTex.Apply(false, true); // upload to GC, don't recompute mipmaps and free CPU clone of the texture

								 // register the texture so we don't import it twice
		ImporterUtils::keyToObj_Set(texKey, uTex);
		if (texKey != origTexKey)
			ImporterUtils::keyToObj_Set(origTexKey, uTex);

		Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
		assets.Add(uTex);

		texOUT = uTex;
	}
	void ImageImporter::importCube(UnityEngine::Cubemap& cubeOUT, const plKey & texKey, PlasmaVer version,
		AssetsLifeBinder assetsBinder)
	{
		if (texKey->getType() != pdClassType::kCubicEnvironmap)
			return;
		if (!texKey.Exists() || !texKey.isLoaded())
			return;

		// check if we already imported the cube
		UnityEngine::Object texObj = ImporterUtils::keyToObj_Get(texKey);
		if (texObj.Handle != 0)
		{
			// already imported. Return it.
			cubeOUT = NativeHelpers::CastObjTo<Cubemap>(texObj);
			return;
		}

		// else import it

		plCubicEnvironmap* plCube = plCubicEnvironmap::Convert(texKey->getObj(), true);
		// fetch texture size and format
		TextureFormat uTexFormat = TextureFormat::DXT1;
		plMipmap* plTex = plCube->getFace(0);
		if (plTex->getCompressionType() == plBitmap::kDirectXCompression)
		{
			if (plTex->getDXCompression() == plBitmap::kDXT1)
				uTexFormat = TextureFormat::DXT1;
			else if (plTex->getDXCompression() == plBitmap::kDXT5)
				uTexFormat = TextureFormat::DXT5;
			else
			{
				char buf[256];
				sprintf_s(buf, 256, "DXT format %d not supported !", plTex->getDXCompression());
				String msg(buf);
				Debug::LogError(msg);
				return;
			}
		}
		else if (plTex->getCompressionType() == plBitmap::kUncompressed)
		{
			if (plTex->getARGBType() == plBitmap::kRGB8888)
				uTexFormat = TextureFormat::BGRA32;
			else
			{
				char buf[256];
				sprintf_s(buf, 256, "uncompressed format %d not supported !", plTex->getARGBType());
				String msg(buf);
				Debug::LogError(msg);
				return;
			}
		}
		else
		{
			char buf[256];
			sprintf_s(buf, 256, "texture format %d not supported !", plTex->getCompressionType());
			String msg(buf);
			Debug::LogError(msg);
			return;
		}

		if (plTex->getSpace() != plBitmap::kDirectSpace &&
			plTex->getSpace() != plBitmap::kNoSpace)
		{
			// we only support direct space. No space means we don't give a fuck about the texture itself,
			// since most of the time it's a white or black 1x1 texture (so it will look ok no matter the colorspace)
			char buf[256];
			sprintf_s(buf, 256, "texture space %d not supported !", plTex->getSpace());
			String msg(buf);
			Debug::LogError(msg);
			return;
		}

		// we'll use a single texture as buffer in which we'll load all 6 faces successively.
		// Each time, we use cubemap.SetPixels(texture.GetPixels, face) to load these to the cubemap.
		// Get/Set pixels and passing data from CPU to GPU is quite slow, so this isn't really optimized...
		// Fortunately, Plasma cubemaps are very seldomly used and small in size, so it shouldn't be much of a drag.
		//
		// Mipmapping:
		//	Loading Plamza mipmaps means we have to work around the mipmap count bug, which is too cumbersome. Instead,
		//  we let Unity generate mipmaps once we upload the final cubemap. For the same reasons as before, it shouldn't
		//  have an impact on load time.
		Texture2D tmpTexture(plTex->getWidth(), plTex->getHeight(), uTexFormat, false, false);
		Cubemap uCube(plTex->getWidth(), TextureFormat::RGB24, true);
		String texName(ImporterUtils::stripIllegalChars(texKey->getName().c_str()).c_str());
		uCube.SetName(texName);

		for (int i = 0; i < 6; i++)
		{
			plTex = plCube->getFace(i);
			tmpTexture.LoadRawTextureData((void*)plTex->getImageData(), (int)plTex->getTotalSize());
			tmpTexture.Apply(false, false);
			// now that our texture is in memory, we can use getpixels to decode its pixels back to the cpu... only to reupload it back.
			System::Array1<Color> pixels(tmpTexture.GetPixels(0));
			CubemapFace face = CubemapFace::NegativeX;
			switch (i)
			{
			case 0:
				face = CubemapFace::NegativeX;
				break;
			case 1:
				face = CubemapFace::PositiveX;
				break;
			case 2:
				face = CubemapFace::PositiveZ;
				break;
			case 3:
				face = CubemapFace::NegativeZ;
				break;
			case 4:
				face = CubemapFace::PositiveY;
				break;
			case 5:
				face = CubemapFace::NegativeY;
				break;
			}
			uCube.SetPixels(pixels, face, 0);
		}
		uCube.Apply(true, true);

		// register the texture so we don't import it twice
		ImporterUtils::keyToObj_Set(texKey, uCube);

		// destroy the temp tex
		MonoBehaviour::Destroy(tmpTexture);

		Collections::Generic::List_1<UnityEngine::Object> assets(assetsBinder.GetAssets());
		assets.Add(uCube);

		cubeOUT = uCube;
	}
}