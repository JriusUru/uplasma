/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "PRP/Geometry/plDrawableSpans.h"
#include "PRP/Surface/hsGMaterial.h"
#include "PRP/Surface/plLayer.h"
#include "PRP/Surface/plLayerAnimation.h"
#include "PRP/Surface/plLayerMovie.h"
#pragma warning(pop)
#include "ImporterUtils.h"
#include "ImageImporter.h"
#include "AnimImporter.h"


namespace PlImporter
{
	/**
	Handles importing materials and layers, and attempts to find the shader that best mimics the material's behavior.
	Texture importing is done by ImageImporter.


	RNADOM INFOS:

	specular: highlight color
	ambient: add to all other colors (emission)
	runtime: multiply dynamic lighting
	preshade: multiply vertex color
	*/
	struct MatImporter
	{
		void import(UnityEngine::Material& matOUT, const plKey& matKey, UnityEngine::Cubemap& cubeOut,
			UnityEngine::GameObject lyrAnimParentObject, PlasmaVer version, UPlasma::PlEmu::AssetsLifeBinder assetsBinder);
		void import(UnityEngine::Material& matOUT, const plKey& matKey, UnityEngine::Cubemap& cubeOut,
			UnityEngine::GameObject lyrAnimParentObject, plDrawableSpans* span, PlasmaVer version, UPlasma::PlEmu::AssetsLifeBinder assetsBinder);
		void importLightLayer(UnityEngine::Material& matOUT, const plKey& lyrKey, PlasmaVer version, UPlasma::PlEmu::AssetsLifeBinder assetsBinder);
	private:
		ImageImporter imgImporter;
		std::map<ST::string, ST::string> matToShader;

		plKey getBaseLayerKey(const plKey& plLayerKey, PlasmaVer version);
		bool isPlLayerVisibleAndSurface(const plKey& plLayerKey, PlasmaVer version);
		bool isPlLayerVisibleAndCubemap(const plKey& plLayerKey, PlasmaVer version);
		bool isPlLayerMask(const plKey& plLayerKey, PlasmaVer version);
		void importAnimLayers(const plKey& plLayerKey, UnityEngine::GameObject animObject, bool createAnimObject, PlasmaVer version);
		AnimImporter animImporter;
	};
}
