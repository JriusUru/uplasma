/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"
#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#include "ResManager/plFactory.h"
#include "PRP/Modifier/plLogicModifier.h"
#include "PRP/Animation/plViewFaceModifier.h"
#include "PRP/Surface/plFadeOpacityMod.h"
#include "PRP/Physics/plDetectorModifier.h"
#include "PRP/Physics/plObjectInVolumeDetector.h"
#include "PRP/ConditionalObject/plFacingConditionalObject.h"
#include "PRP/ConditionalObject/plActivatorConditionalObject.h"
#include "PRP/ConditionalObject/plDetectConditionalObjects.hpp"
#include "PRP/ConditionalObject/plVolumeSensorConditionalObject.h"
#include "PRP/Modifier/plResponderModifier.h"
#include "PRP/Modifier/plExcludeRegionModifier.h"
#include "PRP/Modifier/plPythonFileMod.h"
#include "PRP/Avatar/plArmatureMod.h"
#include "PRP/Modifier/plMaintainersMarkerModifier.h"
#include "PRP/Modifier/plOneShotMod.h"
#include "PRP/Avatar/plSittingModifier.h"
#include "PRP/Modifier/plRandomSoundMod.h"
#include "PRP/Physics/plCollisionDetector.h"
#include "PRP/Surface/plWaveSet.h"
#include "PRP/Surface/plDynamicEnvMap.h"
#include "PRP/Camera/plCameraModifier.h"
#pragma warning(pop)
#include "ImporterUtils.h"
#include "MessageImporter.h"
#include "LightImporter.h"
#include <vector>


namespace PlImporter
{
	/**
	Handles setting up Plasma components from modifiers and interfaces.
	*/
	struct ComponentImporter
	{
		void setupComponents(plLocation location, PlasmaVer version, UPlasma::PlEmu::AssetsLifeBinder& assetsBinder);
		UnityEngine::ReflectionProbe importDynamicEnvMap(plKey envMapKey);

	private:
		MessageImporter messageImporter;
	};

	/**
	Workaround: adds missing getter properties to HSPlasma bindings.
	*/
	struct ArmaturePropertiesGetter : plArmatureMod
	{
		ST::string getRootName() { return fRootName; }
		int getBodyType() { return fBodyType; }
		plKey getDefaultMesh() { return fDefaultMesh; }
		std::vector<plKey>& getMeshKeys() { return fMeshKeys; }
	};
	struct RandomSoundPropGetter : plRandomSoundMod 
	{
		unsigned char getState() { return fState; }
		unsigned char getMode() { return fMode; }
		float getMinDelay() { return fMinDelay; }
		float getMaxDelay() { return fMaxDelay; }
		std::vector<plRandomSoundModGroup>& getGroups() { return fGroups; }
	};
	struct RandomSoundGroupPropGetter : plRandomSoundModGroup
	{
		std::vector<unsigned short>& getIndices() { return fIndices; }
		short getGroupIdx() { return fGroupedIdx; }
	};
}
