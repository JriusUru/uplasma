/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "Bindings.h"
#include "AudioImporter.h"

using namespace System;
using namespace UnityEngine;
using namespace UPlasma::PlEmu;

namespace PlImporter
{
	void AudioImporter::import(UnityEngine::GameObject & gameObject, const plKey & audioKey, PlasmaVer version, bool doDisable)
	{
		if (!audioKey.Exists() || !audioKey.isLoaded())
			return;

		plAudioInterface* audInt = plAudioInterface::Convert(audioKey->getObj(), true);
		plKey audibleKey = audInt->getAudible();
		if (!audibleKey.Exists() || !audibleKey.isLoaded())
			return;
		plWinAudible* winAudible = plWinAudible::Convert(audibleKey->getObj(), true);
		PlAudible uAudible(gameObject.AddComponent<PlAudible>());
		if (doDisable)
			uAudible.SetEnabled(false);
		ImporterUtils::keyToObj_Set(audioKey, uAudible);
		ImporterUtils::keyToObj_Set(audibleKey, uAudible);

		std::vector<plKey>& sounds = winAudible->getSounds();
		Array1<PlSound> uSounds((int)sounds.size());
		int i = 0;
		for (plKey soundKey : sounds)
		{
			if (!soundKey.Exists() || !soundKey.isLoaded())
				continue;

			plWin32Sound* sound = plWin32Sound::Convert(soundKey->getObj(), true);
			plEAXSourceSettings& eaxSource = sound->getEAXSettings();
			plEAXSourceSoftSettings& eaxSoftStart = eaxSource.getSoftStarts();
			plEAXSourceSoftSettings& eaxSoftEnd = eaxSource.getSoftEnds();
			AudioSource source = gameObject.AddComponent<AudioSource>();
			PlSound plSound = gameObject.AddComponent<PlSound>();
			PlFadeParams uFadeInParams;
			PlFadeParams uFadeOutParams;
			PlEAXSourceSettings uEaxSource;
			PlEAXSourceSoftSettings uEaxSoftStart;
			PlEAXSourceSoftSettings uEaxSoftEnd;

			plKey dataBufferKey = sound->getDataBuffer();
			if (dataBufferKey.Exists() && dataBufferKey.isLoaded())
			{
				plSoundBuffer* dataBuffer = plSoundBuffer::Convert(dataBufferKey->getObj(), true);
				plWAVHeader& header = dataBuffer->getHeader();

				PlSoundBuffer soundBuffer;
				PlWAVHeader uHeader;

				uHeader.SetAvgBytesPerSec(header.getAvgBytesPerSec());
				uHeader.SetBitsPerSample(header.getBitsPerSample());
				uHeader.SetBlockAlign(header.getBlockAlign());
				uHeader.SetFormatTag(header.getFormatTag());
				uHeader.SetNumChannels(header.getNumChannels());
				uHeader.SetNumSamplesPerSec(header.getNumSamplesPerSec());

				String soundUrl(dataBuffer->getFileName().c_str());
				soundBuffer.SetDataLength((int)dataBuffer->getDataLength());
				soundBuffer.SetFileName(soundUrl);
				soundBuffer.SetFlagsInt(dataBuffer->getFlags());
				soundBuffer.SetHeader(uHeader);

				plSound.SetDataBuffer(soundBuffer);
			}

			plSound.SetTypeInt(sound->getType());
			plSound.SetPriority(sound->getPriority());
			plSound.SetPlaying(sound->isPlaying());
			plSound.SetTime(sound->getTime());
			plSound.SetMaxFalloff((int)(sound->getMaxFalloff() * ImporterUtils::importScale));
			plSound.SetMinFalloff((int)(sound->getMinFalloff() * ImporterUtils::importScale));
			plSound.SetOuterVol(sound->getOuterVol());
			plSound.SetInnerCone(sound->getInnerCone());
			plSound.SetOuterCone(sound->getOuterCone());
			plSound.SetCurrVolume(sound->getCurrVolume());
			plSound.SetDesiredVol(sound->getDesiredVolume());
			plSound.SetFadedVolume(sound->getFadedVolume());
			plSound.SetPropertiesInt(sound->getProperties());
			plSound.SetChannel(sound->getChannel());

			//plSound.SetEAXSettings(sound->getEAXSettings());
			//plSound.SetFadeInParams(sound->getFadeInParams());
			//plSound.SetFadeOutParams(sound->getFadeOutParams());

			String subtitleId(sound->getSubtitleId().c_str());
			plSound.SetSubtitleId(subtitleId);

			if (doDisable)
				plSound.SetEnabled(false);

			uSounds[i] = plSound;
			i++;
		}

		uAudible.SetSounds(uSounds);
	}
}
