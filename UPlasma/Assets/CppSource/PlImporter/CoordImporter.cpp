/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "CoordImporter.h"

using namespace System;
using namespace UnityEngine;

inline float fsqrt(float number)
{
	// Quake's Fast Inverse Square Root
	float xhalf = 0.5f*number;
	int i = *(int*)&number;
	i = 0x5f3759df - (i >> 1); // what the f*ck ?
	number = *(float*)&i;
	number = number * (1.5f - xhalf * number*number);
	return 1/number;
}

inline float magnitude(float x, float y, float z)
{
	return std::sqrt(x * x + y * y + z * z);
}

namespace PlImporter
{
	void CoordImporter::import(UnityEngine::GameObject & gameObject, const plKey & coordKey, bool& dynamic, PlasmaVer version)
	{
		Transform& transform = gameObject.GetTransform();

		if (!coordKey.Exists() || !coordKey.isLoaded())
			return;

		plCoordinateInterface* coordInt = plCoordinateInterface::Convert(coordKey->getObj(), true);
		if (!coordInt)
			return;

		const hsMatrix44& mat = coordInt->getLocalToWorld();
		
		importMatrixToTransform(transform, mat);

		dynamic = coordInt->getProperty(plCoordinateInterface::kCanEverDelayTransform);
	}

	void CoordImporter::importLocal2Parent(UnityEngine::GameObject & gameObject, const plKey & coordKey, PlasmaVer version)
	{
		Transform& transform = gameObject.GetTransform();

		if (!coordKey.Exists() || !coordKey.isLoaded())
			return;

		plCoordinateInterface* coordInt = plCoordinateInterface::Convert(coordKey->getObj(), true);
		if (!coordInt)
			return;

		const hsMatrix44& mat = coordInt->getLocalToParent();

		importMatrixToTransform(transform, mat);
	}
	void CoordImporter::importMatrixToTransform(UnityEngine::Transform & transform, const hsMatrix44 & mat)
	{
		// decompose the matrix into position and scale vector, and quaternion rotation

		// pos: rightmost column of the matrix
		Vector3 pos(mat(0, 3) * ImporterUtils::importScale, mat(2, 3) * ImporterUtils::importScale, mat(1, 3) * ImporterUtils::importScale);
		transform.SetLocalPosition(pos);

        // rotation and scale: computed from the unit vectors of the coordinate system...
		Vector3 vx(mat(0, 0), mat(2, 0), mat(1, 0));
        Vector3 vy(mat(0, 2), mat(2, 2), mat(1, 2));
		Vector3 vz(mat(0, 1), mat(2, 1), mat(1, 1));

		// sca: magnitude of those vectors
		float sx = magnitude(vx.x, vx.y, vx.z);
        float sy = magnitude(vy.x, vy.y, vy.z);
		float sz = magnitude(vz.x, vz.y, vz.z);

        Vector3 sca(sx, sy, sz);

        // Note about scaling:
        // Accurately extracting scaling coefficients out of a matrix is impossible. Sign of those coefficient is lost
        // when converted to a matrix - all that remains is a left-or-right handed matrix. This is enough for us
        // to find a rotation/scale combination that looks right for normal object positioning, though.
        // However this does cause issues when trying to animate only scale or only rotation - you can't replace one
        // without replacing the other.
        // I *think* this is what the affineparts in the anim is for. Hopefully. Still, as far as ease of use
        // is concerned, it still rates as sucks/20.
		System::Single dot(Vector3::Dot(Vector3::Cross(vz, vx), vy));
        if (dot < 0)
            sca.x = -sca.x;
        transform.SetLocalScale(sca);

        // rot: rebuild a look quaternion from the forward and upward vectors of the matrix
        // (this doesn't account for a non-even number of negative scaling factor like (-1, -1, -1) or (-1, 1, 1), we'll fix it later)
        Quaternion quat(Quaternion::LookRotation(vz, vy));
        transform.SetLocalRotation(quat);
	}
}
