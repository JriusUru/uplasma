/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "PythonPAKReader.h"

using namespace System;
using namespace UnityEngine;

ST::string PlImporter::PythonPAKReader::pythonDecompilerPath;

bool PlImporter::PythonPAKReader::read(hsStream* S)
{
	// code mostly copied from PlasmaShop

	uint32_t magic = S->readInt();
	if (magic != kMyst5Arc) {
		// Python.pak
		fEntries.resize(magic);
		fType = kPythonPak;
		for (size_t i = 0; i<fEntries.size(); i++) {
			fEntries[i].fName = S->readSafeStr();
			fEntries[i].fOffset = S->readInt();
		}

		for (size_t i = 0; i<fEntries.size(); i++) {
			if (fEntries[i].fOffset != S->pos())
				S->seek(fEntries[i].fOffset);
			uint32_t size = S->readInt();
			if (S->pos() + size > S->size()) {
				String warn("Warning: Pak file: Truncating last entry");
				Debug::Log(warn);
				size = S->size() - S->pos();
			}
			uint8_t* data = new uint8_t[size];
			S->read(size, data);
			fEntries[i].fData = FileBlob(data, size);
		}

		return true;
	}

	// not a PAK file
	return false;
}

bool PlImporter::PythonPAKReader::read(ST::string pakFilename)
{
	if (plEncryptedStream::IsFileEncrypted(pakFilename))
	{
		plEncryptedStream S(PlasmaVer::pvPrime);
		S.open(pakFilename, fmRead, plEncryptedStream::kEncAuto);
		if (S.getEncType() == plEncryptedStream::kEncDroid)
			// can't read droid without encryption key...
			return false;
		//else if (S.getEncType() == plEncryptedStream::kEncXtea) { }
		else if (S.getEncType() == plEncryptedStream::kEncAES)
			S.setVer(PlasmaVer::pvEoa);
		return read(&S);
	}
	else
	{
		hsFileStream S(PlasmaVer::pvMoul);
		S.open(pakFilename, fmRead);
		return read(&S);
	}
}

std::vector<ST::string> PlImporter::PythonPAKReader::getFileNames()
{
	std::vector<ST::string> filenames;
	for (FileEntry entry : fEntries)
		filenames.push_back(entry.fName);
	return filenames;
}




ST::string PlImporter::PythonPAKReader::uncompressData(const FileEntry & entry)
{
	// Let's try to keep it simple this time...
	// Decompiling Python: ideally we'd use pycdc as a library.
	// However pycdc is barely usable because of bad coding practice and Windoz sucking very much.
	// Drizzle works pretty well and the generated code is a bit cleaner, so might compile better. We'll use that for now.

	// TODO - fix pycdc so it can be used as library with zero file IO

	ST::string cachedFilePath = pythonCachePath + entry.fName + "c"; // "c" for ".pyc"
	ST::string cachedFilePathDecompiled = pythonCachePath + entry.fName;

	std::ifstream testStream(cachedFilePathDecompiled.c_str());
	if (testStream.is_open())
	{
		// output file already exists. Don't bother with decompiling it.
		std::stringstream buffer;
		buffer << testStream.rdbuf();
		ST::string resultT = buffer.str();
		return resultT;
	}
	testStream.close();

	// write pyc to disk (ugly but required for now)
	std::ofstream os(cachedFilePath.c_str(), std::ios::binary);
	size_t fsize = entry.fData.getSize();
	const uint8_t* data = entry.fData.getData();
	
	int PYC_MAGIC_22 = 0x0A0DED2D;
	int PYC_MAGIC_23 = 0x0A0DF23B;

	if (fsize > 9 && data[9] == 's')
		os.write((const char*) &PYC_MAGIC_22, 4);
	else if (fsize > 17 && data[17] == 's')
		os.write((const char*)&PYC_MAGIC_23, 4);
	else {
		String msg("Could not determine Python code blob version.  Assuming 2.3");
		Debug::LogError(msg);
		os.write((const char*)&PYC_MAGIC_23, 4);
	}
	// Timestamp...  Not saved, so just set it to zero
	os << 0 << 0 << 0 << 0;

	// data
	os.write((const char*)data, fsize);
	os.close(); // otherwise python decompiler won't be able to open it

	// call drizzle with input filename and output filepath
	//ST::string cmd = '"' + pythonDecompilerPath + "\" -decompilepyc \"" + cachedFilePath + "\" \"" + pythonCachePath + '"';
	ST::string cmd = pythonDecompilerPath + " -decompilepyc \"" + cachedFilePath + "\" \"" +
        pythonCachePath.substr(0, pythonCachePath.size()-1) + '"'; // remove the trailing '\', otherwise screws things up

	system(cmd.c_str());
	std::ifstream inputFile(cachedFilePathDecompiled.c_str());
	std::stringstream buffer;
	buffer << inputFile.rdbuf();
	ST::string resultT = buffer.str();

	std::ofstream out(cachedFilePathDecompiled.c_str());
	out << resultT.c_str();
	
	return resultT;
}
