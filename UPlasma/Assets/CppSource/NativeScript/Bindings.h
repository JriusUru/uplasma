/// <summary>
/// Declaration of the various .NET types exposed to C++
/// </summary>
/// <author>
/// Jackson Dunstan, 2017, http://JacksonDunstan.com
/// </author>
/// <license>
/// MIT
/// </license>

#pragma once

// For int32_t, etc.
#include <stdint.h>

// For size_t to support placement new and delete
#include <stdlib.h>

////////////////////////////////////////////////////////////////
// Plugin internals. Do not name these in game code as they may
// change without warning. For example:
//   // Good. Uses behavior, not names.
//   int x = myArray[5];
//   // Bad. Directly uses names.
//   ArrayElementProxy1_1 proxy = myArray[5];
//   int x = proxy;
////////////////////////////////////////////////////////////////

namespace Plugin
{
	enum struct InternalUse
	{
		Only
	};
	
	struct ManagedType
	{
		int32_t Handle;
		
		ManagedType();
		ManagedType(decltype(nullptr));
		ManagedType(InternalUse, int32_t handle);
	};
	
	template <typename TElement> struct ArrayElementProxy1_1;
	
	template <typename TElement> struct ArrayElementProxy1_2;
	template <typename TElement> struct ArrayElementProxy2_2;
	
	template <typename TElement> struct ArrayElementProxy1_3;
	template <typename TElement> struct ArrayElementProxy2_3;
	template <typename TElement> struct ArrayElementProxy3_3;
	
	template <typename TElement> struct ArrayElementProxy1_4;
	template <typename TElement> struct ArrayElementProxy2_4;
	template <typename TElement> struct ArrayElementProxy3_4;
	template <typename TElement> struct ArrayElementProxy4_4;
	
	template <typename TElement> struct ArrayElementProxy1_5;
	template <typename TElement> struct ArrayElementProxy2_5;
	template <typename TElement> struct ArrayElementProxy3_5;
	template <typename TElement> struct ArrayElementProxy4_5;
	template <typename TElement> struct ArrayElementProxy5_5;
}

////////////////////////////////////////////////////////////////
// C# basic types
////////////////////////////////////////////////////////////////

namespace System
{
	struct Object;
	struct ValueType;
	struct Enum;
	struct String;
	struct Array;
	template <typename TElement> struct Array1;
	template <typename TElement> struct Array2;
	template <typename TElement> struct Array3;
	template <typename TElement> struct Array4;
	template <typename TElement> struct Array5;
	struct IComparable;
	template <typename TElement> struct IComparable_1;
	template <typename TElement> struct IEquatable_1;
	struct IFormattable;
	struct IConvertible;
	
	// .NET booleans are four bytes long
	// This struct makes them feel like C++'s bool, int32_t, and uint32_t types
	struct Boolean
	{
		uint32_t Value;
		
		Boolean();
		Boolean(bool value);
		Boolean(int32_t value);
		Boolean(uint32_t value);
		operator bool() const;
		operator int32_t() const;
		operator uint32_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Boolean>() const;
		explicit operator IEquatable_1<Boolean>() const;
	};
	
	// .NET chars are two bytes long
	// This struct helps them interoperate with C++'s char and int16_t types
	struct Char
	{
		int16_t Value;
		
		Char();
		Char(char value);
		Char(int16_t value);
		operator int16_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Char>() const;
		explicit operator IEquatable_1<Char>() const;
	};
	
	struct SByte
	{
		int8_t Value;
		
		SByte();
		SByte(int8_t value);
		operator int8_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<SByte>() const;
		explicit operator IEquatable_1<SByte>() const;
	};
	
	struct Byte
	{
		uint8_t Value;
		
		Byte();
		Byte(uint8_t value);
		operator uint8_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Byte>() const;
		explicit operator IEquatable_1<Byte>() const;
	};
	
	struct Int16
	{
		int16_t Value;
		
		Int16();
		Int16(int16_t value);
		operator int16_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Int16>() const;
		explicit operator IEquatable_1<Int16>() const;
	};
	
	struct UInt16
	{
		uint16_t Value;
		
		UInt16();
		UInt16(uint16_t value);
		operator uint16_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<UInt16>() const;
		explicit operator IEquatable_1<UInt16>() const;
	};
	
	struct Int32
	{
		int32_t Value;
		
		Int32();
		Int32(int32_t value);
		operator int32_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Int32>() const;
		explicit operator IEquatable_1<Int32>() const;
	};
	
	struct UInt32
	{
		uint32_t Value;
		
		UInt32();
		UInt32(uint32_t value);
		operator uint32_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<UInt32>() const;
		explicit operator IEquatable_1<UInt32>() const;
	};
	
	struct Int64
	{
		int64_t Value;
		
		Int64();
		Int64(int64_t value);
		operator int64_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Int64>() const;
		explicit operator IEquatable_1<Int64>() const;
	};
	
	struct UInt64
	{
		uint64_t Value;
		
		UInt64();
		UInt64(uint64_t value);
		operator uint64_t() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<UInt64>() const;
		explicit operator IEquatable_1<UInt64>() const;
	};
	
	struct Single
	{
		float Value;
		
		Single();
		Single(float value);
		operator float() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Single>() const;
		explicit operator IEquatable_1<Single>() const;
	};
	
	struct Double
	{
		double Value;
		
		Double();
		Double(double value);
		operator double() const;
		explicit operator Object() const;
		explicit operator ValueType() const;
		explicit operator IComparable() const;
		explicit operator IFormattable() const;
		explicit operator IConvertible() const;
		explicit operator IComparable_1<Double>() const;
		explicit operator IEquatable_1<Double>() const;
	};
}

/*BEGIN TEMPLATE DECLARATIONS*/
namespace System
{
	template<typename TT0> struct IEquatable_1;
}

namespace System
{
	template<typename TT0> struct IComparable_1;
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<typename TT0> struct IKeyFrame_1;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<typename TT0> struct IController_1;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<typename TT0, typename TT1> struct LeafController_2;
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct IEnumerator_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct IEnumerable_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct ICollection_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct IReadOnlyCollection_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct IReadOnlyList_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct IList_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<typename TT0> struct List_1;
		}
	}
}
/*END TEMPLATE DECLARATIONS*/

/*BEGIN TYPE DECLARATIONS*/
namespace System
{
	struct IFormattable;
}

namespace System
{
	struct IConvertible;
}

namespace System
{
	struct IComparable;
}

namespace System
{
	struct Decimal;
}

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			namespace Marshal
			{
			}
		}
	}
}

namespace System
{
	namespace Runtime
	{
		namespace Serialization
		{
			struct IDeserializationCallback;
		}
	}
}

namespace System
{
	namespace Runtime
	{
		namespace Serialization
		{
			struct ISerializable;
		}
	}
}

namespace System
{
	struct MarshalByRefObject;
}

namespace System
{
	namespace IO
	{
		namespace File
		{
		}
	}
}

namespace System
{
	namespace IO
	{
		struct FileSystemInfo;
	}
}

namespace System
{
	namespace IO
	{
		struct DirectoryInfo;
	}
}

namespace System
{
	namespace IO
	{
		namespace Directory
		{
		}
	}
}

namespace UnityEngine
{
	struct TextureFormat;
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct TextureDimension;
	}
}

namespace UnityEngine
{
	struct FilterMode;
}

namespace UnityEngine
{
	struct TextureWrapMode;
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct LightProbeUsage;
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeUsage;
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeMode;
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeRefreshMode;
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeTimeSlicingMode;
	}
}

namespace UnityEngine
{
	struct Bounds;
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ShadowCastingMode;
	}
}

namespace UnityEngine
{
	struct Color32;
}

namespace UnityEngine
{
	struct Color;
}

namespace UnityEngine
{
	struct Vector2;
}

namespace UnityEngine
{
	struct Vector3;
}

namespace UnityEngine
{
	struct Vector4;
}

namespace UnityEngine
{
	struct Matrix4x4;
}

namespace UnityEngine
{
	struct Quaternion;
}

namespace UnityEngine
{
	struct LayerMask;
}

namespace UnityEngine
{
	struct Object;
}

namespace UnityEngine
{
	struct Component;
}

namespace UnityEngine
{
	struct AudioClip;
}

namespace UnityEngine
{
	struct Resources;
}

namespace UnityEngine
{
	struct Transform;
}

namespace System
{
	namespace Collections
	{
		struct IEnumerator;
	}
}

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			struct _Exception;
		}
	}
}

namespace UnityEngine
{
	struct GameObject;
}

namespace UnityEngine
{
	struct Debug;
}

namespace UnityEngine
{
	struct Behaviour;
}

namespace UnityEngine
{
	struct MonoBehaviour;
}

namespace UnityEngine
{
	struct AudioBehaviour;
}

namespace UnityEngine
{
	struct AudioSource;
}

namespace UnityEngine
{
	struct CollisionDetectionMode;
}

namespace UnityEngine
{
	struct Rigidbody;
}

namespace UnityEngine
{
	struct Renderer;
}

namespace UnityEngine
{
	struct MeshFilter;
}

namespace UnityEngine
{
	struct SkinnedMeshRenderer;
}

namespace UnityEngine
{
	struct ReflectionProbe;
}

namespace UnityEngine
{
	struct MeshRenderer;
}

namespace UnityEngine
{
	struct Shader;
}

namespace UnityEngine
{
	struct Texture;
}

namespace UnityEngine
{
	struct Texture2D;
}

namespace UnityEngine
{
	namespace ImageConversion
	{
	}
}

namespace UnityEngine
{
	struct CubemapFace;
}

namespace UnityEngine
{
	struct Cubemap;
}

namespace UnityEngine
{
	struct Material;
}

namespace UnityEngine
{
	struct BoneWeight;
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct IndexFormat;
	}
}

namespace UnityEngine
{
	struct Mesh;
}

namespace UnityEngine
{
	struct PhysicMaterial;
}

namespace UnityEngine
{
	struct Collider;
}

namespace UnityEngine
{
	struct BoxCollider;
}

namespace UnityEngine
{
	struct SphereCollider;
}

namespace UnityEngine
{
	struct CapsuleCollider;
}

namespace UnityEngine
{
	struct MeshCollider;
}

namespace UnityEngine
{
	struct MeshColliderCookingOptions;
}

namespace UnityEngine
{
	struct PhysicMaterialCombine;
}

namespace UnityEngine
{
	struct Light;
}

namespace UnityEngine
{
	struct Projector;
}

namespace UnityEngine
{
	struct LightType;
}

namespace UnityEngine
{
	struct Application;
}

namespace System
{
	namespace Text
	{
		struct StringBuilder;
	}
}

namespace UnityEngine
{
	struct LightShadows;
}

namespace UnityEngine
{
	struct Animation;
}

namespace System
{
	struct Exception;
}

namespace System
{
	struct SystemException;
}

namespace System
{
	struct NullReferenceException;
}

namespace UnityEngine
{
	struct PrimitiveType;
}

namespace UnityEngine
{
	struct Time;
}

namespace UnityEngine
{
	struct StaticBatchingUtility;
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlasmaManager;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlGameFileAccessor;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AbstractBasePlasmaNativeManager;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct BasePlasmaNativeManager;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlasmaGame;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct HackReturnObject;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlConsole;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAge;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPage;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ICannotBeBatched;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct IPlMessageable;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSceneObject;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCameraModifier;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEAXSourceSoftSettings;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEAXSourceSettings;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFadeParams;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlWAVHeader;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSoundBuffer;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSound;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCluster;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLodDist;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlClusterGroup;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAudible;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct Point3Key;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ScalarKey;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct QuatKey;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct Matrix44Key;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TriScalarToPoint3Controller;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TriScalarToQuatController;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TrsController;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct Point3Controller;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ScalarController;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct QuatController;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AffineParts;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct IAgChannel;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PointControllerChannel;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ScalarControllerChannel;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TrsControllerChannel;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct IAgApplicator;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct MatrixChannelApplicator;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct LightDiffuseApplicator;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ParticlePpsApplicator;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AgAnim;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AgeGlobalAnim;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AtcAnim;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgMasterMod;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgModifier;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLayerAnimation;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlExcludeRegionModifier;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlOneShotMod;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSittingModifier;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlRandomSoundModGroup;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlRandomSoundMod;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPythonFileMod;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMessage;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMessageWithCallbacks;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlNotifyMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCameraConfig;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCameraMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAnimCmdMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlArmatureEffectStateMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEventCallbackMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlExcludeRegionMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSimSuppressMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlUuid;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSpawnPointInfo;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgeInfoStruct;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgeLinkStruct;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgeLinkEffects;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLinkToAgeMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlOneShotMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlTimerCallbackMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEnableMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSoundMsg;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlOneShotCallback;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlViewFaceModifier;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFadeOpacityMod;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLogicModifier;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlDetector;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlObjectInVolumeDetector;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPanicLinkRegion;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPickingDetector;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlConditionalObject;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlActivatorConditionalObject;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFacingConditionalObject;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlObjectInBoxConditionalObject;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlVolumeSensorConditionalObject;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlResponder;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlResponderState;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlResponderCommand;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMessageForwarder;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMaintainersMarkerModifier;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlKey;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AssetsLifeBinder;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlArmatureMod;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct WaterStateParams;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct WaterState;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFixedWaterState7;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlWaveSet7;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct MatOverride;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct MeshRendererOverride;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct LightOverride;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ObjOverride;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PrpImportClues;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct NativeHelpers;
	}
}

namespace System
{
	struct IDisposable;
}

namespace System
{
	namespace Collections
	{
		struct IStructuralEquatable;
	}
}

namespace System
{
	namespace Collections
	{
		struct IStructuralComparable;
	}
}
/*END TYPE DECLARATIONS*/

/*BEGIN TEMPLATE SPECIALIZATION DECLARATIONS*/
namespace System
{
	template<> struct IEquatable_1<System::Boolean>;
}

namespace System
{
	template<> struct IEquatable_1<System::Char>;
}

namespace System
{
	template<> struct IEquatable_1<System::SByte>;
}

namespace System
{
	template<> struct IEquatable_1<System::Byte>;
}

namespace System
{
	template<> struct IEquatable_1<System::Int16>;
}

namespace System
{
	template<> struct IEquatable_1<System::UInt16>;
}

namespace System
{
	template<> struct IEquatable_1<System::Int32>;
}

namespace System
{
	template<> struct IEquatable_1<System::UInt32>;
}

namespace System
{
	template<> struct IEquatable_1<System::Int64>;
}

namespace System
{
	template<> struct IEquatable_1<System::UInt64>;
}

namespace System
{
	template<> struct IEquatable_1<System::Single>;
}

namespace System
{
	template<> struct IEquatable_1<System::Double>;
}

namespace System
{
	template<> struct IEquatable_1<System::Decimal>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Bounds>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Color>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Vector2>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Vector3>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Vector4>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Matrix4x4>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Quaternion>;
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::BoneWeight>;
}

namespace System
{
	template<> struct IComparable_1<System::Boolean>;
}

namespace System
{
	template<> struct IComparable_1<System::Char>;
}

namespace System
{
	template<> struct IComparable_1<System::SByte>;
}

namespace System
{
	template<> struct IComparable_1<System::Byte>;
}

namespace System
{
	template<> struct IComparable_1<System::Int16>;
}

namespace System
{
	template<> struct IComparable_1<System::UInt16>;
}

namespace System
{
	template<> struct IComparable_1<System::Int32>;
}

namespace System
{
	template<> struct IComparable_1<System::UInt32>;
}

namespace System
{
	template<> struct IComparable_1<System::Int64>;
}

namespace System
{
	template<> struct IComparable_1<System::UInt64>;
}

namespace System
{
	template<> struct IComparable_1<System::Single>;
}

namespace System
{
	template<> struct IComparable_1<System::Double>;
}

namespace System
{
	template<> struct IComparable_1<System::Decimal>;
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<UnityEngine::Matrix4x4>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<UnityEngine::Vector3>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<UnityEngine::Quaternion>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<System::Single>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<System::Single>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<UnityEngine::Vector3>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<UnityEngine::Quaternion>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<UPlasma::PlEmu::AffineParts>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>;
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>;
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::GameObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Transform>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::MeshFilter>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Material>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Texture2D>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Cubemap>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Matrix4x4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::IPlMessageable>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlDetector>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlMessage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlPage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PrpImportClues>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlCluster>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::AgAnim>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Color>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Color32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Vector2>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Vector3>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Vector4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::BoneWeight>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Quaternion>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlResponderState>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlSound>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::IAgApplicator>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Single>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Int32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Byte>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::String>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::GameObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Transform>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::MeshFilter>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Material>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Texture2D>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Cubemap>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Matrix4x4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::IPlMessageable>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlDetector>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlMessage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlPage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PrpImportClues>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlCluster>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::AgAnim>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Color>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Color32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Vector2>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Vector3>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Vector4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::BoneWeight>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Quaternion>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlResponderState>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlSound>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::IAgApplicator>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Single>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Int32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Byte>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::String>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::GameObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Transform>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::MeshFilter>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Material>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Texture2D>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Cubemap>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Matrix4x4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::IPlMessageable>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlConditionalObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlDetector>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlMessage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlOneShotCallback>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlPage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PrpImportClues>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlCluster>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::AgAnim>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Color>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Color32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Vector2>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Vector3>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Vector4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::BoneWeight>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Quaternion>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlResponderState>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlResponderCommand>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlSound>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::IAgApplicator>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Single>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Int32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Byte>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::String>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::GameObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Transform>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::MeshFilter>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Material>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Texture2D>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Cubemap>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Matrix4x4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Color>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Color32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Vector2>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Vector3>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Vector4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::BoneWeight>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Quaternion>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Single>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Int32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Byte>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::String>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::GameObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Transform>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::MeshFilter>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Material>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Texture2D>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Cubemap>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Matrix4x4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlDetector>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlMessage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlPage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlCluster>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::AgAnim>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Color>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Color32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Vector2>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Vector3>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Vector4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::BoneWeight>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Quaternion>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlSound>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Single>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Int32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Byte>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::String>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::GameObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Transform>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::MeshFilter>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Material>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Texture2D>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Cubemap>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Matrix4x4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::IPlMessageable>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlConditionalObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlDetector>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlMessage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlOneShotCallback>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlPage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PrpImportClues>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlCluster>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::AgAnim>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Color>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Color32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Vector2>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Vector3>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Vector4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::BoneWeight>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Quaternion>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlResponderState>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlResponderCommand>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlSound>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::IAgApplicator>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Single>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Int32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Byte>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::String>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<System::Int32>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<System::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Object>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Material>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Texture2D>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Cubemap>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Matrix4x4>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::IPlMessageable>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlConditionalObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlDetector>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlMessage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlOneShotCallback>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlRandomSoundModGroup>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::GameObject>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Transform>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlPage>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PrpImportClues>;
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlCluster>;
		}
	}
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Object>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Object>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::GameObject>;
}

namespace System
{
	template<> struct Array1<UnityEngine::GameObject>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Transform>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Transform>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::MeshFilter>;
}

namespace System
{
	template<> struct Array1<UnityEngine::MeshFilter>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Material>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Material>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Texture2D>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Texture2D>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Cubemap>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Cubemap>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Matrix4x4>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Matrix4x4>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Color>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Color>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Color32>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Color32>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Vector2>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Vector2>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Vector3>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Vector3>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Vector4>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Vector4>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::BoneWeight>;
}

namespace System
{
	template<> struct Array1<UnityEngine::BoneWeight>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Quaternion>;
}

namespace System
{
	template<> struct Array1<UnityEngine::Quaternion>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderState>;
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::PlResponderState>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderCommand>;
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::PlResponderCommand>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::PlSound>;
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::PlSound>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::IAgApplicator>;
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::IAgApplicator>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::AgAnim>;
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::AgAnim>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::Single>;
}

namespace System
{
	template<> struct Array1<System::Single>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::Int32>;
}

namespace System
{
	template<> struct Array1<System::Int32>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::Byte>;
}

namespace System
{
	template<> struct Array1<System::Byte>;
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::String>;
}

namespace System
{
	template<> struct Array1<System::String>;
}
/*END TEMPLATE SPECIALIZATION DECLARATIONS*/

////////////////////////////////////////////////////////////////
// C# type definitions
////////////////////////////////////////////////////////////////

namespace System
{
	struct Object : Plugin::ManagedType
	{
		Object();
		Object(Plugin::InternalUse iu, int32_t handle);
		Object(decltype(nullptr));
		virtual ~Object() = default;
		bool operator==(decltype(nullptr)) const;
		bool operator!=(decltype(nullptr)) const;
		virtual void ThrowReferenceToThis();
		
		/*BEGIN UNBOXING METHOD DECLARATIONS*/
		explicit operator System::Decimal();
		explicit operator UnityEngine::TextureFormat();
		explicit operator UnityEngine::Rendering::TextureDimension();
		explicit operator UnityEngine::FilterMode();
		explicit operator UnityEngine::TextureWrapMode();
		explicit operator UnityEngine::Rendering::LightProbeUsage();
		explicit operator UnityEngine::Rendering::ReflectionProbeUsage();
		explicit operator UnityEngine::Rendering::ReflectionProbeMode();
		explicit operator UnityEngine::Rendering::ReflectionProbeRefreshMode();
		explicit operator UnityEngine::Rendering::ReflectionProbeTimeSlicingMode();
		explicit operator UnityEngine::Bounds();
		explicit operator UnityEngine::Rendering::ShadowCastingMode();
		explicit operator UnityEngine::Color32();
		explicit operator UnityEngine::Color();
		explicit operator UnityEngine::Vector2();
		explicit operator UnityEngine::Vector3();
		explicit operator UnityEngine::Vector4();
		explicit operator UnityEngine::Matrix4x4();
		explicit operator UnityEngine::Quaternion();
		explicit operator UnityEngine::LayerMask();
		explicit operator UnityEngine::CollisionDetectionMode();
		explicit operator UnityEngine::CubemapFace();
		explicit operator UnityEngine::BoneWeight();
		explicit operator UnityEngine::Rendering::IndexFormat();
		explicit operator UnityEngine::MeshColliderCookingOptions();
		explicit operator UnityEngine::PhysicMaterialCombine();
		explicit operator UnityEngine::LightType();
		explicit operator UnityEngine::LightShadows();
		explicit operator UnityEngine::PrimitiveType();
		explicit operator UPlasma::PlEmu::PlasmaGame();
		explicit operator UPlasma::PlEmu::PlLodDist();
		explicit operator System::Boolean();
		explicit operator System::SByte();
		explicit operator System::Byte();
		explicit operator System::Int16();
		explicit operator System::UInt16();
		explicit operator System::Int32();
		explicit operator System::UInt32();
		explicit operator System::Int64();
		explicit operator System::UInt64();
		explicit operator System::Char();
		explicit operator System::Single();
		explicit operator System::Double();
		/*END UNBOXING METHOD DECLARATIONS*/
	};
	
	struct ValueType : virtual Object
	{
		ValueType(Plugin::InternalUse iu, int32_t handle);
		ValueType(decltype(nullptr));
	};
	
	struct Enum : virtual ValueType
	{
		Enum(Plugin::InternalUse iu, int32_t handle);
		Enum(decltype(nullptr));
	};
	
	struct String : virtual Object
	{
		String(Plugin::InternalUse iu, int32_t handle);
		String(decltype(nullptr));
		String(const String& other);
		String(String&& other);
		virtual ~String();
		String& operator=(const String& other);
		String& operator=(decltype(nullptr));
		String& operator=(String&& other);
		String(const char* chars);
	};
	
	struct ICloneable : virtual Object
	{
		ICloneable(Plugin::InternalUse iu, int32_t handle);
		ICloneable(decltype(nullptr));
	};
	
	namespace Collections
	{
		struct IEnumerable : virtual Object
		{
			IEnumerable(Plugin::InternalUse iu, int32_t handle);
			IEnumerable(decltype(nullptr));
			IEnumerator GetEnumerator();
		};
		
		struct ICollection : virtual IEnumerable
		{
			ICollection(Plugin::InternalUse iu, int32_t handle);
			ICollection(decltype(nullptr));
		};
		
		struct IList : virtual ICollection, virtual IEnumerable
		{
			IList(Plugin::InternalUse iu, int32_t handle);
			IList(decltype(nullptr));
		};
	}
	
	struct Array : virtual ICloneable, virtual Collections::IList
	{
		Array(Plugin::InternalUse iu, int32_t handle);
		Array(decltype(nullptr));
		int32_t GetLength();
		int32_t GetRank();
	};
}

////////////////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////////////////

namespace Plugin
{
	extern System::String NullString;
}

/*BEGIN TYPE DEFINITIONS*/
namespace System
{
	struct IFormattable : virtual System::Object
	{
		IFormattable(decltype(nullptr));
		IFormattable(Plugin::InternalUse, int32_t handle);
		IFormattable(const IFormattable& other);
		IFormattable(IFormattable&& other);
		virtual ~IFormattable();
		IFormattable& operator=(const IFormattable& other);
		IFormattable& operator=(decltype(nullptr));
		IFormattable& operator=(IFormattable&& other);
		bool operator==(const IFormattable& other) const;
		bool operator!=(const IFormattable& other) const;
	};
}

namespace System
{
	struct IConvertible : virtual System::Object
	{
		IConvertible(decltype(nullptr));
		IConvertible(Plugin::InternalUse, int32_t handle);
		IConvertible(const IConvertible& other);
		IConvertible(IConvertible&& other);
		virtual ~IConvertible();
		IConvertible& operator=(const IConvertible& other);
		IConvertible& operator=(decltype(nullptr));
		IConvertible& operator=(IConvertible&& other);
		bool operator==(const IConvertible& other) const;
		bool operator!=(const IConvertible& other) const;
	};
}

namespace System
{
	struct IComparable : virtual System::Object
	{
		IComparable(decltype(nullptr));
		IComparable(Plugin::InternalUse, int32_t handle);
		IComparable(const IComparable& other);
		IComparable(IComparable&& other);
		virtual ~IComparable();
		IComparable& operator=(const IComparable& other);
		IComparable& operator=(decltype(nullptr));
		IComparable& operator=(IComparable&& other);
		bool operator==(const IComparable& other) const;
		bool operator!=(const IComparable& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Boolean> : virtual System::Object
	{
		IEquatable_1<System::Boolean>(decltype(nullptr));
		IEquatable_1<System::Boolean>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Boolean>(const IEquatable_1<System::Boolean>& other);
		IEquatable_1<System::Boolean>(IEquatable_1<System::Boolean>&& other);
		virtual ~IEquatable_1<System::Boolean>();
		IEquatable_1<System::Boolean>& operator=(const IEquatable_1<System::Boolean>& other);
		IEquatable_1<System::Boolean>& operator=(decltype(nullptr));
		IEquatable_1<System::Boolean>& operator=(IEquatable_1<System::Boolean>&& other);
		bool operator==(const IEquatable_1<System::Boolean>& other) const;
		bool operator!=(const IEquatable_1<System::Boolean>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Char> : virtual System::Object
	{
		IEquatable_1<System::Char>(decltype(nullptr));
		IEquatable_1<System::Char>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Char>(const IEquatable_1<System::Char>& other);
		IEquatable_1<System::Char>(IEquatable_1<System::Char>&& other);
		virtual ~IEquatable_1<System::Char>();
		IEquatable_1<System::Char>& operator=(const IEquatable_1<System::Char>& other);
		IEquatable_1<System::Char>& operator=(decltype(nullptr));
		IEquatable_1<System::Char>& operator=(IEquatable_1<System::Char>&& other);
		bool operator==(const IEquatable_1<System::Char>& other) const;
		bool operator!=(const IEquatable_1<System::Char>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::SByte> : virtual System::Object
	{
		IEquatable_1<System::SByte>(decltype(nullptr));
		IEquatable_1<System::SByte>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::SByte>(const IEquatable_1<System::SByte>& other);
		IEquatable_1<System::SByte>(IEquatable_1<System::SByte>&& other);
		virtual ~IEquatable_1<System::SByte>();
		IEquatable_1<System::SByte>& operator=(const IEquatable_1<System::SByte>& other);
		IEquatable_1<System::SByte>& operator=(decltype(nullptr));
		IEquatable_1<System::SByte>& operator=(IEquatable_1<System::SByte>&& other);
		bool operator==(const IEquatable_1<System::SByte>& other) const;
		bool operator!=(const IEquatable_1<System::SByte>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Byte> : virtual System::Object
	{
		IEquatable_1<System::Byte>(decltype(nullptr));
		IEquatable_1<System::Byte>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Byte>(const IEquatable_1<System::Byte>& other);
		IEquatable_1<System::Byte>(IEquatable_1<System::Byte>&& other);
		virtual ~IEquatable_1<System::Byte>();
		IEquatable_1<System::Byte>& operator=(const IEquatable_1<System::Byte>& other);
		IEquatable_1<System::Byte>& operator=(decltype(nullptr));
		IEquatable_1<System::Byte>& operator=(IEquatable_1<System::Byte>&& other);
		bool operator==(const IEquatable_1<System::Byte>& other) const;
		bool operator!=(const IEquatable_1<System::Byte>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Int16> : virtual System::Object
	{
		IEquatable_1<System::Int16>(decltype(nullptr));
		IEquatable_1<System::Int16>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Int16>(const IEquatable_1<System::Int16>& other);
		IEquatable_1<System::Int16>(IEquatable_1<System::Int16>&& other);
		virtual ~IEquatable_1<System::Int16>();
		IEquatable_1<System::Int16>& operator=(const IEquatable_1<System::Int16>& other);
		IEquatable_1<System::Int16>& operator=(decltype(nullptr));
		IEquatable_1<System::Int16>& operator=(IEquatable_1<System::Int16>&& other);
		bool operator==(const IEquatable_1<System::Int16>& other) const;
		bool operator!=(const IEquatable_1<System::Int16>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::UInt16> : virtual System::Object
	{
		IEquatable_1<System::UInt16>(decltype(nullptr));
		IEquatable_1<System::UInt16>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::UInt16>(const IEquatable_1<System::UInt16>& other);
		IEquatable_1<System::UInt16>(IEquatable_1<System::UInt16>&& other);
		virtual ~IEquatable_1<System::UInt16>();
		IEquatable_1<System::UInt16>& operator=(const IEquatable_1<System::UInt16>& other);
		IEquatable_1<System::UInt16>& operator=(decltype(nullptr));
		IEquatable_1<System::UInt16>& operator=(IEquatable_1<System::UInt16>&& other);
		bool operator==(const IEquatable_1<System::UInt16>& other) const;
		bool operator!=(const IEquatable_1<System::UInt16>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Int32> : virtual System::Object
	{
		IEquatable_1<System::Int32>(decltype(nullptr));
		IEquatable_1<System::Int32>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Int32>(const IEquatable_1<System::Int32>& other);
		IEquatable_1<System::Int32>(IEquatable_1<System::Int32>&& other);
		virtual ~IEquatable_1<System::Int32>();
		IEquatable_1<System::Int32>& operator=(const IEquatable_1<System::Int32>& other);
		IEquatable_1<System::Int32>& operator=(decltype(nullptr));
		IEquatable_1<System::Int32>& operator=(IEquatable_1<System::Int32>&& other);
		bool operator==(const IEquatable_1<System::Int32>& other) const;
		bool operator!=(const IEquatable_1<System::Int32>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::UInt32> : virtual System::Object
	{
		IEquatable_1<System::UInt32>(decltype(nullptr));
		IEquatable_1<System::UInt32>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::UInt32>(const IEquatable_1<System::UInt32>& other);
		IEquatable_1<System::UInt32>(IEquatable_1<System::UInt32>&& other);
		virtual ~IEquatable_1<System::UInt32>();
		IEquatable_1<System::UInt32>& operator=(const IEquatable_1<System::UInt32>& other);
		IEquatable_1<System::UInt32>& operator=(decltype(nullptr));
		IEquatable_1<System::UInt32>& operator=(IEquatable_1<System::UInt32>&& other);
		bool operator==(const IEquatable_1<System::UInt32>& other) const;
		bool operator!=(const IEquatable_1<System::UInt32>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Int64> : virtual System::Object
	{
		IEquatable_1<System::Int64>(decltype(nullptr));
		IEquatable_1<System::Int64>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Int64>(const IEquatable_1<System::Int64>& other);
		IEquatable_1<System::Int64>(IEquatable_1<System::Int64>&& other);
		virtual ~IEquatable_1<System::Int64>();
		IEquatable_1<System::Int64>& operator=(const IEquatable_1<System::Int64>& other);
		IEquatable_1<System::Int64>& operator=(decltype(nullptr));
		IEquatable_1<System::Int64>& operator=(IEquatable_1<System::Int64>&& other);
		bool operator==(const IEquatable_1<System::Int64>& other) const;
		bool operator!=(const IEquatable_1<System::Int64>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::UInt64> : virtual System::Object
	{
		IEquatable_1<System::UInt64>(decltype(nullptr));
		IEquatable_1<System::UInt64>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::UInt64>(const IEquatable_1<System::UInt64>& other);
		IEquatable_1<System::UInt64>(IEquatable_1<System::UInt64>&& other);
		virtual ~IEquatable_1<System::UInt64>();
		IEquatable_1<System::UInt64>& operator=(const IEquatable_1<System::UInt64>& other);
		IEquatable_1<System::UInt64>& operator=(decltype(nullptr));
		IEquatable_1<System::UInt64>& operator=(IEquatable_1<System::UInt64>&& other);
		bool operator==(const IEquatable_1<System::UInt64>& other) const;
		bool operator!=(const IEquatable_1<System::UInt64>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Single> : virtual System::Object
	{
		IEquatable_1<System::Single>(decltype(nullptr));
		IEquatable_1<System::Single>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Single>(const IEquatable_1<System::Single>& other);
		IEquatable_1<System::Single>(IEquatable_1<System::Single>&& other);
		virtual ~IEquatable_1<System::Single>();
		IEquatable_1<System::Single>& operator=(const IEquatable_1<System::Single>& other);
		IEquatable_1<System::Single>& operator=(decltype(nullptr));
		IEquatable_1<System::Single>& operator=(IEquatable_1<System::Single>&& other);
		bool operator==(const IEquatable_1<System::Single>& other) const;
		bool operator!=(const IEquatable_1<System::Single>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Double> : virtual System::Object
	{
		IEquatable_1<System::Double>(decltype(nullptr));
		IEquatable_1<System::Double>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Double>(const IEquatable_1<System::Double>& other);
		IEquatable_1<System::Double>(IEquatable_1<System::Double>&& other);
		virtual ~IEquatable_1<System::Double>();
		IEquatable_1<System::Double>& operator=(const IEquatable_1<System::Double>& other);
		IEquatable_1<System::Double>& operator=(decltype(nullptr));
		IEquatable_1<System::Double>& operator=(IEquatable_1<System::Double>&& other);
		bool operator==(const IEquatable_1<System::Double>& other) const;
		bool operator!=(const IEquatable_1<System::Double>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<System::Decimal> : virtual System::Object
	{
		IEquatable_1<System::Decimal>(decltype(nullptr));
		IEquatable_1<System::Decimal>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<System::Decimal>(const IEquatable_1<System::Decimal>& other);
		IEquatable_1<System::Decimal>(IEquatable_1<System::Decimal>&& other);
		virtual ~IEquatable_1<System::Decimal>();
		IEquatable_1<System::Decimal>& operator=(const IEquatable_1<System::Decimal>& other);
		IEquatable_1<System::Decimal>& operator=(decltype(nullptr));
		IEquatable_1<System::Decimal>& operator=(IEquatable_1<System::Decimal>&& other);
		bool operator==(const IEquatable_1<System::Decimal>& other) const;
		bool operator!=(const IEquatable_1<System::Decimal>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Bounds> : virtual System::Object
	{
		IEquatable_1<UnityEngine::Bounds>(decltype(nullptr));
		IEquatable_1<UnityEngine::Bounds>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::Bounds>(const IEquatable_1<UnityEngine::Bounds>& other);
		IEquatable_1<UnityEngine::Bounds>(IEquatable_1<UnityEngine::Bounds>&& other);
		virtual ~IEquatable_1<UnityEngine::Bounds>();
		IEquatable_1<UnityEngine::Bounds>& operator=(const IEquatable_1<UnityEngine::Bounds>& other);
		IEquatable_1<UnityEngine::Bounds>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::Bounds>& operator=(IEquatable_1<UnityEngine::Bounds>&& other);
		bool operator==(const IEquatable_1<UnityEngine::Bounds>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::Bounds>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Color> : virtual System::Object
	{
		IEquatable_1<UnityEngine::Color>(decltype(nullptr));
		IEquatable_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::Color>(const IEquatable_1<UnityEngine::Color>& other);
		IEquatable_1<UnityEngine::Color>(IEquatable_1<UnityEngine::Color>&& other);
		virtual ~IEquatable_1<UnityEngine::Color>();
		IEquatable_1<UnityEngine::Color>& operator=(const IEquatable_1<UnityEngine::Color>& other);
		IEquatable_1<UnityEngine::Color>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::Color>& operator=(IEquatable_1<UnityEngine::Color>&& other);
		bool operator==(const IEquatable_1<UnityEngine::Color>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::Color>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Vector2> : virtual System::Object
	{
		IEquatable_1<UnityEngine::Vector2>(decltype(nullptr));
		IEquatable_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::Vector2>(const IEquatable_1<UnityEngine::Vector2>& other);
		IEquatable_1<UnityEngine::Vector2>(IEquatable_1<UnityEngine::Vector2>&& other);
		virtual ~IEquatable_1<UnityEngine::Vector2>();
		IEquatable_1<UnityEngine::Vector2>& operator=(const IEquatable_1<UnityEngine::Vector2>& other);
		IEquatable_1<UnityEngine::Vector2>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::Vector2>& operator=(IEquatable_1<UnityEngine::Vector2>&& other);
		bool operator==(const IEquatable_1<UnityEngine::Vector2>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::Vector2>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Vector3> : virtual System::Object
	{
		IEquatable_1<UnityEngine::Vector3>(decltype(nullptr));
		IEquatable_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::Vector3>(const IEquatable_1<UnityEngine::Vector3>& other);
		IEquatable_1<UnityEngine::Vector3>(IEquatable_1<UnityEngine::Vector3>&& other);
		virtual ~IEquatable_1<UnityEngine::Vector3>();
		IEquatable_1<UnityEngine::Vector3>& operator=(const IEquatable_1<UnityEngine::Vector3>& other);
		IEquatable_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::Vector3>& operator=(IEquatable_1<UnityEngine::Vector3>&& other);
		bool operator==(const IEquatable_1<UnityEngine::Vector3>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::Vector3>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Vector4> : virtual System::Object
	{
		IEquatable_1<UnityEngine::Vector4>(decltype(nullptr));
		IEquatable_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::Vector4>(const IEquatable_1<UnityEngine::Vector4>& other);
		IEquatable_1<UnityEngine::Vector4>(IEquatable_1<UnityEngine::Vector4>&& other);
		virtual ~IEquatable_1<UnityEngine::Vector4>();
		IEquatable_1<UnityEngine::Vector4>& operator=(const IEquatable_1<UnityEngine::Vector4>& other);
		IEquatable_1<UnityEngine::Vector4>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::Vector4>& operator=(IEquatable_1<UnityEngine::Vector4>&& other);
		bool operator==(const IEquatable_1<UnityEngine::Vector4>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::Vector4>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Matrix4x4> : virtual System::Object
	{
		IEquatable_1<UnityEngine::Matrix4x4>(decltype(nullptr));
		IEquatable_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::Matrix4x4>(const IEquatable_1<UnityEngine::Matrix4x4>& other);
		IEquatable_1<UnityEngine::Matrix4x4>(IEquatable_1<UnityEngine::Matrix4x4>&& other);
		virtual ~IEquatable_1<UnityEngine::Matrix4x4>();
		IEquatable_1<UnityEngine::Matrix4x4>& operator=(const IEquatable_1<UnityEngine::Matrix4x4>& other);
		IEquatable_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::Matrix4x4>& operator=(IEquatable_1<UnityEngine::Matrix4x4>&& other);
		bool operator==(const IEquatable_1<UnityEngine::Matrix4x4>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::Matrix4x4>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::Quaternion> : virtual System::Object
	{
		IEquatable_1<UnityEngine::Quaternion>(decltype(nullptr));
		IEquatable_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::Quaternion>(const IEquatable_1<UnityEngine::Quaternion>& other);
		IEquatable_1<UnityEngine::Quaternion>(IEquatable_1<UnityEngine::Quaternion>&& other);
		virtual ~IEquatable_1<UnityEngine::Quaternion>();
		IEquatable_1<UnityEngine::Quaternion>& operator=(const IEquatable_1<UnityEngine::Quaternion>& other);
		IEquatable_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::Quaternion>& operator=(IEquatable_1<UnityEngine::Quaternion>&& other);
		bool operator==(const IEquatable_1<UnityEngine::Quaternion>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::Quaternion>& other) const;
	};
}

namespace System
{
	template<> struct IEquatable_1<UnityEngine::BoneWeight> : virtual System::Object
	{
		IEquatable_1<UnityEngine::BoneWeight>(decltype(nullptr));
		IEquatable_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
		IEquatable_1<UnityEngine::BoneWeight>(const IEquatable_1<UnityEngine::BoneWeight>& other);
		IEquatable_1<UnityEngine::BoneWeight>(IEquatable_1<UnityEngine::BoneWeight>&& other);
		virtual ~IEquatable_1<UnityEngine::BoneWeight>();
		IEquatable_1<UnityEngine::BoneWeight>& operator=(const IEquatable_1<UnityEngine::BoneWeight>& other);
		IEquatable_1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
		IEquatable_1<UnityEngine::BoneWeight>& operator=(IEquatable_1<UnityEngine::BoneWeight>&& other);
		bool operator==(const IEquatable_1<UnityEngine::BoneWeight>& other) const;
		bool operator!=(const IEquatable_1<UnityEngine::BoneWeight>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Boolean> : virtual System::Object
	{
		IComparable_1<System::Boolean>(decltype(nullptr));
		IComparable_1<System::Boolean>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Boolean>(const IComparable_1<System::Boolean>& other);
		IComparable_1<System::Boolean>(IComparable_1<System::Boolean>&& other);
		virtual ~IComparable_1<System::Boolean>();
		IComparable_1<System::Boolean>& operator=(const IComparable_1<System::Boolean>& other);
		IComparable_1<System::Boolean>& operator=(decltype(nullptr));
		IComparable_1<System::Boolean>& operator=(IComparable_1<System::Boolean>&& other);
		bool operator==(const IComparable_1<System::Boolean>& other) const;
		bool operator!=(const IComparable_1<System::Boolean>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Char> : virtual System::Object
	{
		IComparable_1<System::Char>(decltype(nullptr));
		IComparable_1<System::Char>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Char>(const IComparable_1<System::Char>& other);
		IComparable_1<System::Char>(IComparable_1<System::Char>&& other);
		virtual ~IComparable_1<System::Char>();
		IComparable_1<System::Char>& operator=(const IComparable_1<System::Char>& other);
		IComparable_1<System::Char>& operator=(decltype(nullptr));
		IComparable_1<System::Char>& operator=(IComparable_1<System::Char>&& other);
		bool operator==(const IComparable_1<System::Char>& other) const;
		bool operator!=(const IComparable_1<System::Char>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::SByte> : virtual System::Object
	{
		IComparable_1<System::SByte>(decltype(nullptr));
		IComparable_1<System::SByte>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::SByte>(const IComparable_1<System::SByte>& other);
		IComparable_1<System::SByte>(IComparable_1<System::SByte>&& other);
		virtual ~IComparable_1<System::SByte>();
		IComparable_1<System::SByte>& operator=(const IComparable_1<System::SByte>& other);
		IComparable_1<System::SByte>& operator=(decltype(nullptr));
		IComparable_1<System::SByte>& operator=(IComparable_1<System::SByte>&& other);
		bool operator==(const IComparable_1<System::SByte>& other) const;
		bool operator!=(const IComparable_1<System::SByte>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Byte> : virtual System::Object
	{
		IComparable_1<System::Byte>(decltype(nullptr));
		IComparable_1<System::Byte>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Byte>(const IComparable_1<System::Byte>& other);
		IComparable_1<System::Byte>(IComparable_1<System::Byte>&& other);
		virtual ~IComparable_1<System::Byte>();
		IComparable_1<System::Byte>& operator=(const IComparable_1<System::Byte>& other);
		IComparable_1<System::Byte>& operator=(decltype(nullptr));
		IComparable_1<System::Byte>& operator=(IComparable_1<System::Byte>&& other);
		bool operator==(const IComparable_1<System::Byte>& other) const;
		bool operator!=(const IComparable_1<System::Byte>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Int16> : virtual System::Object
	{
		IComparable_1<System::Int16>(decltype(nullptr));
		IComparable_1<System::Int16>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Int16>(const IComparable_1<System::Int16>& other);
		IComparable_1<System::Int16>(IComparable_1<System::Int16>&& other);
		virtual ~IComparable_1<System::Int16>();
		IComparable_1<System::Int16>& operator=(const IComparable_1<System::Int16>& other);
		IComparable_1<System::Int16>& operator=(decltype(nullptr));
		IComparable_1<System::Int16>& operator=(IComparable_1<System::Int16>&& other);
		bool operator==(const IComparable_1<System::Int16>& other) const;
		bool operator!=(const IComparable_1<System::Int16>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::UInt16> : virtual System::Object
	{
		IComparable_1<System::UInt16>(decltype(nullptr));
		IComparable_1<System::UInt16>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::UInt16>(const IComparable_1<System::UInt16>& other);
		IComparable_1<System::UInt16>(IComparable_1<System::UInt16>&& other);
		virtual ~IComparable_1<System::UInt16>();
		IComparable_1<System::UInt16>& operator=(const IComparable_1<System::UInt16>& other);
		IComparable_1<System::UInt16>& operator=(decltype(nullptr));
		IComparable_1<System::UInt16>& operator=(IComparable_1<System::UInt16>&& other);
		bool operator==(const IComparable_1<System::UInt16>& other) const;
		bool operator!=(const IComparable_1<System::UInt16>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Int32> : virtual System::Object
	{
		IComparable_1<System::Int32>(decltype(nullptr));
		IComparable_1<System::Int32>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Int32>(const IComparable_1<System::Int32>& other);
		IComparable_1<System::Int32>(IComparable_1<System::Int32>&& other);
		virtual ~IComparable_1<System::Int32>();
		IComparable_1<System::Int32>& operator=(const IComparable_1<System::Int32>& other);
		IComparable_1<System::Int32>& operator=(decltype(nullptr));
		IComparable_1<System::Int32>& operator=(IComparable_1<System::Int32>&& other);
		bool operator==(const IComparable_1<System::Int32>& other) const;
		bool operator!=(const IComparable_1<System::Int32>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::UInt32> : virtual System::Object
	{
		IComparable_1<System::UInt32>(decltype(nullptr));
		IComparable_1<System::UInt32>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::UInt32>(const IComparable_1<System::UInt32>& other);
		IComparable_1<System::UInt32>(IComparable_1<System::UInt32>&& other);
		virtual ~IComparable_1<System::UInt32>();
		IComparable_1<System::UInt32>& operator=(const IComparable_1<System::UInt32>& other);
		IComparable_1<System::UInt32>& operator=(decltype(nullptr));
		IComparable_1<System::UInt32>& operator=(IComparable_1<System::UInt32>&& other);
		bool operator==(const IComparable_1<System::UInt32>& other) const;
		bool operator!=(const IComparable_1<System::UInt32>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Int64> : virtual System::Object
	{
		IComparable_1<System::Int64>(decltype(nullptr));
		IComparable_1<System::Int64>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Int64>(const IComparable_1<System::Int64>& other);
		IComparable_1<System::Int64>(IComparable_1<System::Int64>&& other);
		virtual ~IComparable_1<System::Int64>();
		IComparable_1<System::Int64>& operator=(const IComparable_1<System::Int64>& other);
		IComparable_1<System::Int64>& operator=(decltype(nullptr));
		IComparable_1<System::Int64>& operator=(IComparable_1<System::Int64>&& other);
		bool operator==(const IComparable_1<System::Int64>& other) const;
		bool operator!=(const IComparable_1<System::Int64>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::UInt64> : virtual System::Object
	{
		IComparable_1<System::UInt64>(decltype(nullptr));
		IComparable_1<System::UInt64>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::UInt64>(const IComparable_1<System::UInt64>& other);
		IComparable_1<System::UInt64>(IComparable_1<System::UInt64>&& other);
		virtual ~IComparable_1<System::UInt64>();
		IComparable_1<System::UInt64>& operator=(const IComparable_1<System::UInt64>& other);
		IComparable_1<System::UInt64>& operator=(decltype(nullptr));
		IComparable_1<System::UInt64>& operator=(IComparable_1<System::UInt64>&& other);
		bool operator==(const IComparable_1<System::UInt64>& other) const;
		bool operator!=(const IComparable_1<System::UInt64>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Single> : virtual System::Object
	{
		IComparable_1<System::Single>(decltype(nullptr));
		IComparable_1<System::Single>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Single>(const IComparable_1<System::Single>& other);
		IComparable_1<System::Single>(IComparable_1<System::Single>&& other);
		virtual ~IComparable_1<System::Single>();
		IComparable_1<System::Single>& operator=(const IComparable_1<System::Single>& other);
		IComparable_1<System::Single>& operator=(decltype(nullptr));
		IComparable_1<System::Single>& operator=(IComparable_1<System::Single>&& other);
		bool operator==(const IComparable_1<System::Single>& other) const;
		bool operator!=(const IComparable_1<System::Single>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Double> : virtual System::Object
	{
		IComparable_1<System::Double>(decltype(nullptr));
		IComparable_1<System::Double>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Double>(const IComparable_1<System::Double>& other);
		IComparable_1<System::Double>(IComparable_1<System::Double>&& other);
		virtual ~IComparable_1<System::Double>();
		IComparable_1<System::Double>& operator=(const IComparable_1<System::Double>& other);
		IComparable_1<System::Double>& operator=(decltype(nullptr));
		IComparable_1<System::Double>& operator=(IComparable_1<System::Double>&& other);
		bool operator==(const IComparable_1<System::Double>& other) const;
		bool operator!=(const IComparable_1<System::Double>& other) const;
	};
}

namespace System
{
	template<> struct IComparable_1<System::Decimal> : virtual System::Object
	{
		IComparable_1<System::Decimal>(decltype(nullptr));
		IComparable_1<System::Decimal>(Plugin::InternalUse, int32_t handle);
		IComparable_1<System::Decimal>(const IComparable_1<System::Decimal>& other);
		IComparable_1<System::Decimal>(IComparable_1<System::Decimal>&& other);
		virtual ~IComparable_1<System::Decimal>();
		IComparable_1<System::Decimal>& operator=(const IComparable_1<System::Decimal>& other);
		IComparable_1<System::Decimal>& operator=(decltype(nullptr));
		IComparable_1<System::Decimal>& operator=(IComparable_1<System::Decimal>&& other);
		bool operator==(const IComparable_1<System::Decimal>& other) const;
		bool operator!=(const IComparable_1<System::Decimal>& other) const;
	};
}

namespace System
{
	struct Decimal : Plugin::ManagedType
	{
		Decimal(decltype(nullptr));
		Decimal(Plugin::InternalUse, int32_t handle);
		Decimal(const Decimal& other);
		Decimal(Decimal&& other);
		virtual ~Decimal();
		Decimal& operator=(const Decimal& other);
		Decimal& operator=(decltype(nullptr));
		Decimal& operator=(Decimal&& other);
		bool operator==(const Decimal& other) const;
		bool operator!=(const Decimal& other) const;
		Decimal(System::Double value);
		Decimal(System::UInt64 value);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IComparable_1<System::Decimal>();
		explicit operator System::IConvertible();
		explicit operator System::IEquatable_1<System::Decimal>();
		explicit operator System::Runtime::Serialization::IDeserializationCallback();
		explicit operator System::IFormattable();
	};
}

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			namespace Marshal
			{
				void* StringToHGlobalUni(System::String& s);
				void FreeHGlobal(void* hglobal);
			}
		}
	}
}

namespace System
{
	namespace Runtime
	{
		namespace Serialization
		{
			struct IDeserializationCallback : virtual System::Object
			{
				IDeserializationCallback(decltype(nullptr));
				IDeserializationCallback(Plugin::InternalUse, int32_t handle);
				IDeserializationCallback(const IDeserializationCallback& other);
				IDeserializationCallback(IDeserializationCallback&& other);
				virtual ~IDeserializationCallback();
				IDeserializationCallback& operator=(const IDeserializationCallback& other);
				IDeserializationCallback& operator=(decltype(nullptr));
				IDeserializationCallback& operator=(IDeserializationCallback&& other);
				bool operator==(const IDeserializationCallback& other) const;
				bool operator!=(const IDeserializationCallback& other) const;
			};
		}
	}
}

namespace System
{
	namespace Runtime
	{
		namespace Serialization
		{
			struct ISerializable : virtual System::Object
			{
				ISerializable(decltype(nullptr));
				ISerializable(Plugin::InternalUse, int32_t handle);
				ISerializable(const ISerializable& other);
				ISerializable(ISerializable&& other);
				virtual ~ISerializable();
				ISerializable& operator=(const ISerializable& other);
				ISerializable& operator=(decltype(nullptr));
				ISerializable& operator=(ISerializable&& other);
				bool operator==(const ISerializable& other) const;
				bool operator!=(const ISerializable& other) const;
			};
		}
	}
}

namespace System
{
	struct MarshalByRefObject : virtual System::Object
	{
		MarshalByRefObject(decltype(nullptr));
		MarshalByRefObject(Plugin::InternalUse, int32_t handle);
		MarshalByRefObject(const MarshalByRefObject& other);
		MarshalByRefObject(MarshalByRefObject&& other);
		virtual ~MarshalByRefObject();
		MarshalByRefObject& operator=(const MarshalByRefObject& other);
		MarshalByRefObject& operator=(decltype(nullptr));
		MarshalByRefObject& operator=(MarshalByRefObject&& other);
		bool operator==(const MarshalByRefObject& other) const;
		bool operator!=(const MarshalByRefObject& other) const;
	};
}

namespace System
{
	namespace IO
	{
		namespace File
		{
			System::Boolean Exists(System::String& path);
		}
	}
}

namespace System
{
	namespace IO
	{
		struct FileSystemInfo : virtual System::MarshalByRefObject, virtual System::Runtime::Serialization::ISerializable
		{
			FileSystemInfo(decltype(nullptr));
			FileSystemInfo(Plugin::InternalUse, int32_t handle);
			FileSystemInfo(const FileSystemInfo& other);
			FileSystemInfo(FileSystemInfo&& other);
			virtual ~FileSystemInfo();
			FileSystemInfo& operator=(const FileSystemInfo& other);
			FileSystemInfo& operator=(decltype(nullptr));
			FileSystemInfo& operator=(FileSystemInfo&& other);
			bool operator==(const FileSystemInfo& other) const;
			bool operator!=(const FileSystemInfo& other) const;
		};
	}
}

namespace System
{
	namespace IO
	{
		struct DirectoryInfo : virtual System::IO::FileSystemInfo, virtual System::Runtime::Serialization::ISerializable
		{
			DirectoryInfo(decltype(nullptr));
			DirectoryInfo(Plugin::InternalUse, int32_t handle);
			DirectoryInfo(const DirectoryInfo& other);
			DirectoryInfo(DirectoryInfo&& other);
			virtual ~DirectoryInfo();
			DirectoryInfo& operator=(const DirectoryInfo& other);
			DirectoryInfo& operator=(decltype(nullptr));
			DirectoryInfo& operator=(DirectoryInfo&& other);
			bool operator==(const DirectoryInfo& other) const;
			bool operator!=(const DirectoryInfo& other) const;
		};
	}
}

namespace System
{
	namespace IO
	{
		namespace Directory
		{
			System::IO::DirectoryInfo CreateDirectory(System::String& path);
			System::Array1<System::String> GetFiles(System::String& path);
			System::Boolean Exists(System::String& path);
		}
	}
}

namespace UnityEngine
{
	struct TextureFormat
	{
		int32_t Value;
		static const UnityEngine::TextureFormat Alpha8;
		static const UnityEngine::TextureFormat ARGB4444;
		static const UnityEngine::TextureFormat RGB24;
		static const UnityEngine::TextureFormat RGBA32;
		static const UnityEngine::TextureFormat ARGB32;
		static const UnityEngine::TextureFormat RGB565;
		static const UnityEngine::TextureFormat R16;
		static const UnityEngine::TextureFormat DXT1;
		static const UnityEngine::TextureFormat DXT5;
		static const UnityEngine::TextureFormat RGBA4444;
		static const UnityEngine::TextureFormat BGRA32;
		static const UnityEngine::TextureFormat RHalf;
		static const UnityEngine::TextureFormat RGHalf;
		static const UnityEngine::TextureFormat RGBAHalf;
		static const UnityEngine::TextureFormat RFloat;
		static const UnityEngine::TextureFormat RGFloat;
		static const UnityEngine::TextureFormat RGBAFloat;
		static const UnityEngine::TextureFormat YUY2;
		static const UnityEngine::TextureFormat RGB9e5Float;
		static const UnityEngine::TextureFormat BC4;
		static const UnityEngine::TextureFormat BC5;
		static const UnityEngine::TextureFormat BC6H;
		static const UnityEngine::TextureFormat BC7;
		static const UnityEngine::TextureFormat DXT1Crunched;
		static const UnityEngine::TextureFormat DXT5Crunched;
		static const UnityEngine::TextureFormat PVRTC_RGB2;
		static const UnityEngine::TextureFormat PVRTC_RGBA2;
		static const UnityEngine::TextureFormat PVRTC_RGB4;
		static const UnityEngine::TextureFormat PVRTC_RGBA4;
		static const UnityEngine::TextureFormat ETC_RGB4;
		static const UnityEngine::TextureFormat ATC_RGB4;
		static const UnityEngine::TextureFormat ATC_RGBA8;
		static const UnityEngine::TextureFormat EAC_R;
		static const UnityEngine::TextureFormat EAC_R_SIGNED;
		static const UnityEngine::TextureFormat EAC_RG;
		static const UnityEngine::TextureFormat EAC_RG_SIGNED;
		static const UnityEngine::TextureFormat ETC2_RGB;
		static const UnityEngine::TextureFormat ETC2_RGBA1;
		static const UnityEngine::TextureFormat ETC2_RGBA8;
		static const UnityEngine::TextureFormat ASTC_4x4;
		static const UnityEngine::TextureFormat ASTC_5x5;
		static const UnityEngine::TextureFormat ASTC_6x6;
		static const UnityEngine::TextureFormat ASTC_8x8;
		static const UnityEngine::TextureFormat ASTC_10x10;
		static const UnityEngine::TextureFormat ASTC_12x12;
		static const UnityEngine::TextureFormat ETC_RGB4_3DS;
		static const UnityEngine::TextureFormat ETC_RGBA8_3DS;
		static const UnityEngine::TextureFormat RG16;
		static const UnityEngine::TextureFormat R8;
		static const UnityEngine::TextureFormat ETC_RGB4Crunched;
		static const UnityEngine::TextureFormat ETC2_RGBA8Crunched;
		static const UnityEngine::TextureFormat ASTC_HDR_4x4;
		static const UnityEngine::TextureFormat ASTC_HDR_5x5;
		static const UnityEngine::TextureFormat ASTC_HDR_6x6;
		static const UnityEngine::TextureFormat ASTC_HDR_8x8;
		static const UnityEngine::TextureFormat ASTC_HDR_10x10;
		static const UnityEngine::TextureFormat ASTC_HDR_12x12;
		static const UnityEngine::TextureFormat ASTC_RGB_4x4;
		static const UnityEngine::TextureFormat ASTC_RGB_5x5;
		static const UnityEngine::TextureFormat ASTC_RGB_6x6;
		static const UnityEngine::TextureFormat ASTC_RGB_8x8;
		static const UnityEngine::TextureFormat ASTC_RGB_10x10;
		static const UnityEngine::TextureFormat ASTC_RGB_12x12;
		static const UnityEngine::TextureFormat ASTC_RGBA_4x4;
		static const UnityEngine::TextureFormat ASTC_RGBA_5x5;
		static const UnityEngine::TextureFormat ASTC_RGBA_6x6;
		static const UnityEngine::TextureFormat ASTC_RGBA_8x8;
		static const UnityEngine::TextureFormat ASTC_RGBA_10x10;
		static const UnityEngine::TextureFormat ASTC_RGBA_12x12;
		static const UnityEngine::TextureFormat PVRTC_2BPP_RGB;
		static const UnityEngine::TextureFormat PVRTC_2BPP_RGBA;
		static const UnityEngine::TextureFormat PVRTC_4BPP_RGB;
		static const UnityEngine::TextureFormat PVRTC_4BPP_RGBA;
		TextureFormat();
		explicit TextureFormat(int32_t value);
		explicit operator int32_t() const;
		bool operator==(TextureFormat other);
		bool operator!=(TextureFormat other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct TextureDimension
		{
			int32_t Value;
			static const UnityEngine::Rendering::TextureDimension Unknown;
			static const UnityEngine::Rendering::TextureDimension None;
			static const UnityEngine::Rendering::TextureDimension Any;
			static const UnityEngine::Rendering::TextureDimension Tex2D;
			static const UnityEngine::Rendering::TextureDimension Tex3D;
			static const UnityEngine::Rendering::TextureDimension Cube;
			static const UnityEngine::Rendering::TextureDimension Tex2DArray;
			static const UnityEngine::Rendering::TextureDimension CubeArray;
			TextureDimension();
			explicit TextureDimension(int32_t value);
			explicit operator int32_t() const;
			bool operator==(TextureDimension other);
			bool operator!=(TextureDimension other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	struct FilterMode
	{
		int32_t Value;
		static const UnityEngine::FilterMode Point;
		static const UnityEngine::FilterMode Bilinear;
		static const UnityEngine::FilterMode Trilinear;
		FilterMode();
		explicit FilterMode(int32_t value);
		explicit operator int32_t() const;
		bool operator==(FilterMode other);
		bool operator!=(FilterMode other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct TextureWrapMode
	{
		int32_t Value;
		static const UnityEngine::TextureWrapMode Repeat;
		static const UnityEngine::TextureWrapMode Clamp;
		static const UnityEngine::TextureWrapMode Mirror;
		static const UnityEngine::TextureWrapMode MirrorOnce;
		TextureWrapMode();
		explicit TextureWrapMode(int32_t value);
		explicit operator int32_t() const;
		bool operator==(TextureWrapMode other);
		bool operator!=(TextureWrapMode other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct LightProbeUsage
		{
			int32_t Value;
			static const UnityEngine::Rendering::LightProbeUsage Off;
			static const UnityEngine::Rendering::LightProbeUsage BlendProbes;
			static const UnityEngine::Rendering::LightProbeUsage UseProxyVolume;
			static const UnityEngine::Rendering::LightProbeUsage CustomProvided;
			LightProbeUsage();
			explicit LightProbeUsage(int32_t value);
			explicit operator int32_t() const;
			bool operator==(LightProbeUsage other);
			bool operator!=(LightProbeUsage other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeUsage
		{
			int32_t Value;
			static const UnityEngine::Rendering::ReflectionProbeUsage Off;
			static const UnityEngine::Rendering::ReflectionProbeUsage BlendProbes;
			static const UnityEngine::Rendering::ReflectionProbeUsage BlendProbesAndSkybox;
			static const UnityEngine::Rendering::ReflectionProbeUsage Simple;
			ReflectionProbeUsage();
			explicit ReflectionProbeUsage(int32_t value);
			explicit operator int32_t() const;
			bool operator==(ReflectionProbeUsage other);
			bool operator!=(ReflectionProbeUsage other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeMode
		{
			int32_t Value;
			static const UnityEngine::Rendering::ReflectionProbeMode Baked;
			static const UnityEngine::Rendering::ReflectionProbeMode Realtime;
			static const UnityEngine::Rendering::ReflectionProbeMode Custom;
			ReflectionProbeMode();
			explicit ReflectionProbeMode(int32_t value);
			explicit operator int32_t() const;
			bool operator==(ReflectionProbeMode other);
			bool operator!=(ReflectionProbeMode other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeRefreshMode
		{
			int32_t Value;
			static const UnityEngine::Rendering::ReflectionProbeRefreshMode OnAwake;
			static const UnityEngine::Rendering::ReflectionProbeRefreshMode EveryFrame;
			static const UnityEngine::Rendering::ReflectionProbeRefreshMode ViaScripting;
			ReflectionProbeRefreshMode();
			explicit ReflectionProbeRefreshMode(int32_t value);
			explicit operator int32_t() const;
			bool operator==(ReflectionProbeRefreshMode other);
			bool operator!=(ReflectionProbeRefreshMode other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ReflectionProbeTimeSlicingMode
		{
			int32_t Value;
			static const UnityEngine::Rendering::ReflectionProbeTimeSlicingMode AllFacesAtOnce;
			static const UnityEngine::Rendering::ReflectionProbeTimeSlicingMode IndividualFaces;
			static const UnityEngine::Rendering::ReflectionProbeTimeSlicingMode NoTimeSlicing;
			ReflectionProbeTimeSlicingMode();
			explicit ReflectionProbeTimeSlicingMode(int32_t value);
			explicit operator int32_t() const;
			bool operator==(ReflectionProbeTimeSlicingMode other);
			bool operator!=(ReflectionProbeTimeSlicingMode other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	struct Bounds : Plugin::ManagedType
	{
		Bounds(decltype(nullptr));
		Bounds(Plugin::InternalUse, int32_t handle);
		Bounds(const Bounds& other);
		Bounds(Bounds&& other);
		virtual ~Bounds();
		Bounds& operator=(const Bounds& other);
		Bounds& operator=(decltype(nullptr));
		Bounds& operator=(Bounds&& other);
		bool operator==(const Bounds& other) const;
		bool operator!=(const Bounds& other) const;
		UnityEngine::Vector3 GetCenter();
		void SetCenter(UnityEngine::Vector3& value);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::Bounds>();
	};
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct ShadowCastingMode
		{
			int32_t Value;
			static const UnityEngine::Rendering::ShadowCastingMode Off;
			static const UnityEngine::Rendering::ShadowCastingMode On;
			static const UnityEngine::Rendering::ShadowCastingMode TwoSided;
			static const UnityEngine::Rendering::ShadowCastingMode ShadowsOnly;
			ShadowCastingMode();
			explicit ShadowCastingMode(int32_t value);
			explicit operator int32_t() const;
			bool operator==(ShadowCastingMode other);
			bool operator!=(ShadowCastingMode other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	struct Color32 : Plugin::ManagedType
	{
		Color32(decltype(nullptr));
		Color32(Plugin::InternalUse, int32_t handle);
		Color32(const Color32& other);
		Color32(Color32&& other);
		virtual ~Color32();
		Color32& operator=(const Color32& other);
		Color32& operator=(decltype(nullptr));
		Color32& operator=(Color32&& other);
		bool operator==(const Color32& other) const;
		bool operator!=(const Color32& other) const;
		Color32(System::Byte r, System::Byte g, System::Byte b, System::Byte a);
		System::Byte GetR();
		void SetR(System::Byte value);
		System::Byte GetG();
		void SetG(System::Byte value);
		System::Byte GetB();
		void SetB(System::Byte value);
		System::Byte GetA();
		void SetA(System::Byte value);
		explicit operator System::ValueType();
		explicit operator System::Object();
	};
}

namespace UnityEngine
{
	struct Color
	{
		Color();
		Color(System::Single r, System::Single g, System::Single b, System::Single a);
		System::Single r;
		System::Single g;
		System::Single b;
		System::Single a;
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::Color>();
	};
}

namespace UnityEngine
{
	struct Vector2
	{
		Vector2();
		Vector2(System::Single x, System::Single y);
		System::Single x;
		System::Single y;
		UnityEngine::Vector2 operator+(UnityEngine::Vector2& a);
		void Set(System::Single newX, System::Single newY);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::Vector2>();
	};
}

namespace UnityEngine
{
	struct Vector3
	{
		Vector3();
		Vector3(System::Single x, System::Single y, System::Single z);
		System::Single GetMagnitude();
		System::Single GetSqrMagnitude();
		UnityEngine::Vector3 GetNormalized();
		System::Single x;
		System::Single y;
		System::Single z;
		UnityEngine::Vector3 operator+(UnityEngine::Vector3& a);
		static UnityEngine::Vector3 Cross(UnityEngine::Vector3& lhs, UnityEngine::Vector3& rhs);
		static System::Single Dot(UnityEngine::Vector3& lhs, UnityEngine::Vector3& rhs);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::Vector3>();
	};
}

namespace UnityEngine
{
	struct Vector4
	{
		Vector4();
		Vector4(System::Single x, System::Single y, System::Single z, System::Single w);
		System::Single GetMagnitude();
		System::Single GetSqrMagnitude();
		UnityEngine::Vector4 GetNormalized();
		System::Single x;
		System::Single y;
		System::Single z;
		System::Single w;
		UnityEngine::Vector4 operator+(UnityEngine::Vector4& a);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::Vector4>();
	};
}

namespace UnityEngine
{
	struct Matrix4x4
	{
		Matrix4x4();
		static UnityEngine::Matrix4x4 GetIdentity();
		static UnityEngine::Matrix4x4 GetZero();
		System::Single GetDeterminant();
		UnityEngine::Matrix4x4 GetInverse();
		System::Boolean GetIsIdentity();
		UnityEngine::Vector3 GetLossyScale();
		UnityEngine::Quaternion GetRotation();
		UnityEngine::Matrix4x4 GetTranspose();
		System::Single m00;
		System::Single m10;
		System::Single m20;
		System::Single m30;
		System::Single m01;
		System::Single m11;
		System::Single m21;
		System::Single m31;
		System::Single m02;
		System::Single m12;
		System::Single m22;
		System::Single m32;
		System::Single m03;
		System::Single m13;
		System::Single m23;
		System::Single m33;
		UnityEngine::Matrix4x4 operator*(UnityEngine::Matrix4x4& lhs);
		UnityEngine::Vector4 GetColumn(System::Int32 index);
		UnityEngine::Vector4 GetRow(System::Int32 index);
		void SetColumn(System::Int32 index, UnityEngine::Vector4& column);
		void SetRow(System::Int32 index, UnityEngine::Vector4& row);
		static UnityEngine::Matrix4x4 LookAt(UnityEngine::Vector3& from, UnityEngine::Vector3& to, UnityEngine::Vector3& up);
		static UnityEngine::Matrix4x4 Rotate(UnityEngine::Quaternion& q);
		static UnityEngine::Matrix4x4 Scale(UnityEngine::Vector3& vector);
		static UnityEngine::Matrix4x4 Translate(UnityEngine::Vector3& vector);
		UnityEngine::Vector3 MultiplyPoint(UnityEngine::Vector3& point);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::Matrix4x4>();
	};
}

namespace UnityEngine
{
	struct Quaternion
	{
		Quaternion();
		Quaternion(System::Single x, System::Single y, System::Single z, System::Single w);
		UnityEngine::Vector3 GetEulerAngles();
		void SetEulerAngles(UnityEngine::Vector3& value);
		System::Single x;
		System::Single y;
		System::Single z;
		System::Single w;
		static UnityEngine::Quaternion LookRotation(UnityEngine::Vector3& forward, UnityEngine::Vector3& upwards);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::Quaternion>();
	};
}

namespace UnityEngine
{
	struct LayerMask : Plugin::ManagedType
	{
		LayerMask(decltype(nullptr));
		LayerMask(Plugin::InternalUse, int32_t handle);
		LayerMask(const LayerMask& other);
		LayerMask(LayerMask&& other);
		virtual ~LayerMask();
		LayerMask& operator=(const LayerMask& other);
		LayerMask& operator=(decltype(nullptr));
		LayerMask& operator=(LayerMask&& other);
		bool operator==(const LayerMask& other) const;
		bool operator!=(const LayerMask& other) const;
		static System::Int32 NameToLayer(System::String& layerName);
		explicit operator System::ValueType();
		explicit operator System::Object();
	};
}

namespace UnityEngine
{
	struct Object : virtual System::Object
	{
		Object(decltype(nullptr));
		Object(Plugin::InternalUse, int32_t handle);
		Object(const Object& other);
		Object(Object&& other);
		virtual ~Object();
		Object& operator=(const Object& other);
		Object& operator=(decltype(nullptr));
		Object& operator=(Object&& other);
		bool operator==(const Object& other) const;
		bool operator!=(const Object& other) const;
		System::String GetName();
		void SetName(System::String& value);
		static void Destroy(UnityEngine::Object& obj);
		static void Destroy(UnityEngine::Object& obj, System::Single t);
		static UnityEngine::Object Instantiate(UnityEngine::Object& original);
		static UnityEngine::Object Instantiate(UnityEngine::Object& original, UnityEngine::Transform& parent);
		static UnityEngine::Object Instantiate(UnityEngine::Object& original, UnityEngine::Transform& parent, System::Boolean instantiateInWorldSpace);
		static UnityEngine::Object Instantiate(UnityEngine::Object& original, UnityEngine::Vector3& position, UnityEngine::Quaternion& rotation);
		static UnityEngine::Object Instantiate(UnityEngine::Object& original, UnityEngine::Vector3& position, UnityEngine::Quaternion& rotation, UnityEngine::Transform& parent);
	};
}

namespace UnityEngine
{
	struct Component : virtual UnityEngine::Object
	{
		Component(decltype(nullptr));
		Component(Plugin::InternalUse, int32_t handle);
		Component(const Component& other);
		Component(Component&& other);
		virtual ~Component();
		Component& operator=(const Component& other);
		Component& operator=(decltype(nullptr));
		Component& operator=(Component&& other);
		bool operator==(const Component& other) const;
		bool operator!=(const Component& other) const;
		UnityEngine::Transform GetTransform();
		UnityEngine::GameObject GetGameObject();
		template<typename MT0> System::Array1<UnityEngine::MeshFilter> GetComponentsInChildren(System::Boolean includeInactive);
		template<typename MT0> System::Array1<UnityEngine::Transform> GetComponentsInChildren();
	};
}

namespace UnityEngine
{
	struct AudioClip : virtual UnityEngine::Object
	{
		AudioClip(decltype(nullptr));
		AudioClip(Plugin::InternalUse, int32_t handle);
		AudioClip(const AudioClip& other);
		AudioClip(AudioClip&& other);
		virtual ~AudioClip();
		AudioClip& operator=(const AudioClip& other);
		AudioClip& operator=(decltype(nullptr));
		AudioClip& operator=(AudioClip&& other);
		bool operator==(const AudioClip& other) const;
		bool operator!=(const AudioClip& other) const;
	};
}

namespace UnityEngine
{
	struct Resources : virtual System::Object
	{
		Resources(decltype(nullptr));
		Resources(Plugin::InternalUse, int32_t handle);
		Resources(const Resources& other);
		Resources(Resources&& other);
		virtual ~Resources();
		Resources& operator=(const Resources& other);
		Resources& operator=(decltype(nullptr));
		Resources& operator=(Resources&& other);
		bool operator==(const Resources& other) const;
		bool operator!=(const Resources& other) const;
		static UnityEngine::Object Load(System::String& path);
	};
}

namespace UnityEngine
{
	struct Transform : virtual UnityEngine::Component, virtual System::Collections::IEnumerable
	{
		Transform(decltype(nullptr));
		Transform(Plugin::InternalUse, int32_t handle);
		Transform(const Transform& other);
		Transform(Transform&& other);
		virtual ~Transform();
		Transform& operator=(const Transform& other);
		Transform& operator=(decltype(nullptr));
		Transform& operator=(Transform&& other);
		bool operator==(const Transform& other) const;
		bool operator!=(const Transform& other) const;
		UnityEngine::Transform GetParent();
		void SetParent(UnityEngine::Transform& value);
		UnityEngine::Vector3 GetPosition();
		void SetPosition(UnityEngine::Vector3& value);
		UnityEngine::Vector3 GetLocalPosition();
		void SetLocalPosition(UnityEngine::Vector3& value);
		UnityEngine::Quaternion GetRotation();
		void SetRotation(UnityEngine::Quaternion& value);
		UnityEngine::Quaternion GetLocalRotation();
		void SetLocalRotation(UnityEngine::Quaternion& value);
		UnityEngine::Vector3 GetEulerAngles();
		void SetEulerAngles(UnityEngine::Vector3& value);
		UnityEngine::Vector3 GetLocalEulerAngles();
		void SetLocalEulerAngles(UnityEngine::Vector3& value);
		UnityEngine::Vector3 GetLocalScale();
		void SetLocalScale(UnityEngine::Vector3& value);
		UnityEngine::Matrix4x4 GetWorldToLocalMatrix();
		UnityEngine::Matrix4x4 GetLocalToWorldMatrix();
	};
}

namespace System
{
	namespace Collections
	{
		struct IEnumerator : virtual System::Object
		{
			IEnumerator(decltype(nullptr));
			IEnumerator(Plugin::InternalUse, int32_t handle);
			IEnumerator(const IEnumerator& other);
			IEnumerator(IEnumerator&& other);
			virtual ~IEnumerator();
			IEnumerator& operator=(const IEnumerator& other);
			IEnumerator& operator=(decltype(nullptr));
			IEnumerator& operator=(IEnumerator&& other);
			bool operator==(const IEnumerator& other) const;
			bool operator!=(const IEnumerator& other) const;
			System::Object GetCurrent();
			System::Boolean MoveNext();
		};
	}
}

namespace System
{
	namespace Runtime
	{
		namespace InteropServices
		{
			struct _Exception : virtual System::Object
			{
				_Exception(decltype(nullptr));
				_Exception(Plugin::InternalUse, int32_t handle);
				_Exception(const _Exception& other);
				_Exception(_Exception&& other);
				virtual ~_Exception();
				_Exception& operator=(const _Exception& other);
				_Exception& operator=(decltype(nullptr));
				_Exception& operator=(_Exception&& other);
				bool operator==(const _Exception& other) const;
				bool operator!=(const _Exception& other) const;
			};
		}
	}
}

namespace UnityEngine
{
	struct GameObject : virtual UnityEngine::Object
	{
		GameObject(decltype(nullptr));
		GameObject(Plugin::InternalUse, int32_t handle);
		GameObject(const GameObject& other);
		GameObject(GameObject&& other);
		virtual ~GameObject();
		GameObject& operator=(const GameObject& other);
		GameObject& operator=(decltype(nullptr));
		GameObject& operator=(GameObject&& other);
		bool operator==(const GameObject& other) const;
		bool operator!=(const GameObject& other) const;
		GameObject();
		GameObject(System::String& name);
		UnityEngine::Transform GetTransform();
		System::Int32 GetLayer();
		void SetLayer(System::Int32 value);
		System::String GetTag();
		void SetTag(System::String& value);
		void SetActive(System::Boolean value);
		template<typename MT0> MT0 GetComponent();
		template<typename MT0> MT0 AddComponent();
		static UnityEngine::GameObject CreatePrimitive(UnityEngine::PrimitiveType type);
	};
}

namespace UnityEngine
{
	struct Debug : virtual System::Object
	{
		Debug(decltype(nullptr));
		Debug(Plugin::InternalUse, int32_t handle);
		Debug(const Debug& other);
		Debug(Debug&& other);
		virtual ~Debug();
		Debug& operator=(const Debug& other);
		Debug& operator=(decltype(nullptr));
		Debug& operator=(Debug&& other);
		bool operator==(const Debug& other) const;
		bool operator!=(const Debug& other) const;
		static void Log(System::Object& message);
		static void LogError(System::Object& message);
	};
}

namespace UnityEngine
{
	struct Behaviour : virtual UnityEngine::Component
	{
		Behaviour(decltype(nullptr));
		Behaviour(Plugin::InternalUse, int32_t handle);
		Behaviour(const Behaviour& other);
		Behaviour(Behaviour&& other);
		virtual ~Behaviour();
		Behaviour& operator=(const Behaviour& other);
		Behaviour& operator=(decltype(nullptr));
		Behaviour& operator=(Behaviour&& other);
		bool operator==(const Behaviour& other) const;
		bool operator!=(const Behaviour& other) const;
		System::Boolean GetEnabled();
		void SetEnabled(System::Boolean value);
	};
}

namespace UnityEngine
{
	struct MonoBehaviour : virtual UnityEngine::Behaviour
	{
		MonoBehaviour(decltype(nullptr));
		MonoBehaviour(Plugin::InternalUse, int32_t handle);
		MonoBehaviour(const MonoBehaviour& other);
		MonoBehaviour(MonoBehaviour&& other);
		virtual ~MonoBehaviour();
		MonoBehaviour& operator=(const MonoBehaviour& other);
		MonoBehaviour& operator=(decltype(nullptr));
		MonoBehaviour& operator=(MonoBehaviour&& other);
		bool operator==(const MonoBehaviour& other) const;
		bool operator!=(const MonoBehaviour& other) const;
		UnityEngine::Transform GetTransform();
	};
}

namespace UnityEngine
{
	struct AudioBehaviour : virtual UnityEngine::Behaviour
	{
		AudioBehaviour(decltype(nullptr));
		AudioBehaviour(Plugin::InternalUse, int32_t handle);
		AudioBehaviour(const AudioBehaviour& other);
		AudioBehaviour(AudioBehaviour&& other);
		virtual ~AudioBehaviour();
		AudioBehaviour& operator=(const AudioBehaviour& other);
		AudioBehaviour& operator=(decltype(nullptr));
		AudioBehaviour& operator=(AudioBehaviour&& other);
		bool operator==(const AudioBehaviour& other) const;
		bool operator!=(const AudioBehaviour& other) const;
	};
}

namespace UnityEngine
{
	struct AudioSource : virtual UnityEngine::AudioBehaviour
	{
		AudioSource(decltype(nullptr));
		AudioSource(Plugin::InternalUse, int32_t handle);
		AudioSource(const AudioSource& other);
		AudioSource(AudioSource&& other);
		virtual ~AudioSource();
		AudioSource& operator=(const AudioSource& other);
		AudioSource& operator=(decltype(nullptr));
		AudioSource& operator=(AudioSource&& other);
		bool operator==(const AudioSource& other) const;
		bool operator!=(const AudioSource& other) const;
	};
}

namespace UnityEngine
{
	struct CollisionDetectionMode
	{
		int32_t Value;
		static const UnityEngine::CollisionDetectionMode Discrete;
		static const UnityEngine::CollisionDetectionMode Continuous;
		static const UnityEngine::CollisionDetectionMode ContinuousDynamic;
		static const UnityEngine::CollisionDetectionMode ContinuousSpeculative;
		CollisionDetectionMode();
		explicit CollisionDetectionMode(int32_t value);
		explicit operator int32_t() const;
		bool operator==(CollisionDetectionMode other);
		bool operator!=(CollisionDetectionMode other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct Rigidbody : virtual UnityEngine::Component
	{
		Rigidbody(decltype(nullptr));
		Rigidbody(Plugin::InternalUse, int32_t handle);
		Rigidbody(const Rigidbody& other);
		Rigidbody(Rigidbody&& other);
		virtual ~Rigidbody();
		Rigidbody& operator=(const Rigidbody& other);
		Rigidbody& operator=(decltype(nullptr));
		Rigidbody& operator=(Rigidbody&& other);
		bool operator==(const Rigidbody& other) const;
		bool operator!=(const Rigidbody& other) const;
		System::Single GetMass();
		void SetMass(System::Single value);
		System::Boolean GetFreezeRotation();
		void SetFreezeRotation(System::Boolean value);
		UnityEngine::CollisionDetectionMode GetCollisionDetectionMode();
		void SetCollisionDetectionMode(UnityEngine::CollisionDetectionMode value);
	};
}

namespace UnityEngine
{
	struct Renderer : virtual UnityEngine::Component
	{
		Renderer(decltype(nullptr));
		Renderer(Plugin::InternalUse, int32_t handle);
		Renderer(const Renderer& other);
		Renderer(Renderer&& other);
		virtual ~Renderer();
		Renderer& operator=(const Renderer& other);
		Renderer& operator=(decltype(nullptr));
		Renderer& operator=(Renderer&& other);
		bool operator==(const Renderer& other) const;
		bool operator!=(const Renderer& other) const;
		System::Boolean GetEnabled();
		void SetEnabled(System::Boolean value);
		UnityEngine::Material GetSharedMaterial();
		void SetSharedMaterial(UnityEngine::Material& value);
		System::Array1<UnityEngine::Material> GetSharedMaterials();
		void SetSharedMaterials(System::Array1<UnityEngine::Material>& value);
		UnityEngine::Material GetMaterial();
		void SetMaterial(UnityEngine::Material& value);
		System::Array1<UnityEngine::Material> GetMaterials();
		void SetMaterials(System::Array1<UnityEngine::Material>& value);
		System::Boolean GetAllowOcclusionWhenDynamic();
		void SetAllowOcclusionWhenDynamic(System::Boolean value);
		UnityEngine::Rendering::LightProbeUsage GetLightProbeUsage();
		void SetLightProbeUsage(UnityEngine::Rendering::LightProbeUsage value);
		UnityEngine::Rendering::ReflectionProbeUsage GetReflectionProbeUsage();
		void SetReflectionProbeUsage(UnityEngine::Rendering::ReflectionProbeUsage value);
		UnityEngine::Rendering::ShadowCastingMode GetShadowCastingMode();
		void SetShadowCastingMode(UnityEngine::Rendering::ShadowCastingMode value);
		System::Boolean GetReceiveShadows();
		void SetReceiveShadows(System::Boolean value);
		UnityEngine::Transform GetProbeAnchor();
		void SetProbeAnchor(UnityEngine::Transform& value);
		UnityEngine::Bounds GetBounds();
	};
}

namespace UnityEngine
{
	struct MeshFilter : virtual UnityEngine::Component
	{
		MeshFilter(decltype(nullptr));
		MeshFilter(Plugin::InternalUse, int32_t handle);
		MeshFilter(const MeshFilter& other);
		MeshFilter(MeshFilter&& other);
		virtual ~MeshFilter();
		MeshFilter& operator=(const MeshFilter& other);
		MeshFilter& operator=(decltype(nullptr));
		MeshFilter& operator=(MeshFilter&& other);
		bool operator==(const MeshFilter& other) const;
		bool operator!=(const MeshFilter& other) const;
		UnityEngine::Mesh GetMesh();
		void SetMesh(UnityEngine::Mesh& value);
		UnityEngine::Mesh GetSharedMesh();
		void SetSharedMesh(UnityEngine::Mesh& value);
	};
}

namespace UnityEngine
{
	struct SkinnedMeshRenderer : virtual UnityEngine::Renderer
	{
		SkinnedMeshRenderer(decltype(nullptr));
		SkinnedMeshRenderer(Plugin::InternalUse, int32_t handle);
		SkinnedMeshRenderer(const SkinnedMeshRenderer& other);
		SkinnedMeshRenderer(SkinnedMeshRenderer&& other);
		virtual ~SkinnedMeshRenderer();
		SkinnedMeshRenderer& operator=(const SkinnedMeshRenderer& other);
		SkinnedMeshRenderer& operator=(decltype(nullptr));
		SkinnedMeshRenderer& operator=(SkinnedMeshRenderer&& other);
		bool operator==(const SkinnedMeshRenderer& other) const;
		bool operator!=(const SkinnedMeshRenderer& other) const;
		System::Array1<UnityEngine::Transform> GetBones();
		void SetBones(System::Array1<UnityEngine::Transform>& value);
		UnityEngine::Mesh GetSharedMesh();
		void SetSharedMesh(UnityEngine::Mesh& value);
		System::Boolean GetUpdateWhenOffscreen();
		void SetUpdateWhenOffscreen(System::Boolean value);
	};
}

namespace UnityEngine
{
	struct ReflectionProbe : virtual UnityEngine::Behaviour
	{
		ReflectionProbe(decltype(nullptr));
		ReflectionProbe(Plugin::InternalUse, int32_t handle);
		ReflectionProbe(const ReflectionProbe& other);
		ReflectionProbe(ReflectionProbe&& other);
		virtual ~ReflectionProbe();
		ReflectionProbe& operator=(const ReflectionProbe& other);
		ReflectionProbe& operator=(decltype(nullptr));
		ReflectionProbe& operator=(ReflectionProbe&& other);
		bool operator==(const ReflectionProbe& other) const;
		bool operator!=(const ReflectionProbe& other) const;
		UnityEngine::Texture GetCustomBakedTexture();
		void SetCustomBakedTexture(UnityEngine::Texture& value);
		UnityEngine::Vector3 GetSize();
		void SetSize(UnityEngine::Vector3& value);
		UnityEngine::Rendering::ReflectionProbeMode GetMode();
		void SetMode(UnityEngine::Rendering::ReflectionProbeMode value);
		System::Int32 GetResolution();
		void SetResolution(System::Int32 value);
		UnityEngine::Color GetBackgroundColor();
		void SetBackgroundColor(UnityEngine::Color& value);
		System::Single GetFarClipPlane();
		void SetFarClipPlane(System::Single value);
		System::Boolean GetHdr();
		void SetHdr(System::Boolean value);
		System::Single GetNearClipPlane();
		void SetNearClipPlane(System::Single value);
		UnityEngine::Rendering::ReflectionProbeRefreshMode GetRefreshMode();
		void SetRefreshMode(UnityEngine::Rendering::ReflectionProbeRefreshMode value);
		UnityEngine::Rendering::ReflectionProbeTimeSlicingMode GetTimeSlicingMode();
		void SetTimeSlicingMode(UnityEngine::Rendering::ReflectionProbeTimeSlicingMode value);
	};
}

namespace UnityEngine
{
	struct MeshRenderer : virtual UnityEngine::Renderer
	{
		MeshRenderer(decltype(nullptr));
		MeshRenderer(Plugin::InternalUse, int32_t handle);
		MeshRenderer(const MeshRenderer& other);
		MeshRenderer(MeshRenderer&& other);
		virtual ~MeshRenderer();
		MeshRenderer& operator=(const MeshRenderer& other);
		MeshRenderer& operator=(decltype(nullptr));
		MeshRenderer& operator=(MeshRenderer&& other);
		bool operator==(const MeshRenderer& other) const;
		bool operator!=(const MeshRenderer& other) const;
	};
}

namespace UnityEngine
{
	struct Shader : virtual UnityEngine::Object
	{
		Shader(decltype(nullptr));
		Shader(Plugin::InternalUse, int32_t handle);
		Shader(const Shader& other);
		Shader(Shader&& other);
		virtual ~Shader();
		Shader& operator=(const Shader& other);
		Shader& operator=(decltype(nullptr));
		Shader& operator=(Shader&& other);
		bool operator==(const Shader& other) const;
		bool operator!=(const Shader& other) const;
		static UnityEngine::Shader Find(System::String& name);
	};
}

namespace UnityEngine
{
	struct Texture : virtual UnityEngine::Object
	{
		Texture(decltype(nullptr));
		Texture(Plugin::InternalUse, int32_t handle);
		Texture(const Texture& other);
		Texture(Texture&& other);
		virtual ~Texture();
		Texture& operator=(const Texture& other);
		Texture& operator=(decltype(nullptr));
		Texture& operator=(Texture&& other);
		bool operator==(const Texture& other) const;
		bool operator!=(const Texture& other) const;
		System::Int32 GetAnisoLevel();
		void SetAnisoLevel(System::Int32 value);
		UnityEngine::Rendering::TextureDimension GetDimension();
		void SetDimension(UnityEngine::Rendering::TextureDimension value);
		UnityEngine::FilterMode GetFilterMode();
		void SetFilterMode(UnityEngine::FilterMode value);
		System::Int32 GetHeight();
		void SetHeight(System::Int32 value);
		System::Int32 GetWidth();
		void SetWidth(System::Int32 value);
		System::Single GetMipMapBias();
		void SetMipMapBias(System::Single value);
		UnityEngine::TextureWrapMode GetWrapModeU();
		void SetWrapModeU(UnityEngine::TextureWrapMode value);
		UnityEngine::TextureWrapMode GetWrapModeV();
		void SetWrapModeV(UnityEngine::TextureWrapMode value);
	};
}

namespace UnityEngine
{
	struct Texture2D : virtual UnityEngine::Texture
	{
		Texture2D(decltype(nullptr));
		Texture2D(Plugin::InternalUse, int32_t handle);
		Texture2D(const Texture2D& other);
		Texture2D(Texture2D&& other);
		virtual ~Texture2D();
		Texture2D& operator=(const Texture2D& other);
		Texture2D& operator=(decltype(nullptr));
		Texture2D& operator=(Texture2D&& other);
		bool operator==(const Texture2D& other) const;
		bool operator!=(const Texture2D& other) const;
		Texture2D(System::Int32 width, System::Int32 height);
		Texture2D(System::Int32 width, System::Int32 height, UnityEngine::TextureFormat textureFormat, System::Boolean mipChain, System::Boolean linear);
		UnityEngine::TextureFormat GetFormat();
		System::Int32 GetMipmapCount();
		void Apply(System::Boolean updateMipmaps, System::Boolean makeNoLongerReadable);
		void LoadRawTextureData(System::Array1<System::Byte>& data);
		void LoadRawTextureData(void* data, System::Int32 size);
		System::Array1<UnityEngine::Color> GetPixels(System::Int32 miplevel);
		void SetPixels(System::Array1<UnityEngine::Color>& colors, System::Int32 miplevel);
		System::Boolean Resize(System::Int32 width, System::Int32 height, UnityEngine::TextureFormat format, System::Boolean hasMipMap);
	};
}

namespace UnityEngine
{
	namespace ImageConversion
	{
		System::Boolean LoadImage(UnityEngine::Texture2D& tex, System::Array1<System::Byte>& data, System::Boolean markNonReadable);
	}
}

namespace UnityEngine
{
	struct CubemapFace
	{
		int32_t Value;
		static const UnityEngine::CubemapFace Unknown;
		static const UnityEngine::CubemapFace PositiveX;
		static const UnityEngine::CubemapFace NegativeX;
		static const UnityEngine::CubemapFace PositiveY;
		static const UnityEngine::CubemapFace NegativeY;
		static const UnityEngine::CubemapFace PositiveZ;
		static const UnityEngine::CubemapFace NegativeZ;
		CubemapFace();
		explicit CubemapFace(int32_t value);
		explicit operator int32_t() const;
		bool operator==(CubemapFace other);
		bool operator!=(CubemapFace other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct Cubemap : virtual UnityEngine::Texture
	{
		Cubemap(decltype(nullptr));
		Cubemap(Plugin::InternalUse, int32_t handle);
		Cubemap(const Cubemap& other);
		Cubemap(Cubemap&& other);
		virtual ~Cubemap();
		Cubemap& operator=(const Cubemap& other);
		Cubemap& operator=(decltype(nullptr));
		Cubemap& operator=(Cubemap&& other);
		bool operator==(const Cubemap& other) const;
		bool operator!=(const Cubemap& other) const;
		Cubemap(System::Int32 width, UnityEngine::TextureFormat textureFormat, System::Boolean mipChain);
		void Apply(System::Boolean updateMipmaps, System::Boolean makeNoLongerReadable);
		void SetPixels(System::Array1<UnityEngine::Color>& colors, UnityEngine::CubemapFace face, System::Int32 miplevel);
		void SmoothEdges(System::Int32 smoothRegionWidthInPixels);
	};
}

namespace UnityEngine
{
	struct Material : virtual UnityEngine::Object
	{
		Material(decltype(nullptr));
		Material(Plugin::InternalUse, int32_t handle);
		Material(const Material& other);
		Material(Material&& other);
		virtual ~Material();
		Material& operator=(const Material& other);
		Material& operator=(decltype(nullptr));
		Material& operator=(Material&& other);
		bool operator==(const Material& other) const;
		bool operator!=(const Material& other) const;
		Material(UnityEngine::Shader& shader);
		UnityEngine::Color GetColor();
		void SetColor(UnityEngine::Color& value);
		UnityEngine::Texture GetMainTexture();
		void SetMainTexture(UnityEngine::Texture& value);
		UnityEngine::Vector2 GetMainTextureOffset();
		void SetMainTextureOffset(UnityEngine::Vector2& value);
		UnityEngine::Vector2 GetMainTextureScale();
		void SetMainTextureScale(UnityEngine::Vector2& value);
		System::Int32 GetRenderQueue();
		void SetRenderQueue(System::Int32 value);
		UnityEngine::Shader GetShader();
		void SetShader(UnityEngine::Shader& value);
		System::String GetName();
		void SetName(System::String& value);
		System::Boolean GetEnableInstancing();
		void SetEnableInstancing(System::Boolean value);
		UnityEngine::Color GetColor(System::String& name);
		System::Array1<UnityEngine::Color> GetColorArray(System::String& name);
		System::Single GetFloat(System::String& name);
		System::Array1<System::Single> GetFloatArray(System::String& name);
		System::Int32 GetInt(System::String& name);
		UnityEngine::Matrix4x4 GetMatrix(System::String& name);
		System::Array1<UnityEngine::Matrix4x4> GetMatrixArray(System::String& name);
		UnityEngine::Texture GetTexture(System::String& name);
		UnityEngine::Vector2 GetTextureOffset(System::String& name);
		UnityEngine::Vector2 GetTextureScale(System::String& name);
		UnityEngine::Vector4 GetVector(System::String& name);
		System::Array1<UnityEngine::Vector4> GetVectorArray(System::String& name);
		void SetColor(System::String& name, UnityEngine::Color& value);
		void SetColorArray(System::String& name, System::Array1<UnityEngine::Color>& values);
		void SetFloat(System::String& name, System::Single value);
		void SetFloatArray(System::String& name, System::Array1<System::Single>& values);
		void SetInt(System::String& name, System::Int32 value);
		void SetMatrix(System::String& name, UnityEngine::Matrix4x4& value);
		void SetMatrixArray(System::String& name, System::Array1<UnityEngine::Matrix4x4>& values);
		void SetTexture(System::String& name, UnityEngine::Texture& value);
		void SetTextureOffset(System::String& name, UnityEngine::Vector2& value);
		void SetTextureScale(System::String& name, UnityEngine::Vector2& value);
		void SetVector(System::String& name, UnityEngine::Vector4& value);
		void SetVectorArray(System::String& name, System::Array1<UnityEngine::Vector4>& values);
	};
}

namespace UnityEngine
{
	struct BoneWeight : Plugin::ManagedType
	{
		BoneWeight(decltype(nullptr));
		BoneWeight(Plugin::InternalUse, int32_t handle);
		BoneWeight(const BoneWeight& other);
		BoneWeight(BoneWeight&& other);
		virtual ~BoneWeight();
		BoneWeight& operator=(const BoneWeight& other);
		BoneWeight& operator=(decltype(nullptr));
		BoneWeight& operator=(BoneWeight&& other);
		bool operator==(const BoneWeight& other) const;
		bool operator!=(const BoneWeight& other) const;
		BoneWeight();
		System::Single GetWeight0();
		void SetWeight0(System::Single value);
		System::Single GetWeight1();
		void SetWeight1(System::Single value);
		System::Single GetWeight2();
		void SetWeight2(System::Single value);
		System::Single GetWeight3();
		void SetWeight3(System::Single value);
		System::Int32 GetBoneIndex0();
		void SetBoneIndex0(System::Int32 value);
		System::Int32 GetBoneIndex1();
		void SetBoneIndex1(System::Int32 value);
		System::Int32 GetBoneIndex2();
		void SetBoneIndex2(System::Int32 value);
		System::Int32 GetBoneIndex3();
		void SetBoneIndex3(System::Int32 value);
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IEquatable_1<UnityEngine::BoneWeight>();
	};
}

namespace UnityEngine
{
	namespace Rendering
	{
		struct IndexFormat
		{
			int32_t Value;
			static const UnityEngine::Rendering::IndexFormat UInt16;
			static const UnityEngine::Rendering::IndexFormat UInt32;
			IndexFormat();
			explicit IndexFormat(int32_t value);
			explicit operator int32_t() const;
			bool operator==(IndexFormat other);
			bool operator!=(IndexFormat other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UnityEngine
{
	struct Mesh : virtual UnityEngine::Object
	{
		Mesh(decltype(nullptr));
		Mesh(Plugin::InternalUse, int32_t handle);
		Mesh(const Mesh& other);
		Mesh(Mesh&& other);
		virtual ~Mesh();
		Mesh& operator=(const Mesh& other);
		Mesh& operator=(decltype(nullptr));
		Mesh& operator=(Mesh&& other);
		bool operator==(const Mesh& other) const;
		bool operator!=(const Mesh& other) const;
		Mesh();
		System::Array1<UnityEngine::Color> GetColors();
		void SetColors(System::Array1<UnityEngine::Color>& value);
		System::Array1<UnityEngine::Color32> GetColors32();
		void SetColors32(System::Array1<UnityEngine::Color32>& value);
		System::Array1<UnityEngine::Vector3> GetNormals();
		void SetNormals(System::Array1<UnityEngine::Vector3>& value);
		System::Int32 GetSubMeshCount();
		void SetSubMeshCount(System::Int32 value);
		System::Array1<UnityEngine::Vector4> GetTangents();
		void SetTangents(System::Array1<UnityEngine::Vector4>& value);
		System::Array1<System::Int32> GetTriangles();
		void SetTriangles(System::Array1<System::Int32>& value);
		System::Array1<UnityEngine::Vector2> GetUv();
		void SetUv(System::Array1<UnityEngine::Vector2>& value);
		System::Array1<UnityEngine::Vector2> GetUv2();
		void SetUv2(System::Array1<UnityEngine::Vector2>& value);
		System::Array1<UnityEngine::Vector2> GetUv3();
		void SetUv3(System::Array1<UnityEngine::Vector2>& value);
		System::Array1<UnityEngine::Vector2> GetUv4();
		void SetUv4(System::Array1<UnityEngine::Vector2>& value);
		System::Array1<UnityEngine::Matrix4x4> GetBindposes();
		void SetBindposes(System::Array1<UnityEngine::Matrix4x4>& value);
		System::Array1<UnityEngine::BoneWeight> GetBoneWeights();
		void SetBoneWeights(System::Array1<UnityEngine::BoneWeight>& value);
		System::Int32 GetVertexBufferCount();
		System::Int32 GetVertexCount();
		System::Array1<UnityEngine::Vector3> GetVertices();
		void SetVertices(System::Array1<UnityEngine::Vector3>& value);
		UnityEngine::Rendering::IndexFormat GetIndexFormat();
		void SetIndexFormat(UnityEngine::Rendering::IndexFormat value);
		void Clear();
		void MarkDynamic();
		void RecalculateBounds();
		void RecalculateNormals();
		void RecalculateTangents();
		void UploadMeshData(System::Boolean markNoLongerReadable);
		void SetTriangles(System::Array1<System::Int32>& triangles, System::Int32 submesh, System::Boolean calculateBounds, System::Int32 baseVertex);
		void SetTriangles(System::Array1<System::Int32>& triangles, System::Int32 submesh);
	};
}

namespace UnityEngine
{
	struct PhysicMaterial : virtual UnityEngine::Object
	{
		PhysicMaterial(decltype(nullptr));
		PhysicMaterial(Plugin::InternalUse, int32_t handle);
		PhysicMaterial(const PhysicMaterial& other);
		PhysicMaterial(PhysicMaterial&& other);
		virtual ~PhysicMaterial();
		PhysicMaterial& operator=(const PhysicMaterial& other);
		PhysicMaterial& operator=(decltype(nullptr));
		PhysicMaterial& operator=(PhysicMaterial&& other);
		bool operator==(const PhysicMaterial& other) const;
		bool operator!=(const PhysicMaterial& other) const;
		PhysicMaterial();
		UnityEngine::PhysicMaterialCombine GetBounceCombine();
		void SetBounceCombine(UnityEngine::PhysicMaterialCombine value);
		System::Single GetBounciness();
		void SetBounciness(System::Single value);
		System::Single GetDynamicFriction();
		void SetDynamicFriction(System::Single value);
		UnityEngine::PhysicMaterialCombine GetFrictionCombine();
		void SetFrictionCombine(UnityEngine::PhysicMaterialCombine value);
		System::Single GetStaticFriction();
		void SetStaticFriction(System::Single value);
	};
}

namespace UnityEngine
{
	struct Collider : virtual UnityEngine::Component
	{
		Collider(decltype(nullptr));
		Collider(Plugin::InternalUse, int32_t handle);
		Collider(const Collider& other);
		Collider(Collider&& other);
		virtual ~Collider();
		Collider& operator=(const Collider& other);
		Collider& operator=(decltype(nullptr));
		Collider& operator=(Collider&& other);
		bool operator==(const Collider& other) const;
		bool operator!=(const Collider& other) const;
		System::Boolean GetIsTrigger();
		void SetIsTrigger(System::Boolean value);
		UnityEngine::PhysicMaterial GetMaterial();
		void SetMaterial(UnityEngine::PhysicMaterial& value);
		UnityEngine::PhysicMaterial GetSharedMaterial();
		void SetSharedMaterial(UnityEngine::PhysicMaterial& value);
		System::Boolean GetEnabled();
		void SetEnabled(System::Boolean value);
	};
}

namespace UnityEngine
{
	struct BoxCollider : virtual UnityEngine::Collider
	{
		BoxCollider(decltype(nullptr));
		BoxCollider(Plugin::InternalUse, int32_t handle);
		BoxCollider(const BoxCollider& other);
		BoxCollider(BoxCollider&& other);
		virtual ~BoxCollider();
		BoxCollider& operator=(const BoxCollider& other);
		BoxCollider& operator=(decltype(nullptr));
		BoxCollider& operator=(BoxCollider&& other);
		bool operator==(const BoxCollider& other) const;
		bool operator!=(const BoxCollider& other) const;
		UnityEngine::Vector3 GetCenter();
		void SetCenter(UnityEngine::Vector3& value);
		UnityEngine::Vector3 GetSize();
		void SetSize(UnityEngine::Vector3& value);
	};
}

namespace UnityEngine
{
	struct SphereCollider : virtual UnityEngine::Collider
	{
		SphereCollider(decltype(nullptr));
		SphereCollider(Plugin::InternalUse, int32_t handle);
		SphereCollider(const SphereCollider& other);
		SphereCollider(SphereCollider&& other);
		virtual ~SphereCollider();
		SphereCollider& operator=(const SphereCollider& other);
		SphereCollider& operator=(decltype(nullptr));
		SphereCollider& operator=(SphereCollider&& other);
		bool operator==(const SphereCollider& other) const;
		bool operator!=(const SphereCollider& other) const;
		UnityEngine::Vector3 GetCenter();
		void SetCenter(UnityEngine::Vector3& value);
		System::Single GetRadius();
		void SetRadius(System::Single value);
	};
}

namespace UnityEngine
{
	struct CapsuleCollider : virtual UnityEngine::Collider
	{
		CapsuleCollider(decltype(nullptr));
		CapsuleCollider(Plugin::InternalUse, int32_t handle);
		CapsuleCollider(const CapsuleCollider& other);
		CapsuleCollider(CapsuleCollider&& other);
		virtual ~CapsuleCollider();
		CapsuleCollider& operator=(const CapsuleCollider& other);
		CapsuleCollider& operator=(decltype(nullptr));
		CapsuleCollider& operator=(CapsuleCollider&& other);
		bool operator==(const CapsuleCollider& other) const;
		bool operator!=(const CapsuleCollider& other) const;
		UnityEngine::Vector3 GetCenter();
		void SetCenter(UnityEngine::Vector3& value);
		System::Int32 GetDirection();
		void SetDirection(System::Int32 value);
		System::Single GetHeight();
		void SetHeight(System::Single value);
		System::Single GetRadius();
		void SetRadius(System::Single value);
	};
}

namespace UnityEngine
{
	struct MeshCollider : virtual UnityEngine::Collider
	{
		MeshCollider(decltype(nullptr));
		MeshCollider(Plugin::InternalUse, int32_t handle);
		MeshCollider(const MeshCollider& other);
		MeshCollider(MeshCollider&& other);
		virtual ~MeshCollider();
		MeshCollider& operator=(const MeshCollider& other);
		MeshCollider& operator=(decltype(nullptr));
		MeshCollider& operator=(MeshCollider&& other);
		bool operator==(const MeshCollider& other) const;
		bool operator!=(const MeshCollider& other) const;
		System::Boolean GetConvex();
		void SetConvex(System::Boolean value);
		UnityEngine::MeshColliderCookingOptions GetCookingOptions();
		void SetCookingOptions(UnityEngine::MeshColliderCookingOptions value);
		UnityEngine::Mesh GetSharedMesh();
		void SetSharedMesh(UnityEngine::Mesh& value);
	};
}

namespace UnityEngine
{
	struct MeshColliderCookingOptions
	{
		int32_t Value;
		static const UnityEngine::MeshColliderCookingOptions None;
		static const UnityEngine::MeshColliderCookingOptions InflateConvexMesh;
		static const UnityEngine::MeshColliderCookingOptions CookForFasterSimulation;
		static const UnityEngine::MeshColliderCookingOptions EnableMeshCleaning;
		static const UnityEngine::MeshColliderCookingOptions WeldColocatedVertices;
		MeshColliderCookingOptions();
		explicit MeshColliderCookingOptions(int32_t value);
		explicit operator int32_t() const;
		bool operator==(MeshColliderCookingOptions other);
		bool operator!=(MeshColliderCookingOptions other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct PhysicMaterialCombine
	{
		int32_t Value;
		static const UnityEngine::PhysicMaterialCombine Average;
		static const UnityEngine::PhysicMaterialCombine Minimum;
		static const UnityEngine::PhysicMaterialCombine Multiply;
		static const UnityEngine::PhysicMaterialCombine Maximum;
		PhysicMaterialCombine();
		explicit PhysicMaterialCombine(int32_t value);
		explicit operator int32_t() const;
		bool operator==(PhysicMaterialCombine other);
		bool operator!=(PhysicMaterialCombine other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct Light : virtual UnityEngine::Behaviour
	{
		Light(decltype(nullptr));
		Light(Plugin::InternalUse, int32_t handle);
		Light(const Light& other);
		Light(Light&& other);
		virtual ~Light();
		Light& operator=(const Light& other);
		Light& operator=(decltype(nullptr));
		Light& operator=(Light&& other);
		bool operator==(const Light& other) const;
		bool operator!=(const Light& other) const;
		UnityEngine::Color GetColor();
		void SetColor(UnityEngine::Color& value);
		UnityEngine::Texture GetCookie();
		void SetCookie(UnityEngine::Texture& value);
		System::Single GetCookieSize();
		void SetCookieSize(System::Single value);
		System::Int32 GetCullingMask();
		void SetCullingMask(System::Int32 value);
		System::Single GetBounceIntensity();
		void SetBounceIntensity(System::Single value);
		System::Single GetIntensity();
		void SetIntensity(System::Single value);
		System::Single GetRange();
		void SetRange(System::Single value);
		UnityEngine::LightShadows GetShadows();
		void SetShadows(UnityEngine::LightShadows value);
		System::Single GetShadowStrength();
		void SetShadowStrength(System::Single value);
		System::Single GetSpotAngle();
		void SetSpotAngle(System::Single value);
		UnityEngine::LightType GetType();
		void SetType(UnityEngine::LightType value);
	};
}

namespace UnityEngine
{
	struct Projector : virtual UnityEngine::Behaviour
	{
		Projector(decltype(nullptr));
		Projector(Plugin::InternalUse, int32_t handle);
		Projector(const Projector& other);
		Projector(Projector&& other);
		virtual ~Projector();
		Projector& operator=(const Projector& other);
		Projector& operator=(decltype(nullptr));
		Projector& operator=(Projector&& other);
		bool operator==(const Projector& other) const;
		bool operator!=(const Projector& other) const;
		System::Single GetAspectRatio();
		void SetAspectRatio(System::Single value);
		System::Single GetFarClipPlane();
		void SetFarClipPlane(System::Single value);
		System::Single GetFieldOfView();
		void SetFieldOfView(System::Single value);
		System::Int32 GetIgnoreLayers();
		void SetIgnoreLayers(System::Int32 value);
		UnityEngine::Material GetMaterial();
		void SetMaterial(UnityEngine::Material& value);
		System::Single GetNearClipPlane();
		void SetNearClipPlane(System::Single value);
		System::Boolean GetOrthographic();
		void SetOrthographic(System::Boolean value);
		System::Single GetOrthographicSize();
		void SetOrthographicSize(System::Single value);
	};
}

namespace UnityEngine
{
	struct LightType
	{
		int32_t Value;
		static const UnityEngine::LightType Spot;
		static const UnityEngine::LightType Directional;
		static const UnityEngine::LightType Point;
		static const UnityEngine::LightType Area;
		static const UnityEngine::LightType Rectangle;
		static const UnityEngine::LightType Disc;
		LightType();
		explicit LightType(int32_t value);
		explicit operator int32_t() const;
		bool operator==(LightType other);
		bool operator!=(LightType other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct Application : virtual System::Object
	{
		Application(decltype(nullptr));
		Application(Plugin::InternalUse, int32_t handle);
		Application(const Application& other);
		Application(Application&& other);
		virtual ~Application();
		Application& operator=(const Application& other);
		Application& operator=(decltype(nullptr));
		Application& operator=(Application&& other);
		bool operator==(const Application& other) const;
		bool operator!=(const Application& other) const;
		static System::String GetStreamingAssetsPath();
		static System::Boolean GetIsEditor();
	};
}

namespace System
{
	namespace Text
	{
		struct StringBuilder : virtual System::Runtime::Serialization::ISerializable
		{
			StringBuilder(decltype(nullptr));
			StringBuilder(Plugin::InternalUse, int32_t handle);
			StringBuilder(const StringBuilder& other);
			StringBuilder(StringBuilder&& other);
			virtual ~StringBuilder();
			StringBuilder& operator=(const StringBuilder& other);
			StringBuilder& operator=(decltype(nullptr));
			StringBuilder& operator=(StringBuilder&& other);
			bool operator==(const StringBuilder& other) const;
			bool operator!=(const StringBuilder& other) const;
			StringBuilder(System::String& value);
		};
	}
}

namespace UnityEngine
{
	struct LightShadows
	{
		int32_t Value;
		static const UnityEngine::LightShadows None;
		static const UnityEngine::LightShadows Hard;
		static const UnityEngine::LightShadows Soft;
		LightShadows();
		explicit LightShadows(int32_t value);
		explicit operator int32_t() const;
		bool operator==(LightShadows other);
		bool operator!=(LightShadows other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct Animation : virtual UnityEngine::Behaviour, virtual System::Collections::IEnumerable
	{
		Animation(decltype(nullptr));
		Animation(Plugin::InternalUse, int32_t handle);
		Animation(const Animation& other);
		Animation(Animation&& other);
		virtual ~Animation();
		Animation& operator=(const Animation& other);
		Animation& operator=(decltype(nullptr));
		Animation& operator=(Animation&& other);
		bool operator==(const Animation& other) const;
		bool operator!=(const Animation& other) const;
	};
}

namespace System
{
	struct Exception : virtual System::Runtime::InteropServices::_Exception, virtual System::Runtime::Serialization::ISerializable
	{
		Exception(decltype(nullptr));
		Exception(Plugin::InternalUse, int32_t handle);
		Exception(const Exception& other);
		Exception(Exception&& other);
		virtual ~Exception();
		Exception& operator=(const Exception& other);
		Exception& operator=(decltype(nullptr));
		Exception& operator=(Exception&& other);
		bool operator==(const Exception& other) const;
		bool operator!=(const Exception& other) const;
		Exception(System::String& message);
	};
}

namespace System
{
	struct SystemException : virtual System::Exception, virtual System::Runtime::InteropServices::_Exception, virtual System::Runtime::Serialization::ISerializable
	{
		SystemException(decltype(nullptr));
		SystemException(Plugin::InternalUse, int32_t handle);
		SystemException(const SystemException& other);
		SystemException(SystemException&& other);
		virtual ~SystemException();
		SystemException& operator=(const SystemException& other);
		SystemException& operator=(decltype(nullptr));
		SystemException& operator=(SystemException&& other);
		bool operator==(const SystemException& other) const;
		bool operator!=(const SystemException& other) const;
	};
}

namespace System
{
	struct NullReferenceException : virtual System::SystemException, virtual System::Runtime::InteropServices::_Exception, virtual System::Runtime::Serialization::ISerializable
	{
		NullReferenceException(decltype(nullptr));
		NullReferenceException(Plugin::InternalUse, int32_t handle);
		NullReferenceException(const NullReferenceException& other);
		NullReferenceException(NullReferenceException&& other);
		virtual ~NullReferenceException();
		NullReferenceException& operator=(const NullReferenceException& other);
		NullReferenceException& operator=(decltype(nullptr));
		NullReferenceException& operator=(NullReferenceException&& other);
		bool operator==(const NullReferenceException& other) const;
		bool operator!=(const NullReferenceException& other) const;
	};
}

namespace UnityEngine
{
	struct PrimitiveType
	{
		int32_t Value;
		static const UnityEngine::PrimitiveType Sphere;
		static const UnityEngine::PrimitiveType Capsule;
		static const UnityEngine::PrimitiveType Cylinder;
		static const UnityEngine::PrimitiveType Cube;
		static const UnityEngine::PrimitiveType Plane;
		static const UnityEngine::PrimitiveType Quad;
		PrimitiveType();
		explicit PrimitiveType(int32_t value);
		explicit operator int32_t() const;
		bool operator==(PrimitiveType other);
		bool operator!=(PrimitiveType other);
		explicit operator System::Enum();
		explicit operator System::ValueType();
		explicit operator System::Object();
		explicit operator System::IComparable();
		explicit operator System::IConvertible();
		explicit operator System::IFormattable();
	};
}

namespace UnityEngine
{
	struct Time : virtual System::Object
	{
		Time(decltype(nullptr));
		Time(Plugin::InternalUse, int32_t handle);
		Time(const Time& other);
		Time(Time&& other);
		virtual ~Time();
		Time& operator=(const Time& other);
		Time& operator=(decltype(nullptr));
		Time& operator=(Time&& other);
		bool operator==(const Time& other) const;
		bool operator!=(const Time& other) const;
		static System::Single GetDeltaTime();
	};
}

namespace UnityEngine
{
	struct StaticBatchingUtility : virtual System::Object
	{
		StaticBatchingUtility(decltype(nullptr));
		StaticBatchingUtility(Plugin::InternalUse, int32_t handle);
		StaticBatchingUtility(const StaticBatchingUtility& other);
		StaticBatchingUtility(StaticBatchingUtility&& other);
		virtual ~StaticBatchingUtility();
		StaticBatchingUtility& operator=(const StaticBatchingUtility& other);
		StaticBatchingUtility& operator=(decltype(nullptr));
		StaticBatchingUtility& operator=(StaticBatchingUtility&& other);
		bool operator==(const StaticBatchingUtility& other) const;
		bool operator!=(const StaticBatchingUtility& other) const;
		static void Combine(System::Array1<UnityEngine::GameObject>& gos, UnityEngine::GameObject& staticBatchRoot);
	};
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlasmaManager : virtual UnityEngine::MonoBehaviour
		{
			PlasmaManager(decltype(nullptr));
			PlasmaManager(Plugin::InternalUse, int32_t handle);
			PlasmaManager(const PlasmaManager& other);
			PlasmaManager(PlasmaManager&& other);
			virtual ~PlasmaManager();
			PlasmaManager& operator=(const PlasmaManager& other);
			PlasmaManager& operator=(decltype(nullptr));
			PlasmaManager& operator=(PlasmaManager&& other);
			bool operator==(const PlasmaManager& other) const;
			bool operator!=(const PlasmaManager& other) const;
			static UPlasma::PlEmu::PlasmaManager GetInstance();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlGameFileAccessor : virtual UnityEngine::MonoBehaviour
		{
			PlGameFileAccessor(decltype(nullptr));
			PlGameFileAccessor(Plugin::InternalUse, int32_t handle);
			PlGameFileAccessor(const PlGameFileAccessor& other);
			PlGameFileAccessor(PlGameFileAccessor&& other);
			virtual ~PlGameFileAccessor();
			PlGameFileAccessor& operator=(const PlGameFileAccessor& other);
			PlGameFileAccessor& operator=(decltype(nullptr));
			PlGameFileAccessor& operator=(PlGameFileAccessor&& other);
			bool operator==(const PlGameFileAccessor& other) const;
			bool operator!=(const PlGameFileAccessor& other) const;
			static UPlasma::PlEmu::PlGameFileAccessor GetInstance();
			System::String GetFile(System::String& fname);
			System::Array1<System::String> GetFilesByModificationDateDescending(System::String& folder);
			UPlasma::PlEmu::PlasmaGame GetFileGameType(System::String& fname);
			System::Boolean FileExists(System::String& fname);
			System::Boolean DirectoryExists(System::String& fname);
			System::String GetTexOverride(System::String& ageName, System::String& pageName, System::String& fileName);
			System::String GetAnimOverride(System::String& ageName, System::String& pageName, System::String& fileName);
			System::String GetDrawMeshOverride(System::String& ageName, System::String& pageName, System::String& fileName);
			System::String GetPhysMeshOverride(System::String& ageName, System::String& pageName, System::String& fileName);
			UPlasma::PlEmu::PrpImportClues GetPrpLoadClues(System::String& ageName, System::String& pageName, UPlasma::PlEmu::PlasmaGame game);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AbstractBasePlasmaNativeManager : virtual UnityEngine::MonoBehaviour
		{
			AbstractBasePlasmaNativeManager(decltype(nullptr));
			AbstractBasePlasmaNativeManager(Plugin::InternalUse, int32_t handle);
			AbstractBasePlasmaNativeManager(const AbstractBasePlasmaNativeManager& other);
			AbstractBasePlasmaNativeManager(AbstractBasePlasmaNativeManager&& other);
			virtual ~AbstractBasePlasmaNativeManager();
			AbstractBasePlasmaNativeManager& operator=(const AbstractBasePlasmaNativeManager& other);
			AbstractBasePlasmaNativeManager& operator=(decltype(nullptr));
			AbstractBasePlasmaNativeManager& operator=(AbstractBasePlasmaNativeManager&& other);
			bool operator==(const AbstractBasePlasmaNativeManager& other) const;
			bool operator!=(const AbstractBasePlasmaNativeManager& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct BasePlasmaNativeManager : virtual UPlasma::PlEmu::AbstractBasePlasmaNativeManager
		{
			BasePlasmaNativeManager(decltype(nullptr));
			BasePlasmaNativeManager(Plugin::InternalUse, int32_t handle);
			BasePlasmaNativeManager(const BasePlasmaNativeManager& other);
			BasePlasmaNativeManager(BasePlasmaNativeManager&& other);
			virtual ~BasePlasmaNativeManager();
			BasePlasmaNativeManager& operator=(const BasePlasmaNativeManager& other);
			BasePlasmaNativeManager& operator=(decltype(nullptr));
			BasePlasmaNativeManager& operator=(BasePlasmaNativeManager&& other);
			bool operator==(const BasePlasmaNativeManager& other) const;
			bool operator!=(const BasePlasmaNativeManager& other) const;
			int32_t CppHandle;
			BasePlasmaNativeManager();
			virtual void Start();
			virtual void SetCachePath(System::String& path);
			virtual void SetToolsPath(System::String& path);
			virtual void LoadAge(System::String& ageName);
			virtual void LoadPages(System::String& ageName, System::Array1<System::String>& pageNames);
			virtual void ReadPythonPak(System::String& pakName);
			virtual void ReadPythonPaks();
			virtual void UnloadPythonPak(System::String& pakName);
			virtual void GetPythonScript(System::String& pythonScriptName, UPlasma::PlEmu::HackReturnObject& pythonScript);
			virtual System::Boolean HasPythonScript(System::String& pythonScriptName);
			virtual void ReadEncryptedFile(System::String& filePath, UPlasma::PlEmu::HackReturnObject& decryptedFile);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlasmaGame
		{
			int32_t Value;
			static const UPlasma::PlEmu::PlasmaGame CompleteChronicles;
			static const UPlasma::PlEmu::PlasmaGame MystOnline;
			static const UPlasma::PlEmu::PlasmaGame EndOfAges;
			static const UPlasma::PlEmu::PlasmaGame CrowThistle;
			PlasmaGame();
			explicit PlasmaGame(int32_t value);
			explicit operator int32_t() const;
			bool operator==(PlasmaGame other);
			bool operator!=(PlasmaGame other);
			explicit operator System::Enum();
			explicit operator System::ValueType();
			explicit operator System::Object();
			explicit operator System::IComparable();
			explicit operator System::IConvertible();
			explicit operator System::IFormattable();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct HackReturnObject : virtual System::Object
		{
			HackReturnObject(decltype(nullptr));
			HackReturnObject(Plugin::InternalUse, int32_t handle);
			HackReturnObject(const HackReturnObject& other);
			HackReturnObject(HackReturnObject&& other);
			virtual ~HackReturnObject();
			HackReturnObject& operator=(const HackReturnObject& other);
			HackReturnObject& operator=(decltype(nullptr));
			HackReturnObject& operator=(HackReturnObject&& other);
			bool operator==(const HackReturnObject& other) const;
			bool operator!=(const HackReturnObject& other) const;
			System::Object GetVal();
			void SetVal(System::Object& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlConsole : virtual UnityEngine::MonoBehaviour
		{
			PlConsole(decltype(nullptr));
			PlConsole(Plugin::InternalUse, int32_t handle);
			PlConsole(const PlConsole& other);
			PlConsole(PlConsole&& other);
			virtual ~PlConsole();
			PlConsole& operator=(const PlConsole& other);
			PlConsole& operator=(decltype(nullptr));
			PlConsole& operator=(PlConsole&& other);
			bool operator==(const PlConsole& other) const;
			bool operator!=(const PlConsole& other) const;
			static UPlasma::PlEmu::PlConsole GetInstance();
			System::Boolean Parse(System::String& content);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAge : virtual UnityEngine::MonoBehaviour
		{
			PlAge(decltype(nullptr));
			PlAge(Plugin::InternalUse, int32_t handle);
			PlAge(const PlAge& other);
			PlAge(PlAge&& other);
			virtual ~PlAge();
			PlAge& operator=(const PlAge& other);
			PlAge& operator=(decltype(nullptr));
			PlAge& operator=(PlAge&& other);
			bool operator==(const PlAge& other) const;
			bool operator!=(const PlAge& other) const;
			System::Int32 GetSequencePrefix();
			void SetSequencePrefix(System::Int32 value);
			System::Collections::Generic::List_1<UPlasma::PlEmu::PlPage> GetPages();
			void SetPages(System::Collections::Generic::List_1<UPlasma::PlEmu::PlPage>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPage : virtual UnityEngine::MonoBehaviour
		{
			PlPage(decltype(nullptr));
			PlPage(Plugin::InternalUse, int32_t handle);
			PlPage(const PlPage& other);
			PlPage(PlPage&& other);
			virtual ~PlPage();
			PlPage& operator=(const PlPage& other);
			PlPage& operator=(decltype(nullptr));
			PlPage& operator=(PlPage&& other);
			bool operator==(const PlPage& other) const;
			bool operator!=(const PlPage& other) const;
			System::Int32 GetId();
			void SetId(System::Int32 value);
			System::Collections::Generic::List_1<UnityEngine::GameObject> GetPageObjects();
			void SetPageObjects(System::Collections::Generic::List_1<UnityEngine::GameObject>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ICannotBeBatched : virtual System::Object
		{
			ICannotBeBatched(decltype(nullptr));
			ICannotBeBatched(Plugin::InternalUse, int32_t handle);
			ICannotBeBatched(const ICannotBeBatched& other);
			ICannotBeBatched(ICannotBeBatched&& other);
			virtual ~ICannotBeBatched();
			ICannotBeBatched& operator=(const ICannotBeBatched& other);
			ICannotBeBatched& operator=(decltype(nullptr));
			ICannotBeBatched& operator=(ICannotBeBatched&& other);
			bool operator==(const ICannotBeBatched& other) const;
			bool operator!=(const ICannotBeBatched& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct IPlMessageable : virtual System::Object
		{
			IPlMessageable(decltype(nullptr));
			IPlMessageable(Plugin::InternalUse, int32_t handle);
			IPlMessageable(const IPlMessageable& other);
			IPlMessageable(IPlMessageable&& other);
			virtual ~IPlMessageable();
			IPlMessageable& operator=(const IPlMessageable& other);
			IPlMessageable& operator=(decltype(nullptr));
			IPlMessageable& operator=(IPlMessageable&& other);
			bool operator==(const IPlMessageable& other) const;
			bool operator!=(const IPlMessageable& other) const;
			System::Boolean MsgReceive(UPlasma::PlEmu::PlMessage& msg);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSceneObject : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlSceneObject(decltype(nullptr));
			PlSceneObject(Plugin::InternalUse, int32_t handle);
			PlSceneObject(const PlSceneObject& other);
			PlSceneObject(PlSceneObject&& other);
			virtual ~PlSceneObject();
			PlSceneObject& operator=(const PlSceneObject& other);
			PlSceneObject& operator=(decltype(nullptr));
			PlSceneObject& operator=(PlSceneObject&& other);
			bool operator==(const PlSceneObject& other) const;
			bool operator!=(const PlSceneObject& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCameraModifier : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlCameraModifier(decltype(nullptr));
			PlCameraModifier(Plugin::InternalUse, int32_t handle);
			PlCameraModifier(const PlCameraModifier& other);
			PlCameraModifier(PlCameraModifier&& other);
			virtual ~PlCameraModifier();
			PlCameraModifier& operator=(const PlCameraModifier& other);
			PlCameraModifier& operator=(decltype(nullptr));
			PlCameraModifier& operator=(PlCameraModifier&& other);
			bool operator==(const PlCameraModifier& other) const;
			bool operator!=(const PlCameraModifier& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEAXSourceSoftSettings : virtual System::Object
		{
			PlEAXSourceSoftSettings(decltype(nullptr));
			PlEAXSourceSoftSettings(Plugin::InternalUse, int32_t handle);
			PlEAXSourceSoftSettings(const PlEAXSourceSoftSettings& other);
			PlEAXSourceSoftSettings(PlEAXSourceSoftSettings&& other);
			virtual ~PlEAXSourceSoftSettings();
			PlEAXSourceSoftSettings& operator=(const PlEAXSourceSoftSettings& other);
			PlEAXSourceSoftSettings& operator=(decltype(nullptr));
			PlEAXSourceSoftSettings& operator=(PlEAXSourceSoftSettings&& other);
			bool operator==(const PlEAXSourceSoftSettings& other) const;
			bool operator!=(const PlEAXSourceSoftSettings& other) const;
			PlEAXSourceSoftSettings();
			System::Int16 GetOcclusion();
			void SetOcclusion(System::Int16 value);
			System::Single GetOcclusionLFRatio();
			void SetOcclusionLFRatio(System::Single value);
			System::Single GetOcclusionRoomRatio();
			void SetOcclusionRoomRatio(System::Single value);
			System::Single GetOcclusionDirectRatio();
			void SetOcclusionDirectRatio(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEAXSourceSettings : virtual System::Object
		{
			PlEAXSourceSettings(decltype(nullptr));
			PlEAXSourceSettings(Plugin::InternalUse, int32_t handle);
			PlEAXSourceSettings(const PlEAXSourceSettings& other);
			PlEAXSourceSettings(PlEAXSourceSettings&& other);
			virtual ~PlEAXSourceSettings();
			PlEAXSourceSettings& operator=(const PlEAXSourceSettings& other);
			PlEAXSourceSettings& operator=(decltype(nullptr));
			PlEAXSourceSettings& operator=(PlEAXSourceSettings&& other);
			bool operator==(const PlEAXSourceSettings& other) const;
			bool operator!=(const PlEAXSourceSettings& other) const;
			PlEAXSourceSettings();
			System::Int16 GetRoom();
			void SetRoom(System::Int16 value);
			System::Int16 GetRoomHF();
			void SetRoomHF(System::Int16 value);
			System::Boolean GetEnabled();
			void SetEnabled(System::Boolean value);
			System::Boolean GetRoomAuto();
			void SetRoomAuto(System::Boolean value);
			System::Boolean GetRoomHFAuto();
			void SetRoomHFAuto(System::Boolean value);
			System::Int16 GetOutsideVolHF();
			void SetOutsideVolHF(System::Int16 value);
			System::Single GetAirAbsorptionFactor();
			void SetAirAbsorptionFactor(System::Single value);
			System::Single GetRoomRolloffFactor();
			void SetRoomRolloffFactor(System::Single value);
			System::Single GetDopplerFactor();
			void SetDopplerFactor(System::Single value);
			System::Single GetRolloffFactor();
			void SetRolloffFactor(System::Single value);
			UPlasma::PlEmu::PlEAXSourceSoftSettings GetSoftStarts();
			void SetSoftStarts(UPlasma::PlEmu::PlEAXSourceSoftSettings& value);
			UPlasma::PlEmu::PlEAXSourceSoftSettings GetSoftEnds();
			void SetSoftEnds(UPlasma::PlEmu::PlEAXSourceSoftSettings& value);
			System::Single GetOcclusionSoftValue();
			void SetOcclusionSoftValue(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFadeParams : virtual System::Object
		{
			PlFadeParams(decltype(nullptr));
			PlFadeParams(Plugin::InternalUse, int32_t handle);
			PlFadeParams(const PlFadeParams& other);
			PlFadeParams(PlFadeParams&& other);
			virtual ~PlFadeParams();
			PlFadeParams& operator=(const PlFadeParams& other);
			PlFadeParams& operator=(decltype(nullptr));
			PlFadeParams& operator=(PlFadeParams&& other);
			bool operator==(const PlFadeParams& other) const;
			bool operator!=(const PlFadeParams& other) const;
			PlFadeParams();
			System::Int32 GetTypeInt();
			void SetTypeInt(System::Int32 value);
			System::Single GetLengthInSecs();
			void SetLengthInSecs(System::Single value);
			System::Single GetVolStart();
			void SetVolStart(System::Single value);
			System::Single GetVolEnd();
			void SetVolEnd(System::Single value);
			System::Boolean GetStopWhenDone();
			void SetStopWhenDone(System::Boolean value);
			System::Boolean GetFadeSoftVol();
			void SetFadeSoftVol(System::Boolean value);
			System::Single GetCurrTime();
			void SetCurrTime(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlWAVHeader : virtual System::Object
		{
			PlWAVHeader(decltype(nullptr));
			PlWAVHeader(Plugin::InternalUse, int32_t handle);
			PlWAVHeader(const PlWAVHeader& other);
			PlWAVHeader(PlWAVHeader&& other);
			virtual ~PlWAVHeader();
			PlWAVHeader& operator=(const PlWAVHeader& other);
			PlWAVHeader& operator=(decltype(nullptr));
			PlWAVHeader& operator=(PlWAVHeader&& other);
			bool operator==(const PlWAVHeader& other) const;
			bool operator!=(const PlWAVHeader& other) const;
			PlWAVHeader();
			System::Int32 GetFormatTag();
			void SetFormatTag(System::Int32 value);
			System::Int32 GetNumChannels();
			void SetNumChannels(System::Int32 value);
			System::Int32 GetNumSamplesPerSec();
			void SetNumSamplesPerSec(System::Int32 value);
			System::Int32 GetAvgBytesPerSec();
			void SetAvgBytesPerSec(System::Int32 value);
			System::Int32 GetBlockAlign();
			void SetBlockAlign(System::Int32 value);
			System::Int32 GetBitsPerSample();
			void SetBitsPerSample(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSoundBuffer : virtual System::Object
		{
			PlSoundBuffer(decltype(nullptr));
			PlSoundBuffer(Plugin::InternalUse, int32_t handle);
			PlSoundBuffer(const PlSoundBuffer& other);
			PlSoundBuffer(PlSoundBuffer&& other);
			virtual ~PlSoundBuffer();
			PlSoundBuffer& operator=(const PlSoundBuffer& other);
			PlSoundBuffer& operator=(decltype(nullptr));
			PlSoundBuffer& operator=(PlSoundBuffer&& other);
			bool operator==(const PlSoundBuffer& other) const;
			bool operator!=(const PlSoundBuffer& other) const;
			PlSoundBuffer();
			System::Int32 GetFlagsInt();
			void SetFlagsInt(System::Int32 value);
			UPlasma::PlEmu::PlWAVHeader GetHeader();
			void SetHeader(UPlasma::PlEmu::PlWAVHeader& value);
			System::String GetFileName();
			void SetFileName(System::String& value);
			System::Int32 GetDataLength();
			void SetDataLength(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSound : virtual UnityEngine::MonoBehaviour
		{
			PlSound(decltype(nullptr));
			PlSound(Plugin::InternalUse, int32_t handle);
			PlSound(const PlSound& other);
			PlSound(PlSound&& other);
			virtual ~PlSound();
			PlSound& operator=(const PlSound& other);
			PlSound& operator=(decltype(nullptr));
			PlSound& operator=(PlSound&& other);
			bool operator==(const PlSound& other) const;
			bool operator!=(const PlSound& other) const;
			System::Int32 GetPropertiesInt();
			void SetPropertiesInt(System::Int32 value);
			System::Int32 GetTypeInt();
			void SetTypeInt(System::Int32 value);
			System::Boolean GetChannel();
			void SetChannel(System::Boolean value);
			System::Int32 GetPriority();
			void SetPriority(System::Int32 value);
			System::Boolean GetPlaying();
			void SetPlaying(System::Boolean value);
			System::Double GetTime();
			void SetTime(System::Double value);
			System::Int32 GetMaxFalloff();
			void SetMaxFalloff(System::Int32 value);
			System::Int32 GetMinFalloff();
			void SetMinFalloff(System::Int32 value);
			System::Int32 GetOuterVol();
			void SetOuterVol(System::Int32 value);
			System::Int32 GetInnerCone();
			void SetInnerCone(System::Int32 value);
			System::Int32 GetOuterCone();
			void SetOuterCone(System::Int32 value);
			System::Single GetCurrVolume();
			void SetCurrVolume(System::Single value);
			System::Single GetDesiredVol();
			void SetDesiredVol(System::Single value);
			System::Single GetFadedVolume();
			void SetFadedVolume(System::Single value);
			UPlasma::PlEmu::PlEAXSourceSettings GetEAXSettings();
			void SetEAXSettings(UPlasma::PlEmu::PlEAXSourceSettings& value);
			UPlasma::PlEmu::PlFadeParams GetFadeInParams();
			void SetFadeInParams(UPlasma::PlEmu::PlFadeParams& value);
			UPlasma::PlEmu::PlFadeParams GetFadeOutParams();
			void SetFadeOutParams(UPlasma::PlEmu::PlFadeParams& value);
			UPlasma::PlEmu::PlSoundBuffer GetDataBuffer();
			void SetDataBuffer(UPlasma::PlEmu::PlSoundBuffer& value);
			System::String GetSubtitleId();
			void SetSubtitleId(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCluster : virtual UnityEngine::MonoBehaviour
		{
			PlCluster(decltype(nullptr));
			PlCluster(Plugin::InternalUse, int32_t handle);
			PlCluster(const PlCluster& other);
			PlCluster(PlCluster&& other);
			virtual ~PlCluster();
			PlCluster& operator=(const PlCluster& other);
			PlCluster& operator=(decltype(nullptr));
			PlCluster& operator=(PlCluster&& other);
			bool operator==(const PlCluster& other) const;
			bool operator!=(const PlCluster& other) const;
			UPlasma::PlEmu::PlClusterGroup GetGroup();
			void SetGroup(UPlasma::PlEmu::PlClusterGroup& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLodDist
		{
			PlLodDist();
			System::Single minDist;
			System::Single maxDist;
			explicit operator System::ValueType();
			explicit operator System::Object();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlClusterGroup : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlClusterGroup(decltype(nullptr));
			PlClusterGroup(Plugin::InternalUse, int32_t handle);
			PlClusterGroup(const PlClusterGroup& other);
			PlClusterGroup(PlClusterGroup&& other);
			virtual ~PlClusterGroup();
			PlClusterGroup& operator=(const PlClusterGroup& other);
			PlClusterGroup& operator=(decltype(nullptr));
			PlClusterGroup& operator=(PlClusterGroup&& other);
			bool operator==(const PlClusterGroup& other) const;
			bool operator!=(const PlClusterGroup& other) const;
			UnityEngine::Material GetMaterial();
			void SetMaterial(UnityEngine::Material& value);
			System::Collections::Generic::List_1<UPlasma::PlEmu::PlCluster> GetClusters();
			void SetClusters(System::Collections::Generic::List_1<UPlasma::PlEmu::PlCluster>& value);
			UPlasma::PlEmu::PlLodDist GetLod();
			void SetLod(UPlasma::PlEmu::PlLodDist& value);
			System::Int32 GetRenderLevel();
			void SetRenderLevel(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAudible : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlAudible(decltype(nullptr));
			PlAudible(Plugin::InternalUse, int32_t handle);
			PlAudible(const PlAudible& other);
			PlAudible(PlAudible&& other);
			virtual ~PlAudible();
			PlAudible& operator=(const PlAudible& other);
			PlAudible& operator=(decltype(nullptr));
			PlAudible& operator=(PlAudible&& other);
			bool operator==(const PlAudible& other) const;
			bool operator!=(const PlAudible& other) const;
			System::Array1<UPlasma::PlEmu::PlSound> GetSounds();
			void SetSounds(System::Array1<UPlasma::PlEmu::PlSound>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<UnityEngine::Matrix4x4> : virtual System::Object
		{
			IKeyFrame_1<UnityEngine::Matrix4x4>(decltype(nullptr));
			IKeyFrame_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
			IKeyFrame_1<UnityEngine::Matrix4x4>(const IKeyFrame_1<UnityEngine::Matrix4x4>& other);
			IKeyFrame_1<UnityEngine::Matrix4x4>(IKeyFrame_1<UnityEngine::Matrix4x4>&& other);
			virtual ~IKeyFrame_1<UnityEngine::Matrix4x4>();
			IKeyFrame_1<UnityEngine::Matrix4x4>& operator=(const IKeyFrame_1<UnityEngine::Matrix4x4>& other);
			IKeyFrame_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
			IKeyFrame_1<UnityEngine::Matrix4x4>& operator=(IKeyFrame_1<UnityEngine::Matrix4x4>&& other);
			bool operator==(const IKeyFrame_1<UnityEngine::Matrix4x4>& other) const;
			bool operator!=(const IKeyFrame_1<UnityEngine::Matrix4x4>& other) const;
			System::Single GetTime();
			void SetTime(System::Single value);
			UnityEngine::Matrix4x4 GetValue();
			void SetValue(UnityEngine::Matrix4x4& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<UnityEngine::Vector3> : virtual System::Object
		{
			IKeyFrame_1<UnityEngine::Vector3>(decltype(nullptr));
			IKeyFrame_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
			IKeyFrame_1<UnityEngine::Vector3>(const IKeyFrame_1<UnityEngine::Vector3>& other);
			IKeyFrame_1<UnityEngine::Vector3>(IKeyFrame_1<UnityEngine::Vector3>&& other);
			virtual ~IKeyFrame_1<UnityEngine::Vector3>();
			IKeyFrame_1<UnityEngine::Vector3>& operator=(const IKeyFrame_1<UnityEngine::Vector3>& other);
			IKeyFrame_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
			IKeyFrame_1<UnityEngine::Vector3>& operator=(IKeyFrame_1<UnityEngine::Vector3>&& other);
			bool operator==(const IKeyFrame_1<UnityEngine::Vector3>& other) const;
			bool operator!=(const IKeyFrame_1<UnityEngine::Vector3>& other) const;
			System::Single GetTime();
			void SetTime(System::Single value);
			UnityEngine::Vector3 GetValue();
			void SetValue(UnityEngine::Vector3& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<UnityEngine::Quaternion> : virtual System::Object
		{
			IKeyFrame_1<UnityEngine::Quaternion>(decltype(nullptr));
			IKeyFrame_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
			IKeyFrame_1<UnityEngine::Quaternion>(const IKeyFrame_1<UnityEngine::Quaternion>& other);
			IKeyFrame_1<UnityEngine::Quaternion>(IKeyFrame_1<UnityEngine::Quaternion>&& other);
			virtual ~IKeyFrame_1<UnityEngine::Quaternion>();
			IKeyFrame_1<UnityEngine::Quaternion>& operator=(const IKeyFrame_1<UnityEngine::Quaternion>& other);
			IKeyFrame_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
			IKeyFrame_1<UnityEngine::Quaternion>& operator=(IKeyFrame_1<UnityEngine::Quaternion>&& other);
			bool operator==(const IKeyFrame_1<UnityEngine::Quaternion>& other) const;
			bool operator!=(const IKeyFrame_1<UnityEngine::Quaternion>& other) const;
			System::Single GetTime();
			void SetTime(System::Single value);
			UnityEngine::Quaternion GetValue();
			void SetValue(UnityEngine::Quaternion& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IKeyFrame_1<System::Single> : virtual System::Object
		{
			IKeyFrame_1<System::Single>(decltype(nullptr));
			IKeyFrame_1<System::Single>(Plugin::InternalUse, int32_t handle);
			IKeyFrame_1<System::Single>(const IKeyFrame_1<System::Single>& other);
			IKeyFrame_1<System::Single>(IKeyFrame_1<System::Single>&& other);
			virtual ~IKeyFrame_1<System::Single>();
			IKeyFrame_1<System::Single>& operator=(const IKeyFrame_1<System::Single>& other);
			IKeyFrame_1<System::Single>& operator=(decltype(nullptr));
			IKeyFrame_1<System::Single>& operator=(IKeyFrame_1<System::Single>&& other);
			bool operator==(const IKeyFrame_1<System::Single>& other) const;
			bool operator!=(const IKeyFrame_1<System::Single>& other) const;
			System::Single GetTime();
			void SetTime(System::Single value);
			System::Single GetValue();
			void SetValue(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct Point3Key : virtual UPlasma::PlEmu::IKeyFrame_1<UnityEngine::Vector3>
		{
			Point3Key(decltype(nullptr));
			Point3Key(Plugin::InternalUse, int32_t handle);
			Point3Key(const Point3Key& other);
			Point3Key(Point3Key&& other);
			virtual ~Point3Key();
			Point3Key& operator=(const Point3Key& other);
			Point3Key& operator=(decltype(nullptr));
			Point3Key& operator=(Point3Key&& other);
			bool operator==(const Point3Key& other) const;
			bool operator!=(const Point3Key& other) const;
			Point3Key();
			UnityEngine::Vector3 GetInTan();
			void SetInTan(UnityEngine::Vector3& value);
			UnityEngine::Vector3 GetOutTan();
			void SetOutTan(UnityEngine::Vector3& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ScalarKey : virtual UPlasma::PlEmu::IKeyFrame_1<System::Single>
		{
			ScalarKey(decltype(nullptr));
			ScalarKey(Plugin::InternalUse, int32_t handle);
			ScalarKey(const ScalarKey& other);
			ScalarKey(ScalarKey&& other);
			virtual ~ScalarKey();
			ScalarKey& operator=(const ScalarKey& other);
			ScalarKey& operator=(decltype(nullptr));
			ScalarKey& operator=(ScalarKey&& other);
			bool operator==(const ScalarKey& other) const;
			bool operator!=(const ScalarKey& other) const;
			ScalarKey();
			System::Single GetInTan();
			void SetInTan(System::Single value);
			System::Single GetOutTan();
			void SetOutTan(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct QuatKey : virtual UPlasma::PlEmu::IKeyFrame_1<UnityEngine::Quaternion>
		{
			QuatKey(decltype(nullptr));
			QuatKey(Plugin::InternalUse, int32_t handle);
			QuatKey(const QuatKey& other);
			QuatKey(QuatKey&& other);
			virtual ~QuatKey();
			QuatKey& operator=(const QuatKey& other);
			QuatKey& operator=(decltype(nullptr));
			QuatKey& operator=(QuatKey&& other);
			bool operator==(const QuatKey& other) const;
			bool operator!=(const QuatKey& other) const;
			QuatKey();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct Matrix44Key : virtual UPlasma::PlEmu::IKeyFrame_1<UnityEngine::Matrix4x4>
		{
			Matrix44Key(decltype(nullptr));
			Matrix44Key(Plugin::InternalUse, int32_t handle);
			Matrix44Key(const Matrix44Key& other);
			Matrix44Key(Matrix44Key&& other);
			virtual ~Matrix44Key();
			Matrix44Key& operator=(const Matrix44Key& other);
			Matrix44Key& operator=(decltype(nullptr));
			Matrix44Key& operator=(Matrix44Key&& other);
			bool operator==(const Matrix44Key& other) const;
			bool operator!=(const Matrix44Key& other) const;
			Matrix44Key();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<System::Single> : virtual System::Object
		{
			IController_1<System::Single>(decltype(nullptr));
			IController_1<System::Single>(Plugin::InternalUse, int32_t handle);
			IController_1<System::Single>(const IController_1<System::Single>& other);
			IController_1<System::Single>(IController_1<System::Single>&& other);
			virtual ~IController_1<System::Single>();
			IController_1<System::Single>& operator=(const IController_1<System::Single>& other);
			IController_1<System::Single>& operator=(decltype(nullptr));
			IController_1<System::Single>& operator=(IController_1<System::Single>&& other);
			bool operator==(const IController_1<System::Single>& other) const;
			bool operator!=(const IController_1<System::Single>& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<UnityEngine::Vector3> : virtual System::Object
		{
			IController_1<UnityEngine::Vector3>(decltype(nullptr));
			IController_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
			IController_1<UnityEngine::Vector3>(const IController_1<UnityEngine::Vector3>& other);
			IController_1<UnityEngine::Vector3>(IController_1<UnityEngine::Vector3>&& other);
			virtual ~IController_1<UnityEngine::Vector3>();
			IController_1<UnityEngine::Vector3>& operator=(const IController_1<UnityEngine::Vector3>& other);
			IController_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
			IController_1<UnityEngine::Vector3>& operator=(IController_1<UnityEngine::Vector3>&& other);
			bool operator==(const IController_1<UnityEngine::Vector3>& other) const;
			bool operator!=(const IController_1<UnityEngine::Vector3>& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<UnityEngine::Quaternion> : virtual System::Object
		{
			IController_1<UnityEngine::Quaternion>(decltype(nullptr));
			IController_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
			IController_1<UnityEngine::Quaternion>(const IController_1<UnityEngine::Quaternion>& other);
			IController_1<UnityEngine::Quaternion>(IController_1<UnityEngine::Quaternion>&& other);
			virtual ~IController_1<UnityEngine::Quaternion>();
			IController_1<UnityEngine::Quaternion>& operator=(const IController_1<UnityEngine::Quaternion>& other);
			IController_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
			IController_1<UnityEngine::Quaternion>& operator=(IController_1<UnityEngine::Quaternion>&& other);
			bool operator==(const IController_1<UnityEngine::Quaternion>& other) const;
			bool operator!=(const IController_1<UnityEngine::Quaternion>& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct IController_1<UPlasma::PlEmu::AffineParts> : virtual System::Object
		{
			IController_1<UPlasma::PlEmu::AffineParts>(decltype(nullptr));
			IController_1<UPlasma::PlEmu::AffineParts>(Plugin::InternalUse, int32_t handle);
			IController_1<UPlasma::PlEmu::AffineParts>(const IController_1<UPlasma::PlEmu::AffineParts>& other);
			IController_1<UPlasma::PlEmu::AffineParts>(IController_1<UPlasma::PlEmu::AffineParts>&& other);
			virtual ~IController_1<UPlasma::PlEmu::AffineParts>();
			IController_1<UPlasma::PlEmu::AffineParts>& operator=(const IController_1<UPlasma::PlEmu::AffineParts>& other);
			IController_1<UPlasma::PlEmu::AffineParts>& operator=(decltype(nullptr));
			IController_1<UPlasma::PlEmu::AffineParts>& operator=(IController_1<UPlasma::PlEmu::AffineParts>&& other);
			bool operator==(const IController_1<UPlasma::PlEmu::AffineParts>& other) const;
			bool operator!=(const IController_1<UPlasma::PlEmu::AffineParts>& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TriScalarToPoint3Controller : virtual UPlasma::PlEmu::IController_1<UnityEngine::Vector3>
		{
			TriScalarToPoint3Controller(decltype(nullptr));
			TriScalarToPoint3Controller(Plugin::InternalUse, int32_t handle);
			TriScalarToPoint3Controller(const TriScalarToPoint3Controller& other);
			TriScalarToPoint3Controller(TriScalarToPoint3Controller&& other);
			virtual ~TriScalarToPoint3Controller();
			TriScalarToPoint3Controller& operator=(const TriScalarToPoint3Controller& other);
			TriScalarToPoint3Controller& operator=(decltype(nullptr));
			TriScalarToPoint3Controller& operator=(TriScalarToPoint3Controller&& other);
			bool operator==(const TriScalarToPoint3Controller& other) const;
			bool operator!=(const TriScalarToPoint3Controller& other) const;
			TriScalarToPoint3Controller();
			UPlasma::PlEmu::IController_1<System::Single> GetX();
			void SetX(UPlasma::PlEmu::IController_1<System::Single>& value);
			UPlasma::PlEmu::IController_1<System::Single> GetY();
			void SetY(UPlasma::PlEmu::IController_1<System::Single>& value);
			UPlasma::PlEmu::IController_1<System::Single> GetZ();
			void SetZ(UPlasma::PlEmu::IController_1<System::Single>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TriScalarToQuatController : virtual UPlasma::PlEmu::IController_1<UnityEngine::Quaternion>
		{
			TriScalarToQuatController(decltype(nullptr));
			TriScalarToQuatController(Plugin::InternalUse, int32_t handle);
			TriScalarToQuatController(const TriScalarToQuatController& other);
			TriScalarToQuatController(TriScalarToQuatController&& other);
			virtual ~TriScalarToQuatController();
			TriScalarToQuatController& operator=(const TriScalarToQuatController& other);
			TriScalarToQuatController& operator=(decltype(nullptr));
			TriScalarToQuatController& operator=(TriScalarToQuatController&& other);
			bool operator==(const TriScalarToQuatController& other) const;
			bool operator!=(const TriScalarToQuatController& other) const;
			TriScalarToQuatController();
			UPlasma::PlEmu::IController_1<System::Single> GetX();
			void SetX(UPlasma::PlEmu::IController_1<System::Single>& value);
			UPlasma::PlEmu::IController_1<System::Single> GetY();
			void SetY(UPlasma::PlEmu::IController_1<System::Single>& value);
			UPlasma::PlEmu::IController_1<System::Single> GetZ();
			void SetZ(UPlasma::PlEmu::IController_1<System::Single>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TrsController : virtual UPlasma::PlEmu::IController_1<UPlasma::PlEmu::AffineParts>
		{
			TrsController(decltype(nullptr));
			TrsController(Plugin::InternalUse, int32_t handle);
			TrsController(const TrsController& other);
			TrsController(TrsController&& other);
			virtual ~TrsController();
			TrsController& operator=(const TrsController& other);
			TrsController& operator=(decltype(nullptr));
			TrsController& operator=(TrsController&& other);
			bool operator==(const TrsController& other) const;
			bool operator!=(const TrsController& other) const;
			TrsController();
			UPlasma::PlEmu::IController_1<UnityEngine::Vector3> GetPosCtrl();
			void SetPosCtrl(UPlasma::PlEmu::IController_1<UnityEngine::Vector3>& value);
			UPlasma::PlEmu::IController_1<UnityEngine::Quaternion> GetRotCtrl();
			void SetRotCtrl(UPlasma::PlEmu::IController_1<UnityEngine::Quaternion>& value);
			UPlasma::PlEmu::IController_1<UnityEngine::Vector3> GetScaCtrl();
			void SetScaCtrl(UPlasma::PlEmu::IController_1<UnityEngine::Vector3>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey> : virtual UPlasma::PlEmu::IController_1<System::Single>
		{
			LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>(decltype(nullptr));
			LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>(Plugin::InternalUse, int32_t handle);
			LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>(const LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>& other);
			LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>(LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>&& other);
			virtual ~LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>();
			LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>& operator=(const LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>& other);
			LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>& operator=(decltype(nullptr));
			LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>& operator=(LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>&& other);
			bool operator==(const LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>& other) const;
			bool operator!=(const LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key> : virtual UPlasma::PlEmu::IController_1<UnityEngine::Vector3>
		{
			LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>(decltype(nullptr));
			LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>(Plugin::InternalUse, int32_t handle);
			LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>(const LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>& other);
			LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>(LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>&& other);
			virtual ~LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>();
			LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>& operator=(const LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>& other);
			LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>& operator=(decltype(nullptr));
			LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>& operator=(LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>&& other);
			bool operator==(const LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>& other) const;
			bool operator!=(const LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		template<> struct LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey> : virtual UPlasma::PlEmu::IController_1<UnityEngine::Quaternion>
		{
			LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>(decltype(nullptr));
			LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>(Plugin::InternalUse, int32_t handle);
			LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>(const LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>& other);
			LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>(LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>&& other);
			virtual ~LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>();
			LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>& operator=(const LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>& other);
			LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>& operator=(decltype(nullptr));
			LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>& operator=(LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>&& other);
			bool operator==(const LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>& other) const;
			bool operator!=(const LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct Point3Controller : virtual UPlasma::PlEmu::LeafController_2<UnityEngine::Vector3, UPlasma::PlEmu::Point3Key>, virtual UPlasma::PlEmu::IController_1<UnityEngine::Vector3>
		{
			Point3Controller(decltype(nullptr));
			Point3Controller(Plugin::InternalUse, int32_t handle);
			Point3Controller(const Point3Controller& other);
			Point3Controller(Point3Controller&& other);
			virtual ~Point3Controller();
			Point3Controller& operator=(const Point3Controller& other);
			Point3Controller& operator=(decltype(nullptr));
			Point3Controller& operator=(Point3Controller&& other);
			bool operator==(const Point3Controller& other) const;
			bool operator!=(const Point3Controller& other) const;
			Point3Controller();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ScalarController : virtual UPlasma::PlEmu::LeafController_2<System::Single, UPlasma::PlEmu::ScalarKey>, virtual UPlasma::PlEmu::IController_1<System::Single>
		{
			ScalarController(decltype(nullptr));
			ScalarController(Plugin::InternalUse, int32_t handle);
			ScalarController(const ScalarController& other);
			ScalarController(ScalarController&& other);
			virtual ~ScalarController();
			ScalarController& operator=(const ScalarController& other);
			ScalarController& operator=(decltype(nullptr));
			ScalarController& operator=(ScalarController&& other);
			bool operator==(const ScalarController& other) const;
			bool operator!=(const ScalarController& other) const;
			ScalarController();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct QuatController : virtual UPlasma::PlEmu::LeafController_2<UnityEngine::Quaternion, UPlasma::PlEmu::QuatKey>, virtual UPlasma::PlEmu::IController_1<UnityEngine::Quaternion>
		{
			QuatController(decltype(nullptr));
			QuatController(Plugin::InternalUse, int32_t handle);
			QuatController(const QuatController& other);
			QuatController(QuatController&& other);
			virtual ~QuatController();
			QuatController& operator=(const QuatController& other);
			QuatController& operator=(decltype(nullptr));
			QuatController& operator=(QuatController&& other);
			bool operator==(const QuatController& other) const;
			bool operator!=(const QuatController& other) const;
			QuatController();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AffineParts : virtual System::Object
		{
			AffineParts(decltype(nullptr));
			AffineParts(Plugin::InternalUse, int32_t handle);
			AffineParts(const AffineParts& other);
			AffineParts(AffineParts&& other);
			virtual ~AffineParts();
			AffineParts& operator=(const AffineParts& other);
			AffineParts& operator=(decltype(nullptr));
			AffineParts& operator=(AffineParts&& other);
			bool operator==(const AffineParts& other) const;
			bool operator!=(const AffineParts& other) const;
			AffineParts();
			System::Int32 GetI();
			void SetI(System::Int32 value);
			UnityEngine::Vector3 GetTranslation();
			void SetTranslation(UnityEngine::Vector3& value);
			UnityEngine::Quaternion GetRotation();
			void SetRotation(UnityEngine::Quaternion& value);
			UnityEngine::Quaternion GetScaleRotation();
			void SetScaleRotation(UnityEngine::Quaternion& value);
			UnityEngine::Vector3 GetScale();
			void SetScale(UnityEngine::Vector3& value);
			System::Single GetSignOfDeterminant();
			void SetSignOfDeterminant(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct IAgChannel : virtual System::Object
		{
			IAgChannel(decltype(nullptr));
			IAgChannel(Plugin::InternalUse, int32_t handle);
			IAgChannel(const IAgChannel& other);
			IAgChannel(IAgChannel&& other);
			virtual ~IAgChannel();
			IAgChannel& operator=(const IAgChannel& other);
			IAgChannel& operator=(decltype(nullptr));
			IAgChannel& operator=(IAgChannel&& other);
			bool operator==(const IAgChannel& other) const;
			bool operator!=(const IAgChannel& other) const;
			System::String GetName();
			void SetName(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PointControllerChannel : virtual UPlasma::PlEmu::IAgChannel
		{
			PointControllerChannel(decltype(nullptr));
			PointControllerChannel(Plugin::InternalUse, int32_t handle);
			PointControllerChannel(const PointControllerChannel& other);
			PointControllerChannel(PointControllerChannel&& other);
			virtual ~PointControllerChannel();
			PointControllerChannel& operator=(const PointControllerChannel& other);
			PointControllerChannel& operator=(decltype(nullptr));
			PointControllerChannel& operator=(PointControllerChannel&& other);
			bool operator==(const PointControllerChannel& other) const;
			bool operator!=(const PointControllerChannel& other) const;
			PointControllerChannel();
			UPlasma::PlEmu::IController_1<UnityEngine::Vector3> GetController();
			void SetController(UPlasma::PlEmu::IController_1<UnityEngine::Vector3>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ScalarControllerChannel : virtual UPlasma::PlEmu::IAgChannel
		{
			ScalarControllerChannel(decltype(nullptr));
			ScalarControllerChannel(Plugin::InternalUse, int32_t handle);
			ScalarControllerChannel(const ScalarControllerChannel& other);
			ScalarControllerChannel(ScalarControllerChannel&& other);
			virtual ~ScalarControllerChannel();
			ScalarControllerChannel& operator=(const ScalarControllerChannel& other);
			ScalarControllerChannel& operator=(decltype(nullptr));
			ScalarControllerChannel& operator=(ScalarControllerChannel&& other);
			bool operator==(const ScalarControllerChannel& other) const;
			bool operator!=(const ScalarControllerChannel& other) const;
			ScalarControllerChannel();
			UPlasma::PlEmu::IController_1<System::Single> GetController();
			void SetController(UPlasma::PlEmu::IController_1<System::Single>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct TrsControllerChannel : virtual UPlasma::PlEmu::IAgChannel
		{
			TrsControllerChannel(decltype(nullptr));
			TrsControllerChannel(Plugin::InternalUse, int32_t handle);
			TrsControllerChannel(const TrsControllerChannel& other);
			TrsControllerChannel(TrsControllerChannel&& other);
			virtual ~TrsControllerChannel();
			TrsControllerChannel& operator=(const TrsControllerChannel& other);
			TrsControllerChannel& operator=(decltype(nullptr));
			TrsControllerChannel& operator=(TrsControllerChannel&& other);
			bool operator==(const TrsControllerChannel& other) const;
			bool operator!=(const TrsControllerChannel& other) const;
			TrsControllerChannel();
			UPlasma::PlEmu::TrsController GetController();
			void SetController(UPlasma::PlEmu::TrsController& value);
			UPlasma::PlEmu::AffineParts GetAffineParts();
			void SetAffineParts(UPlasma::PlEmu::AffineParts& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct IAgApplicator : virtual System::Object
		{
			IAgApplicator(decltype(nullptr));
			IAgApplicator(Plugin::InternalUse, int32_t handle);
			IAgApplicator(const IAgApplicator& other);
			IAgApplicator(IAgApplicator&& other);
			virtual ~IAgApplicator();
			IAgApplicator& operator=(const IAgApplicator& other);
			IAgApplicator& operator=(decltype(nullptr));
			IAgApplicator& operator=(IAgApplicator&& other);
			bool operator==(const IAgApplicator& other) const;
			bool operator!=(const IAgApplicator& other) const;
			System::Boolean GetEnabled();
			void SetEnabled(System::Boolean value);
			UPlasma::PlEmu::IAgChannel GetChannel();
			void SetChannel(UPlasma::PlEmu::IAgChannel& value);
			System::String GetChannelName();
			void SetChannelName(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct MatrixChannelApplicator : virtual UPlasma::PlEmu::IAgApplicator
		{
			MatrixChannelApplicator(decltype(nullptr));
			MatrixChannelApplicator(Plugin::InternalUse, int32_t handle);
			MatrixChannelApplicator(const MatrixChannelApplicator& other);
			MatrixChannelApplicator(MatrixChannelApplicator&& other);
			virtual ~MatrixChannelApplicator();
			MatrixChannelApplicator& operator=(const MatrixChannelApplicator& other);
			MatrixChannelApplicator& operator=(decltype(nullptr));
			MatrixChannelApplicator& operator=(MatrixChannelApplicator&& other);
			bool operator==(const MatrixChannelApplicator& other) const;
			bool operator!=(const MatrixChannelApplicator& other) const;
			MatrixChannelApplicator();
			UPlasma::PlEmu::TrsControllerChannel GetTrsChannel();
			void SetTrsChannel(UPlasma::PlEmu::TrsControllerChannel& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct LightDiffuseApplicator : virtual UPlasma::PlEmu::IAgApplicator
		{
			LightDiffuseApplicator(decltype(nullptr));
			LightDiffuseApplicator(Plugin::InternalUse, int32_t handle);
			LightDiffuseApplicator(const LightDiffuseApplicator& other);
			LightDiffuseApplicator(LightDiffuseApplicator&& other);
			virtual ~LightDiffuseApplicator();
			LightDiffuseApplicator& operator=(const LightDiffuseApplicator& other);
			LightDiffuseApplicator& operator=(decltype(nullptr));
			LightDiffuseApplicator& operator=(LightDiffuseApplicator&& other);
			bool operator==(const LightDiffuseApplicator& other) const;
			bool operator!=(const LightDiffuseApplicator& other) const;
			LightDiffuseApplicator();
			UPlasma::PlEmu::PointControllerChannel GetPointChannel();
			void SetPointChannel(UPlasma::PlEmu::PointControllerChannel& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ParticlePpsApplicator : virtual UPlasma::PlEmu::IAgApplicator
		{
			ParticlePpsApplicator(decltype(nullptr));
			ParticlePpsApplicator(Plugin::InternalUse, int32_t handle);
			ParticlePpsApplicator(const ParticlePpsApplicator& other);
			ParticlePpsApplicator(ParticlePpsApplicator&& other);
			virtual ~ParticlePpsApplicator();
			ParticlePpsApplicator& operator=(const ParticlePpsApplicator& other);
			ParticlePpsApplicator& operator=(decltype(nullptr));
			ParticlePpsApplicator& operator=(ParticlePpsApplicator&& other);
			bool operator==(const ParticlePpsApplicator& other) const;
			bool operator!=(const ParticlePpsApplicator& other) const;
			ParticlePpsApplicator();
			UPlasma::PlEmu::ScalarControllerChannel GetScalarChannel();
			void SetScalarChannel(UPlasma::PlEmu::ScalarControllerChannel& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AgAnim : virtual System::Object
		{
			AgAnim(decltype(nullptr));
			AgAnim(Plugin::InternalUse, int32_t handle);
			AgAnim(const AgAnim& other);
			AgAnim(AgAnim&& other);
			virtual ~AgAnim();
			AgAnim& operator=(const AgAnim& other);
			AgAnim& operator=(decltype(nullptr));
			AgAnim& operator=(AgAnim&& other);
			bool operator==(const AgAnim& other) const;
			bool operator!=(const AgAnim& other) const;
			System::Single GetBlend();
			void SetBlend(System::Single value);
			System::Single GetStart();
			void SetStart(System::Single value);
			System::Single GetEnd();
			void SetEnd(System::Single value);
			System::String GetName();
			void SetName(System::String& value);
			System::Array1<UPlasma::PlEmu::IAgApplicator> GetApplicators();
			void SetApplicators(System::Array1<UPlasma::PlEmu::IAgApplicator>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AgeGlobalAnim : virtual UPlasma::PlEmu::AgAnim
		{
			AgeGlobalAnim(decltype(nullptr));
			AgeGlobalAnim(Plugin::InternalUse, int32_t handle);
			AgeGlobalAnim(const AgeGlobalAnim& other);
			AgeGlobalAnim(AgeGlobalAnim&& other);
			virtual ~AgeGlobalAnim();
			AgeGlobalAnim& operator=(const AgeGlobalAnim& other);
			AgeGlobalAnim& operator=(decltype(nullptr));
			AgeGlobalAnim& operator=(AgeGlobalAnim&& other);
			bool operator==(const AgeGlobalAnim& other) const;
			bool operator!=(const AgeGlobalAnim& other) const;
			AgeGlobalAnim();
			System::String GetGlobalVarName();
			void SetGlobalVarName(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AtcAnim : virtual UPlasma::PlEmu::AgAnim
		{
			AtcAnim(decltype(nullptr));
			AtcAnim(Plugin::InternalUse, int32_t handle);
			AtcAnim(const AtcAnim& other);
			AtcAnim(AtcAnim&& other);
			virtual ~AtcAnim();
			AtcAnim& operator=(const AtcAnim& other);
			AtcAnim& operator=(decltype(nullptr));
			AtcAnim& operator=(AtcAnim&& other);
			bool operator==(const AtcAnim& other) const;
			bool operator!=(const AtcAnim& other) const;
			AtcAnim();
			System::Single GetInitial();
			void SetInitial(System::Single value);
			System::Single GetLoopStart();
			void SetLoopStart(System::Single value);
			System::Single GetLoopEnd();
			void SetLoopEnd(System::Single value);
			System::Boolean GetAutoStart();
			void SetAutoStart(System::Boolean value);
			System::Boolean GetLoop();
			void SetLoop(System::Boolean value);
			System::Byte GetEaseInType();
			void SetEaseInType(System::Byte value);
			System::Byte GetEaseOutType();
			void SetEaseOutType(System::Byte value);
			System::Single GetEaseInLength();
			void SetEaseInLength(System::Single value);
			System::Single GetEaseInMin();
			void SetEaseInMin(System::Single value);
			System::Single GetEaseInMax();
			void SetEaseInMax(System::Single value);
			System::Single GetEaseOutLength();
			void SetEaseOutLength(System::Single value);
			System::Single GetEaseOutMin();
			void SetEaseOutMin(System::Single value);
			System::Single GetEaseOutMax();
			void SetEaseOutMax(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgMasterMod : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::ICannotBeBatched, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlAgMasterMod(decltype(nullptr));
			PlAgMasterMod(Plugin::InternalUse, int32_t handle);
			PlAgMasterMod(const PlAgMasterMod& other);
			PlAgMasterMod(PlAgMasterMod&& other);
			virtual ~PlAgMasterMod();
			PlAgMasterMod& operator=(const PlAgMasterMod& other);
			PlAgMasterMod& operator=(decltype(nullptr));
			PlAgMasterMod& operator=(PlAgMasterMod&& other);
			bool operator==(const PlAgMasterMod& other) const;
			bool operator!=(const PlAgMasterMod& other) const;
			System::Array1<UPlasma::PlEmu::AgAnim> GetAnims();
			void SetAnims(System::Array1<UPlasma::PlEmu::AgAnim>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgModifier : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::ICannotBeBatched
		{
			PlAgModifier(decltype(nullptr));
			PlAgModifier(Plugin::InternalUse, int32_t handle);
			PlAgModifier(const PlAgModifier& other);
			PlAgModifier(PlAgModifier&& other);
			virtual ~PlAgModifier();
			PlAgModifier& operator=(const PlAgModifier& other);
			PlAgModifier& operator=(decltype(nullptr));
			PlAgModifier& operator=(PlAgModifier&& other);
			bool operator==(const PlAgModifier& other) const;
			bool operator!=(const PlAgModifier& other) const;
			System::String GetChannelName();
			void SetChannelName(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLayerAnimation : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlLayerAnimation(decltype(nullptr));
			PlLayerAnimation(Plugin::InternalUse, int32_t handle);
			PlLayerAnimation(const PlLayerAnimation& other);
			PlLayerAnimation(PlLayerAnimation&& other);
			virtual ~PlLayerAnimation();
			PlLayerAnimation& operator=(const PlLayerAnimation& other);
			PlLayerAnimation& operator=(decltype(nullptr));
			PlLayerAnimation& operator=(PlLayerAnimation&& other);
			bool operator==(const PlLayerAnimation& other) const;
			bool operator!=(const PlLayerAnimation& other) const;
			UnityEngine::Material GetMaterial();
			void SetMaterial(UnityEngine::Material& value);
			System::Boolean GetPreshadeColorAnimated();
			void SetPreshadeColorAnimated(System::Boolean value);
			System::Boolean GetRuntimeColorAnimated();
			void SetRuntimeColorAnimated(System::Boolean value);
			System::Boolean GetAmbientColorAnimated();
			void SetAmbientColorAnimated(System::Boolean value);
			System::Boolean GetSpecularColorAnimated();
			void SetSpecularColorAnimated(System::Boolean value);
			System::Boolean GetOpacityAnimated();
			void SetOpacityAnimated(System::Boolean value);
			System::Boolean GetUvwTransformAnimated();
			void SetUvwTransformAnimated(System::Boolean value);
			System::Int32 GetAnimationPropIndex();
			void SetAnimationPropIndex(System::Int32 value);
			UnityEngine::Animation GetAnimPlayer();
			void SetAnimPlayer(UnityEngine::Animation& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlExcludeRegionModifier : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlExcludeRegionModifier(decltype(nullptr));
			PlExcludeRegionModifier(Plugin::InternalUse, int32_t handle);
			PlExcludeRegionModifier(const PlExcludeRegionModifier& other);
			PlExcludeRegionModifier(PlExcludeRegionModifier&& other);
			virtual ~PlExcludeRegionModifier();
			PlExcludeRegionModifier& operator=(const PlExcludeRegionModifier& other);
			PlExcludeRegionModifier& operator=(decltype(nullptr));
			PlExcludeRegionModifier& operator=(PlExcludeRegionModifier&& other);
			bool operator==(const PlExcludeRegionModifier& other) const;
			bool operator!=(const PlExcludeRegionModifier& other) const;
			System::Boolean GetSeek();
			void SetSeek(System::Boolean value);
			System::Single GetSeekTime();
			void SetSeekTime(System::Single value);
			System::Collections::Generic::List_1<UnityEngine::Transform> GetSafePoints();
			void SetSafePoints(System::Collections::Generic::List_1<UnityEngine::Transform>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlOneShotMod : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlOneShotMod(decltype(nullptr));
			PlOneShotMod(Plugin::InternalUse, int32_t handle);
			PlOneShotMod(const PlOneShotMod& other);
			PlOneShotMod(PlOneShotMod&& other);
			virtual ~PlOneShotMod();
			PlOneShotMod& operator=(const PlOneShotMod& other);
			PlOneShotMod& operator=(decltype(nullptr));
			PlOneShotMod& operator=(PlOneShotMod&& other);
			bool operator==(const PlOneShotMod& other) const;
			bool operator!=(const PlOneShotMod& other) const;
			System::String GetAnimName();
			void SetAnimName(System::String& value);
			System::Boolean GetDrivable();
			void SetDrivable(System::Boolean value);
			System::Boolean GetReversable();
			void SetReversable(System::Boolean value);
			System::Boolean GetSmartSeek();
			void SetSmartSeek(System::Boolean value);
			System::Boolean GetNoSeek();
			void SetNoSeek(System::Boolean value);
			System::Single GetSeekDuration();
			void SetSeekDuration(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSittingModifier : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlSittingModifier(decltype(nullptr));
			PlSittingModifier(Plugin::InternalUse, int32_t handle);
			PlSittingModifier(const PlSittingModifier& other);
			PlSittingModifier(PlSittingModifier&& other);
			virtual ~PlSittingModifier();
			PlSittingModifier& operator=(const PlSittingModifier& other);
			PlSittingModifier& operator=(decltype(nullptr));
			PlSittingModifier& operator=(PlSittingModifier&& other);
			bool operator==(const PlSittingModifier& other) const;
			bool operator!=(const PlSittingModifier& other) const;
			System::Int32 GetFlagsInt();
			void SetFlagsInt(System::Int32 value);
			System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable> GetNotifies();
			void SetNotifies(System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlRandomSoundModGroup : virtual System::Object
		{
			PlRandomSoundModGroup(decltype(nullptr));
			PlRandomSoundModGroup(Plugin::InternalUse, int32_t handle);
			PlRandomSoundModGroup(const PlRandomSoundModGroup& other);
			PlRandomSoundModGroup(PlRandomSoundModGroup&& other);
			virtual ~PlRandomSoundModGroup();
			PlRandomSoundModGroup& operator=(const PlRandomSoundModGroup& other);
			PlRandomSoundModGroup& operator=(decltype(nullptr));
			PlRandomSoundModGroup& operator=(PlRandomSoundModGroup&& other);
			bool operator==(const PlRandomSoundModGroup& other) const;
			bool operator!=(const PlRandomSoundModGroup& other) const;
			PlRandomSoundModGroup();
			System::Collections::Generic::List_1<System::Int32> GetIndices();
			void SetIndices(System::Collections::Generic::List_1<System::Int32>& value);
			System::Int32 GetGroupedIdx();
			void SetGroupedIdx(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlRandomSoundMod : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlRandomSoundMod(decltype(nullptr));
			PlRandomSoundMod(Plugin::InternalUse, int32_t handle);
			PlRandomSoundMod(const PlRandomSoundMod& other);
			PlRandomSoundMod(PlRandomSoundMod&& other);
			virtual ~PlRandomSoundMod();
			PlRandomSoundMod& operator=(const PlRandomSoundMod& other);
			PlRandomSoundMod& operator=(decltype(nullptr));
			PlRandomSoundMod& operator=(PlRandomSoundMod&& other);
			bool operator==(const PlRandomSoundMod& other) const;
			bool operator!=(const PlRandomSoundMod& other) const;
			System::Int32 GetModeInt();
			void SetModeInt(System::Int32 value);
			System::Int32 GetState();
			void SetState(System::Int32 value);
			System::Single GetMinDelay();
			void SetMinDelay(System::Single value);
			System::Single GetMaxDelay();
			void SetMaxDelay(System::Single value);
			System::Collections::Generic::List_1<UPlasma::PlEmu::PlRandomSoundModGroup> GetGroups();
			void SetGroups(System::Collections::Generic::List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPythonFileMod : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlPythonFileMod(decltype(nullptr));
			PlPythonFileMod(Plugin::InternalUse, int32_t handle);
			PlPythonFileMod(const PlPythonFileMod& other);
			PlPythonFileMod(PlPythonFileMod&& other);
			virtual ~PlPythonFileMod();
			PlPythonFileMod& operator=(const PlPythonFileMod& other);
			PlPythonFileMod& operator=(decltype(nullptr));
			PlPythonFileMod& operator=(PlPythonFileMod&& other);
			bool operator==(const PlPythonFileMod& other) const;
			bool operator!=(const PlPythonFileMod& other) const;
			System::String GetMainClassName();
			void SetMainClassName(System::String& value);
			System::Collections::Generic::List_1<System::Object> GetParameters();
			void SetParameters(System::Collections::Generic::List_1<System::Object>& value);
			System::Collections::Generic::List_1<System::Int32> GetParameterIds();
			void SetParameterIds(System::Collections::Generic::List_1<System::Int32>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMessage : virtual System::Object
		{
			PlMessage(decltype(nullptr));
			PlMessage(Plugin::InternalUse, int32_t handle);
			PlMessage(const PlMessage& other);
			PlMessage(PlMessage&& other);
			virtual ~PlMessage();
			PlMessage& operator=(const PlMessage& other);
			PlMessage& operator=(decltype(nullptr));
			PlMessage& operator=(PlMessage&& other);
			bool operator==(const PlMessage& other) const;
			bool operator!=(const PlMessage& other) const;
			System::Int32 GetCastFlagsInt();
			void SetCastFlagsInt(System::Int32 value);
			UnityEngine::Component GetSender();
			void SetSender(UnityEngine::Component& value);
			System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable> GetReceivers();
			void SetReceivers(System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMessageWithCallbacks : virtual UPlasma::PlEmu::PlMessage
		{
			PlMessageWithCallbacks(decltype(nullptr));
			PlMessageWithCallbacks(Plugin::InternalUse, int32_t handle);
			PlMessageWithCallbacks(const PlMessageWithCallbacks& other);
			PlMessageWithCallbacks(PlMessageWithCallbacks&& other);
			virtual ~PlMessageWithCallbacks();
			PlMessageWithCallbacks& operator=(const PlMessageWithCallbacks& other);
			PlMessageWithCallbacks& operator=(decltype(nullptr));
			PlMessageWithCallbacks& operator=(PlMessageWithCallbacks&& other);
			bool operator==(const PlMessageWithCallbacks& other) const;
			bool operator!=(const PlMessageWithCallbacks& other) const;
			System::Collections::Generic::List_1<UPlasma::PlEmu::PlMessage> GetCallbacks();
			void SetCallbacks(System::Collections::Generic::List_1<UPlasma::PlEmu::PlMessage>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlNotifyMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlNotifyMsg(decltype(nullptr));
			PlNotifyMsg(Plugin::InternalUse, int32_t handle);
			PlNotifyMsg(const PlNotifyMsg& other);
			PlNotifyMsg(PlNotifyMsg&& other);
			virtual ~PlNotifyMsg();
			PlNotifyMsg& operator=(const PlNotifyMsg& other);
			PlNotifyMsg& operator=(decltype(nullptr));
			PlNotifyMsg& operator=(PlNotifyMsg&& other);
			bool operator==(const PlNotifyMsg& other) const;
			bool operator!=(const PlNotifyMsg& other) const;
			PlNotifyMsg();
			System::Int32 GetNotifyTypeInt();
			void SetNotifyTypeInt(System::Int32 value);
			System::Single GetNotifyState();
			void SetNotifyState(System::Single value);
			System::Int32 GetNotifyId();
			void SetNotifyId(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCameraConfig : virtual System::Object
		{
			PlCameraConfig(decltype(nullptr));
			PlCameraConfig(Plugin::InternalUse, int32_t handle);
			PlCameraConfig(const PlCameraConfig& other);
			PlCameraConfig(PlCameraConfig&& other);
			virtual ~PlCameraConfig();
			PlCameraConfig& operator=(const PlCameraConfig& other);
			PlCameraConfig& operator=(decltype(nullptr));
			PlCameraConfig& operator=(PlCameraConfig&& other);
			bool operator==(const PlCameraConfig& other) const;
			bool operator!=(const PlCameraConfig& other) const;
			PlCameraConfig();
			UnityEngine::Vector3 GetOffset();
			void SetOffset(UnityEngine::Vector3& value);
			System::Single GetAccel();
			void SetAccel(System::Single value);
			System::Single GetDecel();
			void SetDecel(System::Single value);
			System::Single GetVel();
			void SetVel(System::Single value);
			System::Single GetFPAccel();
			void SetFPAccel(System::Single value);
			System::Single GetFPDecel();
			void SetFPDecel(System::Single value);
			System::Single GetFPVel();
			void SetFPVel(System::Single value);
			System::Single GetFOVw();
			void SetFOVw(System::Single value);
			System::Single GetFOVh();
			void SetFOVh(System::Single value);
			System::Boolean GetWorldspace();
			void SetWorldspace(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlCameraMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlCameraMsg(decltype(nullptr));
			PlCameraMsg(Plugin::InternalUse, int32_t handle);
			PlCameraMsg(const PlCameraMsg& other);
			PlCameraMsg(PlCameraMsg&& other);
			virtual ~PlCameraMsg();
			PlCameraMsg& operator=(const PlCameraMsg& other);
			PlCameraMsg& operator=(decltype(nullptr));
			PlCameraMsg& operator=(PlCameraMsg&& other);
			bool operator==(const PlCameraMsg& other) const;
			bool operator!=(const PlCameraMsg& other) const;
			PlCameraMsg();
			System::Collections::Generic::List_1<System::Int32> GetCommandsInt();
			void SetCommandsInt(System::Collections::Generic::List_1<System::Int32>& value);
			UPlasma::PlEmu::PlCameraModifier GetNewCam();
			void SetNewCam(UPlasma::PlEmu::PlCameraModifier& value);
			UnityEngine::Component GetTriggerer();
			void SetTriggerer(UnityEngine::Component& value);
			System::Single GetTransTime();
			void SetTransTime(System::Single value);
			UPlasma::PlEmu::PlCameraConfig GetConfig();
			void SetConfig(UPlasma::PlEmu::PlCameraConfig& value);
			System::Boolean GetActivated();
			void SetActivated(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAnimCmdMsg : virtual UPlasma::PlEmu::PlMessageWithCallbacks
		{
			PlAnimCmdMsg(decltype(nullptr));
			PlAnimCmdMsg(Plugin::InternalUse, int32_t handle);
			PlAnimCmdMsg(const PlAnimCmdMsg& other);
			PlAnimCmdMsg(PlAnimCmdMsg&& other);
			virtual ~PlAnimCmdMsg();
			PlAnimCmdMsg& operator=(const PlAnimCmdMsg& other);
			PlAnimCmdMsg& operator=(decltype(nullptr));
			PlAnimCmdMsg& operator=(PlAnimCmdMsg&& other);
			bool operator==(const PlAnimCmdMsg& other) const;
			bool operator!=(const PlAnimCmdMsg& other) const;
			PlAnimCmdMsg();
			System::Collections::Generic::List_1<System::Int32> GetCommandsInt();
			void SetCommandsInt(System::Collections::Generic::List_1<System::Int32>& value);
			System::String GetAnimName();
			void SetAnimName(System::String& value);
			System::String GetLoopName();
			void SetLoopName(System::String& value);
			System::Single GetBegin();
			void SetBegin(System::Single value);
			System::Single GetEnd();
			void SetEnd(System::Single value);
			System::Single GetLoopBegin();
			void SetLoopBegin(System::Single value);
			System::Single GetLoopEnd();
			void SetLoopEnd(System::Single value);
			System::Single GetSpeed();
			void SetSpeed(System::Single value);
			System::Single GetSpeedChangeRate();
			void SetSpeedChangeRate(System::Single value);
			System::Single GetTime();
			void SetTime(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlArmatureEffectStateMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlArmatureEffectStateMsg(decltype(nullptr));
			PlArmatureEffectStateMsg(Plugin::InternalUse, int32_t handle);
			PlArmatureEffectStateMsg(const PlArmatureEffectStateMsg& other);
			PlArmatureEffectStateMsg(PlArmatureEffectStateMsg&& other);
			virtual ~PlArmatureEffectStateMsg();
			PlArmatureEffectStateMsg& operator=(const PlArmatureEffectStateMsg& other);
			PlArmatureEffectStateMsg& operator=(decltype(nullptr));
			PlArmatureEffectStateMsg& operator=(PlArmatureEffectStateMsg&& other);
			bool operator==(const PlArmatureEffectStateMsg& other) const;
			bool operator!=(const PlArmatureEffectStateMsg& other) const;
			PlArmatureEffectStateMsg();
			System::Int32 GetSurfaceInt();
			void SetSurfaceInt(System::Int32 value);
			System::Boolean GetAdd();
			void SetAdd(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEventCallbackMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlEventCallbackMsg(decltype(nullptr));
			PlEventCallbackMsg(Plugin::InternalUse, int32_t handle);
			PlEventCallbackMsg(const PlEventCallbackMsg& other);
			PlEventCallbackMsg(PlEventCallbackMsg&& other);
			virtual ~PlEventCallbackMsg();
			PlEventCallbackMsg& operator=(const PlEventCallbackMsg& other);
			PlEventCallbackMsg& operator=(decltype(nullptr));
			PlEventCallbackMsg& operator=(PlEventCallbackMsg&& other);
			bool operator==(const PlEventCallbackMsg& other) const;
			bool operator!=(const PlEventCallbackMsg& other) const;
			PlEventCallbackMsg();
			System::Int32 GetEventIdInt();
			void SetEventIdInt(System::Int32 value);
			System::Single GetEventTime();
			void SetEventTime(System::Single value);
			System::Int32 GetIndex();
			void SetIndex(System::Int32 value);
			System::Int32 GetRepeats();
			void SetRepeats(System::Int32 value);
			System::Int32 GetUser();
			void SetUser(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlExcludeRegionMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlExcludeRegionMsg(decltype(nullptr));
			PlExcludeRegionMsg(Plugin::InternalUse, int32_t handle);
			PlExcludeRegionMsg(const PlExcludeRegionMsg& other);
			PlExcludeRegionMsg(PlExcludeRegionMsg&& other);
			virtual ~PlExcludeRegionMsg();
			PlExcludeRegionMsg& operator=(const PlExcludeRegionMsg& other);
			PlExcludeRegionMsg& operator=(decltype(nullptr));
			PlExcludeRegionMsg& operator=(PlExcludeRegionMsg&& other);
			bool operator==(const PlExcludeRegionMsg& other) const;
			bool operator!=(const PlExcludeRegionMsg& other) const;
			PlExcludeRegionMsg();
			System::Int32 GetCommandInt();
			void SetCommandInt(System::Int32 value);
			System::Int32 GetSynchFlags();
			void SetSynchFlags(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSimSuppressMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlSimSuppressMsg(decltype(nullptr));
			PlSimSuppressMsg(Plugin::InternalUse, int32_t handle);
			PlSimSuppressMsg(const PlSimSuppressMsg& other);
			PlSimSuppressMsg(PlSimSuppressMsg&& other);
			virtual ~PlSimSuppressMsg();
			PlSimSuppressMsg& operator=(const PlSimSuppressMsg& other);
			PlSimSuppressMsg& operator=(decltype(nullptr));
			PlSimSuppressMsg& operator=(PlSimSuppressMsg&& other);
			bool operator==(const PlSimSuppressMsg& other) const;
			bool operator!=(const PlSimSuppressMsg& other) const;
			PlSimSuppressMsg();
			System::Boolean GetSuppress();
			void SetSuppress(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlUuid : virtual System::Object
		{
			PlUuid(decltype(nullptr));
			PlUuid(Plugin::InternalUse, int32_t handle);
			PlUuid(const PlUuid& other);
			PlUuid(PlUuid&& other);
			virtual ~PlUuid();
			PlUuid& operator=(const PlUuid& other);
			PlUuid& operator=(decltype(nullptr));
			PlUuid& operator=(PlUuid&& other);
			bool operator==(const PlUuid& other) const;
			bool operator!=(const PlUuid& other) const;
			PlUuid();
			System::UInt32 GetData1();
			void SetData1(System::UInt32 value);
			System::UInt16 GetData2();
			void SetData2(System::UInt16 value);
			System::UInt16 GetData3();
			void SetData3(System::UInt16 value);
			System::Array1<System::Byte> GetData4();
			void SetData4(System::Array1<System::Byte>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSpawnPointInfo : virtual System::Object
		{
			PlSpawnPointInfo(decltype(nullptr));
			PlSpawnPointInfo(Plugin::InternalUse, int32_t handle);
			PlSpawnPointInfo(const PlSpawnPointInfo& other);
			PlSpawnPointInfo(PlSpawnPointInfo&& other);
			virtual ~PlSpawnPointInfo();
			PlSpawnPointInfo& operator=(const PlSpawnPointInfo& other);
			PlSpawnPointInfo& operator=(decltype(nullptr));
			PlSpawnPointInfo& operator=(PlSpawnPointInfo&& other);
			bool operator==(const PlSpawnPointInfo& other) const;
			bool operator!=(const PlSpawnPointInfo& other) const;
			PlSpawnPointInfo();
			System::String GetTitle();
			void SetTitle(System::String& value);
			System::String GetSpawnPt();
			void SetSpawnPt(System::String& value);
			System::String GetCameraStack();
			void SetCameraStack(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgeInfoStruct : virtual System::Object
		{
			PlAgeInfoStruct(decltype(nullptr));
			PlAgeInfoStruct(Plugin::InternalUse, int32_t handle);
			PlAgeInfoStruct(const PlAgeInfoStruct& other);
			PlAgeInfoStruct(PlAgeInfoStruct&& other);
			virtual ~PlAgeInfoStruct();
			PlAgeInfoStruct& operator=(const PlAgeInfoStruct& other);
			PlAgeInfoStruct& operator=(decltype(nullptr));
			PlAgeInfoStruct& operator=(PlAgeInfoStruct&& other);
			bool operator==(const PlAgeInfoStruct& other) const;
			bool operator!=(const PlAgeInfoStruct& other) const;
			PlAgeInfoStruct();
			System::Int32 GetFlagsInt();
			void SetFlagsInt(System::Int32 value);
			System::String GetAgeFilename();
			void SetAgeFilename(System::String& value);
			System::String GetAgeInstanceName();
			void SetAgeInstanceName(System::String& value);
			UPlasma::PlEmu::PlUuid GetAgeInstanceGuid();
			void SetAgeInstanceGuid(UPlasma::PlEmu::PlUuid& value);
			System::String GetAgeUserDefinedName();
			void SetAgeUserDefinedName(System::String& value);
			System::String GetAgeDescription();
			void SetAgeDescription(System::String& value);
			System::Int32 GetAgeSequenceNumber();
			void SetAgeSequenceNumber(System::Int32 value);
			System::Int32 GetAgeLanguage();
			void SetAgeLanguage(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgeLinkStruct : virtual System::Object
		{
			PlAgeLinkStruct(decltype(nullptr));
			PlAgeLinkStruct(Plugin::InternalUse, int32_t handle);
			PlAgeLinkStruct(const PlAgeLinkStruct& other);
			PlAgeLinkStruct(PlAgeLinkStruct&& other);
			virtual ~PlAgeLinkStruct();
			PlAgeLinkStruct& operator=(const PlAgeLinkStruct& other);
			PlAgeLinkStruct& operator=(decltype(nullptr));
			PlAgeLinkStruct& operator=(PlAgeLinkStruct&& other);
			bool operator==(const PlAgeLinkStruct& other) const;
			bool operator!=(const PlAgeLinkStruct& other) const;
			PlAgeLinkStruct();
			System::Int32 GetFlagsInt();
			void SetFlagsInt(System::Int32 value);
			System::Int32 GetLinkingRulesInt();
			void SetLinkingRulesInt(System::Int32 value);
			System::Int32 GetAmCCR();
			void SetAmCCR(System::Int32 value);
			UPlasma::PlEmu::PlAgeInfoStruct GetAgeInfo();
			void SetAgeInfo(UPlasma::PlEmu::PlAgeInfoStruct& value);
			UPlasma::PlEmu::PlSpawnPointInfo GetSpawnPoint();
			void SetSpawnPoint(UPlasma::PlEmu::PlSpawnPointInfo& value);
			System::String GetParentAgeFilename();
			void SetParentAgeFilename(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlAgeLinkEffects : virtual System::Object
		{
			PlAgeLinkEffects(decltype(nullptr));
			PlAgeLinkEffects(Plugin::InternalUse, int32_t handle);
			PlAgeLinkEffects(const PlAgeLinkEffects& other);
			PlAgeLinkEffects(PlAgeLinkEffects&& other);
			virtual ~PlAgeLinkEffects();
			PlAgeLinkEffects& operator=(const PlAgeLinkEffects& other);
			PlAgeLinkEffects& operator=(decltype(nullptr));
			PlAgeLinkEffects& operator=(PlAgeLinkEffects&& other);
			bool operator==(const PlAgeLinkEffects& other) const;
			bool operator!=(const PlAgeLinkEffects& other) const;
			PlAgeLinkEffects();
			System::String GetLinkInAnimName();
			void SetLinkInAnimName(System::String& value);
			System::Boolean GetBool1();
			void SetBool1(System::Boolean value);
			System::Boolean GetBool2();
			void SetBool2(System::Boolean value);
			System::Boolean GetBool3();
			void SetBool3(System::Boolean value);
			System::Boolean GetBool4();
			void SetBool4(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLinkToAgeMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlLinkToAgeMsg(decltype(nullptr));
			PlLinkToAgeMsg(Plugin::InternalUse, int32_t handle);
			PlLinkToAgeMsg(const PlLinkToAgeMsg& other);
			PlLinkToAgeMsg(PlLinkToAgeMsg&& other);
			virtual ~PlLinkToAgeMsg();
			PlLinkToAgeMsg& operator=(const PlLinkToAgeMsg& other);
			PlLinkToAgeMsg& operator=(decltype(nullptr));
			PlLinkToAgeMsg& operator=(PlLinkToAgeMsg&& other);
			bool operator==(const PlLinkToAgeMsg& other) const;
			bool operator!=(const PlLinkToAgeMsg& other) const;
			PlLinkToAgeMsg();
			UPlasma::PlEmu::PlAgeLinkStruct GetAgeLink();
			void SetAgeLink(UPlasma::PlEmu::PlAgeLinkStruct& value);
			UPlasma::PlEmu::PlAgeLinkEffects GetLinkEffects();
			void SetLinkEffects(UPlasma::PlEmu::PlAgeLinkEffects& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlOneShotMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlOneShotMsg(decltype(nullptr));
			PlOneShotMsg(Plugin::InternalUse, int32_t handle);
			PlOneShotMsg(const PlOneShotMsg& other);
			PlOneShotMsg(PlOneShotMsg&& other);
			virtual ~PlOneShotMsg();
			PlOneShotMsg& operator=(const PlOneShotMsg& other);
			PlOneShotMsg& operator=(decltype(nullptr));
			PlOneShotMsg& operator=(PlOneShotMsg&& other);
			bool operator==(const PlOneShotMsg& other) const;
			bool operator!=(const PlOneShotMsg& other) const;
			PlOneShotMsg();
			System::Collections::Generic::List_1<UPlasma::PlEmu::PlOneShotCallback> GetCallbacks();
			void SetCallbacks(System::Collections::Generic::List_1<UPlasma::PlEmu::PlOneShotCallback>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlTimerCallbackMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlTimerCallbackMsg(decltype(nullptr));
			PlTimerCallbackMsg(Plugin::InternalUse, int32_t handle);
			PlTimerCallbackMsg(const PlTimerCallbackMsg& other);
			PlTimerCallbackMsg(PlTimerCallbackMsg&& other);
			virtual ~PlTimerCallbackMsg();
			PlTimerCallbackMsg& operator=(const PlTimerCallbackMsg& other);
			PlTimerCallbackMsg& operator=(decltype(nullptr));
			PlTimerCallbackMsg& operator=(PlTimerCallbackMsg&& other);
			bool operator==(const PlTimerCallbackMsg& other) const;
			bool operator!=(const PlTimerCallbackMsg& other) const;
			PlTimerCallbackMsg();
			System::Int32 GetId();
			void SetId(System::Int32 value);
			System::Single GetTime();
			void SetTime(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlEnableMsg : virtual UPlasma::PlEmu::PlMessage
		{
			PlEnableMsg(decltype(nullptr));
			PlEnableMsg(Plugin::InternalUse, int32_t handle);
			PlEnableMsg(const PlEnableMsg& other);
			PlEnableMsg(PlEnableMsg&& other);
			virtual ~PlEnableMsg();
			PlEnableMsg& operator=(const PlEnableMsg& other);
			PlEnableMsg& operator=(decltype(nullptr));
			PlEnableMsg& operator=(PlEnableMsg&& other);
			bool operator==(const PlEnableMsg& other) const;
			bool operator!=(const PlEnableMsg& other) const;
			PlEnableMsg();
			System::Collections::Generic::List_1<System::Int32> GetCommandsInt();
			void SetCommandsInt(System::Collections::Generic::List_1<System::Int32>& value);
			System::Collections::Generic::List_1<System::Int32> GetTypesInt();
			void SetTypesInt(System::Collections::Generic::List_1<System::Int32>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlSoundMsg : virtual UPlasma::PlEmu::PlMessageWithCallbacks
		{
			PlSoundMsg(decltype(nullptr));
			PlSoundMsg(Plugin::InternalUse, int32_t handle);
			PlSoundMsg(const PlSoundMsg& other);
			PlSoundMsg(PlSoundMsg&& other);
			virtual ~PlSoundMsg();
			PlSoundMsg& operator=(const PlSoundMsg& other);
			PlSoundMsg& operator=(decltype(nullptr));
			PlSoundMsg& operator=(PlSoundMsg&& other);
			bool operator==(const PlSoundMsg& other) const;
			bool operator!=(const PlSoundMsg& other) const;
			PlSoundMsg();
			System::Collections::Generic::List_1<System::Int32> GetCommandsInt();
			void SetCommandsInt(System::Collections::Generic::List_1<System::Int32>& value);
			System::Int32 GetFadeTypeInt();
			void SetFadeTypeInt(System::Int32 value);
			System::Single GetBegin();
			void SetBegin(System::Single value);
			System::Single GetEnd();
			void SetEnd(System::Single value);
			System::Boolean GetLoop();
			void SetLoop(System::Boolean value);
			System::Boolean GetPlaying();
			void SetPlaying(System::Boolean value);
			System::Single GetSpeed();
			void SetSpeed(System::Single value);
			System::Single GetTime();
			void SetTime(System::Single value);
			System::Int32 GetIndex();
			void SetIndex(System::Int32 value);
			System::Int32 GetRepeats();
			void SetRepeats(System::Int32 value);
			System::Int32 GetNameStr();
			void SetNameStr(System::Int32 value);
			System::Single GetVolume();
			void SetVolume(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlOneShotCallback : virtual System::Object
		{
			PlOneShotCallback(decltype(nullptr));
			PlOneShotCallback(Plugin::InternalUse, int32_t handle);
			PlOneShotCallback(const PlOneShotCallback& other);
			PlOneShotCallback(PlOneShotCallback&& other);
			virtual ~PlOneShotCallback();
			PlOneShotCallback& operator=(const PlOneShotCallback& other);
			PlOneShotCallback& operator=(decltype(nullptr));
			PlOneShotCallback& operator=(PlOneShotCallback&& other);
			bool operator==(const PlOneShotCallback& other) const;
			bool operator!=(const PlOneShotCallback& other) const;
			PlOneShotCallback();
			System::String GetMarker();
			void SetMarker(System::String& value);
			UPlasma::PlEmu::IPlMessageable GetReceiver();
			void SetReceiver(UPlasma::PlEmu::IPlMessageable& value);
			System::Int32 GetUser();
			void SetUser(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlViewFaceModifier : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::ICannotBeBatched
		{
			PlViewFaceModifier(decltype(nullptr));
			PlViewFaceModifier(Plugin::InternalUse, int32_t handle);
			PlViewFaceModifier(const PlViewFaceModifier& other);
			PlViewFaceModifier(PlViewFaceModifier&& other);
			virtual ~PlViewFaceModifier();
			PlViewFaceModifier& operator=(const PlViewFaceModifier& other);
			PlViewFaceModifier& operator=(decltype(nullptr));
			PlViewFaceModifier& operator=(PlViewFaceModifier&& other);
			bool operator==(const PlViewFaceModifier& other) const;
			bool operator!=(const PlViewFaceModifier& other) const;
			System::Collections::Generic::List_1<System::Int32> GetFlags();
			void SetFlags(System::Collections::Generic::List_1<System::Int32>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFadeOpacityMod : virtual UnityEngine::MonoBehaviour
		{
			PlFadeOpacityMod(decltype(nullptr));
			PlFadeOpacityMod(Plugin::InternalUse, int32_t handle);
			PlFadeOpacityMod(const PlFadeOpacityMod& other);
			PlFadeOpacityMod(PlFadeOpacityMod&& other);
			virtual ~PlFadeOpacityMod();
			PlFadeOpacityMod& operator=(const PlFadeOpacityMod& other);
			PlFadeOpacityMod& operator=(decltype(nullptr));
			PlFadeOpacityMod& operator=(PlFadeOpacityMod&& other);
			bool operator==(const PlFadeOpacityMod& other) const;
			bool operator!=(const PlFadeOpacityMod& other) const;
			System::Int32 GetFlags();
			void SetFlags(System::Int32 value);
			System::Single GetFadeUp();
			void SetFadeUp(System::Single value);
			System::Single GetFadeDown();
			void SetFadeDown(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlLogicModifier : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlLogicModifier(decltype(nullptr));
			PlLogicModifier(Plugin::InternalUse, int32_t handle);
			PlLogicModifier(const PlLogicModifier& other);
			PlLogicModifier(PlLogicModifier&& other);
			virtual ~PlLogicModifier();
			PlLogicModifier& operator=(const PlLogicModifier& other);
			PlLogicModifier& operator=(decltype(nullptr));
			PlLogicModifier& operator=(PlLogicModifier&& other);
			bool operator==(const PlLogicModifier& other) const;
			bool operator!=(const PlLogicModifier& other) const;
			System::Int32 GetCursorInt();
			void SetCursorInt(System::Int32 value);
			System::Collections::Generic::List_1<UPlasma::PlEmu::PlConditionalObject> GetConditions();
			void SetConditions(System::Collections::Generic::List_1<UPlasma::PlEmu::PlConditionalObject>& value);
			UPlasma::PlEmu::PlMessage GetMessage();
			void SetMessage(UPlasma::PlEmu::PlMessage& value);
			void UpdateConditions();
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlDetector : virtual UnityEngine::MonoBehaviour
		{
			PlDetector(decltype(nullptr));
			PlDetector(Plugin::InternalUse, int32_t handle);
			PlDetector(const PlDetector& other);
			PlDetector(PlDetector&& other);
			virtual ~PlDetector();
			PlDetector& operator=(const PlDetector& other);
			PlDetector& operator=(decltype(nullptr));
			PlDetector& operator=(PlDetector&& other);
			bool operator==(const PlDetector& other) const;
			bool operator!=(const PlDetector& other) const;
			System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable> GetReceivers();
			void SetReceivers(System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlObjectInVolumeDetector : virtual UPlasma::PlEmu::PlDetector
		{
			PlObjectInVolumeDetector(decltype(nullptr));
			PlObjectInVolumeDetector(Plugin::InternalUse, int32_t handle);
			PlObjectInVolumeDetector(const PlObjectInVolumeDetector& other);
			PlObjectInVolumeDetector(PlObjectInVolumeDetector&& other);
			virtual ~PlObjectInVolumeDetector();
			PlObjectInVolumeDetector& operator=(const PlObjectInVolumeDetector& other);
			PlObjectInVolumeDetector& operator=(decltype(nullptr));
			PlObjectInVolumeDetector& operator=(PlObjectInVolumeDetector&& other);
			bool operator==(const PlObjectInVolumeDetector& other) const;
			bool operator!=(const PlObjectInVolumeDetector& other) const;
			System::Int32 GetCollisionTypeInt();
			void SetCollisionTypeInt(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPanicLinkRegion : virtual UPlasma::PlEmu::PlObjectInVolumeDetector
		{
			PlPanicLinkRegion(decltype(nullptr));
			PlPanicLinkRegion(Plugin::InternalUse, int32_t handle);
			PlPanicLinkRegion(const PlPanicLinkRegion& other);
			PlPanicLinkRegion(PlPanicLinkRegion&& other);
			virtual ~PlPanicLinkRegion();
			PlPanicLinkRegion& operator=(const PlPanicLinkRegion& other);
			PlPanicLinkRegion& operator=(decltype(nullptr));
			PlPanicLinkRegion& operator=(PlPanicLinkRegion&& other);
			bool operator==(const PlPanicLinkRegion& other) const;
			bool operator!=(const PlPanicLinkRegion& other) const;
			System::Int32 GetCollisionTypeInt();
			void SetCollisionTypeInt(System::Int32 value);
			System::Boolean GetPlayLinkOutAnim();
			void SetPlayLinkOutAnim(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlPickingDetector : virtual UPlasma::PlEmu::PlDetector
		{
			PlPickingDetector(decltype(nullptr));
			PlPickingDetector(Plugin::InternalUse, int32_t handle);
			PlPickingDetector(const PlPickingDetector& other);
			PlPickingDetector(PlPickingDetector&& other);
			virtual ~PlPickingDetector();
			PlPickingDetector& operator=(const PlPickingDetector& other);
			PlPickingDetector& operator=(decltype(nullptr));
			PlPickingDetector& operator=(PlPickingDetector&& other);
			bool operator==(const PlPickingDetector& other) const;
			bool operator!=(const PlPickingDetector& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlConditionalObject : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlConditionalObject(decltype(nullptr));
			PlConditionalObject(Plugin::InternalUse, int32_t handle);
			PlConditionalObject(const PlConditionalObject& other);
			PlConditionalObject(PlConditionalObject&& other);
			virtual ~PlConditionalObject();
			PlConditionalObject& operator=(const PlConditionalObject& other);
			PlConditionalObject& operator=(decltype(nullptr));
			PlConditionalObject& operator=(PlConditionalObject&& other);
			bool operator==(const PlConditionalObject& other) const;
			bool operator!=(const PlConditionalObject& other) const;
			System::Boolean GetSatisfied();
			void SetSatisfied(System::Boolean value);
			System::Boolean GetToggle();
			void SetToggle(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlActivatorConditionalObject : virtual UPlasma::PlEmu::PlConditionalObject, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlActivatorConditionalObject(decltype(nullptr));
			PlActivatorConditionalObject(Plugin::InternalUse, int32_t handle);
			PlActivatorConditionalObject(const PlActivatorConditionalObject& other);
			PlActivatorConditionalObject(PlActivatorConditionalObject&& other);
			virtual ~PlActivatorConditionalObject();
			PlActivatorConditionalObject& operator=(const PlActivatorConditionalObject& other);
			PlActivatorConditionalObject& operator=(decltype(nullptr));
			PlActivatorConditionalObject& operator=(PlActivatorConditionalObject&& other);
			bool operator==(const PlActivatorConditionalObject& other) const;
			bool operator!=(const PlActivatorConditionalObject& other) const;
			System::Collections::Generic::List_1<UPlasma::PlEmu::PlDetector> GetActivators();
			void SetActivators(System::Collections::Generic::List_1<UPlasma::PlEmu::PlDetector>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFacingConditionalObject : virtual UPlasma::PlEmu::PlConditionalObject, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlFacingConditionalObject(decltype(nullptr));
			PlFacingConditionalObject(Plugin::InternalUse, int32_t handle);
			PlFacingConditionalObject(const PlFacingConditionalObject& other);
			PlFacingConditionalObject(PlFacingConditionalObject&& other);
			virtual ~PlFacingConditionalObject();
			PlFacingConditionalObject& operator=(const PlFacingConditionalObject& other);
			PlFacingConditionalObject& operator=(decltype(nullptr));
			PlFacingConditionalObject& operator=(PlFacingConditionalObject&& other);
			bool operator==(const PlFacingConditionalObject& other) const;
			bool operator!=(const PlFacingConditionalObject& other) const;
			System::Single GetTolerance();
			void SetTolerance(System::Single value);
			System::Boolean GetDirectional();
			void SetDirectional(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlObjectInBoxConditionalObject : virtual UPlasma::PlEmu::PlConditionalObject, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlObjectInBoxConditionalObject(decltype(nullptr));
			PlObjectInBoxConditionalObject(Plugin::InternalUse, int32_t handle);
			PlObjectInBoxConditionalObject(const PlObjectInBoxConditionalObject& other);
			PlObjectInBoxConditionalObject(PlObjectInBoxConditionalObject&& other);
			virtual ~PlObjectInBoxConditionalObject();
			PlObjectInBoxConditionalObject& operator=(const PlObjectInBoxConditionalObject& other);
			PlObjectInBoxConditionalObject& operator=(decltype(nullptr));
			PlObjectInBoxConditionalObject& operator=(PlObjectInBoxConditionalObject&& other);
			bool operator==(const PlObjectInBoxConditionalObject& other) const;
			bool operator!=(const PlObjectInBoxConditionalObject& other) const;
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlVolumeSensorConditionalObject : virtual UPlasma::PlEmu::PlConditionalObject, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlVolumeSensorConditionalObject(decltype(nullptr));
			PlVolumeSensorConditionalObject(Plugin::InternalUse, int32_t handle);
			PlVolumeSensorConditionalObject(const PlVolumeSensorConditionalObject& other);
			PlVolumeSensorConditionalObject(PlVolumeSensorConditionalObject&& other);
			virtual ~PlVolumeSensorConditionalObject();
			PlVolumeSensorConditionalObject& operator=(const PlVolumeSensorConditionalObject& other);
			PlVolumeSensorConditionalObject& operator=(decltype(nullptr));
			PlVolumeSensorConditionalObject& operator=(PlVolumeSensorConditionalObject&& other);
			bool operator==(const PlVolumeSensorConditionalObject& other) const;
			bool operator!=(const PlVolumeSensorConditionalObject& other) const;
			System::Int32 GetTypeInt();
			void SetTypeInt(System::Int32 value);
			System::Int32 GetTrigNum();
			void SetTrigNum(System::Int32 value);
			System::Boolean GetFirst();
			void SetFirst(System::Boolean value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlResponder : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlResponder(decltype(nullptr));
			PlResponder(Plugin::InternalUse, int32_t handle);
			PlResponder(const PlResponder& other);
			PlResponder(PlResponder&& other);
			virtual ~PlResponder();
			PlResponder& operator=(const PlResponder& other);
			PlResponder& operator=(decltype(nullptr));
			PlResponder& operator=(PlResponder&& other);
			bool operator==(const PlResponder& other) const;
			bool operator!=(const PlResponder& other) const;
			System::Int32 GetFlagsInt();
			void SetFlagsInt(System::Int32 value);
			System::Int32 GetCurState();
			void SetCurState(System::Int32 value);
			System::Array1<UPlasma::PlEmu::PlResponderState> GetStates();
			void SetStates(System::Array1<UPlasma::PlEmu::PlResponderState>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlResponderState : virtual System::Object
		{
			PlResponderState(decltype(nullptr));
			PlResponderState(Plugin::InternalUse, int32_t handle);
			PlResponderState(const PlResponderState& other);
			PlResponderState(PlResponderState&& other);
			virtual ~PlResponderState();
			PlResponderState& operator=(const PlResponderState& other);
			PlResponderState& operator=(decltype(nullptr));
			PlResponderState& operator=(PlResponderState&& other);
			bool operator==(const PlResponderState& other) const;
			bool operator!=(const PlResponderState& other) const;
			PlResponderState();
			System::Int32 GetNumCallbacks();
			void SetNumCallbacks(System::Int32 value);
			System::Int32 GetSwitchToState();
			void SetSwitchToState(System::Int32 value);
			System::Array1<UPlasma::PlEmu::PlResponderCommand> GetCommands();
			void SetCommands(System::Array1<UPlasma::PlEmu::PlResponderCommand>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlResponderCommand : virtual System::Object
		{
			PlResponderCommand(decltype(nullptr));
			PlResponderCommand(Plugin::InternalUse, int32_t handle);
			PlResponderCommand(const PlResponderCommand& other);
			PlResponderCommand(PlResponderCommand&& other);
			virtual ~PlResponderCommand();
			PlResponderCommand& operator=(const PlResponderCommand& other);
			PlResponderCommand& operator=(decltype(nullptr));
			PlResponderCommand& operator=(PlResponderCommand&& other);
			bool operator==(const PlResponderCommand& other) const;
			bool operator!=(const PlResponderCommand& other) const;
			PlResponderCommand();
			UPlasma::PlEmu::PlMessage GetMessage();
			void SetMessage(UPlasma::PlEmu::PlMessage& value);
			System::Int32 GetWaitOn();
			void SetWaitOn(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMessageForwarder : virtual UnityEngine::MonoBehaviour, virtual UPlasma::PlEmu::IPlMessageable
		{
			PlMessageForwarder(decltype(nullptr));
			PlMessageForwarder(Plugin::InternalUse, int32_t handle);
			PlMessageForwarder(const PlMessageForwarder& other);
			PlMessageForwarder(PlMessageForwarder&& other);
			virtual ~PlMessageForwarder();
			PlMessageForwarder& operator=(const PlMessageForwarder& other);
			PlMessageForwarder& operator=(decltype(nullptr));
			PlMessageForwarder& operator=(PlMessageForwarder&& other);
			bool operator==(const PlMessageForwarder& other) const;
			bool operator!=(const PlMessageForwarder& other) const;
			System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable> GetForwardKeys();
			void SetForwardKeys(System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlMaintainersMarkerModifier : virtual UnityEngine::MonoBehaviour
		{
			PlMaintainersMarkerModifier(decltype(nullptr));
			PlMaintainersMarkerModifier(Plugin::InternalUse, int32_t handle);
			PlMaintainersMarkerModifier(const PlMaintainersMarkerModifier& other);
			PlMaintainersMarkerModifier(PlMaintainersMarkerModifier&& other);
			virtual ~PlMaintainersMarkerModifier();
			PlMaintainersMarkerModifier& operator=(const PlMaintainersMarkerModifier& other);
			PlMaintainersMarkerModifier& operator=(decltype(nullptr));
			PlMaintainersMarkerModifier& operator=(PlMaintainersMarkerModifier&& other);
			bool operator==(const PlMaintainersMarkerModifier& other) const;
			bool operator!=(const PlMaintainersMarkerModifier& other) const;
			System::Int32 GetCalibrationLevelInt();
			void SetCalibrationLevelInt(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlKey : virtual System::Object
		{
			PlKey(decltype(nullptr));
			PlKey(Plugin::InternalUse, int32_t handle);
			PlKey(const PlKey& other);
			PlKey(PlKey&& other);
			virtual ~PlKey();
			PlKey& operator=(const PlKey& other);
			PlKey& operator=(decltype(nullptr));
			PlKey& operator=(PlKey&& other);
			bool operator==(const PlKey& other) const;
			bool operator!=(const PlKey& other) const;
			System::Int32 GetAgePrefix();
			void SetAgePrefix(System::Int32 value);
			System::Int32 GetPageId();
			void SetPageId(System::Int32 value);
			System::Int16 GetClassType();
			void SetClassType(System::Int16 value);
			System::String GetObjName();
			void SetObjName(System::String& value);
			UnityEngine::Object GetObj();
			void SetObj(UnityEngine::Object& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct AssetsLifeBinder : virtual UnityEngine::MonoBehaviour
		{
			AssetsLifeBinder(decltype(nullptr));
			AssetsLifeBinder(Plugin::InternalUse, int32_t handle);
			AssetsLifeBinder(const AssetsLifeBinder& other);
			AssetsLifeBinder(AssetsLifeBinder&& other);
			virtual ~AssetsLifeBinder();
			AssetsLifeBinder& operator=(const AssetsLifeBinder& other);
			AssetsLifeBinder& operator=(decltype(nullptr));
			AssetsLifeBinder& operator=(AssetsLifeBinder&& other);
			bool operator==(const AssetsLifeBinder& other) const;
			bool operator!=(const AssetsLifeBinder& other) const;
			System::Collections::Generic::List_1<UnityEngine::Object> GetAssets();
			void SetAssets(System::Collections::Generic::List_1<UnityEngine::Object>& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlArmatureMod : virtual UnityEngine::MonoBehaviour
		{
			PlArmatureMod(decltype(nullptr));
			PlArmatureMod(Plugin::InternalUse, int32_t handle);
			PlArmatureMod(const PlArmatureMod& other);
			PlArmatureMod(PlArmatureMod&& other);
			virtual ~PlArmatureMod();
			PlArmatureMod& operator=(const PlArmatureMod& other);
			PlArmatureMod& operator=(decltype(nullptr));
			PlArmatureMod& operator=(PlArmatureMod&& other);
			bool operator==(const PlArmatureMod& other) const;
			bool operator!=(const PlArmatureMod& other) const;
			System::Int32 GetBodyTypeInt();
			void SetBodyTypeInt(System::Int32 value);
			System::String GetRootName();
			void SetRootName(System::String& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct WaterStateParams : virtual System::Object
		{
			WaterStateParams(decltype(nullptr));
			WaterStateParams(Plugin::InternalUse, int32_t handle);
			WaterStateParams(const WaterStateParams& other);
			WaterStateParams(WaterStateParams&& other);
			virtual ~WaterStateParams();
			WaterStateParams& operator=(const WaterStateParams& other);
			WaterStateParams& operator=(decltype(nullptr));
			WaterStateParams& operator=(WaterStateParams&& other);
			bool operator==(const WaterStateParams& other) const;
			bool operator!=(const WaterStateParams& other) const;
			System::Single GetRippleScale();
			void SetRippleScale(System::Single value);
			System::Single GetWaterHeight();
			void SetWaterHeight(System::Single value);
			System::Single GetWispiness();
			void SetWispiness(System::Single value);
			System::Single GetPeriod();
			void SetPeriod(System::Single value);
			System::Single GetFingerlength();
			void SetFingerlength(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct WaterState : virtual System::Object
		{
			WaterState(decltype(nullptr));
			WaterState(Plugin::InternalUse, int32_t handle);
			WaterState(const WaterState& other);
			WaterState(WaterState&& other);
			virtual ~WaterState();
			WaterState& operator=(const WaterState& other);
			WaterState& operator=(decltype(nullptr));
			WaterState& operator=(WaterState&& other);
			bool operator==(const WaterState& other) const;
			bool operator!=(const WaterState& other) const;
			System::Single GetMaxLen();
			void SetMaxLen(System::Single value);
			System::Single GetMinLen();
			void SetMinLen(System::Single value);
			System::Single GetAmpOverlen();
			void SetAmpOverlen(System::Single value);
			System::Single GetChop();
			void SetChop(System::Single value);
			System::Single GetAngleDev();
			void SetAngleDev(System::Single value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlFixedWaterState7 : virtual System::Object
		{
			PlFixedWaterState7(decltype(nullptr));
			PlFixedWaterState7(Plugin::InternalUse, int32_t handle);
			PlFixedWaterState7(const PlFixedWaterState7& other);
			PlFixedWaterState7(PlFixedWaterState7&& other);
			virtual ~PlFixedWaterState7();
			PlFixedWaterState7& operator=(const PlFixedWaterState7& other);
			PlFixedWaterState7& operator=(decltype(nullptr));
			PlFixedWaterState7& operator=(PlFixedWaterState7&& other);
			bool operator==(const PlFixedWaterState7& other) const;
			bool operator!=(const PlFixedWaterState7& other) const;
			UPlasma::PlEmu::WaterState GetGeoState();
			void SetGeoState(UPlasma::PlEmu::WaterState& value);
			UPlasma::PlEmu::WaterState GetTexState();
			void SetTexState(UPlasma::PlEmu::WaterState& value);
			UPlasma::PlEmu::WaterStateParams GetStateParams();
			void SetStateParams(UPlasma::PlEmu::WaterStateParams& value);
			UnityEngine::Vector3 GetWindDir();
			void SetWindDir(UnityEngine::Vector3& value);
			UnityEngine::Vector3 GetSpecVec();
			void SetSpecVec(UnityEngine::Vector3& value);
			UnityEngine::Vector3 GetWaterOffset();
			void SetWaterOffset(UnityEngine::Vector3& value);
			UnityEngine::Vector3 GetMaxAtten();
			void SetMaxAtten(UnityEngine::Vector3& value);
			UnityEngine::Vector3 GetMinAtten();
			void SetMinAtten(UnityEngine::Vector3& value);
			UnityEngine::Vector3 GetDepthFalloff();
			void SetDepthFalloff(UnityEngine::Vector3& value);
			UnityEngine::Color GetShoreTint();
			void SetShoreTint(UnityEngine::Color& value);
			UnityEngine::Color GetMaxColor();
			void SetMaxColor(UnityEngine::Color& value);
			UnityEngine::Color GetMinColor();
			void SetMinColor(UnityEngine::Color& value);
			System::Single GetEdgeOpacity();
			void SetEdgeOpacity(System::Single value);
			System::Single GetEdgeRadius();
			void SetEdgeRadius(System::Single value);
			UnityEngine::Color GetWaterTint();
			void SetWaterTint(UnityEngine::Color& value);
			UnityEngine::Color GetSpecularTint();
			void SetSpecularTint(UnityEngine::Color& value);
			System::Single GetEnvRefresh();
			void SetEnvRefresh(System::Single value);
			System::Single GetEnvRadius();
			void SetEnvRadius(System::Single value);
			UnityEngine::Vector3 GetEnvCenter();
			void SetEnvCenter(UnityEngine::Vector3& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlWaveSet7 : virtual UnityEngine::MonoBehaviour
		{
			PlWaveSet7(decltype(nullptr));
			PlWaveSet7(Plugin::InternalUse, int32_t handle);
			PlWaveSet7(const PlWaveSet7& other);
			PlWaveSet7(PlWaveSet7&& other);
			virtual ~PlWaveSet7();
			PlWaveSet7& operator=(const PlWaveSet7& other);
			PlWaveSet7& operator=(decltype(nullptr));
			PlWaveSet7& operator=(PlWaveSet7&& other);
			bool operator==(const PlWaveSet7& other) const;
			bool operator!=(const PlWaveSet7& other) const;
			System::Single GetMaxLen();
			void SetMaxLen(System::Single value);
			UPlasma::PlEmu::PlFixedWaterState7 GetState();
			void SetState(UPlasma::PlEmu::PlFixedWaterState7& value);
			System::Array1<UnityEngine::GameObject> GetShores();
			void SetShores(System::Array1<UnityEngine::GameObject>& value);
			System::Array1<UnityEngine::GameObject> GetDecals();
			void SetDecals(System::Array1<UnityEngine::GameObject>& value);
			UnityEngine::ReflectionProbe GetEnvMap();
			void SetEnvMap(UnityEngine::ReflectionProbe& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct MatOverride : virtual System::Object
		{
			MatOverride(decltype(nullptr));
			MatOverride(Plugin::InternalUse, int32_t handle);
			MatOverride(const MatOverride& other);
			MatOverride(MatOverride&& other);
			virtual ~MatOverride();
			MatOverride& operator=(const MatOverride& other);
			MatOverride& operator=(decltype(nullptr));
			MatOverride& operator=(MatOverride&& other);
			bool operator==(const MatOverride& other) const;
			bool operator!=(const MatOverride& other) const;
			MatOverride();
			System::String GetShaderName();
			void SetShaderName(System::String& value);
			System::Int32 GetRenderQueue();
			void SetRenderQueue(System::Int32 value);
			void ApplyPropsToMat(UnityEngine::Material& mat);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct MeshRendererOverride : virtual System::Object
		{
			MeshRendererOverride(decltype(nullptr));
			MeshRendererOverride(Plugin::InternalUse, int32_t handle);
			MeshRendererOverride(const MeshRendererOverride& other);
			MeshRendererOverride(MeshRendererOverride&& other);
			virtual ~MeshRendererOverride();
			MeshRendererOverride& operator=(const MeshRendererOverride& other);
			MeshRendererOverride& operator=(decltype(nullptr));
			MeshRendererOverride& operator=(MeshRendererOverride&& other);
			bool operator==(const MeshRendererOverride& other) const;
			bool operator!=(const MeshRendererOverride& other) const;
			UnityEngine::Rendering::ShadowCastingMode GetCastShadows();
			void SetCastShadows(UnityEngine::Rendering::ShadowCastingMode value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct LightOverride : virtual System::Object
		{
			LightOverride(decltype(nullptr));
			LightOverride(Plugin::InternalUse, int32_t handle);
			LightOverride(const LightOverride& other);
			LightOverride(LightOverride&& other);
			virtual ~LightOverride();
			LightOverride& operator=(const LightOverride& other);
			LightOverride& operator=(decltype(nullptr));
			LightOverride& operator=(LightOverride&& other);
			bool operator==(const LightOverride& other) const;
			bool operator!=(const LightOverride& other) const;
			System::Single GetRange();
			void SetRange(System::Single value);
			System::Single GetIntensity();
			void SetIntensity(System::Single value);
			System::Boolean GetShadows();
			void SetShadows(System::Boolean value);
			System::Single GetShadowStrength();
			void SetShadowStrength(System::Single value);
			System::Int32 GetLayers();
			void SetLayers(System::Int32 value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct ObjOverride : virtual System::Object
		{
			ObjOverride(decltype(nullptr));
			ObjOverride(Plugin::InternalUse, int32_t handle);
			ObjOverride(const ObjOverride& other);
			ObjOverride(ObjOverride&& other);
			virtual ~ObjOverride();
			ObjOverride& operator=(const ObjOverride& other);
			ObjOverride& operator=(decltype(nullptr));
			ObjOverride& operator=(ObjOverride&& other);
			bool operator==(const ObjOverride& other) const;
			bool operator!=(const ObjOverride& other) const;
			System::Boolean GetEnableSelf();
			void SetEnableSelf(System::Boolean value);
			System::Boolean GetEnableDraw();
			void SetEnableDraw(System::Boolean value);
			System::Boolean GetEnablePhys();
			void SetEnablePhys(System::Boolean value);
			System::Boolean GetEnableLight();
			void SetEnableLight(System::Boolean value);
			System::Boolean GetEnableAnim();
			void SetEnableAnim(System::Boolean value);
			System::Boolean GetEnableAudio();
			void SetEnableAudio(System::Boolean value);
			UPlasma::PlEmu::LightOverride GetLightOverride();
			void SetLightOverride(UPlasma::PlEmu::LightOverride& value);
			UPlasma::PlEmu::MeshRendererOverride GetMeshRendererOverride();
			void SetMeshRendererOverride(UPlasma::PlEmu::MeshRendererOverride& value);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct PrpImportClues : virtual System::Object
		{
			PrpImportClues(decltype(nullptr));
			PrpImportClues(Plugin::InternalUse, int32_t handle);
			PrpImportClues(const PrpImportClues& other);
			PrpImportClues(PrpImportClues&& other);
			virtual ~PrpImportClues();
			PrpImportClues& operator=(const PrpImportClues& other);
			PrpImportClues& operator=(decltype(nullptr));
			PrpImportClues& operator=(PrpImportClues&& other);
			bool operator==(const PrpImportClues& other) const;
			bool operator!=(const PrpImportClues& other) const;
			PrpImportClues();
			System::Boolean GetLoad();
			void SetLoad(System::Boolean value);
			void Save();
			UPlasma::PlEmu::ObjOverride GetObjOverride(System::String& objName);
			UPlasma::PlEmu::MatOverride GetMatOverride(System::String& matName);
			void AddMatOverride(System::String& matName, UPlasma::PlEmu::MatOverride& overrde);
		};
	}
}

namespace UPlasma
{
	namespace PlEmu
	{
		struct NativeHelpers : virtual System::Object
		{
			NativeHelpers(decltype(nullptr));
			NativeHelpers(Plugin::InternalUse, int32_t handle);
			NativeHelpers(const NativeHelpers& other);
			NativeHelpers(NativeHelpers&& other);
			virtual ~NativeHelpers();
			NativeHelpers& operator=(const NativeHelpers& other);
			NativeHelpers& operator=(decltype(nullptr));
			NativeHelpers& operator=(NativeHelpers&& other);
			bool operator==(const NativeHelpers& other) const;
			bool operator!=(const NativeHelpers& other) const;
			template<typename MT0> static System::Boolean HasComponent(UnityEngine::GameObject& obj);
			template<typename MT0> static System::Boolean HasComponentInParent(UnityEngine::GameObject& obj);
			static void CreateFacesForHull(UnityEngine::Mesh& mesh);
			static void DebugType(System::Object& obj);
			static System::String ToString(System::Object& obj);
			static System::Boolean GetObjOverrideLayer(UPlasma::PlEmu::PrpImportClues& clues, System::String& objName, System::Int32* layer);
			static void AddIntToObjList(System::Collections::Generic::List_1<System::Object>& l, System::Int32 i);
			static void AddBoolToObjList(System::Collections::Generic::List_1<System::Object>& l, System::Boolean b);
			static void IsInt(System::Object* thing, System::Boolean* result);
			static System::Boolean IsSameType(System::Object* thingA, System::Object* thingB);
			static void SetLeafControllerKeyframe(UPlasma::PlEmu::Point3Controller& controller, UPlasma::PlEmu::Point3Key& keyframe, System::Int32 index);
			static void SetLeafControllerKeyframe(UPlasma::PlEmu::ScalarController& controller, UPlasma::PlEmu::ScalarKey& keyframe, System::Int32 index);
			static void SetLeafControllerKeyframe(UPlasma::PlEmu::QuatController& controller, UPlasma::PlEmu::QuatKey& keyframe, System::Int32 index);
			static void SetLeafControllerNumKeyframe(UPlasma::PlEmu::Point3Controller& controller, System::Int32 numKeyframes);
			static void SetLeafControllerNumKeyframe(UPlasma::PlEmu::ScalarController& controller, System::Int32 numKeyframes);
			static void SetLeafControllerNumKeyframe(UPlasma::PlEmu::QuatController& controller, System::Int32 numKeyframes);
			template<typename MT0> static MT0 CastObjTo(System::Object& obj);
		};
	}
}

namespace System
{
	struct IDisposable : virtual System::Object
	{
		IDisposable(decltype(nullptr));
		IDisposable(Plugin::InternalUse, int32_t handle);
		IDisposable(const IDisposable& other);
		IDisposable(IDisposable&& other);
		virtual ~IDisposable();
		IDisposable& operator=(const IDisposable& other);
		IDisposable& operator=(decltype(nullptr));
		IDisposable& operator=(IDisposable&& other);
		bool operator==(const IDisposable& other) const;
		bool operator!=(const IDisposable& other) const;
		void Dispose();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Object> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Object>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Object>(const IEnumerator_1<UnityEngine::Object>& other);
				IEnumerator_1<UnityEngine::Object>(IEnumerator_1<UnityEngine::Object>&& other);
				virtual ~IEnumerator_1<UnityEngine::Object>();
				IEnumerator_1<UnityEngine::Object>& operator=(const IEnumerator_1<UnityEngine::Object>& other);
				IEnumerator_1<UnityEngine::Object>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Object>& operator=(IEnumerator_1<UnityEngine::Object>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Object>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Object>& other) const;
				UnityEngine::Object GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::GameObject> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::GameObject>(decltype(nullptr));
				IEnumerator_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::GameObject>(const IEnumerator_1<UnityEngine::GameObject>& other);
				IEnumerator_1<UnityEngine::GameObject>(IEnumerator_1<UnityEngine::GameObject>&& other);
				virtual ~IEnumerator_1<UnityEngine::GameObject>();
				IEnumerator_1<UnityEngine::GameObject>& operator=(const IEnumerator_1<UnityEngine::GameObject>& other);
				IEnumerator_1<UnityEngine::GameObject>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::GameObject>& operator=(IEnumerator_1<UnityEngine::GameObject>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::GameObject>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::GameObject>& other) const;
				UnityEngine::GameObject GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Transform> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Transform>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Transform>(const IEnumerator_1<UnityEngine::Transform>& other);
				IEnumerator_1<UnityEngine::Transform>(IEnumerator_1<UnityEngine::Transform>&& other);
				virtual ~IEnumerator_1<UnityEngine::Transform>();
				IEnumerator_1<UnityEngine::Transform>& operator=(const IEnumerator_1<UnityEngine::Transform>& other);
				IEnumerator_1<UnityEngine::Transform>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Transform>& operator=(IEnumerator_1<UnityEngine::Transform>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Transform>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Transform>& other) const;
				UnityEngine::Transform GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::MeshFilter> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::MeshFilter>(decltype(nullptr));
				IEnumerator_1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::MeshFilter>(const IEnumerator_1<UnityEngine::MeshFilter>& other);
				IEnumerator_1<UnityEngine::MeshFilter>(IEnumerator_1<UnityEngine::MeshFilter>&& other);
				virtual ~IEnumerator_1<UnityEngine::MeshFilter>();
				IEnumerator_1<UnityEngine::MeshFilter>& operator=(const IEnumerator_1<UnityEngine::MeshFilter>& other);
				IEnumerator_1<UnityEngine::MeshFilter>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::MeshFilter>& operator=(IEnumerator_1<UnityEngine::MeshFilter>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::MeshFilter>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::MeshFilter>& other) const;
				UnityEngine::MeshFilter GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Material> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Material>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Material>(const IEnumerator_1<UnityEngine::Material>& other);
				IEnumerator_1<UnityEngine::Material>(IEnumerator_1<UnityEngine::Material>&& other);
				virtual ~IEnumerator_1<UnityEngine::Material>();
				IEnumerator_1<UnityEngine::Material>& operator=(const IEnumerator_1<UnityEngine::Material>& other);
				IEnumerator_1<UnityEngine::Material>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Material>& operator=(IEnumerator_1<UnityEngine::Material>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Material>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Material>& other) const;
				UnityEngine::Material GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Texture2D> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Texture2D>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Texture2D>(const IEnumerator_1<UnityEngine::Texture2D>& other);
				IEnumerator_1<UnityEngine::Texture2D>(IEnumerator_1<UnityEngine::Texture2D>&& other);
				virtual ~IEnumerator_1<UnityEngine::Texture2D>();
				IEnumerator_1<UnityEngine::Texture2D>& operator=(const IEnumerator_1<UnityEngine::Texture2D>& other);
				IEnumerator_1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Texture2D>& operator=(IEnumerator_1<UnityEngine::Texture2D>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Texture2D>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Texture2D>& other) const;
				UnityEngine::Texture2D GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Cubemap> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Cubemap>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Cubemap>(const IEnumerator_1<UnityEngine::Cubemap>& other);
				IEnumerator_1<UnityEngine::Cubemap>(IEnumerator_1<UnityEngine::Cubemap>&& other);
				virtual ~IEnumerator_1<UnityEngine::Cubemap>();
				IEnumerator_1<UnityEngine::Cubemap>& operator=(const IEnumerator_1<UnityEngine::Cubemap>& other);
				IEnumerator_1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Cubemap>& operator=(IEnumerator_1<UnityEngine::Cubemap>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Cubemap>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Cubemap>& other) const;
				UnityEngine::Cubemap GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Matrix4x4> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Matrix4x4>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Matrix4x4>(const IEnumerator_1<UnityEngine::Matrix4x4>& other);
				IEnumerator_1<UnityEngine::Matrix4x4>(IEnumerator_1<UnityEngine::Matrix4x4>&& other);
				virtual ~IEnumerator_1<UnityEngine::Matrix4x4>();
				IEnumerator_1<UnityEngine::Matrix4x4>& operator=(const IEnumerator_1<UnityEngine::Matrix4x4>& other);
				IEnumerator_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Matrix4x4>& operator=(IEnumerator_1<UnityEngine::Matrix4x4>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Matrix4x4>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Matrix4x4>& other) const;
				UnityEngine::Matrix4x4 GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::IPlMessageable> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::IPlMessageable>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::IPlMessageable>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::IPlMessageable>(const IEnumerator_1<UPlasma::PlEmu::IPlMessageable>& other);
				IEnumerator_1<UPlasma::PlEmu::IPlMessageable>(IEnumerator_1<UPlasma::PlEmu::IPlMessageable>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::IPlMessageable>();
				IEnumerator_1<UPlasma::PlEmu::IPlMessageable>& operator=(const IEnumerator_1<UPlasma::PlEmu::IPlMessageable>& other);
				IEnumerator_1<UPlasma::PlEmu::IPlMessageable>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::IPlMessageable>& operator=(IEnumerator_1<UPlasma::PlEmu::IPlMessageable>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				UPlasma::PlEmu::IPlMessageable GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlConditionalObject> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>(const IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>(IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>();
				IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>& operator=(IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				UPlasma::PlEmu::PlConditionalObject GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlDetector> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlDetector>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlDetector>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlDetector>(const IEnumerator_1<UPlasma::PlEmu::PlDetector>& other);
				IEnumerator_1<UPlasma::PlEmu::PlDetector>(IEnumerator_1<UPlasma::PlEmu::PlDetector>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlDetector>();
				IEnumerator_1<UPlasma::PlEmu::PlDetector>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlDetector>& other);
				IEnumerator_1<UPlasma::PlEmu::PlDetector>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlDetector>& operator=(IEnumerator_1<UPlasma::PlEmu::PlDetector>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlDetector>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlDetector>& other) const;
				UPlasma::PlEmu::PlDetector GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlMessage> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlMessage>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlMessage>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlMessage>(const IEnumerator_1<UPlasma::PlEmu::PlMessage>& other);
				IEnumerator_1<UPlasma::PlEmu::PlMessage>(IEnumerator_1<UPlasma::PlEmu::PlMessage>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlMessage>();
				IEnumerator_1<UPlasma::PlEmu::PlMessage>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlMessage>& other);
				IEnumerator_1<UPlasma::PlEmu::PlMessage>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlMessage>& operator=(IEnumerator_1<UPlasma::PlEmu::PlMessage>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlMessage>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlMessage>& other) const;
				UPlasma::PlEmu::PlMessage GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>(const IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>(IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>();
				IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				UPlasma::PlEmu::PlOneShotCallback GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>(const IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>(IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>();
				IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				UPlasma::PlEmu::PlRandomSoundModGroup GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlPage> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlPage>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlPage>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlPage>(const IEnumerator_1<UPlasma::PlEmu::PlPage>& other);
				IEnumerator_1<UPlasma::PlEmu::PlPage>(IEnumerator_1<UPlasma::PlEmu::PlPage>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlPage>();
				IEnumerator_1<UPlasma::PlEmu::PlPage>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlPage>& other);
				IEnumerator_1<UPlasma::PlEmu::PlPage>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlPage>& operator=(IEnumerator_1<UPlasma::PlEmu::PlPage>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlPage>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlPage>& other) const;
				UPlasma::PlEmu::PlPage GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PrpImportClues> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PrpImportClues>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PrpImportClues>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PrpImportClues>(const IEnumerator_1<UPlasma::PlEmu::PrpImportClues>& other);
				IEnumerator_1<UPlasma::PlEmu::PrpImportClues>(IEnumerator_1<UPlasma::PlEmu::PrpImportClues>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PrpImportClues>();
				IEnumerator_1<UPlasma::PlEmu::PrpImportClues>& operator=(const IEnumerator_1<UPlasma::PlEmu::PrpImportClues>& other);
				IEnumerator_1<UPlasma::PlEmu::PrpImportClues>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PrpImportClues>& operator=(IEnumerator_1<UPlasma::PlEmu::PrpImportClues>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				UPlasma::PlEmu::PrpImportClues GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlCluster> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlCluster>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlCluster>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlCluster>(const IEnumerator_1<UPlasma::PlEmu::PlCluster>& other);
				IEnumerator_1<UPlasma::PlEmu::PlCluster>(IEnumerator_1<UPlasma::PlEmu::PlCluster>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlCluster>();
				IEnumerator_1<UPlasma::PlEmu::PlCluster>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlCluster>& other);
				IEnumerator_1<UPlasma::PlEmu::PlCluster>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlCluster>& operator=(IEnumerator_1<UPlasma::PlEmu::PlCluster>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlCluster>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlCluster>& other) const;
				UPlasma::PlEmu::PlCluster GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::AgAnim> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::AgAnim>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::AgAnim>(const IEnumerator_1<UPlasma::PlEmu::AgAnim>& other);
				IEnumerator_1<UPlasma::PlEmu::AgAnim>(IEnumerator_1<UPlasma::PlEmu::AgAnim>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::AgAnim>();
				IEnumerator_1<UPlasma::PlEmu::AgAnim>& operator=(const IEnumerator_1<UPlasma::PlEmu::AgAnim>& other);
				IEnumerator_1<UPlasma::PlEmu::AgAnim>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::AgAnim>& operator=(IEnumerator_1<UPlasma::PlEmu::AgAnim>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::AgAnim>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::AgAnim>& other) const;
				UPlasma::PlEmu::AgAnim GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Color> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Color>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Color>(const IEnumerator_1<UnityEngine::Color>& other);
				IEnumerator_1<UnityEngine::Color>(IEnumerator_1<UnityEngine::Color>&& other);
				virtual ~IEnumerator_1<UnityEngine::Color>();
				IEnumerator_1<UnityEngine::Color>& operator=(const IEnumerator_1<UnityEngine::Color>& other);
				IEnumerator_1<UnityEngine::Color>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Color>& operator=(IEnumerator_1<UnityEngine::Color>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Color>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Color>& other) const;
				UnityEngine::Color GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Color32> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Color32>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Color32>(const IEnumerator_1<UnityEngine::Color32>& other);
				IEnumerator_1<UnityEngine::Color32>(IEnumerator_1<UnityEngine::Color32>&& other);
				virtual ~IEnumerator_1<UnityEngine::Color32>();
				IEnumerator_1<UnityEngine::Color32>& operator=(const IEnumerator_1<UnityEngine::Color32>& other);
				IEnumerator_1<UnityEngine::Color32>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Color32>& operator=(IEnumerator_1<UnityEngine::Color32>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Color32>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Color32>& other) const;
				UnityEngine::Color32 GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Vector2> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Vector2>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Vector2>(const IEnumerator_1<UnityEngine::Vector2>& other);
				IEnumerator_1<UnityEngine::Vector2>(IEnumerator_1<UnityEngine::Vector2>&& other);
				virtual ~IEnumerator_1<UnityEngine::Vector2>();
				IEnumerator_1<UnityEngine::Vector2>& operator=(const IEnumerator_1<UnityEngine::Vector2>& other);
				IEnumerator_1<UnityEngine::Vector2>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Vector2>& operator=(IEnumerator_1<UnityEngine::Vector2>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Vector2>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Vector2>& other) const;
				UnityEngine::Vector2 GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Vector3> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Vector3>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Vector3>(const IEnumerator_1<UnityEngine::Vector3>& other);
				IEnumerator_1<UnityEngine::Vector3>(IEnumerator_1<UnityEngine::Vector3>&& other);
				virtual ~IEnumerator_1<UnityEngine::Vector3>();
				IEnumerator_1<UnityEngine::Vector3>& operator=(const IEnumerator_1<UnityEngine::Vector3>& other);
				IEnumerator_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Vector3>& operator=(IEnumerator_1<UnityEngine::Vector3>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Vector3>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Vector3>& other) const;
				UnityEngine::Vector3 GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Vector4> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Vector4>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Vector4>(const IEnumerator_1<UnityEngine::Vector4>& other);
				IEnumerator_1<UnityEngine::Vector4>(IEnumerator_1<UnityEngine::Vector4>&& other);
				virtual ~IEnumerator_1<UnityEngine::Vector4>();
				IEnumerator_1<UnityEngine::Vector4>& operator=(const IEnumerator_1<UnityEngine::Vector4>& other);
				IEnumerator_1<UnityEngine::Vector4>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Vector4>& operator=(IEnumerator_1<UnityEngine::Vector4>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Vector4>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Vector4>& other) const;
				UnityEngine::Vector4 GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::BoneWeight> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::BoneWeight>(decltype(nullptr));
				IEnumerator_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::BoneWeight>(const IEnumerator_1<UnityEngine::BoneWeight>& other);
				IEnumerator_1<UnityEngine::BoneWeight>(IEnumerator_1<UnityEngine::BoneWeight>&& other);
				virtual ~IEnumerator_1<UnityEngine::BoneWeight>();
				IEnumerator_1<UnityEngine::BoneWeight>& operator=(const IEnumerator_1<UnityEngine::BoneWeight>& other);
				IEnumerator_1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::BoneWeight>& operator=(IEnumerator_1<UnityEngine::BoneWeight>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::BoneWeight>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::BoneWeight>& other) const;
				UnityEngine::BoneWeight GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UnityEngine::Quaternion> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UnityEngine::Quaternion>(decltype(nullptr));
				IEnumerator_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UnityEngine::Quaternion>(const IEnumerator_1<UnityEngine::Quaternion>& other);
				IEnumerator_1<UnityEngine::Quaternion>(IEnumerator_1<UnityEngine::Quaternion>&& other);
				virtual ~IEnumerator_1<UnityEngine::Quaternion>();
				IEnumerator_1<UnityEngine::Quaternion>& operator=(const IEnumerator_1<UnityEngine::Quaternion>& other);
				IEnumerator_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
				IEnumerator_1<UnityEngine::Quaternion>& operator=(IEnumerator_1<UnityEngine::Quaternion>&& other);
				bool operator==(const IEnumerator_1<UnityEngine::Quaternion>& other) const;
				bool operator!=(const IEnumerator_1<UnityEngine::Quaternion>& other) const;
				UnityEngine::Quaternion GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlResponderState> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlResponderState>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlResponderState>(const IEnumerator_1<UPlasma::PlEmu::PlResponderState>& other);
				IEnumerator_1<UPlasma::PlEmu::PlResponderState>(IEnumerator_1<UPlasma::PlEmu::PlResponderState>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlResponderState>();
				IEnumerator_1<UPlasma::PlEmu::PlResponderState>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlResponderState>& other);
				IEnumerator_1<UPlasma::PlEmu::PlResponderState>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlResponderState>& operator=(IEnumerator_1<UPlasma::PlEmu::PlResponderState>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlResponderState>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlResponderState>& other) const;
				UPlasma::PlEmu::PlResponderState GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlResponderCommand> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>(const IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>(IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>();
				IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>& operator=(IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				UPlasma::PlEmu::PlResponderCommand GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::PlSound> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::PlSound>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::PlSound>(const IEnumerator_1<UPlasma::PlEmu::PlSound>& other);
				IEnumerator_1<UPlasma::PlEmu::PlSound>(IEnumerator_1<UPlasma::PlEmu::PlSound>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::PlSound>();
				IEnumerator_1<UPlasma::PlEmu::PlSound>& operator=(const IEnumerator_1<UPlasma::PlEmu::PlSound>& other);
				IEnumerator_1<UPlasma::PlEmu::PlSound>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::PlSound>& operator=(IEnumerator_1<UPlasma::PlEmu::PlSound>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::PlSound>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::PlSound>& other) const;
				UPlasma::PlEmu::PlSound GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<UPlasma::PlEmu::IAgApplicator> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<UPlasma::PlEmu::IAgApplicator>(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<UPlasma::PlEmu::IAgApplicator>(const IEnumerator_1<UPlasma::PlEmu::IAgApplicator>& other);
				IEnumerator_1<UPlasma::PlEmu::IAgApplicator>(IEnumerator_1<UPlasma::PlEmu::IAgApplicator>&& other);
				virtual ~IEnumerator_1<UPlasma::PlEmu::IAgApplicator>();
				IEnumerator_1<UPlasma::PlEmu::IAgApplicator>& operator=(const IEnumerator_1<UPlasma::PlEmu::IAgApplicator>& other);
				IEnumerator_1<UPlasma::PlEmu::IAgApplicator>& operator=(decltype(nullptr));
				IEnumerator_1<UPlasma::PlEmu::IAgApplicator>& operator=(IEnumerator_1<UPlasma::PlEmu::IAgApplicator>&& other);
				bool operator==(const IEnumerator_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				bool operator!=(const IEnumerator_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				UPlasma::PlEmu::IAgApplicator GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Single> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<System::Single>(decltype(nullptr));
				IEnumerator_1<System::Single>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<System::Single>(const IEnumerator_1<System::Single>& other);
				IEnumerator_1<System::Single>(IEnumerator_1<System::Single>&& other);
				virtual ~IEnumerator_1<System::Single>();
				IEnumerator_1<System::Single>& operator=(const IEnumerator_1<System::Single>& other);
				IEnumerator_1<System::Single>& operator=(decltype(nullptr));
				IEnumerator_1<System::Single>& operator=(IEnumerator_1<System::Single>&& other);
				bool operator==(const IEnumerator_1<System::Single>& other) const;
				bool operator!=(const IEnumerator_1<System::Single>& other) const;
				System::Single GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Int32> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<System::Int32>(decltype(nullptr));
				IEnumerator_1<System::Int32>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<System::Int32>(const IEnumerator_1<System::Int32>& other);
				IEnumerator_1<System::Int32>(IEnumerator_1<System::Int32>&& other);
				virtual ~IEnumerator_1<System::Int32>();
				IEnumerator_1<System::Int32>& operator=(const IEnumerator_1<System::Int32>& other);
				IEnumerator_1<System::Int32>& operator=(decltype(nullptr));
				IEnumerator_1<System::Int32>& operator=(IEnumerator_1<System::Int32>&& other);
				bool operator==(const IEnumerator_1<System::Int32>& other) const;
				bool operator!=(const IEnumerator_1<System::Int32>& other) const;
				System::Int32 GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Object> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<System::Object>(decltype(nullptr));
				IEnumerator_1<System::Object>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<System::Object>(const IEnumerator_1<System::Object>& other);
				IEnumerator_1<System::Object>(IEnumerator_1<System::Object>&& other);
				virtual ~IEnumerator_1<System::Object>();
				IEnumerator_1<System::Object>& operator=(const IEnumerator_1<System::Object>& other);
				IEnumerator_1<System::Object>& operator=(decltype(nullptr));
				IEnumerator_1<System::Object>& operator=(IEnumerator_1<System::Object>&& other);
				bool operator==(const IEnumerator_1<System::Object>& other) const;
				bool operator!=(const IEnumerator_1<System::Object>& other) const;
				System::Object GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::Byte> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<System::Byte>(decltype(nullptr));
				IEnumerator_1<System::Byte>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<System::Byte>(const IEnumerator_1<System::Byte>& other);
				IEnumerator_1<System::Byte>(IEnumerator_1<System::Byte>&& other);
				virtual ~IEnumerator_1<System::Byte>();
				IEnumerator_1<System::Byte>& operator=(const IEnumerator_1<System::Byte>& other);
				IEnumerator_1<System::Byte>& operator=(decltype(nullptr));
				IEnumerator_1<System::Byte>& operator=(IEnumerator_1<System::Byte>&& other);
				bool operator==(const IEnumerator_1<System::Byte>& other) const;
				bool operator!=(const IEnumerator_1<System::Byte>& other) const;
				System::Byte GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerator_1<System::String> : virtual System::IDisposable, virtual System::Collections::IEnumerator
			{
				IEnumerator_1<System::String>(decltype(nullptr));
				IEnumerator_1<System::String>(Plugin::InternalUse, int32_t handle);
				IEnumerator_1<System::String>(const IEnumerator_1<System::String>& other);
				IEnumerator_1<System::String>(IEnumerator_1<System::String>&& other);
				virtual ~IEnumerator_1<System::String>();
				IEnumerator_1<System::String>& operator=(const IEnumerator_1<System::String>& other);
				IEnumerator_1<System::String>& operator=(decltype(nullptr));
				IEnumerator_1<System::String>& operator=(IEnumerator_1<System::String>&& other);
				bool operator==(const IEnumerator_1<System::String>& other) const;
				bool operator!=(const IEnumerator_1<System::String>& other) const;
				System::String GetCurrent();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Object> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Object>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Object>(const IEnumerable_1<UnityEngine::Object>& other);
				IEnumerable_1<UnityEngine::Object>(IEnumerable_1<UnityEngine::Object>&& other);
				virtual ~IEnumerable_1<UnityEngine::Object>();
				IEnumerable_1<UnityEngine::Object>& operator=(const IEnumerable_1<UnityEngine::Object>& other);
				IEnumerable_1<UnityEngine::Object>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Object>& operator=(IEnumerable_1<UnityEngine::Object>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Object>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Object>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Object> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::GameObject> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::GameObject>(decltype(nullptr));
				IEnumerable_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::GameObject>(const IEnumerable_1<UnityEngine::GameObject>& other);
				IEnumerable_1<UnityEngine::GameObject>(IEnumerable_1<UnityEngine::GameObject>&& other);
				virtual ~IEnumerable_1<UnityEngine::GameObject>();
				IEnumerable_1<UnityEngine::GameObject>& operator=(const IEnumerable_1<UnityEngine::GameObject>& other);
				IEnumerable_1<UnityEngine::GameObject>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::GameObject>& operator=(IEnumerable_1<UnityEngine::GameObject>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::GameObject>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::GameObject>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::GameObject> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Transform> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Transform>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Transform>(const IEnumerable_1<UnityEngine::Transform>& other);
				IEnumerable_1<UnityEngine::Transform>(IEnumerable_1<UnityEngine::Transform>&& other);
				virtual ~IEnumerable_1<UnityEngine::Transform>();
				IEnumerable_1<UnityEngine::Transform>& operator=(const IEnumerable_1<UnityEngine::Transform>& other);
				IEnumerable_1<UnityEngine::Transform>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Transform>& operator=(IEnumerable_1<UnityEngine::Transform>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Transform>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Transform>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Transform> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::MeshFilter> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::MeshFilter>(decltype(nullptr));
				IEnumerable_1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::MeshFilter>(const IEnumerable_1<UnityEngine::MeshFilter>& other);
				IEnumerable_1<UnityEngine::MeshFilter>(IEnumerable_1<UnityEngine::MeshFilter>&& other);
				virtual ~IEnumerable_1<UnityEngine::MeshFilter>();
				IEnumerable_1<UnityEngine::MeshFilter>& operator=(const IEnumerable_1<UnityEngine::MeshFilter>& other);
				IEnumerable_1<UnityEngine::MeshFilter>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::MeshFilter>& operator=(IEnumerable_1<UnityEngine::MeshFilter>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::MeshFilter>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::MeshFilter>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::MeshFilter> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Material> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Material>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Material>(const IEnumerable_1<UnityEngine::Material>& other);
				IEnumerable_1<UnityEngine::Material>(IEnumerable_1<UnityEngine::Material>&& other);
				virtual ~IEnumerable_1<UnityEngine::Material>();
				IEnumerable_1<UnityEngine::Material>& operator=(const IEnumerable_1<UnityEngine::Material>& other);
				IEnumerable_1<UnityEngine::Material>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Material>& operator=(IEnumerable_1<UnityEngine::Material>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Material>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Material>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Material> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Texture2D> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Texture2D>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Texture2D>(const IEnumerable_1<UnityEngine::Texture2D>& other);
				IEnumerable_1<UnityEngine::Texture2D>(IEnumerable_1<UnityEngine::Texture2D>&& other);
				virtual ~IEnumerable_1<UnityEngine::Texture2D>();
				IEnumerable_1<UnityEngine::Texture2D>& operator=(const IEnumerable_1<UnityEngine::Texture2D>& other);
				IEnumerable_1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Texture2D>& operator=(IEnumerable_1<UnityEngine::Texture2D>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Texture2D>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Texture2D>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Texture2D> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Cubemap> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Cubemap>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Cubemap>(const IEnumerable_1<UnityEngine::Cubemap>& other);
				IEnumerable_1<UnityEngine::Cubemap>(IEnumerable_1<UnityEngine::Cubemap>&& other);
				virtual ~IEnumerable_1<UnityEngine::Cubemap>();
				IEnumerable_1<UnityEngine::Cubemap>& operator=(const IEnumerable_1<UnityEngine::Cubemap>& other);
				IEnumerable_1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Cubemap>& operator=(IEnumerable_1<UnityEngine::Cubemap>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Cubemap>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Cubemap>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Cubemap> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Matrix4x4> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Matrix4x4>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Matrix4x4>(const IEnumerable_1<UnityEngine::Matrix4x4>& other);
				IEnumerable_1<UnityEngine::Matrix4x4>(IEnumerable_1<UnityEngine::Matrix4x4>&& other);
				virtual ~IEnumerable_1<UnityEngine::Matrix4x4>();
				IEnumerable_1<UnityEngine::Matrix4x4>& operator=(const IEnumerable_1<UnityEngine::Matrix4x4>& other);
				IEnumerable_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Matrix4x4>& operator=(IEnumerable_1<UnityEngine::Matrix4x4>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Matrix4x4>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Matrix4x4>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Matrix4x4> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::IPlMessageable> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::IPlMessageable>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::IPlMessageable>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::IPlMessageable>(const IEnumerable_1<UPlasma::PlEmu::IPlMessageable>& other);
				IEnumerable_1<UPlasma::PlEmu::IPlMessageable>(IEnumerable_1<UPlasma::PlEmu::IPlMessageable>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::IPlMessageable>();
				IEnumerable_1<UPlasma::PlEmu::IPlMessageable>& operator=(const IEnumerable_1<UPlasma::PlEmu::IPlMessageable>& other);
				IEnumerable_1<UPlasma::PlEmu::IPlMessageable>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::IPlMessageable>& operator=(IEnumerable_1<UPlasma::PlEmu::IPlMessageable>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IPlMessageable> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlConditionalObject> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>(const IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>(IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>();
				IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>& operator=(IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlConditionalObject> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlDetector> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlDetector>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlDetector>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlDetector>(const IEnumerable_1<UPlasma::PlEmu::PlDetector>& other);
				IEnumerable_1<UPlasma::PlEmu::PlDetector>(IEnumerable_1<UPlasma::PlEmu::PlDetector>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlDetector>();
				IEnumerable_1<UPlasma::PlEmu::PlDetector>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlDetector>& other);
				IEnumerable_1<UPlasma::PlEmu::PlDetector>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlDetector>& operator=(IEnumerable_1<UPlasma::PlEmu::PlDetector>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlDetector>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlDetector>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlDetector> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlMessage> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlMessage>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlMessage>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlMessage>(const IEnumerable_1<UPlasma::PlEmu::PlMessage>& other);
				IEnumerable_1<UPlasma::PlEmu::PlMessage>(IEnumerable_1<UPlasma::PlEmu::PlMessage>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlMessage>();
				IEnumerable_1<UPlasma::PlEmu::PlMessage>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlMessage>& other);
				IEnumerable_1<UPlasma::PlEmu::PlMessage>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlMessage>& operator=(IEnumerable_1<UPlasma::PlEmu::PlMessage>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlMessage>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlMessage>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlMessage> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>(const IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>(IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>();
				IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>(const IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>(IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>();
				IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlPage> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlPage>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlPage>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlPage>(const IEnumerable_1<UPlasma::PlEmu::PlPage>& other);
				IEnumerable_1<UPlasma::PlEmu::PlPage>(IEnumerable_1<UPlasma::PlEmu::PlPage>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlPage>();
				IEnumerable_1<UPlasma::PlEmu::PlPage>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlPage>& other);
				IEnumerable_1<UPlasma::PlEmu::PlPage>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlPage>& operator=(IEnumerable_1<UPlasma::PlEmu::PlPage>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlPage>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlPage>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlPage> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PrpImportClues> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PrpImportClues>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PrpImportClues>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PrpImportClues>(const IEnumerable_1<UPlasma::PlEmu::PrpImportClues>& other);
				IEnumerable_1<UPlasma::PlEmu::PrpImportClues>(IEnumerable_1<UPlasma::PlEmu::PrpImportClues>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PrpImportClues>();
				IEnumerable_1<UPlasma::PlEmu::PrpImportClues>& operator=(const IEnumerable_1<UPlasma::PlEmu::PrpImportClues>& other);
				IEnumerable_1<UPlasma::PlEmu::PrpImportClues>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PrpImportClues>& operator=(IEnumerable_1<UPlasma::PlEmu::PrpImportClues>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PrpImportClues> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlCluster> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlCluster>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlCluster>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlCluster>(const IEnumerable_1<UPlasma::PlEmu::PlCluster>& other);
				IEnumerable_1<UPlasma::PlEmu::PlCluster>(IEnumerable_1<UPlasma::PlEmu::PlCluster>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlCluster>();
				IEnumerable_1<UPlasma::PlEmu::PlCluster>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlCluster>& other);
				IEnumerable_1<UPlasma::PlEmu::PlCluster>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlCluster>& operator=(IEnumerable_1<UPlasma::PlEmu::PlCluster>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlCluster>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlCluster>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlCluster> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::AgAnim> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::AgAnim>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::AgAnim>(const IEnumerable_1<UPlasma::PlEmu::AgAnim>& other);
				IEnumerable_1<UPlasma::PlEmu::AgAnim>(IEnumerable_1<UPlasma::PlEmu::AgAnim>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::AgAnim>();
				IEnumerable_1<UPlasma::PlEmu::AgAnim>& operator=(const IEnumerable_1<UPlasma::PlEmu::AgAnim>& other);
				IEnumerable_1<UPlasma::PlEmu::AgAnim>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::AgAnim>& operator=(IEnumerable_1<UPlasma::PlEmu::AgAnim>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::AgAnim>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::AgAnim>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::AgAnim> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Color> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Color>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Color>(const IEnumerable_1<UnityEngine::Color>& other);
				IEnumerable_1<UnityEngine::Color>(IEnumerable_1<UnityEngine::Color>&& other);
				virtual ~IEnumerable_1<UnityEngine::Color>();
				IEnumerable_1<UnityEngine::Color>& operator=(const IEnumerable_1<UnityEngine::Color>& other);
				IEnumerable_1<UnityEngine::Color>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Color>& operator=(IEnumerable_1<UnityEngine::Color>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Color>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Color>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Color> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Color32> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Color32>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Color32>(const IEnumerable_1<UnityEngine::Color32>& other);
				IEnumerable_1<UnityEngine::Color32>(IEnumerable_1<UnityEngine::Color32>&& other);
				virtual ~IEnumerable_1<UnityEngine::Color32>();
				IEnumerable_1<UnityEngine::Color32>& operator=(const IEnumerable_1<UnityEngine::Color32>& other);
				IEnumerable_1<UnityEngine::Color32>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Color32>& operator=(IEnumerable_1<UnityEngine::Color32>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Color32>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Color32>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Color32> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Vector2> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Vector2>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Vector2>(const IEnumerable_1<UnityEngine::Vector2>& other);
				IEnumerable_1<UnityEngine::Vector2>(IEnumerable_1<UnityEngine::Vector2>&& other);
				virtual ~IEnumerable_1<UnityEngine::Vector2>();
				IEnumerable_1<UnityEngine::Vector2>& operator=(const IEnumerable_1<UnityEngine::Vector2>& other);
				IEnumerable_1<UnityEngine::Vector2>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Vector2>& operator=(IEnumerable_1<UnityEngine::Vector2>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Vector2>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Vector2>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Vector2> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Vector3> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Vector3>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Vector3>(const IEnumerable_1<UnityEngine::Vector3>& other);
				IEnumerable_1<UnityEngine::Vector3>(IEnumerable_1<UnityEngine::Vector3>&& other);
				virtual ~IEnumerable_1<UnityEngine::Vector3>();
				IEnumerable_1<UnityEngine::Vector3>& operator=(const IEnumerable_1<UnityEngine::Vector3>& other);
				IEnumerable_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Vector3>& operator=(IEnumerable_1<UnityEngine::Vector3>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Vector3>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Vector3>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Vector3> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Vector4> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Vector4>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Vector4>(const IEnumerable_1<UnityEngine::Vector4>& other);
				IEnumerable_1<UnityEngine::Vector4>(IEnumerable_1<UnityEngine::Vector4>&& other);
				virtual ~IEnumerable_1<UnityEngine::Vector4>();
				IEnumerable_1<UnityEngine::Vector4>& operator=(const IEnumerable_1<UnityEngine::Vector4>& other);
				IEnumerable_1<UnityEngine::Vector4>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Vector4>& operator=(IEnumerable_1<UnityEngine::Vector4>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Vector4>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Vector4>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Vector4> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::BoneWeight> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::BoneWeight>(decltype(nullptr));
				IEnumerable_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::BoneWeight>(const IEnumerable_1<UnityEngine::BoneWeight>& other);
				IEnumerable_1<UnityEngine::BoneWeight>(IEnumerable_1<UnityEngine::BoneWeight>&& other);
				virtual ~IEnumerable_1<UnityEngine::BoneWeight>();
				IEnumerable_1<UnityEngine::BoneWeight>& operator=(const IEnumerable_1<UnityEngine::BoneWeight>& other);
				IEnumerable_1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::BoneWeight>& operator=(IEnumerable_1<UnityEngine::BoneWeight>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::BoneWeight>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::BoneWeight>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::BoneWeight> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UnityEngine::Quaternion> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UnityEngine::Quaternion>(decltype(nullptr));
				IEnumerable_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UnityEngine::Quaternion>(const IEnumerable_1<UnityEngine::Quaternion>& other);
				IEnumerable_1<UnityEngine::Quaternion>(IEnumerable_1<UnityEngine::Quaternion>&& other);
				virtual ~IEnumerable_1<UnityEngine::Quaternion>();
				IEnumerable_1<UnityEngine::Quaternion>& operator=(const IEnumerable_1<UnityEngine::Quaternion>& other);
				IEnumerable_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
				IEnumerable_1<UnityEngine::Quaternion>& operator=(IEnumerable_1<UnityEngine::Quaternion>&& other);
				bool operator==(const IEnumerable_1<UnityEngine::Quaternion>& other) const;
				bool operator!=(const IEnumerable_1<UnityEngine::Quaternion>& other) const;
				System::Collections::Generic::IEnumerator_1<UnityEngine::Quaternion> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlResponderState> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlResponderState>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlResponderState>(const IEnumerable_1<UPlasma::PlEmu::PlResponderState>& other);
				IEnumerable_1<UPlasma::PlEmu::PlResponderState>(IEnumerable_1<UPlasma::PlEmu::PlResponderState>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlResponderState>();
				IEnumerable_1<UPlasma::PlEmu::PlResponderState>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlResponderState>& other);
				IEnumerable_1<UPlasma::PlEmu::PlResponderState>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlResponderState>& operator=(IEnumerable_1<UPlasma::PlEmu::PlResponderState>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlResponderState>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlResponderState>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderState> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlResponderCommand> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>(const IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>(IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>();
				IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>& operator=(IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderCommand> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::PlSound> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::PlSound>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::PlSound>(const IEnumerable_1<UPlasma::PlEmu::PlSound>& other);
				IEnumerable_1<UPlasma::PlEmu::PlSound>(IEnumerable_1<UPlasma::PlEmu::PlSound>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::PlSound>();
				IEnumerable_1<UPlasma::PlEmu::PlSound>& operator=(const IEnumerable_1<UPlasma::PlEmu::PlSound>& other);
				IEnumerable_1<UPlasma::PlEmu::PlSound>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::PlSound>& operator=(IEnumerable_1<UPlasma::PlEmu::PlSound>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::PlSound>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::PlSound>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlSound> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<UPlasma::PlEmu::IAgApplicator> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<UPlasma::PlEmu::IAgApplicator>(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<UPlasma::PlEmu::IAgApplicator>(const IEnumerable_1<UPlasma::PlEmu::IAgApplicator>& other);
				IEnumerable_1<UPlasma::PlEmu::IAgApplicator>(IEnumerable_1<UPlasma::PlEmu::IAgApplicator>&& other);
				virtual ~IEnumerable_1<UPlasma::PlEmu::IAgApplicator>();
				IEnumerable_1<UPlasma::PlEmu::IAgApplicator>& operator=(const IEnumerable_1<UPlasma::PlEmu::IAgApplicator>& other);
				IEnumerable_1<UPlasma::PlEmu::IAgApplicator>& operator=(decltype(nullptr));
				IEnumerable_1<UPlasma::PlEmu::IAgApplicator>& operator=(IEnumerable_1<UPlasma::PlEmu::IAgApplicator>&& other);
				bool operator==(const IEnumerable_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				bool operator!=(const IEnumerable_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IAgApplicator> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Single> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<System::Single>(decltype(nullptr));
				IEnumerable_1<System::Single>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<System::Single>(const IEnumerable_1<System::Single>& other);
				IEnumerable_1<System::Single>(IEnumerable_1<System::Single>&& other);
				virtual ~IEnumerable_1<System::Single>();
				IEnumerable_1<System::Single>& operator=(const IEnumerable_1<System::Single>& other);
				IEnumerable_1<System::Single>& operator=(decltype(nullptr));
				IEnumerable_1<System::Single>& operator=(IEnumerable_1<System::Single>&& other);
				bool operator==(const IEnumerable_1<System::Single>& other) const;
				bool operator!=(const IEnumerable_1<System::Single>& other) const;
				System::Collections::Generic::IEnumerator_1<System::Single> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Int32> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<System::Int32>(decltype(nullptr));
				IEnumerable_1<System::Int32>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<System::Int32>(const IEnumerable_1<System::Int32>& other);
				IEnumerable_1<System::Int32>(IEnumerable_1<System::Int32>&& other);
				virtual ~IEnumerable_1<System::Int32>();
				IEnumerable_1<System::Int32>& operator=(const IEnumerable_1<System::Int32>& other);
				IEnumerable_1<System::Int32>& operator=(decltype(nullptr));
				IEnumerable_1<System::Int32>& operator=(IEnumerable_1<System::Int32>&& other);
				bool operator==(const IEnumerable_1<System::Int32>& other) const;
				bool operator!=(const IEnumerable_1<System::Int32>& other) const;
				System::Collections::Generic::IEnumerator_1<System::Int32> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Object> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<System::Object>(decltype(nullptr));
				IEnumerable_1<System::Object>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<System::Object>(const IEnumerable_1<System::Object>& other);
				IEnumerable_1<System::Object>(IEnumerable_1<System::Object>&& other);
				virtual ~IEnumerable_1<System::Object>();
				IEnumerable_1<System::Object>& operator=(const IEnumerable_1<System::Object>& other);
				IEnumerable_1<System::Object>& operator=(decltype(nullptr));
				IEnumerable_1<System::Object>& operator=(IEnumerable_1<System::Object>&& other);
				bool operator==(const IEnumerable_1<System::Object>& other) const;
				bool operator!=(const IEnumerable_1<System::Object>& other) const;
				System::Collections::Generic::IEnumerator_1<System::Object> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::Byte> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<System::Byte>(decltype(nullptr));
				IEnumerable_1<System::Byte>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<System::Byte>(const IEnumerable_1<System::Byte>& other);
				IEnumerable_1<System::Byte>(IEnumerable_1<System::Byte>&& other);
				virtual ~IEnumerable_1<System::Byte>();
				IEnumerable_1<System::Byte>& operator=(const IEnumerable_1<System::Byte>& other);
				IEnumerable_1<System::Byte>& operator=(decltype(nullptr));
				IEnumerable_1<System::Byte>& operator=(IEnumerable_1<System::Byte>&& other);
				bool operator==(const IEnumerable_1<System::Byte>& other) const;
				bool operator!=(const IEnumerable_1<System::Byte>& other) const;
				System::Collections::Generic::IEnumerator_1<System::Byte> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IEnumerable_1<System::String> : virtual System::Collections::IEnumerable
			{
				IEnumerable_1<System::String>(decltype(nullptr));
				IEnumerable_1<System::String>(Plugin::InternalUse, int32_t handle);
				IEnumerable_1<System::String>(const IEnumerable_1<System::String>& other);
				IEnumerable_1<System::String>(IEnumerable_1<System::String>&& other);
				virtual ~IEnumerable_1<System::String>();
				IEnumerable_1<System::String>& operator=(const IEnumerable_1<System::String>& other);
				IEnumerable_1<System::String>& operator=(decltype(nullptr));
				IEnumerable_1<System::String>& operator=(IEnumerable_1<System::String>&& other);
				bool operator==(const IEnumerable_1<System::String>& other) const;
				bool operator!=(const IEnumerable_1<System::String>& other) const;
				System::Collections::Generic::IEnumerator_1<System::String> GetEnumerator();
			};
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Object> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Object>
			{
				ICollection_1<UnityEngine::Object>(decltype(nullptr));
				ICollection_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Object>(const ICollection_1<UnityEngine::Object>& other);
				ICollection_1<UnityEngine::Object>(ICollection_1<UnityEngine::Object>&& other);
				virtual ~ICollection_1<UnityEngine::Object>();
				ICollection_1<UnityEngine::Object>& operator=(const ICollection_1<UnityEngine::Object>& other);
				ICollection_1<UnityEngine::Object>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Object>& operator=(ICollection_1<UnityEngine::Object>&& other);
				bool operator==(const ICollection_1<UnityEngine::Object>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineObjectIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineObjectIterator(System::Collections::Generic::ICollection_1<UnityEngine::Object>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineObjectIterator();
		SystemCollectionsGenericICollectionUnityEngineObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineObjectIterator& other);
		UnityEngine::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineObjectIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Object>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineObjectIterator end(System::Collections::Generic::ICollection_1<UnityEngine::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::GameObject> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::GameObject>
			{
				ICollection_1<UnityEngine::GameObject>(decltype(nullptr));
				ICollection_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::GameObject>(const ICollection_1<UnityEngine::GameObject>& other);
				ICollection_1<UnityEngine::GameObject>(ICollection_1<UnityEngine::GameObject>&& other);
				virtual ~ICollection_1<UnityEngine::GameObject>();
				ICollection_1<UnityEngine::GameObject>& operator=(const ICollection_1<UnityEngine::GameObject>& other);
				ICollection_1<UnityEngine::GameObject>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::GameObject>& operator=(ICollection_1<UnityEngine::GameObject>&& other);
				bool operator==(const ICollection_1<UnityEngine::GameObject>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::GameObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineGameObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::GameObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineGameObjectIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineGameObjectIterator(System::Collections::Generic::ICollection_1<UnityEngine::GameObject>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineGameObjectIterator();
		SystemCollectionsGenericICollectionUnityEngineGameObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineGameObjectIterator& other);
		UnityEngine::GameObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineGameObjectIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::GameObject>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineGameObjectIterator end(System::Collections::Generic::ICollection_1<UnityEngine::GameObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Transform> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Transform>
			{
				ICollection_1<UnityEngine::Transform>(decltype(nullptr));
				ICollection_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Transform>(const ICollection_1<UnityEngine::Transform>& other);
				ICollection_1<UnityEngine::Transform>(ICollection_1<UnityEngine::Transform>&& other);
				virtual ~ICollection_1<UnityEngine::Transform>();
				ICollection_1<UnityEngine::Transform>& operator=(const ICollection_1<UnityEngine::Transform>& other);
				ICollection_1<UnityEngine::Transform>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Transform>& operator=(ICollection_1<UnityEngine::Transform>&& other);
				bool operator==(const ICollection_1<UnityEngine::Transform>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Transform>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineTransformIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Transform> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineTransformIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineTransformIterator(System::Collections::Generic::ICollection_1<UnityEngine::Transform>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineTransformIterator();
		SystemCollectionsGenericICollectionUnityEngineTransformIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineTransformIterator& other);
		UnityEngine::Transform operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineTransformIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Transform>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineTransformIterator end(System::Collections::Generic::ICollection_1<UnityEngine::Transform>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::MeshFilter> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::MeshFilter>
			{
				ICollection_1<UnityEngine::MeshFilter>(decltype(nullptr));
				ICollection_1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::MeshFilter>(const ICollection_1<UnityEngine::MeshFilter>& other);
				ICollection_1<UnityEngine::MeshFilter>(ICollection_1<UnityEngine::MeshFilter>&& other);
				virtual ~ICollection_1<UnityEngine::MeshFilter>();
				ICollection_1<UnityEngine::MeshFilter>& operator=(const ICollection_1<UnityEngine::MeshFilter>& other);
				ICollection_1<UnityEngine::MeshFilter>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::MeshFilter>& operator=(ICollection_1<UnityEngine::MeshFilter>&& other);
				bool operator==(const ICollection_1<UnityEngine::MeshFilter>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::MeshFilter>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::MeshFilter> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator(System::Collections::Generic::ICollection_1<UnityEngine::MeshFilter>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator();
		SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator& other);
		UnityEngine::MeshFilter operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::MeshFilter>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineMeshFilterIterator end(System::Collections::Generic::ICollection_1<UnityEngine::MeshFilter>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Material> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Material>
			{
				ICollection_1<UnityEngine::Material>(decltype(nullptr));
				ICollection_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Material>(const ICollection_1<UnityEngine::Material>& other);
				ICollection_1<UnityEngine::Material>(ICollection_1<UnityEngine::Material>&& other);
				virtual ~ICollection_1<UnityEngine::Material>();
				ICollection_1<UnityEngine::Material>& operator=(const ICollection_1<UnityEngine::Material>& other);
				ICollection_1<UnityEngine::Material>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Material>& operator=(ICollection_1<UnityEngine::Material>&& other);
				bool operator==(const ICollection_1<UnityEngine::Material>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Material>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineMaterialIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Material> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineMaterialIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineMaterialIterator(System::Collections::Generic::ICollection_1<UnityEngine::Material>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineMaterialIterator();
		SystemCollectionsGenericICollectionUnityEngineMaterialIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineMaterialIterator& other);
		UnityEngine::Material operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineMaterialIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Material>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineMaterialIterator end(System::Collections::Generic::ICollection_1<UnityEngine::Material>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Texture2D> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Texture2D>
			{
				ICollection_1<UnityEngine::Texture2D>(decltype(nullptr));
				ICollection_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Texture2D>(const ICollection_1<UnityEngine::Texture2D>& other);
				ICollection_1<UnityEngine::Texture2D>(ICollection_1<UnityEngine::Texture2D>&& other);
				virtual ~ICollection_1<UnityEngine::Texture2D>();
				ICollection_1<UnityEngine::Texture2D>& operator=(const ICollection_1<UnityEngine::Texture2D>& other);
				ICollection_1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Texture2D>& operator=(ICollection_1<UnityEngine::Texture2D>&& other);
				bool operator==(const ICollection_1<UnityEngine::Texture2D>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Texture2D>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineTexture2DIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Texture2D> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineTexture2DIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineTexture2DIterator(System::Collections::Generic::ICollection_1<UnityEngine::Texture2D>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineTexture2DIterator();
		SystemCollectionsGenericICollectionUnityEngineTexture2DIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineTexture2DIterator& other);
		UnityEngine::Texture2D operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineTexture2DIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Texture2D>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineTexture2DIterator end(System::Collections::Generic::ICollection_1<UnityEngine::Texture2D>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Cubemap> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Cubemap>
			{
				ICollection_1<UnityEngine::Cubemap>(decltype(nullptr));
				ICollection_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Cubemap>(const ICollection_1<UnityEngine::Cubemap>& other);
				ICollection_1<UnityEngine::Cubemap>(ICollection_1<UnityEngine::Cubemap>&& other);
				virtual ~ICollection_1<UnityEngine::Cubemap>();
				ICollection_1<UnityEngine::Cubemap>& operator=(const ICollection_1<UnityEngine::Cubemap>& other);
				ICollection_1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Cubemap>& operator=(ICollection_1<UnityEngine::Cubemap>&& other);
				bool operator==(const ICollection_1<UnityEngine::Cubemap>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Cubemap>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineCubemapIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Cubemap> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineCubemapIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineCubemapIterator(System::Collections::Generic::ICollection_1<UnityEngine::Cubemap>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineCubemapIterator();
		SystemCollectionsGenericICollectionUnityEngineCubemapIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineCubemapIterator& other);
		UnityEngine::Cubemap operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineCubemapIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Cubemap>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineCubemapIterator end(System::Collections::Generic::ICollection_1<UnityEngine::Cubemap>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Matrix4x4> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Matrix4x4>
			{
				ICollection_1<UnityEngine::Matrix4x4>(decltype(nullptr));
				ICollection_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Matrix4x4>(const ICollection_1<UnityEngine::Matrix4x4>& other);
				ICollection_1<UnityEngine::Matrix4x4>(ICollection_1<UnityEngine::Matrix4x4>&& other);
				virtual ~ICollection_1<UnityEngine::Matrix4x4>();
				ICollection_1<UnityEngine::Matrix4x4>& operator=(const ICollection_1<UnityEngine::Matrix4x4>& other);
				ICollection_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Matrix4x4>& operator=(ICollection_1<UnityEngine::Matrix4x4>&& other);
				bool operator==(const ICollection_1<UnityEngine::Matrix4x4>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Matrix4x4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Matrix4x4> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator(System::Collections::Generic::ICollection_1<UnityEngine::Matrix4x4>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator();
		SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator& other);
		UnityEngine::Matrix4x4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Matrix4x4>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineMatrix4x4Iterator end(System::Collections::Generic::ICollection_1<UnityEngine::Matrix4x4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::IPlMessageable> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::IPlMessageable>
			{
				ICollection_1<UPlasma::PlEmu::IPlMessageable>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::IPlMessageable>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::IPlMessageable>(const ICollection_1<UPlasma::PlEmu::IPlMessageable>& other);
				ICollection_1<UPlasma::PlEmu::IPlMessageable>(ICollection_1<UPlasma::PlEmu::IPlMessageable>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::IPlMessageable>();
				ICollection_1<UPlasma::PlEmu::IPlMessageable>& operator=(const ICollection_1<UPlasma::PlEmu::IPlMessageable>& other);
				ICollection_1<UPlasma::PlEmu::IPlMessageable>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::IPlMessageable>& operator=(ICollection_1<UPlasma::PlEmu::IPlMessageable>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::IPlMessageable>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IPlMessageable> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator& other);
		UPlasma::PlEmu::IPlMessageable operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuIPlMessageableIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlConditionalObject> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>
			{
				ICollection_1<UPlasma::PlEmu::PlConditionalObject>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlConditionalObject>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlConditionalObject>(const ICollection_1<UPlasma::PlEmu::PlConditionalObject>& other);
				ICollection_1<UPlasma::PlEmu::PlConditionalObject>(ICollection_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlConditionalObject>();
				ICollection_1<UPlasma::PlEmu::PlConditionalObject>& operator=(const ICollection_1<UPlasma::PlEmu::PlConditionalObject>& other);
				ICollection_1<UPlasma::PlEmu::PlConditionalObject>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlConditionalObject>& operator=(ICollection_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlConditionalObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator& other);
		UPlasma::PlEmu::PlConditionalObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlConditionalObjectIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlDetector> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlDetector>
			{
				ICollection_1<UPlasma::PlEmu::PlDetector>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlDetector>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlDetector>(const ICollection_1<UPlasma::PlEmu::PlDetector>& other);
				ICollection_1<UPlasma::PlEmu::PlDetector>(ICollection_1<UPlasma::PlEmu::PlDetector>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlDetector>();
				ICollection_1<UPlasma::PlEmu::PlDetector>& operator=(const ICollection_1<UPlasma::PlEmu::PlDetector>& other);
				ICollection_1<UPlasma::PlEmu::PlDetector>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlDetector>& operator=(ICollection_1<UPlasma::PlEmu::PlDetector>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlDetector>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlDetector>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlDetector> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlDetector>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator& other);
		UPlasma::PlEmu::PlDetector operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlDetector>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlDetectorIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlDetector>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlMessage> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlMessage>
			{
				ICollection_1<UPlasma::PlEmu::PlMessage>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlMessage>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlMessage>(const ICollection_1<UPlasma::PlEmu::PlMessage>& other);
				ICollection_1<UPlasma::PlEmu::PlMessage>(ICollection_1<UPlasma::PlEmu::PlMessage>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlMessage>();
				ICollection_1<UPlasma::PlEmu::PlMessage>& operator=(const ICollection_1<UPlasma::PlEmu::PlMessage>& other);
				ICollection_1<UPlasma::PlEmu::PlMessage>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlMessage>& operator=(ICollection_1<UPlasma::PlEmu::PlMessage>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlMessage>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlMessage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlMessage> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlMessage>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator& other);
		UPlasma::PlEmu::PlMessage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlMessage>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlMessageIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlMessage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlOneShotCallback> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>
			{
				ICollection_1<UPlasma::PlEmu::PlOneShotCallback>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlOneShotCallback>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlOneShotCallback>(const ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				ICollection_1<UPlasma::PlEmu::PlOneShotCallback>(ICollection_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlOneShotCallback>();
				ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(const ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(ICollection_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator& other);
		UPlasma::PlEmu::PlOneShotCallback operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlOneShotCallbackIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>
			{
				ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(const ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>();
				ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(const ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator& other);
		UPlasma::PlEmu::PlRandomSoundModGroup operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlRandomSoundModGroupIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlPage> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlPage>
			{
				ICollection_1<UPlasma::PlEmu::PlPage>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlPage>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlPage>(const ICollection_1<UPlasma::PlEmu::PlPage>& other);
				ICollection_1<UPlasma::PlEmu::PlPage>(ICollection_1<UPlasma::PlEmu::PlPage>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlPage>();
				ICollection_1<UPlasma::PlEmu::PlPage>& operator=(const ICollection_1<UPlasma::PlEmu::PlPage>& other);
				ICollection_1<UPlasma::PlEmu::PlPage>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlPage>& operator=(ICollection_1<UPlasma::PlEmu::PlPage>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlPage>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlPage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlPage> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlPage>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator& other);
		UPlasma::PlEmu::PlPage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlPage>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlPageIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlPage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PrpImportClues> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PrpImportClues>
			{
				ICollection_1<UPlasma::PlEmu::PrpImportClues>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PrpImportClues>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PrpImportClues>(const ICollection_1<UPlasma::PlEmu::PrpImportClues>& other);
				ICollection_1<UPlasma::PlEmu::PrpImportClues>(ICollection_1<UPlasma::PlEmu::PrpImportClues>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PrpImportClues>();
				ICollection_1<UPlasma::PlEmu::PrpImportClues>& operator=(const ICollection_1<UPlasma::PlEmu::PrpImportClues>& other);
				ICollection_1<UPlasma::PlEmu::PrpImportClues>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PrpImportClues>& operator=(ICollection_1<UPlasma::PlEmu::PrpImportClues>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PrpImportClues>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PrpImportClues> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator& other);
		UPlasma::PlEmu::PrpImportClues operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPrpImportCluesIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlCluster> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlCluster>
			{
				ICollection_1<UPlasma::PlEmu::PlCluster>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlCluster>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlCluster>(const ICollection_1<UPlasma::PlEmu::PlCluster>& other);
				ICollection_1<UPlasma::PlEmu::PlCluster>(ICollection_1<UPlasma::PlEmu::PlCluster>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlCluster>();
				ICollection_1<UPlasma::PlEmu::PlCluster>& operator=(const ICollection_1<UPlasma::PlEmu::PlCluster>& other);
				ICollection_1<UPlasma::PlEmu::PlCluster>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlCluster>& operator=(ICollection_1<UPlasma::PlEmu::PlCluster>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlCluster>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlCluster>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlCluster> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlCluster>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator& other);
		UPlasma::PlEmu::PlCluster operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlCluster>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlClusterIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlCluster>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::AgAnim> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::AgAnim>
			{
				ICollection_1<UPlasma::PlEmu::AgAnim>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::AgAnim>(const ICollection_1<UPlasma::PlEmu::AgAnim>& other);
				ICollection_1<UPlasma::PlEmu::AgAnim>(ICollection_1<UPlasma::PlEmu::AgAnim>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::AgAnim>();
				ICollection_1<UPlasma::PlEmu::AgAnim>& operator=(const ICollection_1<UPlasma::PlEmu::AgAnim>& other);
				ICollection_1<UPlasma::PlEmu::AgAnim>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::AgAnim>& operator=(ICollection_1<UPlasma::PlEmu::AgAnim>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::AgAnim>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::AgAnim>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::AgAnim> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::AgAnim>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator& other);
		UPlasma::PlEmu::AgAnim operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::AgAnim>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuAgAnimIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::AgAnim>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Color> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Color>
			{
				ICollection_1<UnityEngine::Color>(decltype(nullptr));
				ICollection_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Color>(const ICollection_1<UnityEngine::Color>& other);
				ICollection_1<UnityEngine::Color>(ICollection_1<UnityEngine::Color>&& other);
				virtual ~ICollection_1<UnityEngine::Color>();
				ICollection_1<UnityEngine::Color>& operator=(const ICollection_1<UnityEngine::Color>& other);
				ICollection_1<UnityEngine::Color>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Color>& operator=(ICollection_1<UnityEngine::Color>&& other);
				bool operator==(const ICollection_1<UnityEngine::Color>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Color>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineColorIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineColorIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineColorIterator(System::Collections::Generic::ICollection_1<UnityEngine::Color>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineColorIterator();
		SystemCollectionsGenericICollectionUnityEngineColorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineColorIterator& other);
		UnityEngine::Color operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineColorIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Color>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineColorIterator end(System::Collections::Generic::ICollection_1<UnityEngine::Color>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Color32> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Color32>
			{
				ICollection_1<UnityEngine::Color32>(decltype(nullptr));
				ICollection_1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Color32>(const ICollection_1<UnityEngine::Color32>& other);
				ICollection_1<UnityEngine::Color32>(ICollection_1<UnityEngine::Color32>&& other);
				virtual ~ICollection_1<UnityEngine::Color32>();
				ICollection_1<UnityEngine::Color32>& operator=(const ICollection_1<UnityEngine::Color32>& other);
				ICollection_1<UnityEngine::Color32>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Color32>& operator=(ICollection_1<UnityEngine::Color32>&& other);
				bool operator==(const ICollection_1<UnityEngine::Color32>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Color32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineColor32Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color32> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineColor32Iterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineColor32Iterator(System::Collections::Generic::ICollection_1<UnityEngine::Color32>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineColor32Iterator();
		SystemCollectionsGenericICollectionUnityEngineColor32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineColor32Iterator& other);
		UnityEngine::Color32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineColor32Iterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Color32>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineColor32Iterator end(System::Collections::Generic::ICollection_1<UnityEngine::Color32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Vector2> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Vector2>
			{
				ICollection_1<UnityEngine::Vector2>(decltype(nullptr));
				ICollection_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Vector2>(const ICollection_1<UnityEngine::Vector2>& other);
				ICollection_1<UnityEngine::Vector2>(ICollection_1<UnityEngine::Vector2>&& other);
				virtual ~ICollection_1<UnityEngine::Vector2>();
				ICollection_1<UnityEngine::Vector2>& operator=(const ICollection_1<UnityEngine::Vector2>& other);
				ICollection_1<UnityEngine::Vector2>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Vector2>& operator=(ICollection_1<UnityEngine::Vector2>&& other);
				bool operator==(const ICollection_1<UnityEngine::Vector2>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Vector2>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineVector2Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector2> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineVector2Iterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineVector2Iterator(System::Collections::Generic::ICollection_1<UnityEngine::Vector2>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineVector2Iterator();
		SystemCollectionsGenericICollectionUnityEngineVector2Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineVector2Iterator& other);
		UnityEngine::Vector2 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineVector2Iterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Vector2>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineVector2Iterator end(System::Collections::Generic::ICollection_1<UnityEngine::Vector2>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Vector3> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Vector3>
			{
				ICollection_1<UnityEngine::Vector3>(decltype(nullptr));
				ICollection_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Vector3>(const ICollection_1<UnityEngine::Vector3>& other);
				ICollection_1<UnityEngine::Vector3>(ICollection_1<UnityEngine::Vector3>&& other);
				virtual ~ICollection_1<UnityEngine::Vector3>();
				ICollection_1<UnityEngine::Vector3>& operator=(const ICollection_1<UnityEngine::Vector3>& other);
				ICollection_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Vector3>& operator=(ICollection_1<UnityEngine::Vector3>&& other);
				bool operator==(const ICollection_1<UnityEngine::Vector3>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Vector3>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineVector3Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector3> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineVector3Iterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineVector3Iterator(System::Collections::Generic::ICollection_1<UnityEngine::Vector3>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineVector3Iterator();
		SystemCollectionsGenericICollectionUnityEngineVector3Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineVector3Iterator& other);
		UnityEngine::Vector3 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineVector3Iterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Vector3>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineVector3Iterator end(System::Collections::Generic::ICollection_1<UnityEngine::Vector3>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Vector4> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Vector4>
			{
				ICollection_1<UnityEngine::Vector4>(decltype(nullptr));
				ICollection_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Vector4>(const ICollection_1<UnityEngine::Vector4>& other);
				ICollection_1<UnityEngine::Vector4>(ICollection_1<UnityEngine::Vector4>&& other);
				virtual ~ICollection_1<UnityEngine::Vector4>();
				ICollection_1<UnityEngine::Vector4>& operator=(const ICollection_1<UnityEngine::Vector4>& other);
				ICollection_1<UnityEngine::Vector4>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Vector4>& operator=(ICollection_1<UnityEngine::Vector4>&& other);
				bool operator==(const ICollection_1<UnityEngine::Vector4>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Vector4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineVector4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector4> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineVector4Iterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineVector4Iterator(System::Collections::Generic::ICollection_1<UnityEngine::Vector4>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineVector4Iterator();
		SystemCollectionsGenericICollectionUnityEngineVector4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineVector4Iterator& other);
		UnityEngine::Vector4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineVector4Iterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Vector4>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineVector4Iterator end(System::Collections::Generic::ICollection_1<UnityEngine::Vector4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::BoneWeight> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::BoneWeight>
			{
				ICollection_1<UnityEngine::BoneWeight>(decltype(nullptr));
				ICollection_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::BoneWeight>(const ICollection_1<UnityEngine::BoneWeight>& other);
				ICollection_1<UnityEngine::BoneWeight>(ICollection_1<UnityEngine::BoneWeight>&& other);
				virtual ~ICollection_1<UnityEngine::BoneWeight>();
				ICollection_1<UnityEngine::BoneWeight>& operator=(const ICollection_1<UnityEngine::BoneWeight>& other);
				ICollection_1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::BoneWeight>& operator=(ICollection_1<UnityEngine::BoneWeight>&& other);
				bool operator==(const ICollection_1<UnityEngine::BoneWeight>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::BoneWeight>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::BoneWeight> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator(System::Collections::Generic::ICollection_1<UnityEngine::BoneWeight>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator();
		SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator& other);
		UnityEngine::BoneWeight operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::BoneWeight>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineBoneWeightIterator end(System::Collections::Generic::ICollection_1<UnityEngine::BoneWeight>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UnityEngine::Quaternion> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Quaternion>
			{
				ICollection_1<UnityEngine::Quaternion>(decltype(nullptr));
				ICollection_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UnityEngine::Quaternion>(const ICollection_1<UnityEngine::Quaternion>& other);
				ICollection_1<UnityEngine::Quaternion>(ICollection_1<UnityEngine::Quaternion>&& other);
				virtual ~ICollection_1<UnityEngine::Quaternion>();
				ICollection_1<UnityEngine::Quaternion>& operator=(const ICollection_1<UnityEngine::Quaternion>& other);
				ICollection_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
				ICollection_1<UnityEngine::Quaternion>& operator=(ICollection_1<UnityEngine::Quaternion>&& other);
				bool operator==(const ICollection_1<UnityEngine::Quaternion>& other) const;
				bool operator!=(const ICollection_1<UnityEngine::Quaternion>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUnityEngineQuaternionIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Quaternion> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUnityEngineQuaternionIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUnityEngineQuaternionIterator(System::Collections::Generic::ICollection_1<UnityEngine::Quaternion>& enumerable);
		~SystemCollectionsGenericICollectionUnityEngineQuaternionIterator();
		SystemCollectionsGenericICollectionUnityEngineQuaternionIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUnityEngineQuaternionIterator& other);
		UnityEngine::Quaternion operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUnityEngineQuaternionIterator begin(System::Collections::Generic::ICollection_1<UnityEngine::Quaternion>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUnityEngineQuaternionIterator end(System::Collections::Generic::ICollection_1<UnityEngine::Quaternion>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlResponderState> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlResponderState>
			{
				ICollection_1<UPlasma::PlEmu::PlResponderState>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlResponderState>(const ICollection_1<UPlasma::PlEmu::PlResponderState>& other);
				ICollection_1<UPlasma::PlEmu::PlResponderState>(ICollection_1<UPlasma::PlEmu::PlResponderState>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlResponderState>();
				ICollection_1<UPlasma::PlEmu::PlResponderState>& operator=(const ICollection_1<UPlasma::PlEmu::PlResponderState>& other);
				ICollection_1<UPlasma::PlEmu::PlResponderState>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlResponderState>& operator=(ICollection_1<UPlasma::PlEmu::PlResponderState>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlResponderState>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlResponderState>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderState> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator& other);
		UPlasma::PlEmu::PlResponderState operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderState>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderStateIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlResponderCommand> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>
			{
				ICollection_1<UPlasma::PlEmu::PlResponderCommand>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlResponderCommand>(const ICollection_1<UPlasma::PlEmu::PlResponderCommand>& other);
				ICollection_1<UPlasma::PlEmu::PlResponderCommand>(ICollection_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlResponderCommand>();
				ICollection_1<UPlasma::PlEmu::PlResponderCommand>& operator=(const ICollection_1<UPlasma::PlEmu::PlResponderCommand>& other);
				ICollection_1<UPlasma::PlEmu::PlResponderCommand>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlResponderCommand>& operator=(ICollection_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderCommand> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator& other);
		UPlasma::PlEmu::PlResponderCommand operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlResponderCommandIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::PlSound> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlSound>
			{
				ICollection_1<UPlasma::PlEmu::PlSound>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::PlSound>(const ICollection_1<UPlasma::PlEmu::PlSound>& other);
				ICollection_1<UPlasma::PlEmu::PlSound>(ICollection_1<UPlasma::PlEmu::PlSound>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::PlSound>();
				ICollection_1<UPlasma::PlEmu::PlSound>& operator=(const ICollection_1<UPlasma::PlEmu::PlSound>& other);
				ICollection_1<UPlasma::PlEmu::PlSound>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::PlSound>& operator=(ICollection_1<UPlasma::PlEmu::PlSound>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::PlSound>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::PlSound>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlSound> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlSound>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator& other);
		UPlasma::PlEmu::PlSound operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlSound>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuPlSoundIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlSound>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<UPlasma::PlEmu::IAgApplicator> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::IAgApplicator>
			{
				ICollection_1<UPlasma::PlEmu::IAgApplicator>(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle);
				ICollection_1<UPlasma::PlEmu::IAgApplicator>(const ICollection_1<UPlasma::PlEmu::IAgApplicator>& other);
				ICollection_1<UPlasma::PlEmu::IAgApplicator>(ICollection_1<UPlasma::PlEmu::IAgApplicator>&& other);
				virtual ~ICollection_1<UPlasma::PlEmu::IAgApplicator>();
				ICollection_1<UPlasma::PlEmu::IAgApplicator>& operator=(const ICollection_1<UPlasma::PlEmu::IAgApplicator>& other);
				ICollection_1<UPlasma::PlEmu::IAgApplicator>& operator=(decltype(nullptr));
				ICollection_1<UPlasma::PlEmu::IAgApplicator>& operator=(ICollection_1<UPlasma::PlEmu::IAgApplicator>&& other);
				bool operator==(const ICollection_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				bool operator!=(const ICollection_1<UPlasma::PlEmu::IAgApplicator>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IAgApplicator> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		~SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator();
		SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator& other);
		UPlasma::PlEmu::IAgApplicator operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator begin(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
			Plugin::SystemCollectionsGenericICollectionUPlasmaPlEmuIAgApplicatorIterator end(System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Single> : virtual System::Collections::Generic::IEnumerable_1<System::Single>
			{
				ICollection_1<System::Single>(decltype(nullptr));
				ICollection_1<System::Single>(Plugin::InternalUse, int32_t handle);
				ICollection_1<System::Single>(const ICollection_1<System::Single>& other);
				ICollection_1<System::Single>(ICollection_1<System::Single>&& other);
				virtual ~ICollection_1<System::Single>();
				ICollection_1<System::Single>& operator=(const ICollection_1<System::Single>& other);
				ICollection_1<System::Single>& operator=(decltype(nullptr));
				ICollection_1<System::Single>& operator=(ICollection_1<System::Single>&& other);
				bool operator==(const ICollection_1<System::Single>& other) const;
				bool operator!=(const ICollection_1<System::Single>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionSystemSingleIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Single> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionSystemSingleIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionSystemSingleIterator(System::Collections::Generic::ICollection_1<System::Single>& enumerable);
		~SystemCollectionsGenericICollectionSystemSingleIterator();
		SystemCollectionsGenericICollectionSystemSingleIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionSystemSingleIterator& other);
		System::Single operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionSystemSingleIterator begin(System::Collections::Generic::ICollection_1<System::Single>& enumerable);
			Plugin::SystemCollectionsGenericICollectionSystemSingleIterator end(System::Collections::Generic::ICollection_1<System::Single>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Int32> : virtual System::Collections::Generic::IEnumerable_1<System::Int32>
			{
				ICollection_1<System::Int32>(decltype(nullptr));
				ICollection_1<System::Int32>(Plugin::InternalUse, int32_t handle);
				ICollection_1<System::Int32>(const ICollection_1<System::Int32>& other);
				ICollection_1<System::Int32>(ICollection_1<System::Int32>&& other);
				virtual ~ICollection_1<System::Int32>();
				ICollection_1<System::Int32>& operator=(const ICollection_1<System::Int32>& other);
				ICollection_1<System::Int32>& operator=(decltype(nullptr));
				ICollection_1<System::Int32>& operator=(ICollection_1<System::Int32>&& other);
				bool operator==(const ICollection_1<System::Int32>& other) const;
				bool operator!=(const ICollection_1<System::Int32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionSystemInt32Iterator
	{
		System::Collections::Generic::IEnumerator_1<System::Int32> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionSystemInt32Iterator(decltype(nullptr));
		SystemCollectionsGenericICollectionSystemInt32Iterator(System::Collections::Generic::ICollection_1<System::Int32>& enumerable);
		~SystemCollectionsGenericICollectionSystemInt32Iterator();
		SystemCollectionsGenericICollectionSystemInt32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionSystemInt32Iterator& other);
		System::Int32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionSystemInt32Iterator begin(System::Collections::Generic::ICollection_1<System::Int32>& enumerable);
			Plugin::SystemCollectionsGenericICollectionSystemInt32Iterator end(System::Collections::Generic::ICollection_1<System::Int32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Object> : virtual System::Collections::Generic::IEnumerable_1<System::Object>
			{
				ICollection_1<System::Object>(decltype(nullptr));
				ICollection_1<System::Object>(Plugin::InternalUse, int32_t handle);
				ICollection_1<System::Object>(const ICollection_1<System::Object>& other);
				ICollection_1<System::Object>(ICollection_1<System::Object>&& other);
				virtual ~ICollection_1<System::Object>();
				ICollection_1<System::Object>& operator=(const ICollection_1<System::Object>& other);
				ICollection_1<System::Object>& operator=(decltype(nullptr));
				ICollection_1<System::Object>& operator=(ICollection_1<System::Object>&& other);
				bool operator==(const ICollection_1<System::Object>& other) const;
				bool operator!=(const ICollection_1<System::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionSystemObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionSystemObjectIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionSystemObjectIterator(System::Collections::Generic::ICollection_1<System::Object>& enumerable);
		~SystemCollectionsGenericICollectionSystemObjectIterator();
		SystemCollectionsGenericICollectionSystemObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionSystemObjectIterator& other);
		System::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionSystemObjectIterator begin(System::Collections::Generic::ICollection_1<System::Object>& enumerable);
			Plugin::SystemCollectionsGenericICollectionSystemObjectIterator end(System::Collections::Generic::ICollection_1<System::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::Byte> : virtual System::Collections::Generic::IEnumerable_1<System::Byte>
			{
				ICollection_1<System::Byte>(decltype(nullptr));
				ICollection_1<System::Byte>(Plugin::InternalUse, int32_t handle);
				ICollection_1<System::Byte>(const ICollection_1<System::Byte>& other);
				ICollection_1<System::Byte>(ICollection_1<System::Byte>&& other);
				virtual ~ICollection_1<System::Byte>();
				ICollection_1<System::Byte>& operator=(const ICollection_1<System::Byte>& other);
				ICollection_1<System::Byte>& operator=(decltype(nullptr));
				ICollection_1<System::Byte>& operator=(ICollection_1<System::Byte>&& other);
				bool operator==(const ICollection_1<System::Byte>& other) const;
				bool operator!=(const ICollection_1<System::Byte>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionSystemByteIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Byte> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionSystemByteIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionSystemByteIterator(System::Collections::Generic::ICollection_1<System::Byte>& enumerable);
		~SystemCollectionsGenericICollectionSystemByteIterator();
		SystemCollectionsGenericICollectionSystemByteIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionSystemByteIterator& other);
		System::Byte operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionSystemByteIterator begin(System::Collections::Generic::ICollection_1<System::Byte>& enumerable);
			Plugin::SystemCollectionsGenericICollectionSystemByteIterator end(System::Collections::Generic::ICollection_1<System::Byte>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct ICollection_1<System::String> : virtual System::Collections::Generic::IEnumerable_1<System::String>
			{
				ICollection_1<System::String>(decltype(nullptr));
				ICollection_1<System::String>(Plugin::InternalUse, int32_t handle);
				ICollection_1<System::String>(const ICollection_1<System::String>& other);
				ICollection_1<System::String>(ICollection_1<System::String>&& other);
				virtual ~ICollection_1<System::String>();
				ICollection_1<System::String>& operator=(const ICollection_1<System::String>& other);
				ICollection_1<System::String>& operator=(decltype(nullptr));
				ICollection_1<System::String>& operator=(ICollection_1<System::String>&& other);
				bool operator==(const ICollection_1<System::String>& other) const;
				bool operator!=(const ICollection_1<System::String>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericICollectionSystemStringIterator
	{
		System::Collections::Generic::IEnumerator_1<System::String> enumerator;
		bool hasMore;
		SystemCollectionsGenericICollectionSystemStringIterator(decltype(nullptr));
		SystemCollectionsGenericICollectionSystemStringIterator(System::Collections::Generic::ICollection_1<System::String>& enumerable);
		~SystemCollectionsGenericICollectionSystemStringIterator();
		SystemCollectionsGenericICollectionSystemStringIterator& operator++();
		bool operator!=(const SystemCollectionsGenericICollectionSystemStringIterator& other);
		System::String operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericICollectionSystemStringIterator begin(System::Collections::Generic::ICollection_1<System::String>& enumerable);
			Plugin::SystemCollectionsGenericICollectionSystemStringIterator end(System::Collections::Generic::ICollection_1<System::String>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		struct IStructuralEquatable : virtual System::Object
		{
			IStructuralEquatable(decltype(nullptr));
			IStructuralEquatable(Plugin::InternalUse, int32_t handle);
			IStructuralEquatable(const IStructuralEquatable& other);
			IStructuralEquatable(IStructuralEquatable&& other);
			virtual ~IStructuralEquatable();
			IStructuralEquatable& operator=(const IStructuralEquatable& other);
			IStructuralEquatable& operator=(decltype(nullptr));
			IStructuralEquatable& operator=(IStructuralEquatable&& other);
			bool operator==(const IStructuralEquatable& other) const;
			bool operator!=(const IStructuralEquatable& other) const;
		};
	}
}

namespace System
{
	namespace Collections
	{
		struct IStructuralComparable : virtual System::Object
		{
			IStructuralComparable(decltype(nullptr));
			IStructuralComparable(Plugin::InternalUse, int32_t handle);
			IStructuralComparable(const IStructuralComparable& other);
			IStructuralComparable(IStructuralComparable&& other);
			virtual ~IStructuralComparable();
			IStructuralComparable& operator=(const IStructuralComparable& other);
			IStructuralComparable& operator=(decltype(nullptr));
			IStructuralComparable& operator=(IStructuralComparable&& other);
			bool operator==(const IStructuralComparable& other) const;
			bool operator!=(const IStructuralComparable& other) const;
		};
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Object> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Object>
			{
				IReadOnlyCollection_1<UnityEngine::Object>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Object>(const IReadOnlyCollection_1<UnityEngine::Object>& other);
				IReadOnlyCollection_1<UnityEngine::Object>(IReadOnlyCollection_1<UnityEngine::Object>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Object>();
				IReadOnlyCollection_1<UnityEngine::Object>& operator=(const IReadOnlyCollection_1<UnityEngine::Object>& other);
				IReadOnlyCollection_1<UnityEngine::Object>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Object>& operator=(IReadOnlyCollection_1<UnityEngine::Object>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Object>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Object>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator& other);
		UnityEngine::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Object>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineObjectIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::GameObject> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::GameObject>
			{
				IReadOnlyCollection_1<UnityEngine::GameObject>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::GameObject>(const IReadOnlyCollection_1<UnityEngine::GameObject>& other);
				IReadOnlyCollection_1<UnityEngine::GameObject>(IReadOnlyCollection_1<UnityEngine::GameObject>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::GameObject>();
				IReadOnlyCollection_1<UnityEngine::GameObject>& operator=(const IReadOnlyCollection_1<UnityEngine::GameObject>& other);
				IReadOnlyCollection_1<UnityEngine::GameObject>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::GameObject>& operator=(IReadOnlyCollection_1<UnityEngine::GameObject>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::GameObject>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::GameObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::GameObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::GameObject>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator& other);
		UnityEngine::GameObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::GameObject>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineGameObjectIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::GameObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Transform> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Transform>
			{
				IReadOnlyCollection_1<UnityEngine::Transform>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Transform>(const IReadOnlyCollection_1<UnityEngine::Transform>& other);
				IReadOnlyCollection_1<UnityEngine::Transform>(IReadOnlyCollection_1<UnityEngine::Transform>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Transform>();
				IReadOnlyCollection_1<UnityEngine::Transform>& operator=(const IReadOnlyCollection_1<UnityEngine::Transform>& other);
				IReadOnlyCollection_1<UnityEngine::Transform>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Transform>& operator=(IReadOnlyCollection_1<UnityEngine::Transform>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Transform>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Transform>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Transform> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Transform>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator& other);
		UnityEngine::Transform operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Transform>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineTransformIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Transform>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::MeshFilter> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::MeshFilter>
			{
				IReadOnlyCollection_1<UnityEngine::MeshFilter>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::MeshFilter>(const IReadOnlyCollection_1<UnityEngine::MeshFilter>& other);
				IReadOnlyCollection_1<UnityEngine::MeshFilter>(IReadOnlyCollection_1<UnityEngine::MeshFilter>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::MeshFilter>();
				IReadOnlyCollection_1<UnityEngine::MeshFilter>& operator=(const IReadOnlyCollection_1<UnityEngine::MeshFilter>& other);
				IReadOnlyCollection_1<UnityEngine::MeshFilter>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::MeshFilter>& operator=(IReadOnlyCollection_1<UnityEngine::MeshFilter>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::MeshFilter>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::MeshFilter>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::MeshFilter> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::MeshFilter>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator& other);
		UnityEngine::MeshFilter operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::MeshFilter>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineMeshFilterIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::MeshFilter>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Material> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Material>
			{
				IReadOnlyCollection_1<UnityEngine::Material>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Material>(const IReadOnlyCollection_1<UnityEngine::Material>& other);
				IReadOnlyCollection_1<UnityEngine::Material>(IReadOnlyCollection_1<UnityEngine::Material>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Material>();
				IReadOnlyCollection_1<UnityEngine::Material>& operator=(const IReadOnlyCollection_1<UnityEngine::Material>& other);
				IReadOnlyCollection_1<UnityEngine::Material>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Material>& operator=(IReadOnlyCollection_1<UnityEngine::Material>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Material>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Material>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Material> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Material>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator& other);
		UnityEngine::Material operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Material>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineMaterialIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Material>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Texture2D> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Texture2D>
			{
				IReadOnlyCollection_1<UnityEngine::Texture2D>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Texture2D>(const IReadOnlyCollection_1<UnityEngine::Texture2D>& other);
				IReadOnlyCollection_1<UnityEngine::Texture2D>(IReadOnlyCollection_1<UnityEngine::Texture2D>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Texture2D>();
				IReadOnlyCollection_1<UnityEngine::Texture2D>& operator=(const IReadOnlyCollection_1<UnityEngine::Texture2D>& other);
				IReadOnlyCollection_1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Texture2D>& operator=(IReadOnlyCollection_1<UnityEngine::Texture2D>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Texture2D>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Texture2D>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Texture2D> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Texture2D>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator& other);
		UnityEngine::Texture2D operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Texture2D>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineTexture2DIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Texture2D>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Cubemap> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Cubemap>
			{
				IReadOnlyCollection_1<UnityEngine::Cubemap>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Cubemap>(const IReadOnlyCollection_1<UnityEngine::Cubemap>& other);
				IReadOnlyCollection_1<UnityEngine::Cubemap>(IReadOnlyCollection_1<UnityEngine::Cubemap>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Cubemap>();
				IReadOnlyCollection_1<UnityEngine::Cubemap>& operator=(const IReadOnlyCollection_1<UnityEngine::Cubemap>& other);
				IReadOnlyCollection_1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Cubemap>& operator=(IReadOnlyCollection_1<UnityEngine::Cubemap>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Cubemap>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Cubemap>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Cubemap> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Cubemap>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator& other);
		UnityEngine::Cubemap operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Cubemap>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineCubemapIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Cubemap>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Matrix4x4> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Matrix4x4>
			{
				IReadOnlyCollection_1<UnityEngine::Matrix4x4>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Matrix4x4>(const IReadOnlyCollection_1<UnityEngine::Matrix4x4>& other);
				IReadOnlyCollection_1<UnityEngine::Matrix4x4>(IReadOnlyCollection_1<UnityEngine::Matrix4x4>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Matrix4x4>();
				IReadOnlyCollection_1<UnityEngine::Matrix4x4>& operator=(const IReadOnlyCollection_1<UnityEngine::Matrix4x4>& other);
				IReadOnlyCollection_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Matrix4x4>& operator=(IReadOnlyCollection_1<UnityEngine::Matrix4x4>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Matrix4x4>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Matrix4x4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Matrix4x4> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Matrix4x4>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator& other);
		UnityEngine::Matrix4x4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Matrix4x4>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineMatrix4x4Iterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Matrix4x4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::IPlMessageable>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>(const IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>(IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>();
				IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IPlMessageable> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator& other);
		UPlasma::PlEmu::IPlMessageable operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIPlMessageableIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlConditionalObject>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>(IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlConditionalObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator& other);
		UPlasma::PlEmu::PlConditionalObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlConditionalObjectIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlDetector>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>(IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlDetector> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator& other);
		UPlasma::PlEmu::PlDetector operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlDetectorIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlMessage>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>(IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlMessage> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator& other);
		UPlasma::PlEmu::PlMessage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlMessageIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlOneShotCallback>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>(IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator& other);
		UPlasma::PlEmu::PlOneShotCallback operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlOneShotCallbackIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlRandomSoundModGroup>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>(IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator& other);
		UPlasma::PlEmu::PlRandomSoundModGroup operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlRandomSoundModGroupIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlPage> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlPage>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>(IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlPage> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator& other);
		UPlasma::PlEmu::PlPage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlPageIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PrpImportClues>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>(const IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>(IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PrpImportClues> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator& other);
		UPlasma::PlEmu::PrpImportClues operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPrpImportCluesIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlCluster>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>(IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlCluster> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator& other);
		UPlasma::PlEmu::PlCluster operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlClusterIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::AgAnim>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>(const IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>(IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>();
				IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::AgAnim> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator& other);
		UPlasma::PlEmu::AgAnim operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuAgAnimIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Color> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Color>
			{
				IReadOnlyCollection_1<UnityEngine::Color>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Color>(const IReadOnlyCollection_1<UnityEngine::Color>& other);
				IReadOnlyCollection_1<UnityEngine::Color>(IReadOnlyCollection_1<UnityEngine::Color>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Color>();
				IReadOnlyCollection_1<UnityEngine::Color>& operator=(const IReadOnlyCollection_1<UnityEngine::Color>& other);
				IReadOnlyCollection_1<UnityEngine::Color>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Color>& operator=(IReadOnlyCollection_1<UnityEngine::Color>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Color>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Color>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator& other);
		UnityEngine::Color operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineColorIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Color32> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Color32>
			{
				IReadOnlyCollection_1<UnityEngine::Color32>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Color32>(const IReadOnlyCollection_1<UnityEngine::Color32>& other);
				IReadOnlyCollection_1<UnityEngine::Color32>(IReadOnlyCollection_1<UnityEngine::Color32>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Color32>();
				IReadOnlyCollection_1<UnityEngine::Color32>& operator=(const IReadOnlyCollection_1<UnityEngine::Color32>& other);
				IReadOnlyCollection_1<UnityEngine::Color32>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Color32>& operator=(IReadOnlyCollection_1<UnityEngine::Color32>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Color32>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Color32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color32> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color32>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator& other);
		UnityEngine::Color32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color32>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineColor32Iterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Vector2> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Vector2>
			{
				IReadOnlyCollection_1<UnityEngine::Vector2>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Vector2>(const IReadOnlyCollection_1<UnityEngine::Vector2>& other);
				IReadOnlyCollection_1<UnityEngine::Vector2>(IReadOnlyCollection_1<UnityEngine::Vector2>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Vector2>();
				IReadOnlyCollection_1<UnityEngine::Vector2>& operator=(const IReadOnlyCollection_1<UnityEngine::Vector2>& other);
				IReadOnlyCollection_1<UnityEngine::Vector2>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Vector2>& operator=(IReadOnlyCollection_1<UnityEngine::Vector2>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Vector2>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Vector2>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector2> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector2>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator& other);
		UnityEngine::Vector2 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector2>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector2Iterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector2>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Vector3> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Vector3>
			{
				IReadOnlyCollection_1<UnityEngine::Vector3>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Vector3>(const IReadOnlyCollection_1<UnityEngine::Vector3>& other);
				IReadOnlyCollection_1<UnityEngine::Vector3>(IReadOnlyCollection_1<UnityEngine::Vector3>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Vector3>();
				IReadOnlyCollection_1<UnityEngine::Vector3>& operator=(const IReadOnlyCollection_1<UnityEngine::Vector3>& other);
				IReadOnlyCollection_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Vector3>& operator=(IReadOnlyCollection_1<UnityEngine::Vector3>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Vector3>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Vector3>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector3> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector3>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator& other);
		UnityEngine::Vector3 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector3>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector3Iterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector3>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Vector4> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Vector4>
			{
				IReadOnlyCollection_1<UnityEngine::Vector4>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Vector4>(const IReadOnlyCollection_1<UnityEngine::Vector4>& other);
				IReadOnlyCollection_1<UnityEngine::Vector4>(IReadOnlyCollection_1<UnityEngine::Vector4>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Vector4>();
				IReadOnlyCollection_1<UnityEngine::Vector4>& operator=(const IReadOnlyCollection_1<UnityEngine::Vector4>& other);
				IReadOnlyCollection_1<UnityEngine::Vector4>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Vector4>& operator=(IReadOnlyCollection_1<UnityEngine::Vector4>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Vector4>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Vector4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector4> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector4>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator& other);
		UnityEngine::Vector4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector4>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineVector4Iterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::BoneWeight> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::BoneWeight>
			{
				IReadOnlyCollection_1<UnityEngine::BoneWeight>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::BoneWeight>(const IReadOnlyCollection_1<UnityEngine::BoneWeight>& other);
				IReadOnlyCollection_1<UnityEngine::BoneWeight>(IReadOnlyCollection_1<UnityEngine::BoneWeight>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::BoneWeight>();
				IReadOnlyCollection_1<UnityEngine::BoneWeight>& operator=(const IReadOnlyCollection_1<UnityEngine::BoneWeight>& other);
				IReadOnlyCollection_1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::BoneWeight>& operator=(IReadOnlyCollection_1<UnityEngine::BoneWeight>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::BoneWeight>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::BoneWeight>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::BoneWeight> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::BoneWeight>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator& other);
		UnityEngine::BoneWeight operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::BoneWeight>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineBoneWeightIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::BoneWeight>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UnityEngine::Quaternion> : virtual System::Collections::Generic::IEnumerable_1<UnityEngine::Quaternion>
			{
				IReadOnlyCollection_1<UnityEngine::Quaternion>(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UnityEngine::Quaternion>(const IReadOnlyCollection_1<UnityEngine::Quaternion>& other);
				IReadOnlyCollection_1<UnityEngine::Quaternion>(IReadOnlyCollection_1<UnityEngine::Quaternion>&& other);
				virtual ~IReadOnlyCollection_1<UnityEngine::Quaternion>();
				IReadOnlyCollection_1<UnityEngine::Quaternion>& operator=(const IReadOnlyCollection_1<UnityEngine::Quaternion>& other);
				IReadOnlyCollection_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UnityEngine::Quaternion>& operator=(IReadOnlyCollection_1<UnityEngine::Quaternion>&& other);
				bool operator==(const IReadOnlyCollection_1<UnityEngine::Quaternion>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UnityEngine::Quaternion>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Quaternion> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Quaternion>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator();
		SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator& other);
		UnityEngine::Quaternion operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Quaternion>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUnityEngineQuaternionIterator end(System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Quaternion>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlResponderState>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>(IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderState> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator& other);
		UPlasma::PlEmu::PlResponderState operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderStateIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlResponderCommand>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>(IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderCommand> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator& other);
		UPlasma::PlEmu::PlResponderCommand operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlResponderCommandIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::PlSound> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::PlSound>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>(const IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>(IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>();
				IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlSound> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator& other);
		UPlasma::PlEmu::PlSound operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuPlSoundIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator> : virtual System::Collections::Generic::IEnumerable_1<UPlasma::PlEmu::IAgApplicator>
			{
				IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>(const IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>(IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>&& other);
				virtual ~IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>();
				IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& operator=(const IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& other);
				IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& operator=(IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>&& other);
				bool operator==(const IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				bool operator!=(const IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IAgApplicator> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator();
		SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator& other);
		UPlasma::PlEmu::IAgApplicator operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator begin(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionUPlasmaPlEmuIAgApplicatorIterator end(System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Single> : virtual System::Collections::Generic::IEnumerable_1<System::Single>
			{
				IReadOnlyCollection_1<System::Single>(decltype(nullptr));
				IReadOnlyCollection_1<System::Single>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<System::Single>(const IReadOnlyCollection_1<System::Single>& other);
				IReadOnlyCollection_1<System::Single>(IReadOnlyCollection_1<System::Single>&& other);
				virtual ~IReadOnlyCollection_1<System::Single>();
				IReadOnlyCollection_1<System::Single>& operator=(const IReadOnlyCollection_1<System::Single>& other);
				IReadOnlyCollection_1<System::Single>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<System::Single>& operator=(IReadOnlyCollection_1<System::Single>&& other);
				bool operator==(const IReadOnlyCollection_1<System::Single>& other) const;
				bool operator!=(const IReadOnlyCollection_1<System::Single>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Single> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator(System::Collections::Generic::IReadOnlyCollection_1<System::Single>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator();
		SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator& other);
		System::Single operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator begin(System::Collections::Generic::IReadOnlyCollection_1<System::Single>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemSingleIterator end(System::Collections::Generic::IReadOnlyCollection_1<System::Single>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Int32> : virtual System::Collections::Generic::IEnumerable_1<System::Int32>
			{
				IReadOnlyCollection_1<System::Int32>(decltype(nullptr));
				IReadOnlyCollection_1<System::Int32>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<System::Int32>(const IReadOnlyCollection_1<System::Int32>& other);
				IReadOnlyCollection_1<System::Int32>(IReadOnlyCollection_1<System::Int32>&& other);
				virtual ~IReadOnlyCollection_1<System::Int32>();
				IReadOnlyCollection_1<System::Int32>& operator=(const IReadOnlyCollection_1<System::Int32>& other);
				IReadOnlyCollection_1<System::Int32>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<System::Int32>& operator=(IReadOnlyCollection_1<System::Int32>&& other);
				bool operator==(const IReadOnlyCollection_1<System::Int32>& other) const;
				bool operator!=(const IReadOnlyCollection_1<System::Int32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator
	{
		System::Collections::Generic::IEnumerator_1<System::Int32> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator(System::Collections::Generic::IReadOnlyCollection_1<System::Int32>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator();
		SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator& other);
		System::Int32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator begin(System::Collections::Generic::IReadOnlyCollection_1<System::Int32>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemInt32Iterator end(System::Collections::Generic::IReadOnlyCollection_1<System::Int32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Object> : virtual System::Collections::Generic::IEnumerable_1<System::Object>
			{
				IReadOnlyCollection_1<System::Object>(decltype(nullptr));
				IReadOnlyCollection_1<System::Object>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<System::Object>(const IReadOnlyCollection_1<System::Object>& other);
				IReadOnlyCollection_1<System::Object>(IReadOnlyCollection_1<System::Object>&& other);
				virtual ~IReadOnlyCollection_1<System::Object>();
				IReadOnlyCollection_1<System::Object>& operator=(const IReadOnlyCollection_1<System::Object>& other);
				IReadOnlyCollection_1<System::Object>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<System::Object>& operator=(IReadOnlyCollection_1<System::Object>&& other);
				bool operator==(const IReadOnlyCollection_1<System::Object>& other) const;
				bool operator!=(const IReadOnlyCollection_1<System::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator(System::Collections::Generic::IReadOnlyCollection_1<System::Object>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator();
		SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator& other);
		System::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator begin(System::Collections::Generic::IReadOnlyCollection_1<System::Object>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemObjectIterator end(System::Collections::Generic::IReadOnlyCollection_1<System::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::Byte> : virtual System::Collections::Generic::IEnumerable_1<System::Byte>
			{
				IReadOnlyCollection_1<System::Byte>(decltype(nullptr));
				IReadOnlyCollection_1<System::Byte>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<System::Byte>(const IReadOnlyCollection_1<System::Byte>& other);
				IReadOnlyCollection_1<System::Byte>(IReadOnlyCollection_1<System::Byte>&& other);
				virtual ~IReadOnlyCollection_1<System::Byte>();
				IReadOnlyCollection_1<System::Byte>& operator=(const IReadOnlyCollection_1<System::Byte>& other);
				IReadOnlyCollection_1<System::Byte>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<System::Byte>& operator=(IReadOnlyCollection_1<System::Byte>&& other);
				bool operator==(const IReadOnlyCollection_1<System::Byte>& other) const;
				bool operator!=(const IReadOnlyCollection_1<System::Byte>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Byte> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator(System::Collections::Generic::IReadOnlyCollection_1<System::Byte>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator();
		SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator& other);
		System::Byte operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator begin(System::Collections::Generic::IReadOnlyCollection_1<System::Byte>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemByteIterator end(System::Collections::Generic::IReadOnlyCollection_1<System::Byte>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyCollection_1<System::String> : virtual System::Collections::Generic::IEnumerable_1<System::String>
			{
				IReadOnlyCollection_1<System::String>(decltype(nullptr));
				IReadOnlyCollection_1<System::String>(Plugin::InternalUse, int32_t handle);
				IReadOnlyCollection_1<System::String>(const IReadOnlyCollection_1<System::String>& other);
				IReadOnlyCollection_1<System::String>(IReadOnlyCollection_1<System::String>&& other);
				virtual ~IReadOnlyCollection_1<System::String>();
				IReadOnlyCollection_1<System::String>& operator=(const IReadOnlyCollection_1<System::String>& other);
				IReadOnlyCollection_1<System::String>& operator=(decltype(nullptr));
				IReadOnlyCollection_1<System::String>& operator=(IReadOnlyCollection_1<System::String>&& other);
				bool operator==(const IReadOnlyCollection_1<System::String>& other) const;
				bool operator!=(const IReadOnlyCollection_1<System::String>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator
	{
		System::Collections::Generic::IEnumerator_1<System::String> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator(System::Collections::Generic::IReadOnlyCollection_1<System::String>& enumerable);
		~SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator();
		SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator& other);
		System::String operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator begin(System::Collections::Generic::IReadOnlyCollection_1<System::String>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyCollectionSystemStringIterator end(System::Collections::Generic::IReadOnlyCollection_1<System::String>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Object> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Object>
			{
				IReadOnlyList_1<UnityEngine::Object>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Object>(const IReadOnlyList_1<UnityEngine::Object>& other);
				IReadOnlyList_1<UnityEngine::Object>(IReadOnlyList_1<UnityEngine::Object>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Object>();
				IReadOnlyList_1<UnityEngine::Object>& operator=(const IReadOnlyList_1<UnityEngine::Object>& other);
				IReadOnlyList_1<UnityEngine::Object>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Object>& operator=(IReadOnlyList_1<UnityEngine::Object>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Object>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Object>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator& other);
		UnityEngine::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Object>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineObjectIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::GameObject> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::GameObject>
			{
				IReadOnlyList_1<UnityEngine::GameObject>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::GameObject>(const IReadOnlyList_1<UnityEngine::GameObject>& other);
				IReadOnlyList_1<UnityEngine::GameObject>(IReadOnlyList_1<UnityEngine::GameObject>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::GameObject>();
				IReadOnlyList_1<UnityEngine::GameObject>& operator=(const IReadOnlyList_1<UnityEngine::GameObject>& other);
				IReadOnlyList_1<UnityEngine::GameObject>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::GameObject>& operator=(IReadOnlyList_1<UnityEngine::GameObject>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::GameObject>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::GameObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::GameObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::GameObject>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator& other);
		UnityEngine::GameObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::GameObject>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineGameObjectIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::GameObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Transform> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Transform>
			{
				IReadOnlyList_1<UnityEngine::Transform>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Transform>(const IReadOnlyList_1<UnityEngine::Transform>& other);
				IReadOnlyList_1<UnityEngine::Transform>(IReadOnlyList_1<UnityEngine::Transform>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Transform>();
				IReadOnlyList_1<UnityEngine::Transform>& operator=(const IReadOnlyList_1<UnityEngine::Transform>& other);
				IReadOnlyList_1<UnityEngine::Transform>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Transform>& operator=(IReadOnlyList_1<UnityEngine::Transform>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Transform>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Transform>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Transform> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Transform>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator& other);
		UnityEngine::Transform operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Transform>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineTransformIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Transform>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::MeshFilter> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::MeshFilter>
			{
				IReadOnlyList_1<UnityEngine::MeshFilter>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::MeshFilter>(const IReadOnlyList_1<UnityEngine::MeshFilter>& other);
				IReadOnlyList_1<UnityEngine::MeshFilter>(IReadOnlyList_1<UnityEngine::MeshFilter>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::MeshFilter>();
				IReadOnlyList_1<UnityEngine::MeshFilter>& operator=(const IReadOnlyList_1<UnityEngine::MeshFilter>& other);
				IReadOnlyList_1<UnityEngine::MeshFilter>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::MeshFilter>& operator=(IReadOnlyList_1<UnityEngine::MeshFilter>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::MeshFilter>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::MeshFilter>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::MeshFilter> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::MeshFilter>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator& other);
		UnityEngine::MeshFilter operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::MeshFilter>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineMeshFilterIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::MeshFilter>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Material> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Material>
			{
				IReadOnlyList_1<UnityEngine::Material>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Material>(const IReadOnlyList_1<UnityEngine::Material>& other);
				IReadOnlyList_1<UnityEngine::Material>(IReadOnlyList_1<UnityEngine::Material>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Material>();
				IReadOnlyList_1<UnityEngine::Material>& operator=(const IReadOnlyList_1<UnityEngine::Material>& other);
				IReadOnlyList_1<UnityEngine::Material>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Material>& operator=(IReadOnlyList_1<UnityEngine::Material>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Material>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Material>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Material> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Material>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator& other);
		UnityEngine::Material operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Material>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineMaterialIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Material>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Texture2D> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Texture2D>
			{
				IReadOnlyList_1<UnityEngine::Texture2D>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Texture2D>(const IReadOnlyList_1<UnityEngine::Texture2D>& other);
				IReadOnlyList_1<UnityEngine::Texture2D>(IReadOnlyList_1<UnityEngine::Texture2D>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Texture2D>();
				IReadOnlyList_1<UnityEngine::Texture2D>& operator=(const IReadOnlyList_1<UnityEngine::Texture2D>& other);
				IReadOnlyList_1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Texture2D>& operator=(IReadOnlyList_1<UnityEngine::Texture2D>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Texture2D>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Texture2D>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Texture2D> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Texture2D>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator& other);
		UnityEngine::Texture2D operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Texture2D>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineTexture2DIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Texture2D>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Cubemap> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Cubemap>
			{
				IReadOnlyList_1<UnityEngine::Cubemap>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Cubemap>(const IReadOnlyList_1<UnityEngine::Cubemap>& other);
				IReadOnlyList_1<UnityEngine::Cubemap>(IReadOnlyList_1<UnityEngine::Cubemap>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Cubemap>();
				IReadOnlyList_1<UnityEngine::Cubemap>& operator=(const IReadOnlyList_1<UnityEngine::Cubemap>& other);
				IReadOnlyList_1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Cubemap>& operator=(IReadOnlyList_1<UnityEngine::Cubemap>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Cubemap>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Cubemap>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Cubemap> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Cubemap>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator& other);
		UnityEngine::Cubemap operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Cubemap>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineCubemapIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Cubemap>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Matrix4x4> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Matrix4x4>
			{
				IReadOnlyList_1<UnityEngine::Matrix4x4>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Matrix4x4>(const IReadOnlyList_1<UnityEngine::Matrix4x4>& other);
				IReadOnlyList_1<UnityEngine::Matrix4x4>(IReadOnlyList_1<UnityEngine::Matrix4x4>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Matrix4x4>();
				IReadOnlyList_1<UnityEngine::Matrix4x4>& operator=(const IReadOnlyList_1<UnityEngine::Matrix4x4>& other);
				IReadOnlyList_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Matrix4x4>& operator=(IReadOnlyList_1<UnityEngine::Matrix4x4>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Matrix4x4>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Matrix4x4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Matrix4x4> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Matrix4x4>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator& other);
		UnityEngine::Matrix4x4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Matrix4x4>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineMatrix4x4Iterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Matrix4x4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IPlMessageable>
			{
				IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>(const IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& other);
				IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>(IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>();
				IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& other);
				IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& operator=(IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IPlMessageable> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator& other);
		UPlasma::PlEmu::IPlMessageable operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIPlMessageableIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlConditionalObject>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>(const IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>(IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>();
				IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlConditionalObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator& other);
		UPlasma::PlEmu::PlConditionalObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlConditionalObjectIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlDetector> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlDetector>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlDetector>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlDetector>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlDetector>(const IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlDetector>(IReadOnlyList_1<UPlasma::PlEmu::PlDetector>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlDetector>();
				IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlDetector>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlDetector> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator& other);
		UPlasma::PlEmu::PlDetector operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlDetectorIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlDetector>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlMessage> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlMessage>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlMessage>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlMessage>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlMessage>(const IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlMessage>(IReadOnlyList_1<UPlasma::PlEmu::PlMessage>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlMessage>();
				IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlMessage>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlMessage> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator& other);
		UPlasma::PlEmu::PlMessage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlMessageIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlMessage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlOneShotCallback>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>(const IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>(IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>();
				IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator& other);
		UPlasma::PlEmu::PlOneShotCallback operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlOneShotCallbackIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(const IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>();
				IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator& other);
		UPlasma::PlEmu::PlRandomSoundModGroup operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlRandomSoundModGroupIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlPage> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlPage>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlPage>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlPage>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlPage>(const IReadOnlyList_1<UPlasma::PlEmu::PlPage>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlPage>(IReadOnlyList_1<UPlasma::PlEmu::PlPage>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlPage>();
				IReadOnlyList_1<UPlasma::PlEmu::PlPage>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlPage>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlPage>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlPage>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlPage>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlPage>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlPage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlPage> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlPage>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator& other);
		UPlasma::PlEmu::PlPage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlPage>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlPageIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlPage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PrpImportClues>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>(const IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>(IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>();
				IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PrpImportClues> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator& other);
		UPlasma::PlEmu::PrpImportClues operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPrpImportCluesIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlCluster> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlCluster>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlCluster>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlCluster>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlCluster>(const IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlCluster>(IReadOnlyList_1<UPlasma::PlEmu::PlCluster>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlCluster>();
				IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlCluster>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlCluster> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator& other);
		UPlasma::PlEmu::PlCluster operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlClusterIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlCluster>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::AgAnim> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::AgAnim>
			{
				IReadOnlyList_1<UPlasma::PlEmu::AgAnim>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::AgAnim>(const IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& other);
				IReadOnlyList_1<UPlasma::PlEmu::AgAnim>(IReadOnlyList_1<UPlasma::PlEmu::AgAnim>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::AgAnim>();
				IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& other);
				IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& operator=(IReadOnlyList_1<UPlasma::PlEmu::AgAnim>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::AgAnim> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator& other);
		UPlasma::PlEmu::AgAnim operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuAgAnimIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::AgAnim>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Color> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color>
			{
				IReadOnlyList_1<UnityEngine::Color>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Color>(const IReadOnlyList_1<UnityEngine::Color>& other);
				IReadOnlyList_1<UnityEngine::Color>(IReadOnlyList_1<UnityEngine::Color>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Color>();
				IReadOnlyList_1<UnityEngine::Color>& operator=(const IReadOnlyList_1<UnityEngine::Color>& other);
				IReadOnlyList_1<UnityEngine::Color>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Color>& operator=(IReadOnlyList_1<UnityEngine::Color>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Color>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Color>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator& other);
		UnityEngine::Color operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineColorIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Color32> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Color32>
			{
				IReadOnlyList_1<UnityEngine::Color32>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Color32>(const IReadOnlyList_1<UnityEngine::Color32>& other);
				IReadOnlyList_1<UnityEngine::Color32>(IReadOnlyList_1<UnityEngine::Color32>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Color32>();
				IReadOnlyList_1<UnityEngine::Color32>& operator=(const IReadOnlyList_1<UnityEngine::Color32>& other);
				IReadOnlyList_1<UnityEngine::Color32>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Color32>& operator=(IReadOnlyList_1<UnityEngine::Color32>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Color32>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Color32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color32> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color32>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator& other);
		UnityEngine::Color32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color32>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineColor32Iterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Vector2> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector2>
			{
				IReadOnlyList_1<UnityEngine::Vector2>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Vector2>(const IReadOnlyList_1<UnityEngine::Vector2>& other);
				IReadOnlyList_1<UnityEngine::Vector2>(IReadOnlyList_1<UnityEngine::Vector2>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Vector2>();
				IReadOnlyList_1<UnityEngine::Vector2>& operator=(const IReadOnlyList_1<UnityEngine::Vector2>& other);
				IReadOnlyList_1<UnityEngine::Vector2>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Vector2>& operator=(IReadOnlyList_1<UnityEngine::Vector2>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Vector2>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Vector2>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector2> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector2>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator& other);
		UnityEngine::Vector2 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector2>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineVector2Iterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector2>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Vector3> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector3>
			{
				IReadOnlyList_1<UnityEngine::Vector3>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Vector3>(const IReadOnlyList_1<UnityEngine::Vector3>& other);
				IReadOnlyList_1<UnityEngine::Vector3>(IReadOnlyList_1<UnityEngine::Vector3>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Vector3>();
				IReadOnlyList_1<UnityEngine::Vector3>& operator=(const IReadOnlyList_1<UnityEngine::Vector3>& other);
				IReadOnlyList_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Vector3>& operator=(IReadOnlyList_1<UnityEngine::Vector3>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Vector3>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Vector3>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector3> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector3>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator& other);
		UnityEngine::Vector3 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector3>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineVector3Iterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector3>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Vector4> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Vector4>
			{
				IReadOnlyList_1<UnityEngine::Vector4>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Vector4>(const IReadOnlyList_1<UnityEngine::Vector4>& other);
				IReadOnlyList_1<UnityEngine::Vector4>(IReadOnlyList_1<UnityEngine::Vector4>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Vector4>();
				IReadOnlyList_1<UnityEngine::Vector4>& operator=(const IReadOnlyList_1<UnityEngine::Vector4>& other);
				IReadOnlyList_1<UnityEngine::Vector4>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Vector4>& operator=(IReadOnlyList_1<UnityEngine::Vector4>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Vector4>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Vector4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector4> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector4>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator& other);
		UnityEngine::Vector4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector4>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineVector4Iterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::BoneWeight> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::BoneWeight>
			{
				IReadOnlyList_1<UnityEngine::BoneWeight>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::BoneWeight>(const IReadOnlyList_1<UnityEngine::BoneWeight>& other);
				IReadOnlyList_1<UnityEngine::BoneWeight>(IReadOnlyList_1<UnityEngine::BoneWeight>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::BoneWeight>();
				IReadOnlyList_1<UnityEngine::BoneWeight>& operator=(const IReadOnlyList_1<UnityEngine::BoneWeight>& other);
				IReadOnlyList_1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::BoneWeight>& operator=(IReadOnlyList_1<UnityEngine::BoneWeight>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::BoneWeight>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::BoneWeight>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::BoneWeight> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::BoneWeight>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator& other);
		UnityEngine::BoneWeight operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::BoneWeight>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineBoneWeightIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::BoneWeight>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UnityEngine::Quaternion> : virtual System::Collections::Generic::IReadOnlyCollection_1<UnityEngine::Quaternion>
			{
				IReadOnlyList_1<UnityEngine::Quaternion>(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UnityEngine::Quaternion>(const IReadOnlyList_1<UnityEngine::Quaternion>& other);
				IReadOnlyList_1<UnityEngine::Quaternion>(IReadOnlyList_1<UnityEngine::Quaternion>&& other);
				virtual ~IReadOnlyList_1<UnityEngine::Quaternion>();
				IReadOnlyList_1<UnityEngine::Quaternion>& operator=(const IReadOnlyList_1<UnityEngine::Quaternion>& other);
				IReadOnlyList_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
				IReadOnlyList_1<UnityEngine::Quaternion>& operator=(IReadOnlyList_1<UnityEngine::Quaternion>&& other);
				bool operator==(const IReadOnlyList_1<UnityEngine::Quaternion>& other) const;
				bool operator!=(const IReadOnlyList_1<UnityEngine::Quaternion>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Quaternion> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Quaternion>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator();
		SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator& other);
		UnityEngine::Quaternion operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator begin(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Quaternion>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUnityEngineQuaternionIterator end(System::Collections::Generic::IReadOnlyList_1<UnityEngine::Quaternion>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlResponderState> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderState>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>(IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>();
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderState> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator& other);
		UPlasma::PlEmu::PlResponderState operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderStateIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlResponderCommand>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>(IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>();
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderCommand> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator& other);
		UPlasma::PlEmu::PlResponderCommand operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlResponderCommandIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::PlSound> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::PlSound>
			{
				IReadOnlyList_1<UPlasma::PlEmu::PlSound>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::PlSound>(const IReadOnlyList_1<UPlasma::PlEmu::PlSound>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlSound>(IReadOnlyList_1<UPlasma::PlEmu::PlSound>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::PlSound>();
				IReadOnlyList_1<UPlasma::PlEmu::PlSound>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::PlSound>& other);
				IReadOnlyList_1<UPlasma::PlEmu::PlSound>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::PlSound>& operator=(IReadOnlyList_1<UPlasma::PlEmu::PlSound>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::PlSound>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::PlSound>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlSound> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlSound>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator& other);
		UPlasma::PlEmu::PlSound operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlSound>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuPlSoundIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlSound>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator> : virtual System::Collections::Generic::IReadOnlyCollection_1<UPlasma::PlEmu::IAgApplicator>
			{
				IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>(const IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& other);
				IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>(IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>&& other);
				virtual ~IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>();
				IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& operator=(const IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& other);
				IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& operator=(decltype(nullptr));
				IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& operator=(IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>&& other);
				bool operator==(const IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				bool operator!=(const IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IAgApplicator> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		~SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator();
		SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator& other);
		UPlasma::PlEmu::IAgApplicator operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator begin(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListUPlasmaPlEmuIAgApplicatorIterator end(System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Single> : virtual System::Collections::Generic::IReadOnlyCollection_1<System::Single>
			{
				IReadOnlyList_1<System::Single>(decltype(nullptr));
				IReadOnlyList_1<System::Single>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<System::Single>(const IReadOnlyList_1<System::Single>& other);
				IReadOnlyList_1<System::Single>(IReadOnlyList_1<System::Single>&& other);
				virtual ~IReadOnlyList_1<System::Single>();
				IReadOnlyList_1<System::Single>& operator=(const IReadOnlyList_1<System::Single>& other);
				IReadOnlyList_1<System::Single>& operator=(decltype(nullptr));
				IReadOnlyList_1<System::Single>& operator=(IReadOnlyList_1<System::Single>&& other);
				bool operator==(const IReadOnlyList_1<System::Single>& other) const;
				bool operator!=(const IReadOnlyList_1<System::Single>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListSystemSingleIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Single> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListSystemSingleIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListSystemSingleIterator(System::Collections::Generic::IReadOnlyList_1<System::Single>& enumerable);
		~SystemCollectionsGenericIReadOnlyListSystemSingleIterator();
		SystemCollectionsGenericIReadOnlyListSystemSingleIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListSystemSingleIterator& other);
		System::Single operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListSystemSingleIterator begin(System::Collections::Generic::IReadOnlyList_1<System::Single>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListSystemSingleIterator end(System::Collections::Generic::IReadOnlyList_1<System::Single>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Int32> : virtual System::Collections::Generic::IReadOnlyCollection_1<System::Int32>
			{
				IReadOnlyList_1<System::Int32>(decltype(nullptr));
				IReadOnlyList_1<System::Int32>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<System::Int32>(const IReadOnlyList_1<System::Int32>& other);
				IReadOnlyList_1<System::Int32>(IReadOnlyList_1<System::Int32>&& other);
				virtual ~IReadOnlyList_1<System::Int32>();
				IReadOnlyList_1<System::Int32>& operator=(const IReadOnlyList_1<System::Int32>& other);
				IReadOnlyList_1<System::Int32>& operator=(decltype(nullptr));
				IReadOnlyList_1<System::Int32>& operator=(IReadOnlyList_1<System::Int32>&& other);
				bool operator==(const IReadOnlyList_1<System::Int32>& other) const;
				bool operator!=(const IReadOnlyList_1<System::Int32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListSystemInt32Iterator
	{
		System::Collections::Generic::IEnumerator_1<System::Int32> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListSystemInt32Iterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListSystemInt32Iterator(System::Collections::Generic::IReadOnlyList_1<System::Int32>& enumerable);
		~SystemCollectionsGenericIReadOnlyListSystemInt32Iterator();
		SystemCollectionsGenericIReadOnlyListSystemInt32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListSystemInt32Iterator& other);
		System::Int32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListSystemInt32Iterator begin(System::Collections::Generic::IReadOnlyList_1<System::Int32>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListSystemInt32Iterator end(System::Collections::Generic::IReadOnlyList_1<System::Int32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Object> : virtual System::Collections::Generic::IReadOnlyCollection_1<System::Object>
			{
				IReadOnlyList_1<System::Object>(decltype(nullptr));
				IReadOnlyList_1<System::Object>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<System::Object>(const IReadOnlyList_1<System::Object>& other);
				IReadOnlyList_1<System::Object>(IReadOnlyList_1<System::Object>&& other);
				virtual ~IReadOnlyList_1<System::Object>();
				IReadOnlyList_1<System::Object>& operator=(const IReadOnlyList_1<System::Object>& other);
				IReadOnlyList_1<System::Object>& operator=(decltype(nullptr));
				IReadOnlyList_1<System::Object>& operator=(IReadOnlyList_1<System::Object>&& other);
				bool operator==(const IReadOnlyList_1<System::Object>& other) const;
				bool operator!=(const IReadOnlyList_1<System::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListSystemObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListSystemObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListSystemObjectIterator(System::Collections::Generic::IReadOnlyList_1<System::Object>& enumerable);
		~SystemCollectionsGenericIReadOnlyListSystemObjectIterator();
		SystemCollectionsGenericIReadOnlyListSystemObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListSystemObjectIterator& other);
		System::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListSystemObjectIterator begin(System::Collections::Generic::IReadOnlyList_1<System::Object>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListSystemObjectIterator end(System::Collections::Generic::IReadOnlyList_1<System::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::Byte> : virtual System::Collections::Generic::IReadOnlyCollection_1<System::Byte>
			{
				IReadOnlyList_1<System::Byte>(decltype(nullptr));
				IReadOnlyList_1<System::Byte>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<System::Byte>(const IReadOnlyList_1<System::Byte>& other);
				IReadOnlyList_1<System::Byte>(IReadOnlyList_1<System::Byte>&& other);
				virtual ~IReadOnlyList_1<System::Byte>();
				IReadOnlyList_1<System::Byte>& operator=(const IReadOnlyList_1<System::Byte>& other);
				IReadOnlyList_1<System::Byte>& operator=(decltype(nullptr));
				IReadOnlyList_1<System::Byte>& operator=(IReadOnlyList_1<System::Byte>&& other);
				bool operator==(const IReadOnlyList_1<System::Byte>& other) const;
				bool operator!=(const IReadOnlyList_1<System::Byte>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListSystemByteIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Byte> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListSystemByteIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListSystemByteIterator(System::Collections::Generic::IReadOnlyList_1<System::Byte>& enumerable);
		~SystemCollectionsGenericIReadOnlyListSystemByteIterator();
		SystemCollectionsGenericIReadOnlyListSystemByteIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListSystemByteIterator& other);
		System::Byte operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListSystemByteIterator begin(System::Collections::Generic::IReadOnlyList_1<System::Byte>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListSystemByteIterator end(System::Collections::Generic::IReadOnlyList_1<System::Byte>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IReadOnlyList_1<System::String> : virtual System::Collections::Generic::IReadOnlyCollection_1<System::String>
			{
				IReadOnlyList_1<System::String>(decltype(nullptr));
				IReadOnlyList_1<System::String>(Plugin::InternalUse, int32_t handle);
				IReadOnlyList_1<System::String>(const IReadOnlyList_1<System::String>& other);
				IReadOnlyList_1<System::String>(IReadOnlyList_1<System::String>&& other);
				virtual ~IReadOnlyList_1<System::String>();
				IReadOnlyList_1<System::String>& operator=(const IReadOnlyList_1<System::String>& other);
				IReadOnlyList_1<System::String>& operator=(decltype(nullptr));
				IReadOnlyList_1<System::String>& operator=(IReadOnlyList_1<System::String>&& other);
				bool operator==(const IReadOnlyList_1<System::String>& other) const;
				bool operator!=(const IReadOnlyList_1<System::String>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIReadOnlyListSystemStringIterator
	{
		System::Collections::Generic::IEnumerator_1<System::String> enumerator;
		bool hasMore;
		SystemCollectionsGenericIReadOnlyListSystemStringIterator(decltype(nullptr));
		SystemCollectionsGenericIReadOnlyListSystemStringIterator(System::Collections::Generic::IReadOnlyList_1<System::String>& enumerable);
		~SystemCollectionsGenericIReadOnlyListSystemStringIterator();
		SystemCollectionsGenericIReadOnlyListSystemStringIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIReadOnlyListSystemStringIterator& other);
		System::String operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIReadOnlyListSystemStringIterator begin(System::Collections::Generic::IReadOnlyList_1<System::String>& enumerable);
			Plugin::SystemCollectionsGenericIReadOnlyListSystemStringIterator end(System::Collections::Generic::IReadOnlyList_1<System::String>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Object> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Object>
			{
				IList_1<UnityEngine::Object>(decltype(nullptr));
				IList_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Object>(const IList_1<UnityEngine::Object>& other);
				IList_1<UnityEngine::Object>(IList_1<UnityEngine::Object>&& other);
				virtual ~IList_1<UnityEngine::Object>();
				IList_1<UnityEngine::Object>& operator=(const IList_1<UnityEngine::Object>& other);
				IList_1<UnityEngine::Object>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Object>& operator=(IList_1<UnityEngine::Object>&& other);
				bool operator==(const IList_1<UnityEngine::Object>& other) const;
				bool operator!=(const IList_1<UnityEngine::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineObjectIterator(System::Collections::Generic::IList_1<UnityEngine::Object>& enumerable);
		~SystemCollectionsGenericIListUnityEngineObjectIterator();
		SystemCollectionsGenericIListUnityEngineObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineObjectIterator& other);
		UnityEngine::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineObjectIterator begin(System::Collections::Generic::IList_1<UnityEngine::Object>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineObjectIterator end(System::Collections::Generic::IList_1<UnityEngine::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::GameObject> : virtual System::Collections::Generic::ICollection_1<UnityEngine::GameObject>
			{
				IList_1<UnityEngine::GameObject>(decltype(nullptr));
				IList_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::GameObject>(const IList_1<UnityEngine::GameObject>& other);
				IList_1<UnityEngine::GameObject>(IList_1<UnityEngine::GameObject>&& other);
				virtual ~IList_1<UnityEngine::GameObject>();
				IList_1<UnityEngine::GameObject>& operator=(const IList_1<UnityEngine::GameObject>& other);
				IList_1<UnityEngine::GameObject>& operator=(decltype(nullptr));
				IList_1<UnityEngine::GameObject>& operator=(IList_1<UnityEngine::GameObject>&& other);
				bool operator==(const IList_1<UnityEngine::GameObject>& other) const;
				bool operator!=(const IList_1<UnityEngine::GameObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineGameObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::GameObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineGameObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineGameObjectIterator(System::Collections::Generic::IList_1<UnityEngine::GameObject>& enumerable);
		~SystemCollectionsGenericIListUnityEngineGameObjectIterator();
		SystemCollectionsGenericIListUnityEngineGameObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineGameObjectIterator& other);
		UnityEngine::GameObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineGameObjectIterator begin(System::Collections::Generic::IList_1<UnityEngine::GameObject>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineGameObjectIterator end(System::Collections::Generic::IList_1<UnityEngine::GameObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Transform> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Transform>
			{
				IList_1<UnityEngine::Transform>(decltype(nullptr));
				IList_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Transform>(const IList_1<UnityEngine::Transform>& other);
				IList_1<UnityEngine::Transform>(IList_1<UnityEngine::Transform>&& other);
				virtual ~IList_1<UnityEngine::Transform>();
				IList_1<UnityEngine::Transform>& operator=(const IList_1<UnityEngine::Transform>& other);
				IList_1<UnityEngine::Transform>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Transform>& operator=(IList_1<UnityEngine::Transform>&& other);
				bool operator==(const IList_1<UnityEngine::Transform>& other) const;
				bool operator!=(const IList_1<UnityEngine::Transform>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineTransformIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Transform> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineTransformIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineTransformIterator(System::Collections::Generic::IList_1<UnityEngine::Transform>& enumerable);
		~SystemCollectionsGenericIListUnityEngineTransformIterator();
		SystemCollectionsGenericIListUnityEngineTransformIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineTransformIterator& other);
		UnityEngine::Transform operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineTransformIterator begin(System::Collections::Generic::IList_1<UnityEngine::Transform>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineTransformIterator end(System::Collections::Generic::IList_1<UnityEngine::Transform>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::MeshFilter> : virtual System::Collections::Generic::ICollection_1<UnityEngine::MeshFilter>
			{
				IList_1<UnityEngine::MeshFilter>(decltype(nullptr));
				IList_1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::MeshFilter>(const IList_1<UnityEngine::MeshFilter>& other);
				IList_1<UnityEngine::MeshFilter>(IList_1<UnityEngine::MeshFilter>&& other);
				virtual ~IList_1<UnityEngine::MeshFilter>();
				IList_1<UnityEngine::MeshFilter>& operator=(const IList_1<UnityEngine::MeshFilter>& other);
				IList_1<UnityEngine::MeshFilter>& operator=(decltype(nullptr));
				IList_1<UnityEngine::MeshFilter>& operator=(IList_1<UnityEngine::MeshFilter>&& other);
				bool operator==(const IList_1<UnityEngine::MeshFilter>& other) const;
				bool operator!=(const IList_1<UnityEngine::MeshFilter>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineMeshFilterIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::MeshFilter> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineMeshFilterIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineMeshFilterIterator(System::Collections::Generic::IList_1<UnityEngine::MeshFilter>& enumerable);
		~SystemCollectionsGenericIListUnityEngineMeshFilterIterator();
		SystemCollectionsGenericIListUnityEngineMeshFilterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineMeshFilterIterator& other);
		UnityEngine::MeshFilter operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineMeshFilterIterator begin(System::Collections::Generic::IList_1<UnityEngine::MeshFilter>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineMeshFilterIterator end(System::Collections::Generic::IList_1<UnityEngine::MeshFilter>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Material> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Material>
			{
				IList_1<UnityEngine::Material>(decltype(nullptr));
				IList_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Material>(const IList_1<UnityEngine::Material>& other);
				IList_1<UnityEngine::Material>(IList_1<UnityEngine::Material>&& other);
				virtual ~IList_1<UnityEngine::Material>();
				IList_1<UnityEngine::Material>& operator=(const IList_1<UnityEngine::Material>& other);
				IList_1<UnityEngine::Material>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Material>& operator=(IList_1<UnityEngine::Material>&& other);
				bool operator==(const IList_1<UnityEngine::Material>& other) const;
				bool operator!=(const IList_1<UnityEngine::Material>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineMaterialIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Material> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineMaterialIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineMaterialIterator(System::Collections::Generic::IList_1<UnityEngine::Material>& enumerable);
		~SystemCollectionsGenericIListUnityEngineMaterialIterator();
		SystemCollectionsGenericIListUnityEngineMaterialIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineMaterialIterator& other);
		UnityEngine::Material operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineMaterialIterator begin(System::Collections::Generic::IList_1<UnityEngine::Material>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineMaterialIterator end(System::Collections::Generic::IList_1<UnityEngine::Material>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Texture2D> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Texture2D>
			{
				IList_1<UnityEngine::Texture2D>(decltype(nullptr));
				IList_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Texture2D>(const IList_1<UnityEngine::Texture2D>& other);
				IList_1<UnityEngine::Texture2D>(IList_1<UnityEngine::Texture2D>&& other);
				virtual ~IList_1<UnityEngine::Texture2D>();
				IList_1<UnityEngine::Texture2D>& operator=(const IList_1<UnityEngine::Texture2D>& other);
				IList_1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Texture2D>& operator=(IList_1<UnityEngine::Texture2D>&& other);
				bool operator==(const IList_1<UnityEngine::Texture2D>& other) const;
				bool operator!=(const IList_1<UnityEngine::Texture2D>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineTexture2DIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Texture2D> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineTexture2DIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineTexture2DIterator(System::Collections::Generic::IList_1<UnityEngine::Texture2D>& enumerable);
		~SystemCollectionsGenericIListUnityEngineTexture2DIterator();
		SystemCollectionsGenericIListUnityEngineTexture2DIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineTexture2DIterator& other);
		UnityEngine::Texture2D operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineTexture2DIterator begin(System::Collections::Generic::IList_1<UnityEngine::Texture2D>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineTexture2DIterator end(System::Collections::Generic::IList_1<UnityEngine::Texture2D>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Cubemap> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Cubemap>
			{
				IList_1<UnityEngine::Cubemap>(decltype(nullptr));
				IList_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Cubemap>(const IList_1<UnityEngine::Cubemap>& other);
				IList_1<UnityEngine::Cubemap>(IList_1<UnityEngine::Cubemap>&& other);
				virtual ~IList_1<UnityEngine::Cubemap>();
				IList_1<UnityEngine::Cubemap>& operator=(const IList_1<UnityEngine::Cubemap>& other);
				IList_1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Cubemap>& operator=(IList_1<UnityEngine::Cubemap>&& other);
				bool operator==(const IList_1<UnityEngine::Cubemap>& other) const;
				bool operator!=(const IList_1<UnityEngine::Cubemap>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineCubemapIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Cubemap> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineCubemapIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineCubemapIterator(System::Collections::Generic::IList_1<UnityEngine::Cubemap>& enumerable);
		~SystemCollectionsGenericIListUnityEngineCubemapIterator();
		SystemCollectionsGenericIListUnityEngineCubemapIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineCubemapIterator& other);
		UnityEngine::Cubemap operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineCubemapIterator begin(System::Collections::Generic::IList_1<UnityEngine::Cubemap>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineCubemapIterator end(System::Collections::Generic::IList_1<UnityEngine::Cubemap>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Matrix4x4> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Matrix4x4>
			{
				IList_1<UnityEngine::Matrix4x4>(decltype(nullptr));
				IList_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Matrix4x4>(const IList_1<UnityEngine::Matrix4x4>& other);
				IList_1<UnityEngine::Matrix4x4>(IList_1<UnityEngine::Matrix4x4>&& other);
				virtual ~IList_1<UnityEngine::Matrix4x4>();
				IList_1<UnityEngine::Matrix4x4>& operator=(const IList_1<UnityEngine::Matrix4x4>& other);
				IList_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Matrix4x4>& operator=(IList_1<UnityEngine::Matrix4x4>&& other);
				bool operator==(const IList_1<UnityEngine::Matrix4x4>& other) const;
				bool operator!=(const IList_1<UnityEngine::Matrix4x4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Matrix4x4> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator(System::Collections::Generic::IList_1<UnityEngine::Matrix4x4>& enumerable);
		~SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator();
		SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator& other);
		UnityEngine::Matrix4x4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator begin(System::Collections::Generic::IList_1<UnityEngine::Matrix4x4>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineMatrix4x4Iterator end(System::Collections::Generic::IList_1<UnityEngine::Matrix4x4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::IPlMessageable> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IPlMessageable>
			{
				IList_1<UPlasma::PlEmu::IPlMessageable>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::IPlMessageable>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::IPlMessageable>(const IList_1<UPlasma::PlEmu::IPlMessageable>& other);
				IList_1<UPlasma::PlEmu::IPlMessageable>(IList_1<UPlasma::PlEmu::IPlMessageable>&& other);
				virtual ~IList_1<UPlasma::PlEmu::IPlMessageable>();
				IList_1<UPlasma::PlEmu::IPlMessageable>& operator=(const IList_1<UPlasma::PlEmu::IPlMessageable>& other);
				IList_1<UPlasma::PlEmu::IPlMessageable>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::IPlMessageable>& operator=(IList_1<UPlasma::PlEmu::IPlMessageable>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::IPlMessageable>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IPlMessageable> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator& other);
		UPlasma::PlEmu::IPlMessageable operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuIPlMessageableIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlConditionalObject> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlConditionalObject>
			{
				IList_1<UPlasma::PlEmu::PlConditionalObject>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlConditionalObject>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlConditionalObject>(const IList_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IList_1<UPlasma::PlEmu::PlConditionalObject>(IList_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlConditionalObject>();
				IList_1<UPlasma::PlEmu::PlConditionalObject>& operator=(const IList_1<UPlasma::PlEmu::PlConditionalObject>& other);
				IList_1<UPlasma::PlEmu::PlConditionalObject>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlConditionalObject>& operator=(IList_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlConditionalObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator& other);
		UPlasma::PlEmu::PlConditionalObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlConditionalObjectIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlDetector> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlDetector>
			{
				IList_1<UPlasma::PlEmu::PlDetector>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlDetector>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlDetector>(const IList_1<UPlasma::PlEmu::PlDetector>& other);
				IList_1<UPlasma::PlEmu::PlDetector>(IList_1<UPlasma::PlEmu::PlDetector>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlDetector>();
				IList_1<UPlasma::PlEmu::PlDetector>& operator=(const IList_1<UPlasma::PlEmu::PlDetector>& other);
				IList_1<UPlasma::PlEmu::PlDetector>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlDetector>& operator=(IList_1<UPlasma::PlEmu::PlDetector>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlDetector>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlDetector>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlDetector> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlDetector>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator& other);
		UPlasma::PlEmu::PlDetector operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlDetector>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlDetectorIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlDetector>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlMessage> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlMessage>
			{
				IList_1<UPlasma::PlEmu::PlMessage>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlMessage>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlMessage>(const IList_1<UPlasma::PlEmu::PlMessage>& other);
				IList_1<UPlasma::PlEmu::PlMessage>(IList_1<UPlasma::PlEmu::PlMessage>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlMessage>();
				IList_1<UPlasma::PlEmu::PlMessage>& operator=(const IList_1<UPlasma::PlEmu::PlMessage>& other);
				IList_1<UPlasma::PlEmu::PlMessage>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlMessage>& operator=(IList_1<UPlasma::PlEmu::PlMessage>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlMessage>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlMessage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlMessage> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlMessage>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator& other);
		UPlasma::PlEmu::PlMessage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlMessage>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlMessageIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlMessage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlOneShotCallback> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlOneShotCallback>
			{
				IList_1<UPlasma::PlEmu::PlOneShotCallback>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlOneShotCallback>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlOneShotCallback>(const IList_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IList_1<UPlasma::PlEmu::PlOneShotCallback>(IList_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlOneShotCallback>();
				IList_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(const IList_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				IList_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(IList_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator& other);
		UPlasma::PlEmu::PlOneShotCallback operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlOneShotCallbackIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlRandomSoundModGroup> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlRandomSoundModGroup>
			{
				IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(const IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>(IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>();
				IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(const IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator& other);
		UPlasma::PlEmu::PlRandomSoundModGroup operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlRandomSoundModGroupIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlPage> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlPage>
			{
				IList_1<UPlasma::PlEmu::PlPage>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlPage>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlPage>(const IList_1<UPlasma::PlEmu::PlPage>& other);
				IList_1<UPlasma::PlEmu::PlPage>(IList_1<UPlasma::PlEmu::PlPage>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlPage>();
				IList_1<UPlasma::PlEmu::PlPage>& operator=(const IList_1<UPlasma::PlEmu::PlPage>& other);
				IList_1<UPlasma::PlEmu::PlPage>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlPage>& operator=(IList_1<UPlasma::PlEmu::PlPage>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlPage>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlPage>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlPage> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlPage>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator& other);
		UPlasma::PlEmu::PlPage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlPage>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlPageIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlPage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PrpImportClues> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PrpImportClues>
			{
				IList_1<UPlasma::PlEmu::PrpImportClues>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PrpImportClues>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PrpImportClues>(const IList_1<UPlasma::PlEmu::PrpImportClues>& other);
				IList_1<UPlasma::PlEmu::PrpImportClues>(IList_1<UPlasma::PlEmu::PrpImportClues>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PrpImportClues>();
				IList_1<UPlasma::PlEmu::PrpImportClues>& operator=(const IList_1<UPlasma::PlEmu::PrpImportClues>& other);
				IList_1<UPlasma::PlEmu::PrpImportClues>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PrpImportClues>& operator=(IList_1<UPlasma::PlEmu::PrpImportClues>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PrpImportClues>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PrpImportClues> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator& other);
		UPlasma::PlEmu::PrpImportClues operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPrpImportCluesIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlCluster> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlCluster>
			{
				IList_1<UPlasma::PlEmu::PlCluster>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlCluster>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlCluster>(const IList_1<UPlasma::PlEmu::PlCluster>& other);
				IList_1<UPlasma::PlEmu::PlCluster>(IList_1<UPlasma::PlEmu::PlCluster>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlCluster>();
				IList_1<UPlasma::PlEmu::PlCluster>& operator=(const IList_1<UPlasma::PlEmu::PlCluster>& other);
				IList_1<UPlasma::PlEmu::PlCluster>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlCluster>& operator=(IList_1<UPlasma::PlEmu::PlCluster>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlCluster>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlCluster>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlCluster> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlCluster>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator& other);
		UPlasma::PlEmu::PlCluster operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlCluster>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlClusterIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlCluster>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::AgAnim> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::AgAnim>
			{
				IList_1<UPlasma::PlEmu::AgAnim>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::AgAnim>(const IList_1<UPlasma::PlEmu::AgAnim>& other);
				IList_1<UPlasma::PlEmu::AgAnim>(IList_1<UPlasma::PlEmu::AgAnim>&& other);
				virtual ~IList_1<UPlasma::PlEmu::AgAnim>();
				IList_1<UPlasma::PlEmu::AgAnim>& operator=(const IList_1<UPlasma::PlEmu::AgAnim>& other);
				IList_1<UPlasma::PlEmu::AgAnim>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::AgAnim>& operator=(IList_1<UPlasma::PlEmu::AgAnim>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::AgAnim>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::AgAnim>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::AgAnim> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::AgAnim>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator& other);
		UPlasma::PlEmu::AgAnim operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::AgAnim>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuAgAnimIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::AgAnim>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Color> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Color>
			{
				IList_1<UnityEngine::Color>(decltype(nullptr));
				IList_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Color>(const IList_1<UnityEngine::Color>& other);
				IList_1<UnityEngine::Color>(IList_1<UnityEngine::Color>&& other);
				virtual ~IList_1<UnityEngine::Color>();
				IList_1<UnityEngine::Color>& operator=(const IList_1<UnityEngine::Color>& other);
				IList_1<UnityEngine::Color>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Color>& operator=(IList_1<UnityEngine::Color>&& other);
				bool operator==(const IList_1<UnityEngine::Color>& other) const;
				bool operator!=(const IList_1<UnityEngine::Color>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineColorIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineColorIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineColorIterator(System::Collections::Generic::IList_1<UnityEngine::Color>& enumerable);
		~SystemCollectionsGenericIListUnityEngineColorIterator();
		SystemCollectionsGenericIListUnityEngineColorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineColorIterator& other);
		UnityEngine::Color operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineColorIterator begin(System::Collections::Generic::IList_1<UnityEngine::Color>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineColorIterator end(System::Collections::Generic::IList_1<UnityEngine::Color>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Color32> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Color32>
			{
				IList_1<UnityEngine::Color32>(decltype(nullptr));
				IList_1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Color32>(const IList_1<UnityEngine::Color32>& other);
				IList_1<UnityEngine::Color32>(IList_1<UnityEngine::Color32>&& other);
				virtual ~IList_1<UnityEngine::Color32>();
				IList_1<UnityEngine::Color32>& operator=(const IList_1<UnityEngine::Color32>& other);
				IList_1<UnityEngine::Color32>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Color32>& operator=(IList_1<UnityEngine::Color32>&& other);
				bool operator==(const IList_1<UnityEngine::Color32>& other) const;
				bool operator!=(const IList_1<UnityEngine::Color32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineColor32Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Color32> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineColor32Iterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineColor32Iterator(System::Collections::Generic::IList_1<UnityEngine::Color32>& enumerable);
		~SystemCollectionsGenericIListUnityEngineColor32Iterator();
		SystemCollectionsGenericIListUnityEngineColor32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineColor32Iterator& other);
		UnityEngine::Color32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineColor32Iterator begin(System::Collections::Generic::IList_1<UnityEngine::Color32>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineColor32Iterator end(System::Collections::Generic::IList_1<UnityEngine::Color32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Vector2> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Vector2>
			{
				IList_1<UnityEngine::Vector2>(decltype(nullptr));
				IList_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Vector2>(const IList_1<UnityEngine::Vector2>& other);
				IList_1<UnityEngine::Vector2>(IList_1<UnityEngine::Vector2>&& other);
				virtual ~IList_1<UnityEngine::Vector2>();
				IList_1<UnityEngine::Vector2>& operator=(const IList_1<UnityEngine::Vector2>& other);
				IList_1<UnityEngine::Vector2>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Vector2>& operator=(IList_1<UnityEngine::Vector2>&& other);
				bool operator==(const IList_1<UnityEngine::Vector2>& other) const;
				bool operator!=(const IList_1<UnityEngine::Vector2>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineVector2Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector2> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineVector2Iterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineVector2Iterator(System::Collections::Generic::IList_1<UnityEngine::Vector2>& enumerable);
		~SystemCollectionsGenericIListUnityEngineVector2Iterator();
		SystemCollectionsGenericIListUnityEngineVector2Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineVector2Iterator& other);
		UnityEngine::Vector2 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineVector2Iterator begin(System::Collections::Generic::IList_1<UnityEngine::Vector2>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineVector2Iterator end(System::Collections::Generic::IList_1<UnityEngine::Vector2>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Vector3> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Vector3>
			{
				IList_1<UnityEngine::Vector3>(decltype(nullptr));
				IList_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Vector3>(const IList_1<UnityEngine::Vector3>& other);
				IList_1<UnityEngine::Vector3>(IList_1<UnityEngine::Vector3>&& other);
				virtual ~IList_1<UnityEngine::Vector3>();
				IList_1<UnityEngine::Vector3>& operator=(const IList_1<UnityEngine::Vector3>& other);
				IList_1<UnityEngine::Vector3>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Vector3>& operator=(IList_1<UnityEngine::Vector3>&& other);
				bool operator==(const IList_1<UnityEngine::Vector3>& other) const;
				bool operator!=(const IList_1<UnityEngine::Vector3>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineVector3Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector3> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineVector3Iterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineVector3Iterator(System::Collections::Generic::IList_1<UnityEngine::Vector3>& enumerable);
		~SystemCollectionsGenericIListUnityEngineVector3Iterator();
		SystemCollectionsGenericIListUnityEngineVector3Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineVector3Iterator& other);
		UnityEngine::Vector3 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineVector3Iterator begin(System::Collections::Generic::IList_1<UnityEngine::Vector3>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineVector3Iterator end(System::Collections::Generic::IList_1<UnityEngine::Vector3>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Vector4> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Vector4>
			{
				IList_1<UnityEngine::Vector4>(decltype(nullptr));
				IList_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Vector4>(const IList_1<UnityEngine::Vector4>& other);
				IList_1<UnityEngine::Vector4>(IList_1<UnityEngine::Vector4>&& other);
				virtual ~IList_1<UnityEngine::Vector4>();
				IList_1<UnityEngine::Vector4>& operator=(const IList_1<UnityEngine::Vector4>& other);
				IList_1<UnityEngine::Vector4>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Vector4>& operator=(IList_1<UnityEngine::Vector4>&& other);
				bool operator==(const IList_1<UnityEngine::Vector4>& other) const;
				bool operator!=(const IList_1<UnityEngine::Vector4>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineVector4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Vector4> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineVector4Iterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineVector4Iterator(System::Collections::Generic::IList_1<UnityEngine::Vector4>& enumerable);
		~SystemCollectionsGenericIListUnityEngineVector4Iterator();
		SystemCollectionsGenericIListUnityEngineVector4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineVector4Iterator& other);
		UnityEngine::Vector4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineVector4Iterator begin(System::Collections::Generic::IList_1<UnityEngine::Vector4>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineVector4Iterator end(System::Collections::Generic::IList_1<UnityEngine::Vector4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::BoneWeight> : virtual System::Collections::Generic::ICollection_1<UnityEngine::BoneWeight>
			{
				IList_1<UnityEngine::BoneWeight>(decltype(nullptr));
				IList_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::BoneWeight>(const IList_1<UnityEngine::BoneWeight>& other);
				IList_1<UnityEngine::BoneWeight>(IList_1<UnityEngine::BoneWeight>&& other);
				virtual ~IList_1<UnityEngine::BoneWeight>();
				IList_1<UnityEngine::BoneWeight>& operator=(const IList_1<UnityEngine::BoneWeight>& other);
				IList_1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
				IList_1<UnityEngine::BoneWeight>& operator=(IList_1<UnityEngine::BoneWeight>&& other);
				bool operator==(const IList_1<UnityEngine::BoneWeight>& other) const;
				bool operator!=(const IList_1<UnityEngine::BoneWeight>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineBoneWeightIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::BoneWeight> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineBoneWeightIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineBoneWeightIterator(System::Collections::Generic::IList_1<UnityEngine::BoneWeight>& enumerable);
		~SystemCollectionsGenericIListUnityEngineBoneWeightIterator();
		SystemCollectionsGenericIListUnityEngineBoneWeightIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineBoneWeightIterator& other);
		UnityEngine::BoneWeight operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineBoneWeightIterator begin(System::Collections::Generic::IList_1<UnityEngine::BoneWeight>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineBoneWeightIterator end(System::Collections::Generic::IList_1<UnityEngine::BoneWeight>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UnityEngine::Quaternion> : virtual System::Collections::Generic::ICollection_1<UnityEngine::Quaternion>
			{
				IList_1<UnityEngine::Quaternion>(decltype(nullptr));
				IList_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
				IList_1<UnityEngine::Quaternion>(const IList_1<UnityEngine::Quaternion>& other);
				IList_1<UnityEngine::Quaternion>(IList_1<UnityEngine::Quaternion>&& other);
				virtual ~IList_1<UnityEngine::Quaternion>();
				IList_1<UnityEngine::Quaternion>& operator=(const IList_1<UnityEngine::Quaternion>& other);
				IList_1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
				IList_1<UnityEngine::Quaternion>& operator=(IList_1<UnityEngine::Quaternion>&& other);
				bool operator==(const IList_1<UnityEngine::Quaternion>& other) const;
				bool operator!=(const IList_1<UnityEngine::Quaternion>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUnityEngineQuaternionIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Quaternion> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUnityEngineQuaternionIterator(decltype(nullptr));
		SystemCollectionsGenericIListUnityEngineQuaternionIterator(System::Collections::Generic::IList_1<UnityEngine::Quaternion>& enumerable);
		~SystemCollectionsGenericIListUnityEngineQuaternionIterator();
		SystemCollectionsGenericIListUnityEngineQuaternionIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUnityEngineQuaternionIterator& other);
		UnityEngine::Quaternion operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUnityEngineQuaternionIterator begin(System::Collections::Generic::IList_1<UnityEngine::Quaternion>& enumerable);
			Plugin::SystemCollectionsGenericIListUnityEngineQuaternionIterator end(System::Collections::Generic::IList_1<UnityEngine::Quaternion>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlResponderState> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderState>
			{
				IList_1<UPlasma::PlEmu::PlResponderState>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlResponderState>(const IList_1<UPlasma::PlEmu::PlResponderState>& other);
				IList_1<UPlasma::PlEmu::PlResponderState>(IList_1<UPlasma::PlEmu::PlResponderState>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlResponderState>();
				IList_1<UPlasma::PlEmu::PlResponderState>& operator=(const IList_1<UPlasma::PlEmu::PlResponderState>& other);
				IList_1<UPlasma::PlEmu::PlResponderState>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlResponderState>& operator=(IList_1<UPlasma::PlEmu::PlResponderState>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlResponderState>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlResponderState>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderState> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator& other);
		UPlasma::PlEmu::PlResponderState operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderState>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlResponderStateIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderState>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlResponderCommand> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlResponderCommand>
			{
				IList_1<UPlasma::PlEmu::PlResponderCommand>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlResponderCommand>(const IList_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IList_1<UPlasma::PlEmu::PlResponderCommand>(IList_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlResponderCommand>();
				IList_1<UPlasma::PlEmu::PlResponderCommand>& operator=(const IList_1<UPlasma::PlEmu::PlResponderCommand>& other);
				IList_1<UPlasma::PlEmu::PlResponderCommand>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlResponderCommand>& operator=(IList_1<UPlasma::PlEmu::PlResponderCommand>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlResponderCommand>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlResponderCommand> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator& other);
		UPlasma::PlEmu::PlResponderCommand operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlResponderCommandIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderCommand>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::PlSound> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::PlSound>
			{
				IList_1<UPlasma::PlEmu::PlSound>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::PlSound>(const IList_1<UPlasma::PlEmu::PlSound>& other);
				IList_1<UPlasma::PlEmu::PlSound>(IList_1<UPlasma::PlEmu::PlSound>&& other);
				virtual ~IList_1<UPlasma::PlEmu::PlSound>();
				IList_1<UPlasma::PlEmu::PlSound>& operator=(const IList_1<UPlasma::PlEmu::PlSound>& other);
				IList_1<UPlasma::PlEmu::PlSound>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::PlSound>& operator=(IList_1<UPlasma::PlEmu::PlSound>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::PlSound>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::PlSound>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlSound> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlSound>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator& other);
		UPlasma::PlEmu::PlSound operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlSound>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuPlSoundIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::PlSound>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<UPlasma::PlEmu::IAgApplicator> : virtual System::Collections::Generic::ICollection_1<UPlasma::PlEmu::IAgApplicator>
			{
				IList_1<UPlasma::PlEmu::IAgApplicator>(decltype(nullptr));
				IList_1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle);
				IList_1<UPlasma::PlEmu::IAgApplicator>(const IList_1<UPlasma::PlEmu::IAgApplicator>& other);
				IList_1<UPlasma::PlEmu::IAgApplicator>(IList_1<UPlasma::PlEmu::IAgApplicator>&& other);
				virtual ~IList_1<UPlasma::PlEmu::IAgApplicator>();
				IList_1<UPlasma::PlEmu::IAgApplicator>& operator=(const IList_1<UPlasma::PlEmu::IAgApplicator>& other);
				IList_1<UPlasma::PlEmu::IAgApplicator>& operator=(decltype(nullptr));
				IList_1<UPlasma::PlEmu::IAgApplicator>& operator=(IList_1<UPlasma::PlEmu::IAgApplicator>&& other);
				bool operator==(const IList_1<UPlasma::PlEmu::IAgApplicator>& other) const;
				bool operator!=(const IList_1<UPlasma::PlEmu::IAgApplicator>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IAgApplicator> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator(decltype(nullptr));
		SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator(System::Collections::Generic::IList_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		~SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator();
		SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator& other);
		UPlasma::PlEmu::IAgApplicator operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator begin(System::Collections::Generic::IList_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
			Plugin::SystemCollectionsGenericIListUPlasmaPlEmuIAgApplicatorIterator end(System::Collections::Generic::IList_1<UPlasma::PlEmu::IAgApplicator>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Single> : virtual System::Collections::Generic::ICollection_1<System::Single>
			{
				IList_1<System::Single>(decltype(nullptr));
				IList_1<System::Single>(Plugin::InternalUse, int32_t handle);
				IList_1<System::Single>(const IList_1<System::Single>& other);
				IList_1<System::Single>(IList_1<System::Single>&& other);
				virtual ~IList_1<System::Single>();
				IList_1<System::Single>& operator=(const IList_1<System::Single>& other);
				IList_1<System::Single>& operator=(decltype(nullptr));
				IList_1<System::Single>& operator=(IList_1<System::Single>&& other);
				bool operator==(const IList_1<System::Single>& other) const;
				bool operator!=(const IList_1<System::Single>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListSystemSingleIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Single> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListSystemSingleIterator(decltype(nullptr));
		SystemCollectionsGenericIListSystemSingleIterator(System::Collections::Generic::IList_1<System::Single>& enumerable);
		~SystemCollectionsGenericIListSystemSingleIterator();
		SystemCollectionsGenericIListSystemSingleIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListSystemSingleIterator& other);
		System::Single operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListSystemSingleIterator begin(System::Collections::Generic::IList_1<System::Single>& enumerable);
			Plugin::SystemCollectionsGenericIListSystemSingleIterator end(System::Collections::Generic::IList_1<System::Single>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Int32> : virtual System::Collections::Generic::ICollection_1<System::Int32>
			{
				IList_1<System::Int32>(decltype(nullptr));
				IList_1<System::Int32>(Plugin::InternalUse, int32_t handle);
				IList_1<System::Int32>(const IList_1<System::Int32>& other);
				IList_1<System::Int32>(IList_1<System::Int32>&& other);
				virtual ~IList_1<System::Int32>();
				IList_1<System::Int32>& operator=(const IList_1<System::Int32>& other);
				IList_1<System::Int32>& operator=(decltype(nullptr));
				IList_1<System::Int32>& operator=(IList_1<System::Int32>&& other);
				bool operator==(const IList_1<System::Int32>& other) const;
				bool operator!=(const IList_1<System::Int32>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListSystemInt32Iterator
	{
		System::Collections::Generic::IEnumerator_1<System::Int32> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListSystemInt32Iterator(decltype(nullptr));
		SystemCollectionsGenericIListSystemInt32Iterator(System::Collections::Generic::IList_1<System::Int32>& enumerable);
		~SystemCollectionsGenericIListSystemInt32Iterator();
		SystemCollectionsGenericIListSystemInt32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListSystemInt32Iterator& other);
		System::Int32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListSystemInt32Iterator begin(System::Collections::Generic::IList_1<System::Int32>& enumerable);
			Plugin::SystemCollectionsGenericIListSystemInt32Iterator end(System::Collections::Generic::IList_1<System::Int32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Object> : virtual System::Collections::Generic::ICollection_1<System::Object>
			{
				IList_1<System::Object>(decltype(nullptr));
				IList_1<System::Object>(Plugin::InternalUse, int32_t handle);
				IList_1<System::Object>(const IList_1<System::Object>& other);
				IList_1<System::Object>(IList_1<System::Object>&& other);
				virtual ~IList_1<System::Object>();
				IList_1<System::Object>& operator=(const IList_1<System::Object>& other);
				IList_1<System::Object>& operator=(decltype(nullptr));
				IList_1<System::Object>& operator=(IList_1<System::Object>&& other);
				bool operator==(const IList_1<System::Object>& other) const;
				bool operator!=(const IList_1<System::Object>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListSystemObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListSystemObjectIterator(decltype(nullptr));
		SystemCollectionsGenericIListSystemObjectIterator(System::Collections::Generic::IList_1<System::Object>& enumerable);
		~SystemCollectionsGenericIListSystemObjectIterator();
		SystemCollectionsGenericIListSystemObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListSystemObjectIterator& other);
		System::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListSystemObjectIterator begin(System::Collections::Generic::IList_1<System::Object>& enumerable);
			Plugin::SystemCollectionsGenericIListSystemObjectIterator end(System::Collections::Generic::IList_1<System::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::Byte> : virtual System::Collections::Generic::ICollection_1<System::Byte>
			{
				IList_1<System::Byte>(decltype(nullptr));
				IList_1<System::Byte>(Plugin::InternalUse, int32_t handle);
				IList_1<System::Byte>(const IList_1<System::Byte>& other);
				IList_1<System::Byte>(IList_1<System::Byte>&& other);
				virtual ~IList_1<System::Byte>();
				IList_1<System::Byte>& operator=(const IList_1<System::Byte>& other);
				IList_1<System::Byte>& operator=(decltype(nullptr));
				IList_1<System::Byte>& operator=(IList_1<System::Byte>&& other);
				bool operator==(const IList_1<System::Byte>& other) const;
				bool operator!=(const IList_1<System::Byte>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListSystemByteIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Byte> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListSystemByteIterator(decltype(nullptr));
		SystemCollectionsGenericIListSystemByteIterator(System::Collections::Generic::IList_1<System::Byte>& enumerable);
		~SystemCollectionsGenericIListSystemByteIterator();
		SystemCollectionsGenericIListSystemByteIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListSystemByteIterator& other);
		System::Byte operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListSystemByteIterator begin(System::Collections::Generic::IList_1<System::Byte>& enumerable);
			Plugin::SystemCollectionsGenericIListSystemByteIterator end(System::Collections::Generic::IList_1<System::Byte>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct IList_1<System::String> : virtual System::Collections::Generic::ICollection_1<System::String>
			{
				IList_1<System::String>(decltype(nullptr));
				IList_1<System::String>(Plugin::InternalUse, int32_t handle);
				IList_1<System::String>(const IList_1<System::String>& other);
				IList_1<System::String>(IList_1<System::String>&& other);
				virtual ~IList_1<System::String>();
				IList_1<System::String>& operator=(const IList_1<System::String>& other);
				IList_1<System::String>& operator=(decltype(nullptr));
				IList_1<System::String>& operator=(IList_1<System::String>&& other);
				bool operator==(const IList_1<System::String>& other) const;
				bool operator!=(const IList_1<System::String>& other) const;
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericIListSystemStringIterator
	{
		System::Collections::Generic::IEnumerator_1<System::String> enumerator;
		bool hasMore;
		SystemCollectionsGenericIListSystemStringIterator(decltype(nullptr));
		SystemCollectionsGenericIListSystemStringIterator(System::Collections::Generic::IList_1<System::String>& enumerable);
		~SystemCollectionsGenericIListSystemStringIterator();
		SystemCollectionsGenericIListSystemStringIterator& operator++();
		bool operator!=(const SystemCollectionsGenericIListSystemStringIterator& other);
		System::String operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericIListSystemStringIterator begin(System::Collections::Generic::IList_1<System::String>& enumerable);
			Plugin::SystemCollectionsGenericIListSystemStringIterator end(System::Collections::Generic::IList_1<System::String>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<System::Int32> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<System::Int32>, virtual System::Collections::Generic::IReadOnlyList_1<System::Int32>
			{
				List_1<System::Int32>(decltype(nullptr));
				List_1<System::Int32>(Plugin::InternalUse, int32_t handle);
				List_1<System::Int32>(const List_1<System::Int32>& other);
				List_1<System::Int32>(List_1<System::Int32>&& other);
				virtual ~List_1<System::Int32>();
				List_1<System::Int32>& operator=(const List_1<System::Int32>& other);
				List_1<System::Int32>& operator=(decltype(nullptr));
				List_1<System::Int32>& operator=(List_1<System::Int32>&& other);
				bool operator==(const List_1<System::Int32>& other) const;
				bool operator!=(const List_1<System::Int32>& other) const;
				List_1();
				System::Int32 GetItem(System::Int32 index);
				void SetItem(System::Int32 index, System::Int32 value);
				System::Int32 GetCount();
				void Add(System::Int32 item);
				System::Boolean Remove(System::Int32 item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListSystemInt32Iterator
	{
		System::Collections::Generic::IEnumerator_1<System::Int32> enumerator;
		bool hasMore;
		SystemCollectionsGenericListSystemInt32Iterator(decltype(nullptr));
		SystemCollectionsGenericListSystemInt32Iterator(System::Collections::Generic::List_1<System::Int32>& enumerable);
		~SystemCollectionsGenericListSystemInt32Iterator();
		SystemCollectionsGenericListSystemInt32Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericListSystemInt32Iterator& other);
		System::Int32 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListSystemInt32Iterator begin(System::Collections::Generic::List_1<System::Int32>& enumerable);
			Plugin::SystemCollectionsGenericListSystemInt32Iterator end(System::Collections::Generic::List_1<System::Int32>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<System::Object> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<System::Object>, virtual System::Collections::Generic::IReadOnlyList_1<System::Object>
			{
				List_1<System::Object>(decltype(nullptr));
				List_1<System::Object>(Plugin::InternalUse, int32_t handle);
				List_1<System::Object>(const List_1<System::Object>& other);
				List_1<System::Object>(List_1<System::Object>&& other);
				virtual ~List_1<System::Object>();
				List_1<System::Object>& operator=(const List_1<System::Object>& other);
				List_1<System::Object>& operator=(decltype(nullptr));
				List_1<System::Object>& operator=(List_1<System::Object>&& other);
				bool operator==(const List_1<System::Object>& other) const;
				bool operator!=(const List_1<System::Object>& other) const;
				List_1();
				System::Object GetItem(System::Int32 index);
				void SetItem(System::Int32 index, System::Object& value);
				void Add(System::Object& item);
				System::Boolean Remove(System::Object& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListSystemObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<System::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericListSystemObjectIterator(decltype(nullptr));
		SystemCollectionsGenericListSystemObjectIterator(System::Collections::Generic::List_1<System::Object>& enumerable);
		~SystemCollectionsGenericListSystemObjectIterator();
		SystemCollectionsGenericListSystemObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListSystemObjectIterator& other);
		System::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListSystemObjectIterator begin(System::Collections::Generic::List_1<System::Object>& enumerable);
			Plugin::SystemCollectionsGenericListSystemObjectIterator end(System::Collections::Generic::List_1<System::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Object> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Object>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Object>
			{
				List_1<UnityEngine::Object>(decltype(nullptr));
				List_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
				List_1<UnityEngine::Object>(const List_1<UnityEngine::Object>& other);
				List_1<UnityEngine::Object>(List_1<UnityEngine::Object>&& other);
				virtual ~List_1<UnityEngine::Object>();
				List_1<UnityEngine::Object>& operator=(const List_1<UnityEngine::Object>& other);
				List_1<UnityEngine::Object>& operator=(decltype(nullptr));
				List_1<UnityEngine::Object>& operator=(List_1<UnityEngine::Object>&& other);
				bool operator==(const List_1<UnityEngine::Object>& other) const;
				bool operator!=(const List_1<UnityEngine::Object>& other) const;
				List_1();
				UnityEngine::Object GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UnityEngine::Object& value);
				System::Int32 GetCount();
				void Add(UnityEngine::Object& item);
				System::Boolean Remove(UnityEngine::Object& item);
				void RemoveAt(System::Int32 index);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUnityEngineObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Object> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUnityEngineObjectIterator(decltype(nullptr));
		SystemCollectionsGenericListUnityEngineObjectIterator(System::Collections::Generic::List_1<UnityEngine::Object>& enumerable);
		~SystemCollectionsGenericListUnityEngineObjectIterator();
		SystemCollectionsGenericListUnityEngineObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUnityEngineObjectIterator& other);
		UnityEngine::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUnityEngineObjectIterator begin(System::Collections::Generic::List_1<UnityEngine::Object>& enumerable);
			Plugin::SystemCollectionsGenericListUnityEngineObjectIterator end(System::Collections::Generic::List_1<UnityEngine::Object>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Material> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Material>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Material>
			{
				List_1<UnityEngine::Material>(decltype(nullptr));
				List_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
				List_1<UnityEngine::Material>(const List_1<UnityEngine::Material>& other);
				List_1<UnityEngine::Material>(List_1<UnityEngine::Material>&& other);
				virtual ~List_1<UnityEngine::Material>();
				List_1<UnityEngine::Material>& operator=(const List_1<UnityEngine::Material>& other);
				List_1<UnityEngine::Material>& operator=(decltype(nullptr));
				List_1<UnityEngine::Material>& operator=(List_1<UnityEngine::Material>&& other);
				bool operator==(const List_1<UnityEngine::Material>& other) const;
				bool operator!=(const List_1<UnityEngine::Material>& other) const;
				List_1();
				UnityEngine::Material GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UnityEngine::Material& value);
				void Add(UnityEngine::Material& item);
				System::Boolean Remove(UnityEngine::Material& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUnityEngineMaterialIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Material> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUnityEngineMaterialIterator(decltype(nullptr));
		SystemCollectionsGenericListUnityEngineMaterialIterator(System::Collections::Generic::List_1<UnityEngine::Material>& enumerable);
		~SystemCollectionsGenericListUnityEngineMaterialIterator();
		SystemCollectionsGenericListUnityEngineMaterialIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUnityEngineMaterialIterator& other);
		UnityEngine::Material operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUnityEngineMaterialIterator begin(System::Collections::Generic::List_1<UnityEngine::Material>& enumerable);
			Plugin::SystemCollectionsGenericListUnityEngineMaterialIterator end(System::Collections::Generic::List_1<UnityEngine::Material>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Texture2D> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Texture2D>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Texture2D>
			{
				List_1<UnityEngine::Texture2D>(decltype(nullptr));
				List_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
				List_1<UnityEngine::Texture2D>(const List_1<UnityEngine::Texture2D>& other);
				List_1<UnityEngine::Texture2D>(List_1<UnityEngine::Texture2D>&& other);
				virtual ~List_1<UnityEngine::Texture2D>();
				List_1<UnityEngine::Texture2D>& operator=(const List_1<UnityEngine::Texture2D>& other);
				List_1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
				List_1<UnityEngine::Texture2D>& operator=(List_1<UnityEngine::Texture2D>&& other);
				bool operator==(const List_1<UnityEngine::Texture2D>& other) const;
				bool operator!=(const List_1<UnityEngine::Texture2D>& other) const;
				List_1();
				UnityEngine::Texture2D GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UnityEngine::Texture2D& value);
				void Add(UnityEngine::Texture2D& item);
				System::Boolean Remove(UnityEngine::Texture2D& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUnityEngineTexture2DIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Texture2D> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUnityEngineTexture2DIterator(decltype(nullptr));
		SystemCollectionsGenericListUnityEngineTexture2DIterator(System::Collections::Generic::List_1<UnityEngine::Texture2D>& enumerable);
		~SystemCollectionsGenericListUnityEngineTexture2DIterator();
		SystemCollectionsGenericListUnityEngineTexture2DIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUnityEngineTexture2DIterator& other);
		UnityEngine::Texture2D operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUnityEngineTexture2DIterator begin(System::Collections::Generic::List_1<UnityEngine::Texture2D>& enumerable);
			Plugin::SystemCollectionsGenericListUnityEngineTexture2DIterator end(System::Collections::Generic::List_1<UnityEngine::Texture2D>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Cubemap> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Cubemap>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Cubemap>
			{
				List_1<UnityEngine::Cubemap>(decltype(nullptr));
				List_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
				List_1<UnityEngine::Cubemap>(const List_1<UnityEngine::Cubemap>& other);
				List_1<UnityEngine::Cubemap>(List_1<UnityEngine::Cubemap>&& other);
				virtual ~List_1<UnityEngine::Cubemap>();
				List_1<UnityEngine::Cubemap>& operator=(const List_1<UnityEngine::Cubemap>& other);
				List_1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
				List_1<UnityEngine::Cubemap>& operator=(List_1<UnityEngine::Cubemap>&& other);
				bool operator==(const List_1<UnityEngine::Cubemap>& other) const;
				bool operator!=(const List_1<UnityEngine::Cubemap>& other) const;
				List_1();
				UnityEngine::Cubemap GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UnityEngine::Cubemap& value);
				void Add(UnityEngine::Cubemap& item);
				System::Boolean Remove(UnityEngine::Cubemap& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUnityEngineCubemapIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Cubemap> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUnityEngineCubemapIterator(decltype(nullptr));
		SystemCollectionsGenericListUnityEngineCubemapIterator(System::Collections::Generic::List_1<UnityEngine::Cubemap>& enumerable);
		~SystemCollectionsGenericListUnityEngineCubemapIterator();
		SystemCollectionsGenericListUnityEngineCubemapIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUnityEngineCubemapIterator& other);
		UnityEngine::Cubemap operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUnityEngineCubemapIterator begin(System::Collections::Generic::List_1<UnityEngine::Cubemap>& enumerable);
			Plugin::SystemCollectionsGenericListUnityEngineCubemapIterator end(System::Collections::Generic::List_1<UnityEngine::Cubemap>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Matrix4x4> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Matrix4x4>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Matrix4x4>
			{
				List_1<UnityEngine::Matrix4x4>(decltype(nullptr));
				List_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
				List_1<UnityEngine::Matrix4x4>(const List_1<UnityEngine::Matrix4x4>& other);
				List_1<UnityEngine::Matrix4x4>(List_1<UnityEngine::Matrix4x4>&& other);
				virtual ~List_1<UnityEngine::Matrix4x4>();
				List_1<UnityEngine::Matrix4x4>& operator=(const List_1<UnityEngine::Matrix4x4>& other);
				List_1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
				List_1<UnityEngine::Matrix4x4>& operator=(List_1<UnityEngine::Matrix4x4>&& other);
				bool operator==(const List_1<UnityEngine::Matrix4x4>& other) const;
				bool operator!=(const List_1<UnityEngine::Matrix4x4>& other) const;
				List_1();
				UnityEngine::Matrix4x4 GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UnityEngine::Matrix4x4& value);
				System::Int32 GetCount();
				void Add(UnityEngine::Matrix4x4& item);
				System::Boolean Remove(UnityEngine::Matrix4x4& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUnityEngineMatrix4x4Iterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Matrix4x4> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUnityEngineMatrix4x4Iterator(decltype(nullptr));
		SystemCollectionsGenericListUnityEngineMatrix4x4Iterator(System::Collections::Generic::List_1<UnityEngine::Matrix4x4>& enumerable);
		~SystemCollectionsGenericListUnityEngineMatrix4x4Iterator();
		SystemCollectionsGenericListUnityEngineMatrix4x4Iterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUnityEngineMatrix4x4Iterator& other);
		UnityEngine::Matrix4x4 operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUnityEngineMatrix4x4Iterator begin(System::Collections::Generic::List_1<UnityEngine::Matrix4x4>& enumerable);
			Plugin::SystemCollectionsGenericListUnityEngineMatrix4x4Iterator end(System::Collections::Generic::List_1<UnityEngine::Matrix4x4>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::IPlMessageable> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::IPlMessageable>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IPlMessageable>
			{
				List_1<UPlasma::PlEmu::IPlMessageable>(decltype(nullptr));
				List_1<UPlasma::PlEmu::IPlMessageable>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::IPlMessageable>(const List_1<UPlasma::PlEmu::IPlMessageable>& other);
				List_1<UPlasma::PlEmu::IPlMessageable>(List_1<UPlasma::PlEmu::IPlMessageable>&& other);
				virtual ~List_1<UPlasma::PlEmu::IPlMessageable>();
				List_1<UPlasma::PlEmu::IPlMessageable>& operator=(const List_1<UPlasma::PlEmu::IPlMessageable>& other);
				List_1<UPlasma::PlEmu::IPlMessageable>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::IPlMessageable>& operator=(List_1<UPlasma::PlEmu::IPlMessageable>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::IPlMessageable>& other) const;
				List_1();
				UPlasma::PlEmu::IPlMessageable GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::IPlMessageable& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::IPlMessageable& item);
				System::Boolean Remove(UPlasma::PlEmu::IPlMessageable& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::IPlMessageable> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator();
		SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator& other);
		UPlasma::PlEmu::IPlMessageable operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuIPlMessageableIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::IPlMessageable>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlConditionalObject> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlConditionalObject>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlConditionalObject>
			{
				List_1<UPlasma::PlEmu::PlConditionalObject>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlConditionalObject>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PlConditionalObject>(const List_1<UPlasma::PlEmu::PlConditionalObject>& other);
				List_1<UPlasma::PlEmu::PlConditionalObject>(List_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				virtual ~List_1<UPlasma::PlEmu::PlConditionalObject>();
				List_1<UPlasma::PlEmu::PlConditionalObject>& operator=(const List_1<UPlasma::PlEmu::PlConditionalObject>& other);
				List_1<UPlasma::PlEmu::PlConditionalObject>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlConditionalObject>& operator=(List_1<UPlasma::PlEmu::PlConditionalObject>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PlConditionalObject>& other) const;
				List_1();
				UPlasma::PlEmu::PlConditionalObject GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PlConditionalObject& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PlConditionalObject& item);
				System::Boolean Remove(UPlasma::PlEmu::PlConditionalObject& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlConditionalObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator& other);
		UPlasma::PlEmu::PlConditionalObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlConditionalObjectIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PlConditionalObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlDetector> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlDetector>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlDetector>
			{
				List_1<UPlasma::PlEmu::PlDetector>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlDetector>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PlDetector>(const List_1<UPlasma::PlEmu::PlDetector>& other);
				List_1<UPlasma::PlEmu::PlDetector>(List_1<UPlasma::PlEmu::PlDetector>&& other);
				virtual ~List_1<UPlasma::PlEmu::PlDetector>();
				List_1<UPlasma::PlEmu::PlDetector>& operator=(const List_1<UPlasma::PlEmu::PlDetector>& other);
				List_1<UPlasma::PlEmu::PlDetector>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlDetector>& operator=(List_1<UPlasma::PlEmu::PlDetector>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PlDetector>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PlDetector>& other) const;
				List_1();
				UPlasma::PlEmu::PlDetector GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PlDetector& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PlDetector& item);
				System::Boolean Remove(UPlasma::PlEmu::PlDetector& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlDetector> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PlDetector>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator& other);
		UPlasma::PlEmu::PlDetector operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PlDetector>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlDetectorIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PlDetector>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlMessage> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlMessage>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlMessage>
			{
				List_1<UPlasma::PlEmu::PlMessage>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlMessage>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PlMessage>(const List_1<UPlasma::PlEmu::PlMessage>& other);
				List_1<UPlasma::PlEmu::PlMessage>(List_1<UPlasma::PlEmu::PlMessage>&& other);
				virtual ~List_1<UPlasma::PlEmu::PlMessage>();
				List_1<UPlasma::PlEmu::PlMessage>& operator=(const List_1<UPlasma::PlEmu::PlMessage>& other);
				List_1<UPlasma::PlEmu::PlMessage>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlMessage>& operator=(List_1<UPlasma::PlEmu::PlMessage>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PlMessage>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PlMessage>& other) const;
				List_1();
				UPlasma::PlEmu::PlMessage GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PlMessage& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PlMessage& item);
				System::Boolean Remove(UPlasma::PlEmu::PlMessage& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlMessage> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PlMessage>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator& other);
		UPlasma::PlEmu::PlMessage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PlMessage>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlMessageIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PlMessage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlOneShotCallback> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlOneShotCallback>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlOneShotCallback>
			{
				List_1<UPlasma::PlEmu::PlOneShotCallback>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlOneShotCallback>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PlOneShotCallback>(const List_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				List_1<UPlasma::PlEmu::PlOneShotCallback>(List_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				virtual ~List_1<UPlasma::PlEmu::PlOneShotCallback>();
				List_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(const List_1<UPlasma::PlEmu::PlOneShotCallback>& other);
				List_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlOneShotCallback>& operator=(List_1<UPlasma::PlEmu::PlOneShotCallback>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PlOneShotCallback>& other) const;
				List_1();
				UPlasma::PlEmu::PlOneShotCallback GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PlOneShotCallback& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PlOneShotCallback& item);
				System::Boolean Remove(UPlasma::PlEmu::PlOneShotCallback& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlOneShotCallback> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator& other);
		UPlasma::PlEmu::PlOneShotCallback operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlOneShotCallbackIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PlOneShotCallback>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlRandomSoundModGroup> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlRandomSoundModGroup>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlRandomSoundModGroup>
			{
				List_1<UPlasma::PlEmu::PlRandomSoundModGroup>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlRandomSoundModGroup>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PlRandomSoundModGroup>(const List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				List_1<UPlasma::PlEmu::PlRandomSoundModGroup>(List_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				virtual ~List_1<UPlasma::PlEmu::PlRandomSoundModGroup>();
				List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(const List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other);
				List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& operator=(List_1<UPlasma::PlEmu::PlRandomSoundModGroup>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& other) const;
				List_1();
				UPlasma::PlEmu::PlRandomSoundModGroup GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PlRandomSoundModGroup& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PlRandomSoundModGroup& item);
				System::Boolean Remove(UPlasma::PlEmu::PlRandomSoundModGroup& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlRandomSoundModGroup> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator& other);
		UPlasma::PlEmu::PlRandomSoundModGroup operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlRandomSoundModGroupIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PlRandomSoundModGroup>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::GameObject> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::GameObject>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::GameObject>
			{
				List_1<UnityEngine::GameObject>(decltype(nullptr));
				List_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
				List_1<UnityEngine::GameObject>(const List_1<UnityEngine::GameObject>& other);
				List_1<UnityEngine::GameObject>(List_1<UnityEngine::GameObject>&& other);
				virtual ~List_1<UnityEngine::GameObject>();
				List_1<UnityEngine::GameObject>& operator=(const List_1<UnityEngine::GameObject>& other);
				List_1<UnityEngine::GameObject>& operator=(decltype(nullptr));
				List_1<UnityEngine::GameObject>& operator=(List_1<UnityEngine::GameObject>&& other);
				bool operator==(const List_1<UnityEngine::GameObject>& other) const;
				bool operator!=(const List_1<UnityEngine::GameObject>& other) const;
				List_1();
				UnityEngine::GameObject GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UnityEngine::GameObject& value);
				System::Int32 GetCount();
				void Add(UnityEngine::GameObject& item);
				System::Boolean Remove(UnityEngine::GameObject& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUnityEngineGameObjectIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::GameObject> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUnityEngineGameObjectIterator(decltype(nullptr));
		SystemCollectionsGenericListUnityEngineGameObjectIterator(System::Collections::Generic::List_1<UnityEngine::GameObject>& enumerable);
		~SystemCollectionsGenericListUnityEngineGameObjectIterator();
		SystemCollectionsGenericListUnityEngineGameObjectIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUnityEngineGameObjectIterator& other);
		UnityEngine::GameObject operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUnityEngineGameObjectIterator begin(System::Collections::Generic::List_1<UnityEngine::GameObject>& enumerable);
			Plugin::SystemCollectionsGenericListUnityEngineGameObjectIterator end(System::Collections::Generic::List_1<UnityEngine::GameObject>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UnityEngine::Transform> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Transform>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Transform>
			{
				List_1<UnityEngine::Transform>(decltype(nullptr));
				List_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
				List_1<UnityEngine::Transform>(const List_1<UnityEngine::Transform>& other);
				List_1<UnityEngine::Transform>(List_1<UnityEngine::Transform>&& other);
				virtual ~List_1<UnityEngine::Transform>();
				List_1<UnityEngine::Transform>& operator=(const List_1<UnityEngine::Transform>& other);
				List_1<UnityEngine::Transform>& operator=(decltype(nullptr));
				List_1<UnityEngine::Transform>& operator=(List_1<UnityEngine::Transform>&& other);
				bool operator==(const List_1<UnityEngine::Transform>& other) const;
				bool operator!=(const List_1<UnityEngine::Transform>& other) const;
				List_1();
				UnityEngine::Transform GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UnityEngine::Transform& value);
				System::Int32 GetCount();
				void Add(UnityEngine::Transform& item);
				System::Boolean Remove(UnityEngine::Transform& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUnityEngineTransformIterator
	{
		System::Collections::Generic::IEnumerator_1<UnityEngine::Transform> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUnityEngineTransformIterator(decltype(nullptr));
		SystemCollectionsGenericListUnityEngineTransformIterator(System::Collections::Generic::List_1<UnityEngine::Transform>& enumerable);
		~SystemCollectionsGenericListUnityEngineTransformIterator();
		SystemCollectionsGenericListUnityEngineTransformIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUnityEngineTransformIterator& other);
		UnityEngine::Transform operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUnityEngineTransformIterator begin(System::Collections::Generic::List_1<UnityEngine::Transform>& enumerable);
			Plugin::SystemCollectionsGenericListUnityEngineTransformIterator end(System::Collections::Generic::List_1<UnityEngine::Transform>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlPage> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlPage>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlPage>
			{
				List_1<UPlasma::PlEmu::PlPage>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlPage>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PlPage>(const List_1<UPlasma::PlEmu::PlPage>& other);
				List_1<UPlasma::PlEmu::PlPage>(List_1<UPlasma::PlEmu::PlPage>&& other);
				virtual ~List_1<UPlasma::PlEmu::PlPage>();
				List_1<UPlasma::PlEmu::PlPage>& operator=(const List_1<UPlasma::PlEmu::PlPage>& other);
				List_1<UPlasma::PlEmu::PlPage>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlPage>& operator=(List_1<UPlasma::PlEmu::PlPage>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PlPage>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PlPage>& other) const;
				List_1();
				UPlasma::PlEmu::PlPage GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PlPage& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PlPage& item);
				System::Boolean Remove(UPlasma::PlEmu::PlPage& item);
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlPage> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PlPage>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator& other);
		UPlasma::PlEmu::PlPage operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PlPage>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlPageIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PlPage>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PrpImportClues> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PrpImportClues>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PrpImportClues>
			{
				List_1<UPlasma::PlEmu::PrpImportClues>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PrpImportClues>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PrpImportClues>(const List_1<UPlasma::PlEmu::PrpImportClues>& other);
				List_1<UPlasma::PlEmu::PrpImportClues>(List_1<UPlasma::PlEmu::PrpImportClues>&& other);
				virtual ~List_1<UPlasma::PlEmu::PrpImportClues>();
				List_1<UPlasma::PlEmu::PrpImportClues>& operator=(const List_1<UPlasma::PlEmu::PrpImportClues>& other);
				List_1<UPlasma::PlEmu::PrpImportClues>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PrpImportClues>& operator=(List_1<UPlasma::PlEmu::PrpImportClues>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PrpImportClues>& other) const;
				List_1();
				UPlasma::PlEmu::PrpImportClues GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PrpImportClues& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PrpImportClues& item);
				System::Boolean Remove(UPlasma::PlEmu::PrpImportClues& item);
				void Clear();
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PrpImportClues> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator& other);
		UPlasma::PlEmu::PrpImportClues operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPrpImportCluesIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PrpImportClues>& enumerable);
		}
	}
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			template<> struct List_1<UPlasma::PlEmu::PlCluster> : virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlCluster>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlCluster>
			{
				List_1<UPlasma::PlEmu::PlCluster>(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlCluster>(Plugin::InternalUse, int32_t handle);
				List_1<UPlasma::PlEmu::PlCluster>(const List_1<UPlasma::PlEmu::PlCluster>& other);
				List_1<UPlasma::PlEmu::PlCluster>(List_1<UPlasma::PlEmu::PlCluster>&& other);
				virtual ~List_1<UPlasma::PlEmu::PlCluster>();
				List_1<UPlasma::PlEmu::PlCluster>& operator=(const List_1<UPlasma::PlEmu::PlCluster>& other);
				List_1<UPlasma::PlEmu::PlCluster>& operator=(decltype(nullptr));
				List_1<UPlasma::PlEmu::PlCluster>& operator=(List_1<UPlasma::PlEmu::PlCluster>&& other);
				bool operator==(const List_1<UPlasma::PlEmu::PlCluster>& other) const;
				bool operator!=(const List_1<UPlasma::PlEmu::PlCluster>& other) const;
				List_1();
				UPlasma::PlEmu::PlCluster GetItem(System::Int32 index);
				void SetItem(System::Int32 index, UPlasma::PlEmu::PlCluster& value);
				System::Int32 GetCount();
				void Add(UPlasma::PlEmu::PlCluster& item);
				System::Boolean Remove(UPlasma::PlEmu::PlCluster& item);
				void Clear();
			};
		}
	}
}

namespace Plugin
{
	struct SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator
	{
		System::Collections::Generic::IEnumerator_1<UPlasma::PlEmu::PlCluster> enumerator;
		bool hasMore;
		SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator(decltype(nullptr));
		SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator(System::Collections::Generic::List_1<UPlasma::PlEmu::PlCluster>& enumerable);
		~SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator();
		SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator& operator++();
		bool operator!=(const SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator& other);
		UPlasma::PlEmu::PlCluster operator*();
	};
}

namespace System
{
	namespace Collections
	{
		namespace Generic
		{
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator begin(System::Collections::Generic::List_1<UPlasma::PlEmu::PlCluster>& enumerable);
			Plugin::SystemCollectionsGenericListUPlasmaPlEmuPlClusterIterator end(System::Collections::Generic::List_1<UPlasma::PlEmu::PlCluster>& enumerable);
		}
	}
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Object>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Object item);
		operator UnityEngine::Object();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Object> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Object>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Object>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Object>(decltype(nullptr));
		Array1<UnityEngine::Object>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Object>(const Array1<UnityEngine::Object>& other);
		Array1<UnityEngine::Object>(Array1<UnityEngine::Object>&& other);
		virtual ~Array1<UnityEngine::Object>();
		Array1<UnityEngine::Object>& operator=(const Array1<UnityEngine::Object>& other);
		Array1<UnityEngine::Object>& operator=(decltype(nullptr));
		Array1<UnityEngine::Object>& operator=(Array1<UnityEngine::Object>&& other);
		bool operator==(const Array1<UnityEngine::Object>& other) const;
		bool operator!=(const Array1<UnityEngine::Object>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Object> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineObjectArray1Iterator
	{
		System::Array1<UnityEngine::Object>& array;
		int index;
		UnityEngineObjectArray1Iterator(System::Array1<UnityEngine::Object>& array, int32_t index);
		UnityEngineObjectArray1Iterator& operator++();
		bool operator!=(const UnityEngineObjectArray1Iterator& other);
		UnityEngine::Object operator*();
	};
}

namespace System
{
	Plugin::UnityEngineObjectArray1Iterator begin(System::Array1<UnityEngine::Object>& array);
	Plugin::UnityEngineObjectArray1Iterator end(System::Array1<UnityEngine::Object>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::GameObject>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::GameObject item);
		operator UnityEngine::GameObject();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::GameObject> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::GameObject>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::GameObject>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::GameObject>(decltype(nullptr));
		Array1<UnityEngine::GameObject>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::GameObject>(const Array1<UnityEngine::GameObject>& other);
		Array1<UnityEngine::GameObject>(Array1<UnityEngine::GameObject>&& other);
		virtual ~Array1<UnityEngine::GameObject>();
		Array1<UnityEngine::GameObject>& operator=(const Array1<UnityEngine::GameObject>& other);
		Array1<UnityEngine::GameObject>& operator=(decltype(nullptr));
		Array1<UnityEngine::GameObject>& operator=(Array1<UnityEngine::GameObject>&& other);
		bool operator==(const Array1<UnityEngine::GameObject>& other) const;
		bool operator!=(const Array1<UnityEngine::GameObject>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::GameObject> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineGameObjectArray1Iterator
	{
		System::Array1<UnityEngine::GameObject>& array;
		int index;
		UnityEngineGameObjectArray1Iterator(System::Array1<UnityEngine::GameObject>& array, int32_t index);
		UnityEngineGameObjectArray1Iterator& operator++();
		bool operator!=(const UnityEngineGameObjectArray1Iterator& other);
		UnityEngine::GameObject operator*();
	};
}

namespace System
{
	Plugin::UnityEngineGameObjectArray1Iterator begin(System::Array1<UnityEngine::GameObject>& array);
	Plugin::UnityEngineGameObjectArray1Iterator end(System::Array1<UnityEngine::GameObject>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Transform>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Transform item);
		operator UnityEngine::Transform();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Transform> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Transform>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Transform>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Transform>(decltype(nullptr));
		Array1<UnityEngine::Transform>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Transform>(const Array1<UnityEngine::Transform>& other);
		Array1<UnityEngine::Transform>(Array1<UnityEngine::Transform>&& other);
		virtual ~Array1<UnityEngine::Transform>();
		Array1<UnityEngine::Transform>& operator=(const Array1<UnityEngine::Transform>& other);
		Array1<UnityEngine::Transform>& operator=(decltype(nullptr));
		Array1<UnityEngine::Transform>& operator=(Array1<UnityEngine::Transform>&& other);
		bool operator==(const Array1<UnityEngine::Transform>& other) const;
		bool operator!=(const Array1<UnityEngine::Transform>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Transform> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineTransformArray1Iterator
	{
		System::Array1<UnityEngine::Transform>& array;
		int index;
		UnityEngineTransformArray1Iterator(System::Array1<UnityEngine::Transform>& array, int32_t index);
		UnityEngineTransformArray1Iterator& operator++();
		bool operator!=(const UnityEngineTransformArray1Iterator& other);
		UnityEngine::Transform operator*();
	};
}

namespace System
{
	Plugin::UnityEngineTransformArray1Iterator begin(System::Array1<UnityEngine::Transform>& array);
	Plugin::UnityEngineTransformArray1Iterator end(System::Array1<UnityEngine::Transform>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::MeshFilter>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::MeshFilter item);
		operator UnityEngine::MeshFilter();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::MeshFilter> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::MeshFilter>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::MeshFilter>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::MeshFilter>(decltype(nullptr));
		Array1<UnityEngine::MeshFilter>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::MeshFilter>(const Array1<UnityEngine::MeshFilter>& other);
		Array1<UnityEngine::MeshFilter>(Array1<UnityEngine::MeshFilter>&& other);
		virtual ~Array1<UnityEngine::MeshFilter>();
		Array1<UnityEngine::MeshFilter>& operator=(const Array1<UnityEngine::MeshFilter>& other);
		Array1<UnityEngine::MeshFilter>& operator=(decltype(nullptr));
		Array1<UnityEngine::MeshFilter>& operator=(Array1<UnityEngine::MeshFilter>&& other);
		bool operator==(const Array1<UnityEngine::MeshFilter>& other) const;
		bool operator!=(const Array1<UnityEngine::MeshFilter>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::MeshFilter> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineMeshFilterArray1Iterator
	{
		System::Array1<UnityEngine::MeshFilter>& array;
		int index;
		UnityEngineMeshFilterArray1Iterator(System::Array1<UnityEngine::MeshFilter>& array, int32_t index);
		UnityEngineMeshFilterArray1Iterator& operator++();
		bool operator!=(const UnityEngineMeshFilterArray1Iterator& other);
		UnityEngine::MeshFilter operator*();
	};
}

namespace System
{
	Plugin::UnityEngineMeshFilterArray1Iterator begin(System::Array1<UnityEngine::MeshFilter>& array);
	Plugin::UnityEngineMeshFilterArray1Iterator end(System::Array1<UnityEngine::MeshFilter>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Material>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Material item);
		operator UnityEngine::Material();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Material> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Material>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Material>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Material>(decltype(nullptr));
		Array1<UnityEngine::Material>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Material>(const Array1<UnityEngine::Material>& other);
		Array1<UnityEngine::Material>(Array1<UnityEngine::Material>&& other);
		virtual ~Array1<UnityEngine::Material>();
		Array1<UnityEngine::Material>& operator=(const Array1<UnityEngine::Material>& other);
		Array1<UnityEngine::Material>& operator=(decltype(nullptr));
		Array1<UnityEngine::Material>& operator=(Array1<UnityEngine::Material>&& other);
		bool operator==(const Array1<UnityEngine::Material>& other) const;
		bool operator!=(const Array1<UnityEngine::Material>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Material> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineMaterialArray1Iterator
	{
		System::Array1<UnityEngine::Material>& array;
		int index;
		UnityEngineMaterialArray1Iterator(System::Array1<UnityEngine::Material>& array, int32_t index);
		UnityEngineMaterialArray1Iterator& operator++();
		bool operator!=(const UnityEngineMaterialArray1Iterator& other);
		UnityEngine::Material operator*();
	};
}

namespace System
{
	Plugin::UnityEngineMaterialArray1Iterator begin(System::Array1<UnityEngine::Material>& array);
	Plugin::UnityEngineMaterialArray1Iterator end(System::Array1<UnityEngine::Material>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Texture2D>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Texture2D item);
		operator UnityEngine::Texture2D();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Texture2D> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Texture2D>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Texture2D>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Texture2D>(decltype(nullptr));
		Array1<UnityEngine::Texture2D>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Texture2D>(const Array1<UnityEngine::Texture2D>& other);
		Array1<UnityEngine::Texture2D>(Array1<UnityEngine::Texture2D>&& other);
		virtual ~Array1<UnityEngine::Texture2D>();
		Array1<UnityEngine::Texture2D>& operator=(const Array1<UnityEngine::Texture2D>& other);
		Array1<UnityEngine::Texture2D>& operator=(decltype(nullptr));
		Array1<UnityEngine::Texture2D>& operator=(Array1<UnityEngine::Texture2D>&& other);
		bool operator==(const Array1<UnityEngine::Texture2D>& other) const;
		bool operator!=(const Array1<UnityEngine::Texture2D>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Texture2D> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineTexture2DArray1Iterator
	{
		System::Array1<UnityEngine::Texture2D>& array;
		int index;
		UnityEngineTexture2DArray1Iterator(System::Array1<UnityEngine::Texture2D>& array, int32_t index);
		UnityEngineTexture2DArray1Iterator& operator++();
		bool operator!=(const UnityEngineTexture2DArray1Iterator& other);
		UnityEngine::Texture2D operator*();
	};
}

namespace System
{
	Plugin::UnityEngineTexture2DArray1Iterator begin(System::Array1<UnityEngine::Texture2D>& array);
	Plugin::UnityEngineTexture2DArray1Iterator end(System::Array1<UnityEngine::Texture2D>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Cubemap>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Cubemap item);
		operator UnityEngine::Cubemap();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Cubemap> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Cubemap>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Cubemap>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Cubemap>(decltype(nullptr));
		Array1<UnityEngine::Cubemap>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Cubemap>(const Array1<UnityEngine::Cubemap>& other);
		Array1<UnityEngine::Cubemap>(Array1<UnityEngine::Cubemap>&& other);
		virtual ~Array1<UnityEngine::Cubemap>();
		Array1<UnityEngine::Cubemap>& operator=(const Array1<UnityEngine::Cubemap>& other);
		Array1<UnityEngine::Cubemap>& operator=(decltype(nullptr));
		Array1<UnityEngine::Cubemap>& operator=(Array1<UnityEngine::Cubemap>&& other);
		bool operator==(const Array1<UnityEngine::Cubemap>& other) const;
		bool operator!=(const Array1<UnityEngine::Cubemap>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Cubemap> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineCubemapArray1Iterator
	{
		System::Array1<UnityEngine::Cubemap>& array;
		int index;
		UnityEngineCubemapArray1Iterator(System::Array1<UnityEngine::Cubemap>& array, int32_t index);
		UnityEngineCubemapArray1Iterator& operator++();
		bool operator!=(const UnityEngineCubemapArray1Iterator& other);
		UnityEngine::Cubemap operator*();
	};
}

namespace System
{
	Plugin::UnityEngineCubemapArray1Iterator begin(System::Array1<UnityEngine::Cubemap>& array);
	Plugin::UnityEngineCubemapArray1Iterator end(System::Array1<UnityEngine::Cubemap>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Matrix4x4>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Matrix4x4 item);
		operator UnityEngine::Matrix4x4();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Matrix4x4> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Matrix4x4>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Matrix4x4>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Matrix4x4>(decltype(nullptr));
		Array1<UnityEngine::Matrix4x4>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Matrix4x4>(const Array1<UnityEngine::Matrix4x4>& other);
		Array1<UnityEngine::Matrix4x4>(Array1<UnityEngine::Matrix4x4>&& other);
		virtual ~Array1<UnityEngine::Matrix4x4>();
		Array1<UnityEngine::Matrix4x4>& operator=(const Array1<UnityEngine::Matrix4x4>& other);
		Array1<UnityEngine::Matrix4x4>& operator=(decltype(nullptr));
		Array1<UnityEngine::Matrix4x4>& operator=(Array1<UnityEngine::Matrix4x4>&& other);
		bool operator==(const Array1<UnityEngine::Matrix4x4>& other) const;
		bool operator!=(const Array1<UnityEngine::Matrix4x4>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Matrix4x4> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineMatrix4x4Array1Iterator
	{
		System::Array1<UnityEngine::Matrix4x4>& array;
		int index;
		UnityEngineMatrix4x4Array1Iterator(System::Array1<UnityEngine::Matrix4x4>& array, int32_t index);
		UnityEngineMatrix4x4Array1Iterator& operator++();
		bool operator!=(const UnityEngineMatrix4x4Array1Iterator& other);
		UnityEngine::Matrix4x4 operator*();
	};
}

namespace System
{
	Plugin::UnityEngineMatrix4x4Array1Iterator begin(System::Array1<UnityEngine::Matrix4x4>& array);
	Plugin::UnityEngineMatrix4x4Array1Iterator end(System::Array1<UnityEngine::Matrix4x4>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Color>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Color item);
		operator UnityEngine::Color();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Color> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Color>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Color>(decltype(nullptr));
		Array1<UnityEngine::Color>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Color>(const Array1<UnityEngine::Color>& other);
		Array1<UnityEngine::Color>(Array1<UnityEngine::Color>&& other);
		virtual ~Array1<UnityEngine::Color>();
		Array1<UnityEngine::Color>& operator=(const Array1<UnityEngine::Color>& other);
		Array1<UnityEngine::Color>& operator=(decltype(nullptr));
		Array1<UnityEngine::Color>& operator=(Array1<UnityEngine::Color>&& other);
		bool operator==(const Array1<UnityEngine::Color>& other) const;
		bool operator!=(const Array1<UnityEngine::Color>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Color> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineColorArray1Iterator
	{
		System::Array1<UnityEngine::Color>& array;
		int index;
		UnityEngineColorArray1Iterator(System::Array1<UnityEngine::Color>& array, int32_t index);
		UnityEngineColorArray1Iterator& operator++();
		bool operator!=(const UnityEngineColorArray1Iterator& other);
		UnityEngine::Color operator*();
	};
}

namespace System
{
	Plugin::UnityEngineColorArray1Iterator begin(System::Array1<UnityEngine::Color>& array);
	Plugin::UnityEngineColorArray1Iterator end(System::Array1<UnityEngine::Color>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Color32>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Color32 item);
		operator UnityEngine::Color32();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Color32> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Color32>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Color32>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Color32>(decltype(nullptr));
		Array1<UnityEngine::Color32>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Color32>(const Array1<UnityEngine::Color32>& other);
		Array1<UnityEngine::Color32>(Array1<UnityEngine::Color32>&& other);
		virtual ~Array1<UnityEngine::Color32>();
		Array1<UnityEngine::Color32>& operator=(const Array1<UnityEngine::Color32>& other);
		Array1<UnityEngine::Color32>& operator=(decltype(nullptr));
		Array1<UnityEngine::Color32>& operator=(Array1<UnityEngine::Color32>&& other);
		bool operator==(const Array1<UnityEngine::Color32>& other) const;
		bool operator!=(const Array1<UnityEngine::Color32>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Color32> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineColor32Array1Iterator
	{
		System::Array1<UnityEngine::Color32>& array;
		int index;
		UnityEngineColor32Array1Iterator(System::Array1<UnityEngine::Color32>& array, int32_t index);
		UnityEngineColor32Array1Iterator& operator++();
		bool operator!=(const UnityEngineColor32Array1Iterator& other);
		UnityEngine::Color32 operator*();
	};
}

namespace System
{
	Plugin::UnityEngineColor32Array1Iterator begin(System::Array1<UnityEngine::Color32>& array);
	Plugin::UnityEngineColor32Array1Iterator end(System::Array1<UnityEngine::Color32>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Vector2>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Vector2 item);
		operator UnityEngine::Vector2();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Vector2> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Vector2>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector2>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Vector2>(decltype(nullptr));
		Array1<UnityEngine::Vector2>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Vector2>(const Array1<UnityEngine::Vector2>& other);
		Array1<UnityEngine::Vector2>(Array1<UnityEngine::Vector2>&& other);
		virtual ~Array1<UnityEngine::Vector2>();
		Array1<UnityEngine::Vector2>& operator=(const Array1<UnityEngine::Vector2>& other);
		Array1<UnityEngine::Vector2>& operator=(decltype(nullptr));
		Array1<UnityEngine::Vector2>& operator=(Array1<UnityEngine::Vector2>&& other);
		bool operator==(const Array1<UnityEngine::Vector2>& other) const;
		bool operator!=(const Array1<UnityEngine::Vector2>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Vector2> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineVector2Array1Iterator
	{
		System::Array1<UnityEngine::Vector2>& array;
		int index;
		UnityEngineVector2Array1Iterator(System::Array1<UnityEngine::Vector2>& array, int32_t index);
		UnityEngineVector2Array1Iterator& operator++();
		bool operator!=(const UnityEngineVector2Array1Iterator& other);
		UnityEngine::Vector2 operator*();
	};
}

namespace System
{
	Plugin::UnityEngineVector2Array1Iterator begin(System::Array1<UnityEngine::Vector2>& array);
	Plugin::UnityEngineVector2Array1Iterator end(System::Array1<UnityEngine::Vector2>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Vector3>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Vector3 item);
		operator UnityEngine::Vector3();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Vector3> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Vector3>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector3>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Vector3>(decltype(nullptr));
		Array1<UnityEngine::Vector3>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Vector3>(const Array1<UnityEngine::Vector3>& other);
		Array1<UnityEngine::Vector3>(Array1<UnityEngine::Vector3>&& other);
		virtual ~Array1<UnityEngine::Vector3>();
		Array1<UnityEngine::Vector3>& operator=(const Array1<UnityEngine::Vector3>& other);
		Array1<UnityEngine::Vector3>& operator=(decltype(nullptr));
		Array1<UnityEngine::Vector3>& operator=(Array1<UnityEngine::Vector3>&& other);
		bool operator==(const Array1<UnityEngine::Vector3>& other) const;
		bool operator!=(const Array1<UnityEngine::Vector3>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Vector3> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineVector3Array1Iterator
	{
		System::Array1<UnityEngine::Vector3>& array;
		int index;
		UnityEngineVector3Array1Iterator(System::Array1<UnityEngine::Vector3>& array, int32_t index);
		UnityEngineVector3Array1Iterator& operator++();
		bool operator!=(const UnityEngineVector3Array1Iterator& other);
		UnityEngine::Vector3 operator*();
	};
}

namespace System
{
	Plugin::UnityEngineVector3Array1Iterator begin(System::Array1<UnityEngine::Vector3>& array);
	Plugin::UnityEngineVector3Array1Iterator end(System::Array1<UnityEngine::Vector3>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Vector4>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Vector4 item);
		operator UnityEngine::Vector4();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Vector4> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Vector4>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Vector4>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Vector4>(decltype(nullptr));
		Array1<UnityEngine::Vector4>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Vector4>(const Array1<UnityEngine::Vector4>& other);
		Array1<UnityEngine::Vector4>(Array1<UnityEngine::Vector4>&& other);
		virtual ~Array1<UnityEngine::Vector4>();
		Array1<UnityEngine::Vector4>& operator=(const Array1<UnityEngine::Vector4>& other);
		Array1<UnityEngine::Vector4>& operator=(decltype(nullptr));
		Array1<UnityEngine::Vector4>& operator=(Array1<UnityEngine::Vector4>&& other);
		bool operator==(const Array1<UnityEngine::Vector4>& other) const;
		bool operator!=(const Array1<UnityEngine::Vector4>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Vector4> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineVector4Array1Iterator
	{
		System::Array1<UnityEngine::Vector4>& array;
		int index;
		UnityEngineVector4Array1Iterator(System::Array1<UnityEngine::Vector4>& array, int32_t index);
		UnityEngineVector4Array1Iterator& operator++();
		bool operator!=(const UnityEngineVector4Array1Iterator& other);
		UnityEngine::Vector4 operator*();
	};
}

namespace System
{
	Plugin::UnityEngineVector4Array1Iterator begin(System::Array1<UnityEngine::Vector4>& array);
	Plugin::UnityEngineVector4Array1Iterator end(System::Array1<UnityEngine::Vector4>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::BoneWeight>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::BoneWeight item);
		operator UnityEngine::BoneWeight();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::BoneWeight> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::BoneWeight>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::BoneWeight>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::BoneWeight>(decltype(nullptr));
		Array1<UnityEngine::BoneWeight>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::BoneWeight>(const Array1<UnityEngine::BoneWeight>& other);
		Array1<UnityEngine::BoneWeight>(Array1<UnityEngine::BoneWeight>&& other);
		virtual ~Array1<UnityEngine::BoneWeight>();
		Array1<UnityEngine::BoneWeight>& operator=(const Array1<UnityEngine::BoneWeight>& other);
		Array1<UnityEngine::BoneWeight>& operator=(decltype(nullptr));
		Array1<UnityEngine::BoneWeight>& operator=(Array1<UnityEngine::BoneWeight>&& other);
		bool operator==(const Array1<UnityEngine::BoneWeight>& other) const;
		bool operator!=(const Array1<UnityEngine::BoneWeight>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::BoneWeight> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineBoneWeightArray1Iterator
	{
		System::Array1<UnityEngine::BoneWeight>& array;
		int index;
		UnityEngineBoneWeightArray1Iterator(System::Array1<UnityEngine::BoneWeight>& array, int32_t index);
		UnityEngineBoneWeightArray1Iterator& operator++();
		bool operator!=(const UnityEngineBoneWeightArray1Iterator& other);
		UnityEngine::BoneWeight operator*();
	};
}

namespace System
{
	Plugin::UnityEngineBoneWeightArray1Iterator begin(System::Array1<UnityEngine::BoneWeight>& array);
	Plugin::UnityEngineBoneWeightArray1Iterator end(System::Array1<UnityEngine::BoneWeight>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UnityEngine::Quaternion>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UnityEngine::Quaternion item);
		operator UnityEngine::Quaternion();
	};
}

namespace System
{
	template<> struct Array1<UnityEngine::Quaternion> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UnityEngine::Quaternion>, virtual System::Collections::Generic::IReadOnlyList_1<UnityEngine::Quaternion>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UnityEngine::Quaternion>(decltype(nullptr));
		Array1<UnityEngine::Quaternion>(Plugin::InternalUse, int32_t handle);
		Array1<UnityEngine::Quaternion>(const Array1<UnityEngine::Quaternion>& other);
		Array1<UnityEngine::Quaternion>(Array1<UnityEngine::Quaternion>&& other);
		virtual ~Array1<UnityEngine::Quaternion>();
		Array1<UnityEngine::Quaternion>& operator=(const Array1<UnityEngine::Quaternion>& other);
		Array1<UnityEngine::Quaternion>& operator=(decltype(nullptr));
		Array1<UnityEngine::Quaternion>& operator=(Array1<UnityEngine::Quaternion>&& other);
		bool operator==(const Array1<UnityEngine::Quaternion>& other) const;
		bool operator!=(const Array1<UnityEngine::Quaternion>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UnityEngine::Quaternion> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UnityEngineQuaternionArray1Iterator
	{
		System::Array1<UnityEngine::Quaternion>& array;
		int index;
		UnityEngineQuaternionArray1Iterator(System::Array1<UnityEngine::Quaternion>& array, int32_t index);
		UnityEngineQuaternionArray1Iterator& operator++();
		bool operator!=(const UnityEngineQuaternionArray1Iterator& other);
		UnityEngine::Quaternion operator*();
	};
}

namespace System
{
	Plugin::UnityEngineQuaternionArray1Iterator begin(System::Array1<UnityEngine::Quaternion>& array);
	Plugin::UnityEngineQuaternionArray1Iterator end(System::Array1<UnityEngine::Quaternion>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderState>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UPlasma::PlEmu::PlResponderState item);
		operator UPlasma::PlEmu::PlResponderState();
	};
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::PlResponderState> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderState>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderState>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UPlasma::PlEmu::PlResponderState>(decltype(nullptr));
		Array1<UPlasma::PlEmu::PlResponderState>(Plugin::InternalUse, int32_t handle);
		Array1<UPlasma::PlEmu::PlResponderState>(const Array1<UPlasma::PlEmu::PlResponderState>& other);
		Array1<UPlasma::PlEmu::PlResponderState>(Array1<UPlasma::PlEmu::PlResponderState>&& other);
		virtual ~Array1<UPlasma::PlEmu::PlResponderState>();
		Array1<UPlasma::PlEmu::PlResponderState>& operator=(const Array1<UPlasma::PlEmu::PlResponderState>& other);
		Array1<UPlasma::PlEmu::PlResponderState>& operator=(decltype(nullptr));
		Array1<UPlasma::PlEmu::PlResponderState>& operator=(Array1<UPlasma::PlEmu::PlResponderState>&& other);
		bool operator==(const Array1<UPlasma::PlEmu::PlResponderState>& other) const;
		bool operator!=(const Array1<UPlasma::PlEmu::PlResponderState>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderState> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UPlasmaPlEmuPlResponderStateArray1Iterator
	{
		System::Array1<UPlasma::PlEmu::PlResponderState>& array;
		int index;
		UPlasmaPlEmuPlResponderStateArray1Iterator(System::Array1<UPlasma::PlEmu::PlResponderState>& array, int32_t index);
		UPlasmaPlEmuPlResponderStateArray1Iterator& operator++();
		bool operator!=(const UPlasmaPlEmuPlResponderStateArray1Iterator& other);
		UPlasma::PlEmu::PlResponderState operator*();
	};
}

namespace System
{
	Plugin::UPlasmaPlEmuPlResponderStateArray1Iterator begin(System::Array1<UPlasma::PlEmu::PlResponderState>& array);
	Plugin::UPlasmaPlEmuPlResponderStateArray1Iterator end(System::Array1<UPlasma::PlEmu::PlResponderState>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderCommand>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UPlasma::PlEmu::PlResponderCommand item);
		operator UPlasma::PlEmu::PlResponderCommand();
	};
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::PlResponderCommand> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlResponderCommand>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlResponderCommand>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UPlasma::PlEmu::PlResponderCommand>(decltype(nullptr));
		Array1<UPlasma::PlEmu::PlResponderCommand>(Plugin::InternalUse, int32_t handle);
		Array1<UPlasma::PlEmu::PlResponderCommand>(const Array1<UPlasma::PlEmu::PlResponderCommand>& other);
		Array1<UPlasma::PlEmu::PlResponderCommand>(Array1<UPlasma::PlEmu::PlResponderCommand>&& other);
		virtual ~Array1<UPlasma::PlEmu::PlResponderCommand>();
		Array1<UPlasma::PlEmu::PlResponderCommand>& operator=(const Array1<UPlasma::PlEmu::PlResponderCommand>& other);
		Array1<UPlasma::PlEmu::PlResponderCommand>& operator=(decltype(nullptr));
		Array1<UPlasma::PlEmu::PlResponderCommand>& operator=(Array1<UPlasma::PlEmu::PlResponderCommand>&& other);
		bool operator==(const Array1<UPlasma::PlEmu::PlResponderCommand>& other) const;
		bool operator!=(const Array1<UPlasma::PlEmu::PlResponderCommand>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UPlasma::PlEmu::PlResponderCommand> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UPlasmaPlEmuPlResponderCommandArray1Iterator
	{
		System::Array1<UPlasma::PlEmu::PlResponderCommand>& array;
		int index;
		UPlasmaPlEmuPlResponderCommandArray1Iterator(System::Array1<UPlasma::PlEmu::PlResponderCommand>& array, int32_t index);
		UPlasmaPlEmuPlResponderCommandArray1Iterator& operator++();
		bool operator!=(const UPlasmaPlEmuPlResponderCommandArray1Iterator& other);
		UPlasma::PlEmu::PlResponderCommand operator*();
	};
}

namespace System
{
	Plugin::UPlasmaPlEmuPlResponderCommandArray1Iterator begin(System::Array1<UPlasma::PlEmu::PlResponderCommand>& array);
	Plugin::UPlasmaPlEmuPlResponderCommandArray1Iterator end(System::Array1<UPlasma::PlEmu::PlResponderCommand>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::PlSound>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UPlasma::PlEmu::PlSound item);
		operator UPlasma::PlEmu::PlSound();
	};
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::PlSound> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::PlSound>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::PlSound>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UPlasma::PlEmu::PlSound>(decltype(nullptr));
		Array1<UPlasma::PlEmu::PlSound>(Plugin::InternalUse, int32_t handle);
		Array1<UPlasma::PlEmu::PlSound>(const Array1<UPlasma::PlEmu::PlSound>& other);
		Array1<UPlasma::PlEmu::PlSound>(Array1<UPlasma::PlEmu::PlSound>&& other);
		virtual ~Array1<UPlasma::PlEmu::PlSound>();
		Array1<UPlasma::PlEmu::PlSound>& operator=(const Array1<UPlasma::PlEmu::PlSound>& other);
		Array1<UPlasma::PlEmu::PlSound>& operator=(decltype(nullptr));
		Array1<UPlasma::PlEmu::PlSound>& operator=(Array1<UPlasma::PlEmu::PlSound>&& other);
		bool operator==(const Array1<UPlasma::PlEmu::PlSound>& other) const;
		bool operator!=(const Array1<UPlasma::PlEmu::PlSound>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UPlasma::PlEmu::PlSound> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UPlasmaPlEmuPlSoundArray1Iterator
	{
		System::Array1<UPlasma::PlEmu::PlSound>& array;
		int index;
		UPlasmaPlEmuPlSoundArray1Iterator(System::Array1<UPlasma::PlEmu::PlSound>& array, int32_t index);
		UPlasmaPlEmuPlSoundArray1Iterator& operator++();
		bool operator!=(const UPlasmaPlEmuPlSoundArray1Iterator& other);
		UPlasma::PlEmu::PlSound operator*();
	};
}

namespace System
{
	Plugin::UPlasmaPlEmuPlSoundArray1Iterator begin(System::Array1<UPlasma::PlEmu::PlSound>& array);
	Plugin::UPlasmaPlEmuPlSoundArray1Iterator end(System::Array1<UPlasma::PlEmu::PlSound>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::IAgApplicator>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UPlasma::PlEmu::IAgApplicator item);
		operator UPlasma::PlEmu::IAgApplicator();
	};
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::IAgApplicator> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::IAgApplicator>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::IAgApplicator>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UPlasma::PlEmu::IAgApplicator>(decltype(nullptr));
		Array1<UPlasma::PlEmu::IAgApplicator>(Plugin::InternalUse, int32_t handle);
		Array1<UPlasma::PlEmu::IAgApplicator>(const Array1<UPlasma::PlEmu::IAgApplicator>& other);
		Array1<UPlasma::PlEmu::IAgApplicator>(Array1<UPlasma::PlEmu::IAgApplicator>&& other);
		virtual ~Array1<UPlasma::PlEmu::IAgApplicator>();
		Array1<UPlasma::PlEmu::IAgApplicator>& operator=(const Array1<UPlasma::PlEmu::IAgApplicator>& other);
		Array1<UPlasma::PlEmu::IAgApplicator>& operator=(decltype(nullptr));
		Array1<UPlasma::PlEmu::IAgApplicator>& operator=(Array1<UPlasma::PlEmu::IAgApplicator>&& other);
		bool operator==(const Array1<UPlasma::PlEmu::IAgApplicator>& other) const;
		bool operator!=(const Array1<UPlasma::PlEmu::IAgApplicator>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UPlasma::PlEmu::IAgApplicator> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UPlasmaPlEmuIAgApplicatorArray1Iterator
	{
		System::Array1<UPlasma::PlEmu::IAgApplicator>& array;
		int index;
		UPlasmaPlEmuIAgApplicatorArray1Iterator(System::Array1<UPlasma::PlEmu::IAgApplicator>& array, int32_t index);
		UPlasmaPlEmuIAgApplicatorArray1Iterator& operator++();
		bool operator!=(const UPlasmaPlEmuIAgApplicatorArray1Iterator& other);
		UPlasma::PlEmu::IAgApplicator operator*();
	};
}

namespace System
{
	Plugin::UPlasmaPlEmuIAgApplicatorArray1Iterator begin(System::Array1<UPlasma::PlEmu::IAgApplicator>& array);
	Plugin::UPlasmaPlEmuIAgApplicatorArray1Iterator end(System::Array1<UPlasma::PlEmu::IAgApplicator>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<UPlasma::PlEmu::AgAnim>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(UPlasma::PlEmu::AgAnim item);
		operator UPlasma::PlEmu::AgAnim();
	};
}

namespace System
{
	template<> struct Array1<UPlasma::PlEmu::AgAnim> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<UPlasma::PlEmu::AgAnim>, virtual System::Collections::Generic::IReadOnlyList_1<UPlasma::PlEmu::AgAnim>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<UPlasma::PlEmu::AgAnim>(decltype(nullptr));
		Array1<UPlasma::PlEmu::AgAnim>(Plugin::InternalUse, int32_t handle);
		Array1<UPlasma::PlEmu::AgAnim>(const Array1<UPlasma::PlEmu::AgAnim>& other);
		Array1<UPlasma::PlEmu::AgAnim>(Array1<UPlasma::PlEmu::AgAnim>&& other);
		virtual ~Array1<UPlasma::PlEmu::AgAnim>();
		Array1<UPlasma::PlEmu::AgAnim>& operator=(const Array1<UPlasma::PlEmu::AgAnim>& other);
		Array1<UPlasma::PlEmu::AgAnim>& operator=(decltype(nullptr));
		Array1<UPlasma::PlEmu::AgAnim>& operator=(Array1<UPlasma::PlEmu::AgAnim>&& other);
		bool operator==(const Array1<UPlasma::PlEmu::AgAnim>& other) const;
		bool operator!=(const Array1<UPlasma::PlEmu::AgAnim>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<UPlasma::PlEmu::AgAnim> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct UPlasmaPlEmuAgAnimArray1Iterator
	{
		System::Array1<UPlasma::PlEmu::AgAnim>& array;
		int index;
		UPlasmaPlEmuAgAnimArray1Iterator(System::Array1<UPlasma::PlEmu::AgAnim>& array, int32_t index);
		UPlasmaPlEmuAgAnimArray1Iterator& operator++();
		bool operator!=(const UPlasmaPlEmuAgAnimArray1Iterator& other);
		UPlasma::PlEmu::AgAnim operator*();
	};
}

namespace System
{
	Plugin::UPlasmaPlEmuAgAnimArray1Iterator begin(System::Array1<UPlasma::PlEmu::AgAnim>& array);
	Plugin::UPlasmaPlEmuAgAnimArray1Iterator end(System::Array1<UPlasma::PlEmu::AgAnim>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::Single>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<System::Single>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(System::Single item);
		operator System::Single();
	};
}

namespace System
{
	template<> struct Array1<System::Single> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<System::Single>, virtual System::Collections::Generic::IReadOnlyList_1<System::Single>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<System::Single>(decltype(nullptr));
		Array1<System::Single>(Plugin::InternalUse, int32_t handle);
		Array1<System::Single>(const Array1<System::Single>& other);
		Array1<System::Single>(Array1<System::Single>&& other);
		virtual ~Array1<System::Single>();
		Array1<System::Single>& operator=(const Array1<System::Single>& other);
		Array1<System::Single>& operator=(decltype(nullptr));
		Array1<System::Single>& operator=(Array1<System::Single>&& other);
		bool operator==(const Array1<System::Single>& other) const;
		bool operator!=(const Array1<System::Single>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<System::Single> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct SystemSingleArray1Iterator
	{
		System::Array1<System::Single>& array;
		int index;
		SystemSingleArray1Iterator(System::Array1<System::Single>& array, int32_t index);
		SystemSingleArray1Iterator& operator++();
		bool operator!=(const SystemSingleArray1Iterator& other);
		System::Single operator*();
	};
}

namespace System
{
	Plugin::SystemSingleArray1Iterator begin(System::Array1<System::Single>& array);
	Plugin::SystemSingleArray1Iterator end(System::Array1<System::Single>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::Int32>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<System::Int32>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(System::Int32 item);
		operator System::Int32();
	};
}

namespace System
{
	template<> struct Array1<System::Int32> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<System::Int32>, virtual System::Collections::Generic::IReadOnlyList_1<System::Int32>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<System::Int32>(decltype(nullptr));
		Array1<System::Int32>(Plugin::InternalUse, int32_t handle);
		Array1<System::Int32>(const Array1<System::Int32>& other);
		Array1<System::Int32>(Array1<System::Int32>&& other);
		virtual ~Array1<System::Int32>();
		Array1<System::Int32>& operator=(const Array1<System::Int32>& other);
		Array1<System::Int32>& operator=(decltype(nullptr));
		Array1<System::Int32>& operator=(Array1<System::Int32>&& other);
		bool operator==(const Array1<System::Int32>& other) const;
		bool operator!=(const Array1<System::Int32>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<System::Int32> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct SystemInt32Array1Iterator
	{
		System::Array1<System::Int32>& array;
		int index;
		SystemInt32Array1Iterator(System::Array1<System::Int32>& array, int32_t index);
		SystemInt32Array1Iterator& operator++();
		bool operator!=(const SystemInt32Array1Iterator& other);
		System::Int32 operator*();
	};
}

namespace System
{
	Plugin::SystemInt32Array1Iterator begin(System::Array1<System::Int32>& array);
	Plugin::SystemInt32Array1Iterator end(System::Array1<System::Int32>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::Byte>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<System::Byte>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(System::Byte item);
		operator System::Byte();
	};
}

namespace System
{
	template<> struct Array1<System::Byte> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<System::Byte>, virtual System::Collections::Generic::IReadOnlyList_1<System::Byte>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<System::Byte>(decltype(nullptr));
		Array1<System::Byte>(Plugin::InternalUse, int32_t handle);
		Array1<System::Byte>(const Array1<System::Byte>& other);
		Array1<System::Byte>(Array1<System::Byte>&& other);
		virtual ~Array1<System::Byte>();
		Array1<System::Byte>& operator=(const Array1<System::Byte>& other);
		Array1<System::Byte>& operator=(decltype(nullptr));
		Array1<System::Byte>& operator=(Array1<System::Byte>&& other);
		bool operator==(const Array1<System::Byte>& other) const;
		bool operator!=(const Array1<System::Byte>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<System::Byte> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct SystemByteArray1Iterator
	{
		System::Array1<System::Byte>& array;
		int index;
		SystemByteArray1Iterator(System::Array1<System::Byte>& array, int32_t index);
		SystemByteArray1Iterator& operator++();
		bool operator!=(const SystemByteArray1Iterator& other);
		System::Byte operator*();
	};
}

namespace System
{
	Plugin::SystemByteArray1Iterator begin(System::Array1<System::Byte>& array);
	Plugin::SystemByteArray1Iterator end(System::Array1<System::Byte>& array);
}

namespace Plugin
{
	template<> struct ArrayElementProxy1_1<System::String>
	{
		int32_t Handle;
		int32_t Index0;
		ArrayElementProxy1_1<System::String>(Plugin::InternalUse, int32_t handle, int32_t index0);
		void operator=(System::String item);
		operator System::String();
	};
}

namespace System
{
	template<> struct Array1<System::String> : virtual System::Array, virtual System::ICloneable, virtual System::Collections::IList, virtual System::Collections::Generic::IList_1<System::String>, virtual System::Collections::Generic::IReadOnlyList_1<System::String>, virtual System::Collections::IStructuralComparable, virtual System::Collections::IStructuralEquatable
	{
		Array1<System::String>(decltype(nullptr));
		Array1<System::String>(Plugin::InternalUse, int32_t handle);
		Array1<System::String>(const Array1<System::String>& other);
		Array1<System::String>(Array1<System::String>&& other);
		virtual ~Array1<System::String>();
		Array1<System::String>& operator=(const Array1<System::String>& other);
		Array1<System::String>& operator=(decltype(nullptr));
		Array1<System::String>& operator=(Array1<System::String>&& other);
		bool operator==(const Array1<System::String>& other) const;
		bool operator!=(const Array1<System::String>& other) const;
		int32_t InternalLength;
		Array1(System::Int32 length0);
		System::Int32 GetLength();
		System::Int32 GetRank();
		Plugin::ArrayElementProxy1_1<System::String> operator[](int32_t index);
	};
}

namespace Plugin
{
	struct SystemStringArray1Iterator
	{
		System::Array1<System::String>& array;
		int index;
		SystemStringArray1Iterator(System::Array1<System::String>& array, int32_t index);
		SystemStringArray1Iterator& operator++();
		bool operator!=(const SystemStringArray1Iterator& other);
		System::String operator*();
	};
}

namespace System
{
	Plugin::SystemStringArray1Iterator begin(System::Array1<System::String>& array);
	Plugin::SystemStringArray1Iterator end(System::Array1<System::String>& array);
}
/*END TYPE DEFINITIONS*/

/*BEGIN MACROS*/
#define UPLASMA_PL_EMU_PLASMA_NATIVE_MANAGER_DEFAULT_CONSTRUCTOR_DECLARATION \
		PlasmaNativeManager(Plugin::InternalUse iu, int32_t handle);

#define UPLASMA_PL_EMU_PLASMA_NATIVE_MANAGER_DEFAULT_CONSTRUCTOR_DEFINITION \
		PlasmaNativeManager::PlasmaNativeManager(Plugin::InternalUse iu, int32_t handle) \
			: UnityEngine::Object(nullptr) \
			, UnityEngine::Component(nullptr) \
			, UnityEngine::Behaviour(nullptr) \
			, UnityEngine::MonoBehaviour(nullptr) \
			, UPlasma::PlEmu::AbstractBasePlasmaNativeManager(nullptr) \
			, UPlasma::PlEmu::BasePlasmaNativeManager(iu, handle)
#define UPLASMA_PL_EMU_PLASMA_NATIVE_MANAGER_DEFAULT_CONSTRUCTOR \
		PlasmaNativeManager(Plugin::InternalUse iu, int32_t handle) \
			: UnityEngine::Object(nullptr) \
			, UnityEngine::Component(nullptr) \
			, UnityEngine::Behaviour(nullptr) \
			, UnityEngine::MonoBehaviour(nullptr) \
			, UPlasma::PlEmu::AbstractBasePlasmaNativeManager(nullptr) \
			, UPlasma::PlEmu::BasePlasmaNativeManager(iu, handle) \
		{ \
		} \
/*END MACROS*/

////////////////////////////////////////////////////////////////
// Support for using IEnumerable with range for loops
////////////////////////////////////////////////////////////////

namespace Plugin
{
	struct EnumerableIterator
	{
		System::Collections::IEnumerator enumerator;
		bool hasMore;
		EnumerableIterator(decltype(nullptr));
		EnumerableIterator(System::Collections::IEnumerable& enumerable);
		EnumerableIterator& operator++();
		bool operator!=(const EnumerableIterator& other);
		System::Object operator*();
	};
}

namespace System
{
	namespace Collections
	{
		Plugin::EnumerableIterator begin(IEnumerable& enumerable);
		Plugin::EnumerableIterator end(IEnumerable& enumerable);
	}
}

////////////////////////////////////////////////////////////////
// User-defined literals for creating decimals (System.Decimal)
////////////////////////////////////////////////////////////////

System::Decimal operator"" _m(long double x);
System::Decimal operator"" _m(unsigned long long x);
