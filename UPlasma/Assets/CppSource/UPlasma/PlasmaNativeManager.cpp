/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#include "PlasmaNativeManager.h"

using namespace System;
using namespace UnityEngine;

namespace UPlasma
{
	namespace PlEmu
	{
		void PlasmaNativeManager::Start()
		{
		}
		void PlasmaNativeManager::SetCachePath(System::String & path)
		{
			char ossep = PlImporter::ImporterUtils::pathSeparator;
			cachePath = PlImporter::ImporterUtils::sharpStringToTheoryString(path);
			if (!cachePath.ends_with("/") && !cachePath.ends_with("\\"))
				cachePath += ossep;
		}
		void PlasmaNativeManager::SetToolsPath(System::String & path)
		{
			char ossep = PlImporter::ImporterUtils::pathSeparator;
			toolsPath = PlImporter::ImporterUtils::sharpStringToTheoryString(path);
			if (!toolsPath.ends_with("/") && !toolsPath.ends_with("\\"))
				toolsPath += ossep;
			PlImporter::PythonPAKReader::setPythonDecompilerPath(toolsPath + "Drizzle32.jar");
		}
		void PlasmaNativeManager::LoadAge(String & ageName)
		{
			PlImporter::ImporterUtils::init();
			char separator = '/';
			PlImporter::PageImporter pgImport;
			ST::string ageNameTheory = PlImporter::ImporterUtils::sharpStringToTheoryString(ageName);
			ST::string ageFileName(PlImporter::ImporterUtils::getGameFile((ST::string)
				"dat" + separator + ageNameTheory + ".age"));
			ST::string ageFniName(PlImporter::ImporterUtils::getGameFile((ST::string)
				"dat" + separator + ageNameTheory + ".fni"));
			String ageNameRelCs(((ST::string)"dat" + separator + ageNameTheory + ".age").c_str());
			PlasmaGame game(PlGameFileAccessor::GetInstance().GetFileGameType(ageNameRelCs));
			plResManager rmgr(PlImporter::ImporterUtils::getPlasmaVerFromGame(game));
			// set the extended resmanager so subimporters can search secific items by name
			PlImporter::ImporterUtils::curResManager = (PlImporter::ExtendedResManager*) &rmgr;
			String gameNameCs = NativeHelpers::ToString((System::Object)game);
			ST::string gameName = PlImporter::ImporterUtils::sharpStringToTheoryString(gameNameCs);
			ST::string fullCachePath = cachePath + gameName + separator + "ages" + separator + ageNameTheory + separator;
			String fullCachePathCs(fullCachePath.c_str());
			IO::Directory::CreateDirectory(fullCachePathCs);
			std::clock_t start = std::clock();
			plAgeInfo *age = rmgr.ReadAge(ageFileName, true);
			if (!age->getNumPages())
			{
				String errorMsg("Age doesn't have any page ?");
				Debug::LogError(errorMsg);
				return;
			}
			float ageOpenTime = (float)((std::clock() - start) / (double)CLOCKS_PER_SEC);

			char buf[256];
			sprintf_s(buf, 256, "Read Age %s", age->getAgeName().c_str());
			String msg(buf);
			Debug::Log(msg);

			String ageNameStr(age->getAgeName().c_str());
			GameObject ageParentObj(ageNameStr);
			PlAge uAge(ageParentObj.AddComponent<PlAge>());
			// note: add the asset binder to the age itself, as pages may share objects among themselves !
			AssetsLifeBinder binder = ageParentObj.AddComponent<AssetsLifeBinder>();
			float ageImportTime = 0;

			// try to load the FNI file
			ReadFni(ageFniName);

			// open the import clues
			PlGameFileAccessor accessor = PlGameFileAccessor::GetInstance();
			PlImporter::ImporterUtils::prpClues_Clear();
			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plAgeInfo::PageEntry entry(age->getPage(i));
				plLocation loc = age->getPageLoc(i, rmgr.getVer());
				String pageName(age->getPage(i).fName.c_str());
				PrpImportClues clues(accessor.GetPrpLoadClues(ageName, pageName, game));
				PlImporter::ImporterUtils::prpClues_Set(loc, clues);
				// init the list of known pages so we can access it later... otherwise
				// screws everything up
				PlImporter::ImporterUtils::keyToObj_InitPage(loc);
			}

			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plAgeInfo::PageEntry entry(age->getPage(i));
				//if (entry.fLoadFlags)
					// this page shouldn't be loaded. Well, actually we could load it, but we'd need to
					// immediately disable it.
					// To make loading faster while debugging, we'll just skip importing it...
					//continue;
				plLocation& loc = age->getPageLoc(i, rmgr.getVer());
				PrpImportClues clues(PlImporter::ImporterUtils::prpClues_Get(loc));
				if (!clues.GetLoad())
					// loading clues tell us there is no need to load the page... ok.
					continue;
				String pageName(age->getPage(i).fName.c_str());

				GameObject pageParentObj(pageName);
				Transform ageParentTransform = ageParentObj.GetTransform();
				uAge.GetPages().Add(pageParentObj.AddComponent<PlPage>());
				pageParentObj.GetTransform().SetParent(ageParentTransform);

				start = std::clock();
				pgImport.import(rmgr, loc, pageParentObj, binder);
				float pageImportTime = (float)((std::clock() - start) / (double)CLOCKS_PER_SEC);
				sprintf_s(buf, 256, "Imported %s in %.2f seconds", age->getPage(i).fName.c_str(), pageImportTime);
				String msg2(buf);
				Debug::Log(msg2);
				ageImportTime += pageImportTime;
			}

			// second pass: setup all objects
			String msg2("Setting up relationships...");
			Debug::Log(msg2);
			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plLocation& loc = age->getPageLoc(i, rmgr.getVer());
				PrpImportClues clues(PlImporter::ImporterUtils::prpClues_Get(loc));
				if (!clues.GetLoad())
					// loading clues tell us there is no need to load the page... ok.
					continue;

				start = std::clock();
				pgImport.postProcessImport(rmgr, loc, binder);
				float pageSetupTime = (float)((std::clock() - start) / (double)CLOCKS_PER_SEC);
				ageImportTime += pageSetupTime;
			}

			// now clear our plKey to Unity objects map
			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plLocation& location = age->getPageLoc(i, rmgr.getVer());
				PlImporter::ImporterUtils::keyToObj_Clear(location);
			}
			// unset the resmgr
			PlImporter::ImporterUtils::curResManager = nullptr;

			// save all clues
			PlImporter::ImporterUtils::prpClues_SaveAll();

			buf[256];
			sprintf_s(buf, 256, "Done in %.2f seconds (%.2f open time, %.2f import time)",
				ageOpenTime + ageImportTime,
				ageOpenTime,
				ageImportTime);
			String msg3(buf);
			Debug::Log(msg3);
		}
		void PlasmaNativeManager::LoadPages(String & ageName, System::Array1<System::String>& pageNames)
		{
			PlImporter::ImporterUtils::init();
			std::vector<ST::string> pageNamesToLoad;
			// FIXME: array foreach loop doesn't go over the last item
			//for (String pageToLoad : pageNames)
			for (int i = 0; i < pageNames.GetLength(); i++)
			{
				String pageToLoad = pageNames[i];
				ST::string pageToLoadT = PlImporter::ImporterUtils::sharpStringToTheoryString(pageToLoad);
				pageNamesToLoad.push_back(pageToLoadT);
			}

			char separator = '/';
			PlImporter::PageImporter pgImport;
			ST::string ageNameTheory = PlImporter::ImporterUtils::sharpStringToTheoryString(ageName);
			ST::string ageFileName(PlImporter::ImporterUtils::getGameFile("dat" + separator + ageNameTheory + ".age"));
			String ageNameRelCs(ageFileName.c_str());
			PlasmaGame game(PlGameFileAccessor::GetInstance().GetFileGameType(ageNameRelCs));
			plResManager rmgr(PlImporter::ImporterUtils::getPlasmaVerFromGame(game));
			// set the extended resmanager so subimporters can search secific items by name
			PlImporter::ImporterUtils::curResManager = (PlImporter::ExtendedResManager*) &rmgr;
			String gameNameCs = NativeHelpers::ToString((System::Object)game);
			ST::string gameName = PlImporter::ImporterUtils::sharpStringToTheoryString(gameNameCs);
			ST::string fullCachePath = cachePath + gameName + separator + "ages" + separator + ageNameTheory + separator;
			String fullCachePathCs(fullCachePath.c_str());
			IO::Directory::CreateDirectory(fullCachePathCs);
			std::clock_t start = std::clock();
			plAgeInfo *age = rmgr.ReadAge(ageFileName, true);
			float ageOpenTime = (float)((std::clock() - start) / (double)CLOCKS_PER_SEC);

			char buf[256];
			sprintf_s(buf, 256, "Read Age %s", age->getAgeName().c_str());
			String msg(buf);
			Debug::Log(msg);

			String ageNameStr(age->getAgeName().c_str());
			GameObject ageParentObj(ageNameStr);
			PlAge uAge(ageParentObj.AddComponent<PlAge>());
			AssetsLifeBinder binder = ageParentObj.AddComponent<AssetsLifeBinder>();
			float ageImportTime = 0;

			// open the import clues
			PlGameFileAccessor accessor = PlGameFileAccessor::GetInstance();
			PlImporter::ImporterUtils::prpClues_Clear();
			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plAgeInfo::PageEntry entry(age->getPage(i));
				plLocation loc = age->getPageLoc(i, rmgr.getVer());
				String pageName(age->getPage(i).fName.c_str());
				PrpImportClues clues(accessor.GetPrpLoadClues(ageName, pageName, game));
				PlImporter::ImporterUtils::prpClues_Set(loc, clues);
			}

			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plLocation& loc = age->getPageLoc(i, rmgr.getVer());
				ST::string pageNameTheory = age->getPage(i).fName.c_str();
				String pageName(pageNameTheory.c_str());
				bool found = false;
				for (ST::string pageToLoad : pageNamesToLoad)
				{
					if (pageToLoad == pageNameTheory)
					{
						found = true;
						break;
					}
				}
				if (!found)
					// this page isn't supposed to be loaded
					continue;

				GameObject pageParentObj(pageName);
				uAge.GetPages().Add(pageParentObj.AddComponent<PlPage>());
				Transform ageParentTransform = ageParentObj.GetTransform();
				pageParentObj.GetTransform().SetParent(ageParentTransform);

				start = std::clock();
				pgImport.import(rmgr, loc, pageParentObj, binder);
				float pageImportTime = (float)((std::clock() - start) / (double)CLOCKS_PER_SEC);
				sprintf_s(buf, 256, "Imported %s in %.2f seconds", age->getPage(i).fName.c_str(), pageImportTime);
				String msg2(buf);
				Debug::Log(msg2);
				ageImportTime += pageImportTime;
			}

			// second pass: setup all objects
			String msg2("Setting up relationships...");
			Debug::Log(msg2);
			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plLocation& loc = age->getPageLoc(i, rmgr.getVer());

				start = std::clock();
				pgImport.postProcessImport(rmgr, loc, binder);
				float pageImportTime = (float)((std::clock() - start) / (double)CLOCKS_PER_SEC);
				ageImportTime += pageImportTime;
			}

			// now clear our plKey to Unity objects map
			for (int i = 0; i < (int)age->getNumPages(); i++)
			{
				plLocation& location = age->getPageLoc(i, rmgr.getVer());
				PlImporter::ImporterUtils::keyToObj_Clear(location);
			}
			// unset the resmgr
			PlImporter::ImporterUtils::curResManager = nullptr;

			// save all clues
			PlImporter::ImporterUtils::prpClues_SaveAll();

			buf[256];
			sprintf_s(buf, 256, "Done in %.2f seconds (%.2f open time, %.2f import time)",
				ageOpenTime + ageImportTime,
				ageOpenTime,
				ageImportTime);
			String msg3(buf);
			Debug::Log(msg3);
		}
		bool PlasmaNativeManager::ReadFni(String& filename)
		{
			ST::string filenameT = PlImporter::ImporterUtils::sharpStringToTheoryString(filename);
			return ReadFni(filenameT);
		}
		bool PlasmaNativeManager::ReadFni(ST::string filename)
		{
			// make sure the file exists first...
			if (!System::IO::File::Exists(String(filename.c_str())))
				return false;
			ST::string fniContentT;
			if (PlImporter::EncryptedTextFile::read(filename, fniContentT))
			{
				String fniContent(fniContentT.c_str());
				PlConsole::GetInstance().Parse(fniContent);
				return true;
			}
			// else fni failed to load, but we already warned the user about it.
			return false;
		}
		void PlasmaNativeManager::ReadPythonPak(System::String & pakName)
		{
			ST::string pakNameTheory(PlImporter::ImporterUtils::sharpStringToTheoryString(pakName));
			PlImporter::PythonPAKReader pyRdr(pakNameTheory);
			char separator = PlImporter::ImporterUtils::pathSeparator;
			ST::string pakFilename;

			ST::string pythonPath("python");
			pythonPath += separator;
			pythonPath += pakNameTheory;
			pakFilename = PlImporter::ImporterUtils::getGameFile(pythonPath);

			for (PlImporter::PythonPAKReader& pak : pythonPaks)
			{
				if (pak.getName() == pakNameTheory)
				{
					// someone is trying to override a PAK file.
					// ...
					// Sigh...
					char errorMsgc[256];
					sprintf_s(errorMsgc, sizeof errorMsgc, "Can't load %s, as it's already loaded. \
Reminder: overriding PAKs is not possible due to how script priority works in Plasma ! \
Instead give a unique name to the PAK, and let the normal script override process take over...", pakNameTheory.c_str());
					System::String errorMsg(errorMsgc);
					Debug::LogError(errorMsg);
					return;
				}
			}

			String pakFilenameCs(pakFilename.c_str());
			PlasmaGame game(PlGameFileAccessor::GetInstance().GetFileGameType(pakFilenameCs));
			String gameNameCs = NativeHelpers::ToString((System::Object)game);
			ST::string gameName = PlImporter::ImporterUtils::sharpStringToTheoryString(gameNameCs);
			ST::string pythonCachePath(cachePath + gameName + separator + "pythoncache" + separator);
			String pythonCachePathCs(pythonCachePath.c_str());
			IO::Directory::CreateDirectory(pythonCachePathCs);
			pyRdr.setPythonCachePath(pythonCachePath);
			if (pyRdr.read(pakFilename))
			{
				pythonPaks.push_back(pyRdr);
			}
			else
			{
				char errorMsgc[256];
				sprintf_s(errorMsgc, sizeof errorMsgc, "Can't load %s, may not be a Python package, or uses unsupported droid encryption", pakNameTheory.c_str());
				System::String errorMsg(errorMsgc);
				Debug::LogError(errorMsg);
			}
		}
		void PlasmaNativeManager::ReadPythonPaks()
		{
			char separator = PlImporter::ImporterUtils::pathSeparator;
			ST::string pakFolderT("python");
			pakFolderT += separator;
			String pakFolder(pakFolderT.c_str());
			if (!PlGameFileAccessor::GetInstance().DirectoryExists(pakFolder))
			{
				String errorMsg("Unable to read Python files, might be a MOUL install ?");
				Debug::LogError(errorMsg);
				return;
			}
			// Note: Python files are read from most recent to older, so that we always fetch the latest version of the Python script.
			Array1<String> filesByWriteDate(PlGameFileAccessor::GetInstance()
				.GetFilesByModificationDateDescending(pakFolder));
			for (int i = 0; i < filesByWriteDate.GetLength(); i++)
			{
				String fileName = filesByWriteDate[i];
				ST::string fileNameT(PlImporter::ImporterUtils::sharpStringToTheoryString(fileName));
				if (fileNameT.to_lower().ends_with(".pak"))
					ReadPythonPak(fileName);
			}
		}
		void PlasmaNativeManager::UnloadPythonPak(System::String & pakName)
		{
			ST::string pakNameTheory(PlImporter::ImporterUtils::sharpStringToTheoryString(pakName));
			for (int i = 0; i < pythonPaks.size(); i++)
			{
				if (pythonPaks[i].getName() == pakNameTheory)
				{
					pythonPaks.erase(pythonPaks.begin() + i);
					break;
				}
			}
		}
		void PlasmaNativeManager::GetPythonScript(System::String & pythonScriptName, HackReturnObject & pythonScript)
		{
			ST::string pythonScriptNameT = PlImporter::ImporterUtils::sharpStringToTheoryString(pythonScriptName) + ".py";

			/*ST::string msg1t = "Looking for script: " + pythonScriptNameT;
			String msg1(msg1t.c_str());
			Debug::Log(msg1);*/

			for (PlImporter::PythonPAKReader pack : pythonPaks)
			{
				int i = 0;
				for (ST::string fname : pack.getFileNames())
				{
					if (fname == pythonScriptNameT)
					{
						ST::string codeT = pack.decompileScript(i);
						String code(codeT.c_str());
						pythonScript.SetVal(code);
						return;
					}
					i++;
				}
			}
		}
		System::Boolean PlasmaNativeManager::HasPythonScript(System::String & pythonScriptName)
		{
			ST::string pythonScriptNameT = PlImporter::ImporterUtils::sharpStringToTheoryString(pythonScriptName) + ".py";

			for (PlImporter::PythonPAKReader pack : pythonPaks)
			{
				int i = 0;
				for (ST::string fname : pack.getFileNames())
				{
					if (fname == pythonScriptNameT)
						return true;
					i++;
				}
			}
			return false;
		}
		void PlasmaNativeManager::ReadEncryptedFile(System::String & fileName, UPlasma::PlEmu::HackReturnObject & decryptedFile)
		{
			String completeFilename(UPlasma::PlEmu::PlGameFileAccessor::GetInstance().GetFile(fileName));
			// make sure the file exists first...
			if (!System::IO::File::Exists(completeFilename))
				return;
			ST::string fileNameT(PlImporter::ImporterUtils::sharpStringToTheoryString(completeFilename));
			ST::string fniContentT;
			if (PlImporter::EncryptedTextFile::read(fileNameT, fniContentT))
			{
				String fniContent(fniContentT.c_str());
				decryptedFile.SetVal(fniContent);
			}
		}
	}
}


namespace
{
	struct GameState
	{
		//float SomeVariable;
	};

	GameState* gameState;
}

// Called when the plugin is initialized
void PluginMain(
	void* memory,
	int32_t memorySize,
	bool isFirstBoot)
{
	gameState = (GameState*)memory;
	if (isFirstBoot)
	{
		String message("Bindings loaded");
		Debug::Log(message);

		// find the Plasma manager
		UPlasma::PlEmu::PlasmaManager mgr(UPlasma::PlEmu::PlasmaManager::GetInstance());
		// and add a PlasmaNativeManager to it :)
		mgr.GetGameObject().AddComponent<UPlasma::PlEmu::BasePlasmaNativeManager>();
	}
}
