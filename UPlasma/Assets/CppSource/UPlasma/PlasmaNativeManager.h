/*
This file is part of UPlasma.

UPlasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UPlasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UPlasma.  If not, see <https://www.gnu.org/licenses/>
*/


#pragma once

#include "Bindings.h"

#pragma warning(push, 0)
#include "ResManager/plResManager.h"
#pragma warning(pop)
#include "../PlImporter/PageImporter.h"
#include "../PlImporter/ImporterUtils.h"
#include "../PlImporter/EncryptedTextFileReader.h"
#include "../PlImporter/PythonPAKReader.h"

#include <ctime> // import time profiling

namespace UPlasma
{
	namespace PlEmu
	{
		struct PlasmaNativeManager : UPlasma::PlEmu::BasePlasmaNativeManager
		{
			UPLASMA_PL_EMU_PLASMA_NATIVE_MANAGER_DEFAULT_CONSTRUCTOR

			void Start() override;
			void SetCachePath(System::String& path) override;
			void SetToolsPath(System::String& path) override;
			void LoadAge(System::String& ageName) override;
			void LoadPages(System::String& ageName, System::Array1<System::String>& pageNames) override;
			bool ReadFni(System::String& filename);
			bool ReadFni(ST::string filename);
			void ReadPythonPak(System::String& pakName) override;
			void ReadPythonPaks() override;
			void UnloadPythonPak(System::String& pakName) override;
			void GetPythonScript(System::String& pythonScriptName, UPlasma::PlEmu::HackReturnObject& pythonScript) override;
			System::Boolean HasPythonScript(System::String& pythonScriptName) override;
			void ReadEncryptedFile(System::String& fileName, UPlasma::PlEmu::HackReturnObject& decryptedFile);

		private:
			//ST::string gamePath;
			ST::string cachePath;
			ST::string toolsPath;
			std::vector<PlImporter::PythonPAKReader> pythonPaks;
		};
	}
}
